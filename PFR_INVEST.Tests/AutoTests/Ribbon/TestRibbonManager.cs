﻿using PFR_INVEST.Ribbon;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PFR_INVEST.Tests.AutoTests.Ribbon
{
    
    [TestClass()]
    public class TestRibbonManager : BaseTest
    {

        [TestMethod()]
        public void TestRibbonManagerConstructor()
        {
            Microsoft.Windows.Controls.Ribbon.Ribbon ribbon = new Microsoft.Windows.Controls.Ribbon.Ribbon(); 
            RibbonManager target = new RibbonManager(ribbon);
        }

        
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUpdateState()
        {
            PrivateObject param0 = null; 
            RibbonManager_Accessor target = new RibbonManager_Accessor(param0);
            RibbonStates newState = RibbonStates.SettingsState;
            target.UpdateState(newState);
        }

        /// <summary>
        ///A test for State
        ///</summary>
        [TestMethod()]
        public void TestState()
        {
            Microsoft.Windows.Controls.Ribbon.Ribbon ribbon = new Microsoft.Windows.Controls.Ribbon.Ribbon(); 
            RibbonManager target = new RibbonManager(ribbon);
            RibbonStates expected = RibbonStates.SettingsState; 
            RibbonStates actual;
            target.State = expected;
            actual = target.State;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRibbonManagerConstructor1()
        {
            Microsoft.Windows.Controls.Ribbon.Ribbon ribbon = new Microsoft.Windows.Controls.Ribbon.Ribbon();
            RibbonManager target = new RibbonManager(ribbon);
        }

       
    }
}
