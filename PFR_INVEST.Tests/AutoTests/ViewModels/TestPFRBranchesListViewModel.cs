﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPFRBranchesListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestPFRBranchesListViewModelConstructor()
        {
            PFRBranchesListViewModel target = new PFRBranchesListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            PFRBranchesListViewModel_Accessor target = new PFRBranchesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            PFRBranchesListViewModel_Accessor target = new PFRBranchesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            PFRBranchesListViewModel_Accessor target = new PFRBranchesListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            PFRBranchesListViewModel_Accessor target = new PFRBranchesListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestList()
        {
            PFRBranchesListViewModel target = new PFRBranchesListViewModel();
            List<PFRBranchListItem> expected = null;
            List<PFRBranchListItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            PFRBranchesListViewModel target = new PFRBranchesListViewModel();
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
