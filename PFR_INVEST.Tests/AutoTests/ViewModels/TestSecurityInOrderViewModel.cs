﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSecurityInOrderViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSecurityInOrderViewModelConstructor()
        {
            long id = 1; 
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    SecurityInOrderViewModel_Accessor target = new SecurityInOrderViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action);

            target.RefreshLists();
        }


        [TestMethod()]
        public void TestCountVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action);
            Visibility expected = Visibility.Visible;
            Visibility actual;
            target.CountVisible = expected;
            actual = target.CountVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFaceValueVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.FaceValueVisible = expected;
            actual = target.FaceValueVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFixPriceVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.FixPriceVisible = expected;
            actual = target.FixPriceVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestHintVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.HintVisible = expected;
            actual = target.HintVisible;
            Assert.AreEqual(expected, actual);
        }

       

        [TestMethod()]
        public void TestMaxBuyPriceVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MaxBuyPriceVisible = expected;
            actual = target.MaxBuyPriceVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMaxCostVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MaxCostVisible = expected;
            actual = target.MaxCostVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMaxPriceVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MaxPriceVisible = expected;
            actual = target.MaxPriceVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMinCostVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MinCostVisible = expected;
            actual = target.MinCostVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMinPriceVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MinPriceVisible = expected;
            actual = target.MinPriceVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMinSellPriceVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.MinSellPriceVisible = expected;
            actual = target.MinSellPriceVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecuritiesList()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            List<SecurityItem> expected = null; 
            List<SecurityItem> actual;
            target.SecuritiesList = expected;
            actual = target.SecuritiesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecurity()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            SecurityItem expected = null; 
            SecurityItem actual;
            target.SelectedSecurity = expected;
            actual = target.SelectedSecurity;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecurityCurrency()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            string expected = string.Empty; 
            string actual;
            target.SelectedSecurityCurrency = expected;
            actual = target.SelectedSecurityCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecurityKind()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            string expected = string.Empty; 
            string actual;
            target.SelectedSecurityKind = expected;
            actual = target.SelectedSecurityKind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedSecurityNominal()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            string expected = string.Empty; 
            string actual;
            target.SelectedSecurityNominal = expected;
            actual = target.SelectedSecurityNominal;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumMoneyVisible()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            Visibility expected = new Visibility(); 
            Visibility actual;
            target.SumMoneyVisible = expected;
            actual = target.SumMoneyVisible;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            long id = 1;
            OrderViewModel _ordVM = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SecurityInOrderViewModel target = new SecurityInOrderViewModel(id, _ordVM, action); 
            SECURITY_IN_ORDER_TYPE actual;
            actual = target.Type;
        }
    }
}
