﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestOVSITransfersListViewModel : BaseTest
    {




        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            OVSITransfersListViewModel_Accessor target = new OVSITransfersListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            OVSITransfersListViewModel_Accessor target = new OVSITransfersListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            OVSITransfersListViewModel_Accessor target = new OVSITransfersListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            OVSITransfersListViewModel_Accessor target = new OVSITransfersListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            OVSITransfersListViewModel_Accessor target = new OVSITransfersListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOVSITransfersListViewModelConstructor()
        {
            OVSITransfersListViewModel target = new OVSITransfersListViewModel();
        }
    }
}
