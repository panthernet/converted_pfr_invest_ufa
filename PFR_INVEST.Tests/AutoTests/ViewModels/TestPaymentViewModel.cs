﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPaymentViewModel : BaseTest
    {

        [TestMethod()]
        public void TestPaymentViewModelConstructor()
        {
            Dictionary<string, DBField> parentSec = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(parentSec, action);
        }

        [TestMethod()]
        public void TestPaymentViewModelConstructor1()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(id, action);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            Dictionary<string, DBField> parentSec = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(parentSec, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    PaymentViewModel_Accessor target = new PaymentViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRecalcFields()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(id, action); 
            target.RecalcFields();
        }

       
        [TestMethod()]
        public void TestCouponSize()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(id, action);
            double expected = -1; 
            double actual;
            target.CouponSize = expected;
            actual = target.CouponSize;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIsCreateMode()
        {
            Dictionary<string, DBField> parentSec = null;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(parentSec, action); 
            bool actual;
            actual = target.IsCreateMode;
        }

        [TestMethod()]
        public void TestPaymentBond()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Create;
            PaymentViewModel target = new PaymentViewModel(id, action);
            double expected = 0F; 
            double actual;
            target.PaymentBond = expected;
            actual = target.PaymentBond;
            Assert.AreEqual(expected, actual);
        }
    }
}
