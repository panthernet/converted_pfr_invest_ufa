﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueDeadViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDueDeadViewModelConstructor()
        {
            DueDeadViewModel target = new DueDeadViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopAPS()
        {
            DueDeadViewModel_Accessor target = new DueDeadViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddDopAPS();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectDstAccount()
        {
            DueDeadViewModel_Accessor target = new DueDeadViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectDstAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectSrcAccount()
        {
            DueDeadViewModel_Accessor target = new DueDeadViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectSrcAccount();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DueDeadViewModel_Accessor target = new DueDeadViewModel_Accessor();
            target.ExecuteSave();
        }




        [TestMethod()]
        public void TestLoad()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            long lId = 1;
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestAddDopAPS()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            ICommand actual;
            actual = target.AddDopAPS;
        }

        [TestMethod()]
        public void TestDopAPSs()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopAPSs = expected;
            actual = target.DopAPSs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopAPSsGrid()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            List<PrepaymentAccurateListItem> expected = null;
            List<PrepaymentAccurateListItem> actual;
            target.DopAPSsGrid = expected;
            actual = target.DopAPSsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestSelectDstAccount()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            ICommand actual;
            actual = target.SelectDstAccount;
        }

        [TestMethod()]
        public void TestSelectSrcAccount()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            ICommand actual;
            actual = target.SelectSrcAccount;
        }

        [TestMethod()]
        public void TestSrcAccount()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueDeadViewModel target = new DueDeadViewModel();
            Decimal expected = new Decimal();
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
