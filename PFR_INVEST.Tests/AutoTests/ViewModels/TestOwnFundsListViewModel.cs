﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestOwnFundsListViewModel : BaseTest
    {


        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            OwnFundsListViewModel_Accessor target = new OwnFundsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            OwnFundsListViewModel_Accessor target = new OwnFundsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            OwnFundsListViewModel_Accessor target = new OwnFundsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOwnFundsListViewModelConstructor()
        {
            OwnFundsListViewModel target = new OwnFundsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            OwnFundsListViewModel_Accessor target = new OwnFundsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestF50List()
        {
            OwnFundsListViewModel target = new OwnFundsListViewModel();
            List<OwnFundsListItem> expected = null;
            List<OwnFundsListItem> actual;
            target.F50List = expected;
            actual = target.F50List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            OwnFundsListViewModel_Accessor target = new OwnFundsListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
