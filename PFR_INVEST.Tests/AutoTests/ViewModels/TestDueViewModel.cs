﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueViewModel : BaseTest
    {

        [TestMethod()]
        public void TestDueViewModelConstructor()
        {
            DueViewModel target = new DueViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopSPN()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddDopSPN();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueViewModel target = new DueViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            target.ExecuteSave();
        }

        

        [TestMethod()]
        public void TestLoad()
        {
            DueViewModel target = new DueViewModel();
            long lId = 1;
            target.Load(lId);
        }

       
        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestsortDopSPNsByDate()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            List<DBEntity> mixedList = null;
            target.sortDopSPNsByDate(mixedList);
        }

        [TestMethod()]
        public void TestAccount()
        {
            DueViewModel target = new DueViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestAddDopSPN()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.AddDopSPN = expected;
            actual = target.AddDopSPN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopSPNs()
        {
            DueViewModel target = new DueViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopSPNs = expected;
            actual = target.DopSPNs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopSPNsGrid()
        {
            DueViewModel target = new DueViewModel();
            List<DopSPNListItem> expected = null;
            List<DopSPNListItem> actual;
            target.DopSPNsGrid = expected;
            actual = target.DopSPNsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestExtLebID()
        {
            DueViewModel target = new DueViewModel();
            long expected = 1;
            long actual;
            target.ExtLebID = expected;
            actual = target.ExtLebID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestExtPfID()
        {
            DueViewModel target = new DueViewModel();
            long expected = 1;
            long actual;
            target.ExtPfID = expected;
            actual = target.ExtPfID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            DueViewModel target = new DueViewModel();
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestMonthsList()
        {
            DueViewModel target = new DueViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPaymentBoss()
        {
            DueViewModel target = new DueViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.PaymentBoss = expected;
            actual = target.PaymentBoss;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPaymentDSV()
        {
            DueViewModel target = new DueViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.PaymentDSV = expected;
            actual = target.PaymentDSV;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectAccount()
        {
            DueViewModel_Accessor target = new DueViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.SelectAccount = expected;
            actual = target.SelectAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            DueViewModel target = new DueViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedMonth = expected;
            actual = target.SelectedMonth;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSmartComment()
        {
            DueViewModel target = new DueViewModel();
            string expected = string.Empty;
            string actual;
            target.SmartComment = expected;
            actual = target.SmartComment;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueViewModel target = new DueViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            DueViewModel target = new DueViewModel();
            DUE_TYPE expected = DUE_TYPE.DSV;
            DUE_TYPE actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}
