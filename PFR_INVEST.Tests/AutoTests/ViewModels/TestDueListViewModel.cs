﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDueListViewModelConstructor()
        {
            DUE_TYPE type = DUE_TYPE.Normal;
            DueListViewModel target = new DueListViewModel(type);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            DUE_TYPE param0 = DUE_TYPE.Normal;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(param0);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            PrivateObject param0 = null;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            PrivateObject param0 = null;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestcalculateSupply()
        {
            DUE_TYPE type = new DUE_TYPE();
            DueListViewModel target = new DueListViewModel(DUE_TYPE.Normal);
            IEnumerable<DueListItem> inListItems = null;
            List<DueListItem> expected = null;
            List<DueListItem> actual;
            actual = target.calculateSupply(inListItems);
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestcorrectDBData()
        {
            PrivateObject param0 = null;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            IEnumerable<DueListItem> inListItems = null;
            List<DueListItem> expected = null;
            List<DueListItem> actual;
            actual = target.correctDBData(inListItems);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestdoubleTreasurityDoc()
        {
            PrivateObject param0 = null;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            List<DueListItem> listItems = new List<DueListItem>();
            List<DueListItem> expected = listItems;
            List<DueListItem> actual;
            actual = target.doubleTreasurityDoc(listItems);
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestpopulateSourcePortfolioItems()
        {
            DUE_TYPE type = new DUE_TYPE();
            DueListViewModel target = new DueListViewModel(DUE_TYPE.Normal);
            IEnumerable<DueListItem> inListItems = null;
            List<DueListItem> expected = null;
            List<DueListItem> actual;
            actual = target.populateSourcePortfolioItems(inListItems);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestprocessList()
        {
            DUE_TYPE type = new DUE_TYPE();
            DueListViewModel target = new DueListViewModel(DUE_TYPE.Normal);
            IEnumerable<DueListItem> inListItems = null;
            target.processList(inListItems);
        }

        [TestMethod()]
        public void TestList()
        {
            DUE_TYPE type = new DUE_TYPE();
            DueListViewModel target = new DueListViewModel(DUE_TYPE.Normal);
            List<DueListItem> expected = null;
            List<DueListItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            PrivateObject param0 = null;
            DueListViewModel_Accessor target = new DueListViewModel_Accessor(DUE_TYPE.Normal);
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            DUE_TYPE type = new DUE_TYPE();
            DueListViewModel target = new DueListViewModel(DUE_TYPE.Normal);
            DUE_TYPE expected = new DUE_TYPE();
            DUE_TYPE actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}
