﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestYieldsListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestYieldsListViewModelConstructor()
        {
            YieldsListViewModel target = new YieldsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            YieldsListViewModel_Accessor target = new YieldsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            YieldsListViewModel_Accessor target = new YieldsListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            YieldsListViewModel_Accessor target = new YieldsListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            YieldsListViewModel_Accessor target = new YieldsListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            YieldsListViewModel_Accessor target = new YieldsListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYieldsList()
        {
            YieldsListViewModel target = new YieldsListViewModel(); 
            List<YieldListItem> expected = null; 
            List<YieldListItem> actual;
            target.YieldsList = expected;
            actual = target.YieldsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
