﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestReorganizationViewModel : BaseTest
    {


        [TestMethod()]
        public void TestReorganizationViewModelConstructor()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ReorganizationViewModel_Accessor target = new ReorganizationViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadReorganization()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            long id = 1;
            target.LoadReorganization(id);
        }

        [TestMethod()]
        public void TestRefreshLists()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            target.RefreshLists();
        }

       

       

        [TestMethod()]
        public void TestContragentsList()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.ContragentsList = expected;
            actual = target.ContragentsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDestID()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            long expected = 0;
            long actual;
            target.DestID = expected;
            actual = target.DestID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDestName()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            string expected = string.Empty;
            string actual;
            target.DestName = expected;
            actual = target.DestName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestRegNum()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            string expected = string.Empty;
            string actual;
            target.RegNum = expected;
            actual = target.RegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReorganizationDate()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            DateTime expected = new DateTime();
            DateTime actual;
            target.ReorganizationDate = expected;
            actual = target.ReorganizationDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReorganizationTypesList()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            List<DB2ReorganizationType> expected = null;
            List<DB2ReorganizationType> actual;
            target.ReorganizationTypesList = expected;
            actual = target.ReorganizationTypesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContragent()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedContragent = expected;
            actual = target.SelectedContragent;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedReorganizationType()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            DB2ReorganizationType expected = null;
            DB2ReorganizationType actual;
            target.SelectedReorganizationType = expected;
            actual = target.SelectedReorganizationType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSourceID()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            long expected = 0;
            long actual;
            target.SourceID = expected;
            actual = target.SourceID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSourceName()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            string expected = string.Empty;
            string actual;
            target.SourceName = expected;
            actual = target.SourceName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTypeID()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            long expected = 0;
            long actual;
            target.TypeID = expected;
            actual = target.TypeID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTypeName()
        {
            ReorganizationViewModel target = new ReorganizationViewModel();
            string expected = string.Empty;
            string actual;
            target.TypeName = expected;
            actual = target.TypeName;
            Assert.AreEqual(expected, actual);
        }
    }
}
