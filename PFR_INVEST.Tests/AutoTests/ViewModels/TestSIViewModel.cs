﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections.Generic;
using PFR_INVEST.Proxy;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSIViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSIViewModelConstructor()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddBranch()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action);
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddBranch();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteDeleteBranch()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action);
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteDeleteBranch();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

       

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeleteBranch()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            target.ExecuteDeleteBranch();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    long id = 1;
        //    VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
        //    SIViewModel target = new SIViewModel(id, action); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            target.RefreshLists();
        }

        
       

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestisEmail()
        {
            string inputEmail = string.Empty; 
            bool expected = false; 
            bool actual;
            actual = SIViewModel_Accessor.isEmail(inputEmail);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAddBranch()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            ICommand actual;
            actual = target.AddBranch;
        }

        [TestMethod()]
        public void TestBranchesList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            List<BranchItem> expected = null; 
            List<BranchItem> actual;
            target.BranchesList = expected;
            actual = target.BranchesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContactsList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            List<Contact> expected = null; 
            List<Contact> actual;
            target.ContactsList = expected;
            actual = target.ContactsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContractsList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            List<Contract> expected = null; 
            List<Contract> actual;
            target.ContractsList = expected;
            actual = target.ContractsList;
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //public void TestContragentsList()
        //{
        //    long id = 1; 
        //    VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
        //    SIViewModel target = new SIViewModel(id, action); 
        //    List<DBEntity> expected = null; 
        //    List<DBEntity> actual;
        //    target.ContragentsList = expected;
        //    actual = target.ContragentsList;
        //    Assert.AreEqual(expected, actual);
        //}

        [TestMethod()]
        public void TestDeleteBranch()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            ICommand actual;
            actual = target.DeleteBranch;
        }

        [TestMethod()]
        public void TestItem()
        {
            long id = 0; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            string columnName = string.Empty; 
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestLStatusesList()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            List<LegalStatus> expected = null;
            List<LegalStatus> actual;
            target.LStatusesList = expected;
            actual = target.LStatusesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOldSINames()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            List<OldSIItem> expected = null; 
            List<OldSIItem> actual;
            target.OldSINames = expected;
            actual = target.OldSINames;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContragent()
        {
            //long id = 1; 
            //VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            //SIViewModel target = new SIViewModel(id, action); 
            //Contragent expected = null;
            //Contragent actual;
            //target.SelectedContragent = expected;
            //actual = target.SelectedContragent;
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedItem()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action); 
            BranchItem expected = null; 
            BranchItem actual;
            target.SelectedItem = expected;
            actual = target.SelectedItem;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedLStatus()
        {
            long id = 1; 
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            SIViewModel target = new SIViewModel(id, action);
            LegalStatus expected = null;
            LegalStatus actual;
            target.SelectedLStatus = expected;
            actual = target.SelectedLStatus;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedStatus()
        {
            //long id = 1; 
            //VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            //SIViewModel target = new SIViewModel(id, action); 
            //DBEntity expected = null; 
            //DBEntity actual;
            //target.SelectedStatus = expected;
            //actual = target.SelectedStatus;
            //Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //public void TestStatusesList()
        //{
        //    long id = 1; 
        //    VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
        //    SIViewModel target = new SIViewModel(id, action); 
        //    List<DBEntity> expected = null; 
        //    List<DBEntity> actual;
        //    target.StatusesList = expected;
        //    actual = target.StatusesList;
        //    Assert.AreEqual(expected, actual);
        //}
    }
}
