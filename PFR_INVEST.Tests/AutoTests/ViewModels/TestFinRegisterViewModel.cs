﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Windows;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestFinRegisterViewModel : BaseTest
    {



        [TestMethod()]
        public void TestFinRegisterViewModelConstructor()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshFinRegisterCorrs()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshFinRegisterCorrs();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshTranches()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshTranches();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestExecuteRefreshFinRegisterCorrs()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            long recordID1 = 1;
            target.ExecuteRefreshFinRegisterCorrs(recordID1);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshTranches()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            target.ExecuteRefreshTranches(recordID);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    FinRegisterViewModel_Accessor target = new FinRegisterViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestfillAccounts()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            target.fillAccounts();
        }

        [TestMethod()]
        public void TestFinRegisterCorrs()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            FinRegisterCorrListItem[] expected = null;
            FinRegisterCorrListItem[] actual;
            target.FinRegisterCorrs = expected;
            actual = target.FinRegisterCorrs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinRegisterCorrsVisibility()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            Visibility expected = new Visibility();
            Visibility actual;
            target.FinRegisterCorrsVisibility = expected;
            actual = target.FinRegisterCorrsVisibility;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestFinRegisterID()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            long actual;
            actual = target.FinRegisterID;
        }

    

        [TestMethod()]
        public void TestNPFList()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshFinRegisterCorrs()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            ICommand expected = null;
            ICommand actual;
            
            actual = target.RefreshFinRegisterCorrs;
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshTranches()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            
            ICommand actual;
            
            actual = target.RefreshTranches;
            
        }

        [TestMethod()]
        public void TestRegisterID()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            long actual;
            actual = target.RegisterID;
        }

        [TestMethod()]
        public void TestSPNSum()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            long expected = 0;
            long actual;
            target.SPNSum = expected;
            actual = target.SPNSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedNPF()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            DB2LegalEntityCard expected = new DB2LegalEntityCard();
            DB2LegalEntityCard actual;
            target.SelectedNPF = expected;
            actual = target.SelectedNPF;
         //   Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTranches()
        {
            long recordID = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            FinRegisterViewModel target = new FinRegisterViewModel(recordID, action);
            TrancheView[] expected = null;
            TrancheView[] actual;
            target.Tranches = expected;
            actual = target.Tranches;
            Assert.AreEqual(expected, actual);
        }
    }
}
