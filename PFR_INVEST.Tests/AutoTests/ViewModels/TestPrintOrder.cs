﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrintOrder : BaseTest
    {

        [TestMethod()]
        public void TestPrintOrderConstructor()
        {
            long or_id = 1;
            Decimal _fixedPercent = 15;
            PrintOrder target = new PrintOrder(or_id, _fixedPercent);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestgenerateDoc()
        {
            long or_id = 1;
            Decimal _fixedPercent = 15;
            PrintOrder target = new PrintOrder(or_id, _fixedPercent);
            string fName = string.Empty; 
            target.generateDoc(fName);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestgetTemplateName()
        {
            long or_id = 1;
            Decimal _fixedPercent = 15;
            PrintOrder target = new PrintOrder(or_id, _fixedPercent);
            string expected = PrintOrder.wtBuyCB;
            string actual;
            actual = target.getTemplateName();
            Assert.AreEqual(expected, actual);
        }


        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestFixedPercent()
        //{
        //    PrivateObject param0 = null; 
        //    PrintOrder_Accessor target = new PrintOrder_Accessor(param0); 
        //    Decimal expected = new Decimal(); 
        //    Decimal actual;
        //    target.FixedPercent = expected;
        //    actual = target.FixedPercent;
        //    Assert.AreEqual(expected, actual);
        //}
    }
}
