﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTransferRequestViewModel : BaseTest
    {

        [TestMethod()]
        public void TestTransferRequestViewModelConstructor()
        {
            TransferRequestViewModel target = new TransferRequestViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshRegistersList()
        {
            TransferRequestViewModel_Accessor target = new TransferRequestViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteRefreshRegistersList();
            Assert.AreEqual(expected, actual);
        }

        

       
        [TestMethod()]
        public void TestDate()
        {
            TransferRequestViewModel target = new TransferRequestViewModel(); 
            DateTime expected = new DateTime(); 
            DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestRegistersList()
        {
            TransferRequestViewModel target = new TransferRequestViewModel(); 
            List<TransferRequestListItem> expected = null; 
            List<TransferRequestListItem> actual;
            target.RegistersList = expected;
            actual = target.RegistersList;
            Assert.AreEqual(expected, actual);
        }
    }
}
