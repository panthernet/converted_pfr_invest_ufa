﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueUndistributedViewModel : BaseTest
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void TestDueUndistributedViewModelConstructor()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopAPS()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddDopAPS();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectDstAccount()
        {
            DueUndistributedViewModel_Accessor target = new DueUndistributedViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectDstAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectSrcAccount()
        {
            DueUndistributedViewModel_Accessor target = new DueUndistributedViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectSrcAccount();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DueUndistributedViewModel_Accessor target = new DueUndistributedViewModel_Accessor();
            target.ExecuteSave();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSelectDstAccount()
        //{
        //    DueUndistributedViewModel_Accessor target = new DueUndistributedViewModel_Accessor();
        //    target.ExecuteSelectDstAccount();
        //}


        [TestMethod()]
        public void TestLoad()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            long lId = 0;
            target.Load(lId);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestValidate()
        //{
        //    DueUndistributedViewModel_Accessor target = new DueUndistributedViewModel_Accessor();
        //    string expected = string.Empty;
        //    string actual;
        //    actual = target.Validate();
        //    Assert.AreEqual(expected, actual);
        //}

        [TestMethod()]
        public void TestAddDopAPS()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            ICommand actual;
            actual = target.AddDopAPS;
        }

        [TestMethod()]
        public void TestDopAPSs()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopAPSs = expected;
            actual = target.DopAPSs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopAPSsGrid()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            List<PrepaymentAccurateListItem> expected = null;
            List<PrepaymentAccurateListItem> actual;
            target.DopAPSsGrid = expected;
            actual = target.DopAPSsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestItem()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            string columnName = string.Empty;
            string actual;
            actual = target[columnName];
        }

        [TestMethod()]
        public void TestSelectDstAccount()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            ICommand actual;
            actual = target.SelectDstAccount;
        }

        [TestMethod()]
        public void TestSelectSrcAccount()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            ICommand actual;
            actual = target.SelectSrcAccount;
        }

        [TestMethod()]
        public void TestSrcAccount()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueUndistributedViewModel target = new DueUndistributedViewModel();
            Decimal expected = new Decimal();
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
