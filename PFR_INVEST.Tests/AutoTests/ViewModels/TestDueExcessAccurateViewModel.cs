﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueExcessAccurateViewModel : BaseTest
    {

        [TestMethod()]
        public void TestDueExcessAccurateViewModelConstructor()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DueExcessAccurateViewModel_Accessor target = new DueExcessAccurateViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestLoad()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            long lId = 1;
            target.Load(lId);
        }

        
        [TestMethod()]
        public void TestChSumm()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            Decimal expected = 12;
            Decimal actual;
            target.ChSumm = expected;
            actual = target.ChSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestSrcAccount()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueExcessViewModel parentVM = new DueExcessViewModel();
            parentVM.Load(1);
            DueExcessAccurateViewModel target = new DueExcessAccurateViewModel(parentVM);
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
