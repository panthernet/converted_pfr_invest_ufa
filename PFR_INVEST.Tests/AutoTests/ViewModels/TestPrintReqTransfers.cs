﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrintReqTransfers : BaseTest
    {


        [TestMethod()]
        public void TestPrintReqTransfersConstructor()
        {
            long _reg_id = 1; 
            string _addressType = string.Empty; 
            PrintReqTransfers target = new PrintReqTransfers(_reg_id, _addressType);
        }

        
       

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestupdateTransferStatus()
        {
            long _reg_id = 1;
            string _addressType = string.Empty;
            PrintReqTransfers target = new PrintReqTransfers(_reg_id, _addressType); 
            target.updateTransfersStatus();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestAddressType()
        {
            long _reg_id = 1;
            string _addressType = string.Empty;
            PrintReqTransfers target = new PrintReqTransfers(_reg_id, _addressType); 
            string expected = string.Empty; 
            string actual;
            target.AddressType = expected;
            actual = target.AddressType;
            Assert.AreEqual(expected, actual);
        }
    }
}
