﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestViolationCategoryViewModel : BaseTest
    {


        [TestMethod()]
        public void TestViolationCategoryViewModelConstructor()
        {
            ViolationCategoryViewModel target = new ViolationCategoryViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ViolationCategoryViewModel target = new ViolationCategoryViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ViolationCategoryViewModel_Accessor target = new ViolationCategoryViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            ViolationCategoryViewModel target = new ViolationCategoryViewModel(); 
            long lId = 1; 
            target.Load(lId);
        }

      

        [TestMethod()]
        public void TestViolationCharCode()
        {
            ViolationCategoryViewModel target = new ViolationCategoryViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.ViolationCharCode = expected;
            actual = target.ViolationCharCode;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestViolationTypeCode()
        {
            ViolationCategoryViewModel target = new ViolationCategoryViewModel(); 
            string expected = string.Empty; 
            string actual;
            target.ViolationTypeCode = expected;
            actual = target.ViolationTypeCode;
            Assert.AreEqual(expected, actual);
        }
    }
}
