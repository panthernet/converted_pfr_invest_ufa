﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDraftViewModel : BaseTest
    {


        [TestMethod()]
        public void TestDraftViewModelConstructor()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DraftViewModel_Accessor target = new DraftViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestLoadDraft()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            long _id = 1;
            target.LoadDraft(_id);
        }

        [TestMethod()]
        public void TestcopyDic()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            Dictionary<string, DBField> din = null;
            Dictionary<string, string> expected = null;
            Dictionary<string, string> actual;
            actual = target.copyDic(din);
            //Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            string actual;
            actual = target.INN;
        }


        [TestMethod()]
        public void TestNPFList()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedNPF()
        {
            long in_fr_id = 1;
            DraftViewModel target = new DraftViewModel(in_fr_id);
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedNPF = expected;
            actual = target.SelectedNPF;
            //Assert.AreEqual(expected, actual);
        }
    }
}
