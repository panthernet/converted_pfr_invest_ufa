﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF50ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF50ViewModelConstructor()
        {
            long id = 1;
            F50ViewModel target = new F50ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F50ViewModel target = new F50ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F50ViewModel target = new F50ViewModel(id);
            target.RefreshLists();
        }



        [TestMethod()]
        public void TestCBInfoList()
        {
            long id = 1;
            F50ViewModel target = new F50ViewModel(id);
            List<F50CBInfoListItem> expected = null;
            List<F50CBInfoListItem> actual;
            target.CBInfoList = expected;
            actual = target.CBInfoList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestInfoList()
        {
            long id = 1;
            F50ViewModel target = new F50ViewModel(id);
            List<F50InfoListItem> expected = null;
            List<F50InfoListItem> actual;
            target.InfoList = expected;
            actual = target.InfoList;
            Assert.AreEqual(expected, actual);
        }


    }
}
