﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPaymentItem : BaseTest
    {

        [TestMethod()]
        public void TestPaymentItemConstructor()
        {
            DBEntity ent = null; 
            long n = 1; 
            PaymentItem target = new PaymentItem(ent, n);
        }

        [TestMethod()]
        public void TestCoupon()
        {
            DBEntity ent = null; 
            long n = 1; 
            PaymentItem target = new PaymentItem(ent, n); 
            Decimal actual;
            actual = target.Coupon;
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            DateTime actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestEntity()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            DBEntity actual;
            actual = target.Entity;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestNominal()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            Decimal actual;
            actual = target.Nominal;
        }

        [TestMethod()]
        public void TestNum()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            string actual;
            actual = target.Num;
        }

        [TestMethod()]
        public void TestPaymentBond()
        {
            DBEntity ent = null; 
            long n = 0; 
            PaymentItem target = new PaymentItem(ent, n); 
            Decimal actual;
            actual = target.PaymentBond;
        }
    }
}
