﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestArchiveDepositsListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestDepositsListViewModelConstructor()
        {
            ArchiveDepositsListViewModel target = new ArchiveDepositsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            ArchiveDepositsListViewModel_Accessor target = new ArchiveDepositsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            ArchiveDepositsListViewModel_Accessor target = new ArchiveDepositsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            ArchiveDepositsListViewModel_Accessor target = new ArchiveDepositsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            ArchiveDepositsListViewModel_Accessor target = new ArchiveDepositsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestDepositsList()
        {
            ArchiveDepositsListViewModel target = new ArchiveDepositsListViewModel();
            List<DepositListItem> expected = null;
            List<DepositListItem> actual;
            target.DepositsList = expected;
            actual = target.DepositsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            ArchiveDepositsListViewModel target = new ArchiveDepositsListViewModel();
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
