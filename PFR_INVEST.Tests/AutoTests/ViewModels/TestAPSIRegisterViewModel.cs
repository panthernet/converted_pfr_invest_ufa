﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestAPSIRegisterViewModel : BaseTest
    {
        [TestMethod()]
        public void TestRemainder()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            Decimal expected = 15;
            Decimal actual;
            target.Remainder = expected;
            actual = target.Remainder;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDocTypes()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            List<string> expected = null;
            List<string> actual;
            target.DocTypes = expected;
            actual = target.DocTypes;
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    APSIRegisterViewModel_Accessor target = new APSIRegisterViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedDocType()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            string expected = "test string";
            string actual;
            target.SelectedDocType = expected;
            actual = target.SelectedDocType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedYear()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            DBEntity expected = new DBEntity(new Dictionary<string,DBField>());
            expected.Fields.Add("ID", new DBField("ID","DB","PFR","TBL",FIELD_TYPE.PrimaryKey,DATA_TYPE.Long,1));
            expected.Fields["ID"].Value = 555;
            DBEntity actual;
            target.SelectedYear = expected;
            actual = target.SelectedYear;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalInvestDohod()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            Decimal expected = 64;
            Decimal actual;
            target.TotalInvestDohod = expected;
            actual = target.TotalInvestDohod;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalZLSumm()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            Decimal expected = 64;
            Decimal actual;
            target.TotalZLSumm = expected;
            actual = target.TotalZLSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKTotalSumm()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            Decimal expected = 64;
            Decimal actual;
            target.UKTotalSumm = expected;
            actual = target.UKTotalSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYearsList()
        {
            long id = 0;
            VIEWMODEL_STATES action = new VIEWMODEL_STATES();
            APSIRegisterViewModel target = new APSIRegisterViewModel(id, action);
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.YearsList = expected;
            actual = target.YearsList;
            Assert.AreEqual(expected, actual);
        }
    }
}
