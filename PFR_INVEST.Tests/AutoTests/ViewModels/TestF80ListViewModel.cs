﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF80ListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF80ListViewModelConstructor()
        {
            F80ListViewModel target = new F80ListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            F80ListViewModel_Accessor target = new F80ListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            F80ListViewModel_Accessor target = new F80ListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            F80ListViewModel_Accessor target = new F80ListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            F80ListViewModel_Accessor target = new F80ListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestF80List()
        {
            F80ListViewModel target = new F80ListViewModel(); 
            List<F80ListItem> expected = null; 
            List<F80ListItem> actual;
            target.F80List = expected;
            actual = target.F80List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            F80ListViewModel_Accessor target = new F80ListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
