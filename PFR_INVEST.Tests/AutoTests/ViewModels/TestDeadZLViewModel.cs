﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDeadZLViewModel : BaseTest
    {

        [TestMethod()]
        public void TestDeadZLViewModelConstructor()
        {
            DeadZLViewModel target = new DeadZLViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DeadZLViewModel target = new DeadZLViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DeadZLViewModel_Accessor target = new DeadZLViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            DeadZLViewModel target = new DeadZLViewModel();
            long id = 1;
            target.Load(id);
        }

        [TestMethod()]
        public void TestRefreshLists()
        {
            DeadZLViewModel target = new DeadZLViewModel();
            target.RefreshLists();
        }


        [TestMethod()]
        public void TestContragentsList()
        {
            DeadZLViewModel target = new DeadZLViewModel();
            List<ContragentListItem> expected = null;
            List<ContragentListItem> actual;
            target.ContragentsList = expected;
            actual = target.ContragentsList;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestNPFDisplayName()
        {
            DeadZLViewModel target = new DeadZLViewModel();
            string expected = string.Empty;
            string actual;
            target.NPFDisplayName = expected;
            actual = target.NPFDisplayName;
            Assert.AreEqual(expected, actual);
        }
    }
}
