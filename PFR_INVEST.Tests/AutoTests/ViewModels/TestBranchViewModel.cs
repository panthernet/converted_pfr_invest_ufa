﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBranchViewModel : BaseTest
    {

        [TestMethod()]
        public void TestBranchViewModelConstructor()
        {
            ViewModelCard vm = null;
            long branchID = 1;
            BranchViewModel target = new BranchViewModel(vm, branchID);
        }

        [TestMethod()]
        public void TestBranchViewModelConstructor1()
        {
            ViewModelCard vm = null;
            BranchViewModel target = new BranchViewModel(vm);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ViewModelCard vm = null;
            BranchViewModel target = new BranchViewModel(vm);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    BranchViewModel_Accessor target = new BranchViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}


        [TestMethod()]
        public void TestBranchAddress()
        {
            ViewModelCard vm = null;
            BranchViewModel target = new BranchViewModel(vm);
            string expected = "test string";
            string actual;
            target.BranchAddress = expected;
            actual = target.BranchAddress;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBranchName()
        {
            ViewModelCard vm = null;
            BranchViewModel target = new BranchViewModel(vm);
            string expected = "test string";
            string actual;
            target.BranchName = expected;
            actual = target.BranchName;
            Assert.AreEqual(expected, actual);
        }

       
    }
}
