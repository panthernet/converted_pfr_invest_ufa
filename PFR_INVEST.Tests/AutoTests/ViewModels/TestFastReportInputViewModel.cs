﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestFastReportInputViewModel : BaseTest
    {

        [TestMethod()]
        public void TestFastReportInputViewModelConstructor()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestaddNewRow()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            target.addNewRow();
        }

        [TestMethod()]
        public void TestgetSecurities()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            
            List<Issue> actual;
            actual = target.getSecurities();
            
        }

        

        
        [TestMethod()]
        public void TestAccountDepo()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            string expected = string.Empty;
            string actual;
            target.AccountDepo = expected;
            actual = target.AccountDepo;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBank()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            string expected = string.Empty;
            string actual;
            target.Bank = expected;
            actual = target.Bank;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDefaultIssue()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            Issue expected = null;
            Issue actual;
            target.DefaultIssue = expected;
            actual = target.DefaultIssue;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestItems()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            BindingList<FastReportItem> expected = null;
            BindingList<FastReportItem> actual;
            target.Items = expected;
            actual = target.Items;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            long orId = 1;
            FastReportInputViewModel target = new FastReportInputViewModel(orId);
            string expected = string.Empty;
            string actual;
            target.Portfolio = expected;
            actual = target.Portfolio;
            Assert.AreEqual(expected, actual);
        }
    }
}
