﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestAccountViewModel : BaseTest
    {




        [TestMethod()]
        public void TestAccountViewModelConstructor()
        {
            AccountViewModel target = new AccountViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            AccountViewModel target = new AccountViewModel(); 
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            AccountViewModel_Accessor target = new AccountViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadAccount()
        {
            AccountViewModel target = new AccountViewModel(); 
            long id = 1; 
            target.LoadAccount(id);
        }

        [TestMethod()]
        public void TestRefreshAccountKindsList()
        {
            AccountViewModel target = new AccountViewModel(); 
            target.RefreshAccountKindsList();
        }

        [TestMethod()]
        public void TestRefreshContragentsList()
        {
            AccountViewModel target = new AccountViewModel(); 
            target.RefreshContragentsList();
        }

        [TestMethod()]
        public void TestAccountKindsList()
        {
            AccountViewModel target = new AccountViewModel(); 
            List<DB2AccountKind> expected = new List<DB2AccountKind>(); 
            List<DB2AccountKind> actual;
            target.AccountKindsList = expected;
            actual = target.AccountKindsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAccountTypeID()
        {
            AccountViewModel target = new AccountViewModel(); 
            long expected = 1; 
            long actual;
            target.AccountTypeID = expected;
            actual = target.AccountTypeID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentID()
        {
            AccountViewModel target = new AccountViewModel(); 
            long expected = 1; 
            long actual;
            target.ContragentID = expected;
            actual = target.ContragentID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentsList()
        {
            AccountViewModel target = new AccountViewModel(); 
            List<DBEntity> expected = new List<DBEntity>();
            List<DBEntity> actual;
            target.ContragentsList = expected;
            actual = target.ContragentsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            AccountViewModel target = new AccountViewModel(); 
            long expected = 1; 
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestName()
        {
            AccountViewModel target = new AccountViewModel(); 
            string expected = "test string"; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedAccountKind()
        {
            AccountViewModel target = new AccountViewModel(); 

            DB2AccountKind expected = null; 
            DB2AccountKind actual;
            target.SelectedAccountKind = expected;
            actual = target.SelectedAccountKind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedContragent()
        {
            AccountViewModel target = new AccountViewModel(); 
            DBEntity expected = null; 
            DBEntity actual;
            target.SelectedContragent = expected;
            actual = target.SelectedContragent;
            Assert.AreEqual(expected, actual);
        }
    }
}
