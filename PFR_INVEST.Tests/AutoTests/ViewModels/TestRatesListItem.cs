﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRatesListItem : BaseTest
    {


        [TestMethod()]
        public void TestRatesListItemConstructor()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
        }

        [TestMethod()]
        public void TestCurrency()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDate()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
            DateTime actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestRate()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
            Decimal actual;
            actual = target.Rate;
        }

        [TestMethod()]
        public void TestYear()
        {
            DBEntity ent = null;
            RatesListItem target = new RatesListItem(ent);
            string actual;
            actual = target.Year;
        }
    }
}
