﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSelectOrgModel : BaseTest
    {

        [TestMethod()]
        public void TestSelectOrgModelConstructor()
        {
            SelectOrgModel target = new SelectOrgModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            SelectOrgModel target = new SelectOrgModel();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            SelectOrgModel_Accessor target = new SelectOrgModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestRefreshLists()
        {
            SelectOrgModel target = new SelectOrgModel(); 
            target.RefreshLists();
        }

    

        [TestMethod()]
        public void TestINN()
        {
            SelectOrgModel target = new SelectOrgModel(); 
            string expected = string.Empty; 
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestNPFList()
        {
            SelectOrgModel target = new SelectOrgModel(); 
            List<DB2LegalEntityCard> expected = null; 
            List<DB2LegalEntityCard> actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOrgs()
        {
            SelectOrgModel target = new SelectOrgModel(); 
            Org[] expected = null; 
            Org[] actual;
            target.Orgs = expected;
            actual = target.Orgs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedNPF()
        {
            SelectOrgModel target = new SelectOrgModel(); 
            DB2LegalEntityCard expected = null; 
            DB2LegalEntityCard actual;
            target.SelectedNPF = expected;
            actual = target.SelectedNPF;
            Assert.AreEqual(expected, actual);
        }
    }
}
