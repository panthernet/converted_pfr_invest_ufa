﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDueExcessViewModel : BaseTest
    {

        [TestMethod()]
        public void TestDueExcessViewModelConstructor()
        {
            DueExcessViewModel target = new DueExcessViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddDopAPS()
        {
            DueExcessViewModel_Accessor target = new DueExcessViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteAddDopAPS();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectDstAccount()
        {
            DueExcessViewModel_Accessor target = new DueExcessViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectDstAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectSrcAccount()
        {
            DueExcessViewModel_Accessor target = new DueExcessViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectSrcAccount();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            DueExcessViewModel_Accessor target = new DueExcessViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            long lId = 1;
            target.Load(lId);
        }

        [TestMethod()]
        public void TestAddDopAPS()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            ICommand actual;
            actual = target.AddDopAPS;
        }

        [TestMethod()]
        public void TestDopAPSs()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DopAPSs = expected;
            actual = target.DopAPSs;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDopAPSsGrid()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            List<PrepaymentAccurateListItem> expected = null;
            List<PrepaymentAccurateListItem> actual;
            target.DopAPSsGrid = expected;
            actual = target.DopAPSsGrid;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDstAccount()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.DstAccount = expected;
            actual = target.DstAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectDstAccount()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            ICommand actual;
            actual = target.SelectDstAccount;
        }

        [TestMethod()]
        public void TestSelectSrcAccount()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            ICommand actual;
            actual = target.SelectSrcAccount;
        }

        [TestMethod()]
        public void TestSrcAccount()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.SrcAccount = expected;
            actual = target.SrcAccount;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            DueExcessViewModel target = new DueExcessViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }
    }
}
