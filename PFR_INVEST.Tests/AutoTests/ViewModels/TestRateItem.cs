﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestRateItem : BaseTest
    {


        [TestMethod()]
        public void TestRateItemConstructor()
        {
            RateItem target = new RateItem();
        }

        [TestMethod()]
        public void TestDate()
        {
            RateItem target = new RateItem();
            DateTime expected = new DateTime();
            DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestEUR()
        {
            RateItem target = new RateItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.EUR = expected;
            actual = target.EUR;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUSD()
        {
            RateItem target = new RateItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.USD = expected;
            actual = target.USD;
            Assert.AreEqual(expected, actual);
        }
    }
}
