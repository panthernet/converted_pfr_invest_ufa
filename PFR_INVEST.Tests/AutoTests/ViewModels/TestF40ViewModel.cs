﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF40ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF40ViewModelConstructor()
        {
            long id = 1;
            F40ViewModel target = new F40ViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            F40ViewModel target = new F40ViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    F40ViewModel_Accessor target = new F40ViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshLists()
        {
            long id = 1;
            F40ViewModel target = new F40ViewModel(id);
            target.RefreshLists();
        }

        [TestMethod()]
        public void TestInfoList()
        {
            long id = 1;
            F40ViewModel target = new F40ViewModel(id);
            List<F40InfoListItem> expected = null;
            List<F40InfoListItem> actual;
            target.InfoList = expected;
            actual = target.InfoList;
            Assert.AreEqual(expected, actual);
        }

       
    }
}
