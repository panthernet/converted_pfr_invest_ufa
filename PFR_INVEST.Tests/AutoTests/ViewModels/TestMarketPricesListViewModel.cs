﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestMarketPricesListViewModel : BaseTest
    {

        [TestMethod()]
        public void TestMarketPricesListViewModelConstructor()
        {
            MarketPricesListViewModel target = new MarketPricesListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            MarketPricesListViewModel_Accessor target = new MarketPricesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            MarketPricesListViewModel_Accessor target = new MarketPricesListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            MarketPricesListViewModel_Accessor target = new MarketPricesListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            MarketPricesListViewModel_Accessor target = new MarketPricesListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestList()
        {
            MarketPricesListViewModel target = new MarketPricesListViewModel();
            List<MarketPriceListItem> expected = null;
            List<MarketPriceListItem> actual;
            target.List = expected;
            actual = target.List;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            MarketPricesListViewModel_Accessor target = new MarketPricesListViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }
    }
}
