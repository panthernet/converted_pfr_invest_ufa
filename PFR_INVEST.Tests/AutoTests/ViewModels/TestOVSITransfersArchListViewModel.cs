﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections.Generic;

using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.ViewModels;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestOVSITransfersArchListViewModel : BaseTest
    {
        [TestMethod()]
        public void TestSelectedRegID()
        {
            OVSITransfersArchListViewModel target = new OVSITransfersArchListViewModel(); 
            long expected = 0; 
            long actual;
            target.SelectedRegisterID = expected;
            actual = target.SelectedRegisterID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefresh()
        {
            OVSITransfersArchListViewModel_Accessor target = new OVSITransfersArchListViewModel_Accessor(); 
            ICommand expected = null; 
            ICommand actual;
            target.Refresh = expected;
            actual = target.Refresh;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestList()
        {
            OVSITransfersArchListViewModel target = new OVSITransfersArchListViewModel(); 
            List<SITransferListItem> expected = null; 
            List<SITransferListItem> actual;
            target.SITransferArchiveList = expected;
            actual = target.SITransferArchiveList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            OVSITransfersArchListViewModel_Accessor target = new OVSITransfersArchListViewModel_Accessor(); 
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            OVSITransfersArchListViewModel_Accessor target = new OVSITransfersArchListViewModel_Accessor(); 
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            OVSITransfersArchListViewModel_Accessor target = new OVSITransfersArchListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            OVSITransfersArchListViewModel_Accessor target = new OVSITransfersArchListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOVSITransfersArchListViewModelConstructor()
        {
            OVSITransfersArchListViewModel target = new OVSITransfersArchListViewModel();
        }
    }
}
