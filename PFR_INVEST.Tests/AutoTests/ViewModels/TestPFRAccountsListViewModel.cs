﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPFRAccountsListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPFRAccountsListViewModelConstructor()
        {
            PFRAccountsListViewModel target = new PFRAccountsListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefresh()
        {
            PFRAccountsListViewModel_Accessor target = new PFRAccountsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefresh();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            PFRAccountsListViewModel_Accessor target = new PFRAccountsListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefresh()
        {
            PFRAccountsListViewModel_Accessor target = new PFRAccountsListViewModel_Accessor();
            target.ExecuteRefresh();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            PFRAccountsListViewModel_Accessor target = new PFRAccountsListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        public void TestPFRAccountsList()
        {
            PFRAccountsListViewModel target = new PFRAccountsListViewModel();
            List<PFRAccountsListItem> expected = null;
            List<PFRAccountsListItem> actual;
            target.PFRAccountsList = expected;
            actual = target.PFRAccountsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefresh()
        {
            PFRAccountsListViewModel target = new PFRAccountsListViewModel();
            ICommand actual;
            actual = target.Refresh;
        }
    }
}
