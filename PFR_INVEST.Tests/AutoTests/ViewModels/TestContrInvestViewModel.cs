﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestContrInvestViewModel : BaseTest
    {


        [TestMethod()]
        public void TestContrInvestViewModelConstructor()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestAddItemToList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            string tableName = string.Empty;
            Dictionary<string, DBField> strct = null;
            List<ContrInvestChildItem> list = null;
            target.AddItemToList(tableName, strct, list);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCreateChildLists()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            target.CreateChildLists();
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    ContrInvestViewModel_Accessor target = new ContrInvestViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestGenerateInsertQuery()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            Dictionary<string, DBField> flds = null;
            string expected = string.Empty;
            string actual;
            actual = target.GenerateInsertQuery(flds);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshParentLists()
        {
            Type viewModelList = null;
            ContrInvestViewModel.RefreshParentLists(viewModelList);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestToArray()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            List<ContrInvestChildItem> list = null;
            DBEntity[] expected = new DBEntity[0];
            DBEntity[] actual;
            actual = target.ToArray(list);
            ///Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestAkcList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            List<ContrInvestChildItem> expected = null;
            List<ContrInvestChildItem> actual;
            target.AkcList = expected;
            actual = target.AkcList;
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        public void TestOblList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            List<ContrInvestChildItem> expected = null;
            List<ContrInvestChildItem> actual;
            target.OblList = expected;
            actual = target.OblList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPayList()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            List<ContrInvestChildItem> expected = null;
            List<ContrInvestChildItem> actual;
            target.PayList = expected;
            actual = target.PayList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKFormalName()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            string actual;
            actual = target.UKFormalName;
        }

        [TestMethod()]
        public void TestUKFullName()
        {
            long id = 1;
            VIEWMODEL_STATES action = VIEWMODEL_STATES.Edit;
            ContrInvestViewModel target = new ContrInvestViewModel(id, action);
            string actual;
            actual = target.UKFullName;
        }
    }
}
