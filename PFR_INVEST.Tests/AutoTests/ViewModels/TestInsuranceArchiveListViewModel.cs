﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestInsuranceArchiveListViewModel : BaseTest
    {



        [TestMethod()]
        public void TestInsuranceArchiveListViewModelConstructor()
        {
            PFR_INVEST.ViewModels.InsuranceArchiveListViewModel target = new PFR_INVEST.ViewModels.InsuranceArchiveListViewModel();
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            PFR_INVEST.ViewModels.InsuranceArchiveListViewModel_Accessor target = new PFR_INVEST.ViewModels.InsuranceArchiveListViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            PFR_INVEST.ViewModels.InsuranceArchiveListViewModel_Accessor target = new PFR_INVEST.ViewModels.InsuranceArchiveListViewModel_Accessor(); 
            target.ExecuteRefreshList();
           
        }

        [TestMethod()]
        public void TestInsuranceList()
        {
            PFR_INVEST.ViewModels.InsuranceArchiveListViewModel target = new PFR_INVEST.ViewModels.InsuranceArchiveListViewModel(); 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.ListItems.InsuranceListItem> expected = null; 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.ListItems.InsuranceListItem> actual;
            target.InsuranceList = expected;
            actual = target.InsuranceList;
            Assert.AreEqual(expected, actual);
            
        }
    }
}
