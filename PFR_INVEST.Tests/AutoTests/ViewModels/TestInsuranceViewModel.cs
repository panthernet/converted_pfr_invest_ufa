﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestInsuranceViewModel : BaseTest
    {
        [TestMethod()]
        public void TestInsuranceViewModelConstructor()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel();
            
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel_Accessor target = new PFR_INVEST.ViewModels.InsuranceViewModel_Accessor(); 
            target.ExecuteSave();
            
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestInsuranceItem_OnItemPropertyChanged()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel_Accessor target = new PFR_INVEST.ViewModels.InsuranceViewModel_Accessor(); 
            string propertyName = string.Empty; 
            target.InsuranceItem_OnItemPropertyChanged(propertyName);
            
        }

        [TestMethod()]
        public void TestLoad()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel();
            long lId = 1;
            target.Load(lId);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestValidate()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel_Accessor target = new PFR_INVEST.ViewModels.InsuranceViewModel_Accessor(); 
            bool expected = false; 
            bool actual;
            actual = target.Validate();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContract()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            PFR_INVEST.Proxy.DBEntity expected = null; 
            PFR_INVEST.Proxy.DBEntity actual;
            target.Contract = expected;
            actual = target.Contract;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContracts()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            PFR_INVEST.Proxy.DBEntity[] actual;
            actual = target.Contracts;
        }

        [TestMethod()]
        public void TestInsuranceItem()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem expected = null; 
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem actual;
            target.InsuranceItem = expected;
            actual = target.InsuranceItem;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestUK()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            PFR_INVEST.ViewModels.UKItem expected = null; 
            PFR_INVEST.ViewModels.UKItem actual;
            target.UK = expected;
            actual = target.UK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUKs()
        {
            PFR_INVEST.ViewModels.InsuranceViewModel target = new PFR_INVEST.ViewModels.InsuranceViewModel(); 
            System.Collections.Generic.List<PFR_INVEST.ViewModels.UKItem> actual;
            actual = target.UKs;
        }
    }
}
