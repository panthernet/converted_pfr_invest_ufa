﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ActiveDirectory;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestUserViewModel : BaseTest
    {



        [TestMethod()]
        public void TestUserViewModelConstructor()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null; 
        //    UserViewModel_Accessor target = new UserViewModel_Accessor(param0); 
        //    target.ExecuteSave();
        //}

        [TestMethod()]
        public void TestAddedRoles()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User); 
            List<Role> expected = null; 
            List<Role> actual;
            target.AddedRoles = expected;
            actual = target.AddedRoles;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestLogin()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User); 
            string expected = string.Empty; 
            string actual;
            target.Login = expected;
            actual = target.Login;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestName()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User); 
            string expected = string.Empty; 
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRemovedRoles()
        {
            User User = null; 
            UserViewModel target = new UserViewModel(User); 
            List<Role> expected = null; 
            List<Role> actual;
            target.RemovedRoles = expected;
            actual = target.RemovedRoles;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUser()
        {
            User User = null;
            UserViewModel target = new UserViewModel(User); 
            
            User actual;
            
            actual = target.User;
            
        }
    }
}
