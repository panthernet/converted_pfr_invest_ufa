﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestExchangeRateViewModel : BaseTest
    {

        [TestMethod()]
        public void TestExchangeRateViewModelConstructor()
        {
            ExchangeRateViewModel target = new ExchangeRateViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteBrowseSource()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteBrowseSource();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteUploadSource()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteUploadSource();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestClearFromTrash()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            string val = string.Empty;
            string expected = string.Empty;
            string actual;
            actual = target.ClearFromTrash(val);
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteBrowseSource()
        //{
        //    ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
        //    target.ExecuteBrowseSource();
        //}

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteUploadSource()
        //{
        //    ExchangeRateViewModel target = new ExchangeRateViewModel();
        //    target.ExecuteUploadSource();
        //}

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestGenerateInsertQuery()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            Dictionary<string, DBField> flds = null;
            string expected = string.Empty;
            string actual;
            actual = target.GenerateInsertQuery(flds);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestBrowseSource()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.BrowseSource = expected;
            actual = target.BrowseSource;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestGeneratedQuery()
        {
            ExchangeRateViewModel target = new ExchangeRateViewModel();
            string expected = string.Empty;
            string actual;
            target.GeneratedQuery = expected;
            actual = target.GeneratedQuery;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSQLOnly()
        {
            ExchangeRateViewModel target = new ExchangeRateViewModel();
            bool expected = false;
            bool actual;
            target.SQLOnly = expected;
            actual = target.SQLOnly;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedFilePath()
        {
            ExchangeRateViewModel target = new ExchangeRateViewModel();
            string expected = string.Empty;
            string actual;
            target.SelectedFilePath = expected;
            actual = target.SelectedFilePath;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestUploadSource()
        {
            ExchangeRateViewModel_Accessor target = new ExchangeRateViewModel_Accessor();
            ICommand expected = null;
            ICommand actual;
            target.UploadSource = expected;
            actual = target.UploadSource;
            Assert.AreEqual(expected, actual);
        }
    }
}
