﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestEnrollmentOtherViewModel : BaseTest
    {



        [TestMethod()]
        public void TestEnrollmentOtherViewModelConstructor()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            EnrollmentOtherViewModel_Accessor target = new EnrollmentOtherViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            EnrollmentOtherViewModel_Accessor target = new EnrollmentOtherViewModel_Accessor();
            target.ExecuteSave();
        }



        [TestMethod()]
        public void TestLoad()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            long lId = 1;
            target.Load(lId);
        }

       

        [TestMethod()]
        public void TestAccount()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCurrencyList()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.CurrencyList = expected;
            actual = target.CurrencyList;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestRate()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectAccount()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedCurrency()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedCurrency = expected;
            actual = target.SelectedCurrency;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            Decimal expected = 12;
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotal()
        {
            EnrollmentOtherViewModel target = new EnrollmentOtherViewModel();
            Decimal actual;
            actual = target.Total;
        }
    }
}
