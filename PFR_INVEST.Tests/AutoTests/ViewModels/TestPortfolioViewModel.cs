﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Input;
using System.Collections.Generic;
using PFR_INVEST.ViewModels.ListItems;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPortfolioViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPortfolioViewModelConstructor()
        {
            PortfolioViewModel target = new PortfolioViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteAddAccount()
        {
            PortfolioViewModel_Accessor target = new PortfolioViewModel_Accessor(); 
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteAddAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteDeleteAccount()
        {
            PortfolioViewModel_Accessor target = new PortfolioViewModel_Accessor();
            bool expected = true; 
            bool actual;
            actual = target.CanExecuteDeleteAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteDeleteAccount()
        {
            PortfolioViewModel_Accessor target = new PortfolioViewModel_Accessor(); 
            target.ExecuteDeleteAccount();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            PortfolioViewModel_Accessor target = new PortfolioViewModel_Accessor(); 
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoad()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            long id = 1; 
            target.Load(id);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshAccounts()
        {
            PortfolioViewModel_Accessor target = new PortfolioViewModel_Accessor(); 
            target.RefreshAccounts();
        }

      

        [TestMethod()]
        public void TestAddAccount()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            ICommand actual;
            actual = target.AddAccount;
        }

        [TestMethod()]
        public void TestDeleteAccount()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            ICommand actual;
            actual = target.DeleteAccount;
        }


        [TestMethod()]
        public void TestPFRAccountIDsList()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            List<long> expected = null; 
            List<long> actual;
            target.PFRAccountIDsList = expected;
            actual = target.PFRAccountIDsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPFRAccountsList()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            List<PFRAccountsListItem> expected = null; 
            List<PFRAccountsListItem> actual;
            target.PFRAccountsList = expected;
            actual = target.PFRAccountsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedAccountID()
        {
            PortfolioViewModel target = new PortfolioViewModel(); 
            long expected = 0; 
            long actual;
            target.SelectedAccountID = expected;
            actual = target.SelectedAccountID;
            Assert.AreEqual(expected, actual);
        }
    }
}
