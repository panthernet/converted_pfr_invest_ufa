﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestBankAccountViewModel : BaseTest
    {

        [TestMethod()]
        public void TestBankAccountViewModelConstructor()
        {
            BankAccountViewModel target = new BankAccountViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            BankAccountViewModel_Accessor target = new BankAccountViewModel_Accessor();
            target.ExecuteSave();
        }

        [TestMethod()]
        public void TestLoadBankAccount()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            long _id = 1;
            target.LoadBankAccount(_id);
        }

        [TestMethod()]
        public void TestRefreshLegalEntitiesList()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            target.RefreshLegalEntitiesList();
        }

     
        [TestMethod()]
        public void TestAccountNumber()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.AccountNumber = expected;
            actual = target.AccountNumber;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBIK()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.BIK = expected;
            actual = target.BIK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBankLocation()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.BankLocation = expected;
            actual = target.BankLocation;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestBankName()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.BankName = expected;
            actual = target.BankName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCloseDate()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            DateTime expected = new DateTime();
            DateTime actual;
            target.CloseDate = expected;
            actual = target.CloseDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentID()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            long expected = 0;
            long actual;
            target.ContragentID = expected;
            actual = target.ContragentID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContragentName()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.ContragentName = expected;
            actual = target.ContragentName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCorrespondentAccountNumber()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.CorrespondentAccountNumber = expected;
            actual = target.CorrespondentAccountNumber;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestKPP()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            string expected = string.Empty;
            string actual;
            target.KPP = expected;
            actual = target.KPP;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLegalEntitiesList()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            List<DB2LegalEntityCard> expected = null;
            List<DB2LegalEntityCard> actual;
            target.LegalEntitiesList = expected;
            actual = target.LegalEntitiesList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLegalEntityID()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            long expected = 0;
            long actual;
            target.LegalEntityID = expected;
            actual = target.LegalEntityID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSelectedLegalEntity()
        {
            BankAccountViewModel target = new BankAccountViewModel();
            DB2LegalEntityCard expected = null;
            DB2LegalEntityCard actual;
            target.SelectedLegalEntity = expected;
            actual = target.SelectedLegalEntity;
            Assert.AreEqual(expected, actual);
        }
    }
}
