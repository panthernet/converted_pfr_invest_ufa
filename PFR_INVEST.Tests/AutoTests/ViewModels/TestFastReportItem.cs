﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestFastReportItem : BaseTest
    {
        [TestMethod()]
        public void TestFastReportItemConstructor()
        {
            FastReportItem target = new FastReportItem();
        }

        [TestMethod()]
        public void TestcalcSum()
        {
            FastReportItem target = new FastReportItem();
            target.calcSum();
        }

        [TestMethod()]
        public void TestCount()
        {
            FastReportItem target = new FastReportItem();
            long expected = 0;
            long actual;
            target.Count = expected;
            actual = target.Count;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDate()
        {
            FastReportItem target = new FastReportItem();
            DateTime expected = new DateTime();
            DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNKD()
        {
            FastReportItem target = new FastReportItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.NKD = expected;
            actual = target.NKD;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNop()
        {
            FastReportItem target = new FastReportItem();
            string expected = string.Empty;
            string actual;
            target.Nop = expected;
            actual = target.Nop;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOpDate()
        {
            FastReportItem target = new FastReportItem();
            DateTime expected = new DateTime();
            DateTime actual;
            target.OpDate = expected;
            actual = target.OpDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPrice()
        {
            FastReportItem target = new FastReportItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.Price = expected;
            actual = target.Price;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSaler()
        {
            FastReportItem target = new FastReportItem();
            string expected = string.Empty;
            string actual;
            target.Saler = expected;
            actual = target.Saler;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSecName()
        {
            FastReportItem target = new FastReportItem();
            Issue expected = null;
            Issue actual;
            target.SecName = expected;
            actual = target.SecName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumm()
        {
            FastReportItem target = new FastReportItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.Summ = expected;
            actual = target.Summ;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestYield()
        {
            FastReportItem target = new FastReportItem();
            Decimal expected = new Decimal();
            Decimal actual;
            target.Yield = expected;
            actual = target.Yield;
            Assert.AreEqual(expected, actual);
        }
    }
}
