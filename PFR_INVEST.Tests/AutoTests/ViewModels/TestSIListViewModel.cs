﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestSIListViewModel : BaseTest
    {


        [TestMethod()]
        public void TestSIListViewModelConstructor()
        {
            SIListViewModel target = new SIListViewModel();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshList()
        {
            SIListViewModel_Accessor target = new SIListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteRefreshNPFList()
        {
            SIListViewModel_Accessor target = new SIListViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteRefreshNPFList();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshList()
        {
            SIListViewModel_Accessor target = new SIListViewModel_Accessor();
            target.ExecuteRefreshList();
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteRefreshNPFList()
        {
            SIListViewModel_Accessor target = new SIListViewModel_Accessor();
            target.ExecuteRefreshNPFList();
        }

        [TestMethod()]
        public void TestLegalEntityID()
        {
            SIListViewModel target = new SIListViewModel();
            long expected = 0;
            long actual;
            target.LegalEntityID = expected;
            actual = target.LegalEntityID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNPFList()
        {
            SIListViewModel target = new SIListViewModel();
            DB2NPFListItem[] expected = null;
            DB2NPFListItem[] actual;
            target.NPFList = expected;
            actual = target.NPFList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRefreshNPFList()
        {
            SIListViewModel target = new SIListViewModel();
            ICommand actual;
            actual = target.RefreshNPFList;
        }
    }
}
