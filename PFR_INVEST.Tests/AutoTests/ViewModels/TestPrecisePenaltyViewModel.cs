﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestPrecisePenaltyViewModel : BaseTest
    {


        [TestMethod()]
        public void TestPrecisePenaltyViewModelConstructor()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            bool expected = false; 
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            PenaltyViewModel dueVM = null;
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 

            bool expected = true; 
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

      


        [TestMethod()]
        public void TestAccount()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            PortfolioListItemFull expected = null; 
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestChSumm()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.ChSumm = expected;
            actual = target.ChSumm;
            Assert.AreEqual(expected, actual);
        }

      

        [TestMethod()]
        public void TestMSumm()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            Decimal expected = new Decimal(); 
            Decimal actual;
            target.MSumm = expected;
            actual = target.MSumm;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMonthsList()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            List<DBEntity> expected = null; 
            List<DBEntity> actual;
            target.MonthsList = expected;
            actual = target.MonthsList;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSelectAccount()
        {
            PenaltyViewModel dueVM = null;
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM);
            
            ICommand actual;
            
            actual = target.SelectAccount;
            
        }

        [TestMethod()]
        public void TestSelectedMonth()
        {
            PenaltyViewModel dueVM = null; 
            PrecisePenaltyViewModel target = new PrecisePenaltyViewModel(dueVM); 
            DBEntity expected = null; 
            DBEntity actual;
            actual = target.SelectedMonth;
            
        }
    }
}
