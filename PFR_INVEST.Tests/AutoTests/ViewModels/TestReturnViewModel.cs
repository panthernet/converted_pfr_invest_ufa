﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.ViewModels.ListItems;
using PFR_INVEST.Proxy;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestReturnViewModel : BaseTest
    {
        [TestMethod()]
        public void TestReturnViewModelConstructor()
        {
            ReturnViewModel target = new ReturnViewModel();
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            ReturnViewModel target = new ReturnViewModel();
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestCanExecuteSelectAccount()
        {
            ReturnViewModel_Accessor target = new ReturnViewModel_Accessor();
            bool expected = true;
            bool actual;
            actual = target.CanExecuteSelectAccount();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestExecuteSave()
        {
            ReturnViewModel_Accessor target = new ReturnViewModel_Accessor();
            target.ExecuteSave();
        }

      

        [TestMethod()]
        public void TestLoad()
        {
            ReturnViewModel target = new ReturnViewModel();
            long lId = 1;
            target.Load(lId);
        }

    
        [TestMethod()]
        public void TestAccount()
        {
            ReturnViewModel target = new ReturnViewModel();
            PortfolioListItemFull expected = null;
            PortfolioListItemFull actual;
            target.Account = expected;
            actual = target.Account;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDirectionsList()
        {
            ReturnViewModel target = new ReturnViewModel();
            List<DBEntity> expected = null;
            List<DBEntity> actual;
            target.DirectionsList = expected;
            actual = target.DirectionsList;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestSelectAccount()
        {
            ReturnViewModel target = new ReturnViewModel();
            ICommand actual;
            actual = target.SelectAccount;
        }

        [TestMethod()]
        public void TestSelectedDirection()
        {
            ReturnViewModel target = new ReturnViewModel();
            DBEntity expected = null;
            DBEntity actual;
            target.SelectedDirection = expected;
            actual = target.SelectedDirection;
            Assert.AreEqual(expected, actual);
        }
    }
}
