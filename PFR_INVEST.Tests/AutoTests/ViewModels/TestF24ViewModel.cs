﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.ViewModels.ListItems;
using System.Collections.Generic;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestF24ViewModel : BaseTest
    {


        [TestMethod()]
        public void TestF24ViewModelConstructor()
        {
            long identity = 1;
            F24ViewModel target = new F24ViewModel(identity);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long identity = 1;
            F24ViewModel target = new F24ViewModel(identity);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestRefreshF24List()
        {
            long identity = 1;
            F24ViewModel target = new F24ViewModel(identity);
            target.RefreshF24List();
        }

        [TestMethod()]
        public void TestF24Form()
        {
            long identity = 1;
            F24ViewModel target = new F24ViewModel(identity);
            F24FormItem expected = null;
            F24FormItem actual;
            target.F24Form = expected;
            actual = target.F24Form;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestF24List()
        {
            long identity = 1;
            F24ViewModel target = new F24ViewModel(identity);
            List<F24ListItem> expected = null;
            List<F24ListItem> actual;
            target.F24List = expected;
            actual = target.F24List;
            Assert.AreEqual(expected, actual);
        }

    }
}
