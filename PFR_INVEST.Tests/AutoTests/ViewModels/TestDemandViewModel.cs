﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestDemandViewModel : BaseTest
    {

        [TestMethod()]
        public void TestDemandViewModelConstructor()
        {
            long id = 1;
            DemandViewModel target = new DemandViewModel(id);
        }

        [TestMethod()]
        public void TestCanExecuteSave()
        {
            long id = 0;
            DemandViewModel target = new DemandViewModel(id);
            bool expected = false;
            bool actual;
            actual = target.CanExecuteSave();
            Assert.AreEqual(expected, actual);
        }

        //[TestMethod()]
        //[DeploymentItem("PFR_INVEST.exe")]
        //public void TestExecuteSave()
        //{
        //    PrivateObject param0 = null;
        //    DemandViewModel_Accessor target = new DemandViewModel_Accessor(param0);
        //    target.ExecuteSave();
        //}


        [TestMethod()]
        public void TestITOGO()
        {
            long id = 0;
            DemandViewModel target = new DemandViewModel(id);
            string expected = "-1";
            string actual;
            target.ITOGO = expected;
            actual = target.ITOGO;
            Assert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void TestNEWTRANSH()
        {
            long id = 0;
            DemandViewModel target = new DemandViewModel(id);
            string expected = "-1";
            string actual;
            target.NEWTRANSH = expected;
            actual = target.NEWTRANSH;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPAYFROMAVERAGE()
        {
            long id = 0;
            DemandViewModel target = new DemandViewModel(id);
            string expected = "-1";
            string actual;
            target.PAYFROMAVERAGE = expected;
            actual = target.PAYFROMAVERAGE;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPAYFROMTRANSH()
        {
            long id = 0;
            DemandViewModel target = new DemandViewModel(id);
            string expected = "-1";
            string actual;
            target.PAYFROMTRANSH = expected;
            actual = target.PAYFROMTRANSH;
            Assert.AreEqual(expected, actual);
        }
    }
}
