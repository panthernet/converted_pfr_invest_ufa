﻿using PFR_INVEST.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ViewModels
{


    [TestClass()]
    public class TestTransferWizardViewModel_ModelContractRecord : BaseTest
    {


        [TestMethod()]
        public void TestTransferWizardViewModel_ModelContractRecordConstructor()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
        }

        [TestMethod()]
        public void TestContractID()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            long actual;
            actual = target.ContractID;
        }

        [TestMethod()]
        public void TestContractName()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            string actual;
            actual = target.ContractName;
        }

        [TestMethod()]
        public void TestContractOperateSum()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            Decimal actual;
            actual = target.ContractOperateSum;
        }

        [TestMethod()]
        public void TestInvestIncome()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            Decimal expected = new Decimal();
            Decimal actual;
            target.InvestIncome = expected;
            actual = target.InvestIncome;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestQuantityZL()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            int expected = 0;
            int actual;
            target.QuantityZL = expected;
            actual = target.QuantityZL;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSum()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            Decimal expected = new Decimal();
            Decimal actual;
            target.Sum = expected;
            actual = target.Sum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestUkFormalizedName()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            string actual;
            actual = target.UkFormalizedName;
        }

        [TestMethod()]
        public void TestUkName()
        {
            UKItem p_uk = null;
            ContractItem p_contract = null;
            TransferWizardViewModel.ModelContractRecord target = new TransferWizardViewModel.ModelContractRecord(p_uk, p_contract);
            string actual;
            actual = target.UkName;
        }
    }
}
