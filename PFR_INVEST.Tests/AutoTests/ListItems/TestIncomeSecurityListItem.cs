﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestIncomeSecurityListItem : BaseTest
    {



        [TestMethod()]
        public void TestIncomeSecurityListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
        }

        [TestMethod()]
        public void TestComment()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string expected = string.Empty;
            string actual;
            target.Comment = expected;
            actual = target.Comment;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestIncomeKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string expected = string.Empty;
            string actual;
            target.IncomeKind = expected;
            actual = target.IncomeKind;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOperationDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.OperationDate = expected;
            actual = target.OperationDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOrderDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.OrderDate = expected;
            actual = target.OrderDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOrderNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string expected = string.Empty;
            string actual;
            target.OrderNum = expected;
            actual = target.OrderNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOrderSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            decimal expected = new decimal();
            decimal actual;
            target.OrderSum = expected;
            actual = target.OrderSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPFID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            long expected = 0;
            long actual;
            target.PFID = expected;
            actual = target.PFID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPfrAccID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            long expected = 0;
            long actual;
            target.PfrAccID = expected;
            actual = target.PfrAccID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem target = new PFR_INVEST.ViewModels.ListItems.IncomeSecurityListItem(ent);
            string actual;
            actual = target.Quarter;
        }
    }
}
