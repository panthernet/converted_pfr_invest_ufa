﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestOrdersListItem : BaseTest
    {



        [TestMethod()]
        public void TestOrdersListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
        }

        [TestMethod()]
        public void TestCount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Count;
        }

        [TestMethod()]
        public void TestEntity()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            PFR_INVEST.Proxy.DBEntity actual;
            actual = target.Entity;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestIssue()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Issue;
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestNomValue()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.NomValue;
        }

        [TestMethod()]
        public void TestOrderID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            long actual;
            actual = target.OrderID;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Price;
        }

        [TestMethod()]
        public void TestRegDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.RegDate;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestStatus()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Status;
        }

        [TestMethod()]
        public void TestSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Sum;
        }

        [TestMethod()]
        public void TestTerm()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Term;
        }

        [TestMethod()]
        public void TestType()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.OrdersListItem target = new PFR_INVEST.ViewModels.ListItems.OrdersListItem(ent);
            string actual;
            actual = target.Type;
        }
    }
}
