﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestSPNMovementListItem : BaseTest
    {



        [TestMethod()]
        public void TestSPNMovementListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestisFromUK()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject param0 = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem_Accessor target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem_Accessor(param0);
            bool expected = false;
            bool actual;
            actual = target.isFromUK();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestActDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            System.DateTime actual;
            actual = target.ActDate;
        }

        [TestMethod()]
        public void TestActNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.ActNum;
        }

        [TestMethod()]
        public void TestCompany()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.Company;
        }

        [TestMethod()]
        public void TestContractNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.ContractNum;
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            System.DateTime actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestDatePP()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            System.Nullable<System.DateTime> actual;
            actual = target.DatePP;
        }

        [TestMethod()]
        public void TestEndSPNCount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.EndSPNCount;
        }

        [TestMethod()]
        public void TestFromUK()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.FromUK;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestIncome()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            decimal actual;
            actual = target.Income;
        }

        [TestMethod()]
        public void TestOperation()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPPNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            long actual;
            actual = target.PPNum;
        }

        [TestMethod()]
        public void TestStartSPNCount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.StartSPNCount;
        }

        [TestMethod()]
        public void TestToUK()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.ToUK;
        }

        [TestMethod()]
        public void TestTrID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            long actual;
            actual = target.TrID;
        }

        [TestMethod()]
        public void TestUK()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.SPNMovementListItem target = new PFR_INVEST.ViewModels.ListItems.SPNMovementListItem(ent);
            string actual;
            actual = target.UK;
        }
    }
}
