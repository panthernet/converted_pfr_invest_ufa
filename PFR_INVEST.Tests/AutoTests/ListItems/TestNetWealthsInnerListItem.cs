﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestNetWealthsInnerListItem : BaseTest
    {



        [TestMethod()]
        public void TestNetWealthsInnerListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            string typeField = string.Empty;
            string currDateField = string.Empty;
            string startDateField = string.Empty;
            PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem target = new PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem(ent, typeField, currDateField, startDateField);
        }

        [TestMethod()]
        public void TestCurrDateValue()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            string typeField = string.Empty;
            string currDateField = string.Empty;
            string startDateField = string.Empty;
            PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem target = new PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem(ent, typeField, currDateField, startDateField);
            decimal expected = new decimal();
            decimal actual;
            target.CurrDateValue = expected;
            actual = target.CurrDateValue;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNetWealthType()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            string typeField = string.Empty;
            string currDateField = string.Empty;
            string startDateField = string.Empty;
            PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem target = new PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem(ent, typeField, currDateField, startDateField);
            string expected = string.Empty;
            string actual;
            target.NetWealthType = expected;
            actual = target.NetWealthType;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestStartDateValue()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            string typeField = string.Empty;
            string currDateField = string.Empty;
            string startDateField = string.Empty;
            PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem target = new PFR_INVEST.ViewModels.ListItems.NetWealthsInnerListItem(ent, typeField, currDateField, startDateField);
            decimal expected = new decimal();
            decimal actual;
            target.StartDateValue = expected;
            actual = target.StartDateValue;
            Assert.AreEqual(expected, actual);
        }
    }
}
