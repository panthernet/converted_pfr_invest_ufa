﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestERZLNotifyInZLRedistListItem : BaseTest
    {


        [TestMethod()]
        public void TestERZLNotifyInZLRedistListItemConstructor()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
        }

        [TestMethod()]
        public void TestDate()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.Date = expected;
            actual = target.Date;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNPF2OTHER()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            long expected = 0;
            long actual;
            target.NPF2OTHER = expected;
            actual = target.NPF2OTHER;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNPF2PFR()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            long expected = 0;
            long actual;
            target.NPF2PFR = expected;
            actual = target.NPF2PFR;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNPFName()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            string expected = string.Empty;
            string actual;
            target.NPFName = expected;
            actual = target.NPFName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNPF_ID()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            long expected = 0;
            long actual;
            target.NPF_ID = expected;
            actual = target.NPF_ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestNotifyNum()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            string expected = string.Empty;
            string actual;
            target.NotifyNum = expected;
            actual = target.NotifyNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOTHER2NPF()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            long expected = 0;
            long actual;
            target.OTHER2NPF = expected;
            actual = target.OTHER2NPF;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPFR2NPF()
        {
            string name = string.Empty;
            PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem target = new PFR_INVEST.ViewModels.ListItems.ERZLNotifyInZLRedistListItem(name);
            long expected = 0;
            long actual;
            target.PFR2NPF = expected;
            actual = target.PFR2NPF;
            Assert.AreEqual(expected, actual);
        }
    }
}
