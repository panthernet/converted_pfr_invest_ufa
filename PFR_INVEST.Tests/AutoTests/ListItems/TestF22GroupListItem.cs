﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF22GroupListItem : BaseTest
    {


        [TestMethod()]
        public void TestF22GroupListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent);
        }

        [TestMethod()]
        public void TestAccount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.Account;
        }

        [TestMethod()]
        public void TestAcquisitionPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            decimal actual;
            actual = target.AcquisitionPrice;
        }

        [TestMethod()]
        public void TestAmount()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            decimal actual;
            actual = target.Amount;
        }

        [TestMethod()]
        public void TestBankName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.BankName;
        }

        [TestMethod()]
        public void TestBrokerName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.BrokerName;
        }

        [TestMethod()]
        public void TestCertificateID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.CertificateID;
        }

        [TestMethod()]
        public void TestDebName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.DebName;
        }

        [TestMethod()]
        public void TestDocDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            System.DateTime actual;
            actual = target.DocDate;
        }

        [TestMethod()]
        public void TestDocNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.DocNum;
        }

        [TestMethod()]
        public void TestFondManagerName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.FondManagerName;
        }

        [TestMethod()]
        public void TestFondName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.FondName;
        }

        [TestMethod()]
        public void TestInterest()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            decimal actual;
            actual = target.Interest;
        }

        [TestMethod()]
        public void TestIssueNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.IssueNum;
        }

        [TestMethod()]
        public void TestIssuerName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.IssuerName;
        }

        [TestMethod()]
        public void TestManageCompanyName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.ManageCompanyName;
        }

        [TestMethod()]
        public void TestMarketPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            decimal actual;
            actual = target.MarketPrice;
        }

        [TestMethod()]
        public void TestMarketPriceSource()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.MarketPriceSource;
        }

        [TestMethod()]
        public void TestMunicipalName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.MunicipalName;
        }

        [TestMethod()]
        public void TestQuantity()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            long actual;
            actual = target.Quantity;
        }

        [TestMethod()]
        public void TestRegion()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.Region;
        }

        [TestMethod()]
        public void TestRulesRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.RulesRegNum;
        }

        [TestMethod()]
        public void TestSecurityCategory()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.SecurityCategory;
        }

        [TestMethod()]
        public void TestSecurityClassification()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.SecurityClassification;
        }

        [TestMethod()]
        public void TestStateRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F22GroupListItem target = new PFR_INVEST.ViewModels.ListItems.F22GroupListItem(ent); 
            string actual;
            actual = target.StateRegNum;
        }
    }
}
