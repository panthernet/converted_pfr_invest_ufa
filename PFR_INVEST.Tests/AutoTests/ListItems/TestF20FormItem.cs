﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF20FormItem : BaseTest
    {


        [TestMethod()]
        public void TestF20FormItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity);
        }

     

        [TestMethod()]
        public void TestFullName()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.FullName = expected;
            actual = target.FullName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestINN()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            long expected = 0; 
            long actual;
            target.INN = expected;
            actual = target.INN;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestOnDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.OnDate = expected;
            actual = target.OnDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPersonSpecDep()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.PersonSpecDep = expected;
            actual = target.PersonSpecDep;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPersonUK()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.PersonUK = expected;
            actual = target.PersonUK;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfName()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.PortfName = expected;
            actual = target.PortfName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RegDate = expected;
            actual = target.RegDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RegNum = expected;
            actual = target.RegNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNumOut()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.RegNumOut = expected;
            actual = target.RegNumOut;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReportDate()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            string expected = string.Empty; 
            string actual;
            target.ReportDate = expected;
            actual = target.ReportDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReportType()
        {
            PFR_INVEST.Proxy.DBEntity entity = null; 
            PFR_INVEST.ViewModels.ListItems.F20FormItem target = new PFR_INVEST.ViewModels.ListItems.F20FormItem(entity); 
            long expected = 0; 
            long actual;
            target.ReportType = expected;
            actual = target.ReportType;
            Assert.AreEqual(expected, actual);
        }
    }
}
