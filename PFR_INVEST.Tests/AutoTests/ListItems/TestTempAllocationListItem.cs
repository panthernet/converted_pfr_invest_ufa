﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestTempAllocationListItem : BaseTest
    {



        [TestMethod()]
        public void TestTempAllocationListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            decimal rate = new decimal();
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent, rate);
        }

        [TestMethod()]
        public void TestTempAllocationListItemConstructor1()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
        }

        [TestMethod()]
        public void TestCurrSumm()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            decimal actual;
            actual = target.CurrSumm;
        }

        [TestMethod()]
        public void TestCurrency()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Currency;
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestHalfYear()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.HalfYear;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Kind;
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestOperation()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Operation;
        }

        [TestMethod()]
        public void TestPortfolio()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Portfolio;
        }

        [TestMethod()]
        public void TestQuarter()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.Quarter;
        }

        [TestMethod()]
        public void TestRate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            decimal expected = new decimal();
            decimal actual;
            target.Rate = expected;
            actual = target.Rate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestRubSumm()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem(ent);
            decimal actual;
            actual = target.RubSumm;
        }

        [TestMethod()]
        [DeploymentItem("PFR_INVEST.exe")]
        public void TestSum()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.PrivateObject param0 = null;
            PFR_INVEST.ViewModels.ListItems.TempAllocationListItem_Accessor target = new PFR_INVEST.ViewModels.ListItems.TempAllocationListItem_Accessor(param0);
            decimal actual;
            actual = target.Sum;
        }
    }
}
