﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestInsuranceListItem : BaseTest
    {



        [TestMethod()]
        public void TestInsuranceListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
        }

        [TestMethod()]
        public void TestChangeDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.ChangeDate = expected;
            actual = target.ChangeDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestCloseDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.CloseDate = expected;
            actual = target.CloseDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestContractID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            long expected = 0;
            long actual;
            target.ContractID = expected;
            actual = target.ContractID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestDiscrepancy()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            decimal actual;
            actual = target.Discrepancy;
        }

        [TestMethod()]
        public void TestDocSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string expected = string.Empty;
            string actual;
            target.DocSum = expected;
            actual = target.DocSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            long expected = 0;
            long actual;
            target.ID = expected;
            actual = target.ID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestInsurance()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string expected = string.Empty;
            string actual;
            target.Insurance = expected;
            actual = target.Insurance;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestLegalEntity()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string actual;
            actual = target.LegalEntity;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestReqDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            System.DateTime expected = new System.DateTime();
            System.DateTime actual;
            target.ReqDate = expected;
            actual = target.ReqDate;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestReqNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string expected = string.Empty;
            string actual;
            target.ReqNum = expected;
            actual = target.ReqNum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestRequiedInsuranceSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            decimal actual;
            actual = target.RequiedInsuranceSum;
        }

        [TestMethod()]
        public void TestSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            decimal expected = new decimal();
            decimal actual;
            target.Sum = expected;
            actual = target.Sum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestSumString()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            string expected = string.Empty;
            string actual;
            target.SumString = expected;
            actual = target.SumString;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestTotalInmanagement()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.ListItems.InsuranceListItem target = new PFR_INVEST.ViewModels.ListItems.InsuranceListItem(ent);
            decimal actual;
            actual = target.TotalInmanagement;
        }
    }
}
