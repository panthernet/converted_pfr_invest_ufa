﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestPortfolioListItemFull : BaseTest
    {



        [TestMethod()]
        public void TestPFType()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            string actual;
            actual = target.PFType;
        }

        [TestMethod()]
        public void TestPFName()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            string expected = string.Empty;
            string actual;
            target.PFName = expected;
            actual = target.PFName;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestBankID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            long actual;
            actual = target.BankID;
        }

        [TestMethod()]
        public void TestBank()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            string actual;
            actual = target.Bank;
        }

        [TestMethod()]
        public void TestAccountType()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            string actual;
            actual = target.AccountType;
        }

        [TestMethod()]
        public void TestAccountNumber()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            string expected = string.Empty;
            string actual;
            target.AccountNumber = expected;
            actual = target.AccountNumber;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestAccountID()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
            long expected = 0;
            long actual;
            target.AccountID = expected;
            actual = target.AccountID;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestPortfolioListItemFullConstructor()
        {
            PFR_INVEST.Proxy.DBEntity dbEn = null;
            PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull target = new PFR_INVEST.ViewModels.ListItems.PortfolioListItemFull(dbEn);
        }
    }
}
