﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestContragentListItem : BaseTest
    {
        [TestMethod()]
        public void TestContragentListItemConstructor()
        {
            DBEntity dbEn = null;
            ContragentListItem target = new ContragentListItem(dbEn);
        }

        [TestMethod()]
        public void TestHasReverse()
        {
            DBEntity dbEn = null;
            ContragentListItem target = new ContragentListItem(dbEn);
            bool expected = false;
            bool actual;
            target.HasReverse = expected;
            actual = target.HasReverse;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestID()
        {
            DBEntity dbEn = null;
            ContragentListItem target = new ContragentListItem(dbEn);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestName()
        {
            DBEntity dbEn = null;
            ContragentListItem target = new ContragentListItem(dbEn);
            string expected = string.Empty;
            string actual;
            target.Name = expected;
            actual = target.Name;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestType()
        {
            DBEntity dbEn = null;
            ContragentListItem target = new ContragentListItem(dbEn);
            string expected = string.Empty;
            string actual;
            target.Type = expected;
            actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}
