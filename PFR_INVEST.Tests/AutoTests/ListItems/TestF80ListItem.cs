﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestF80ListItem : BaseTest
    {



        [TestMethod()]
        public void TestF80ListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent);
        }

        [TestMethod()]
        public void TestDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.Date;
        }

        [TestMethod()]
        public void TestDiff()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.Diff;
        }

        [TestMethod()]
        public void TestFactPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.FactPrice;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestKind()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.Kind;
        }

        [TestMethod()]
        public void TestMarketPrice()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.MarketPrice;
        }

        [TestMethod()]
        public void TestName()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.Name;
        }

        [TestMethod()]
        public void TestRegNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.RegNum;
        }

        [TestMethod()]
        public void TestYear()
        {
            PFR_INVEST.Proxy.DBEntity ent = null; 
            PFR_INVEST.ViewModels.ListItems.F80ListItem target = new PFR_INVEST.ViewModels.ListItems.F80ListItem(ent); 
            string actual;
            actual = target.Year;
        }
    }
}
