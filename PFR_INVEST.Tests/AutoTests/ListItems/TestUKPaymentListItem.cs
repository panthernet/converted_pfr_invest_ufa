﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace PFR_INVEST.Tests.AutoTests.ListItems
{


    [TestClass()]
    public class TestUKPaymentListItem : BaseTest
    {



        [TestMethod()]
        public void TestTrID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            long actual;
            actual = target.TrID;
        }

        [TestMethod()]
        public void TestStatus()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            string actual;
            actual = target.Status;
        }

        [TestMethod()]
        public void TestReqID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            long actual;
            actual = target.ReqID;
        }

        [TestMethod()]
        public void TestPlanSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            decimal expected = new decimal();
            decimal actual;
            target.PlanSum = expected;
            actual = target.PlanSum;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void TestMonth()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            string actual;
            actual = target.Month;
        }

        [TestMethod()]
        public void TestInvestDohod()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            decimal actual;
            actual = target.InvestDohod;
        }

        [TestMethod()]
        public void TestID()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            long actual;
            actual = target.ID;
        }

        [TestMethod()]
        public void TestFactSum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            decimal actual;
            actual = target.FactSum;
        }

        [TestMethod()]
        public void TestActNum()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            string actual;
            actual = target.ActNum;
        }

        [TestMethod()]
        public void TestActDate()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
            string actual;
            actual = target.ActDate;
        }

        [TestMethod()]
        public void TestUKPaymentListItemConstructor()
        {
            PFR_INVEST.Proxy.DBEntity ent = null;
            PFR_INVEST.ViewModels.UKPaymentPlanViewModel vm = null;
            PFR_INVEST.ViewModels.ListItems.UKPaymentListItem target = new PFR_INVEST.ViewModels.ListItems.UKPaymentListItem(ent, vm);
        }
    }
}
