﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.BusinessLogic
{
    [TestClass]
    public class DateToolsTest:BaseTest
    {
        [TestMethod]
        public void TestIsPeriodIntersect()
        {
            var result = DateTools.IsPeriodIntersect(DateTime.Parse("2010.10.1"), DateTime.Parse("2010.11.1"), DateTime.Parse("2011.10.1"), DateTime.Parse("2011.11.1"));
            Assert.IsFalse(result);

            result = DateTools.IsPeriodIntersect(DateTime.Parse("2011.10.1"), DateTime.Parse("2011.11.1"),DateTime.Parse("2010.10.1"), DateTime.Parse("2010.11.1"));
            Assert.IsFalse(result);

            result = DateTools.IsPeriodIntersect(DateTime.Parse("2010.1.1"), DateTime.Parse("2020.1.1"), DateTime.Parse("2015.1.1"), DateTime.Parse("2025.1.1"));
            Assert.IsTrue(result);

            result = DateTools.IsPeriodIntersect(DateTime.Parse("2015.1.1"), DateTime.Parse("2025.1.1"),DateTime.Parse("2010.1.1"), DateTime.Parse("2020.1.1") );
            Assert.IsTrue(result);

            result = DateTools.IsPeriodIntersect(DateTime.Parse("2010.1.1"), DateTime.Parse("2025.1.1"), DateTime.Parse("2015.1.1"), DateTime.Parse("2020.1.1"));
            Assert.IsTrue(result);

            result = DateTools.IsPeriodIntersect(DateTime.Parse("2015.1.1"), DateTime.Parse("2020.1.1"),DateTime.Parse("2010.1.1"), DateTime.Parse("2025.1.1"));
            Assert.IsTrue(result);
        }
    }
}
