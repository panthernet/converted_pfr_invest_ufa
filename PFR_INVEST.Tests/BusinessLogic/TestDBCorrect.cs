﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.BusinessLogic
{
    [TestClass]
    public class TestDBCorrect : BaseTest
    {
		/// <summary>
		/// Проверка совпадения сумм для перечислений УК в терминальном статусе
		/// </summary>
        [TestMethod]
        public void TestTransferPPStatus()
        {
			// список перечислений, для которых обязательно совпадение сумм
			var tTrList = DataContainerFacade.GetListByPropertyConditions<ReqTransfer>(new List<ListPropertyCondition> { 
					ListPropertyCondition.Equal("TransferStatus", TransferStatusIdentifier.sSPNTransferred),
					ListPropertyCondition.NotEqual("StatusID", (long)-1)
				});

			// писок поручений, для которых обязательно расхождение сумм
			var ntTrList = DataContainerFacade.GetListByPropertyConditions<ReqTransfer>(new List<ListPropertyCondition> 
			{ 
				ListPropertyCondition.NotEqual("TransferStatus", TransferStatusIdentifier.sSPNTransferred),
				ListPropertyCondition.NotEqual("TransferStatus", TransferStatusIdentifier.sDemandReceivedUK),
				ListPropertyCondition.NotEqual("TransferStatus", TransferStatusIdentifier.sActSigned),
				ListPropertyCondition.NotEqual("StatusID", (long)-1)
			});

			var asgForTransfers = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(new List<ListPropertyCondition> 
			{ 
				ListPropertyCondition.IsNotNull("ReqTransferID"), 
				ListPropertyCondition.NotEqual("StatusID", (long)-1) 
			});
			var grPP = asgForTransfers.GroupBy(x => x.ReqTransferID);

			foreach (var tr in tTrList)
			{
				var asgGroup = grPP.FirstOrDefault(x => x.Key == tr.ID);
				Assert.IsNotNull(asgGroup, "Не найдено платежных поручений (AsgFinTr) для перечисления (ReqTransfer) c ID = " + tr.ID);
				Assert.AreEqual(tr.Sum, asgGroup.Sum(x => x.DraftAmount), "Несоответствие суммы платежных поручений (AsgFinTr) и перечисления (ReqTransfer) c ID = " + tr.ID);
			}

			foreach (var tr in ntTrList)
			{
				var asgGroup = grPP.FirstOrDefault(x => x.Key == tr.ID);
				if (asgGroup != null && tr.Sum > 0)
				{
					Assert.AreNotEqual(tr.Sum, asgGroup.Sum(x => x.DraftAmount), "Cуммы платежных поручений (AsgFinTr) и перечисления (ReqTransfer) c ID = " + tr.ID + " совпадают, но поручение не находится в терминальном статусе");
				}
			}
        }

		[TestMethod]
		public void TestTempAllocationSum()
		{
			//var aList = DataContainerFacade.GetListByProperty<Register>("Kind","Передача СПН во временное размещение");
			var rList = DataContainerFacade.GetListByProperty<Register>("Kind", "Изъятие СПН из временного размещения");

			var pfIDList = rList.Select(x => x.PortfolioID).Where(x => x.HasValue).Select(x => x.Value).Distinct();

			foreach (var pfID in pfIDList)
			{
				var frList = BLServiceSystem.Client.GetSPNAllocatedFinregisters(pfID).GroupBy(x => x.NPFName);
				var frRetList = BLServiceSystem.Client.GetSPNReturnedFinregisters(pfID).GroupBy(x => x.NPFName);

				foreach (var grRet in frRetList)
				{
					var sumRet = grRet.Sum(x => x.Count);
					var zlRet = grRet.Sum(x => x.ZLCount);

					var gr = frList.FirstOrDefault(x => x.Key == grRet.Key);
					const string descr = "Для изъятия НПФ \"{0}\"(Статус {1}, DBT_ACC_ID = {2}) в портфеле c ID {3}";
					if (gr == null)
					{
						Assert.Fail(descr + " не найдено размещений", grRet.First().NPFName, grRet.First().NPFStatus, grRet.First().DbtAccID, pfID);
					}

					if (gr.Sum(x => x.Count) < sumRet)
					{
						Assert.Fail(descr + " недостаточно количество средств в размещении", grRet.First().NPFName, grRet.First().NPFStatus, grRet.First().DbtAccID, pfID);
					}

					if (gr.Sum(x => x.ZLCount) < zlRet)
					{
						Assert.Fail(descr + " недостаточно количество ЗЛ в размещении", grRet.First().NPFName, grRet.First().NPFStatus, grRet.First().DbtAccID, pfID);
					}
				}
			}
		}
    }
}
