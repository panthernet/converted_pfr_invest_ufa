﻿using PFR_INVEST.ViewModels.ListItems;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.Tests
{


    [TestClass()]
    public class TestBalanceListItem
    {


        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


[TestMethod()]
public void TestBalanceListItemConstructor()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
Decimal rate = new Decimal(); // TODO: Initialize to an appropriate value
    BalanceListItem target = new BalanceListItem(ent, rate);
}

[TestMethod()]
public void TestBalanceListItemConstructor1()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
    BalanceListItem target = new BalanceListItem(ent);
}

[TestMethod()]
public void TestAccountNum()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.AccountNum;
}

[TestMethod()]
public void TestAccountType()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.AccountType;
}

[TestMethod()]
public void TestCurrency()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Currency;
}

[TestMethod()]
public void TestDOCKIND()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    long actual;
    actual = target.DOCKIND;
}

[TestMethod()]
public void TestDate()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Date;
}

[TestMethod()]
public void TestFinish()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Finish;
}

[TestMethod()]
public void TestID()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    long actual;
    actual = target.ID;
}

[TestMethod()]
public void TestMinus()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Minus;
}

[TestMethod()]
public void TestMonth()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Month;
}

[TestMethod()]
public void TestOperation()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Operation;
}

[TestMethod()]
public void TestPlus()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Plus;
}

[TestMethod()]
public void TestPortfolio()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Portfolio;
}

[TestMethod()]
public void TestQuarter()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Quarter;
}

[TestMethod()]
public void TestRate()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
Decimal expected = new Decimal(); // TODO: Initialize to an appropriate value
    Decimal actual;
    target.Rate = expected;
    actual = target.Rate;
    Assert.AreEqual(expected, actual);
}

[TestMethod()]
public void TestSource()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Source;
}

[TestMethod()]
public void TestStart()
{
DBEntity ent = null; // TODO: Initialize to an appropriate value
BalanceListItem target = new BalanceListItem(ent); // TODO: Initialize to an appropriate value
    string actual;
    actual = target.Start;
}
    }
}
