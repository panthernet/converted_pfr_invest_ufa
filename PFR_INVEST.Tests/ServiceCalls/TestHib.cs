﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.ServiceCalls
{
    [TestClass]
    public class TestHib : BaseTest
    {
        private void TestHibCommon<TEntity>() where TEntity : BaseDataObject
        {
            TestHibCommonInternal<TEntity, long>();
        }

        private void TestHibCommonInternal<TEntity, TKey>() where TKey : struct, IComparable
        {
            IList<TEntity> list;
            try
            {
                list = DataContainerFacade.GetListLimit<TEntity>(10);
            }
            catch (Exception ex)
            {
                if (_mHibDataObjectsErrors == null)
                    Assert.Inconclusive();
                _mHibDataObjectsErrors.Add($"{typeof(TEntity).FullName}: Ошибка GetListLimit. Описание исключения: {ex.Message}{Environment.NewLine}{ex.StackTrace}.");
                return;
            }

            if (list.Count == 0)
                return;

            if (list.Count > 3)
            {
                IList<TEntity> listPart;
                try
                {
                    listPart = DataContainerFacade.GetListPage<TEntity>(3, 3);
                }
                catch (Exception ex)
                {
                    if (_mHibDataObjectsErrors == null)
                        Assert.Inconclusive();
                    _mHibDataObjectsErrors.Add($"{typeof(TEntity).FullName}: Ошибка GetListPage. Описание исключения: {ex.Message}{Environment.NewLine}{ex.StackTrace}.");
                    return;
                }
                var idp = typeof(TEntity).GetProperty("ID");
                var idPart = (TKey)idp.GetValue(listPart[0], null);
                var idList = (TKey)idp.GetValue(list[3], null);

                Assert.AreEqual(idPart, idList);
            }

            var idProp = typeof(TEntity).GetProperty("ID");
            var id = (TKey)idProp.GetValue(list[0], null);

            var obj = default(TEntity);
            try
            {
                obj = DataContainerFacade.GetByID<TEntity, TKey>(id);
            }
            catch (Exception ex)
            {
                _mHibDataObjectsErrors.Add($"{typeof(TEntity).FullName}: Ошибка GetByID. Описание исключения: {ex.Message}{Environment.NewLine}{ex.StackTrace}.");
                return;
            }

            Assert.IsNotNull(obj);

            try
            {
                DataContainerFacade.Save<TEntity, TKey>(obj);
            }
            catch (Exception ex)
            {
                _mHibDataObjectsErrors.Add($"{typeof(TEntity).FullName}: Ошибка Save. Описание исключения: {ex.Message}{Environment.NewLine}{ex.StackTrace}.");
            }
        }

        private static IEnumerable<Type> GetDataObjectsForTesting()
        {
            var checkTypes = new List<Type>();
            var asm = Assembly.GetAssembly(typeof(BaseDataObject));
            foreach (var cls in asm.GetTypes())
            {
                if (cls.Namespace == "PFR_INVEST.DataObjects" &&
                    cls.IsClass &&
                    !(cls.IsSealed && cls.IsAbstract) && // Игнорируем статик класы                    
                    cls != typeof(BalanceExtra) && //не наследуется от BaseDataObject
                    cls != typeof(BaseDataObject) &&
                    cls != typeof(BaseReportType) &&
                    cls != typeof(IF022Part) &&
                    cls != typeof(ILoadedFromDBF) &&
                    cls != typeof(BaseDataObject.ValidateEventArgs))
                    checkTypes.Add(cls);
            }
            return checkTypes;
        }

        private List<string> _mHibDataObjectsErrors = new List<string>();

        private readonly List<string> _notActiveTypes = new List<string>
        {
            "PFR_INVEST.DataObjects.AuditEvent",
            "PFR_INVEST.DataObjects.AuditUI",
            "PFR_INVEST.DataObjects.AuditInterface",
            "PFR_INVEST.DataObjects.Audit",
            "PFR_INVEST.DataObjects.ListPropertyCondition",
            "PFR_INVEST.DataObjects.Element",
            "PFR_INVEST.DataObjects.F050CBInfoLite",
            "PFR_INVEST.DataObjects.F040DetailLite",
            "PFR_INVEST.DataObjects.F040DetailsListItemLite",
            "PFR_INVEST.DataObjects.F050CBInfoListItemLite"
            };

        [TestMethod]
        public void TestDataObjects()
        {
            //cheking all HibDataObjects
            var checkTypes = GetDataObjectsForTesting();
            _mHibDataObjectsErrors = new List<string>();
            foreach (var cls in checkTypes)
            {
                if (_notActiveTypes.Contains(cls.FullName) || !(typeof(BaseDataObject).IsAssignableFrom(cls)))
                    continue;

                var mi = GetType().GetMethod("TestHibCommon",
                                                         BindingFlags.NonPublic | BindingFlags.Instance);
                mi = mi.MakeGenericMethod(cls);
                mi.Invoke(this, null);

            }
            if (_mHibDataObjectsErrors.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, _mHibDataObjectsErrors));
            }
        }

        //[TestMethod]
        //public void TestServiceKnownTypes()
        //{          

        //    //check ServiceKnownTypeAttribute
        //    List<Type> checkTypes = GetDataObjectsForTesting();
        //    checkTypes.Add(typeof(ArrayList));
        //    System.Attribute[] attrs = System.Attribute.GetCustomAttributes(typeof(db2connector.IDB2Connector), typeof(ServiceKnownTypeAttribute));

        //    foreach (Type chk in checkTypes)
        //    {
        //        bool find = false;
        //        foreach (System.Attribute attr in attrs)
        //        {
        //            if (attr is ServiceKnownTypeAttribute)
        //            {
        //                ServiceKnownTypeAttribute a = (ServiceKnownTypeAttribute)attr;
        //                if (a.Type.Equals(chk))
        //                {
        //                    find = true;
        //                    break;
        //                }
        //            }
        //        }
        //        Assert.IsTrue(find, string.Format("[ServiceKnownType(typeof({0}))] is missed", chk.Name));
        //    }
        //}

        [TestMethod]
        public void TestHibDepClaim()
        {
            TestHibCommonInternal<DepClaim2, long>();
        }


        [TestMethod]
        public void TestHibTDate()
        {
            TestHibCommonInternal<TDate, long>();
        }

        [TestMethod]
        public void TestHibTemplate()
        {
            TestHibCommonInternal<Template, long>();
        }

        [TestMethod]
        public void TestHibDepClaimSelectParams()
        {
            TestHibCommonInternal<DepClaimSelectParams, long>();
        }

        [TestMethod]
        public void TestHibDepClaimOffer()
        {
            TestHibCommonInternal<DepClaimOffer, long>();
        }

        [TestMethod]
        public void TestLegalEntityCourier()
        {
            TestHibCommonInternal<LegalEntityCourier, long>();
        }

        [TestMethod]
        public void TestDocument()
        {
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<Document, long>();
            }
        }

        [TestMethod]
        public void TestAttach()
        {
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<Attach, long>();
            }
        }

        [TestMethod]
        public void TestDelayedPaymentClaim()
        {
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<DelayedPaymentClaim, long>();
            }
        }

        [TestMethod]
        public void TestReportType()
        {
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<SIReportType, long>();
            }
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<VRReportType, long>();
            }
        }

        [TestMethod]
        public void TestStock()
        {
            using (PerformanceChecker.New())
            {
                TestHibCommonInternal<Stock, long>();
            }            
        }

        [TestMethod]
        public void SettingOpenForm()
        {
            using (PerformanceChecker.New())
            {
               TestHibCommonInternal<SettingOpenForm, long>();
            }            
        }

		[TestMethod]
		public void TestPaymentDetail()
		{
			using (PerformanceChecker.New())
			{
				TestHibCommonInternal<PaymentDetail, long>();
			}
		}
    }
}