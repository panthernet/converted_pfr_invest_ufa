﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ServiceItems;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.ServiceCalls
{
	[TestClass]
	public class TestListSelectors : BaseTest
	{
		#region Test generator
		/*[TestMethod]
        public void GenerateTests()
        {
            Type t = GetClient().Client.GetType();
            MethodInfo [] aMethods = t.GetMethods();

            string sResult = "";
            foreach (MethodInfo info in aMethods)
            {
                string sN = info.Name.ToLower();

                if (sN.StartsWith("get") && sN.EndsWith("list"))
                {
                    //Console.WriteLine(info.Name);

                    sResult += @"        [TestMethod]
        public void Test" + info.Name  + @"()
        {
            PerformanceChecker chk = new PerformanceChecker();
            GetClient().Client." + info.Name + @"();
            chk.CheckPerformance();                
        }" + "\r\n\r\n";
                }
            }
            Console.WriteLine(sResult);
		
        }*/
		#endregion

		[TestMethod]
		public void TestGetContractsForUKs()
		{
			var chk = new PerformanceChecker();
			BLServiceSystem.Client.GetContractsForUKs();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetPortfolioFull()
		{
			var lst = DataContainerFacade.GetList<Return>();
			if (lst == null || lst.Count < 1)
				Assert.Inconclusive();

			var ret = lst.FirstOrDefault(r => r.PortfolioID.HasValue && r.PFRBankAccountID.HasValue);
			if (ret == null)
				Assert.Inconclusive();

			var chk = new PerformanceChecker();
			GetClient().Client.GetPortfolioFull(ret.PFRBankAccountID.Value, ret.PortfolioID.Value);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetPortfolioFullForBank()
		{
			var lst = DataContainerFacade.GetList<AddSPN>();
			if (lst == null || lst.Count < 1)
				Assert.Inconclusive();

			var penalty = lst.FirstOrDefault(p => p.PortfolioID.HasValue && p.LegalEntityID.HasValue);
			if (penalty == null)
				Assert.Inconclusive();

			var chk = new PerformanceChecker();
			GetClient().Client.GetPortfolioFullForBank(penalty.PortfolioID.Value, penalty.LegalEntityID.Value);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetPortfoliosByType()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetPortfoliosByType();
			chk.CheckPerformance();
			GetClient().Client.GetPortfoliosByType(new[] { Portfolio.Types.SPN });
			chk.CheckPerformance();
			GetClient().Client.GetPortfoliosByType(new[] { Portfolio.Types.DSV });
			chk.CheckPerformance();
			GetClient().Client.GetPortfoliosByType(new[] { Portfolio.Types.NoType });
			chk.CheckPerformance();
			GetClient().Client.GetPortfoliosByType(new[] { Portfolio.Types.ROPS });
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetPortfoliosFiltered()
		{
			var lst = DataContainerFacade.GetList<Portfolio>();
			if (!lst.Any())
				Assert.Inconclusive();

			var chk = new PerformanceChecker();
			var p = new PortfolioIdentifier.PortfolioPBAParams
			{
				PortfolioID = lst.First().ID,
				CurrencyName = new[] { "рубли" },
				AccountType = new[] { "текущий" },
				LegalEntityFormalizedNameContains = new[] { "цб" }
			};
			GetClient().Client.GetPortfoliosFiltered(p);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetJournalEventObjectList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetJournalEventObjectList();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetRatesList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetRatesListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetDeadZLListHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetDeadZLListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetZLTransfersList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetZLMovementsListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetInsuranceActualList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetInsuranceList(true);
			chk.CheckPerformance();
			GetClient().Client.GetInsuranceList(false);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetERZLList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetERZLList();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetOwnFundsList()
		{
			var chk = new PerformanceChecker();
			var ownFounds = GetClient().Client.GetOwnedFoundsListHib();
			chk.CheckPerformance();

			var ownFoundsPrevious = GetClient().Client.GetOwnedFoundsListHibPrevious();
			Assert.IsTrue(ownFoundsPrevious.Count == ownFounds.Count);

			foreach (var item in ownFoundsPrevious)
			{
				var previousItem = ownFoundsPrevious.FirstOrDefault(ownf => ownf.ID == item.ID);
				Assert.IsTrue(previousItem != null);
				Assert.IsTrue(previousItem.Equals(item));
			}
		}

		[TestMethod]
		public void TestGetF60ListHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetF60ListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetF70ListHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetF70ListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetDraftsList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetTransferToNPFArchiveListFilteredByPage(0, null, null);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetAllocationRequestList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetAllocationRequestListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetBudgetsList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetBudgetsListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetCostsList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetCostsListFilteredByPage(0, null, null);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetPFRAccountsList()
		{
			var chk = new PerformanceChecker();
			var optResult = GetClient().Client.GetPFRAccountsList();
			chk.CheckPerformance();

		}

		[TestMethod]
		public void TestGetDepositsListByPropertyHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetDepositsListByPropertyHib2("Status", (long)1);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetFZList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetFZList();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetNPFListLEHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetSIListHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetSIListHib(true);
			chk.CheckPerformance();
			GetClient().Client.GetSIListHib(false);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetCurrencyList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetCurrencyListHib();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetSchilsCostsList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetSchilsCostsList();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetSellReportsList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetBuyOrSaleReportsListHib(true, null);
			chk.CheckPerformance();
			GetClient().Client.GetBuyOrSaleReportsListHib(false, null);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetNetWealthsF010List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetNetWealthsF010List();
			chk.CheckPerformance();
		}
		//[TestMethod]
		//public void TestGetNetWealthsF012List()
		//{
		//    PerformanceChecker chk = new PerformanceChecker();
		//    GetClient().Client.GetNetWealthsF012List();
		//    chk.CheckPerformance();
		//}
		//[TestMethod]
		//public void TestGetNetWealthsF014List()
		//{
		//    PerformanceChecker chk = new PerformanceChecker();
		//    GetClient().Client.GetNetWealthsF014List();
		//    chk.CheckPerformance();
		//}
		[TestMethod]
		public void TestGetNetWealthsF015List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetNetWealthsF015List();
			chk.CheckPerformance();
		}
		[TestMethod]
		public void TestGetNetWealthsF016List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetNetWealthsF016List();
			chk.CheckPerformance();
		}



		[TestMethod]
		public void TestGetMarketCostF020List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetMarketCostF020List();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetMarketCostF020ListNullContract()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetMarketCostF020ListNullContract();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetMarketCostF022List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetMarketCostF022List();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetMarketCostF025ListNullContract()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetMarketCostF025ListNullContract();
			chk.CheckPerformance();
		}

		//[TestMethod]
		//public void TestGetMarketCostF024List()
		//{
		//    PerformanceChecker chk = new PerformanceChecker();
		//    GetClient().Client.GetMarketCostF024List();
		//    chk.CheckPerformance();
		//}

		[TestMethod]
		public void TestGetMarketCostF026List()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetMarketCostF026List();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetF040List()
		{
			var chk = new PerformanceChecker();
			var lst = GetClient().Client.GetF040List();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetBankListForDeposit()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetBankListForDeposit();
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetJournalLog()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetJournalLog(new JournalFilter { DateFrom = DateTime.Now.Subtract(TimeSpan.FromDays(7)), DateTo = DateTime.Now});
			chk.CheckPerformance();
		}

		[TestMethod]
		public void GetAddSPNFullListByPage()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetAddSPNFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetAddSPNFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetAddSPNFullListByPage(new PortfolioTypeFilter() , 1);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void GetDopSPNFullListByPage()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetDopSPNFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetDopSPNFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetDopSPNFullListByPage(new PortfolioTypeFilter(), 1);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void GetApsFullListByPage()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetApsFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetApsFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetApsFullListByPage(new PortfolioTypeFilter(), 1);
			chk.CheckPerformance();
		}

		[TestMethod]
		public void GetDopApsFullListByPage()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetDopApsFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV }, Exclude = true }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetDopApsFullListByPage(new PortfolioTypeFilter { Types = new[] { Portfolio.Types.DSV } }, 1);
			chk.CheckPerformance();
			GetClient().Client.GetDopApsFullListByPage(new PortfolioTypeFilter() , 1);
			chk.CheckPerformance();
		}


		[TestMethod]
		public void TestGetOVSITransfersList()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetTransfersListBYContractType(Document.Types.SI, true, false, false);
			chk.CheckPerformance();
			GetClient().Client.GetTransfersListBYContractType(Document.Types.SI, false, false, false);
			chk.CheckPerformance();
			GetClient().Client.GetTransfersListBYContractType(Document.Types.SI, true, true, false);
			chk.CheckPerformance();
			GetClient().Client.GetTransfersListBYContractType(Document.Types.SI, false, true, false);
			chk.CheckPerformance();
		}

		public static string[] ContrAgentsTypes = { "УК", "НПФ" };




		[TestMethod]
		public void TestGetSIDocList()
		{
			using (PerformanceChecker.New())
				GetClient().Client.GetSIDocumentsListHib(Document.Statuses.Control);
			using (PerformanceChecker.New())
				GetClient().Client.GetSIDocumentsListHib(Document.Statuses.Executed);
			using (PerformanceChecker.New())
				GetClient().Client.GetSIDocumentsListHib(Document.Statuses.All);
			using (PerformanceChecker.New())
				GetClient().Client.GetSIDocumentsListHib(Document.Statuses.Active);

		}

		[TestMethod]
		public void TestGetBankAccountListByTypeHib()
		{
			var chk = new PerformanceChecker();
			GetClient().Client.GetBankAccountListByTypeHib(new[] { "НПФ" });
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetMaxAuctionNumDepClaimSelectParams()
		{
			var chk = new PerformanceChecker();
			var max = GetClient().Client.GetMaxAuctionNumDepClaimSelectParams(2013);
			Assert.IsTrue(max != -1, "Не удалось получить максимальный номер аукциона на 2013 год");
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetCountSelectDateDepClaimSelectParams()
		{
			var chk = new PerformanceChecker();
			var count = GetClient().Client.GetCountSelectDateDepClaimSelectParams(DateTime.Now);
			Assert.IsTrue(count != -1, "Не удалось получить количество уведомлений на текущую дату");
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestDivisionAndPersonList()
		{
			var chk = new PerformanceChecker();
			var divisions = DataContainerFacade.GetList<Division>();
			Assert.IsTrue(divisions != null && divisions.Count > 0, "Не удалось получить список подразделений (Division)");
			var persons = GetClient().Client.GetPersonListHib(divisions.First().ID);
			Assert.IsTrue(persons != null, "Не удалось получить список сотрудников подразделений (Person)");
			chk.CheckPerformance();
		}

		[TestMethod]
		public void TestGetListByPropertyCondition()
		{
			var list = DataContainerFacade.GetListByPropertyCondition<Person>("LastName", "%а%", "like").ToList();
			Assert.IsTrue(list != null, "Ошибка при загрузке Person по условию like для поля LastName");
		}

		[TestMethod]
		public void TestIncludedToThe761List()
		{
			var lst = DataContainerFacade.GetList<BankConclusion>();
			if (!lst.Any())
			{
				Assert.Inconclusive();
			}

			var t = lst.First();
			var included = GetClient().Client.IncludedToThe761List(t.Code, t.ImportDate.AddDays(5));
			Assert.IsTrue(included, "Ошибка при загрузке списка банков удовлетворяющему  требованиям постановления Правительства РФ № 761");
			included = GetClient().Client.IncludedToThe761List(t.Code + "----------", t.ImportDate.AddDays(5));
			Assert.IsTrue(!included, "Ошибка при загрузке списка банков удовлетворяющему  требованиям постановления Правительства РФ № 761");
		}



		[TestMethod]
		public void TestGetDepClaimOfferListItem()
		{
			var list = GetClient().Client.GetDepClaimOfferList();
			if (!list.Any())
				Assert.Inconclusive();
		}

		[TestMethod]
		public void TestGetPfrReportInsuredList()
		{
			var chk = new PerformanceChecker();
			var list = GetClient().Client.GetPfrReportInsuredList(9, 2013, 1);

			chk.CheckPerformance();

			if (!list.Any())
				Assert.Inconclusive();
		}

		[TestMethod]
		public void TestGetDepClaimOfferListFiltered()
		{
			var offers = GetClient().Client.GetDepClaimOfferList();
			if (!offers.Any())
				Assert.Inconclusive();


			var checkedStatus = DepClaimOffer.Statuses.Accepted;
			{
				var accepted = offers.Where(o => o.Offer.Status == checkedStatus).ToList();
				if (!accepted.Any())
					Assert.Inconclusive();

				var list = GetClient().Client.GetDepClaimOfferListFiltered(accepted.First().Auction.ID, new List<DepClaimOffer.Statuses> { checkedStatus });
				if (!list.Any())
					Assert.Fail("GetDepClaimOfferListFiltered not work as expected");
				Assert.IsTrue(list.Any(o => o.Offer.ID == accepted.First().Offer.ID));
			}

			checkedStatus = DepClaimOffer.Statuses.NotAccepted;
			{
				var accepted = offers.Where(o => o.Offer.Status == checkedStatus).ToList();
				if (!accepted.Any())
					Assert.Inconclusive();

				var list = GetClient().Client.GetDepClaimOfferListFiltered(accepted.First().Auction.ID, new List<DepClaimOffer.Statuses> { checkedStatus });
				if (!list.Any())
					Assert.Fail("GetDepClaimOfferListFiltered not work as expected");
				Assert.IsTrue(list.Any(o => o.Offer.ID == accepted.First().Offer.ID));
			}
			checkedStatus = DepClaimOffer.Statuses.NotSigned;
			{
				var accepted = offers.Where(o => o.Offer.Status == checkedStatus).ToList();
				if (!accepted.Any())
					Assert.Inconclusive();

				var list = GetClient().Client.GetDepClaimOfferListFiltered(accepted.First().Auction.ID, new List<DepClaimOffer.Statuses> { checkedStatus });
				if (!list.Any())
					Assert.Fail("GetDepClaimOfferListFiltered not work as expected");
				Assert.IsTrue(list.Any(o => o.Offer.ID == accepted.First().Offer.ID));
			}
			checkedStatus = DepClaimOffer.Statuses.Signed;
			{
				var accepted = offers.Where(o => o.Offer.Status == checkedStatus).ToList();
				if (!accepted.Any())
					Assert.Inconclusive();

				var list = GetClient().Client.GetDepClaimOfferListFiltered(accepted.First().Auction.ID, new List<DepClaimOffer.Statuses> { checkedStatus });
				if (!list.Any())
					Assert.Fail("GetDepClaimOfferListFiltered not work as expected");
				Assert.IsTrue(list.Any(o => o.Offer.ID == accepted.First().Offer.ID));
			}
		}

		[TestMethod]
		public void TestGetElementByType()
		{
			var list = GetClient().Client.GetElementByType(Element.Types.NpfDocumentAdditionalExecutionInfo);
			if (!list.Any())
				Assert.Inconclusive();
		}

		[TestMethod]
		public void TestGetOfferPfrBankAccountSumForOffer()
		{
			var list = GetClient().Client.GetOfferPfrBankAccountSumForOffer(100);
			if (!list.Any())
				Assert.Inconclusive();
		}

		[TestMethod]
		public void TestGetDepClaimOfferListByDate()
		{
			var list = GetClient().Client.GetDepClaimOfferListBySettleDate(new[] { DepClaimOffer.Statuses.Accepted }, new DateTime(2013, 8, 8));
			if (!list.Any())
				Assert.Inconclusive();
		}


		[TestMethod]
		public void TestGetActiveLegalEntityByType()
		{
			using (PerformanceChecker.New())
			{
				var list = GetClient().Client.GetActiveLegalEntityByType(Contragent.Types.Npf);
				if (!list.Any())
					Assert.Inconclusive();
			}
		}
		[TestMethod]
		public void TestGetPaymentHistoryList()
		{
			var ids =
				GetClient().Client.GetList(typeof(OfferPfrBankAccountSum).FullName).Cast<OfferPfrBankAccountSum>().ToList
					().Select(item => item.ID).Distinct().ToArray();
			using (PerformanceChecker.New())
			{
				var list = GetClient().Client.GetPaymentHistoryList(ids);
				if (!list.Any())
					Assert.Inconclusive();
			}
		}
		[TestMethod]
		public void TestBLServiceSystemClient()
		{
			// var uk = BLServiceSystem.Client.GetValidUKListByMcProfitAbilityHib(1, 1, 1);

			//if (uk != null && uk.Any())
			//{var contr = BLServiceSystem.Client.GetValidContractListByMcProfitAbilityHib(1, uk.FirstOrDefault().ID, 1, 1);
			//}

			var result = BLServiceSystem.Client.GetDocumentsBypropertiesListHib(Document.Types.SI, "1", DateTime.Now);

		}

		[TestMethod]
		public void TestPaggedF50()
		{
			using (PerformanceChecker.New())
			{
				var list = GetClient().Client.GetF050CBInfoByPage((int)Document.Types.SI, 100);
				if (!list.Any())
					Assert.Inconclusive();
			}
		}

		[TestMethod]
		public void TestPaggedF10()
		{
			using (PerformanceChecker.New())
			{
				var count = GetClient().Client.GetCountNetWealthsSumF010();
				if (count == 0)
					Assert.Inconclusive();

				var list = GetClient().Client.GetNetWealthsSumF010ListByOnlyPage((int)Math.Max(0, count - 100));
				if (!list.Any())
					Assert.Inconclusive();


			}
		}

		[TestMethod]
		public void TestPaggedCosts()
		{
			using (PerformanceChecker.New())
			{
				var count = GetClient().Client.GetCountCostsListFiltered(null, null);
				if (count == 0)
					Assert.Inconclusive();

				var list = GetClient().Client.GetCostsListFilteredByPage((int)Math.Max(0, count - 100), null, null);
				if (!list.Any())
					Assert.Inconclusive();


			}
		}

		[TestMethod]
		public void TestTransfersStartSum()
		{
			Assert.Inconclusive("Тест временно не коректен");

			using (PerformanceChecker.New())
			{
				var list = GetClient().Client.GetTransfersListBYContractType(Document.Types.SI, true, false, false);
				var grouped = list.GroupBy(x => x.ContractId).ToList();
				foreach (var group in grouped)
				{
					var s_group = group.ToList().OrderBy(x => x.ActDate).ThenBy(x => x.ReqTransferID).ToList();
					var diff = 0;
					for (var i = 0; i < s_group.Count - 1; )
					{
						var prev = s_group[i];
						var cur = s_group[++i];


						if (cur.SumBeforeOperation != prev.SumAfterOperation)
						{
							diff++;
							Assert.Fail("Не совпадают конечная {0} и начальная {1} суммы перечислений для предыдущего перечисления {2} и последующего {3} за даты {4} и {5} для договора {6} ",
								prev.SumAfterOperation,
								cur.SumBeforeOperation,
								prev.ReqTransferID,
								cur.ReqTransferID,
								prev.ActDate.Value.ToShortDateString(),
								cur.ActDate.Value.ToShortDateString(),
								cur.ContractNumber
								);
						}

					}
					if (diff > 0)
						Assert.Fail("Не совпадают суммы в {0} записей", diff);
				}
			}
		}
	}
}
