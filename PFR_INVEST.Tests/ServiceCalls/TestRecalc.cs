﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.ServiceCalls
{
    using DataObjects;
    using DataAccess.Client;

    [TestClass]
    public class TestRecalc : BaseTest
    {
        [TestMethod]
        public void TestRecals()
        {
            var contract = DataContainerFacade.GetListLimit<Contract>(1).FirstOrDefault();
            if (contract == null) { Assert.Inconclusive(); }
            GetClient().Client.RecalcUKPortfoliosForContract(contract.ID);
        } 
    }
}
