﻿using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.ServiceCalls
{
	[TestClass]
	public class TestListSerialize : BaseTest
	{
		[TestMethod]
		public void TestListProperties()
		{
			TestHibernateClass<DepClaim2>();
			TestHibernateClass<LegalEntity>();
			TestHibernateClass<RatingAgency>();
			TestHibernateClass<LegalEntityRating>();
			TestHibernateClass<MultiplierRating>();
			TestHibernateClass<Rating>();

		}

		[TestMethod]
		public void TestBankDepositsList()
		{
			var banks = BLServiceSystem.Client.GetBankListForDeposit();
			var ent = BLServiceSystem.Client.GetBankDepositInfoListByBankIds(banks.Select(x => x.ID).ToArray());

			// метод для получения xml без маппинга на поля и таблицы БД из-за составной логики
			var xml = Util.GetXmlByEntityList(ent.ToList());
			var entDirect = Util.GetEntityListByXml<BankDepositInfoItem>(xml);
			CompareLists(ent, entDirect);

			// метод для получения xml с маппингом, с запросом от клиентской логики из-за сложности условия выбора(join и проверка полей связанных таблиц) и сортировки
			var bankXML = DataContainerFacade.GetXMLByList(banks.ToList());
			var bankDirect = DataContainerFacade.GetListByXML<LegalEntity>(bankXML);
			CompareLists(banks, bankDirect);
		}

		[TestMethod]
		public void TestBankVerificationDataContainer()
		{
			var dc = new BanksVerificationDataContainer();

			// проверка общих полей для проверки банков и формирования лимитов
			if (dc.Banks.Count == 0 || dc.RatingAgencies.Count == 0 || dc.Ratings.Count == 0 || dc.MultiplierRatings.Count == 0 || dc.LegalEntityRatings.Count == 0)
			{
				Assert.Inconclusive();
			}

			var sdc = dc.GetXML();
			var rdc = BanksVerificationDataContainer.FromXML(sdc);

			CompareLists(dc.Banks, rdc.Banks);
			CompareLists(dc.RatingAgencies, rdc.RatingAgencies);
			CompareLists(dc.Ratings, rdc.Ratings);
			CompareLists(dc.MultiplierRatings, rdc.MultiplierRatings);
			CompareLists(dc.LegalEntityRatings, rdc.LegalEntityRatings);

			// проверка частных полей при формировании лимитов (данные по депозитам)
			dc.LoadBankLimitData();

			sdc = dc.GetXML();
			rdc = BanksVerificationDataContainer.FromXML(sdc);

			CompareLists(dc.BankDepositInfo, rdc.BankDepositInfo);
			CompareLists(dc.Depclaims, rdc.Depclaims);
		}

		[TestMethod]
		public void TestDataContainersInDB()
		{
			var dbRecords = DataContainerFacade.GetListByProperty<RepositoryImpExpFile>("ImpExp", 0);//все файлы экспорта
			foreach (var r in dbRecords)
			{
				var strR = Encoding.UTF8.GetString(GZipCompressor.Decompress(r.Repository));

				BanksVerificationDataContainer.FromXML(strR);
			}
		}

		private void TestHibernateClass<T>()
		{
			var type = typeof(T);
			var name = type.Name;
			var properties = type.GetProperties().Where(x => x.GetCustomAttributes(typeof(DataMemberAttribute), true).Any() && x.Name != "ExtensionData");
			var idProperty = properties.SingleOrDefault(x => x.Name == "ID");
			if (idProperty == null) Assert.Inconclusive("В классе {0} не найден идентификатор", name);

			var xml = DataContainerFacade.GetXMLByPropertyConditions<T>(new List<ListPropertyCondition>());
			var ent = DataContainerFacade.GetListByXML<T>(xml);
			var entDirect = DataContainerFacade.GetListByPropertyConditions<T>(new List<ListPropertyCondition>());
			CompareLists(ent, entDirect);
		}

		private void CompareLists<T>(IList<T> ent, IList<T> entDirect)
		{
			var type = typeof(T);
			var name = type.Name;
			var properties = type.GetProperties().Where(x => x.GetCustomAttributes(typeof(DataMemberAttribute), true).Any() && x.Name != "ExtensionData");
			var idProperty = type.GetProperties().SingleOrDefault(x => x.Name == "ID");
			if (idProperty == null) Assert.Inconclusive("В классе {0} не найден идентификатор", name);

			Assert.AreEqual(ent.Count, entDirect.Count, "Ошибка в количестве восстановленных после сериализации записей " + name);

			foreach (var e in ent)
			{
				Assert.IsTrue(entDirect.Any(y => (long)idProperty.GetValue(y, null) == (long)idProperty.GetValue(e, null)), "Ошибка при сериализации или восстановлении первичного ключа " + name);
			}

		    var propertyInfos = properties as PropertyInfo[] ?? properties.ToArray();
		    foreach (var item in entDirect)
			{
				var sItem = ent.Single(x => (long)idProperty.GetValue(item, null) == (long)idProperty.GetValue(x, null));

				foreach (var pi in propertyInfos)
				{
					var left = pi.GetValue(item, null);
					var right = pi.GetValue(sItem, null);

					//Баг сериализатора. не различает пустую строку и null для типа String.
					if (pi.PropertyType == typeof(string))
					{
						left = left ?? string.Empty;
						right = right ?? string.Empty;
					}

					Assert.AreEqual(left, right, "Несоответствие значений {0} для поля {1}, ID = {2}", name, pi.Name, (long)idProperty.GetValue(item, null));
				}
			}
		}

	}
}
