﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Tests.MetricService;

namespace PFR_INVEST.Tests.ServiceCalls
{
	[TestClass]
	public class TestMetricService
	{
		[TestMethod]
		public void TestSOAP()
		{
			var client = new MetricServiceClient();

			var metric = client.GetMetric();

			Assert.IsTrue(metric.Any());
						
			Assert.IsNull(metric.FirstOrDefault(m => m.Name == "DBSERVICE" && m.Value == "BAD"), "DBSERVICE");
			Assert.IsNull(metric.FirstOrDefault(m => m.Name == "DBSERVER" && m.Value == "BAD"), "DBSERVER");
			Assert.IsNull(metric.FirstOrDefault(m => m.Name == "ALLSYSTEMS" && m.Value == "BAD"), "ALLSYSTEMS");
		}
	}
}
