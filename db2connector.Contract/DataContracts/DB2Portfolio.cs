﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public class DB2Portfolio
    {
        #region Fields
        private long id = 0;
        private string year = "";
        List<PfrBankAccount> lPfrBAcc = new List<PfrBankAccount>();

        #endregion

        #region Properties

        [DataMember]
        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        public List<PfrBankAccount> PfrBankAccountList
        {
            get { return lPfrBAcc; }
            set { lPfrBAcc = value; }
        }

        #endregion

        public DB2Portfolio() { }

        public DB2Portfolio(Portfolio p)
        {
            id = p.ID;
            year = p.Year;
        }
    }
}
