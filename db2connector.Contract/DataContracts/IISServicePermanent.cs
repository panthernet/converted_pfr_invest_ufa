﻿using System;

namespace PFR_INVEST.Proxy
{
    public partial class DB2LegalEntityCard
    {


        public DB2LegalEntityCard(string _FullNameField,
                                string _INNField,
                                string _HeadPositionField,
                                string _HeadFullNameField,
                                string _LegalAddressField,
                                string _RealAddressField,
                                string _PostalAddressField,
                                string _PhoneField,
                                string _FaxField,
                                string _CommentField,
                                string _EMailField,
                                long _ContragentIDField,
                                long _ID,

                                DateTime _RegistrationDateField,
                                DateTime _EndDateField,
                                DateTime _CloseDateField,
                                string _ShortNameField,
                                string _RegistrationNumberField,
                                string _FormalizedNameField,
                                string _SiteField,
                                string _SubsidiariesField,
                                string _OkppField,
                                string _RegistratorField,
                                string _licNumber,
                                DateTime _licRegDate,
                                DateTime _licCloseDate,
                                DateTime _licDate,
                                string _licRegistrator,
                                string _npfStatus,
                                string _declName,
                                string _letterWho,
                                string _transfDocKind,
                                long _ls_id)
        {
            FullName = _FullNameField;
            INN = _INNField;
            HeadPosition = _HeadPositionField;
            HeadFullName = _HeadFullNameField;
            LegalAddress = _LegalAddressField;
            RealAddress = _RealAddressField;
            PostalAddress = _PostalAddressField;
            Phone = _PhoneField;
            Fax = _FaxField;
            Comment = _CommentField;
            EMail = _EMailField;
            ContragentID = _ContragentIDField;
            ID = _ID;
            OKPP = _OkppField;

            RegistrationDate = _RegistrationDateField;
            EndDate = _EndDateField;
            CloseDate = _CloseDateField;
            ShortName = _ShortNameField;
            RegistrationNumber = _RegistrationNumberField;
            FormalizedName = _FormalizedNameField;
            Site = _SiteField;
            Subsidiaries = _SubsidiariesField;
            Registrator = _RegistratorField;
            LicNumber = _licNumber;
            LicRegDate = _licRegDate;
            LicCloseDate = _licCloseDate;
            LicDate = _licDate;
            LicRegistrator = _licRegistrator;
            NPFStatus = _npfStatus;
            DeclName = _declName;
            LetterWho = _letterWho;
            TransfDocKind = _transfDocKind;
            LS_ID = _ls_id;
        }

        public override string ToString()
        {
            return FullName;
        }
    }

    public partial class DB2Account
    {
        public DB2Account(long id, string name, long contragentID, long accountKindID)
        {
            ID = id;
            Name = name;
            ContragentID = contragentID;
            AccountTypeID = accountKindID;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public partial class DB2AccountKind
    {
        public DB2AccountKind(long id, string measure)
        {
            ID = id;
            Measure = measure;
        }

        public override string ToString()
        {
            return Measure;
        }
    }
}
