﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public partial class DB2LegalEntityCard
    {
        #region Fields
        // ID - id юр. лица
        private long id = 0;

        // CONTRAGENTID - id контрагента
        private long contragentID = 0;

        // FULLNAME - Полное наименование юр. лица
        private string fullName = "";

        // INN - ИНН
        private string inn = "";

        // OKPP - 
        private string okpp = "";

        // HEADPOSITION - Должность руководителя
        private string headPosition = "";

        // HEADFULLNAME - Полное имя руководителя
        private string headFullName = "";

        // LEGALADDRESS - Юридический адрес
        private string legalAddress = "";

        // STREETADDRESS - Фактический адрес
        private string streetAddress = "";

        // POSTADDRESS - Почтовый адрес
        private string postalAddress = "";

        // PHONE - Телефон
        private string phone = "";

        // FAX - Факс
        private string fax = "";

        // EADDRESS - E-Mail
        private string email = "";

        // REGISTRATIONDATE - дата регистрации
        private DateTime registrationDate = DateTime.Now;

        // ENDDATE - Дата окончания срока действия договора
        private DateTime endDate = DateTime.Now;

        // CLOSEDATE - дата
        private DateTime closeDate = DateTime.Now;

        // SHORTNAME - Краткое официальное наименование
        private string shortName = "";

        // REGISTRATIONNUM - Номер государственной регистрации
        private string registrationNumber = "";

        // FORMALIZEDNAME - Формализованное наименование
        private string formalizedName = "";

        // SITE - Сайт
        private string site = "";

        // SUBSIDIARIES - Сайт
        private string subsidiaries = "";

        // REGISTRATOR - Орган осуществляющий регистрацию
        private string registrator = "";

        // COMMENT - Комментарий
        private string comment = "";

        // INFOSOURCE - информация по
        private string infoSource = "";

        // DECLNAME - 
        private string declName = "";

        // LETTERWHO - 
        private string letterWho = "";

        // TRANSFDOCKIND - 
        private string transfDocKind = "";

        // LS_ID - 
        private long ls_id = 0;

        private string licNumber = "";

        private DateTime licRegDate = DateTime.Now;

        private DateTime licCloseDate = DateTime.Now;

        private DateTime licDate = DateTime.Now;

        private DateTime licSuspStartDate = DateTime.MinValue;

        private DateTime licSuspEndDate = DateTime.MinValue;

        private string licRegistrator = "";

        private long licID = 0;

        private string npfStatus = "";
        #endregion

        #region DataMembers
        [DataMember]
        public string DeclName
        {
            get { return declName; }
            set { declName = value; }
        }

        [DataMember]
        public string LetterWho
        {
            get { return letterWho; }
            set { letterWho = value; }
        }

        [DataMember]
        public string TransfDocKind
        {
            get { return transfDocKind; }
            set { transfDocKind = value; }
        }

        [DataMember]
        public long LS_ID
        {
            get { return ls_id; }
            set { ls_id = value; }
        }

        [DataMember]
        public string OKPP
        {
            get { return okpp; }
            set { okpp = value; }
        }

        [DataMember]
        public string InfoSource
        {
            get { return infoSource; }
            set { infoSource = value; }
        }

        [DataMember]
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        [DataMember]
        public string Registrator
        {
            get { return registrator; }
            set { registrator = value; }
        }

        [DataMember]
        public string Subsidiaries
        {
            get { return subsidiaries; }
            set { subsidiaries = value; }
        }

        [DataMember]
        public string Site
        {
            get { return site; }
            set { site = value; }
        }

        [DataMember]
        public string FormalizedName
        {
            get { return formalizedName; }
            set { formalizedName = value; }
        }

        [DataMember]
        public string RegistrationNumber
        {
            get { return registrationNumber; }
            set { registrationNumber = value; }
        }

        [DataMember]
        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; }
        }

        [DataMember]
        public DateTime CloseDate
        {
            get { return closeDate; }
            set { closeDate = value; }
        }

        [DataMember]
        public DateTime RegistrationDate
        {
            get { return registrationDate; }
            set { registrationDate = value; }
        }

        [DataMember]
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        [DataMember]
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        [DataMember]
        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        [DataMember]
        public string EMail
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public string LegalAddress
        {
            get { return legalAddress; }
            set { legalAddress = value; }
        }

        [DataMember]
        public string RealAddress
        {
            get { return streetAddress; }
            set { streetAddress = value; }
        }

        [DataMember]
        public string PostalAddress
        {
            get { return postalAddress; }
            set { postalAddress = value; }
        }

        [DataMember]
        public string HeadFullName
        {
            get { return headFullName; }
            set { headFullName = value; }
        }

        [DataMember]
        public string HeadPosition

        {
            get { return headPosition; }
            set { headPosition = value; }
        }

        [DataMember]
        public string INN
        {
            get { return inn; }
            set { inn = value; }
        }

        [DataMember]
        public string FullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        [DataMember]
        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public long ContragentID
        {
            get { return contragentID; }
            set { contragentID = value; }
        }

        [DataMember]
        public string LicNumber
        {
            get { return licNumber; }
            set { licNumber = value; }
        } 

        [DataMember]
        public DateTime LicRegDate
        {
            get { return licRegDate; }
            set { licRegDate = value; }
        }

        [DataMember]
        public DateTime LicCloseDate
        {
            get { return licCloseDate; }
            set { licCloseDate = value; }
        }
            
        [DataMember]
        public DateTime LicDate
        {
            get { return licDate; }
            set { licDate = value; }
        }

        [DataMember]
        public DateTime LicSuspStartDate
        {
            get { return licSuspStartDate; }
            set { licSuspStartDate = value; }
        }

        [DataMember]
        public DateTime LicSuspEndDate
        {
            get { return licSuspEndDate; }
            set { licSuspEndDate = value; }
        }

        [DataMember]
        public string LicRegistrator
        {
            get { return licRegistrator; }
            set { licRegistrator = value; }
        }

        [DataMember]
        public long LicID
        {
            get { return licID; }
            set { licID = value; }
        }

        [DataMember]
        public string NPFStatus
        {
            get { return npfStatus; }
            set { npfStatus = value; }
        }
        #endregion

        public DB2LegalEntityCard() { }
    }
}
