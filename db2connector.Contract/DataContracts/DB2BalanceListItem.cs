﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.Proxy.DataContracts
{
    public struct pairs
    {
        public string key;
        public string val;
        public pairs(string k, string v)
        {
            key = k;
            val = v;
        }
    }

    [DataContract]
    public class DB2BalanceListItem
    {
        #region properties

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string PORTFOLIO { get; set; }

        [DataMember]
        public string PORTFOLIO_YEAR { get; set; }

        [DataMember]
        public long PORTFOLIO_TYPE { get; set; }

        [DataMember]
        public DateTime DATE { get; set; }

        [DataMember]
        public string KIND { get; set; }

        [DataMember]
        public string CURRENCY { get; set; }

        [DataMember]
        public string ACCOUNT { get; set; }

        [DataMember]
        public string ACCOUNT_TYPE { get; set; }

        [DataMember]
        public decimal SUMM { get; set; }

        [DataMember]
        public decimal CURRENCYSUMM { get; set; }

        [Obsolete("Mistyped, use ForeignKey instead")]
        [IgnoreDataMember]
        public long ForeighKey
        {
            get { return ForeignKey; }
            set { ForeignKey = value; }
        }

        [DataMember]
        public long ForeignKey { get; set; }

        [DataMember]
        public int Ord { get; set; }

        [DataMember]
        public int SubQuery { get; set; }

        [DataMember]
        public string Content { get; set; }

        #endregion

        public string Year
        {
            get { return DATE.Year.ToString(); }
        }

    }
}
