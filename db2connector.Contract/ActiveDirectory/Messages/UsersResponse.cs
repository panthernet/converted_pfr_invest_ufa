﻿using System.Runtime.Serialization;
using db2connector.Contract.ActiveDirectory.MessageBase;

namespace db2connector.Contract.ActiveDirectory.Messages
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class UsersResponse : ResponseBase
    {
        [DataMember]
        public ADUser[] Users { get; set; }
    }
}