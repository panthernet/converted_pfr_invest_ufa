﻿using System.Runtime.Serialization;
using db2connector.Contract.ActiveDirectory.MessageBase;

namespace db2connector.Contract.ActiveDirectory.Messages
{
    [DataContract(Namespace = ActiveDirectorySettingsHelper.ServiceNamespace)]
    public class GroupsResponse : ResponseBase
    {
        [DataMember]
        public ADGroup[] Groups { get; set; }
    }
}