﻿using System;

namespace db2connector.Contract.ActiveDirectory.MessageBase
{
    /// <summary>
    /// Перечисление возможных типов валидации запроса.
    /// </summary>
    [Flags]
    public enum Validate
    {
        ClientTag = 0x0001,

        AccessToken = 0x0002,

        UserCredentials = 0x0004,

        All = ClientTag | AccessToken | UserCredentials
    }
}