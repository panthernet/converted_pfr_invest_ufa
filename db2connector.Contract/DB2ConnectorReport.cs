﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using PFR_INVEST.DataObjects.Reports;

namespace db2connector
{
    public partial interface IDB2Connector
    {
        [OperationContract]
        ReportFile CreateReportForNpfOpsReport(int year, int quarter, bool toNpf, List<long> npfIds, bool loadUnlinkedPP, bool isFormalizedName);

        [OperationContract]
        ReportFile CreateReportIPUKTotal(ReportIPUKPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportIPUK(ReportIPUKPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        List<ReportErrorTypesQueryItem> GetReportErrorTypeData(ReportErrorTypesPrompt prompt);

        [OperationContract]
        List<ReportErrorSpreadQueryItem> GetReportErrorNpfSpreadData(ReportErrorTypesPrompt prompt);

        [OperationContract]
        List<ReportErrorSpreadQueryItem> GetReportErrorRegionSpreadData(ReportErrorTypesPrompt prompt);

        [OperationContract]
        List<ReportErrorSpreadQueryItem> GetReportErrorMonthSpreadData(ReportErrorTypesPrompt prompt);

        [OperationContract]
        ReportFile CreateReportDeposits(ReportDepositsPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportBalanceUK(ReportBalanceUKPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportRSASCA(ReportRSASCAPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportF060(ReportF060Prompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportF070(ReportF070Prompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportProfitability(ReportProfitabilityPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportOwnedFounds(ReportOwnedFoundsPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportInvestRatio(ReportInvestRatioPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportUKAccounts(List<long> ukIds, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportUKList(DateTime reportDate, bool isShowOldNamesUk = false, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportSPN01(DateTime reportDate, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN02(DateTime startDate, DateTime endDate, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN03(DateTime startDate, DateTime endDate, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportSPN17(DateTime reportDate, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN18(DateTime startDate, DateTime endDate, bool showOldContracts, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN19(DateTime startDate, DateTime endDate, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN20(string year, int measure, out List<SPNReport20Item> items, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportSPN40(DateTime reportDate, ReportFile.Types type = ReportFile.Types.Excel);
        [OperationContract]
        ReportFile CreateReportSPN39(DateTime reportDate, ReportFile.Types type = ReportFile.Types.Excel);

        [OperationContract]
        ReportFile CreateReportAgrregateSPN(long[] npfIds, long[] opIds, bool addNpf, string direction, DateTime dateFrom, DateTime dateTo, bool isExtended, bool isGarant);
    }
}
