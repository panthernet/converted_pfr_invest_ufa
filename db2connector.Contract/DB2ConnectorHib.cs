﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataAccess.Server.DataObjects.Analyze;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;

namespace db2connector
{
    /// <summary>
    /// Часть интерфейса, отвечающая за работу с объектами из PFR_INVEST.DataObjects
    /// </summary>
    [ServiceKnownType("GetDataObjectKnownTypes", typeof(KnownTypeHelper))]
    public partial interface IDB2Connector
    {
        #region Общие операции DataAccess
        [OperationContract]
        void Delete(string p_type, object p_Object);
        [OperationContract]
        object Save(string p_type, object p_Object);

        /// <summary>
        /// Сохранения списка сущностей одного типа
        /// </summary>
        /// <param name="typeFullName">Полное имя типа</param>
        /// <param name="sList">Список</param>
        /// <param name="returnIds">ВОзвращать список идентификаторов для сущностей</param>
        [OperationContract]
        IEnumerable<object> BatchSave(string typeFullName, IEnumerable<object> sList, bool returnIds = false);
        /// <summary>
        /// Удаление списка сущностей одного типа
        /// </summary>
        /// <param name="typeFullName">Полное имя типа</param>
        /// <param name="sList">Список</param>
        [OperationContract]
        void BatchDelete(string typeFullName, List<object> sList);
        [OperationContract]
        object GetByID(string p_type, object p_iId);
        [OperationContract]
        IList GetList(string p_type);
        [OperationContract]
        IList GetListLimit(string p_type, int limit);
        [OperationContract]
        IList GetListPage(string p_type, int startindex, int limit);

        [OperationContract]
        IList GetListByProperty(string p_type, string propertyName, object value);

        [OperationContract]
        long GetListByPropertyCount(string p_type, string propertyName, object value);

        [OperationContract]
        IList GetListByPropertyCondition(string p_type, string propertyName, object value, string operation);

        [OperationContract]
        IList GetListByPropertyConditions(string p_type, List<ListPropertyCondition> propertyConditionList);

        [OperationContract]
        string GetXMLByPropertyConditions(string p_type, List<ListPropertyCondition> propertyConditionList);

        [OperationContract]
        string GetXMLByList(string p_type, IList list);

        [OperationContract]
        Dictionary<string, object> GetEntityDBProjection(string p_type, object source);

        [OperationContract]
        IList GetListByXML(string p_type, string xml);

        [OperationContract]
        long GetListByPropertyConditionsCount(string p_type, List<ListPropertyCondition> propertyConditionList);

        [OperationContract]
        object GetObjectProperty(string p_type, object p_iId, string p_sPropertyName);

        [OperationContract]
        long GetObjectPropertyCount(string p_type, object p_iId, string p_sPropertyName);
        #endregion

        [OperationContract]
        OpfrTransferListItem GetOpfrTransferListItem(long transferId);

        [OperationContract]
        object[] FillInOwnCapital(long bankId);

        [OperationContract]
        List<DepClaimOfferListItem> GetAcceptedDepClaimOffers();

        [OperationContract]
        void OpfrRegisterUpdateRequest(OpfrRegister register);
        [OperationContract]
        void UpdateReqTrAndContractByActSigned(long contractId, bool unlinked);

        [OperationContract]
        List<EDOLogListItem> GetSuccessLoadedODKByForm(DateTime? dateFrom, DateTime? dateTo, string form, string docRegNumberOut = null);


        [OperationContract]
        long GetTotalZLSummForSIRegister(long id);

        [OperationContract]
        string GePayAssignmentForNpf(string operation, long dirId);

        [OperationContract]
        List<PaymentAssignment> GetPayAssignmentsForNpf();


        [OperationContract]
        List<EdoOdkF140ListItem> GetEdoOdkF140ListItems(int? contractType = null);


        [OperationContract]
        SPNOperation GetAPSIOperationHib();

        [OperationContract]
        List<SPNOperation> GetSPNOperationsPFRtoUKListHib();

        [OperationContract]
        List<SPNOperation> GetSPNOperationsUKtoPFRListHib();

        [OperationContract]
        List<SPNOperation> GetSPNOperationsByTypeListHib(long directionID);

        [OperationContract]
        List<ReqTransfer> GetTransfersForSIRegisterHib(long registerID);

        [OperationContract]
        List<ReqTransfer> GetTransfersForSIRegistersHib(List<long> registerIDs);

        [OperationContract]
        List<SIRegister> GetSiRegistersByContractOperationHib(int typeContract, long? operationID);

        [OperationContract]
        List<SIRegister> GetSiRegistersByCondition(int typeContract, long directionID, long operationID, List<long> contractNamesIds, bool isArchive);

        [OperationContract]
        List<ExportReportVRListItem> GetDataReportVRApplication1Pfr(List<long> siregIDs);

        [OperationContract]
        List<IncomeSecurityListItem> GetIncomeSecurityList(long? recordID = null, string typeFilter = null, bool withPercents = true);

        [OperationContract]
        List<ZLMovementListItem> GetZLMovementsListHib();

        [OperationContract]
        List<ZLMovementListItem> GetZLMovementsListByContractTypeHib(int contractType);

        [OperationContract]
        List<RatesListItem> GetRatesListHib();

        [OperationContract]
        List<RatesListItem> GetMissedRatesListHib();





        [OperationContract]
        long GetRegistersListCountPeriod(bool is_archive, DateTime? start, DateTime? end);

        [OperationContract]
        long GetNPFTempAllocationCountPeriod(DateTime? start, DateTime? end);

        [OperationContract]
        List<RegistersListItem> GetRegistersListPeriodByPage(bool is_archive, int startIndex, DateTime? start, DateTime? end, long? finregisterID = null);

        [OperationContract]
        List<RegistersListItem> GetRegistersListPeriodByPageForRegister(bool is_archive, int startIndex, DateTime? start, DateTime? end, long? registerID = null);

        [OperationContract]
        List<RegistersListItem> GetNPFTempAllocationListPeriodByPage(int startIndex, DateTime? start, DateTime? end);

        [OperationContract]
        List<LegalEntity> GetUKListHib();

        [OperationContract]
        List<LegalEntity> GetActiveUKListByTypeContractHibForRegister(int contractType, long registerId, long? leId = null, long? contractId = null);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetAvailableContractsForUkTransfer(int contractType, long registerId, long leId, long? contractId = null);

        //[OperationContract]
        //List<LegalEntity> GetUKListByContractTypeHib(int contractType);

        [OperationContract]
        List<LegalEntity> GetValidUKListByMcProfitAbilityHib(int contractType, long yearId, long quarter);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetValidContractListByMcProfitAbilityHib(int contractType, long legalEntityId, long yearId, long quarter);

        [OperationContract]
        List<LegalEntity> GetActiveUKListHib();


        [OperationContract]
        List<LegalEntity> GetActiveUKListTransferHib(int contractType);

        [OperationContract]
        List<LegalEntity> GetActiveNpfListHib();

        [OperationContract]
        List<Attach> GetLinkedDocumentsListHib(long DocumentID);

        [OperationContract]
        IList<F050CBInfo> GetCBInfosListForF50(long EdoId);


        [OperationContract]
        List<TransferListItem> GetTransfersListForSPNMoveList(Document.Types contractType);

        [OperationContract]
        List<TransferListItem> GetTransfersListBYContractType(Document.Types contractType, bool isArchive, bool forAssignPayment, bool onlyActiveContract, long yearId = 0, DateTime? periodStart = null, DateTime? periodEnd = null, bool mergeReqTr = false);

        [OperationContract]
        List<TransferListItem> GetTransfersListBYContractTypeForUKArchive(Document.Types contractType, bool isArchive, bool forAssignPayment, bool onlyActiveContract);


        [OperationContract]
        List<TransferListItem> GetTransfersUkPlanListByRegId(Document.Types contractType, bool isArchive, bool forAssignPayment, long regID);

        [OperationContract]
        List<TransferListItem> GetTransfersUkPlanListByRegListId(
        Document.Types contractType, bool isArchive, bool forAssignPayment, string regIDs);

        [OperationContract]
        List<ReqTransferWithStatsListItem> GetReqTransfersWithStatsFromUkPlanId(long ukPlanId);

        [OperationContract]
        List<Contact> GetContactsForUKs();

        [OperationContract]
        List<Budget> GetBudgetsListHib();

        [OperationContract]
        List<Year> GetYearsListAvailableForBudget();

        [OperationContract]
        List<BudgetCorrection> GetCorrectionsForBudget(long ID);

        [OperationContract]
        List<DirectionTransferListItem> GetTransfersListForDirection(long id);

        [OperationContract]
        List<RejectApplication> GetRejectApplications();

        [OperationContract]
        IList<Contragent> GetNPFListHib();

        [OperationContract]
        IList<LegalEntity> GetNPFGarantedListLEHib(StatusFilter filter, long garantType);

        [OperationContract]
        IList<LegalEntity> GetNPFListLEHib(StatusFilter filter);

        [OperationContract]
        IList<Contragent> GetFilteredNPFContragentListHib(StatusFilter filter, long[] excludedStatuses, long ownLEID);

        [OperationContract]
        IList<LegalEntity> GetFilteredNPFListLEHib(StatusFilter filter, long[] excludedStatuses);

        [OperationContract]
        IList<LegalEntity> GetNPFListLEReorgHib();

        [OperationContract]
        long GetCountNPFListLEReorgHib();

        [OperationContract]
        IList<LegalEntity> GetNPFListLEReorgByPageHib(int startIndex);

        [OperationContract]
        IList<LegalEntity> GetNPFListLEAccHib(StatusFilter filter);

        [OperationContract]
        List<OldNPFName> GetOldNPFNamesSortedByDate(long legal_entity_ID);

        [OperationContract]
        List<OldSIName> GetOldSINamesSortedByDate(long legal_entity_ID);

        [OperationContract]
        List<OldSIName> GetOldSINamesForInvDecl(List<long> leIdList);

        [OperationContract]
        List<ContrInvest> GetAllContrInvestForInvDecl(List<long> contractIdList);

        [OperationContract]
        List<ContrInvestOBL> GetOBLContrInvestForInvDecl(List<long> ciIdList);

        [OperationContract]
        List<ContrInvestAKC> GetAKCContrInvestForInvDecl(List<long> ciIdList);

        [OperationContract]
        List<ContrInvestPAY> GetPAYContrInvestForInvDecl(List<long> ciIdList);

        [OperationContract]
        License GetCurrentLicense(long legal_entity_ID);

        [OperationContract]
        Account GetContragentAccountByMeasure(long contragent_ID, string measure);

        [OperationContract]
        Account GetContragentRoubleAccount(long contragent_ID);

        [OperationContract]
        List<Contragent> GetContragentsListHib(string type);

        [OperationContract]
        Account GetPFRAccountByMeasure(string measure);

        [OperationContract]
        Account GetPFRRoubleAccount();

        [OperationContract]
        List<SchilsCostsListItem> GetSchilsCostsList();

        [OperationContract]
        LegalEntity GetLegalEntityForContragent(long contragent_ID);

        [OperationContract]
        BankAccount GetCurrentBankAccount(long legal_entity_ID);

        [OperationContract]
        List<ERZLListItem> GetERZLListHib();

        [OperationContract]
        long GetCountUKTransfersList(bool PPEntered);


        [OperationContract]
        List<ReqTransfer> GetUKTransfersListByPage(bool PPEntered, int startIngex);

        [OperationContract]
        List<UKListItemHib> GetUKTransfersListHibByPage(bool PPEntered, int startIngex);

        [OperationContract]
        List<UKListItemHib> GetUKTransfersListHibByID(bool PPEntered, long id);


        [OperationContract]
        List<ReqTransfer> GetUKTransfersList(bool PPEntered);


        [OperationContract]
        long GetCountUKTransfersListArchiveFiltered(DateTime? from, DateTime? to);

        [OperationContract]
        List<ReqTransfer> GetUKTransfersListArchiveFilteredByPage(DateTime? from, DateTime? to, int startIngex);

        [OperationContract]
        List<UKListItemHib> GetUKTransfersListHibByPageArchiveFiltered(DateTime? from, DateTime? to, int startIngex, long? recordID = null);

        [OperationContract]
        long GetCountTransferToNPFArchiveList();

        [OperationContract]
        List<PPTransferListItem> GetUKPPTransfersByTransferID(long transferID);

        [OperationContract]
        long GetCountTransferToNPFArchiveListFiltered(DateTime? from, DateTime? to);

        [OperationContract]
        List<TransferFromOrToNPFListItem> GetTransferToNPFArchiveListFilteredByPage(int startIngex, DateTime? from, DateTime? to);


        [OperationContract]
        List<Year> GetValidYearListForYearPlanCreation(Document.Types contractType);

        [OperationContract]
        List<Year> GetYearListCreateSIUKPlanByOperationType(Document.Types contractType, long operationTypeId, bool includeArchive = true);

        [OperationContract]
        List<Year> GetYearListCreateSIUKPlan(Document.Types contractType, long? operationID);

        [OperationContract]
        List<Year> GetYearListForYearPlanCreation(Document.Types contractType, long operationTypeId);

        [OperationContract]
        List<Year> GetValidYearListForYearPlanCorrection(Document.Types contractType, long operationTypeId);

        [OperationContract]
        List<Month> GetValidMonthListForYearPlanCorrection(Document.Types contractType, long p_YearID, long operationTypeId);

        //[OperationContract]
        //List<Month> GetMonthListCreateSIUKPlan(Document.Types contractType, long companyYearID, long operationID);

        [OperationContract]
        List<Month> GetMonthListCreateSIUKPlanByOperationType(Document.Types contractType, long companyYearID, long operationTypeID, long operationContentID);

        [OperationContract]
        long SaveTransferWizardResult(SIRegister register, List<SITransfer> listSITransfers, List<ReqTransfer> listReqTransfers, bool isFinVypl, long yearId, long monthId, bool createSITransfers);

        [OperationContract]
        bool IsValidYearForPrintMonthPlan(Document.Types contractType, long p_YearID);

        [OperationContract]
        List<YearPlanListItem> GetUKPaymentPlansForYearPlan(long p_regID);

        [OperationContract]
        List<LegalEntity> GetAvailableUkListForYearPlan(long p_regID, int contractType, bool isNew);

        [OperationContract]
        long GetZLCountForDate(long CotractID, DateTime date);

        /* начало РОПС, АСВ */
        [OperationContract]
        decimal GetSCHAForContractPeriod(long contrID, DateTime start, DateTime end);
        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetSIContractsForPeriod(DateTime start, DateTime end);
        /* конец РОПС, АСВ */
        [OperationContract]
        long IsEnoughZLForTransferWizardOnDate(Dictionary<long, int> contractData, DateTime date);

        [OperationContract]
        bool DeleteYearPlan(long registerId, bool undelete);

        [OperationContract]
        List<SIUKPlan> GetUKPlansForYearMonthAndOperation(Document.Types contractType, long p_MonthID, long p_YearID, long operationId, bool forPrint, bool isOperationTypeIdSupplied = false);

        [OperationContract]
        List<SIUKPlan> GetUKPlansForYearMonthAndOperationForMonthTransferWizard(Document.Types contractType, long p_MonthID, long p_YearID, long operationTypeId, long operationId);

        //[OperationContract]
        //List<SIUKPlan> GetUKPlansByTransferId(long transferId);

        [OperationContract]
        List<SITransfer> GetUKPlansForYearAndOperation(Document.Types contractType, long p_YearID, long operationId);

        [OperationContract]
        List<SITransfer> GetUKPlansForYearAndOperationType(Document.Types contractType, long p_YearID, long operationTypeIdukId, long ukId = 0);

        /// <summary>
        /// Для писем о выплатах СИ/ВР
        /// </summary>
        [OperationContract]
        Dictionary<long, string> GetUKListForYearAndOperationType(Document.Types contractType, long pYearID, long operationTypeId);


        [OperationContract]
        List<UKPaymentListItem> GetUKPaymentsForPlan(long p_PlanID);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsForUKs();

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsForUKsByType(int contractType, bool showDissolved = true);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsWithoutRegister(int contractType, long registerOperationID, long registerDirectionID, DateTime registerDate);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsForUKsByTypeAndName(int contractType, int[] contractNames, bool showDissolved = true);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsByType(int contractType);

        [OperationContract]
        PFR_INVEST.DataObjects.Contract GetContractByDogovorNum(string dogovorNum);

        [OperationContract]
        List<EdoOdkF140ListItem> GetReportViolationsByType(int contractType);

        [OperationContract]
        List<DeadZLListItem> GetDeadZLListHib();

        [OperationContract]
        List<ERZLNotify> GetERZLNotifiesHib(long erzl_id, string notify_num);

        [OperationContract]
        List<ERZLNotifiesListItem> GetERZLNotifiesListByERZL_ID(long erzl_id);

        [OperationContract]
        bool SaveERZLNotifiesList(List<ERZLNotify> list);

        [OperationContract]
        bool EraseERZLNotifiesList(List<ERZLNotify> list);

        [OperationContract]
        long? GetReverseNotifyCount(ERZLNotify notify);

        [OperationContract]
        long? GetInputZLCountByNPFAccountID(long npf_account_id);

        [OperationContract]
        long? GetOutputZLCountByNPFAccountID(long npf_account_id);

        [OperationContract]
        List<NPFforERZLNotifyListItem> GetNPFforERZLNotifyListHib(long erzl_id, string notifynum, bool fromNPF);

        [OperationContract]
        /*bool*/
        Dictionary<bool, long> GenerateRegisterByERZL(long erzl_id);

        [OperationContract]
        List<FinregisterWithCorrAndNPFListItem> GetFinregisterWithCorrAndNPFList(long? register_id);

        [OperationContract]
        List<FinregisterWithCorrAndNPFListItem> GetFinregisterWithCorrAndNPFListByPortfolio(long? portfolio_id);

        [OperationContract]
        long GetOrderCounterForPortfolio(long PortfolioYearID, string OrderType);

        [OperationContract]
        long GetOrderCounterForPortfolioYear(long? PortfolioYearID, string OrderType);

        [OperationContract]
        List<OrdersListItem> GetOrdersListHib();

        [OperationContract]
        long GetCountOrdersListHib();

        [OperationContract]
        List<OrdersListItem> GetOrdersListByPageHib(int startIndex);

        [OperationContract]
        List<CbInOrderListItem> GetCbInOrderList(long? order_id);

        [OperationContract]
        List<SecuritiesListItem> GetSecuritiesListHib(long? currency_id);

        ///<summary>
        ///Возвращает список ЦБ, доступных для добавления в поручение на покупку
        ///</summary>
        ///<param name="p_iSelectedOrderCurrencyID">ID валюты счета ДЕПО поручения на покупку</param>
        ///<param name="p_OrderTerm">дата "исполнить по" поручения на покупку</param>
        [OperationContract]
        List<SecurityAvailableForOrderListItem> GetSecuritiesAvailableForBuyOrder(long p_iSelectedOrderCurrencyID, DateTime p_OrderTerm);

        ///<summary>
        ///Возвращает список ЦБ, доступных для добавления в поручение на продажу
        ///</summary>
        ///<param name="p_iSelectedOrderCurrencyID">ID валюты счета ДЕПО поручения на продажу</param>
        ///<param name="p_iPortfolioID)">ID портфеля, бумаги из которого продаются</param>
        [OperationContract]
        List<SecurityAvailableForOrderListItem> GetSecuritiesAvailableForSaleOrder(long p_iSelectedOrderCurrencyID, long p_iPortfolioID);

        ///<summary>
        ///Возвращает список умеших ЗЛ во всех активных НПФ за указаный день.
        ///</summary>
        [OperationContract]
        List<NPFforERZLNotifyListItem> GetDeadZLForAllNPF(DateTime date);

        ///<summary>
        ///Возвращает список ЦБ, доступных для добавления в поручение на покупку рублевых бумаг с аукциона
        ///</summary>
        /// <param name="p_iSecurityID">ID ценной бумаги уже добавленной в поручение</param>
        [OperationContract]
        SecurityAvailableForOrderListItem GetSecurityAvailableForAuctionOrder(long p_iSecurityID);

        [OperationContract]
        ///<summary>
        ///Возвращает список ЦБ, добавленных в поручение (повторные ЦБ исключаются) 
        ///</summary>
        List<PFR_INVEST.DataObjects.Security> GetSecuritiesListForOrderHib(long? order_id);

        [OperationContract]
        decimal? GetRateForDateHib(long? currency_id, DateTime? date);

        [OperationContract]
        List<Payment> GetPaymentsListHib(long? security_id);

        [OperationContract]
        List<OrdReport> GetOrdReportsBySecurityPortfolioAndType(long security_id, long portfolio_id, string type);

        [OperationContract]
        List<BuyOrSaleReportsListItem> GetBuyOrSaleReportsListHib(bool forSale, long? orderID);

        [OperationContract]
        long GetCountBuyOrSaleReportsListHib(bool forSale, long? orderID);

        [OperationContract]
        List<BuyOrSaleReportsListItem> GetBuyOrSaleReportsListByPageHib(bool forSale, long? orderID, int startIndex);

        [OperationContract]
        long GetCountBuyOrSaleReportsListFilteredHib(bool forSale, long? orderID, DateTime? from, DateTime? to);

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocationBuyOrSaleListFilteredByPageHib(bool forSale, long? orderID, int startIndex, DateTime? from, DateTime? to);

        //[OperationContract]
        //List<BuyOrSaleReportsListItem> GetBuyOrSaleReportsListHibPrevious(bool forSale, long? OrderID);

        [OperationContract]
        long[] GetOrdReportsIDsForRecalc();

        [OperationContract]
        void RecalcNKDForReport(long ReportID);

        [OperationContract]
        List<AllocationRequestListItem> GetAllocationRequestListHib();

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocationListHib();


        [OperationContract]
        List<TempAllocationListItem> GetTempAllocationSellPaperListByPageHib(int startIndex);

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocationBuyPaperListByPageHib(int startIndex);

        [OperationContract]
        long GetCountTempAllocateDepositListHib();

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateDepositListByPageHib(int startIndex);

        [OperationContract]
        long GetCountTempAllocateDepositListFilteredHib(DateTime? from, DateTime? to);

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateDepositListFilteredByPageHib(int startIndex, DateTime? from, DateTime? to);


        [OperationContract]
        long GetCountTempAllocateReturnDepositListHib();

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateReturnDepositListHib(int startIndex);

        [OperationContract]
        long GetCountTempAllocateReturnDepositListFilteredHib(DateTime? from, DateTime? to);

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateReturnDepositListFilteredHib(int startIndex, DateTime? from, DateTime? to);

        [OperationContract]
        long GetCountTempAllocateBalanceExtraListFilteredHib(bool isAllocate, DateTime? from, DateTime? to);

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateBalanceExtraListFilteredHib(bool isAllocate, int startIndex, DateTime? from, DateTime? to);

        [OperationContract]
        long GetCountTempAllocateBalanceExtraListHib();

        [OperationContract]
        List<TempAllocationListItem> GetTempAllocateBalanceExtraListHib(int startIndex);

        [OperationContract]
        DepositInfo GetDepositInfoHib(long depositId);

        [OperationContract]
        List<LegalEntity> GetBanksListHib();

        [OperationContract]
        List<PfrBankAccount> GetPFRBankAccounts(long p_bank_agent_id, long p_portfolio_id, string p_type, bool includeDeleted);

        [OperationContract]
        List<NPFforERZLNotifyListItem> GetNPF2NPFListHib(long erzl_id, string notifynum, long? mainNPFAcc_id, bool fromNPF);

        [OperationContract]
        List<CorrespondenceListItemNew> GetSIDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null);

        [OperationContract]
        List<CorrespondenceListItemNew> GetNpfDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null);

        [OperationContract]
        List<CorrespondenceListItemNew> GetVRDocumentsListHib(Document.Statuses status, long? documentID = null, long? attachID = null);

        [OperationContract]
        List<Document> GetDocumentsBypropertiesListHib(Document.Types type, string incomingNumber, DateTime? dateTime);

        [OperationContract]
        DocFileBody GetDocFileBodyForDocument(long lID);

        [OperationContract]
        long SaveDocFileBodyForDocument(long lID, DocFileBody file, byte[] body);

        [OperationContract]
        long SaveFileContent(long? fileContentId, byte[] data);


        [OperationContract]
        long GetF050CBInfoCount(int contractType, int year = 0);

        [OperationContract]
        IList<F050CBInfoListItem> GetF050CBInfoByPage(int contractType, int startIndex, int year = 0);

        [OperationContract]
        IList<F050CBInfoListItemLite> GetF050CBInfoByPageLite(int contractType, int startIndex, int year = 0);

        [OperationContract]
        long GetF050ReportCount(int contractType, int year = 0);

        [OperationContract]
        IList<F050ReportListItem> GetF050ReportByPage(int contractType, int startIndex, int year = 0);


        [OperationContract]
        long GetF040DetailsCount(int contractType);

        [OperationContract]
        IList<F040DetailsListItem> GetF040DetailsByPage(int contractType, int startIndex);

        [OperationContract]
        IList<F040DetailsListItemLite> GetF040DetailsByPageLite(int contractType, int startIndex);

        [OperationContract]
        IList<F040Detail> GetF040Details(long EdoId);

        [OperationContract]
        IList<F040Rrz> GetF040Rrzs(long EdoId);

        #region EdoOdkF010 and EdoOdkF015

        [OperationContract]
        long GetCountNetWealthsF010();

        [OperationContract]
        long GetCountNetWealthsSumF010(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF010List();

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF010ListByOnlyPage(int startIndex);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsSumF010ListByOnlyPage(int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);


        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF010ListByTypeContract(int contractType);

        [OperationContract]
        long GetCountNetWealthsF010ListByTypeContract(int contractType, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF010ListByPage(int contractType, int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF010ListNullContract();

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF015List();

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF015ListByTypeContract(int contractType);

        [OperationContract]
        long GetCountNetWealthsF015ListByTypeContract(int contractType, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF015ListByPage(int contractType, int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<int> GetNetWealthsF010YearsList(int contractType);

        [OperationContract]
        IList<int> GetNetWealthsF015YearsList(int contractType);

        #endregion

        #region EdoOdkF012, EdoOdkF014 , EdoOdkF016

        [OperationContract]
        long GetCountNetWealthsF012(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF012List();

        [OperationContract]
        long GetCountNetWealthsSumF012(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsSumF012ListByPage(int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF012ListByPage(int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        [Obsolete("Устаревший, использовать GetNetWealthsF015ListByTypeContract", true)]
        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF012ListByTypeContract(int contractType);

        [OperationContract]
        long GetCountNetWealthsF014(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF014List();

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF014ListByPage(int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        long GetCountNetWealthsF016(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF016List();

        [OperationContract]
        IList<NetWealthListItem> GetNetWealthsF016ListByPage(int startIndex, DateTime? periodStart = null, DateTime? periodEnd = null);

        #endregion

        #region EdoOdkF022
        [Obsolete("Устаревший, использовать GetMarketCostF025List", false)]
        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF022List();
        [Obsolete("Устаревший, использовать GetMarketCostF025ListByTypeContract", false)]
        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF022ListByTypeContract(int contractType);
        [Obsolete("Устаревший, использовать GetMarketCostF025ListByIds", false)]
        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF022ListByIds(IList<long> Ids);
        #endregion

        #region EdoOdkF020 EdoOdkF025

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF020List();

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF020ListByTypeContract(int contractType);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF020ListNullContract(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF025ListNullContract(DateTime? periodStart = null, DateTime? periodEnd = null);



        [OperationContract]
        long GetCountMarketCostF020(int contractType);

        [OperationContract]
        long GetCountMarketCostF025(int contractType);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF020ListByPage(int contractType, int startIndex);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF025ListByPage(int contractType, int startIndex);

        [OperationContract]
        long GetCountMarketCostF020Filtered(int contractType, DateTime? from, DateTime? to);

        [OperationContract]
        long GetCountMarketCostF025Filtered(int contractType, DateTime? from, DateTime? to);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF020ListFilteredByPage(int contractType, int startIndex, DateTime? from, DateTime? to);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF025ListFilteredByPage(int contractType, int startIndex, DateTime? from, DateTime? to);

        #endregion


        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF024List(DateTime? periodStart = null, DateTime? periodEnd = null);

        [OperationContract]
        IList<MarketCostListItem> GetMarketCostF026List(DateTime? periodStart = null, DateTime? periodEnd = null);


        [OperationContract]
        List<F040DetailsListItem> GetF040List();

        [OperationContract]
        List<F040DetailsListItem> GetF040ListByTypeContract(int contractType);


        [OperationContract]
        List<F060DetailsListItem> GetF60ListHib();

        [OperationContract]
        List<F060DetailsListItem> GetF60ListByTypeContractHib(int contractType);

        [OperationContract]
        List<F070DetailsListItem> GetF70ListHib();

        [OperationContract]
        long GetCountF70ListByTypeContractHib(int contractType);

        [OperationContract]
        List<F070DetailsListItem> GetF70ListByTypeContractHib(int contractType);

        [OperationContract]
        List<F070DetailsListItem> GetF70ListByTypeContractPageHib(int contractType, int startIndex);

        [OperationContract]
        List<F080DetailsListItem> GetF80ListHib();

        [OperationContract]
        List<F080DetailsListItem> GetF80ListByContractTypeHib(int contractType);

        /// <summary>
        /// Возвращает простые банки (TypeName = банк)
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        IList<BankConclusion> GetBankConclusionList();

        [OperationContract]
        IList<LegalEntity> GetBankListForDeposit(long? stockId = null);

        [OperationContract]
        IList<BankItem> GetBankItemListForDeposit();

        [OperationContract]
        IList<LegalEntity> GetFullBankListForDeposit();

        [OperationContract]
        IList<LegalEntity> GetBankAgentListForDeposit();

        [OperationContract]
        IList<LegalEntity> GetFullBankAgentListForDeposit();

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsListForLegalEntityHib(long leID);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsListForLegalEntityHibByContractType(long leID, Document.Types contractType);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetContractsListForInvDecl(List<long> idList, Document.Types contractType, bool showEnded);

        [OperationContract]
        List<SIListItem> GetSIListHibForInvDecl(bool withContacts, bool forSI);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Contract> GetGukConractsByContractName(String contractName);


        [OperationContract]
        List<SIContractListItem> GetSIContractsListHib();

        [OperationContract]
        List<SIContractListItem> GetVRContractsListHib();

        [OperationContract]
        IList<AddSPN> GetBaseDocumentRecordsNum(long PortfolioID, long PeriodID);

        [OperationContract]
        List<Month> GetValidMothsListForAddPSNWithPortfolioIDAndMonth(long? pID, long? mID);

        [OperationContract]
        IList<Curs> GetCurrentCursesHib();

        [OperationContract]
        IList<Curs> GetOnDateCurrentCursesHib(DateTime thisDate);

        [OperationContract]
        IList<Curs> GetOnDateCurrentCursesHibForLine30(DateTime thisDate, int cursType);

        [OperationContract]
        List<SIListItem> GetSIListHib(bool withContacts);

        [OperationContract]
        IList<BankAccount> GetOldBankAccounts(long legalEnrityId);

        [OperationContract]
        IList<BankAccount> GetBankAccountListByTypeHib(string[] type);

        [OperationContract]
        IList<Rating> GetRatingsListHib();

        [OperationContract]
        IList<AgencyRatingListItem> GetAgencyRatingsListHib();

        [OperationContract]
        IList<MultiplierRating> GetMultiplierRatingByLegalEntityID(long ID);

        /// <summary>
        /// Спиоск портфелей у которых есть счёт
        /// </summary>
        /// <param name="type">Тип портфеля, при null возвращает все типы</param>		
        /// <returns></returns>
        [OperationContract]
        IList<PortfolioFullListItem> GetPortfoliosByType(Portfolio.Types[] types = null, bool exclude = false);

        /// <summary>
        /// Спиоск всех портфелей
        /// </summary>
        [OperationContract]
        IList<PortfolioFullListItem> GetFullPortfoliosListByType(Portfolio.Types? type = null);

        [OperationContract]
        IList<PortfolioFullListItem> GetPortfoliosFiltered(PortfolioIdentifier.PortfolioPBAParams p);

        [OperationContract]
        PortfolioFullListItem GetPortfolioFull(long pfrBankAccId, long portfolioId);

        [OperationContract]
        PortfolioFullListItem GetPortfolioFullByPfBaccID(long portfolioPfrBankAccountID);

        [OperationContract]
        PortfolioFullListItem GetPortfolioFullForBank(long portfolioId, long bankId, long accId = 0);

        [OperationContract]
        long GetCountAddSPNFullList(PortfolioTypeFilter filter);

        [OperationContract]
        IList<AddSPN> GetAddSPNFullListByPage(PortfolioTypeFilter filter, int startIndex);

        [OperationContract]
        IList<AddSPN> GetAddSPNFullListByID(PortfolioTypeFilter filter, long id);



        [OperationContract]
        long GetCountDopSPNFullList(PortfolioTypeFilter filter);

        [OperationContract]
        IList<DopSPN> GetDopSPNFullListByPage(PortfolioTypeFilter filter, int startIndex);

        [OperationContract]
        IList<DopSPN> GetDopSPNFullListByID(PortfolioTypeFilter filter, long id);


        [OperationContract]
        IList<DopSPN> GetDopSPNList(long pfrBankAccId, long portfolioId);

        [OperationContract]
        IList<AddSPN> GetAddSPNList(long pfrBankAccId, long portfolioId);

        [OperationContract]
        long GetCountApsFullList(PortfolioTypeFilter filter);

        [OperationContract]
        IList<Aps> GetApsFullListByPage(PortfolioTypeFilter filter, int startIndex);

        [OperationContract]
        IList<Aps> GetApsFullListByID(PortfolioTypeFilter filter, long id);



        [OperationContract]
        long GetCountDopApsFullList(PortfolioTypeFilter filter);

        [OperationContract]
        IList<DopAps> GetDopApsFullListByPage(PortfolioTypeFilter filter, int startIndex);

        [OperationContract]
        IList<DopAps> GetDopApsFullListByID(PortfolioTypeFilter filter, long ID);



        [OperationContract]
        decimal GetAddSPNSumByPeriod(PortfolioTypeFilter filter, DateTime targetDate);

        [OperationContract]
        WebServiceDataError RestoreDocument(long delId);

        [OperationContract]
        void SaveDeleteEntry(string doc_type, long? doc_id, int op_type, DeletedDataItem data = null);
        [OperationContract]
        void SaveDeleteEntryLog(string doc_type, string doc_caption, long? doc_id, int op_type, DeletedDataItem data = null);

        [OperationContract]
        IList<DeleteDocumentEntry> GetDeletedDocumentsList();

        [OperationContract]
        PFR_INVEST.DataObjects.Contract GetSDContractOnDate(DateTime date);

        [OperationContract]
        List<PFR_INVEST.DataObjects.Security> GetAllSecuritiesListHib();

        [OperationContract]
        List<Currency> GetCurrencyListHib();

        [OperationContract]
        List<AccBankType> GetAccBankTypes();

        [OperationContract]
        List<PFRContact> GetPFRContactsListHib(long id);

        [OperationContract]
        PFRContact GetPFRContactHib(long id);

        [OperationContract]
        List<FEDO> GetFedoListHib();

        [OperationContract]
        FEDO GetFedoHib(long id);

        [OperationContract]
        ERZLNotify GetERZLNotifyHib(long id);

        [OperationContract]
        List<ReqTransfer> GetTransfersListBySITR(long id);

        [OperationContract]
        List<SITransfer> GetSITransfersByRegister(long id);

        [OperationContract]
        List<SIUKPlan> GetSIUKPlansByReqListID(long id);

        [OperationContract]
        List<SIUKPlan> GetSIUKPlansByReqID(long id);

        [OperationContract]
        long ImportBDate(IList<BDate> list);

        [OperationContract]
        long ImportTDate(IList<TDate> list);

        [OperationContract]
        long ImportINDate(IList<INDate> list);

        [OperationContract]
        List<TemplateListItem> GetTemplatesList();

        [OperationContract]
        Template GetTemplateByName(string templateFileName);

        [OperationContract]
        List<PortfolioStatusListItem> GetPortfolioStatusListHib();

        [OperationContract]
        List<PortfolioPFRBankAccountListItem> GetAccountsForPorPortfolio(long PortfolioID);

        [OperationContract]
        List<OfferPfrBankAccountSum> GetOfferPfrBankAccountSumForOffer(long offerId);

        [OperationContract]
        IList<OfferPfrBankAccountSumListItem> GetOfferPfrBankAccountSumForOffers(long[] offerIds);

        [OperationContract]
        ActionResult GenerateDeposits(List<OfferPfrBankAccountSum> offerPfrBankAccountSums);


        [OperationContract]
        List<long> GetFailedDepClaimOffersAucIds();

        [OperationContract]
        List<DepClaimOfferListItem> GetDepClaimOfferListBySettleDate(ICollection<DepClaimOffer.Statuses> statuses,
                                                               DateTime date);
        [OperationContract]
        DepClaimOfferListItem GetDepClaimOfferById(long id);

        [OperationContract]
        List<PortfolioPFRBankAccountListItem> GetAccountsForPorPortfolioByIDs(long[] IDs);

        [OperationContract]
        List<DopSPNListItem> GetDopSPNListForDue(long AddSPNID);



        [OperationContract]
        bool IsDateClosedByKDoc(long PortfolioID, DateTime Date);

        [OperationContract]
        bool IsAddSPNClosedByKDoc(long AddSPNID);

        [OperationContract]
        bool IsKDocClosed(long CheckedKDocID);

        [OperationContract]
        ActionResult CheckNewKDoc(KDoc doc);

        [OperationContract]
        List<EDOLogListItem> GetLoadedODKLogList(bool success, DateTime? dateFrom, DateTime? dateTo, string form);

        [OperationContract]
        List<EDOLogListItem> GetLoadedSuccessODKLogListByTypeContract(DateTime? dateFrom, DateTime? dateTo, string form, int contractType);

        [OperationContract]
        string GetRegNumEDOLog();

        [OperationContract]
        ActionResult ImportUKF015(EdoOdkF015 import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF016(EdoOdkF016 import, EDOLog edoLog, string xmlBody);


        [OperationContract]
        ActionResult ImportUKF025(EdoOdkF025Hib import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF026(EdoOdkF026Hib import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF040(EdoOdkF040Hib import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF050(EdoOdkF050Hib import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF060(EdoOdkF060 import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF070(EdoOdkF070 import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF080(EdoOdkF080Hib import, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF140(EdoOdkF140 import, EDOLog edoLog, string xmlBody, int reportTypeID);

        [OperationContract]
        ActionResult ImportUKF401(EdoOdkF401402 requirement, List<F401402UK> details, EDOLog edoLog, string xmlBody);

        [OperationContract]
        ActionResult ImportUKF402(List<F401402UK> details, EDOLog edoLog, string xmlBody);


        //[OperationContract(AsyncPattern = true)]
        //IAsyncResult BeginImportUK026(EdoOdkF026Hib import, AsyncCallback callback, object state);


        //ActionResult EndImportUK026(IAsyncResult result);


        [OperationContract]
        long GetCountF401402Filtered(Document.Types contractType, DateTime? from, DateTime? to);

        [OperationContract]
        List<F401402UKListItem> GetF401402ListFilteredByPage(Document.Types contractType, int startIndex, DateTime? from, DateTime? to);

        [OperationContract]
        long GetCountF401402(Document.Types contractType);

        [OperationContract]
        List<F401402UKListItem> GetF401402ListByPage(Document.Types contractType, int startIndex);

        [OperationContract]
        long? GetOriginalXmlIDForEdoOdkF401402(long edoOdkF401402Id, string regNumberOut);

        [OperationContract]
        List<InsuranceListItem> GetInsuranceList(bool archive);

        [OperationContract]
        List<InsuranceListItem> GetInsuranceListByType(bool archive, int type);


        [OperationContract]
        List<McProfitAbility> GetMCProfitHistoryList(long YearID, long QuarterID, long ContractID, long MCID);

        [OperationContract]
        List<McProfitAbility> GetMcProfitAbilityListHib();

        [OperationContract]
        List<McProfitAbility> GetMcProfitAbilityListHibByContractType(int contractType);



        [OperationContract]
        int DeleteMcProfit(long YearID, long QuarterID, long ContractID);

        [OperationContract]
        void DeleteDepositPayment(long paymentID);

        [OperationContract]
        List<DepositListItem> GetDepositsListByPropertyHib(string PropertyName, object status);
        [OperationContract]
        List<DepositListItem> GetDepositsListByPropertyHib2(string PropertyName, object status);

        [OperationContract]
        List<DepositListItem> GetDepositsListByPortfolioHib();

        [OperationContract]
        //IList<DepositReturnListItem> GetDepositReturnListForQuarter(long pid, int year, int quarter);
        decimal GetReturnedDepSumForPortfolio(long pid, int year, int quartal);


        [OperationContract]
        List<OwnedFounds> GetOwnedFoundsHistoryList(long YearID, long QuartalID, long LegalEntityID, long OFID);

        [OperationContract]
        List<OwnFundsListItem> GetOwnedFoundsListHib();

        [OperationContract]
        int DeleteOwnedFounds(long YearID, long QuartalID, long LegalEntityID);

        [OperationContract]
        List<RatingAgency> GetRatingAgenciesList();

        [OperationContract]
        List<DepClaimSelectParams> GetDepClaimSelectParamsList();

        [OperationContract]
        int GetAuctionNextLocalNum(DateTime auctionDate);

        [OperationContract]
        List<DepClaimMaxListItem> GetDepClaimMaxList(int type);

        [OperationContract]
        List<Person> GetPersonListHib(long id);

        [OperationContract]
        Person GetPersonHib(long id);

        [OperationContract]
        ActionResult AuctionImportDepClaim2(long auctionID, IList<DepClaim2> claims, bool forceUpdate);

        [OperationContract]
        ActionResult AuctionImportDepClaim2Confirm(long auctionID, IList<DepClaim2> claims, bool forceUpdate);

        [OperationContract]
        ActionResult DepClaimOfferCreate(long auctionID);

        [OperationContract]
        List<DepClaimOfferListItem> GetDepClaimOfferList();

        //[OperationContract]
        //List<AuctionRepositoryLinkItem> GetLastRepositoryLinks();

        [OperationContract]
        List<DepClaimOfferListItem> GetDepClaimOfferListFiltered(long auctionID, ICollection<DepClaimOffer.Statuses> statuses);

        [OperationContract]
        List<DepClaimOfferListItem> GetDepClaimOfferListByAuctionId(long auctionID);

        [OperationContract]
        ActionResult CheckLicenseUnique(long bankID, string licenseNumber, string caType);


        [OperationContract]
        bool ImportKO761(long importId, DateTime importDate, IList<BankConclusion> conclusions, bool forceUpdate, long elId);

        [OperationContract]
        bool ImportOwnCapital(DateTime reportDate, IList<OwnCapitalHistory> items, bool forceUpdate);

        [OperationContract]
        PfrBranchReportImportResponse ImportPfrBranchReport(PfrBranchReportImportRequest parameters);

        [OperationContract]
        ActionResult SaveNPFContacts(long npfID, IList<LegalEntityCourier> contacts);

        [OperationContract]
        ActionResult SaveNPFIdentifiers(long npfID, IList<LegalEntityIdentifier> identifiers);

        [Obsolete("Устаревший, использовать GetExistingIdentifiers")]
        [OperationContract]
        IList<LegalEntityIdentifier> GetExistingNPFIdentifiers();

        [OperationContract]
        IList<LegalEntityIdentifier> GetNPFIdentifiers(long npfID);

        [Obsolete("Устаревший, использовать GetExistingIdentifiers")]
        [OperationContract]
        IList<LegalEntityIdentifier> GetExistingUKIdentifiers();

        [OperationContract]
        List<LegalEntityIdentifier> GetUKIdentifiers(long ukID);

        [OperationContract]
        IList<LegalEntityIdentifier> GetExistingIdentifiers();

        [OperationContract]
        List<PfrBranchReportInsuredPerson> GetPfrReportInsuredList(int year, int? month, long typeId);

        [OperationContract]
        List<PfrBranchReportInsuredPersonListItem> GetPfrReportInsuredPersonList();

        [OperationContract]
        List<PfrBranchReportNpfNoticeListItem> GetPfrReportNpfNoticeList();

        [OperationContract]
        List<PfrBranchReportAssigneePaymentListItem> GetPfrReportAssigneePaymentList();

        [OperationContract]
        List<PfrBranchReportCashExpendituresListItem> GetPfrReportCashExpendituresList();

        [OperationContract]
        List<PfrBranchReportApplication741ListItem> GetPfrReportApplication741List();

        [OperationContract]
        List<PfrBranchReportDeliveryCostListItem> GetPfrReportDeliveryCostList();

        [OperationContract]
        List<CBPaymentDetailListItem> GetCBPaymentDetailList();

        [OperationContract]
        List<PfrBranchReport> GetPfrBranchReportsByTypeAndPeriod(int year, int? month, long typeId);

        [OperationContract]
        List<Element> GetElementByType(Element.Types type);

        [OperationContract]
        IList<SimpleObjectValue> GetActiveLegalEntityByType(Contragent.Types type);

        /// <summary>
        /// Обновление статуса аукциона после изменений статусов оферт
        /// </summary>
        /// <param name="auctionId"></param>
        [OperationContract]
        void UpdateAuctionStatus(long auctionId);

        [OperationContract]
        bool IsOfferNumberUsed(int auctionYear, long offerId, long offerNum);

        [OperationContract]
        int GetDepClaim2Count(long auctionID, bool withConfirmationOnly);

        [OperationContract]
        IList<PaymentHistory> GetPaymentHistoryList(long[] depclaimOfferIds);


        [OperationContract]
        IList<DepositReturnListItem> GetDepositReturnList(DateTime? date);

        [OperationContract]
        IList<BankDepositInfoItem> GetBankDepositInfoList(long legalEntityID);

        [OperationContract]
        IList<BankDepositInfoItem> GetBankDepositInfoListByBankIds(long[] legalEntityIDs);

        [OperationContract]
        string[] GetBankDepositInfoQuery(long[] legalEntityIDs);

        [OperationContract]
        RepositoryImpExpFile GetLastBankDepositImpExpData(DateTime auctionDate, int auctionLocalNum, int key, int status, int impExp);

        [OperationContract]
        IList<BlUserSetting> GetBlUserSettings();

        [OperationContract]
        void SaveBlUserSetting(BlUserSetting setting);

        [OperationContract]
        IList<LoginMessageItem> GetLoginMessageItems();

        [OperationContract]
        void SetIgnoredLoginMessages(IList<string> mesagesKeys);

        [OperationContract]
        LoginMessageItem GetROPSLimitMessage(bool ignoreHidden = false, bool updateNow = false);

        [OperationContract]
        bool CheckSIRegisterDuplicate(SIRegister register);


        [OperationContract]
        long SaveKDoc(KDoc doc);

        [OperationContract]
        void DeleteKDoc(KDoc doc);

        [OperationContract]
        CommonRegisterListItem GetCommonRegister(AsgFinTr.Sections? section, long id);

        [OperationContract]
        List<CommonRegisterListItem> GetCommonRegisterList(AsgFinTr.Sections section, long direction, string content = null);

        [OperationContract]
        List<CommonRegisterListItem> GetCommonRegisterListForPP(AsgFinTr.Sections section, long direction);

        [OperationContract]
        List<FinregisterListItem> GetFinregisterForCommonPP(long registerID, bool onlyValidNames = false);

        /// <summary>
        /// Получить перечисления СИ / ВР
        /// </summary>
        /// <param name="registerID">ID реестра</param>
        /// <param name="type">Тип: СИ/ВР</param>
        [OperationContract]
        List<ReqTransferListItem> GetReqTransferForCommonPP(long registerID, Document.Types type);

        [OperationContract]
        FinregisterListItem GetFinregisterForCommonPPByID(long finregisterID);

        [OperationContract]
        ReqTransferListItem GetReqTransferForCommonPPByID(long reqTransferID, bool isOpfr = false);

        /// <summary>
        /// Сохранение платёжного поручения и выставление всех нужных статусов
        /// </summary>
        /// <param name="payment">ПП</param>
        [OperationContract]
        long SaveCommonPP(AsgFinTr payment);

        [OperationContract]
        List<AsgFinTr> GetUnlinkedCommonPPList(AsgFinTr.Sections section, AsgFinTr.Directions direction);

        [OperationContract]
        bool IsUKPlansHasReqTransfers(List<long> transferIdList);

        [OperationContract]
        List<long> GetUsedContragentIdListForRegister(long regId);

        [OperationContract]
        List<KDoc> GetKDocsForPeriod(long periodId);


        #region СОХРАНЕНИЕ/УДАЛЕНИЕ СУЩНОСТЕЙ

        [OperationContract]
        void DeleteDopSPN(long dopSpnId, bool isKDoc);

        [OperationContract]
        void DeleteSIVRRegister(List<SITransfer> list);

        [OperationContract]
        void DeleteTransfer(List<ReqTransferDeleteListItem> reqTransferIdList, long transferListId);

        [OperationContract]
        WebServiceDataError DeleteFinregister(long id, bool checkFailConditions = false, string logData = null, bool checkIsCreate = false);

        [OperationContract]
        WebServiceDataError DeleteRegister(long id);

        [OperationContract]
        void DeleteReqTransfer(long reqTransferId, long? siTransferId, long contractId, bool isActSigned);

        [OperationContract]
        void DeleteDocFileBodyWithContent(long docFileBodyId, long? fileContentId = null);

        [OperationContract]
        void DeleteUKPaymentsPlan(long planId);

        [OperationContract]
        void DeleteContract(long contractId);

        [OperationContract]
        void DeleteSI(long contragentId, long legalEntityId);

        [OperationContract]
        void DeleteDocument(long documentId, List<long> attachIdList, List<long> fileContentIdList);

        [OperationContract]
        void DeleteBudgetSchils(long budgetId);

        [OperationContract]
        void DeleteDirectionSchils(long dirId);

        [OperationContract]
        void DeleteDue(long addSpnId);




        [OperationContract]
        long SaveReqTransfer(ReqTransfer req);

        [OperationContract]
        long SaveAps(Aps aps, List<DopAps> dopAps);

        [OperationContract]
        long SaveDue(AddSPN addSpn, List<DopSPNListItem> dopSPNList);

        [OperationContract]
        List<long> SaveReturnFinregisters(List<FinregisterWithCorrAndNPFListItem> finregisters);

        [OperationContract]
        SaveSIContractResultItem SaveSIContract(SaveSIContractItem data, bool isNew);

        [OperationContract]
        long SaveTransfer(SITransfer siTransfer, List<ReqTransfer> reqList);

        [OperationContract]
        SaveSIResultItem SaveSI(SaveSIItem data);

        [OperationContract]
        LongLongLongTuple SaveBank(Contragent ca,
            LegalEntity bank,
            License license,
            List<LegalEntityHead> headList,
            List<long> delHeadList,
            List<LegalEntityRating> ratingList,
            List<long> delRatingList,
            List<BankStockCode> stockCodeList,
            List<long> delStockCodeList,
            List<LegalEntityCourier> personListChanged
            );

        [OperationContract]
        long SaveUKPaymentPlan(SITransfer transfer, List<LongDecimalTuple> paymentsList);

        [OperationContract]
        long SaveBankAccount(BankAccount bankAccount);

        [OperationContract]
        WebServiceDataError SaveAndCleanDocument(Document document, List<long> removeAttachIds = null, long? docFileIdToRemove = null, long? docFileContentIdToRemove = null);

        [OperationContract]
        long SaveMonthTransferWizardResults(SIRegister register, List<MonthTransferWizardResultListItem> contracts);

        #endregion

        [OperationContract]
        long GetListCount(string pType);

        [OperationContract]
        IList GetListPageConditions(string pTypeName, int startIndex, List<ListPropertyCondition> propertyConditionList);

        [OperationContract]
        IList<BankListItem> GetCommonBankListItems();

        [OperationContract]
        PaymentAssignment GetPayAssForReport(long appPart, long direction, long? opId = null);

        [OperationContract]
        string GetLeIdentifierCodeForFinregister(long id, bool isNpfToPfr);

        [OperationContract]
        void SetFinregisterStateTransfered(DateTime? finDate, string finMoveType, List<long> frIdList, bool isNpfToPfr);

        [OperationContract]
        void SetFinregisterStateIssued(List<long> frIdList, bool isNpfToPfr);

        [OperationContract]
        void UpdateOfferClaimData(long id, string claimNumber, DateTime? claimDate);

        [OperationContract]
        void RemoveOnesExportByExportId(long exportId);

        [OperationContract]
        BOReportForm1 GetCBReport(long reportId);

        [OperationContract]
        bool HasCBReportFreeYearsAndQaurters(List<BOReportForm1.ReportTypeEnum> rTypeList);

        [OperationContract]
        List<long> SaveReportForm1(BOReportForm1 report);

        [OperationContract]
        decimal GetKDocsSumForBOReport(int year, int quartal);

        [OperationContract]
        decimal GetKDocsSumForBOReportQuarter(int year, int quartal);

        [OperationContract]
        decimal GetTempAllocSumForBOReport(int year, int quartal, bool qOnly);

        [OperationContract]
        decimal GetDepPercForBOReport(int year, int quartal, bool qOnly, long? portfolioId = null);

        [OperationContract]
        decimal GetDSVForBOReport(int year, int quartal, bool qOnly, bool for3);

        [OperationContract]
        decimal GetOrdReportForBOReport(int year, int quartal, bool qOnly);

        [OperationContract]
        decimal GetCostsForBOReport(int year, int quartal, bool qOnly, string kind);

        [OperationContract]
        List<BOReportForm1> GetGeneralBOReportForm1List(BOReportForm1.ReportTypeEnum reportType, bool monthlyOnly = false);

        [OperationContract]
        Dictionary<int, List<int>> GetBOReportForm1FreeYearsAndQaurtersList(BOReportForm1.ReportTypeEnum reportType);

        [OperationContract]
        Dictionary<int, List<int>> GetBOReportMonthlyFreeYearsAnMonthsList(int debugCount = 0);

        [OperationContract]
        Dictionary<string, Dictionary<int, List<int>>> GetNotApprovedCBReportsYearsAndQuartersList(BOReportForm1.ReportTypeEnum reportType);

        [OperationContract]
        long CBReportCreate(int year, int quarter, BOReportForm1.ReportTypeEnum repType);

        [OperationContract]
        BOReportForm1 TryGetPreviousReport(BOReportForm1 report);

        [OperationContract]
        List<CBReportForm2TransferListItem> GetTransfersListForCBReportForm2(int year, int quarter);

        [OperationContract]
        List<DepositCBOrderListItem> GetDepositsListForCBReportForm3(int year, int quarter, bool monthly = false);

        [OperationContract]
        List<DepositCBOrderListItem> GetDepositsListForCBMonthlyReportForm3(int year, int month);

        [OperationContract]
        void SaveCBReportToRepository(byte[] fileContent, long reportId, string comment);

        [OperationContract]
        void BatchUpdatePP(List<AsgFinTr> list, bool isTwoPP, List<long> singleIdList);

        [OperationContract]
        List<ReqTransfer> GetReqTransferListForCommonPPSplit(long id);

        [OperationContract]
        List<ReqTransferPartialListItem> GetReqTransferPartialListForCommonPPSplit(long? idList);

        [OperationContract]
        List<Finregister> GetFinregListForCommonPPSplit(long id, bool isReturn);

        [OperationContract]
        long GetOpfrRegisterListItemCount(DateTime? from, DateTime? to);
        [OperationContract]
        List<OpfrRegisterListItem> GetOpfrRegisterListItemPage(int index, DateTime? from, DateTime? to);

        [OperationContract]
        IList<OpfrTransferListItem> GetOpfrTransferListItems(long registerId);

        [OperationContract]
        OpfrRegisterListItem GetOpfrRegisterListItem(long id);

        [OperationContract]
        List<OpfrRegisterListItem> GetOpfrRegisterListItems(long id);


        [OperationContract]
        OpfrRegisterListItem GetOpfrRegisterListItemByTransferID(long id);

        [OperationContract]
        void UpdateOpfrTransfersPortfolio(long[] idList, long? importPortfolioID);

        [OperationContract]
        bool CBReportCanApprove(CBRApprovalRequest item);

        [OperationContract]
        List<long> CBReportRequestApproval(CBRApprovalRequest item);

        [OperationContract]
        LoginMessageItem GetCBReportForApproveMessage();

       

        #region Analyze DOKIP

        //Report3
        [OperationContract]
        List<AnalyzePensionplacementData> GetPensionplacementDataByReportId(long rId);

        [OperationContract]
        List<AnalyzePensionplacementData> GetPensionplacementDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);

        [OperationContract]
        List<AnalyzePensionplacementReport> GetPensionplacementReportByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);


        //Report1-2
        [OperationContract]
        List<AnalyzePensionfundtonpfReport> GetPensionfundReportsByYearKvartal(int? year = null, int? kvartal = null, int? subtraction = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzePensionfundtonpfData> GetPensionfundDataByYearKvartal(int? year = null, int? kvartal = null, int? subtraction = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzePensionfundtonpfData> GetPensionfundDataByReportId(long reportId);


        //Report4
        [OperationContract]
        List<AnalyzeIncomingstatementsReport> GetIncomingReportsByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzeIncomingstatementsData> GetIncomingstatementsDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzeIncomingstatementsData> GetIncomingstatementsDataByReportId(long rId);

        //Report5
        [OperationContract]
        List<AnalyzeInsuredpersonReport> GetInsuredpersonReportsByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzeInsuredpersonData> GetInsuredpersonDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzeInsuredpersonData> GetInsuredpersonDataByReportId(long rId);

        //Report6
        [OperationContract]
        List<AnalyzeYieldfundsReport> GetYieldfundsReportsByYearKvartal(int? year = null, int? kvartal = null, int? actId = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzeYieldfundsData> GetYieldfundsDataByYearKvartal(int? year = null, int? kvartal = null, int? actId = null, bool archiveData = false);

        [OperationContract]
        List<AnalyzeYieldfundsData> GetYieldfundsDataByReportId(long rId);

        //Report7
        [OperationContract]
        List<AnalyzePaymentyearrateReport> GetPaymentyearrateReportByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzePaymentyearrateData> GetPaymentyearrateDataByYearKvartal(int? year = null, int? kvartal = null, bool archiveData = false);
        [OperationContract]
        List<AnalyzePaymentyearrateData> GetPaymentyearrateDataByReportId(long rId);

        //[OperationContract]
        //LoginMessageItem GetAnalyzeWarningMessage();

        #endregion

        [OperationContract]
        void UpdateAuctionWizardStep(long auctionID, int auctionStep);

        [OperationContract]
        List<AccOpListItem> GetTransfersForChfr(long portfolioID, int selectedYear, int selectedQuarter, int mode);

        [OperationContract]
        void DeleteOpfrPPLink(long transferId, long ppId);

        [OperationContract]
        void RemoveOpfrTransferLink(long ppId, long transferId);

        [OperationContract]
        IList<OpfrTransferListItem> GetOpfrTransferListItemsByPP(long id);

        [OperationContract]
        void UpdateFinregisterIsTransferred(long? ntParentId, int value);

        [OperationContract]
        List<EDOLogListItem> GetSuccessLoadedODKByForm2(DateTime? dateFrom, DateTime? dateTo, string form, int quartal, long year, string dogovorNum);
    }
}
