﻿using System.ServiceModel;

namespace db2connector
{
    [ServiceContract]
    public interface IClientCallback
    {
        [OperationContract]
        void OnServerPing();
    }
}