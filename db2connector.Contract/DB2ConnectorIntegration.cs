﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace db2connector
{
    public partial interface IDB2Connector
    {
        [OperationContract]
        WebServiceDataError StartOnesImport(string importPath, List<ImportFileListItem> files = null);

        [OperationContract]
        int[] GetOnesImportProgress();

        [OperationContract]
        List<string> GetOnesKindsList();

        [OperationContract]
        List<OnesErrorJournalListItem> GetOnesErrorJournal(int index, DateTime? periodStart, DateTime? periodEnd, int exportComplete, string s);

        [OperationContract]
        List<OnesJournal> GetOnesExportJournal(int index, DateTime? periodStart, DateTime? periodEnd);

        [OperationContract]
        List<object> StartOnesExport(string exportPath, bool isClient, OnesSettingsDef.IntGroup type, List<long> idList = null);

        [OperationContract]
        List<RepositoryNoAucImpExpFile> OnesGetGeneratedExportFiles();

        //[OperationContract]
        //List<OnesSettingsDef> GetSelectedOnesDefinitions(string pageType);

        [OperationContract]
        List<long> SaveOnesOpSettings(List<OnesOpSettings> addList, List<long> remList);

        [OperationContract]
        List<OnesExportDataItem> GetExportDataItemsList(OnesSettingsDef.IntGroup part);

        [OperationContract]
        OnesExportStatus GetOnesExportStatus(OnesSettingsDef.IntGroup type);

        [OperationContract]
        long GetOnesExportJournalListCount(DateTime? periodStart, DateTime? periodEnd);

        [OperationContract]
        RepositoryNoAucImpExpFile GetOnesXmlStorageItem(long id);

        [OperationContract]
        long OnesImportErrorsCount(DateTime? periodStart, DateTime? periodEnd, int exportComplete, string s);
    }
}
