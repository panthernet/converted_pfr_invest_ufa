﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using PFR_INVEST.LotusMigration.App_Code.DataItems;


    /// <summary>
    /// Ищет столбец во всех таблицах схемы и выбирает все его значения
    /// </summary>
    public class GuidScriptHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "GuidScript"; }
        }


        private string[] Args;

        public string SchemaFromName;
        public string SchemaToName;

        public string FileName;
        public string FilePath;

        public string SearchString;

        List<string> IgnoreTables;


        public GuidScriptHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            SchemaFromName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            SchemaToName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 2)
                throw new ArgumentNullException();


            FileName = args[1];
            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            //string s = ConfigurationManager.AppSettings.Get("IgnoreSearchTableList");

            IgnoreTables = new List<string>();

            //if (!string.IsNullOrEmpty(s))
            //{
            //    foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
            //        IgnoreTables.Add(t.Trim());
            //}
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            //SchemaInfoBase schemaFrom = SchemaInfoBase.GetSchemaAllTables(SchemaFromName);
            SchemaInfoBase schemaTo = SchemaInfoBase.GetSchemaAllTables(SchemaToName);

            Logger.Instance.Info("Выборка значений в таблицах");

            Console.WriteLine(SearchString);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("-- Set Lotus_GUID values");

            foreach (var t in schemaTo.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                if (!t.IsMigratedTableGuid())// No corresponding columns
                    continue;

                Console.WriteLine(t.Name);
                try
                {
                    t.AppendGuidScript(sb, SchemaFromName);
                }
                catch
                {
                    sb.AppendFormat("-- ERROR");
                    sb.AppendLine();
                    sb.AppendFormat("Call PFR_LOTUS.Set_Lotus_Guid('{0}', '{1}');", "", t.Name.ToUpper());
                    sb.AppendLine();
                }
            }


            SaveToFile(FilePath, sb.ToString());


            Logger.Instance.Info("Готово");
        }

    }
}
