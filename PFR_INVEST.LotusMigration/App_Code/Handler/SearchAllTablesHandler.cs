﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;

    public class SearchAllTablesHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "SearchAllTables"; }
        }


        private string[] Args;

        public string SchemaName;

        public string FileName;
        public string FilePath;

        public string SearchString;

        List<string> IgnoreTables;

        public SearchAllTablesHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;

            if (args[1].ToLower().Contains("lotus"))
                SchemaName = ConfigurationManager.AppSettings.Get("LotusSchemeName");
            else
                SchemaName = ConfigurationManager.AppSettings.Get("PfrSchemeName");


            if (args.Length < 3)
                throw new ArgumentNullException();

            SearchString = args[2];


            if (args.Length < 4)
                FileName = string.Format("search {0}.txt",ClearFileString(SearchString));
            else
                FileName = args[3];


            FilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, FileName);

            string s = ConfigurationManager.AppSettings.Get("IgnoreSearchTableList");

            IgnoreTables = new List<string>();

            if (!string.IsNullOrEmpty(s))
            {
                foreach (string t in s.ToUpper().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    IgnoreTables.Add(t.Trim());
            }
        }

        private string ClearFileString(string s)
        {
            s = s.Replace(";", " ").Replace(".", "_").Replace(",", "_").Replace("/", " ").Replace("\\", " ").Replace("*", " ").Replace("?", "");
            return s;
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных о таблицах");

            SchemaInfoBase s = SchemaInfoBase.GetSchemaAllTables(SchemaName);

            Logger.Instance.Info("Поиск в таблицах");

            Console.WriteLine(SearchString);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Искомая строка");
            sb.AppendFormat("[{0}]\r\n", SearchString);

            sb.AppendLine("Tables found");

            foreach (var t in s.Tables)
            {
                if (IgnoreTables.Contains(t.Name.ToUpper()))
                    continue;

                Console.WriteLine(t.Name);
                sb.Append(t.GetMatchText(OdbcConnection, SearchString));
            }


            SaveToFile(FilePath, sb.ToString());


            Logger.Instance.Info("Готово");
        }

    }
}
