﻿
namespace PFR_INVEST.DBUtils
{
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;
    using PFR_INVEST.Core.Logger;
    using System.Configuration;
    using System.IO;
    using PFR_INVEST.LotusMigration.App_Code;
    using PFR_INVEST.LotusMigration.App_Code.Common;
    using PFR_INVEST.LotusMigration.App_Code.DataItems;
    using System.Text.RegularExpressions;
    

    public class PortfolioYearHandler : ScriptHandlerBase
    {
        public override string ActionName
        {
            get { return "PortfolioYear"; }
        }


        private string[] Args;

        public PortfolioYearHandler(string connectionString, string[] args)
        {
            ConnectionString = connectionString;
            Args = args;
        }

        public override void Process()
        {
            Logger.Instance.Info("Выборка данных");

            var pL = Portfolio.SelectAll();

            //var yL = Year.SelectAll();

            Logger.Instance.Info("Обработка");

            foreach (var p in pL)
            {
                if (p.YearID == null)
                {
                    int? year = GetYear(p.Name);

                    if (year != null)
                        p.SetYear(year.Value);
                }
            }
            

            Logger.Instance.Info("Готово");
        }

        private int? GetYear(string text)
        {
            Match m = Regex.Match(text, @"(?<!\d)(?<y>[\d]{4})(?!\d)");

            if (!m.Success)
                return null;

            var c = m.Groups["y"].Captures;

            if (c.Count != 1)
                return null;

            return int.Parse(c[0].Value);

            return null;
        }

    }
}
