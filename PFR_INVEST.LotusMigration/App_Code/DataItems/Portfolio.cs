﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;

namespace PFR_INVEST.LotusMigration.App_Code.DataItems
{
    public class PortfolioList : List<Portfolio>
    { }

    public class Portfolio : DataItemBase
    {
        public long ID;
        public string Name;
        public long? YearID;

        public Portfolio()
        {
        }

        public Portfolio(System.Data.Odbc.OdbcDataReader reader)
        {
            // TODO: Complete member initialization
            ID = (long)reader["ID"];
            Name = (string)reader["YEAR"];
            YearID = (long?)IsDbNullValue(reader["YEAR_ID"]);
        }

        internal static PortfolioList SelectAll()
        {
            PortfolioList res = new PortfolioList();

            string commandText =
    string.Format(@"
select 
ID, YEAR, YEAR_ID

from PFR_BASIC.PORTFOLIO
");

            ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                Portfolio p = new Portfolio(reader);
                res.Add(p);
            });

            return res;
        }

        internal void SetYear(int year)
        {
            string cmd = string.Format(@"
update PFR_BASIC.PORTFOLIO set YEAR_ID = 
(select y.ID
from
PFR_BASIC.YEARS y
where 
y.NAME='{0}')
where
ID = {1}", year, ID);

            ExecuteNonQuery(cmd);
        }
    }
}
