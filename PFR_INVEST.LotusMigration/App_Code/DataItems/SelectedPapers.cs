﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Globalization;

namespace PFR_INVEST.LotusMigration.App_Code.DataItems
{
    public class SelectedPapersList : List<SelectedPapers>
    { }

    public class SelectedPapers : DataItemBase
    {
        public long ID;
        public string ID_LOTUS;
        public string SECURITYCOUNT;
        public string NOMINALPRICE;

        public SelectedPapers()
        {
        }

        public SelectedPapers(System.Data.Odbc.OdbcDataReader reader)
        {
            ID = (long)reader["ID"];
            ID_LOTUS = (string)reader["ID_LOTUS"];
            SECURITYCOUNT = (string)reader["SECURITYCOUNT"];
            NOMINALPRICE = (string)reader["NOMINALPRICE"];
        }

        internal static SelectedPapersList SelectAll()
        {
            SelectedPapersList res = new SelectedPapersList();

            string commandText =
    string.Format(@"
select 
*
from PFR_LOTUS.SELECTEDPAPERS___CALCULATION
where ordertype='покупка' and PRODUCTPLACE='Аукцион'
");

            ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                SelectedPapers p = new SelectedPapers(reader);
                res.Add(p);
            });

            return res;
        }



        internal List<long> GetCountList()
        {
            string[] a = SECURITYCOUNT.Split(new string[]{";"}, StringSplitOptions.RemoveEmptyEntries);

            List<long> res = new List<long>();

            foreach (var x in a)
                res.Add(Convert.ToInt64(x));

            return res;
        }

        internal List<decimal> GetPriceList()
        {
            string[] a = NOMINALPRICE.Replace(",",".").Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            List<decimal> res = new List<decimal>();

            foreach (var x in a)
                res.Add(Convert.ToDecimal(x, CultureInfo.InvariantCulture.NumberFormat));

            return res;

        }

        internal void InsertSplit(long count, decimal price)
        {
            string cmd = string.Format(CultureInfo.InvariantCulture.NumberFormat, @"
insert into PFR_LOTUS.SELECTEDPAPERS___CALCULATION_SPLIT
(PARENT_ID, PARENT_ID_LOTUS, SECURITYCOUNT, NOMINALPRICE)
values
({0}, '{1}', {2}, {3} )
", ID, ID_LOTUS, count, price);

            ExecuteNonQuery(cmd);
        }
    }
}