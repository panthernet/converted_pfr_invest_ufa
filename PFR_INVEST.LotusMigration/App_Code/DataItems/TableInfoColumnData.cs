﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.LotusMigration.App_Code.Common;
using System.Data.Odbc;

namespace PFR_INVEST.LotusMigration.App_Code.DataItems
{
    public class TableInfoColumnData : TableInfoBase
    {

        public TableInfoColumnData(TableInfoBase t)
        {
            Name = t.Name;
            Schema = t.Schema;
            Cols = t.Cols;

            IdentityCol = t.IdentityCol;


        }
        public override string GetMatchText(OdbcConnection OdbcConnection, string searchString)
        {
            ColInfoBase c = Cols.FirstOrDefault(p => p.ColName.ToUpper() == searchString);

            if (c == null)
                return null;//No such column


            FindMatch(OdbcConnection, c);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Table: {0}", Name);
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendFormat("Column: {0}  -- {1}", c.ColName, c.Remarks);
            sb.AppendLine();
            sb.AppendLine();
            foreach (string s in c.ValueList)
            {
                sb.AppendFormat("{0}", s);
                sb.AppendLine();
            }
            sb.AppendLine();

            return sb.ToString();
        }

        private void FindMatch(OdbcConnection OdbcConnection, ColInfoBase c)
        {
            c.ValueList = new List<string>();

            //if (c.DataType == ColInfoBase.enType.date || 
            //    c.DataType == ColInfoBase.enType.time || 
            //    c.DataType== ColInfoBase.enType.integer || 
            //    c.DataType== ColInfoBase.enType.bigint || 
            //    c.DataType== ColInfoBase.enType.decimalType )
            //    continue;

            if (c.DataType != ColInfoBase.enType.varchar)
                return;

            string cmd = string.Format(@"
select 
distinct ""{2}"" as Value
from ""{0}"".""{1}""
", this.Schema, this.Name, c.ColName);

            _CurrentColumn = c;

            DataItemBase.ReadAllRecords(cmd, AddMatch);
        }

        ColInfoBase _CurrentColumn;

        private void AddMatch(OdbcDataReader reader)
        {
            string s = (string)IsDbNull(reader["VALUE"], "[NULL]");

            _CurrentColumn.ValueList.Add(s);
        }

    }
}
