﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using PFR_INVEST.LotusMigration.App_Code.DataItems;
using System.Xml;

namespace PFR_INVEST.LotusMigration.App_Code.Common
{
    public class SchemaInfoBase
    {
        public string SchemaName;

        public List<TableInfoBase> Tables;

        public SchemaInfoBase()
        {
            Tables = new List<TableInfoBase>();
        }

        internal TableInfoBase AddCol(System.Data.Odbc.OdbcDataReader reader)
        {
            string tableName = (string)reader["TABNAME"];
            TableInfoBase t = GetTableOpen(tableName);
            t.AddCol(reader);
            return t;
        }

        /// <summary>
        /// Если таблица не найдена, создаем ее
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public TableInfoBase GetTableOpen(string tableName)
        {
            return GetTable(tableName, true);
        }

        public TableInfoBase GetTable(string tableName, bool isAutoCreate)
        {
            TableInfoBase t = Tables.FirstOrDefault(p => p.Name.ToLower() == tableName.ToLower());

            if (t == null)
            {
                if (!isAutoCreate)
                    return null;

                t = new TableInfoBase() { Name = tableName };
                Tables.Add(t);
            }

            return t;
        }

        public static SchemaInfoBase GetSchemaAllTables(string schemaName)
        {
            SchemaInfoBase res = new SchemaInfoBase() { SchemaName = schemaName };

            string commandText =
    string.Format(@"
select 
tabName, ColNo, ColName, Remarks, TypeName, Length, Scale, Identity, Nulls

from syscat.columns 
where tabschema = '{0}'
order by tabname, colno
", schemaName);

            FillTableInfo(commandText, res, schemaName);

            return res;
        }

        private static void FillTableInfo(string commandText, SchemaInfoBase res, string schemaName)
        {
            DataItemBase.ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                TableInfoBase r = res.AddCol(reader);
                r.Schema = schemaName;
            });
        }

        internal void ApplyMapData(SchemaInfoBase sBasic)
        {
            foreach (var tL in this.Tables)
            {
                tL.MapIndex = new Dictionary<string, int>();

                foreach (var t in sBasic.Tables)
                {
                    foreach (var x in t.MapIndex)
                        if (x.Key == tL.Name)
                            tL.AddMapIndex(t.Name, x.Value);
                }
            }
        }

        internal void SetSettings(XmlElement s)
        {
            if (s == null)
                return;

            foreach (XmlElement x in s.SelectNodes("table"))
            {
                string tName = x.GetAttribute("name");
                TableInfoBase t = GetTable(tName, false);
                if (t == null)
                    return;

                t.SetSettings(x);
            }

        }

        internal void LoadRowCount()
        {
            foreach (var x in Tables)
                x.GetRowCount();
        }

        internal void LoadNullLotusRowCount()
        {
            foreach (var x in Tables)
            {
                Console.WriteLine(x.Name);

                bool b = true;

                for (int i = 0; i < 3; i++)
                {
                    b = true;
                    try
                    {
                        x.LoadNullLotusRowCount();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("EXCEPTION!!!");
                        
                        b = false;
                    }

                    if (b)
                        continue;
                }

                if (!b)//failed 3 attempts
                {
                    x.RowCountLotusID = -1;
                    x.RowCountLotusGUID = -1;
                    x.RowCountLotusTable = -1;
                }
            }
        }


        public void FilterTables(PFR_INVEST.DBUtils.Audit2AllTablesHandler.ConditionHandler selectCondition)
        {
            Tables = Tables.Where(p => !selectCondition(p)).ToList();
        }

        internal void LoadTableRemarks()
        {
            string commandText =
    string.Format(@"
select 
tabName,Remarks

from syscat.TABLES 
where tabschema = '{0}'
order by tabname
", SchemaName);

            DataItemBase.ReadAllRecords(commandText, delegate(OdbcDataReader reader)
            {
                string tabName = (string)IsDbNull(reader["TABNAME"], null);
                string remarks = (string)IsDbNull(reader["REMARKS"], null);

                var t = GetTable(tabName, false);
                if (t != null)//Table not found, do nothing
                {
                    t.Remarks = remarks;
                }
            });
        }

        public static object IsDbNull(object value, object defaultValue)
        {
            if (value == DBNull.Value)
                return defaultValue;

            return value;
        }

        internal void CountNullLotus()
        {
            throw new NotImplementedException();
        }
    }
}
