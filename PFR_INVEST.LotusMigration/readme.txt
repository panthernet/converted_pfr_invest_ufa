
ODBS ���������� �������� � .config �����
<add key="DSN" value="PFR_D2" />




Usage

��������� �������� ������� �������
PFR_INVEST.LotusMigration.exe i <Lotus_table_name> <Pfr_table_name> <output_script_file_name>


��������� ������� �������, ���� ������������� �� ���� ������
PFR_INVEST.LotusMigration.exe it <Lotus_table_name> <Pfr_table_name> <output_script_file_name>


����� ���������� �������� �� ���� �������� �����
PFR_INVEST.LotusMigration.exe s <lotus|pfr> "search string" <output_script_file_name>

�������� ������, ������� ���������� ������� ���
    <!--������ ������, ����� � ������� ����� �� ����, ����������� ";"-->
    <add key="IgnoreSearchTableList" value="_LOTUS_ID_IMPORTED;_ALL_LOTUS_ID" />


����� decimal �������� �� ���� �������� �����
PFR_INVEST.LotusMigration.exe sd <lotus|pfr> "search string" <output_script_file_name>


����� date �������� �� ���� �������� �����
PFR_INVEST.LotusMigration.exe sdate <lotus|pfr> "search string" <output_script_file_name>


Trim (������� ���������/�������� ��������) ���������� �������� �� ���� �������� �����
PFR_INVEST.LotusMigration.exe trim <lotus|pfr> <output_file_name>

�������� ������, ������� ���������� ������� ���
    <add key="IgnoreTrimTableList" value="_LOTUS_ID_IMPORTED;_ALL_LOTUS_ID" />


�������� ����� �������� ������
PFR_INVEST.LotusMigration.exe audit <output_file_name>

������������� ������ ��� ������ �������� ������
PFR_INVEST.LotusMigration.exe auditTemplate <output_file_name.xml>

�������� ����� �������� ������ (������ 2)
PFR_INVEST.LotusMigration.exe audit2 <config_file_name.xml> <output_file_name>
