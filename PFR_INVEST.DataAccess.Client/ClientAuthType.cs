﻿namespace PFR_INVEST.DataAccess.Client
{
    public enum ClientAuthType
    {
        None = 0,
        ECASA = 1,
        ECASAHTTPS = 2
    }
}
