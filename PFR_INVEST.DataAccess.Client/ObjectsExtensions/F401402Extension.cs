﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
    public static class F401402Extension
    {

        public static List<F401402UK> GetDetailList(this EdoOdkF401402 requirement)
        {

            if (requirement != null)
            {
                return DataContainerFacade.GetListByProperty<F401402UK>("EdoID", requirement.ID).ToList();
            }
            return new List<F401402UK>();
        }

        public static Contract GetContract(this F401402UK detail)
        {

            if (detail != null)
            {
                return DataContainerFacade.GetByID<Contract, long>(detail.ContractId.Value); ;
            }
            return null;
        }


    }


}
