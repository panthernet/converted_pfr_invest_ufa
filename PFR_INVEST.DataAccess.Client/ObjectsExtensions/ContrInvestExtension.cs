﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ContrInvestExtension
    {
        public static IList GetContrInvestOBLList(this ContrInvest p_contrInvest)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_contrInvest, "ContrInvestOBLList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<ContrInvest, long, IList>(p_contrInvest.ID, "ContrInvestOBLList");
            });
        }

        public static IList GetContrInvestAKCList(this ContrInvest p_contrInvest)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_contrInvest, "ContrInvestAKCList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<ContrInvest, long, IList>(p_contrInvest.ID, "ContrInvestAKCList");
            });
        }

        public static IList GetContrInvestPAYList(this ContrInvest p_contrInvest)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_contrInvest, "ContrInvestPAYList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<ContrInvest, long, IList>(p_contrInvest.ID, "ContrInvestPAYList");
            });
        }
    }
}
