﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
	public static class McProfitAbilityExtension
	{
		public static Contract GetContract(this McProfitAbility profit) 
		{
			if (profit != null && profit.ContractId.HasValue)
			{
				return DataContainerFacade.GetExtensionData<Contract>(profit, "Contract", delegate()
				{
					return DataContainerFacade.GetByID<Contract, long>(profit.ContractId.Value);
				});
			}
			else
				return null;
		}

	}
}
