﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class RegisterExtension
    {
       

        public static FZ GetFZ(this Register p_register)
        {
            return DataContainerFacade.GetExtensionData(p_register, "FZ", delegate()
            {
                return DataContainerFacade.GetByID<FZ, long>(p_register.FZ_ID);
            });
        }

        public static ApproveDoc GetApproveDoc(this Register p_register)
        {
            return DataContainerFacade.GetExtensionData(p_register, "ApproveDoc", delegate()
            {
                return DataContainerFacade.GetByID<ApproveDoc, long>(p_register.ApproveDocID);
            });
        }

        public static ERZL GetERZL(this Register p_register)
        {
            return DataContainerFacade.GetExtensionData(p_register, "ERZL", delegate()
            {
                return DataContainerFacade.GetByID<ERZL, long>(p_register.ERZL_ID);
            });
        }

        public static IList<Finregister> GetFinregistersList(this Register pRegister)
        {
            return DataContainerFacade.GetExtensionData<IList<Finregister>>(pRegister, "FinregistersList", delegate()
            {
                var list =  DataContainerFacade.GetObjectProperty<Register, long, IList>(pRegister.ID, "FinregistersList");
                return list.Cast<Finregister>().ToList();
            });
        }

		public static Register GetMainRegister(this Register retRegister)
		{
			return DataContainerFacade.GetExtensionData(retRegister, "MainRegister", delegate()
			{
				var mR = DataContainerFacade.GetListByProperty<Register>("ReturnID", retRegister.ID);
				return mR.FirstOrDefault();
			});
		}
    }
}
