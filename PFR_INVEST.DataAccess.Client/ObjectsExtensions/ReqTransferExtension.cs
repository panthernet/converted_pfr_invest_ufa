﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ReqTransferExtension
    {
        public static SITransfer GetTransferList(this ReqTransfer pTr)
        {
            return DataContainerFacade.GetExtensionData(pTr, "TransferList", () => DataContainerFacade.GetByID<SITransfer, long>(pTr.TransferListID));
        }
        public static Portfolio GetPortfolio(this ReqTransfer pTr)
        {
            return DataContainerFacade.GetExtensionData(pTr, "Portfolio", () => DataContainerFacade.GetByID<Portfolio, long>(pTr.PortfolioID));
        }
        public static PfrBankAccount GetPfrBankAccount(this ReqTransfer pTr)
        {
            return DataContainerFacade.GetExtensionData(pTr, "PfrBankAccount", () => DataContainerFacade.GetByID<PfrBankAccount, long>(pTr.PFRBankAccountID));
        }

		public static List<AsgFinTr> GetCommonPPList(this ReqTransfer pTr) 
		{
			if (pTr == null || pTr.ID == 0)
				return new List<AsgFinTr>();
			return DataContainerFacade.GetExtensionData(pTr, "CommonPPList",
			    () => DataContainerFacade.GetObjectProperty<ReqTransfer, long, IList>(pTr.ID, "CommonPPList").Cast<AsgFinTr>().ToList());
		}
    }
}
