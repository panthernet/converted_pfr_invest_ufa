﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class SPNOperationExtension
    {
        public static IList GetDirections(this SPNOperation p_Operation)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_Operation, "Directions", delegate()
            {
                return DataContainerFacade.GetObjectProperty<SPNOperation, long, IList>(p_Operation.ID, "Directions");
            });
        }
    }
}
