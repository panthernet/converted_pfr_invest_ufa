﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client
{
    public static class SecurityExtension
    {
        public static IList<Payment> GetPayments(this Security security)
        {
            if (security != null)
            {
                var list = DataContainerFacade.GetExtensionData<IList>(security, "Payments", delegate()
                {
                    return DataContainerFacade.GetObjectProperty<Security, long, IList>(security.ID, "Payments");
                });
				return (list != null) ? list.Cast<Payment>().ToList() : new List<Payment>();
            }
            else
                return new List<Payment>();
        }

        public static IList<MarketPrice> GetMarketPrices(this Security security)
        {
            if (security != null)
            {
                var list = DataContainerFacade.GetExtensionData<IList>(security, "MarketPrices", delegate()
                {
                    return DataContainerFacade.GetObjectProperty<Security, long, IList>(security.ID, "MarketPrices");
                });
                return (list != null) ? list.Cast<MarketPrice>().ToList() : new List<MarketPrice>();
            }
            else
                return new List<MarketPrice>();
        }

        public static Currency GetCurrency(this Security p_security)
        {
            return DataContainerFacade.GetExtensionData<Currency>(p_security, "Currency", delegate()
            {
                return DataContainerFacade.GetByID<Currency, long>(p_security.CurrencyID);
            });
        }
    }
}
