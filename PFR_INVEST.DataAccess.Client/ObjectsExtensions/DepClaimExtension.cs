﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    using PFR_INVEST.Constants.Identifiers;
    using PFR_INVEST.DataObjects.ListItems;

    public static class DepClaimExtension
    {
        public static IList<DepClaim2> GetDepClaim2List(this DepClaimSelectParams auction)
        {

            if (auction != null)
            {
                return DataContainerFacade.GetListByProperty<DepClaim2>("AuctionID", auction.ID).OrderByDescending(x => x.Rate).ToList();
            }
            return new List<DepClaim2>();
        }

        public static IList<DepClaim2> GetDepClaim2ConfirmList(this DepClaimSelectParams auction)
        {
            return auction.GetDepClaim2List().Where(c => c.Status == DepClaim2.Statuses.Confirm || c.Status == DepClaim2.Statuses.ConfirmNotAccepted || c.Status == DepClaim2.Statuses.ConfirmOfferCreated).ToList();
        }

        public static IList<DepClaimMaxListItem> DepClaimMaxList(this DepClaimSelectParams auction, int depclaimType)
        {

            if (auction != null)
            {
                return DataContainerFacade.GetListByProperty<DepClaimMaxListItem>("DepclaimselectparamsId", auction.ID).Where(x => x.DepclaimType == depclaimType).ToList();
            }
            return new List<DepClaimMaxListItem>();
        }

        public static IList<DepClaimOffer> DepOfferList(this DepClaimSelectParams auction)
        {
            if (auction != null)
            {
                return DataContainerFacade.GetListByProperty<DepClaimOffer>("AuctionID", auction.ID).ToList();
            }
            return new List<DepClaimOffer>();
        }
    }
}
