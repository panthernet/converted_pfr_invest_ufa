﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ERZLExtension
    {
        public static FZ GetFZ(this ERZL p_erzl)
        {
            return DataContainerFacade.GetExtensionData<FZ>(p_erzl, "FZ", delegate()
            {
                return DataContainerFacade.GetByID<FZ, long>(p_erzl.FZ_ID);
            });
        }

        public static IList GetERZLNotifiesList(this ERZL p_erzl)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_erzl, "ERZLNotifiesList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<ERZL, long, IList>(p_erzl.ID, "ERZLNotifiesList");
            });
        }
    }
}
