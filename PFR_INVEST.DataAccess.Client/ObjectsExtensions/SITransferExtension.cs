﻿using System.Collections;
using System.Collections.Generic;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class SITransferExtension
    {
        public static SIRegister GetTransferRegister(this SITransfer transfer)
        {
            return DataContainerFacade.GetExtensionData(transfer, "TransferRegister", () => 
                DataContainerFacade.GetByID<SIRegister, long>(transfer.TransferRegisterID));
        }

        public static IList GetTransfers(this SITransfer transfer)
        {
            return DataContainerFacade.GetExtensionData(transfer, "Transfers", () => 
                DataContainerFacade.GetObjectProperty<SITransfer, long, IList>(transfer.ID, "Transfers"));
        }

        public static IList<ReqTransfer> GetReqTransfers(this SITransfer transfer)
        {
            return DataContainerFacade.GetExtensionData(transfer, "ReqTransfers", () => 
                DataContainerFacade.GetListByProperty<ReqTransfer>("TransferListID", transfer.ID));
        }

        public static IList GetUKPlans(this SITransfer transfer)
        {
            return DataContainerFacade.GetExtensionData(transfer, "UKPlans", () => 
                DataContainerFacade.GetObjectProperty<SITransfer, long, IList>(transfer.ID, "UKPlans"));
        }

        public static Contract GetContract(this SITransfer transfer)
        {
            return DataContainerFacade.GetExtensionData(transfer, "Contract", () => 
                DataContainerFacade.GetByID<Contract, long>(transfer.ContractID));
        }
    }
}
