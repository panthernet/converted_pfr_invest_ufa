﻿using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class AsgFinTrExtension
    {
        public static Finregister GetFinregister(this AsgFinTr pAsgFinTr)
        {
            return DataContainerFacade.GetExtensionData(pAsgFinTr, "Finregister", () => DataContainerFacade.GetByID<Finregister, long>(pAsgFinTr.FinregisterID));
        }

        public static PfrBankAccount GetPFRBankAccount(this AsgFinTr pAsgFinTr)
        {
            return DataContainerFacade.GetExtensionData(pAsgFinTr, "PFRBankAccount",
                () => DataContainerFacade.GetByID<PfrBankAccount, long>(pAsgFinTr.PFRBankAccountID));
        }
    }
}
