﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
	public static class IncomesecExtension
	{
        public static Curs GetCurs(this Incomesec p_incomesec)
        {
            return DataContainerFacade.GetExtensionData<Curs>(p_incomesec, "Curs", delegate()
            {
                return DataContainerFacade.GetByID<Curs, long>(p_incomesec.CursID);
            });
        }

        public static PfrBankAccount GetPfrBankAccount(this Incomesec p_incomesec)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_incomesec, "PfrBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(p_incomesec.PFRBankAccountID);
            });
        }

        public static Portfolio GetPortfolio(this Incomesec p_incomesec)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(p_incomesec, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(p_incomesec.PortfolioID);
            });
        }
	}
}
