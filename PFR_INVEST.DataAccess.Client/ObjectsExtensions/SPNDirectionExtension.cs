﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class SPNDirectionExtension
    {
        public static IList GetOperations(this SPNDirection p_Direction)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_Direction, "Operations", delegate()
            {
                return DataContainerFacade.GetObjectProperty<SPNDirection, long, IList>(p_Direction.ID, "Operations");
            });
        }
    }
}
