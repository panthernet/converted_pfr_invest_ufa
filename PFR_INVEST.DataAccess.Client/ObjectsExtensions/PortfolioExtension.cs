﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class PortfolioExtension
    {
        public static IList GetPortfolioPFRBankAccountList(this Portfolio p_portfolio)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_portfolio, "PortfolioToPFRBankAccounts", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Portfolio, long, IList>(p_portfolio.ID, "PortfolioToPFRBankAccounts");
            });
        }
    }
}
