﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class ERZLNotifyExtension
    {
        public static ERZL GetERZL(this ERZLNotify p_erzlNotify)
        {
            return DataContainerFacade.GetExtensionData<ERZL>(p_erzlNotify, "ERZL", delegate()
            {
                return DataContainerFacade.GetByID<ERZL, long>(p_erzlNotify.ERZL_ID);
            });
        }

        public static Account GetCreditAccount(this ERZLNotify p_erzlNotify)
        {
            return DataContainerFacade.GetExtensionData<Account>(p_erzlNotify, "CreditAccount", delegate()
            {
                return DataContainerFacade.GetByID<Account, long>(p_erzlNotify.CrAccID);
            });
        }

        public static Account GetDebitAccount(this ERZLNotify p_erzlNotify)
        {
            return DataContainerFacade.GetExtensionData<Account>(p_erzlNotify, "DebitAccount", delegate()
            {
                return DataContainerFacade.GetByID<Account, long>(p_erzlNotify.DbtAccID);
            });
        }
    }
}
