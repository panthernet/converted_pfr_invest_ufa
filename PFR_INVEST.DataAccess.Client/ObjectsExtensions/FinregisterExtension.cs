﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class FinregisterExtension
    {
        public static Register GetRegister(this Finregister p_finregister)
        {
            if (p_finregister != null)
                return DataContainerFacade.GetExtensionData<Register>(p_finregister, "Register", delegate()
                {
                    return DataContainerFacade.GetByID<Register, long>(p_finregister.RegisterID);
                });
            else
                return null;
        }

        public static Account GetCreditAccount(this Finregister p_finregister)
        {
            return DataContainerFacade.GetExtensionData<Account>(p_finregister, "CreditAccount", delegate()
            {
                return DataContainerFacade.GetByID<Account, long>(p_finregister.CrAccID);
            });
        }

        public static Account GetDebitAccount(this Finregister p_finregister)
        {
            return DataContainerFacade.GetExtensionData<Account>(p_finregister, "DebitAccount", delegate()
            {
                return DataContainerFacade.GetByID<Account, long>(p_finregister.DbtAccID);
            });
        }

        /*public static IList GetCorrList(this Finregister p_finregister)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_finregister, "CorrList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Finregister, long, IList>(p_finregister.ID, "CorrList");
            });
        }
        */
        /// <summary>
        /// Выбрать все платежные поручения для финреестра
        /// </summary>
        public static IList GetDraftsList(this Finregister p_finregister)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_finregister, "DraftsList", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Finregister, long, IList>(p_finregister.ID, "DraftsList");
            });
        }

        public static long GetDraftsListCount(this Finregister p_finregister)
        {
            return DataContainerFacade.GetObjectPropertyCount<Finregister>(p_finregister.ID, "DraftsList");
        }

        public static IList<Finregister> GetChildFinregistersList(this Finregister p_finregister)
        {
            return DataContainerFacade.GetListByProperty<Finregister>("ParentFinregisterID", p_finregister.ID);
        }

        public static Finregister GetParentFinregister(this Finregister p_finregister)
        {
            return p_finregister.ParentFinregisterID == null ? null : DataContainerFacade.GetByID<Finregister>(p_finregister.ParentFinregisterID);
        }

        /// <summary>
        /// Загрузка всех счетов за 1 запрос к базе
        /// </summary>
        /// <param name="list"></param>
        public static void PopulateBankAccounts(this IEnumerable<Finregister> list)
        {
            if (list == null) return;

            var ids = list.Select(fr => fr.CrAccID)
                        .Concat(list.Select(fr => fr.DbtAccID))
                        .Distinct().ToList();
            var acc = DataContainerFacade.GetListByPropertyCondition<Account>("ID", new ArrayList(ids), "in");

            foreach (var fr in list)
            {
                fr.ExtensionData = fr.ExtensionData ?? new Dictionary<string, object>();
				const string debitName = "DebitAccount";
				const string creditName = "CreditAccount";
				if(fr.ExtensionData.ContainsKey(debitName)) fr.ExtensionData.Remove(debitName);
				if(fr.ExtensionData.ContainsKey(creditName)) fr.ExtensionData.Remove(creditName);
                fr.ExtensionData.Add(debitName, acc.FirstOrDefault(a => a.ID == fr.DbtAccID));
                fr.ExtensionData.Add(creditName, acc.FirstOrDefault(a => a.ID == fr.CrAccID));
            }

        }
    }
}
