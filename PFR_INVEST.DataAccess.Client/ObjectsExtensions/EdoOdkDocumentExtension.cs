﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client
{
    using PFR_INVEST.DataObjects.Interfaces;

    public static class EdoOdkDocumentExtension
    {
        private static Contract GetContarctById(BaseDataObject value, long Id)
        {
            return DataContainerFacade.GetExtensionData<Contract>(value, "Contract", delegate()
            {
                return DataContainerFacade.GetByID<Contract, long>(Id);
            });
        }

        public static Contract GetContract(this EdoOdkF050 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF010 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF012 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF015 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }


        public static Contract GetContract(this EdoOdkF020 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF025 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }
        public static Contract GetContract(this EdoOdkF022 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }
        public static Contract GetContract(this EdoOdkF060 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF070 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Contract GetContract(this EdoOdkF080 value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static Year GetYear(this EdoOdkF060 value)
        {
            if (value != null && value.YearId.HasValue)
                return DataContainerFacade.GetExtensionData<Year>(value, "Year", delegate()
                {
                    return DataContainerFacade.GetByID<Year, long>(value.YearId.Value);
                });
            else
                return null;
        }

        public static Year GetYear(this EdoOdkF070 value)
        {
            if (value != null && value.YearId.HasValue)
                return DataContainerFacade.GetExtensionData<Year>(value, "Year", delegate()
                {
                    return DataContainerFacade.GetByID<Year, long>(value.YearId.Value);
                });
            else
                return null;
        }

        public static IList<F80Deal> GetDeals(this EdoOdkF080 value)
        {
            if (value != null)
            {
                var list = DataContainerFacade.GetExtensionData<IList>(value, "Deals", delegate()
                {
                    return DataContainerFacade.GetObjectProperty<EdoOdkF080, long, IList>(value.ID, "Deals");
                });
                if (list != null)
                    return list.Cast<F80Deal>().ToList();
                else
                    return new List<F80Deal>();
            }
            else
                return new List<F80Deal>();
        }

        public static EdoOdkF401402 GetDocument(this F401402UK value)
        {
            if (value != null && value.EdoID.HasValue)
                return DataContainerFacade.GetExtensionData<EdoOdkF401402>(value, "Document", delegate()
                {
                    return DataContainerFacade.GetByID<EdoOdkF401402, long>(value.EdoID.Value);
                });
            else
                return null;
        }

        public static Contract GetContract(this F401402UK value)
        {
            if (value != null && value.ContractId.HasValue)
                return GetContarctById(value, value.ContractId.Value);
            else
                return null;
        }

        public static IList<T> GetGroup<T>(this EdoOdkF022 value) where T : IF022Part
        {
            if (value != null && value.ContractId.HasValue)
                return DataContainerFacade.GetExtensionData<IList<T>>(value, typeof(T).Name, delegate()
                {
                    return DataContainerFacade.GetListByProperty<T>("EdoID", value.ID);
                });
            else
                return new List<T>();
        } 
        
        public static IList<T> GetGroup<T>(this EdoOdkF025 value) where T : IF025Part
        {
            if (value != null && value.ContractId.HasValue)
                return DataContainerFacade.GetExtensionData<IList<T>>(value, typeof(T).Name, delegate()
                {
                    return DataContainerFacade.GetListByProperty<T>("EdoID", value.ID);
                });
            else
                return new List<T>();
        }

        public static IList<T> GetGroup<T>(this EdoOdkF026 value) where T : IF026Part
        {
            if (value != null)
                return DataContainerFacade.GetExtensionData<IList<T>>(value, typeof(T).Name, delegate()
                {
                    return DataContainerFacade.GetListByProperty<T>("EdoID", value.ID);
                });
            else
                return new List<T>();
        }

    }
}
