﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions
{
	public static class MarketPriceExtension
	{
		public static Security GetSecurity(this MarketPrice marketPrice)
		{
			if (marketPrice != null)

				return DataContainerFacade.GetExtensionData<Security>(marketPrice, "Security", delegate()
				{
					return DataContainerFacade.GetByID<Security, long>(marketPrice.SecurityId);
				});
			else
				return null;
		}
	}
}
