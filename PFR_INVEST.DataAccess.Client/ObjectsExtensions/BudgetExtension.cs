﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class BudgetExtension
    {
        public static IList GetCorrections(this Budget p_budget)
        {
            return DataContainerFacade.GetExtensionData<IList>(p_budget, "Corrections", delegate()
            {
                return DataContainerFacade.GetObjectProperty<Budget, long, IList>(p_budget.ID, "Corrections");
            });
        }

        public static Portfolio GetPortfolio(this Budget p_budget)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(p_budget, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(p_budget.PortfolioID);
            });
        }
    }
}
