﻿using System.Collections.Generic;
using PFR_INVEST.DataAccess.Client;
using System.Collections;

namespace PFR_INVEST.DataObjects
{
    public static class OrderExtension
    {
        public static Portfolio GetPortfolio(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "Portfolio", () => DataContainerFacade.GetByID<Portfolio, long>(p_order.PortfolioID));
        }

        public static PfrBankAccount GetTradeAccount(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "TradeAccount", () => DataContainerFacade.GetByID<PfrBankAccount, long>(p_order.TradeAccountID));
        }

        public static PfrBankAccount GetDEPOAccount(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "DEPOAccount", () => DataContainerFacade.GetByID<PfrBankAccount, long>(p_order.DEPOAccountID));
        }

        public static PfrBankAccount GetCurrentAccount(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "CurrentAccount", () => DataContainerFacade.GetByID<PfrBankAccount, long>(p_order.CurrentAccountID));
        }

        public static PfrBankAccount GetTransitAccount(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "TransitAccount", () => DataContainerFacade.GetByID<PfrBankAccount, long>(p_order.TransitAccountID));
        }


        public static LegalEntity GetBankAgent(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "BankAgent", () => DataContainerFacade.GetByID<LegalEntity, long>(p_order.BankAgentID));
        }

		public static Person GetPerson(this CbOrder p_order)
        {
			return DataContainerFacade.GetExtensionData(p_order, "Person", () => DataContainerFacade.GetByID<Person, long>(p_order.FIO_ID));
        }

        public static IList GetCbInOrderList(this CbOrder p_order)
        {
            return DataContainerFacade.GetExtensionData(p_order, "CbInOrderList", () => DataContainerFacade.GetObjectProperty<CbOrder, long, IList>(p_order.ID, "CbInOrderList"));
        }

        public static CbOrder GetParent(this CbOrder p_order)
        {
            return p_order == null || !p_order.ParentId.HasValue ? null : DataContainerFacade.GetExtensionData(p_order, "Parent", () => DataContainerFacade.GetByID<CbOrder, long>(p_order.ParentId));
        }

        //public static IList GetChildrenList(this CbOrder p_order)
        //{
        //    return DataContainerFacade.GetExtensionData<IList>(p_order, "Children", delegate()
        //    {
        //        return DataContainerFacade.GetObjectProperty<CbOrder, long, IList>(p_order.ID, "Children");
        //    });
        //}

        public static IList<CbOrder> GetChildrenList(this CbOrder p_order)
        {
            return DataContainerFacade.GetListByProperty<CbOrder>("ParentId", p_order.ID);
        }


        public static PfrBankAccount GetPfrBankAccount(this CbOrder p_order)
        {
             if (p_order.TradeAccountID != null)
            {
                return p_order.GetTradeAccount();
            }
            if (p_order.TransitAccountID != null)
            {
                return p_order.GetTransitAccount();
            }
            if (p_order.CurrentAccountID != null)
            {
                return p_order.GetCurrentAccount();
            }
            if (p_order.DEPOAccountID != null)
            {
                return p_order.GetDEPOAccount();
            }
            return null;
        }

        //public static FIO GetPerson(this CbOrder p_order)
        //{
        //    return DataContainerFacade.GetByID<FIO>(p_order.FIO_ID);
        //}
    }
}
