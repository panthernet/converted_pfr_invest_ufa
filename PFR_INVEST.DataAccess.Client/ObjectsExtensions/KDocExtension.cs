﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Client
{
	public static class KDocExtension
	{
		public static Portfolio GetPortfolioSPN(this KDoc p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<Portfolio>(p_aps, "Portfolio", delegate()
				{
					return DataContainerFacade.GetByID<Portfolio, long>(p_aps.PortfolioID);
				});
			else
				return null;
		}

		public static Portfolio GetPortfolioDSV(this KDoc p_aps)
		{
			if (p_aps != null)
				return DataContainerFacade.GetExtensionData<Portfolio>(p_aps, "Portfolio", delegate()
				{
					return DataContainerFacade.GetByID<Portfolio, long>(p_aps.PortfolioID);
				});
			else
				return null;
		}
	}
}
