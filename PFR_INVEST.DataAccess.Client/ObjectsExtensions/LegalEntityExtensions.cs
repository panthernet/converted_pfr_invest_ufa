﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class LegalEntityExtensions
    {
        public static IList GetOldNPFNames(this LegalEntity p_legalentity)
        {
            return DataContainerFacade.GetExtensionData(p_legalentity, "OldNPFNames", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "OldNPFNames"));
        }

        public static Contragent GetContragent(this LegalEntity p_legalentity)
        {
            return p_legalentity?.ContragentID != null ? DataContainerFacade.GetExtensionData(p_legalentity, "Contragent", () => DataContainerFacade.GetByID<Contragent, long>(p_legalentity.ContragentID)) : null;
        }

        public static IList GetLicensiesList(this LegalEntity p_legalentity)
        {
            return DataContainerFacade.GetExtensionData(p_legalentity, "Licensies", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "Licensies"));
        }

        public static IList<Contract> GetContracts(this LegalEntity p_legalentity)
        {
            if (p_legalentity != null)
            {
                var list = DataContainerFacade.GetExtensionData(p_legalentity, "Contracts", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "Contracts"));
                return list?.Cast<Contract>().ToList() ?? new List<Contract>();
            }
            return new List<Contract>();
        }

        public static IList GetPfrBankAccounts(this LegalEntity p_legalentity)
        {
            return DataContainerFacade.GetExtensionData(p_legalentity, "PfrBankAccounts", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "PfrBankAccounts"));
        }

        public static IList GetContacts(this LegalEntity p_legalentity)
        {
            return DataContainerFacade.GetExtensionData(p_legalentity, "Contacts", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "Contacts"));
        }

        public static LegalStatus GetLegalStatus(this LegalEntity p_legalentity)
        {
            return DataContainerFacade.GetExtensionData(p_legalentity, "LegalStatus", () => DataContainerFacade.GetByID<LegalStatus, long>(p_legalentity.LegalStatusID));
        }

        public static IList<BankAccount> GetBankAccounts(this LegalEntity p_legalentity)
        {
            return p_legalentity == null ? new List<BankAccount>() : 
                DataContainerFacade.GetExtensionData(p_legalentity, "BankAccounts", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(p_legalentity.ID, "BankAccounts")).Cast<BankAccount>().ToList();
        }

        public static BankAccount GetBankAccount(this LegalEntity p_legalentity)
        {
            return p_legalentity.GetBankAccounts().FirstOrDefault(ba => ba.CloseDate == null);
        }

        //[Obsolete("Не работает. ContragentID is null")]
        //public static IList<Deposit> GetBankDeposits(this LegalEntity legalEntity)
        //{
        //    // Этот код уже двано не работает. 
        //    //if (legalEntity != null && legalEntity.ContragentID.HasValue)
        //    //{
        //    //    return DataContainerFacade.GetListByProperty<Deposit>("ContragentID", legalEntity.ContragentID);
        //    //}

        //    return new List<Deposit>();
        //}

        public static IList<BankConclusion> GetBankStatuses(this LegalEntity legalEntity)
        {
            if (string.IsNullOrEmpty(legalEntity?.RegistrationNum))
                return new List<BankConclusion>();

            var code = legalEntity.RegistrationNum.Trim();

           // var elements = DataContainerFacade.GetListByProperty<Element>("Key", (long) Element.Types.BankConclusionNames).Select(a=> a.ID).ToList();
            IList<BanksConclusion> imports = DataContainerFacade.GetList<BanksConclusion>();
            if (imports.Count == 0)
                return new List<BankConclusion>();

            IList<BankConclusion> statuses = new List<BankConclusion>();
            var existingStatuses = DataContainerFacade.GetListByProperty<BankConclusion>("Code", code);


            foreach (var x in imports)
            {
                var status = existingStatuses.FirstOrDefault(s => s.ImportID == x.ID && s.Code == code);
                if (status == null)
                {
                    status = new BankConclusion
                    {
                        ImportDate = x.ImportDate,
                        BankStatusBool = false,
                    };
                }else status.BankStatusBool = true;

                status.ElementName = x.ElementName;
                status.ImportElementID = x.ElementID;

                statuses.Add(status);
            }

            return statuses;
        }


        public static IList<LegalEntityHead> GetHeads(this LegalEntity legalEntity)
        {
            return legalEntity != null ? DataContainerFacade.GetListByProperty<LegalEntityHead>("LegalEntityID", legalEntity.ID) : 
                new List<LegalEntityHead>();
        }

        public static IList<OwnCapitalHistory> GetOwnCapitalHistory(this LegalEntity legalEntity)
        {
            return legalEntity != null ? DataContainerFacade.GetListByProperty<OwnCapitalHistory>("LegalEntityID", legalEntity.ID) :
                new List<OwnCapitalHistory>();
        }

        public static IList<LegalEntityRating> GetRatings(this LegalEntity legalEntity)
        {
            return legalEntity != null ? DataContainerFacade.GetListByProperty<LegalEntityRating>("LegalEntityID", legalEntity.ID) : 
                new List<LegalEntityRating>();
        }

        public static IList<BankStockCode> GetStockCodes(this LegalEntity legalEntity)
        {
            return legalEntity != null ? DataContainerFacade.GetListByProperty<BankStockCode>("BankID", legalEntity.ID) : 
                new List<BankStockCode>();
        }

        public static IList<LegalEntityChief> GetChiefs(this LegalEntity legalEntity)
        {
            if (legalEntity == null || legalEntity.ID <= 0)
                return new List<LegalEntityChief>();
            return DataContainerFacade.GetExtensionData(legalEntity, "Chiefs", () => DataContainerFacade.GetObjectProperty<LegalEntity, long, IList>(legalEntity.ID, "Chiefs")).Cast<LegalEntityChief>().ToList();
        }
    }
}
