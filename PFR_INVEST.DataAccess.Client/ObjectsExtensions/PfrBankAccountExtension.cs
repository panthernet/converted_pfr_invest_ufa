﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
	public static class PfrBankAccountExtension
	{
		public static Currency GetCurrency(this PfrBankAccount pb_acc)
		{
			if (pb_acc != null && pb_acc.CurrencyID.HasValue)
				return DataContainerFacade.GetExtensionData<Currency>(pb_acc, "Currency", delegate()
				{
					return DataContainerFacade.GetByID<Currency, long>(pb_acc.CurrencyID);
				});
			else
				return null;
		}

		public static LegalEntity GetLegalEntity(this PfrBankAccount pb_acc)
		{
			return DataContainerFacade.GetExtensionData<LegalEntity>(pb_acc, "LegalEntity", delegate()
			{
				return DataContainerFacade.GetByID<LegalEntity, long>(pb_acc.LegalEntityBankID);
			});
		}

		public static AccBankType GetAccountType(this PfrBankAccount pb_acc)
		{
			return DataContainerFacade.GetExtensionData<AccBankType>(pb_acc, "AccountType", delegate()
			{
				return DataContainerFacade.GetByID<AccBankType, long>(pb_acc.AccountTypeID);
			});
		}
	}
}
