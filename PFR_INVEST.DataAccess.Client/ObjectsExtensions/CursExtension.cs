﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Collections;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
	public static class CursExtension
	{
        public static Currency GetCurrency(this Curs p_curs)
        {
            return DataContainerFacade.GetExtensionData<Currency>(p_curs, "Currency", delegate()
            {
                return DataContainerFacade.GetByID<Currency, long>(p_curs.CurrencyID);
            });
        }
	}
}
