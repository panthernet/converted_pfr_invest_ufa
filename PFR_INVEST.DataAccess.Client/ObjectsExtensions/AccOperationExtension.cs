﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.DataObjects
{
    public static class AccOperationExtension
    {
        public static Portfolio GetPortfolio(this AccOperation p_acc)
        {
            return DataContainerFacade.GetExtensionData<Portfolio>(p_acc, "Portfolio", delegate()
            {
                return DataContainerFacade.GetByID<Portfolio, long>(p_acc.PortfolioID);
            });
        }

        public static Currency GetCurrency(this AccOperation p_acc)
        {
            return DataContainerFacade.GetExtensionData<Currency>(p_acc, "Currency", delegate()
            {
                return DataContainerFacade.GetByID<Currency, long>(p_acc.CurrencyID);
            });
        }

        public static PfrBankAccount GetPFRBankAccount(this AccOperation p_acc)
        {
            return DataContainerFacade.GetExtensionData<PfrBankAccount>(p_acc, "PFRBankAccount", delegate()
            {
                return DataContainerFacade.GetByID<PfrBankAccount, long>(p_acc.PFRBankAccountID);
            });
        }

        public static AccOperationKind GetKind(this AccOperation p_acc)
        {
            return DataContainerFacade.GetExtensionData<AccOperationKind>(p_acc, "Kind", delegate()
            {
                return DataContainerFacade.GetByID<AccOperationKind, long>((long)p_acc.KindID);
            });
        }
    }
}
