﻿
namespace PFR_INVEST.DBUtils
{
    using Common.Logger;
    using System;
    using System.Collections.Generic;
    using System.Data.Odbc;
    using System.Text;

    public class ResetIdentityStartValue : IDBUtil
    {
        private string connectionString;
        private string schemeName;
        public string ActionName { get; private set; }

        public ResetIdentityStartValue(ExecutionContext executionContext)
        {
            this.connectionString = executionContext.ConnectionString;
            this.schemeName = executionContext.SchemaName;
            this.ActionName = "RESETIDENTITYSTARTVALUE";
        }

        public bool Execute()
        {
            bool result = true;
            using (OdbcConnection odbcConnection = new OdbcConnection(connectionString))
            {
                try
                {
                    odbcConnection.Open();
                }
                catch (Exception ex)
                {
                    Logger.Instance.Error("Error. " + ex.Message);
                    return false;
                }
                Logger.Instance.Info("Выполняется поиск таблиц, индексы которых требуется пересчитать.");

                string commandText =
                    string.Format(
                        "select tbname  from sysibm.syscolumns s  where tbcreator = '{0}' and  keyseq=1 and generated!='' and  (typename='BIGINT' or typename='INTEGER') and colcard>0  order by tbname ;",
                        this.schemeName);

                using (OdbcCommand command = new OdbcCommand(commandText, odbcConnection))
                {
                    command.CommandType = System.Data.CommandType.Text;
                    List<string> tableNames = new List<string>();
                    using (OdbcDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(0))
                                {
                                    tableNames.Add(reader[0].ToString());
                                }
                            }
                        }
                    }
                    Logger.Instance.Info(string.Format("Найдено таблиц: {0}", tableNames.Count));
                    foreach (var tableName in tableNames)
                    {
                        command.CommandText = string.Format(
                            "call {0}.RESETIDENTITYSTARTVALUE('{0}', '{1}');", this.schemeName, tableName);

                        Logger.Instance.Info(string.Format("Идет обработка таблицы {0}...", tableName));
                        try
                        {
                            command.ExecuteNonQuery();
                            Logger.Instance.Info(string.Format("Таблица {0} переиндексирована.", tableName));
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.Error(string.Format("Произошла следующая ошибка - {0}.", ex.Message));
                            result = false;
                        }
                    }
                }
            }
            return result;
        }
    }
}
