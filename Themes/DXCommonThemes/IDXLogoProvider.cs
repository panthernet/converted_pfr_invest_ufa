﻿namespace DXCommonThemes
{
    public interface IDXLogoProvider
    {
        string LogoUri { get; }
    }
}
