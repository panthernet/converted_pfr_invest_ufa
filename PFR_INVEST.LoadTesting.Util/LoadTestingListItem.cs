﻿using System;
using System.Timers;
using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Util
{
    public class LoadTestingListItem
    {
        public int Number { get; set; }
        public Type Scenario { get; set; }
        public string Step { get; set; }
        public double FinishedPercent { get; set; }
        public int ErrorCount { get; set; }
        public string ScenarioReport { get; set; }
        public string ScenarioName { get; set; }

        public DateTime ElapsedTime { get; set; }
        public string MemoryUsed { get; set; }
        public List<double> MemoryStats { get; set; }
        public List<MeasureItem> Measures { get; set; }
        public string ProcessorUsed { get; set; }
        public bool TimerActive { get; set; }

        public LoadTestingListItem()
        {
            MemoryStats = new List<double>();
            Measures = new List<MeasureItem>();
        }
    }

    public class MeasureItem
    {
        public string Text { get; set; }
        public double Value { get; set; }
    }
}
