﻿using System.Windows;
using System.Reflection;
using System;

namespace PFR_INVEST.LoadTesting.Util
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static LoadTestingController TestingController { get; private set; }
		public App()
		{
			AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);
			TestingController = new LoadTestingController();
		}

		Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			if (!args.Name.Contains(".resources,"))
			{
				string message = string.Format("Не найдена требуемая библиотека '{0}'", args.Name);
				MessageBox.Show(message);
			}
			return null;
		}
	}
}
