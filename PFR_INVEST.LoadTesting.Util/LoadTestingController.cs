﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using DevExpress.Utils;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.LoadTesting.Scenarios;

namespace PFR_INVEST.LoadTesting.Util
{
    public class LoadTestingController : MarshalByRefObject, ILoadApplicationController, INotifyPropertyChanged
    {
        public Type ScenarioType = typeof(ILoadScenario);

        Log Logger;

        #region TestingIsNotInProgress
        private bool m_TestingIsNotInProgress = true;
        public bool TestingIsNotInProgress
        {
            get
            {
                return m_TestingIsNotInProgress;
            }
            private set
            {
                if (!value) { OverallTime = DateTime.MinValue; lock (memlocker) { foreach (var item in TestItems) item.MemoryStats.Clear(); } OnPropertyChanged("OverallTime"); ElapsedTimer.Start(); }
                else { ElapsedTimer.Stop(); }

                m_TestingIsNotInProgress = value;
                m_AllowScenarioEditing = value ? DefaultBoolean.True : DefaultBoolean.False;
                OnPropertyChanged("TestingIsNotInProgress");
                OnPropertyChanged("TestingIsInProgress");
                OnPropertyChanged("AllowScenarioEditing");
            }
        }
        public bool TestingIsInProgress
        {
            get
            {
                return !m_TestingIsNotInProgress;
            }
        }
        private DefaultBoolean m_AllowScenarioEditing = DefaultBoolean.True;
        public DefaultBoolean AllowScenarioEditing
        {
            get
            {
                return m_AllowScenarioEditing;
            }
        }
        #endregion

        #region Stopped
        readonly object lockObj = new object();
        bool m_stopped = true;
        bool Stopped
        {
            get
            {
                lock (lockObj)
                {
                    return m_stopped;
                }
            }
            set
            {
                lock (lockObj)
                {
                    m_stopped = value;
                }
            }
        }
        #endregion

        #region TestItems
        public List<LoadTestingListItem> m_TestItems;
        public List<LoadTestingListItem> TestItems
        {
            get
            {
                return m_TestItems;
            }
            private set
            {
                m_TestItems = value;
                RefreshTestItemsList();
            }
        }
        #endregion

        #region ItemsCount
        private int m_ItemsCount = 2;
        public int ItemsCount
        {
            get
            {
                return m_ItemsCount;
            }
            set
            {
                m_ItemsCount = value;
                InitTestItems(false);
                OnPropertyChanged("ItemsCount");
            }
        }
        #endregion

        #region CycleCount
        private int m_CycleCount = 2;
        public int CycleCount
        {
            get
            {
                return m_CycleCount;
            }
            set
            {
                m_CycleCount = value;
                InitTestItems(false);
                OnPropertyChanged("CycleCount");
            }
        }
        #endregion

        private int m_LatencyDelay;
        public int LatencyDelay
        {
            get
            {
                return m_LatencyDelay;
            }
            set
            {
                m_LatencyDelay = value;
                OnPropertyChanged("LatencyDelay");
            }
        }

        #region LaunchTime
        private TimeSpan? m_LaunchTime = DateTime.Now.TimeOfDay;
        public TimeSpan? LaunchTime
        {
            get
            {
                return m_LaunchTime;
            }
            set
            {
                m_LaunchTime = value;
                OnPropertyChanged("LaunchTime");
            }
        }
        #endregion

        #region CurrentTime
        private string currenttime;
        public string CurrentTime
        {
            get { return currenttime; }
            set { currenttime = value; OnPropertyChanged("CurrentTime"); }
        }
        System.Timers.Timer cttimer = new System.Timers.Timer(1000) { AutoReset = true };
        #endregion

        #region Scenarios
        private List<ScenarioItem> m_Scenarios;
        public List<ScenarioItem> Scenarios
        {
            get
            {
                return m_Scenarios;
            }
            set
            {
                m_Scenarios = value;
                OnPropertyChanged("Scenarios");
            }
        }
        #endregion

        #region IsDelayEnabled
        private bool m_Isdelayenabled;
        public bool IsDelayEnabled
        {
            get
            {
                return m_Isdelayenabled;
            }
            set
            {
                m_Isdelayenabled = value;
                OnPropertyChanged("IsDelayEnabled");
            }
        }
        #endregion
        private readonly object InstanceLock = new object();

        private bool isnewscenariorandom;
        public bool IsNewScenarioRandom
        {
            get
            {
                return isnewscenariorandom;
            }
            set
            {
                isnewscenariorandom = value;
                OnPropertyChanged("IsNewScenarioRandom");
            }
        }

        public LoadTestingController()
        {
            PopulateLoadScenarios();
            InitTestItems(true);
            ElapsedTimer.Elapsed += ElapsedTimer_Elapsed;
            OverallTime = DateTime.MinValue;
            IsDelayEnabled = true;
            LatencyDelay = 5000;
            cttimer.Elapsed += cttimer_Elapsed;
            cttimer.Start();

            CycleCount = 1;
            ItemsCount = 1;
            Logger = new Log("exceptions.log");
        }

        #region Timer operations

        void cttimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            CurrentTime = string.Format("({0})", DateTime.Now.ToLongTimeString());
        }

        private System.Timers.Timer ElapsedTimer = new System.Timers.Timer() { Interval = 1000, AutoReset = true };
        public DateTime OverallTime { get; set; }
        readonly object memlocker = new object();
        void ElapsedTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var item in TestItems)
            {
                if (item.TimerActive)
                {
                    try
                    {
                        item.ElapsedTime = item.ElapsedTime.AddSeconds(1);

                        lock (InstanceLock)
                        {
                            var index = InstancesByDomains.Values.ToList().FindIndex(a => a.InstanceID == item.Number);
                            if (index != -1)
                            {
                                lock (memlocker)
                                {
                                    //item.MemoryStats.Add(InstancesByDomains.Keys.ToList()[index].MonitoringSurvivedMemorySize / 1024);
                                    //item.MemoryUsed = string.Format("{0}кб", (int)(item.MemoryStats.Sum() / item.MemoryStats.Count));
                                    var ptime = InstancesByDomains.Keys.ToList()[index].MonitoringTotalProcessorTime;
                                    item.ProcessorUsed = string.Format("{0}:{1}.{2}", ptime.Minutes.ToString("D2"), ptime.Seconds.ToString("D2"), ptime.Milliseconds.ToString("D2"));
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
            OverallTime = OverallTime.AddSeconds(1);
            OnPropertyChanged("OverallTime");

            //System.Diagnostics.Process.GetCurrentProcess().

            //TestMemory =  AppDomain.MonitoringSurvivedProcessMemorySize / 1024; //kb
            //OnPropertyChanged("TestMemory");
            RefreshTestItemsList();
        }
        #endregion

        private void InitTestItems(bool clear)
        {
            List<LoadTestingListItem> lst = new List<LoadTestingListItem>();
            for (int i = 0; i < ItemsCount; i++)
            {
                var rndnum = 0;
                if (IsNewScenarioRandom)
                    rndnum = rnd.Next(0, Scenarios.Count - 1);
                var item = new LoadTestingListItem()
                {
                    Number = i,
                    Scenario = clear || (!clear && i >= TestItems.Count) ? Scenarios[rndnum].Type : TestItems[i].Scenario,
                    ScenarioName = clear || (!clear && i >= TestItems.Count) ? Scenarios[rndnum].Name : Scenarios.Find(sc => sc.Type == TestItems[i].Scenario).Name,
                    MemoryUsed = clear || (!clear && i >= TestItems.Count) ? "0кб" : TestItems[i].MemoryUsed,
                    ProcessorUsed = clear || (!clear && i >= TestItems.Count) ? "0:00.0" : TestItems[i].ProcessorUsed,
                    Measures = clear || (!clear && i >= TestItems.Count) ? new List<MeasureItem>() : TestItems[i].Measures,
                    ErrorCount = 0,
                    Step = null
                };
                //item.ScenarioName = ((ScenarioBase)Activator.CreateInstance(item.Scenario)).ScenarioName;
                lst.Add(item);
            }
            TestItems = lst;
        }

        private readonly Random rnd = new Random();
        private Type GetRandomScenario()
        {
            if (Scenarios != null)
                return Scenarios[rnd.Next(Scenarios.Count)].Type;
            return null;
        }

        private void PopulateLoadScenarios()
        {
            var types = typeof(ILoadScenario).Assembly.GetTypes()
                        .Where(t => ScenarioType.IsAssignableFrom(t) && !t.IsAbstract)
                        .ToList();
            Scenarios = new List<ScenarioItem>();
            //StreamWriter sw = File.CreateText("Scenarios.txt");
            foreach (var item in types)
            {
                var ScenarioItem = new ScenarioItem() { Name = ((ScenarioBase)Activator.CreateInstance(item)).ScenarioName, Type = item };
                Scenarios.Add(ScenarioItem);
                //sw.WriteLine(ScenarioItem.Name);
            }
            //sw.Close();
            OnPropertyChanged("Scenarios");
        }

        #region RUN / STOP scenarios

        private readonly Dictionary<AppDomain, ILoadAplicationInstance> InstancesByDomains = new Dictionary<AppDomain, ILoadAplicationInstance>();
        public void Run()
        {
            //ElapsedTimer.Start();
            //OverallTime = DateTime.MinValue;
            //OnPropertyChanged("OverallTime");
            foreach (var appd in InstancesByDomains.Keys)
            {
                try
                {
                    AppDomain.Unload(appd);
                }
                catch { }
            }
            InstancesByDomains.Clear();

            TestingIsNotInProgress = false;
            ThreadPool.QueueUserWorkItem(_ =>
            {
                Stopped = false;
                InitTestItems(false);

                //PFR_INVEST.App.log = new Log("error.log");

                AppDomain.MonitoringIsEnabled = true;
                foreach (LoadTestingListItem item in TestItems)
                {
                    try
                    {
                        item.Measures.Clear();
                        item.ElapsedTime = DateTime.MinValue;
                        item.TimerActive = false;
                        AppDomainSetup domainSetup = new AppDomainSetup()
                        {
                            ApplicationBase = Path.GetDirectoryName(ScenarioType.Assembly.Location),
                            ConfigurationFile = ScenarioType.Assembly.Location + ".config",
                            LoaderOptimization = LoaderOptimization.MultiDomainHost
                        };

                        AppDomain childDomain = AppDomain.CreateDomain("Your Child AppDomain", null, domainSetup);
                        childDomain.DomainUnload += childDomain_DomainUnload;
                        ILoadAplicationInstance instance = (ILoadAplicationInstance)childDomain.CreateInstanceAndUnwrap(typeof(ILoadAplicationInstance).Assembly.FullName,
                            typeof(LoadApplicationInstance).FullName);
                        //instance.InstanceID = item.Number;

                        if (Stopped)
                            break;
                        InstancesByDomains.Add(childDomain, instance);
                        DateTime? time = null;
                        if (LaunchTime != null) time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, LaunchTime.Value.Hours, LaunchTime.Value.Minutes, LaunchTime.Value.Seconds);
                        instance.RunScenario(this, item.Scenario, item.Number, CycleCount, time, IsDelayEnabled, LatencyDelay);
                        Thread.Sleep(1000);
                    }
                    catch
                    { }
                }
            });
        }

        public void Stop()
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                Stopped = true;
                lock (InstanceLock)
                {
                    foreach (KeyValuePair<AppDomain, ILoadAplicationInstance> pair in InstancesByDomains)
                    {
                        try
                        {
                            pair.Value.Stop();
                            var iitem = TestItems.Find(item => item.Number == pair.Value.InstanceID);
                            iitem.Step = "Тестирование отменено";
                            iitem.TimerActive = false;
                            AppDomain.Unload(pair.Key);
                        }
                        catch { }
                    }
                    RefreshTestItemsList();
                    InstancesByDomains.Clear();
                    TestingIsNotInProgress = true;
                }
            });
            // ElapsedTimer.Stop();
        }

        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public delegate void RefreshTestItemsListDelegate();
        public event RefreshTestItemsListDelegate OnRefreshTestItemsList;
        protected void RefreshTestItemsList()
        {
            if (OnRefreshTestItemsList != null)
                OnRefreshTestItemsList();
        }

        public delegate void RefreshMeasureDelegate(LoadTestingListItem item);
        public event RefreshMeasureDelegate OnRefreshMeasure;
        protected void RefreshMeasure(LoadTestingListItem item)
        {
            if (OnRefreshMeasure != null)
                OnRefreshMeasure(item);
        }
        #endregion

        #region Callbacks

        public void MeasureCallback(ILoadAplicationInstance instance, string text, double value)
        {
            try
            {
                var tlitem = TestItems.Find(item => item.Number == instance.InstanceID);
                if (tlitem == null) return;
                if (tlitem.Measures.Count == 0 && !text.Contains("Общий:"))
                    tlitem.Measures.Add(new MeasureItem() { Text = text, Value = 0 });
                else if (text.Contains("Общий:"))
                    tlitem.Measures.Add(new MeasureItem() { Text = text, Value = value });
                else
                {
                    if (!tlitem.Measures[tlitem.Measures.Count - 1].Text.Contains("Общий:")) tlitem.Measures[tlitem.Measures.Count - 1].Value = value;
                    tlitem.Measures.Add(new MeasureItem() { Text = text, Value = 0 });
                }
                //tlitem.Measures.Add(new MeasureItem() { Text = text, Value = value });
                RefreshMeasure(tlitem);
            }
            catch { }
        }

        public void StepCallback(ILoadAplicationInstance instance, string stepName)
        {
            var tlitem = TestItems.Find(item => item.Number == instance.InstanceID);
            tlitem.Step = stepName;
            if (!stepName.Contains("Подключение") && !stepName.Contains("Ожидание запуска...") && !tlitem.TimerActive) tlitem.TimerActive = true;
            RefreshTestItemsList();
        }

        public void ErrorCallback(ILoadAplicationInstance instance, int errorCount, int ID)
        {
            TestItems.Find(item => item.Number == ID).ErrorCount = errorCount;
            RefreshTestItemsList();
        }

        public void FinishedPercentCallback(ILoadAplicationInstance instance, double percent)
        {
            TestItems.Find(item => item.Number == instance.InstanceID).FinishedPercent = percent;
            RefreshTestItemsList();
        }

        public void ExceptionCallback(ILoadAplicationInstance instance, Exception ex)
        {
            Logger.WriteException(ex);
        }

        public void ScenarioFinishedCallback(ILoadAplicationInstance instance)
        {
            var inst_id = 0;
            try
            {
                try
                {
                    inst_id = instance.InstanceID;
                }
                catch { return; }
                var tlitem = TestItems.Find(item => item.Number == inst_id);
                tlitem.Step = "Тестирование завершено";
                tlitem.ScenarioReport = instance.ScenarioReport;
                tlitem.TimerActive = false;
                OnPropertyChanged("TestItems");
                foreach (var tm in TestItems)
                {
                    if (tm.Step == "Подключение" || tm.Step == "Ожидание запуска...") return;
                    if (tm.TimerActive) return;
                }
                RefreshTestItemsList();
                TestingIsNotInProgress = true;
                /*lock (InstanceLock)
                {
                    if (InstancesByDomains.Values.Contains(instance))
                    {
                        var key = InstancesByDomains.First(rec => rec.Value == instance).Key;
                        ThreadPool.QueueUserWorkItem(_ =>
                            { Thread.Sleep(2000); try { AppDomain.Unload(key); } catch { } });
                    }
                }*/
            }
            catch { }

        }
        #endregion

        private void childDomain_DomainUnload(object sender, EventArgs e)
        {
            AppDomain child = sender as AppDomain;
            child.DomainUnload -= childDomain_DomainUnload;

            //InstancesByDomains.Remove(child);
            if (InstancesByDomains.Keys.Count == 0)
                TestingIsNotInProgress = true;
            RefreshTestItemsList();
        }

        private const string REPORTFILENAME = "ReportFile.txt";
        public void ShowScenarioReport(string message)
        {
            File.WriteAllText(REPORTFILENAME, message);
            Process.Start(REPORTFILENAME);
        }
    }


    public class ScenarioItem
    {
        public string Name { get; set; }
        public Type Type { get; set; }
    }

    internal class ScenarioComparer : IComparer<ScenarioItem>
    {

        int IComparer<ScenarioItem>.Compare(ScenarioItem x, ScenarioItem y)
        {
            return string.Compare(x.Name, y.Name);
        }
    }

}
