﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using System.Data;

namespace PFR_INVEST.LoadTesting.Util
{
    /// <summary>
    /// Interaction logic for MeasureTable.xaml
    /// </summary>
    public partial class MeasureTable : DXWindow
    {
        public List<TotalMeasureItem> Measures { get; set; }

        public MeasureTable(List<LoadTestingListItem> items)
        {
            InitializeComponent();
            ThemeManager.SetThemeName(this, "Office2007Blue");

            Measures = new List<TotalMeasureItem>();

            RefreshData(items); 
            
        }

        public void UpdateData(List<LoadTestingListItem> items)
        {
            RefreshData(items);
        }

        private void RefreshData(List<LoadTestingListItem> items)
        {
            Measures.Clear();
            foreach (var item in items)
            {
                Measures.AddRange(item.Measures.ConvertAll<TotalMeasureItem>(bg =>
                {
                    return new TotalMeasureItem(item.ScenarioName, bg);
                }));
            }
            mGrid.DataSource = null;
            mGrid.DataSource = Measures;

        }

        private void but_export_Click(object sender, RoutedEventArgs e)
        {
            if (DXMessageBox.Show("Экспортировать данные?","Экспорт", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

            RKLib.ExportData.Export exp = new RKLib.ExportData.Export("Win");

            var dt = new DataTable();
            dt.Columns.Add(new DataColumn("name", typeof(string)));
            dt.Columns.Add(new DataColumn("cycle", typeof(string)));
            dt.Columns.Add(new DataColumn("text", typeof(string)));
            dt.Columns.Add(new DataColumn("extime", typeof(double)));
            foreach (var item in Measures)
            {
                var row = dt.NewRow();
                row.ItemArray = new object[] { item.ScenarioName, item.ScenarioCycleNumber, item.Text, item.ExecutionTime };
                dt.Rows.Add(row);
            }

            var filename = string.Format("CSV/{0}_{1}.{2}.{3}.csv", DateTime.Now.ToShortDateString(), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

            if (!System.IO.Directory.Exists("CSV"))
                System.IO.Directory.CreateDirectory("CSV");
            string sTempFile = System.IO.Path.GetTempFileName();

            exp.ExportDetails(dt, RKLib.ExportData.Export.ExportFormat.CSV, sTempFile);
            string sText = System.IO.File.ReadAllText(sTempFile, Encoding.UTF8);
            sText = sText.Replace("\",\"", "\";\"");
            System.IO.File.WriteAllText(filename, sText, Encoding.UTF8);

            

            //string sText = System.IO.File.ReadAllText(sTempFile, Encoding.Unicode);

            //System.IO.File.WriteAllText(, sText, Encoding.Unicode);

        }
    }

    public class TotalMeasureItem
    {
        public string ScenarioName { get; set; }
        public string ScenarioCycleNumber { get; set; }
        public string Text { get; set; }
        public double ExecutionTime { get; set; }

        public TotalMeasureItem(string name,MeasureItem item)
        {
            var strs = Split(item.Text);
            ScenarioName = name;
            ScenarioCycleNumber = strs[0];
            Text = strs[1];
            ExecutionTime = item.Value;
        }

        private string[] Split(string text)
        {
            var index = text.IndexOf(':');
            if (index > 0)
            {
                return new string[] { text.Remove(index), text.Remove(0, index+1) };
            }
            else return new string[] { "", text};
        }
    }
}
