﻿using System;
#if !WEBCLIENT
using PFR_INVEST.Integration.KIPClient.KIPService;
#else
using PFR_INVEST.Web2.Integration.KIP.KIPService;
#endif

namespace PFR_INVEST.Integration.KIPClient
{
	public class KIPClientFacade
	{
	    private bool _isError;
		public string SendDocument(byte[] file)
		{
		    try
		    {
		        using (var client = new serviceClient())
		        {
		            var result = client.Register(new RegisterRequest {inputData = file});
		            return result.outputData;
		        }
		    }
		    catch (Exception ex)
		    {
		        _isError = true;
		        Logs.Log.Fatal("Exception on KPI::Register() method call!", ex);
		        return null;
		    }
		    finally
		    {
                if(!_isError)
                    Logs.Log.Info("SEND[STEP2]: SUCCESS");
		        _isError = false;
		    }
		}
	}
}
