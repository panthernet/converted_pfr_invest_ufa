﻿using log4net;

namespace PFR_INVEST.Integration.KIPClient
{
    public static class Logs
    {
        internal static readonly ILog Log = LogManager.GetLogger("Integration.KIP.Log");
    }
}
