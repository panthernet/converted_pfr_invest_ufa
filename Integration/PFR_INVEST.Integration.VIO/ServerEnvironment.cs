﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Integration.VIO.BL;

namespace PFR_INVEST.Integration.VIO
{
	public static class ServerEnvironment
	{
		public static string ConfigFilePath => @"Integration\vio\VIO.Settings.xml";
	    public static string DebugFilePath => @"Integration\vio\VIO.Debug.xml";
	    public const string VIO_DEBUG_FOLDER = @"Integration\vio\DebugData";

		public static VIOSettings LoadServerConfig()
		{
			var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigFilePath);
			//var file = System.Web.HttpContext.Current.Server.MapPath(ServerEnvironment.ConfigFilePath);
			var settings = VIOSettings.Load(file);
			return settings;
		}

	    public static List<string> LoadDebugFilesVIO()
	    {
            var fContent = new List<string>();
            Directory.GetFiles( Path.Combine(AppDomain.CurrentDomain.BaseDirectory, VIO_DEBUG_FOLDER), "*.json", SearchOption.TopDirectoryOnly)
                .OrderBy(a=> a)
                .ForEach(a=> fContent.Add(File.ReadAllText(a)));
	        return fContent;
	    }
	}
}
