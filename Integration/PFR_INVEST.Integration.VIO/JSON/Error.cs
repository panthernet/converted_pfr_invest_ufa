﻿using Newtonsoft.Json;

namespace PFR_INVEST.Integration.VIO.JSON
{
    /// <summary>
    /// Описывает ошибку
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        [JsonProperty("errorCode")]
        public int ErrorCode { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
    }
}