﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.Integration.VIO.BL;
using PFR_INVEST.Integration.VIO.Import.UKReport;
using PFR_INVEST.Integration.VIO.JSON;
using PFR_INVEST.Integration.VIO.MQ;
using PFR_INVEST.Integration.VIO.Rest;

namespace PFR_INVEST.Integration.VIO
{
	public class MessageProcessor
	{
		public VIOSettings Settings { get; private set; }

		public MessageProcessor()
		{
            Settings = ServerEnvironment.LoadServerConfig();
		}

		public void ParseMessage(string message)
		{
			Guid msgID = Guid.NewGuid();
			Logs.Messages.InfoFormat("Новое сообщение номер: {0}", msgID);
			Logs.Messages.Info(message);
			Logs.MainLog.InfoFormat("Получено сообщение номер: {0}", msgID);
			Message msg;
            var rm = new RestManager(Settings);
            
			// Парсим сообщение
			try
			{
				msg = JsonConvert.DeserializeObject<Message>(message);
			}
			catch (Exception ex)
			{
				Logs.MainLog.Error("Ошибка парсинга JSON сообщения", ex);
				return;
			}

		    if (msg?.EventType == EventType.Notification)
		    {
		        var docId = msg.DocumentId;

		        var docBody = rm.GetDocumentBody(docId);

		        Logs.MainLog.Info($"Получили документ: {docBody}");

		        var replyMessage = new Message
		        {
		            EventType = EventType.Reply,
		            PeerBlock = msg.PeerBlock,
		            DocumentId = docId,
		            MessageId = msg.MessageId,
		            RequestId = msg.RequestId,
		            Process = msg.Process,
		            Type = 315, // TODO заменить на реальный тип
		            From = msg.To,
		            To = msg.From,
                    Status = MessageStatus.Ok,
                    Errors = new List<Error>()
		        };

		        if (docBody.ToLower().Contains("edo_odkf401"))
		        {
		            var importHandler = new UKReportImportHandlerF401();

		            var import = importHandler.ParseDocument(docBody);
		            if (import != null)
		            {
		                Logs.MainLog.Info("Документ успешно распарсен");
		                Logs.MainLog.Info("Импортируем...");
		                var importResult = MQConnector.Instance.Service.ImportUKF401(import.F401402, import.F401402UK, import.edoLog, import.xmlBody);
		                if (importResult.IsSuccess)
		                {   
		                    Logs.MainLog.Info("Импорт успешно завершен");
		                    replyMessage.Status = MessageStatus.Ok;
		                }
		                else
		                {
		                    Logs.MainLog.Error($"Импорт завершен с ошибкой: {importResult.ErrorMessage}");
		                    replyMessage.Status = MessageStatus.Failed;
		                    replyMessage.Errors.Add(new Error
		                    {
		                        ErrorCode = 1,
		                        ErrorMessage = importResult.ErrorMessage
		                    });
		                }
		            }
		            else
		            {
		                Logs.MainLog.Error("Во время парсинга документа возникли ошибки. Детали смотрите выше.");
		                replyMessage.Status = MessageStatus.Failed;
		                replyMessage.Errors.Add(new Error
		                {
		                    ErrorCode = 1,
		                    ErrorMessage = "Во время парсинга документа возникли ошибки"
		                });
		            }
		        }
		        else if (docBody.ToLower().Contains("edo_odkf402"))
		        {
		            var importHandler = new UKReportImportHandlerF402();

		            var import = importHandler.ParseDocument(docBody);
		            if (import != null)
		            {
		                Logs.MainLog.Info("Документ успешно распарсен");
		                Logs.MainLog.Info("Импортируем...");
		                var importResult = MQConnector.Instance.Service.ImportUKF402(import.F401402UK, import.edoLog, import.xmlBody);
		                if (importResult.IsSuccess)
		                {
		                    Logs.MainLog.Info("Импорт успешно завершен");
		                    replyMessage.Status = MessageStatus.Ok;
		                }
		                else
		                {
		                    Logs.MainLog.Error($"Импорт завершен с ошибкой: {importResult.ErrorMessage}");
		                    replyMessage.Status = MessageStatus.Failed;
		                    replyMessage.Errors.Add(new Error
		                    {
		                        ErrorCode = 1,
		                        ErrorMessage = importResult.ErrorMessage
		                    });
		                }
		            }
		            else
		            {
		                Logs.MainLog.Error("Во время парсинга документа возникли ошибки. Детали смотрите выше.");
		                replyMessage.Status = MessageStatus.Failed;
		                replyMessage.Errors.Add(new Error
		                {
		                    ErrorCode = 1,
		                    ErrorMessage = "Во время парсинга документа возникли ошибки"
		                });
		            }
		        }

                // Отправляем в ВИО квитанцию типа Reply
		        var jsonReplyMessage = JsonConvert.SerializeObject(replyMessage);
		        if (Settings.MQConnection.IsDebugEnabled)
		        {
                    // Сохраняем json в папку DebugData
                    File.WriteAllText(
                        Path.Combine(
                            Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ServerEnvironment.VIO_DEBUG_FOLDER), 
                            $"{Guid.NewGuid()}.jsonreply"), jsonReplyMessage, Encoding.UTF8);
		        }
		        else
		        {
                    MQConnector.Instance.CoreConnector.QueueMessage(jsonReplyMessage, Settings.MQConnection.MQQueueName);
                }
		    }
		}

	    public static List<string> PushDebugData(VIOSettings cfg)
	    {
	        try
	        {
                return ServerEnvironment.LoadDebugFilesVIO();
	        }
	        catch (Exception ex)
	        {
                Logs.MainLog.Error("Ошибка загрузки отладочной информации VIO", ex);
                return new List<string>();
	        }
	    }
	}
}
