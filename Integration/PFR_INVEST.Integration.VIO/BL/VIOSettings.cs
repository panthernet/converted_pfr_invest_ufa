﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace PFR_INVEST.Integration.VIO.BL
{
	[XmlRoot("VIOSettings")]
	public class VIOSettings
	{
		public class Connection
		{
			public class ConSetting
			{
				[XmlAttribute("Key")]
				public string Key { get; set; }
				[XmlAttribute("Value")]
				public string Value { get; set; }
			}

			public string MQConnectionString { get; set; }

            [XmlElement("IsVerbose")]
            public bool IsVerbose { get; set; }

            [XmlElement("IsQueueMode")]
            public bool IsQueueMode { get; set; }

            [XmlElement("MQManagerName")]
			public string MQManagerName { get; set; }

            [XmlElement("IsDebugEnabled")]
            public bool IsDebugEnabled { get; set; }
            
			public string SubscriptionName { get; set; }

            [XmlElement("MQQueueName")]
            public string MQQueueName { get; set; }

            [XmlElement("NotifyTopicName")]
			public string NotifyTopicName { get; set; }

            [XmlElement("NotifyTopicPrefix")]
            public string NotifyTopicPrefix { get; set; }

            [XmlElement("NotifyTopicInterval")]
			public int NotifyTopicInterval { get; set; }

            [XmlElement("EHDHost")]
            public string EHDHost { get; set; }

            [XmlElement("EHDPort")]
            public int EHDPort { get; set; }

            [XmlElement("EHDBaseUri")]
            public string EHDBaseUri { get; set; }
            

            [XmlArray("ConnectionSettings")]
			[XmlArrayItem("Setting")]
			public List<ConSetting> ConnectionSettings { get; set; }


			public Hashtable GetConnectionSettingsAsHashTable()
			{
				var ht = new Hashtable();
			    ConnectionSettings?.ForEach(s => ht.Add(s.Key, s.Value));
			    return ht;
			}
		}
        
		[XmlElement("MQConnection")]
		public Connection MQConnection { get; set; }
        public static VIOSettings Load(string fileName)
		{
			var serializer = new XmlSerializer(typeof(VIOSettings));
			var settings = new XmlReaderSettings();
			//settings.ValidationType = ValidationType.Schema;
			//settings.Schemas.Add(string.Empty, KIP_Import.GetShema("I_TRANS_MSK_1.0.1.xsd"));
			using (var reader = XmlReader.Create(fileName, settings))
			{
				var res = (VIOSettings)serializer.Deserialize(reader);
				return res;
			}
		}

	}
}
