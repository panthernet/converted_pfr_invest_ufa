﻿using log4net;

namespace PFR_INVEST.Integration.VIO.BL
{
	public static class Logs
	{
		internal static readonly ILog MainLog = LogManager.GetLogger("Integration.VIO.MainLog");
		internal static readonly ILog Messages = LogManager.GetLogger("Integration.VIO.Messages");
 
	}
}
