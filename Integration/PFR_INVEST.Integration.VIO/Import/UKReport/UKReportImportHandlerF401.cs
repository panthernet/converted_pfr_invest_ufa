﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Integration.VIO.BL;
using PFR_INVEST.Integration.VIO.MQ;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.Integration.VIO.Import.UKReport
{
    /// <summary>
    /// ВИО
    /// Импорт документа из УК F401
    /// </summary>
    public class UKReportImportHandlerF401: DocumentParseHandlerBase<ImportEdoOdkF401402>
    {   
        public override ImportEdoOdkF401402 ParseDocument(string documentBody)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(EDO_ODKF401));
            
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            CultureInfo ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas.Add(DocumentBase.GetShemaWithIncludes("F401.xsd"));

            try
            {
                using (var ms = new MemoryStream())
                {
                    var contentBytes = Encoding.GetEncoding(1251).GetBytes(documentBody);

                    ms.Write(contentBytes, 0, contentBytes.Length);
                    ms.Position = 0;
                    ms.Seek(0, SeekOrigin.Begin);

                    using (XmlReader reader = XmlReader.Create(ms, settings))
                    {
                        var doc = (EDO_ODKF401) serializer.Deserialize(reader);

                        doc.Details = doc.Details.Where(d => !string.IsNullOrEmpty(d.DogPFRNum)).ToList();

                        if (doc.Details.Count == 0)
                        {
                            throw new ReportImportException(
                                $"Документ с регистрационным номером {doc.RegNumberOut}\nне содержит информации к требованию на оплату.");
                        }

                        //if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                        //{
                        //    return ActionResult.Error(
                        //        $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                        //}


                        //if (Items.Any(i => string.Equals((i as ImportEdoOdkF401402).edoLog.DocRegNumberOut, doc.RegNumberOut, StringComparison.OrdinalIgnoreCase)))
                        //{
                        //    return ActionResult.Error(
                        //        $"Документ с регистрационным номером {doc.RegNumberOut} уже загружен для импорта");
                        //}

                        //if (
                        //    Items.Any(
                        //        item =>
                        //        (item as ImportEdoOdkF401402).F401402.FromDate == DateUtil.ParseImportDate(doc.FromDate) &&
                        //        (item as ImportEdoOdkF401402).F401402.ToDate == DateUtil.ParseImportDate(doc.ToDate) &&
                        //        (item as ImportEdoOdkF401402).F401402UK.Any(uk => doc.Details.Select(d => d.DogPFRNum).Contains(uk.ContractNumber)))
                        //    )
                        //{
                        //    return ActionResult.Error(
                        //        $"Данные из файла {fName}\nдля одного или некольких договоров\nс периодом оплаты с {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\n уже загружены для импорта");
                        //}

                        using (var session = DataAccessSystem.OpenSession())
                        {
                            var requirements = session.CreateCriteria(typeof (EdoOdkF401402)).List<EdoOdkF401402>();

                            if (
                                requirements.Any(
                                    r =>
                                        string.Equals(r.RegNumberOut, doc.RegNumberOut,
                                            StringComparison.OrdinalIgnoreCase)))
                            {
                                throw new ReportImportException(
                                    $"Документ с регистрационным номером {doc.RegNumberOut} уже был импортирован");
                            }

                            requirements =
                                requirements.Where(
                                    r =>
                                        DateUtil.IsDateEquals(r.FromDate, DateUtil.ParseImportDate(doc.FromDate)) &&
                                        DateUtil.IsDateEquals(r.ToDate, DateUtil.ParseImportDate(doc.ToDate))).ToList();


                            XDocument xdoc = XDocument.Parse(documentBody);
                            var fName = "Получено из ВИО";
                            xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                            //                    long? IdDelete = this.GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                            ImportEdoOdkF401402 importEdoOdkF401402 = new ImportEdoOdkF401402();

                            importEdoOdkF401402.xmlBody = xdoc.ToString();
                            importEdoOdkF401402.F401402 = new EdoOdkF401402
                            {
                                ReportDate = DateUtil.ParseImportDate(doc.ReportDate),
                                RegDate = DateTime.Now.Date,
                                AuthorizedPerson = doc.AuthorizedPerson,
                                Executor = doc.Executor,
                                FromDate = DateUtil.ParseImportDate(doc.FromDate),
                                ToDate = DateUtil.ParseImportDate(doc.ToDate),
                                RegNumberOut = doc.RegNumberOut
                            };


                            foreach (var info in doc.Details)
                            {
                                Contract contract = MQConnector.Instance.Service.GetContractByDogovorNum(info.DogPFRNum);

                                if (contract == null)
                                {
                                    throw new ReportImportException(
                                        $"Договор с регистрационным номером {info.DogPFRNum} не найден");
                                }

                                if (requirements.Count > 0)
                                {
                                    var lists = requirements.Select(r => r.GetDetailList(session));

                                    if (lists.Any(item => item.Any(i => i.ContractId == contract.ID)))
                                    {
                                        throw new ReportImportException(
                                            $"Для договора с регистрационным номером {info.DogPFRNum}\nтребование на оплату с {DateUtil.ParseImportDate(doc.FromDate).Value.ToString("dd.MM.yyyy")} по {DateUtil.ParseImportDate(doc.ToDate).Value.ToString("dd.MM.yyyy")}\nуже существует");
                                    }
                                }

                                importEdoOdkF401402.F401402UK.Add(new F401402UK
                                {
                                    ContractId = contract.ID,
                                    StatusID = 1,
                                    DogSDNum = info.DogSDNum,
                                    DogSDDate = DateUtil.ParseImportDate(info.DogSDDate),
                                    AverageScha = info.AverageSCHA,
                                    NewTransh = info.NewTransh,
                                    PayFromAverage = info.PayFromAverage,
                                    PayFromTransh = info.PayFromTransh,
                                    Itogo = info.Itogo,
                                    OutgoingDocDate = DateUtil.ParseImportDate(doc.ReportDate),
                                    OutgoingDocNumber = doc.RegNumberOut,
                                    ContractNumber = info.DogPFRNum
                                });
                            }

                            importEdoOdkF401402.edoLog = new EDOLog
                            {
                                DocTable = xdoc.Root.Name.ToString(),
                                DocForm = xdoc.Root.Name.ToString(),
                                Filename = fName,
                                DocRegNumberOut = doc.RegNumberOut,
                                RegDate = DateTime.Now.Date
                            };

                            importEdoOdkF401402.FileName = fName; //  Path.GetFileName(fName);
                            importEdoOdkF401402.PathName = fName; // Path.GetDirectoryName(fileName);
                            importEdoOdkF401402.ReportOnDate =
                                importEdoOdkF401402.F401402.ReportDate?.ToString("dd.MM.yyyy") ?? string.Empty;
                            importEdoOdkF401402.ReportOnDateNative = importEdoOdkF401402.F401402.ReportDate;

                            return importEdoOdkF401402;
                        }
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        throw new ReportImportException(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        throw new ReportImportException(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                }
                else
                {
                    Logs.MainLog.Error("Неверный формат документа", ex);
                }
            }
            catch (ReportImportException ex)
            {
                Logs.MainLog.Error("Общая ошибка импорта отчета", ex);
            }
            catch (Exception ex)
            {
                Logs.MainLog.Error("Ошибка открытия документа", ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return null;
        }
    }
}
