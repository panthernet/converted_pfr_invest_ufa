﻿using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.Integration.VIO.Import.UKReport
{
    public static class F401402Extension
    {
        public static List<F401402UK> GetDetailList(this EdoOdkF401402 requirement, ISession session)
        {
            if (requirement != null)
            {
                var criteria = session.CreateCriteria(typeof (F401402UK));
                criteria.Add(Restrictions.Eq("EdoID", requirement.ID));

                return (List<F401402UK>) criteria.List<F401402UK>();
            }

            return new List<F401402UK>();
        }

        public static Contract GetContract(this F401402UK detail, ISession session)
        {
            if (detail != null)
            {
                return session.Get<Contract>(detail.ContractId.Value);
            }

            return null;
        }

        public static List<EDOLog> GetEdoLogsByRegNumberOut(this EDO_ODKF402 doc, ISession session)
        {
            var criteria = session.CreateCriteria(typeof(EDOLog));
            criteria.Add(Restrictions.Eq("DocRegNumberOut", doc.RegNumberOut));

            return (List<EDOLog>)criteria.List<EDOLog>();
        }
    }
}
