﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Integration.VIO.Import.UKReport
{
    /// <summary>
    /// , который должен реализовывать каждый класс, импортирующий документ из ВИО
    /// Пока что это формы F401, F402
    /// </summary>
    public abstract class DocumentParseHandlerBase<T> where T: new()
    {
        /// <summary>
        /// Парсит документ в объект
        /// </summary>
        /// <typeparam name="T">тип объекта</typeparam>
        /// <param name="documentBody">содержание документа в текстовом виде, например XML</param>
        /// <returns></returns>
        public abstract T ParseDocument(string documentBody);
    }
}
