﻿namespace PFR_INVEST.Integration.NSI.JSON
{
	public class Dictionary
	{
		/// <summary>
		/// Идентификатор справочника, согласно выделенному диапазону
		/// </summary>
		public long? id { get; set; }

		/// <summary>
		/// Название справочника
		/// </summary>
		public string name { get; set; }

		/// <summary>
		/// Ссылка на родительский справочник
		/// </summary>
		public long? parent { get; set; }

		/// <summary>
		/// Тип справочника. Cls 101
		/// </summary>
		public long? type { get; set; }

		/// <summary>
		/// Статус справочника. Cls 218
		/// </summary>
		public long? status { get; set; }

		/// <summary>
		/// Комментарий к справочнику
		/// </summary>
		public string dsc { get; set; }

        /// <summary>
        /// Дата начала действия справочника. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00
        /// </summary>
        public string sd { get; set; }

		/// <summary>
		/// Дата окончания действия справочника. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00
		/// </summary>
		public string ed { get; set; }


        //С ВЕРСИИ 1.32

        /// <summary>
        /// Контрольная сумма
        /// </summary>
        public long? crc { get; set; }


    }
}
