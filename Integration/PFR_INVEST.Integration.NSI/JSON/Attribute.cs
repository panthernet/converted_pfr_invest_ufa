﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Integration.NSI.JSON
{
	public class Attribute
	{
		/// <summary>
		/// Идентификатор справочника. То же значение, что и в dictionary.id
		/// </summary>
		public long? dictionaryId { get; set; }

		/// <summary>
		/// Системное название атрибута. Должно быть уникальным среди всех терминов справочника
		/// </summary>
		public string name { get; set; }

		/// <summary>
		/// Пользовательское название атрибута
		/// </summary>
		public string dscName { get; set; }

		/// <summary>
		/// Порядковый номер атрибута в пределах справочника
		/// </summary>
		public long? number { get; set; }

		/// <summary>
		/// Тип данных атрибута. Cls 259
		/// </summary>
		public long? type { get; set; }

		/// <summary>
		/// Признак обязательности заполнения значениями. 1 - Да, 0 - Нет
		/// </summary>
		public int? required { get; set; }

		/// <summary>
		/// Признак уникальности значения. 1 - Да, 0 - Нет
		/// </summary>
		public int? key { get; set; }

		/// <summary>
		/// Маска формата значения атрибута. В настоящее время может использоваться только для типа Date
		/// </summary>
		public string mask { get; set; }

		/// <summary>
		/// Комментарий к атрибуту
		/// </summary>
		public string dsc { get; set; }

		/// <summary>
		/// Дата начала действия описателя атрибута. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00"
		/// </summary>
		public string sd { get; set; }

		/// <summary>
		/// Дата окончания действия описателя атрибута. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00
		/// </summary>
		public string ed { get; set; }
	}
}
