﻿using System.Diagnostics;

namespace PFR_INVEST.Integration.NSI.JSON
{
	public class DictionaryRecord
	{
		[DebuggerDisplay("{termName} - '{term}'")]
		public class Term 
		{
			/// <summary>
			/// Номер атрибута
			/// </summary>
			public long? termNumber { get; set; }

			/// <summary>
			/// Системное название атрибута. Должно быть уникальным среди всех терминов справочника
			/// </summary>
			public string termType { get; set; }

			/// <summary>
			/// Пользовательское название атрибута
			/// </summary>
			public string termName { get; set; }

			/// <summary>
			/// Значение атрибута
			/// </summary>
			public string term { get; set; }
		}



		/// <summary>
		/// Идентификатор справочника. То же значение, что и в dictionary.id
		/// </summary>
		public long? dictionaryId { get; set; }

		/// <summary>
		/// Номер термина в справочнике
		/// </summary>
		public long? number { get; set; }

		/// <summary>
		/// Ссылка на number родительского термина в пределах текущего справочника
		/// </summary>
		public long? parentRecordNumber { get; set; }

		/// <summary>
		/// Ссылка на number родительского термина справочника с id = dictionary.parent
		/// </summary>
		public long? parentDictionaryRecordNumber { get; set; }

		/// <summary>
		/// Статус термина. Cls 218
		/// </summary>
		public long? status { get; set; }

		/// <summary>
		/// Признак системного термина. 1 - Да, 0 - Нет
		/// </summary>
		public int? system { get; set; }

		/// <summary>
		/// Комментарий к термину
		/// </summary>
		public string dsc { get; set; }

		/// <summary>
		/// Дата начала действия термина. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00
		/// </summary>
		public string sd { get; set; }

		/// <summary>
		/// Дата окончания действия термина. Формат "YYYY-MM-DDTHH:MI:SS.000+TZ:00
		/// </summary>
		public string ed { get; set; }

		/// <summary>
		/// Блоки значений атрибутов термина
		/// </summary>
		public Term[] terms { get; set; }


        //С ВЕРСИИ 1.32

        /// <summary>
        /// Контрольная сумма
        /// </summary>
        public long? crc { get; set; }
    }
}
