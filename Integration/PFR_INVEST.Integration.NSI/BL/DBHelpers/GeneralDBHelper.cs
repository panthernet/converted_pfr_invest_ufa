﻿using System;
using System.Collections;
using System.Linq;
using NHibernate;
using PFR_INVEST.DataAccess.Server;

namespace PFR_INVEST.Integration.NSI.BL.DBHelpers
{
    /// <summary>
    /// Используется для обработки словарей без специальной логики при создании/удалении
    /// не поддерживает общее изменение статуса (ввести интерфейс для всех со statusId?)
    /// </summary>
	internal class GeneralDBHelper : IDBHelper
	{
		public string Dictionary { get; }
        public Type Type { get; }
        public string DBTable { get; }

        private readonly string _dbFieldStatus;
        public bool HasStatus => !string.IsNullOrEmpty(_dbFieldStatus);
        public string DbFieldStatusType { get; set; }
        public object DbFieldStatusEnable { get; set; }
        public object DbFieldStatusDisable { get; set; }
        public bool DbCheckForDupes { get; set; }

        public GeneralDBHelper(Type type, string dbTable, string dbFieldStatus, string dicName)
        {
	        Type = type;
            DBTable = dbTable;
            _dbFieldStatus = dbFieldStatus;
            Dictionary = dicName;
        }
	
		public long Create(NHibernate.ISession session, JSON.DictionaryRecord record, DBUpdater updater)
		{
		    var obj = Activator.CreateInstance(Type);
			
			if (obj != null)
			{
			    InjectStatusValue(updater, obj);
			    updater.UpdateFieldsGeneral(session, obj, Type, record, OpType.Create);
			   // updater.UpdateSpecialFields(session, obj, record, OpType.Create);
				session.SaveOrUpdate(obj);
			    var id = (long)session.GetIdentifier(obj);
				session.Flush();
                return id;
			}
		    return -1;
		}

        private void InjectStatusValue(DBUpdater updater, object obj)
        {
            if (string.IsNullOrEmpty(_dbFieldStatus)) return;
            var field = updater.GetMetadata(Type).FirstOrDefault(a => a.PropertyInfo.Name == _dbFieldStatus);
            if (field != null)
                updater.SetReflectionValue(field, (string)DbFieldStatusEnable ?? "1", obj);
        }

        public void Update(NHibernate.ISession session, long id, JSON.DictionaryRecord record, DBUpdater updater)
        {
            object obj;
            //используем отдельную сессию, т.к. хибер запоминает объект выборки и не даст сохранить другую сущность с таким же ID
            using(var s = DataAccessSystem.OpenSession())
                obj = s.Get(Type, id);
            //Обход бага хибера: конвертируем хиб сущность в базовый класс, иначе получаем ошибки индекса ODBC соединения
            obj = HibObjectsToDtoConverter.ConvertFromHibType(obj) ?? obj;

            if (obj == null) return;
		    updater.UpdateFieldsGeneral(session, obj, Type, record, OpType.Update);
			//updater.UpdateSpecialFields(session, obj, record, OpType.Create);
            session.SaveOrUpdate(obj);
		    session.Flush();
		}

		public bool Enable(NHibernate.ISession session, long id)
		{
		    if (!HasStatus) return false;
		    UpdateStatusField(session, id, DbFieldStatusEnable ?? 1);
		    return true;
		}

		public bool Disable(NHibernate.ISession session, long id)
		{
		    if (!HasStatus) return false;
		    UpdateStatusField(session, id, DbFieldStatusDisable ?? - 1);
		    return true;
		}


        private void UpdateStatusField(NHibernate.ISession session, long id, object iValue)
        {
            session.CreateSQLQuery($@"update PFR_BASIC.{DBTable} set {_dbFieldStatus}=:value where id=:id")
                .SetParameter("value", ConvertStatusValue(iValue))
                .SetParameter("id", id)
                .ExecuteUpdate();
            session.Flush();
        }

        private object ConvertStatusValue(object iValue)
        {
            var iStatusType = string.IsNullOrEmpty(DbFieldStatusType) ? "Int64" : DbFieldStatusType;
            switch (iStatusType)
            {
                case "Int64":
                    return Convert.ToInt64(iValue);
                case "Int32":
                    return Convert.ToInt32(iValue);
                case "Int16":
                    return Convert.ToInt16(iValue);
                default:
                    throw new Exception($"Ошибка файла конфигурации, указан неизвестный тип знaчения статуса: {iStatusType}");
            }
        }
	}
}
