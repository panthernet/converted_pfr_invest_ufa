﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DevExpress.Web.Mvc;
using Microsoft.ApplicationInsights.Extensibility;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FormDataHelper.Initialize();

            AreaRegistration.RegisterAllAreas();

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new CustomViewEngine());

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.Filters.Add(new LogExceptionFilterAttribute());

            ModelBinders.Binders.Add(typeof(DevExpressEditorsBinder), new DevExpressEditorsBinder());
            ModelBinders.Binders.Add(typeof(CustomBinder), new CustomBinder());

            ViewModelBase.Logger = Logger.GetSystemLoggerInstance();
            AcquireRequestState += Application_AcquireRequestState;

            TelemetryConfiguration.Active.DisableTelemetry = true;
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            var ci = new CultureInfo("ru-RU");
            Thread.CurrentThread.CurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = ci;
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            DevExpressHelper.Theme = "Office2010Blue";
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            
            Logger.LogEx(exception);
            Server.ClearError();
        }
    }

    public class LogExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            Logger.LogEx(context.Exception);
        }
    }

    public class CustomViewEngine : RazorViewEngine
    {
        public CustomViewEngine()
        {
            var viewLocations = new[] {
                "~/Views/{1}/{0}.cshtml",
                "~/Views/Shared/{0}.cshtml",
                "~/Views/Shared/Controls/{0}.cshtml",
                "~/Views/Shared/Ribbon/{0}.cshtml",
                "~/Views/Menu/{0}.cshtml",
                "~/Views/Menu/{1}/{0}.cshtml",

                "~/Views/Menu/Dictionary/NPF/{0}.cshtml",
                "~/Views/Menu/Dictionary/SI/{0}.cshtml",
                "~/Views/Menu/Dictionary/NPF/Dialogs/{0}.cshtml",
                "~/Views/Menu/NPF/{0}.cshtml",
                "~/Views/Menu/NPF/DeadZL/{0}.cshtml",
                "~/Views/Menu/NPF/RegZL/{0}.cshtml",
                "~/Views/Menu/NPF/SpnTransfer/{0}.cshtml",
                "~/Views/Menu/NPF/TempAllocation/{0}.cshtml",
                "~/Views/Menu/NPF/Correspondence/{0}.cshtml",
                "~/Views/Menu/NPF/RejectApplications/{0}.cshtml",
                
            };

            PartialViewLocationFormats = viewLocations;
            ViewLocationFormats = viewLocations;
        }
    }
}
