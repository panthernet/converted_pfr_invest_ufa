﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PFR_INVEST.Web2.BusinessLogic
{
	public static class StringExtension
	{
	    public static readonly string[] Executables =  { ".exe", ".com", ".cmd", ".bat" , ".ini" };
		/// <summary>
		/// Formats the string according to the specified mask
		/// </summary>
		/// <param name="input">The input string.</param>
		/// <param name="mask">The mask for formatting. Like "A##-##-T-###Z"</param>
		/// <returns>The formatted string</returns>
		public static string FormatWithMask(this string input, string mask)
		{
			if (String.IsNullOrEmpty(input))
				return input;
			var output = String.Empty;
			var index = 0;
			foreach (var m in mask)
			{
				if (m == '#')
				{
					if (index < input.Length)
					{
						output += input[index];
						index++;
					}
				}
				else
					output += m;
			}
			return output;
		}

	    public static void Add<T1, T2>(this ConcurrentDictionary<T1, T2> dic, T1 index, T2 value)
	    {
	        if (!dic.TryAdd(index, value))
	            dic[index] = value;
	    }

	    public static void Remove<T1, T2>(this ConcurrentDictionary<T1, T2> dic, T1 index)
	    {
	        T2 value;
	        dic.TryRemove(index, out value);
	    }


	    public static bool IsRequired<T>(this T item, string propname) where T : class
	    {
	        var type = item.GetType();
	        var property = type.GetProperties().FirstOrDefault(p => p.Name == propname && p.GetCustomAttributes(false)
	                                                                    .Any(a => a.GetType() == typeof(RequiredAttribute)));
	        return property != null;
	    }

	    public static int GetMaxLengthFromAttr<T>(this T item, string propname) where T : class
	    {
	        var type = item.GetType();
	        var property = type.GetProperties().FirstOrDefault(p => p.Name == propname && p.GetCustomAttributes(false)
	                                                                    .Any(a => a.GetType() == typeof(StringLengthAttribute)));
	        var x = (property?.GetCustomAttributes(false)
	            .FirstOrDefault(a => a.GetType() == typeof(StringLengthAttribute)) as StringLengthAttribute)?.MaximumLength ?? 0;

            return x;
	    }
    }
}