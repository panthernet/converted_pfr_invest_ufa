﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFR_INVEST.Web2
{
    public static class LinqExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if(source == null) return;
            foreach (var obj in source)
                action(obj);
        }

        public static void ForEach<T>(this IList<T> source, Action<T> action)
        {
            if(source == null) return;
            foreach (var obj in source)
                action(obj);
        }

        public static bool Equals2(this string value, string target)
        {
            return value.Equals(target, StringComparison.OrdinalIgnoreCase);
        }

    }

}

namespace NHibernate.Linq
{
    public class Tuple<T1, T2>
    {
        public T1 First { get; set; }
        public T2 Second { get; set; }
    }
}