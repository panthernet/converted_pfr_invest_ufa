﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using db2connector.Contract;
using DevExpress.XtraPrinting.Native;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Web2.Models;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public class LightService: IDisposable
    {
        private static readonly ConcurrentDictionary<string, WCFClient> Clients = new ConcurrentDictionary<string, WCFClient>();
        private const string DEFAULT_USERNAME = "SPY10001HEX";

        public static WCFClient Get(string username = null)
        {
            var name = string.IsNullOrEmpty(username) ? DEFAULT_USERNAME : username;
            if (!Clients.ContainsKey(name) || Clients[name].IsFaulted())
            {
                Clients.Add(name, WCFClient.CreateLightClient(name));
            }
            return Clients[name];
        }

        public void Dispose()
        {
            Clients.ForEach(a=> a.Value?.Dispose());
            Clients.Clear();
        }

        public static ApplicationUser GetUser(string userName, string password = null)
        {
            ApplicationUser user = null;
            using (var cli = WCFClient.CreateLightClient(userName, password))
            {
                var token = cli.Client.GetToken().ToString();
                var obj = cli.Client.GetUserInfo(new AuthRequest { Value1 = userName, AccessToken = token });
                if (obj == null) return null;
                var u = obj.Result as User;
                if (u != null)
                {
                    user = new ApplicationUser { UserName = userName, DisplayName = u.Name };
                    var roles = cli.Client.GetRoles(new AuthRequest { Value1 = userName, AccessToken = token }).Result as List<Role>;
                    roles?.ForEach(a => user.DokipRoles.Add(a.Name));
                }
            }
            return user;

        }

        public static Task<bool> ValidateUser(string username, string password)
        {
            try
            {
                var user = GetUser(username, password);
                var userRole = UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.User);
                return Task.FromResult(user.DokipRoles.Any(a => a == userRole));
            }
            catch (InvalidLoginOrPasswordException)
            {
                return Task.FromResult(false);
            }
            catch (NotUserException)
            {
                return Task.FromResult(false);
            }
            catch (EndpointNotFoundException e)
            {
                return Task.FromResult(false);
            }
            catch (SecurityNegotiationException e)
            {
                return Task.FromResult(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Task.FromResult(false);
            }
        }
    }
}