﻿using System.Web;
using System.Web.Mvc;
using PFR_INVEST.Web2.Controllers;

namespace PFR_INVEST.Web2.BusinessLogic
{
    /// <summary>
    /// Возвращает юзера к окну логина, если сессия истекла
    /// </summary>
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = filterContext.HttpContext.Session;
            string cookieHeader = filterContext.HttpContext.Request.Headers["Cookie"];
            var sid = cookieHeader.IndexOf("ASP.NET_SessionId");
            bool isOk;
            if (sid > 0)
            {
                sid += 18;
                var eIndex = cookieHeader.IndexOf(";", sid);
                if (eIndex < 0) eIndex = cookieHeader.Length - sid;
                else eIndex = eIndex - sid;
                isOk = session.SessionID != cookieHeader.Substring(sid, eIndex);
            }
            else isOk = true;

            if (session.IsNewSession && cookieHeader != null && sid >= 0 && isOk)
            {
                var controller = filterContext.Controller as IPublicController;
                if(controller != null)
                    filterContext.Result = controller.ViewPublic("Index", "Dictionary");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}