﻿using System;
using db2connector;

namespace PFR_INVEST.Web2.BusinessLogic.Database
{
    public class PlainDataServiceClient: IDisposable
    {
        private readonly IISService _db = new IISService();

        public IDB2Connector GetChannel()
        {
            return _db;
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
    }

}