﻿using PFR_INVEST.Web2.BusinessLogic.Models;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;
using PureWebFormViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebFormViewModel;
using PureWebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebViewModel;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public static class WebModelHelperEx
    {
        public const string DEL_ENTRY_TEXT = "Удалить запись?";

        public static void InitializeDefault(WebViewModel model, long id)
        {
            var data = FormDataHelper.GetDataByModel(model.GetType().Name);
            model.WebTitle = id != 0 ? data.DisplayName : data.DisplayCreateName;
            model.FormId = model.AjaxAction = data.Id;
        }

        public static void InitializeDefault(WebViewModel model)
        {
            var data = FormDataHelper.GetDataByModel(model.GetType().Name);
            model.WebTitle = data.DisplayName;
            model.FormId = model.AjaxAction = data.Id;
            switch (data.DialogMode)
            {
                case "DialogNoValidation":
                case "Dialog":
                    model.AjaxMain = "_SharedAjaxDialogPartialView";
                    break;
            }
        }

        public static void InitializeDefault(WebViewModel model, string controller, string partial)
        {
            InitializeDefault(model);
            model.AjaxController = controller;
            model.AjaxPartial = partial;
        }

        /// <summary>
        /// Получает состояние модели по состоянию идентификатора связанной с ней записи
        /// </summary>
        /// <param name="id">Идентификатор</param>
        internal static ViewModelState GetModelState(long id = 0)
        {
            return id > 0 ? ViewModelState.Edit : ViewModelState.Create;
        }

    /*    /// <summary>
        /// Обновляет состояние модели с учетом проверки на права доступа
        /// </summary>
        internal static void UpdateModelState(ViewModelBase model)
        {
            model.State = model.EditAccessDenied ? ViewModelState.Read : GetModelState(model.ID);
        }*/

        internal static void UpdateModelState(PureWebViewModel model)
        {
            model.State = model.EditAccessDenied ? ViewModelState.Read : (model is PureWebDialogViewModel ? ViewModelState.Edit : (model is PureWebFormViewModel ? GetModelState(((PureWebFormViewModel)model).ID) : ViewModelState.Read ));
        }
    }
}