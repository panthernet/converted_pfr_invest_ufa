﻿using System;
using System.Configuration;
using System.Data;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Timers;
using System.Web;
using db2connector;
using Microsoft.AspNet.Identity;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.Web2.BusinessLogic.Database;
using PFR_INVEST.Web2.BusinessLogic.From;
using PFR_INVEST.Web2.Database.EF;

namespace PFR_INVEST.Web2.BusinessLogic
{
    public class WCFClient: IDisposable
    {
        private WebDataServiceClient _wcfClient; //ServiceSystem.CreateNewDataClient();
        private PlainDataServiceClient _dbClient;
        public bool UseAutoRepair;
        private readonly Timer _pingTimer = new Timer();

        public ClientCredentials CurrentClientCredentials => _wcfClient?.ClientCredentials;
        public ClientCredentials ClientCredentials { get; set; }
        public static readonly ClientAuthType AuthType;

        public static bool IsDirectConnection = true;

        public WCFClient()
        {
            if (!IsDirectConnection)
            {
                _pingTimer.AutoReset = true;
                _pingTimer.Interval = 30000;
                _pingTimer.Elapsed += PingTimer_Elapsed;
            }
        }

        static WCFClient()
        {
            ServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
            AuthServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
            BLServiceSystem.GetDataServiceDelegate = GetDataServiceFunc;
            AP.GetUserAP = GetDataServiceAP;

            switch (ConfigurationManager.AppSettings["AuthType"])
            {
                case "ECASA":
                    AuthType = ClientAuthType.ECASA;
                    break;
                case "ECASAHTTPS":
                    AuthType = ClientAuthType.ECASAHTTPS;
                    break;
                default:
                    AuthType = ClientAuthType.None;
                    break;
            }

            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => true;

        }

        //public static ApplicationUser User => MyUserStore.Current.GetDefaultUser().Result;

        /*public static ApplicationUser GetCurrentUser()
        {
            var user = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(HttpContext.Current.User.Identity.GetUserId());
            return user;
        }

        //TODO надеемся что в хранилище первый юзер - то что надо!
        public static WCFClient Current => GetCurrentUser()?.Client;
        */
        internal static WCFClient CreateLightClient(string username, string password = null)
        {
            return new WCFClient().CreateLightClientInternal(username, password);
        }

        internal WCFClient CreateLightClientInternal(string username, string password = null)
        {
            if (!IsDirectConnection)
            {
                _wcfClient = WebDataServiceClient.CreateNew(AuthType);
                _wcfClient.ClientCredentials.Windows.ClientCredential.UserName = username;
                var behavior = _wcfClient.Endpoint.Behaviors.Find<IDB2Connector>();
                if (behavior == null)
                    _wcfClient.Endpoint.Behaviors.Add(new MessageCompressionAttribute(Compress.Reply | Compress.Request));
                _wcfClient.ClientCredentials.SupportInteractive = false;
                _wcfClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                    X509CertificateValidationMode.None;
                _wcfClient.Open();
                _wcfClient.GetChannel().InitService(new object[]
                {
                    username,
                    password,
                    null
                });
            }
            else
            {
                _dbClient = new PlainDataServiceClient();
                _dbClient.GetChannel().InitService(new object[]
                {
                    username,
                    password,
                    null
                });
            }

            return this;
        }

        private void PingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                _wcfClient?.GetChannel().GetServiceVersion();
            }
            catch (Exception ex)
            {
                //App.log.WriteException(ex, "При обращении к WCF сервису возникла ошибка.");
            }
        }

        public bool IsFaulted()
        {
            return !IsDirectConnection && (_wcfClient == null ||  _wcfClient.State == CommunicationState.Faulted || _wcfClient.State == CommunicationState.Closed || !TestPing());
        }

        private bool TestPing()
        {
            try
            {
                //Simple call to check connection state
                _wcfClient.GetChannel().GetServiceVersion();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private async void RepairConnection()
        {
            //пропускаем восстановление связи если приложение уже закрывается, больше обращений к сервису не требуется
            //if (App.IsClosing) return false;
            bool bError = false;
            try
            {
                if (_wcfClient.State == CommunicationState.Faulted)
                    bError = true;
                else
                {
                    //Simple call to check connection state
                    _wcfClient.GetChannel().GetServiceVersion();
                }

            }
            catch (Exception ex)
            {
                bError = true;
                //App.log.WriteException(ex, "Возникла ошибка при восстановлении соединения к WCF сервису.");
            }

            if (_wcfClient.State != CommunicationState.Opened || bError)
            {
               // if (ClientCredentials != null)
               //     CreateClientByCredentials();
            }
        }

        /// <summary>
        /// Текущий канал общения с БД
        /// </summary>
        public IDB2Connector Client
        {
            get
            {
                if (_wcfClient == null)
                {
                    return _dbClient?.GetChannel();
                }
                if (!UseAutoRepair) return _wcfClient?.GetChannel();
                if (ClientCredentials == null) return _wcfClient.GetChannel();
                UseAutoRepair = false;
                try
                {
                    RepairConnection();
                }
                finally
                {
                    UseAutoRepair = true;
                }
                return _wcfClient.GetChannel();
            }
        }

        private AppDatabase _efClient;
        private AuthProvider _authProvider;

        public AuthProvider AuthProvider => _authProvider ?? (_authProvider = new WebAuthProvider());

        //похоже EF закрывает и открывает соедиение самостоятельно, просто нужен инстанс
        public AppDatabase EFClient => _efClient ?? (_efClient = new AppDatabase());


        /// <summary>
        /// Метод-привязка для получения канала общения с БД для каждого отдельного юзера
        /// </summary>
        public static IDB2Connector GetDataServiceFunc()
        {
            return LightService.Get(HttpContext.Current == null ? null : HttpContext.Current.User.Identity.GetUserName()).Client;
        }

        public static AuthProvider GetDataServiceAP()
        {
            return LightService.Get(HttpContext.Current == null ? null : HttpContext.Current.User.Identity.GetUserName()).AuthProvider;
        }

        public static AppDatabase EF => GetEFServiceFunc();


        public static AppDatabase GetEFServiceFunc()
        {
            return LightService.Get(HttpContext.Current == null ? null : HttpContext.Current.User.Identity.GetUserName()).EFClient;
        }

        public static IDB2Connector LocalInstance => GetDataServiceFunc();

        public void Dispose()
        {
            ((IDisposable) _wcfClient)?.Dispose();
            _efClient?.Dispose();
            _dbClient?.Dispose();
            _pingTimer?.Dispose();
        }
    }
}