﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace PFR_INVEST.Web2.BusinessLogic.Models
{
    /// <summary>
    /// Единая базовая модель для всех веб-моделей данных
    /// </summary>
    public abstract class WebViewModel : IWebViewModel, ISavable
    {
        [JsonIgnore]
        public IList<NameEntity> FormData { get; } = new List<NameEntity>();
        [JsonIgnore]
        public string AjaxController { get; set; }
        [JsonIgnore]
        public string AjaxAction { get; set; }
        [JsonIgnore]
        public string AjaxPartial { get;  set; }
        [JsonIgnore]
        public string AjaxMain { get; set; } = "_SharedAjaxFormPartialView";
        [JsonIgnore]
        public string FormId { get; set; }
        [JsonIgnore]
        public int FormWidth { get; protected set; }
        [JsonIgnore]
        public int FormHeight { get; protected set; }
        [JsonIgnore]
        public bool IsModalEx { get; set; }

        [JsonIgnore]
        public string CustomDeleteMessage { get; set; } = WebModelHelperEx.DEL_ENTRY_TEXT;

        [JsonIgnore]
        public bool IsDeleteButtonVisible { get; set; } = true;
        /// <summary>
        /// Загрузить скрипты для грида по умолчанию на форме
        /// </summary>
        [JsonIgnore]
        public bool AttachDefaultGridScripts { get; protected set; }

        /// <summary>
        /// Доп. параметр для AJAX запроса перезагрузки формы по запросу
        /// Нужен для передачи данных, например при обновлении формы при выборе элемента комбо-бокса
        /// </summary>        
        public int AjaxRequestParamValue { get; set; }

        public virtual bool WebExecuteDelete(long id)
        {
            return false;
        }

        protected virtual bool WebCanExecuteDelete()
        {
            return true;
        }

        protected virtual bool WebBeforeExecuteDeleteCheck()
        {
            return true;
        }

        public void WebDelete(long delID)
        {
            try
            {
                if(DeleteAccessDenied || !WebCanExecuteDelete() || !WebBeforeExecuteDeleteCheck())
                    return;
                if(!WebExecuteDelete(delID))
                    return;
                IsDeleteCompleted = true;
                WCFClient.GetDataServiceFunc().SaveDeleteEntryLog(GetType().Name, "", delID, 1);
            }
            catch
            {
                IsDeleteCompleted = false;
                throw;
            }
        }

        [JsonIgnore]
        public string WebTitle { get; set; }


        [JsonIgnore]
        public virtual bool EditAccessDenied => (this as PureWebViewModel)?.IsEditAccessDenied() ?? false;
        [JsonIgnore]
        public virtual bool DeleteAccessDenied => (this as PureWebViewModel)?.IsDeleteAccessDenied() ?? false;
        /// <summary>
        /// Указывает были ли изменены данные в модели
        /// </summary>
        [JsonIgnore]
        public virtual bool IsDataChanged { get; set; }

        /// <summary>
        /// Возвращает 
        /// </summary>
        /// <returns></returns>
        public abstract long WebGetEntryId();

        /// <summary>
        /// Указывает успешно ли прошло сохранение
        /// </summary>
        [JsonIgnore]
        public virtual bool IsSaveCompleted { get; protected set; }

        [JsonIgnore]
        public bool IsDeleteCompleted { get; protected set; }

        /// <summary>
        /// Выполняется после создания шаблона формы
        /// </summary>
        public virtual void WebPostModelCreateAction()
        {
        }

        /// <summary>
        /// Выполняется после обновления модели данными со страницы (POST)
        /// </summary>
        public virtual void WebPostModelUpdateAction()
        {
        }

        public virtual bool WebExecuteSave()
        {
            return true;
        }

        public virtual bool WebBeforeExecuteSaveCheck()
        {
            return true;
        }


        protected virtual string WebValidate()
        {
            var model = this as IValidatableViewModel;
            return model?.Validate();
        }

        [JsonIgnore]
        public string WebLastModelError { get; set; }

        public virtual bool WebExecuteSaveWithChecks()
        {
            if (!WebBeforeExecuteSaveCheck()) return false;

            bool result;
            try
            {
                result = WebExecuteSave();
            }
            catch (Exception ex)
            {
                result = false;
                Common.Logger.Logger.Instance.Error($"WebExecuteSave {GetType().Name}", ex);
            }

            if (!result)
            {
                if (string.IsNullOrEmpty(WebLastModelError))
                    WebLastModelError = "Ошибка при сохранении объекта!";
                return false;
            }
            return true;
        }

        public string WebSave()
        {
            try
            {
                if (EditAccessDenied)
                    return "Недостаточно прав на редактирование!";
                WebLastModelError = null;
                var result = WebValidate();
                IsSaveCompleted = string.IsNullOrEmpty(result) && IsDataChanged;
                if (!string.IsNullOrEmpty(result))
                {
                    WebLastModelError = result;
                    return result;
                }
                //TODO
                IsDataChanged = true;

                if (!IsDataChanged)
                {
                    return (WebLastModelError = "Нет новой информации для сохранения!");
                }
                IsSaveCompleted = WebExecuteSaveWithChecks();
                if (!IsSaveCompleted)
                    result = WebLastModelError;
                return result;
            }
            catch (Exception ex)
            {
                IsSaveCompleted = false;
                WebLastModelError = "Критическая ошибка!";
                Common.Logger.Logger.Instance.Error($"WebSave {GetType().Name}", ex);
                return WebLastModelError;
            }
        }
    }

    public interface ISavable
    {
        bool WebExecuteSave();
        bool WebBeforeExecuteSaveCheck();
    }
}