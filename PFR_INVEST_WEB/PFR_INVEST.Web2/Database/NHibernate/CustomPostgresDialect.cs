﻿using NHibernate.Dialect;
using NHibernate.SqlCommand;

namespace PFR_INVEST.Web2.Database.NHibernate
{
    public class CustomPostgresDialect: PostgreSQL82Dialect
    {
        public CustomPostgresDialect()
        {
            RegisterKeyword("varchar");
            RegisterKeyword("interval");
        }
        //наш чудный хибер 3 не знает, что нужно использовать RETURNING при работе с PostgreSQL
        public override string IdentitySelectString => " returning id; ";

        //Из-за особенности RETURNING нужно переопределить этот метод нa новый лад
        public override SqlString AppendIdentitySelectToInsert(SqlString insertSql)
        {
            if (!this.SupportsInsertSelectIdentity) return insertSql;          
            return insertSql.Append(IdentitySelectString);
        }
    }
}
