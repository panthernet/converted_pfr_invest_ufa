﻿using System;
using System.Data;
using System.Data.Common;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace PFR_INVEST.Web2.Database.NHibernate
{

	public class BoolToIntHibType : IUserType
	{
		object IUserType.Assemble(object cached, object owner)
		{
			return cached;
		}

	    public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
	    {
	        if (value == null)
	        {
	            ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
	        }
	        else
	        {
	            ((IDataParameter)cmd.Parameters[index]).Value = (bool)value == false ? 0 : 1;
	        }
	    }

	    object IUserType.DeepCopy(object value)
		{
			return value;
		}

		object IUserType.Disassemble(object value)
		{
			return value;
		}

		bool IUserType.Equals(object x, object y)
		{
			if (x == null)
			{
				if (y == null)
					return true;
				return false;
			}

			return x.Equals(y);
		}

		int IUserType.GetHashCode(object x)
		{
			if (x == null)
				return 0;
			return x.GetHashCode();
		}

	    public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
	    {
	        var obj = NHibernateUtil.Decimal.NullSafeGet(rs, names[0], session);

	        if (obj == null || obj == DBNull.Value)
	            return null;

	        return Convert.ToInt16(obj) != 0;
	    }

	    bool IUserType.IsMutable => false;

	    object IUserType.Replace(object original, object target, object owner)
		{
			return original;
		}

		Type IUserType.ReturnedType => typeof(bool);

	    SqlType[] IUserType.SqlTypes => new[] { SqlTypeFactory.Int16 };
	}
}
