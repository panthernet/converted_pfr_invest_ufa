﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Schema;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.Enums;
using PFR_INVEST.Web2.DataObjects.Web.Extensions;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;

namespace PFR_INVEST.Web2.Database.EF
{
    public partial class AppDatabase
    {
        private const string ONES_EXPORT_XSD = @"Database/XSD/Scheme_1C.xsd";
        private readonly bool _onesDebugNotWriteUsedExportIds = false;
        private XmlSchemaSet _onesExportSchema;

        public object OnesExportStateLocker  = new object();
        /// <summary>
        /// Хранит текущее кол-во обработанных сущностей для раздела
        /// </summary>
        private readonly int[] _onesExportCounts = new int[6];
        /// <summary>
        /// Хранит макс. кол-во обработанных сущностей для раздела
        /// </summary>
        private readonly int[] _onesExportMaxCounts = new int[6];
        /// <summary>
        /// Хранит состояние сессии экспорта/импорта 1С для раздела
        /// </summary>
        private static readonly bool[] OnesExportStates = new bool[6];
        
        private List<Settings1CWeb> _onesOpSettings;
        private List<Def1CWeb> _onesOpDefs;


        #region Data classes

        private class StringWrapper
        {
            public StringWrapper(object data)
            {
                _value = data;
            }
            private readonly object _value;
            public string Value => _value?.ToString() ?? "";
        }

        internal class OnesExportDocReqTransferSumPP
        {
            public string Type_KBK { get; set; }
            public string KBK { get; set; }
            public string Bag { get; set; }
            public decimal Sum { get; set; }
        }

        internal class OnesExportDocReqTransferOrg
        {
            public string INN { get; set; }
            public string KPP { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
        }

        internal class OnesExportDocReqTransferAcc
        {
            public string BS { get; set; }
            public string BIC { get; set; }
            public string Name_BIC { get; set; }
            public string BS_KS { get; set; }
        }

        private class OnesExportDocReqTransferPayment
        {
            public OnesExportDocReqTransferOrg Organization_Rcp { get; set; }
            public OnesExportDocReqTransferAcc Account_Rcp { get; set; }
            public string AgreementNumber { get; set; }
            public DateTime? AgreementDate { get; set; }
            public string DepositNumber { get; set; }
            public decimal Sum_payment { get; set; }
            /// <summary>
            /// Этот список в XML кладется как SUM_PP -> SUM, SUM...
            /// </summary>
            public List<OnesExportDocReqTransferSumPP> Sum_PP { get; set; }
            public string Purpose { get; set; }
            /// <summary>
            /// Временно хранит ID сущности для внутренней обработки
            /// </summary>
            public long ID { get; set; }

            // public string OPFR_NAME { get; set; }
            // public string OPFR_KBK { get; set; }
            // public decimal OPFR_SUM { get; set; }
            //Временно хранит ID перечислений, с которых взята сумма ОПФР
            public long[] OPFR_IDLIST { get; set; }

            public OnesExportDocReqTransferPayment()
            {
                Organization_Rcp = new OnesExportDocReqTransferOrg();
                Account_Rcp = new OnesExportDocReqTransferAcc();
                Sum_PP = new List<OnesExportDocReqTransferSumPP>();
            }
        }

        private class OnesExportDocReqTransfer
        {
            /// <summary>
            /// Текущая дата со временем
            /// </summary>
            public DateTime? WorkDate { get; set; }

            public string Type { get; set; }

            public string Number { get; set; }

            public DateTime? Date { get; set; }

            public List<OnesExportDocReqTransferPayment> Payments { get; }

            /// <summary>
            /// Временно хранит ID сущности для внутренней обработки
            /// </summary>
            public long ID { get; set; }

            public OnesExportDocReqTransfer()
            {
                Payments = new List<OnesExportDocReqTransferPayment>();
            }
        }
#endregion

        public List<object> StartOnesExport(string exportPath, bool isClient, Def1CWeb.IntGroup type, List<long> idList = null)
        {
            var resultList = new List<object>();
            try
            {
                //server check
                if (!isClient && !Directory.Exists(exportPath))
                {
                    try
                    {
                        Directory.CreateDirectory(exportPath);
                    }
                    catch
                    {
                        resultList.Add(WebServiceDataErrorWeb.DirectoryNotFound);
                        return resultList;
                    }
                }
                //ищем активную сессию экспорта
                lock (OnesExportStateLocker)
                {
                    if (OnesExportStates[(int)type])
                    {
                        resultList.Add(WebServiceDataErrorWeb.SessionInProgress);
                        return resultList;
                    }
                }
                SetSessionProgess(type, true);

                //создаем новую сессию
                var sessionId = WCFClient.EF.SaveEntity(new Session1CWeb {SessionDate = DateTime.Now, IsImport = 0});
                _onesOpSettings =  WCFClient.GetEFServiceFunc().Settings1C.ToList();
                _onesOpDefs =  WCFClient.GetEFServiceFunc().Defs1C.ToList();

                //JournalLogger.LogEvent($"Операция экспорта данных для 1С в ПК ИБиБУ ПФР. Сессия ID={sessionId}", JournalEventType.EXPORT_DATA, 0, "", "", null, "");

                ThreadPool.QueueUserWorkItem(state =>
                {
                    try
                    {
                        if (_onesExportSchema == null)
                        {
                            var xsdPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ONES_EXPORT_XSD);
                            if (!File.Exists(xsdPath))
                            {
                                OnesLogJournal("Не найдена XSD схема для импорта/экспорта данных 1С!", sessionId, Journal1CWeb.Actions.Export, Journal1CWeb.LogTypes.Error);
                                return;
                            }
                            using (var xsdReader = new StreamReader(xsdPath))
                            {
                                var xmlSchema = XmlSchema.Read(xsdReader, (sender, eventArgs) =>
                                {
                                });
                                var xmlSchemaSet = new XmlSchemaSet();
                                xmlSchemaSet.Add(xmlSchema);
                                xmlSchemaSet.Compile();
                                _onesExportSchema = xmlSchemaSet;
                            }
                        }
                        var exportDate = DateTime.Now;
                        var exportType = FileExport1CWeb.OnesExportType.Finregister;
                        switch (type)
                        {
                            case Def1CWeb.IntGroup.All:
                                OnesLogJournal("Запущена операция экспорта файлов 1С для всех разделов", sessionId);
                                OnesGenerateExport(FileExport1CWeb.OnesExportType.Finregister, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(FileExport1CWeb.OnesExportType.ReqTransfer, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(FileExport1CWeb.OnesExportType.ReqTransferVr, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(FileExport1CWeb.OnesExportType.Deposit, exportDate, sessionId, isClient, exportPath, idList);
                                break;
                            case Def1CWeb.IntGroup.Npf:
                                exportType = FileExport1CWeb.OnesExportType.Finregister;
                                break;
                            case Def1CWeb.IntGroup.Si:
                                exportType = FileExport1CWeb.OnesExportType.ReqTransfer;
                                break;
                            case Def1CWeb.IntGroup.Vr:
                                exportType = FileExport1CWeb.OnesExportType.ReqTransferVr;
                                break;
                            case Def1CWeb.IntGroup.Depo:
                                exportType = FileExport1CWeb.OnesExportType.Deposit;
                                break;
                            case Def1CWeb.IntGroup.Opfr:
                                exportType = FileExport1CWeb.OnesExportType.Opfr;
                                break;
                            default:
                                throw new Exception("Неизвестный тип экспорта 1С!");
                        }
                        if (type == Def1CWeb.IntGroup.All)
                            OnesLogJournal("Завершена операция экспорта файлов 1С для всех разделов", sessionId);
                        else
                        {
                            OnesLogJournal("Запущена операция экспорта файлов 1С для раздела " + GetSessionTypeName(exportType), sessionId);
                            OnesGenerateExport(exportType, exportDate, sessionId, isClient, exportPath, idList);
                            OnesLogJournal("Завершена операция экспорта файлов 1С для раздела " + GetSessionTypeName(exportType), sessionId);
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.LogEx(ex);
                    }
                    finally
                    {
                        SetSessionProgess(type, false);
                        //сохраняем статус сессии
                        _onesOpSettings = null;
                        _onesOpDefs = null;
                    }
                });
                return resultList;
            }
            catch (Exception ex)
            {
                SetSessionProgess(type, false);
                Logger.LogEx(ex);
                resultList.Add(WebServiceDataErrorWeb.GeneralException);
                return resultList;
            }
        }

        private void OnesGenerateExport(FileExport1CWeb.OnesExportType type, DateTime exportDate, long sessionId, bool isClient, string exportPath, List<long> inputIdList)
        {
            string partName;
            switch (type)
            {
                case FileExport1CWeb.OnesExportType.Finregister:
                    partName = " НПФ";
                    break;
                case FileExport1CWeb.OnesExportType.ReqTransfer:
                case FileExport1CWeb.OnesExportType.ReqTransferVr:
                    partName = "СИ/ВР";
                    break;
                case FileExport1CWeb.OnesExportType.Deposit:
                    partName = "Депозитов";
                    break;
                case FileExport1CWeb.OnesExportType.Opfr:
                    partName = " Распоряжений";
                    break;
                default: throw new Exception("unk type");
            }

            try
            {
                List<object> lResult;
                var resultList = new List<OnesExportDocReqTransfer>();
                var intGroup = 4;

                switch (type)
                {
                    case FileExport1CWeb.OnesExportType.Finregister:
                        resultList = OnesGenerateNPFExport(exportDate, inputIdList, out lResult);
                        intGroup = (int)Def1CWeb.IntGroup.Npf;
                        break;
                 /*   case FileExport1CWeb.OnesExportType.ReqTransfer:
                        resultList = OnesGenerateSIExport(exportDate, false, inputIdList, out lResult);
                        intGroup = (int)Def1CWeb.IntGroup.Si;
                        break;
                    case FileExport1CWeb.OnesExportType.ReqTransferVr:
                        resultList = OnesGenerateSIExport(exportDate, true, inputIdList, out lResult);
                        intGroup = (int)Def1CWeb.IntGroup.Vr;
                        break;
                    case FileExport1CWeb.OnesExportType.Deposit:
                        resultList = OnesGenerateDepositExport(exportDate, inputIdList, out lResult);
                        intGroup = (int)Def1CWeb.IntGroup.Depo;
                        break;
                    case FileExport1CWeb.OnesExportType.Opfr:
                        resultList = OnesGenerateOpfrExport(exportDate, inputIdList, out lResult);
                        intGroup = (int)Def1CWeb.IntGroup.Opfr;
                        break;*/
                    default:
                        intGroup = (int)Def1CWeb.IntGroup.All;
                        return;
                }

                lResult.ForEach(a => OnesLogJournal(a.ToString(), sessionId, Journal1CWeb.Actions.Export, Journal1CWeb.LogTypes.Error));


                var bodyList = new List<ImportFileListWebItem>();
                string typeName = GetSessionTypeName(type);
                lock (OnesExportStateLocker) { _onesExportMaxCounts[intGroup] = resultList.Sum(a => a.Payments.Count); }


                resultList.ForEach(result =>
                {
                    //получили result можно генерить XML
                    List<string> errorsList;
                    var resultFile = OnesGenerateExportReqTransferXML(result, out errorsList);
                    //обработка результатов идет отдельной транзакцией для каждого файла, чтобы исключить только ошибочные

                    if (resultFile == null)
                    {
                        //логируем ошибки генерации XML
                        OnesLogJournal(string.Format(@"Ошибка генерации XML файла для '{0}'({3}) на дату '{1}': {2}", result.Type, result.Date, string.Join("|\n", errorsList.ToArray()), partName), sessionId,
                            Journal1CWeb.Actions.Export, Journal1CWeb.LogTypes.Error);
                    }
                    else
                    {
                        try
                        {
                            var id =
                                WCFClient.EF.SaveEntity(
                                    new RepositoryImpExpFileWeb(true)
                                    {
                                        Repository = resultFile.FileContent,
                                        Key = (int) RepositoryImpExpFileWeb.Keys.OnesExportXml,
                                        UserName = WCFClient.LocalInstance.DomainUser,
                                        DDate = DateTime.Now.Date,
                                        TTime = DateTime.Now.TimeOfDay,
                                        Comment = "-",
                                        ImpExp = 1,
                                        Status = !isClient ? 1 : 0
                                    });
                            //помечаем данные, как успешно экспортированные
                            var idList = result.Payments.Where(a => a.ID != 0).Select(a => a.ID).ToList();
                            result.Payments.Where(a => a.OPFR_IDLIST != null).ForEach(a => idList.AddRange(a.OPFR_IDLIST));
                            idList = idList.Distinct().ToList();

                            if (!_onesDebugNotWriteUsedExportIds)
                                OnesSaveExportEntries(idList, type, sessionId, id);
                            OnesLogJournal(@"Экспортирован файл 1С для раздела " + typeName, sessionId, Journal1CWeb.Actions.ExportComplete, Journal1CWeb.LogTypes.Info, id);
                            if (!isClient)
                                bodyList.Add(resultFile);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogEx(ex);
                            OnesLogJournal(string.Format(@"Oшибка сохранения XML файла для {0} ! ОШИБКА: " + ex, partName), sessionId, Journal1CWeb.Actions.Export, Journal1CWeb.LogTypes.Error);
                        }
                    }

                    lock (OnesExportStateLocker)
                    {
                        _onesExportCounts[intGroup] += result.Payments.Count;
                    }
                });

                //записываем файлы на сервер, если требуется
                if (!isClient && bodyList.Count > 0)
                {
                    var count = 0;
                    //вычисляем порядковый номер в рамках дня
                    //АНАЛОГИЧНАЯ ЛОГИКА В OnesExecuteExportWrapper
                    Directory.GetFiles(exportPath, "docip_*.xml").ToList().ForEach(a =>
                    {
                        var array = Path.GetFileNameWithoutExtension(a)?.Split('_');
                        var src = array?[2];
                        if (DateTime.ParseExact(src, "yyMMdd", new DateTimeFormatInfo()).Date == DateTime.Now.Date)
                        {
                            var nCount = Convert.ToInt32(array?[4]);
                            count = nCount > count ? nCount : count;
                        }
                    });

                    bodyList.ForEach(a =>
                    {
                        if (a.FileContent == null || a.FileContent.Length == 0) return;
                        var file = Path.Combine(exportPath, $"docip_RequestForTransfer_{DateTime.Now:yyMMdd_hhmmss}_{++count}.xml");
                        using (var writer = File.CreateText(file))
                        {
                            writer.Write(Encoding.Default.GetChars(GZipCompressor.Decompress(a.FileContent)));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.LogEx(ex);
                //логируем ошибки генерации XML
                OnesLogJournal($@"Критическая ошибка генерации XML файлов для {partName} ! ОШИБКА: " + ex, sessionId, Journal1CWeb.Actions.Export, Journal1CWeb.LogTypes.Error);
            }
        }

#region Генерация экспорта финреестров (НПФ)

        private const string ONES_FROM_NPF_QUERY = @"
            from FinregisterHib fr 
            join fr.Register r
            join fr.DebitAccount da
            {0}
            join da.Contragent ca
            join ca.LegalEntities le
            {1}
            join le.BankAccounts bank
            where r.Content in (:list) and r.TrancheDate is not null and coalesce(r.IsCustomArchive,0)=0 and fr.StatusID <> -1 and ca.StatusID <> -1 and bank.CloseDate is null
                and lower(trim(fr.Status)) not in (:status)
                and (select count(fe.ID) from OnesFileExport fe where fe.EntityID = fr.ID and fe.EntityType = :type) = 0
                and (select count(pp.ID) from AsgFinTr pp where pp.FinregisterID = fr.ID and pp.StatusID != -1) = 0";


        private List<OnesExportDocReqTransfer> OnesGenerateNPFExport(DateTime exportDate, ICollection<long> inputIdList, out List<object> midResult)
        {
            midResult = new List<object>();
            var resList = new List<OnesExportDocReqTransfer>();
            var packs = OnesGenerateOpPacks<ElementWeb>(Def1CWeb.Groups.NPF);
            packs.ForEach(pack =>
            {
                //TODO
                var statusesList = new string[] {RegisterIdentifier.FinregisterStatuses.Transferred, RegisterIdentifier.FinregisterStatuses.NotTransferred};
                var contentList = pack.Value.Select(a => a.Name).ToList();
                var list = WCFClient.EF.SpnFinregisters.Where(fr => fr.StatusID != -1 && !statusesList.Contains(fr.Status) && contentList.Contains(fr.Register.Content)
                                                                    && fr.Register.TrancheDate != null && (fr.Register.IsCustomArchive == null || fr.Register.IsCustomArchive == 0)
                                                                    && fr.AccountDebit.Contragent.StatusID != -1 &&
                                                                    fr.AccountDebit.Contragent.LegalEntity.BankAccounts.FirstOrDefault().CloseDate == null).ToList()
                    .Select(a=> new object[] {a, a.AccountDebit.Contragent.LegalEntity, a.AccountDebit.Contragent.LegalEntity.BankAccounts.FirstOrDefault(), a.Register.Content, a.Register.TrancheDate, a.Register.TrancheNumber, a.Register.PayAssignment} );

              /*  var list = SessionWrapper(ses => ses.CreateQuery($@"select distinct fr, le, bank, r.Content, r.TrancheDate, r.TrancheNumber, r.PayAssignment {
                            string.Format(ONES_FROM_NPF_QUERY, "", "left join fetch le.Licensies lic")
                        }  and fr.ID in (:inputIdList)")
                    .SetParameterList("list", pack.Value.Select(a => a.Name).ToList())
                    .SetParameterList("inputIdList", inputIdList)
                    .SetParameter("type", (int)FileExport1CWeb.OnesExportType.Finregister)
                    .SetParameterList("status", new object[] { RegisterIdentifier.FinregisterStatuses.Transferred, RegisterIdentifier.FinregisterStatuses.NotTransferred })
                    .List<object[]>()
                    .ToList());*/
                var grouppedList = list.GroupBy(a => new { ((DateTime?)a[4]).Value.Date, new StringWrapper(string.IsNullOrEmpty((string)a[5]) ? "1" : a[5]).Value });
                grouppedList.ForEach(group =>
                {
                    var result = new OnesExportDocReqTransfer
                    {
                        WorkDate = exportDate,
                        //дата заявки
                        Date = group.Key.Date,
                        //Номер заявки
                        Number = group.Key.Value,
                        //Тип передаваемых сведений 
                        Type = pack.Key
                    };

                    //обработка финреестров для Payments
                    group.ToList().ForEach(fro =>
                    {
                        var fr = fro[0] as FinregisterWeb;
                        //var frId = (long) fro[1];
                        var le = (LegalEntityWeb)fro[1];
                       //TODO var lic = le.Licensies?.OrderByDescending(a => a.CloseDate).FirstOrDefault();

                        var oName = le.FullName;//(string) fro[1];
                        var oShortName = le.ShortName;//(string) fro[2];
                        var bank = fro[2] as BankAccountWeb;
                        //var opContent = (string)fro[3];
                        var payAss = (string)fro[6];

                        var payment = new OnesExportDocReqTransferPayment
                        {
                            ID = fr.ID,
                            Organization_Rcp =
                            {
                                Name = oName,
                                ShortName = oShortName,
                                INN = le.INN,
                                KPP = le.OKPP
                            },
                            Account_Rcp =
                            {
                                BIC = bank.BIK,
                                BS = bank.AccountNumber,
                                BS_KS = bank.CorrespondentAccountNumber,
                                Name_BIC = bank.BankName
                            },
                            //TODO AgreementDate = lic?.RegistrationDate,
                            //AgreementNumber = lic?.Number,
                            DepositNumber = null,
                            Purpose = payAss,
                            Sum_payment = fr.Count ?? 0
                        };

                        payment.Sum_PP.Add(new OnesExportDocReqTransferSumPP
                        {
                            Sum = fr.Count ?? 0,
                            Bag = null,
                            KBK = null,
                            Type_KBK = null
                        });
                        result.Payments.Add(payment);
                    });
                    resList.Add(result);
                });
            });
            return resList;
        }
#endregion

        private Dictionary<string, T[]> OnesGenerateOpPacks<T>(string exportGroup)
        {
            var opPacks = new Dictionary<string, T[]>();
            var egEntries = _onesOpDefs.Where(a => a.ExportGroup == exportGroup).ToList();
            //хак для ВР
            if (exportGroup == Def1CWeb.Groups.VR)
                egEntries = _onesOpDefs.Where(a => a.ExportGroup == Def1CWeb.Groups.UK).ToList();
            var egIds = egEntries.Select(a => a.ID).ToList();
            var opEntries = _onesOpSettings.Where(a => egIds.Contains(a.SetDefID));

            Type type;
            switch (exportGroup)
            {
                case Def1CWeb.Groups.NPF:
                case Def1CWeb.Groups.DEPOSIT:
                    type = typeof(ElementWeb);
                    break;
                //case Def1CWeb.Groups.UK:
               // case Def1CWeb.Groups.VR:
                   //TODO type = typeof(SPNOperationWeb);
                    break;
                default:
                    throw new Exception("Неизвестный тип операции!");
            }

            var ids = opEntries.Select(a => exportGroup == Def1CWeb.Groups.DEPOSIT ? a.PortfolioTypeID : a.OperationContentID)
                .ToArray();
            var els = WCFClient.EF.Elements.Where(a => ids.Contains(a.ID));

            egEntries.ForEach(a =>
            {
                var groupElementsId = _onesOpSettings.Where(b => egIds.Contains(b.SetDefID) && b.SetDefID == a.ID)
                    .Select(b => exportGroup == Def1CWeb.Groups.DEPOSIT ? b.PortfolioTypeID : b.OperationContentID)
                    .ToList();
                switch (exportGroup)
                {
                    case Def1CWeb.Groups.NPF:
                    case Def1CWeb.Groups.DEPOSIT:
                        var list = els.Cast<ElementWeb>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list.Any()) return;
                        opPacks.Add(a.Name, list.Cast<T>().ToArray());
                        break;
                    //TODO
                    /*case Def1CWeb.Groups.UK:
                    case Def1CWeb.Groups.VR:
                        var list2 = els.Cast<SPNOperation>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list2.Any()) return;
                        opPacks.Add(a.Name, list2.Cast<T>().ToArray());
                        break;*/
                    default:
                        throw new Exception("Неизвестный тип операции!");
                }
            });
            return opPacks;
        }

         private ImportFileListWebItem OnesGenerateExportReqTransferXML(OnesExportDocReqTransfer input, out List<string> errorsList)
        {
            errorsList = new List<string>();

            var doc = XML.CreateXmlDocumentWithRoot("RequestForTransfer");
            doc.DocumentElement.SetAttribute("xmlns", "http://www.pfr.ru");
            doc.DocumentElement.SetAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "WorkDate", input.WorkDate.HasValue ? input.WorkDate.Value.ToOnesFullXmlString() : "");
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Type", input.Type);
            var pNode = XML.CreateAddXmlNode(doc.FirstChild, "Payments");

            input.Payments.ForEach(payment =>
            {
                var ipNode = XML.CreateAddXmlNode(pNode, "Payment");

                var p2Node = XML.CreateAddXmlNode(ipNode, "Organization_Rcp");
                XML.CreateAddXmlNodeWithValue(p2Node, "INN", payment.Organization_Rcp.INN);
                XML.CreateAddXmlNodeWithValue(p2Node, "KPP", payment.Organization_Rcp.KPP);
                XML.CreateAddXmlNodeWithValue(p2Node, "Name", payment.Organization_Rcp.Name);
                XML.CreateAddXmlNodeWithValue(p2Node, "ShortName", payment.Organization_Rcp.ShortName);//Н
                p2Node = XML.CreateAddXmlNode(ipNode, "Account_Rcp");
                XML.CreateAddXmlNodeWithValue(p2Node, "BS", payment.Account_Rcp.BS); //Н
                XML.CreateAddXmlNodeWithValue(p2Node, "BIC", payment.Account_Rcp.BIC);
                XML.CreateAddXmlNodeWithValue(p2Node, "Name_BIC", payment.Account_Rcp.Name_BIC);
                XML.CreateAddXmlNodeWithValue(p2Node, "BS_KS", payment.Account_Rcp.BS_KS);//Н


                XML.CreateAddXmlNodeWithValue(ipNode, "AgreementNumber", payment.AgreementNumber);//Н
                XML.CreateAddXmlNodeWithValue(ipNode, "AgreementDate", payment.AgreementDate.HasValue ? payment.AgreementDate.Value.ToOnesXmlString() : null);//Н
                XML.CreateAddXmlNodeWithValue(ipNode, "DepositNumber", payment.DepositNumber); //Н
                XML.CreateAddXmlNodeWithValue(ipNode, "Sum_payment", payment.Sum_payment.ToOnesXmlString());
                if (payment.Sum_PP.Count > 0)
                {
                    var ppNode = XML.CreateAddXmlNode(ipNode, "Sum_PP");
                    payment.Sum_PP.ForEach(sumpp =>
                    {
                        var iNode = XML.CreateAddXmlNode(ppNode, "Sum");
                        if (!string.IsNullOrEmpty(sumpp.Type_KBK))
                            XML.CreateAddXmlNodeWithValue(iNode, "Type_KBK", sumpp.Type_KBK);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "KBK", sumpp.KBK);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "Bag", sumpp.Bag);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "Sum", sumpp.Sum.ToOnesXmlString());
                    });
                }
                XML.CreateAddXmlNodeWithValue(ipNode, "Purpose", payment.Purpose);
            });
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Number", input.Number);
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Date", input.Date.HasValue ? input.Date.Value.ToOnesXmlString() : "");


            var result = new ImportFileListWebItem
            {
                FileContent = Encoding.Default.GetBytes(doc.OuterXml),
                IsCompressed = false
            };

            var valResult = XML.ValidateXmlFile(result.FileContent, _onesExportSchema);
            if (!string.IsNullOrEmpty(valResult))
            {
                errorsList = valResult.Split('|').ToList();
                return null;
            }

            //создаем сжатый XML файл
            result.Compress();
            return result;
        }

        /// <summary>
        /// Запись об успешном экспорте сущности
        /// </summary>
        /// <param name="entityIdList">Список идентификаторов сущностей</param>
        /// <param name="entityType">Тип сущности</param>
        /// <param name="sessionId">Идентфикатор сессии</param>
        /// <param name="session">Сессия хибернейта</param>
        /// <param name="exportId"></param>
        private static void OnesSaveExportEntries(List<long> entityIdList, FileExport1CWeb.OnesExportType entityType, long sessionId, long exportId)
        {
            // var sw = Stopwatch.StartNew();
            var eType = (int)entityType;
            const string defaultQuery = "INSERT INTO PFR_BASIC.ONES_FILEEXPORT(ENTITY_ID, ENTITY_TYPE, SESSION_ID, EXPORT_ID) ";
            var query = defaultQuery;
            const int maxCount = 15;
            var count = 0;
            var stopValue = entityIdList.Count - 1;
            for (int i = 0; i < entityIdList.Count; i++)
            {
                var item = entityIdList[i];
                count++;
                query += $@"VALUES ({item}, {eType}, {sessionId}, {exportId}) ";
                if (count == maxCount)
                {
                    count = 0;
                    WCFClient.EF.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, query);
                    query = defaultQuery;
                }
                else if (i != stopValue) query += " UNION ALL ";
            }
            if (count != 0)
                WCFClient.EF.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, query);
            //SaveEntities(list, session);
        }

        private static string GetSessionTypeName(FileExport1CWeb.OnesExportType type)
        {
            switch (type)
            {
                case FileExport1CWeb.OnesExportType.ReqTransfer:
                    return "СИ";
                case FileExport1CWeb.OnesExportType.ReqTransferVr:
                    return "ВР";
                case FileExport1CWeb.OnesExportType.Finregister:
                    return "НПФ";
                case FileExport1CWeb.OnesExportType.Deposit:
                    return "Депозиты";
                case FileExport1CWeb.OnesExportType.Opfr:
                    return "Бэк-офис/Расчеты с ОПФР";
                default: throw new Exception("Неизвестный тип экспорта!");
            }
        }

        private static void OnesLogJournal(string message, long sessionId, Journal1CWeb.Actions action = Journal1CWeb.Actions.General, Journal1CWeb.LogTypes logType = Journal1CWeb.LogTypes.Info, long? importId = null)
        {
            var msg = string.IsNullOrEmpty(message) ? "" : message.Length > 2500 ? message.Substring(0, 2499) : message;
            WCFClient.EF.SaveEntity(new Journal1CWeb
            {
                Message = msg,
                SessionID = sessionId,
                ImportID = importId,
                Action = (int)action,
                LogType = (int)logType,
                LogDate = DateTime.Now
            });
        }

        private void SetSessionProgess(Def1CWeb.IntGroup type, bool isEnabled)
        {
            lock (OnesExportStateLocker)
            {
                OnesExportStates[(int)type] = isEnabled;
            }
        }


        /*   private List<OnesOpSettings> _onesOpSettings;
        private List<OnesSettingsDef> _onesOpDefs;

                private const string ONES_FROM_NPF_QUERY = @"
            from FinregisterHib fr 
            join fr.Register r
            join fr.DebitAccount da
            {0}
            join da.Contragent ca
            join ca.LegalEntities le
            {1}
            join le.BankAccounts bank
            where r.Content in (:list) and r.TrancheDate is not null and coalesce(r.IsCustomArchive,0)=0 and fr.StatusID <> -1 and ca.StatusID <> -1 and bank.CloseDate is null
                and lower(trim(fr.Status)) not in (:status)
                and (select count(fe.ID) from OnesFileExport fe where fe.EntityID = fr.ID and fe.EntityType = :type) = 0
                and (select count(pp.ID) from AsgFinTr pp where pp.FinregisterID = fr.ID and pp.StatusID != -1) = 0";

        public IQueryable GetSpnListForExport(OnesSettingsDef.IntGroup part)
        {
            try
            {
                var nameList = new List<string>();
                switch (part)
                {
                    case OnesSettingsDef.IntGroup.Npf:
                        OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.NPF, session)
                            .Select(a => a.Value.Select(b => b.Name)).
                            ForEach(a => nameList.AddRange(a));

                        return from fr in SpnFinregisters where 
                               fr.Register.Content

                        OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.NPF, session)
                            .Select(a => a.Value.Select(b => b.Name)).ForEach(a => nameList.AddRange(a));
                        if (!nameList.Any()) return new List<OnesExportDataItem>();

                        list = session.CreateQuery(
                                $"select distinct fr.ID, r.ID, le.FormalizedName, ad.Name, r.RegNum, r.RegDate, fr.Count, fr.ZLCount, fr.RegNum, fr.Date, fr.Status, r.Content {string.Format(ONES_FROM_NPF_QUERY, "left join r.ApproveDoc ad", "")}")
                            .SetParameterList("list", nameList)
                            .SetParameter("type", (int) OnesFileExport.OnesExportType.Finregister)
                            .SetParameterList("status", new object[] {RegisterIdentifier.FinregisterStatuses.Transferred, RegisterIdentifier.FinregisterStatuses.NotTransferred})
                            .List<object[]>()
                            .ToList();
                        return list.Select(a => new OnesExportDataItem
                        {
                            ID = (long) a[0],
                            Item1 = (string) a[11], //content
                            Item2 = $"{a[1]} \tдокумент: {a[3]}     \r\n\t№ {a[4]} от: {(a[5] == null ? "" : ((DateTime) a[5]).ToOnesXmlString())}",
                            Item3 = (string) a[10], //status
                            Item4 = (string) a[2], //name
                            Item5 = Convert.ToDecimal(a[6]).ToString("n2", culture), //sum
                            Item6 = Convert.ToInt64(a[7]).ToString("N0", culture), //zl
                            Item7 = a[8], //regnum
                            Item8 = a[9] //date
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                Logger.LogEx(ex, nameof(GetSpnListForExport));
                throw;
            }
        }


        private Dictionary<string, T[]> OnesGenerateOpPacks<T>(string exportGroup)
        {
            var opPacks = new Dictionary<string, T[]>();
            var egEntries = _onesOpDefs.Where(a => a.ExportGroup == exportGroup).ToList();
            //хак для ВР
            if (exportGroup == OnesSettingsDef.Groups.VR)
                egEntries = _onesOpDefs.Where(a => a.ExportGroup == OnesSettingsDef.Groups.UK).ToList();
            var egIds = egEntries.Select(a => a.ID).ToList();
            var opEntries = _onesOpSettings.Where(a => egIds.Contains(a.SetDefID));

            Type type;
            switch (exportGroup)
            {
                case OnesSettingsDef.Groups.NPF:
                case OnesSettingsDef.Groups.DEPOSIT:
                    type = typeof(Element);
                    break;
                case OnesSettingsDef.Groups.UK:
                case OnesSettingsDef.Groups.VR:
                    type = typeof(SPNOperation);
                    break;
                default:
                    throw new Exception("Неизвестный тип операции!");
            }

            var els = GetListByPropertyConditions(type, session, new List<ListPropertyCondition>
            {
                ListPropertyCondition.In("ID", opEntries.Select(a => exportGroup == OnesSettingsDef.Groups.DEPOSIT ? a.PortfolioTypeID : a.OperationContentID)
                    .Cast<object>()
                    .ToArray())
            });

            egEntries.ForEach(a =>
            {
                var groupElementsId = _onesOpSettings.Where(b => egIds.Contains(b.SetDefID) && b.SetDefID == a.ID)
                    .Select(b => exportGroup == OnesSettingsDef.Groups.DEPOSIT ? b.PortfolioTypeID : b.OperationContentID)
                    .ToList();
                switch (exportGroup)
                {
                    case OnesSettingsDef.Groups.NPF:
                    case OnesSettingsDef.Groups.DEPOSIT:
                        var list = els.Cast<Element>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list.Any()) return;
                        opPacks.Add(a.Name, list.Cast<T>().ToArray());
                        break;
                    case OnesSettingsDef.Groups.UK:
                    case OnesSettingsDef.Groups.VR:
                        var list2 = els.Cast<SPNOperation>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list2.Any()) return;
                        opPacks.Add(a.Name, list2.Cast<T>().ToArray());
                        break;
                    default:
                        throw new Exception("Неизвестный тип операции!");
                }
            });
            return opPacks;
        }*/
    }
}