﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassFinregisterMap: EntityTypeConfiguration<FinregisterWeb>
    {
        public ClassFinregisterMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("finregister");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.RegisterID).HasColumnName("reg_id");
            Property(t => t.Date).HasColumnName("date");
            Property(t => t.Count).HasColumnName("count");
            Property(t => t.RegNum).HasColumnName("regnum");
            Property(t => t.CFRCount).HasColumnName("cfrcount");
            Property(t => t.CrAccID).HasColumnName("cr_acc_id");
            Property(t => t.DbtAccID).HasColumnName("dbt_acc_id");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.Status).HasColumnName("status");
            Property(t => t.LetterDate).HasColumnName("letterdt");
            Property(t => t.LetterNum).HasColumnName("letternum");
            Property(t => t.ZLCount).HasColumnName("zlcount");
            Property(t => t.FinDate).HasColumnName("findate");
            Property(t => t.FinNum).HasColumnName("finnum");
            Property(t => t.StatusID).HasColumnName("status_id");
            Property(t => t.FinMoveType).HasColumnName("finmovetype");
            Property(t => t.ParentFinregisterID).HasColumnName("parent_finregister_id");
            Property(t => t.SelectedContentID).HasColumnName("selected_content_id");
            Property(t => t.SelectedRegisterID).HasColumnName("selected_register_id");
            Property(t => t.IsTransfered).HasColumnName("is_transferred");

            HasRequired(t => t.Register).WithMany(t=> t.Finregisters).HasForeignKey(t=> t.RegisterID);
            HasRequired(t => t.AccountCredit).WithMany(t=> t.Finregisters).HasForeignKey(t=> t.CrAccID);
            HasRequired(t => t.AccountDebit).WithMany(t=> t.FinregistersD).HasForeignKey(t=> t.DbtAccID);
            HasMany(t => t.AsgFinTrs).WithOptional(t => t.Finregister).HasForeignKey(t => t.FinregisterID);
            HasMany(t => t.AsgFinTrExports).WithOptional(t => t.Finregister).HasForeignKey(t => t.FinregisterID);
        }
    }
}