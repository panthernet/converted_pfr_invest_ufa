﻿using System.Data.Entity.ModelConfiguration;
using NHibernate.Linq;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassContragentAccountMap : EntityTypeConfiguration<ContragentWeb>
    {
        public ClassContragentAccountMap()
        {
            // Primary Key
            HasKey(t => t.ID);
            // Table & Column Mappings
            ToTable("contragent");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.TypeName).HasColumnName("type");
            Property(t => t.StatusID).HasColumnName("status_id");         
           // Property(t => t.LegalEntityID).HasColumnName("le_id");

            Ignore(t => t.LegalEntityID);
            //Ignore(t => t.StatusID);

            HasOptional(t => t.LegalEntity).WithOptionalPrincipal(t => t.Contragent).Map(t=> t.MapKey("contragentid"));
           // HasRequired(t => t.Status).WithRequiredDependent();

        }
    }
}