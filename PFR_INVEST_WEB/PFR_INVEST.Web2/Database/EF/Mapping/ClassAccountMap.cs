﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassAccountMap : EntityTypeConfiguration<AccountWeb>
    {
        public ClassAccountMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("account");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");
            Property(t => t.ContragentID).HasColumnName("contragentid");
            Property(t => t.AccountTypeID).HasColumnName("accounttypeid");

            HasOptional(t => t.Contragent).WithMany(t => t.Accounts).HasForeignKey(t => t.ContragentID);
        }
    }
}