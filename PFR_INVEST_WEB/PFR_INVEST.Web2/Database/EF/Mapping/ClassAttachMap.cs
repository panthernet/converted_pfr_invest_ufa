﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassAttachMap : EntityTypeConfiguration<AttachWeb>
    {
        public ClassAttachMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("attach");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.DocumentID).HasColumnName("document_id");
            Property(t => t.AttachClassificID).HasColumnName("attach_classific_id");
            Property(t => t.Additional).HasColumnName("additional");
            Property(t => t.ExecutorID).HasColumnName("executor_id");
            Property(t => t.ControlDate).HasColumnName("control_date");
            Property(t => t.ExecutionDate).HasColumnName("ex_date");
            Property(t => t.AdditionalToExecution).HasColumnName("addexecute");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.FileContentId).HasColumnName("file_content_id");
            Property(t => t.TypeID).HasColumnName("typeid");
            Property(t => t.ExecutorName).HasColumnName("executor_name");
            Property(t => t.OriginalPlaceID).HasColumnName("originalplaceid");
            Property(t => t.StatusID).HasColumnName("status");

            HasOptional(t => t.AttachClassific);
            HasOptional(t => t.Executor);

            Ignore(t => t.OriginalPlace);
            Ignore(t => t.Type);
        }
    }
}