﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassDocumentMap : EntityTypeConfiguration<DocumentWeb>
    {
        public ClassDocumentMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("document");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.IncomingNumber).HasColumnName("in_regnum");
            Property(t => t.IncomingDate).HasColumnName("in_date");
            Property(t => t.LegalEntityID).HasColumnName("le_id");
            Property(t => t.OutgoingNumber).HasColumnName("out_regnum");
            Property(t => t.OutgoingDate).HasColumnName("out_date");
            Property(t => t.DocumentClassID).HasColumnName("doc_classific_id");
            Property(t => t.AdditionalInfoID).HasColumnName("add_info_id");
            Property(t => t.AdditionalInfoText).HasColumnName("add_info_text");
            Property(t => t.ExecutorID).HasColumnName("executor_id");
            Property(t => t.ExecutionDate).HasColumnName("ex_date");
            Property(t => t.AdditionalExecutionInfo).HasColumnName("addexecute");
            Property(t => t.ControlDate).HasColumnName("control_date");
            Property(t => t.OriginalStoragePlace).HasColumnName("orirginalplace");
            Property(t => t.Comment).HasColumnName("comment");
            Property(t => t.TypeID).HasColumnName("typeid");
            Property(t => t.ExecutorName).HasColumnName("executor_name");
            Property(t => t.StatusID).HasColumnName("status");

            HasMany(t => t.Attaches).WithRequired(t => t.Document).HasForeignKey(t => t.DocumentID);
            HasRequired(t => t.DocumentClass);
            HasOptional(t => t.Executor);
            HasOptional(t => t.AdditionalInfo);

            Ignore(t => t.Type);
        }
    }
}