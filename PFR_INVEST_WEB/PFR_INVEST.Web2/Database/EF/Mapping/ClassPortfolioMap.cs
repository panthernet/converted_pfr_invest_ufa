﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassPortfolioMap : EntityTypeConfiguration<PortfolioWeb>
    {
        public ClassPortfolioMap()
        {
            // Table & Column Mappings
            ToTable("portfolio");
            HasKey(t=>t.ID);
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Year).HasColumnName("year");
            Property(t => t.YearID).HasColumnName("year_id");
            Property(t => t.StatusID).HasColumnName("status_id");
            Property(t => t.TypeElementID).HasColumnName("type_id");

            Ignore(t => t.TypeEl);
            Ignore(t => t.Type);
            Ignore(t => t.Name);

            HasRequired(t => t.TypeElement);
        }
    }
}