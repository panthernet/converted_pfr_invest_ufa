﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassApproveDocMap: EntityTypeConfiguration<ApproveDocWeb>
    {
        public ClassApproveDocMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("approvedoc");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Name).HasColumnName("name");  
        }
    }
}