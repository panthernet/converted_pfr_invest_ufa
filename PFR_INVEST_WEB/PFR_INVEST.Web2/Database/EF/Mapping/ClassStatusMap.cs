﻿using System.Data.Entity.ModelConfiguration;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Database.EF.Mapping
{
    public class ClassStatusMap : EntityTypeConfiguration<StatusWeb>
    {
        public ClassStatusMap()
        {
            // Primary Key
            HasKey(t => t.ID);

            // Table & Column Mappings
            ToTable("status");
            Property(t => t.ID).HasColumnName("id");
            Property(t => t.Name).HasColumnName("status");

            HasMany(t => t.Contragents).WithRequired(t => t.Status).HasForeignKey(t => t.StatusID);
        }
    }
}