﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;

namespace PFR_INVEST.Web2.Database.EF
{
    public partial class AppDatabase
    {
        public IQueryable GetSpnArchiveList()
        {
            var sw = Stopwatch.StartNew();

            var y = SpnFinregisters.Where(fr =>
                (fr.Register.Kind.Equals(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase) ||
                 fr.Register.Kind.Equals(RegisterIdentifier.PFRtoNPF, StringComparison.OrdinalIgnoreCase)) && fr.Register.StatusID != -1 &&
                (fr.Status.Equals(RegisterIdentifier.FinregisterStatuses.Transferred, StringComparison.OrdinalIgnoreCase) ||
                 fr.Status.Equals(RegisterIdentifier.FinregisterStatuses.NotTransferred, StringComparison.OrdinalIgnoreCase)) && fr.StatusID != -1
                && (fr.IsTransfered == 1 || fr.Register.IsCustomArchive == 1) &&
                (fr.AccountDebit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ||
                 fr.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase))
            ).Select(fr => new RegistersListItem
            {
                RegisterID = fr.Register.ID,
                RegisterKind = fr.Register.Kind,
                ID = fr.ID,
                Content = fr.Register.Content,
                ContragentName = fr.Register == null
                    ? "(финреестры отсутствуют)"
                    : (fr.Register.Kind.Equals(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase)
                        ? fr.AccountCredit.Contragent.LegalEntity.FormalizedName
                        : fr.AccountDebit.Contragent.LegalEntity.FormalizedName),
                Count = fr.Count ?? 0,
                ZLCount = fr.ZLCount,
                FinregRegNum = fr.RegNum,
                FinregDate = fr.Date,
                RegNum = fr.Register.RegNum,
                RegDate = fr.Register.RegDate,
                Status = fr.Status,
                ApproveDocName = fr.Register.ApproveDoc.Name,
                TrancheDate = fr.Register.TrancheDate,
                IsCustomArchive = fr.Register.IsCustomArchive == 1,
                ParentFinregisterID = fr.ParentFinregisterID
            });
            Debug.WriteLine($"GetArchive: {sw.Elapsed.TotalMilliseconds}");
            return y;
        }

        public IQueryable GetSpnList()
        {
            var xx = from register in SpnRegisters
                from finreg in SpnFinregisters.Where(a => a.RegisterID == register.ID && a.StatusID!=-1).DefaultIfEmpty(null)
                where register.StatusID > 0 &&
                      (register.Kind.Equals(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase) ||
                       register.Kind.Equals(RegisterIdentifier.PFRtoNPF, StringComparison.OrdinalIgnoreCase))
                      && (register.IsCustomArchive == null || register.IsCustomArchive == 0)
                      && (finreg == null || finreg.IsTransfered == 0 && (finreg.AccountDebit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ||
                                                                         finreg.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase)))
                select new RegistersListItem
                {
                    RegisterID = register.ID,
                    RegisterKind = register.Kind,
                    ID = finreg == null ? register.ID : finreg.ID,
                    Content = register.Content,
                    ContragentName = finreg == null
                        ? "(финреестры отсутствуют)"
                        : (finreg.AccountCredit != null && finreg.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ? finreg.AccountCredit.Contragent.LegalEntity.FormalizedName : finreg.AccountDebit.Contragent.LegalEntity.FormalizedName),
                    Count = finreg == null ? 0 : finreg.Count ?? 0,
                    ZLCount = finreg == null ? 0 : finreg.ZLCount,
                    FinregRegNum = finreg == null ? "" : finreg.RegNum,
                    FinregDate = finreg == null ? null : finreg.Date,
                    RegNum = register.RegNum,
                    RegDate = register.RegDate,
                    Status = finreg == null ? null : finreg.Status,
                    ApproveDocName = register.ApproveDoc.Name,
                    DraftSum = finreg == null ? 0 : finreg.AsgFinTrs.Sum(a => a.DraftAmount ?? 0),
                    TrancheDate = register.TrancheDate,
                    IsCustomArchive = register.IsCustomArchive == 1,
                    ParentFinregisterID = finreg == null ? 0 : finreg.ParentFinregisterID
                };
            return xx;
        }

        public void SpnUpdateFinregisterStatusIssued(long parentId)
        {
            var q = from finreg in SpnFinregisters.Include(t=> t.AsgFinTrs).Where(a => a.RegisterID == parentId && a.StatusID!=-1)
                select new RegistersListItem
                {
                    ID = finreg.ID,
                    RegisterID = finreg.RegisterID ?? 0,
                    RegisterKind = finreg.Register.Kind,
                    DraftSum = finreg.AsgFinTrs.Sum(b => b.DraftAmount ?? 0),
                    Count = finreg.Count ?? 0,
                    Status = finreg.Status,
                    TrancheDate = finreg.Register.TrancheDate
                };
            var ids = q.ToList().Where(ViewModelHelperNpf.IsFinregisterStatusReq).Select(a => a.ID).ToList();
            SpnFinregisters.Where(r => ids.Contains(r.ID)).ForEach(fr => fr.Status = RegisterIdentifier.FinregisterStatuses.Issued);
            SaveChanges();
        }

        public IQueryable GetTempAllocationList()
        {
            long fakeId = 0;
                var xx = from register in SpnRegisters
                from finreg in SpnFinregisters.Where(a => a.RegisterID == register.ID && a.StatusID!=-1).DefaultIfEmpty(null)
                where register.StatusID > 0 &&
                      (register.Kind.Equals(RegisterIdentifier.TempAllocation, StringComparison.OrdinalIgnoreCase) ||
                       register.Kind.Equals(RegisterIdentifier.TempReturn, StringComparison.OrdinalIgnoreCase))
                      && (finreg == null || (finreg.AccountDebit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ||
                                                                         finreg.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase)))
                select new RegistersListItem
                {
                    RegisterID = register.ID,
                    ID = finreg == null ? register.ID : finreg.ID,
                    ContragentID = finreg == null ? 0 : (finreg.AccountCredit != null && finreg.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ? finreg.AccountCredit.Contragent.LegalEntity.ID : finreg.AccountDebit.Contragent.LegalEntity.ID),
                    RegisterKind = register.Kind,
                    Content = register.Content,
                    ContragentName =  finreg.AccountCredit != null && finreg.AccountCredit.Contragent.TypeName.Equals("НПФ", StringComparison.OrdinalIgnoreCase) ? finreg.AccountCredit.Contragent.LegalEntity.FormalizedName : finreg.AccountDebit.Contragent.LegalEntity.FormalizedName,
                    Count = finreg == null ? 0 : finreg.Count ?? 0,
                    ZLCount = finreg == null ? 0 : finreg.ZLCount,
                    ApproveDocName = register.ApproveDoc.Name,
                    FinregRegNum = finreg == null ? "" : finreg.RegNum,
                    FinregDate = finreg.Date == DateTime.MinValue ? null : finreg.Date,
                    DraftSum = finreg.AsgFinTrs.Sum(a => a.DraftAmount ?? 0),
                    CreditAccID = finreg == null ? 0 : finreg.CrAccID,
                    DebitAccID = finreg == null ? 0 : finreg.DbtAccID,
                    TrancheDate = register.TrancheDate,
                    Status = finreg.Status,
                    RegNum = register.RegNum,
                    RegDate = register.RegDate == DateTime.MinValue ? null : register.RegDate,
                    IsActiveNPF = finreg != null && (finreg.AccountCredit != null ? finreg.AccountCredit.Contragent.StatusID : finreg.AccountDebit.Contragent.StatusID) == 1,
                    CFR = finreg.CFRCount,
                    ParentFinregisterID = finreg == null ? 0 : finreg.ParentFinregisterID,
                    Portfolio =  register.Portfolio.Year
                };
           // var cc = Contragents.ToList();
           // var re = xx.ToList();
            return xx;
        }

        public List<FinregisterWebListItem> GetSPNAllocatedFinregistersList(long portfolioID, string content = null)
        {
            return SpnFinregisters.Where(fr => fr.StatusID != -1 && fr.Register.Kind == RegisterIdentifier.TempAllocation && fr.Register.PortfolioID == portfolioID
                                               && (content == null || fr.Register.Content == content || fr.Register.Content == RegisterIdentifier.TempContent)).ToList()
                .Select(fr => new FinregisterWebListItem(fr, fr.Register, false, fr.AccountCredit.Name, fr.AccountDebit.Name, fr.AccountCredit.Contragent.Status.Name,
                    fr.AccountDebit.Contragent.Status.Name))
                .ToList();

           /* var xx =  from fr in SpnFinregisters
                where fr.StatusID != -1 && fr.Register.Kind == RegisterIdentifier.TempAllocation && fr.Register.PortfolioID == portfolioID
                      && (content == null || fr.Register.Content == content || fr.Register.Content == RegisterIdentifier.TempContent)
                select new FinregisterWebListItem(fr, fr.Register, false, fr.AccountCredit.Name, fr.AccountDebit.Name, fr.AccountCredit.Contragent.Status.Name, fr.AccountDebit.Contragent.Status.Name);
            return xx.ToList();*/
        }

        public List<FinregisterWebListItem> GetSPNReturnedFinregistersList(long portfolioID, string content = null)
        {
            /*using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"SELECT new FinregisterListItem(fr, rr, acc.Name, adc.Name, ccs.Name, dcs.Name)  FROM 
												Finregister fr												
												JOIN fr.Register mr
												JOIN mr.Return rr
												join fr.CreditAccount ac
												join fr.DebitAccount ad
												join ac.Contragent acc
												join acc.Status ccs
												join ad.Contragent adc
												join adc.Status dcs
												WHERE fr.StatusID <> -1 and (rr.Kind = :kind and rr.Portfolio = :portfolio and (:content is null OR rr.Content = :content OR rr.Content = :oldcontent))");
                query.SetParameter("kind", RegisterIdentifier.TempReturn);
                query.SetParameter("portfolio", portfolioID);
                query.SetParameter("content", content);
                query.SetParameter("oldcontent", RegisterIdentifier.TempContent);
                var res = query.List().Cast<FinregisterListItem>().ToList();
                return res;
            }*/

            return SpnFinregisters.Where(fr => fr.StatusID != -1 && (fr.Register.Return.Kind == RegisterIdentifier.TempReturn && fr.Register.Return.PortfolioID == portfolioID
                                                                                                                              && (content == null ||
                                                                                                                                  fr.Register.Return.Content == content ||
                                                                                                                                  fr.Register.Return.Content ==
                                                                                                                                  RegisterIdentifier.TempContent)))
                .ToList().Select(fr => new FinregisterWebListItem(fr, fr.Register, false, fr.AccountCredit.Name, fr.AccountDebit.Name, fr.AccountCredit.Contragent.Status.Name,
                    fr.AccountDebit.Contragent.Status.Name))
                .ToList();

            /*var xx =  from fr in SpnFinregisters
                where fr.StatusID != -1 && (fr.Register.Return.Kind == RegisterIdentifier.TempReturn && fr.Register.Return.PortfolioID == portfolioID
                      && (content == null || fr.Register.Return.Content == content || fr.Register.Return.Content == RegisterIdentifier.TempContent))
                select new FinregisterWebListItem(fr, fr.Register, false, fr.AccountCredit.Name, fr.AccountDebit.Name, fr.AccountCredit.Contragent.Status.Name, fr.AccountDebit.Contragent.Status.Name);
            return xx.ToList();*/
        }

        public IQueryable GetUnlinkedPPForNpf(AsgFinTr.Directions direction)
        {
            var xx = from pp in AsgFinTrs
                where pp.LinkElID == (long) AsgFinTr.Links.NoLink && pp.ReqTransferID == null && pp.FinregisterID == null &&
                      pp.DirectionElID == (long) direction && (pp.Portfolio == null || pp.Portfolio.StatusID > -1 && pp.Portfolio.StatusID < 2) &&
                      pp.StatusID != -1 && pp.ContragentTypeID != (long) AsgFinTr.ContragentTypeEnum.OPFR
                select pp;
            var dd = xx.ToList();
            return xx;
        }


        private long? GetLegalEntityIdFromFinregister(long finregisterID)
        {
            var fr = SpnFinregisters.FirstOrDefault(a => a.ID == finregisterID);
            return fr?.Register.Kind == RegisterIdentifier.NPFtoPFR ? fr?.AccountCredit?.Contragent?.LegalEntity?.ID : fr?.AccountDebit?.Contragent?.LegalEntity?.ID;
        }

        public void SaveCommonPP(AsgFinTrWeb pp)
        {
            try
            {
                using (var tr = Database.BeginTransaction())
                {
                    decimal? sum = 0;
                    if (pp.ReqTransferID.HasValue)
                    {
                        var comPP = AsgFinTrs.Where(a => a.ReqTransferID == pp.ReqTransferID.Value).ToList();
                        sum = comPP.Where(a => a.ID != pp.ID).Sum(p => p.DraftAmount);
                        sum += pp.DraftAmount;
                    }

                    if (pp.FinregisterID.HasValue)
                        pp.InnLegalEntityID = GetLegalEntityIdFromFinregister(pp.FinregisterID.Value);
                        
                    else if (pp.ReqTransferID.HasValue)
                    {
                        //TODO
 /*                       var result = ;
                        pp.InnLegalEntityID = result[0];
                        pp.InnContractID = result[1];*/
                    }
                    else
                    {
                        pp.InnLegalEntityID = null;
                        pp.InnContractID = null;
                    }

                    if (pp.ReqTransferID.HasValue)
                    {
                        pp.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.UK;
                        //TODO
                    }

                    switch (pp.SectionElID)
                    {
                        case (long) AsgFinTr.Sections.NPF:
                            pp.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.NPF;
                            break;
                        case (long) AsgFinTr.Sections.SI:
                        case (long) AsgFinTr.Sections.VR:
                            pp.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.UK;
                            break;
                        case (long) AsgFinTr.Sections.BackOffice:
                            pp.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.OPFR;
                            break;
                    }

                    SaveChanges();
                    tr.Commit();
                }
            }
            catch (Exception ex)
            {
                Logger.LogEx(ex, nameof(SaveCommonPP));
                throw;
            }
        }


        public List<FinregisterWithCorrAndNPFWebListItem> GetFinregisterWithCorrAndNPFList(long registerID)
        {
                var result = new List<FinregisterWithCorrAndNPFWebListItem>();
                var reg = SpnRegisters.FirstOrDefault(a=> a.ID == registerID);

                if (reg?.Kind != null)
                {
                    return SpnFinregisters.Where(a => a.RegisterID == registerID && a.StatusID == 1)
                        .Select(fr =>
                            new FinregisterWithCorrAndNPFWebListItem
                            {
                                ID = fr.ID,
                                Finregister = fr,
                                NPFName = reg.Kind.Trim().Equals(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase)//RegisterIdentifier.IsFromNPF(reg.Kind.ToLower())
                                    ? fr.AccountCredit.Contragent.Name
                                    : fr.AccountDebit.Contragent.Name,
                                NPFStatus =  reg.Kind.Trim().Equals(RegisterIdentifier.NPFtoPFR, StringComparison.OrdinalIgnoreCase)//RegisterIdentifier.IsFromNPF(reg.Kind.ToLower())
                                    ? fr.AccountCredit.Contragent.Status.Name
                                    : fr.AccountDebit.Contragent.Status.Name,
                                Count = fr.Count ?? 0,
                                CorrCount = null,
                                CFRCount = fr.CFRCount,
                                ZLCount = fr.ZLCount
                            }
                        ).ToList();
                }

                return result;
        }

        /*
            var xx = from d in Documents.Where(d=> d.StatusID != -1 && d.TypeID == docTypeId && (documentID == null || documentID == d.ID)
                                                   && ((stat == 2 && d.ExecutionDate == null) || (stat == 3 && d.ExecutionDate != null) || (stat == 4 && d.ExecutionDate == null && d.ControlDate != null) ||
                                                       stat == 1))
                join a in Attaches on d.ID equals a.DocumentID into docAtt
                from att in docAtt.DefaultIfEmpty(null)*/

        public IQueryable GetNpfDocumentsListHib(DocumentWeb.Statuses status, long? documentID = null, long? attachID = null)
        {
            var docTypeId = (int) DocumentWeb.Types.Npf;
            var stat = (int) status;

            var xx = (from d in Documents.Where(d=> d.StatusID != -1 && d.TypeID == docTypeId && (documentID == null || documentID == d.ID)
                                                   && ((stat == 2 && d.ExecutionDate == null) || (stat == 3 && d.ExecutionDate != null) || (stat == 4 && d.ExecutionDate == null && d.ControlDate != null) ||
                                                       stat == 1))
                select new CorrespondenceListItemWeb
                {
                    ID = d.ID,
                    IncomingDate = d.IncomingDate,
                    IncomingNumber = d.IncomingNumber == null ? null : d.IncomingNumber.Trim(),
                    OutgoingDate = d.OutgoingDate,
                    OutgoingNumber = d.OutgoingNumber == null ? null : d.OutgoingNumber.Trim(),
                    Sender = d.LegalEntity == null ? null : d.LegalEntity.FormalizedName,
                    DocumentClass = d.DocumentClass == null ? null : d.DocumentClass.Name,
                    Executor = d.TypeID == 2 || d.TypeID == 3 ? d.ExecutorName : (d.Executor != null ? d.Executor.LastName : CorrespondenceComments.ExecutorNotAssigned ),
                    //AdditionalDocumentInfo = att != null ? att.Additional : ( d.AdditionalInfo != null ?d.AdditionalInfo.Name : d.AdditionalInfoText),
                    ControlDate = d.ControlDate,
                    ExecutionDate = d.ExecutionDate,
                    IsAttach = false,               
                }).Concat(
                from d in Documents.Where(d=> d.StatusID != -1 && d.TypeID == docTypeId && (documentID == null || documentID == d.ID)
                                              && ((stat == 2 && d.ExecutionDate == null) || (stat == 3 && d.ExecutionDate != null) || (stat == 4 && d.ExecutionDate == null && d.ControlDate != null) ||
                                                  stat == 1)).DefaultIfEmpty(null)
                join att in Attaches on d.ID equals att.DocumentID
                select new CorrespondenceListItemWeb
                {
                    ID = att.ID,
                    IncomingDate = d.IncomingDate,
                    IncomingNumber = d.IncomingNumber == null ? null : d.IncomingNumber.Trim(),
                    OutgoingDate = d.OutgoingDate,
                    OutgoingNumber = d.OutgoingNumber == null ? null : d.OutgoingNumber.Trim(),
                    Sender = d.LegalEntity == null ? null : d.LegalEntity.FormalizedName,
                    DocumentClass = att.AttachClassific == null ? null : (att.AttachClassific.Name+" ("+CorrespondenceComments.Attachment+")"),
                    Executor = att.ExecutorName ?? CorrespondenceComments.ExecutorNotAssigned,
                    //AdditionalDocumentInfo = att != null ? att.Additional : ( d.AdditionalInfo != null ?d.AdditionalInfo.Name : d.AdditionalInfoText),
                    ControlDate = att.ControlDate,
                    ExecutionDate = att.ExecutionDate,
                    IsAttach = true,               
                });

            var x = xx.ToList();
            return xx;
        }

        public List<LegalEntityWeb> GetActiveNpfList()
        {
            var xx = from le in LegalEntities
                where le.Contragent.TypeName == "НПФ" && (le.Contragent.StatusID == 1 || le.Contragent.StatusID == 2 && (le.CloseDate == null || le.CloseDate > DateTime.Today))
                orderby le.Contragent.Name
                     select le;
            return xx.Include(t => t.Contragent).Include(t => t.Contragent.Status).ToList();
        }

        public List<LegalEntityWeb> GetNpfList(bool isActive, long garant = 0, string status = null)
        {
            var xx = from le in LegalEntities
                where le.Contragent.TypeName == "НПФ" && (le.CloseDate == null || le.CloseDate > DateTime.Today)
                select le;
            if (isActive)
                xx = xx.Where(le => le.Contragent.StatusID == 1 || le.Contragent.StatusID == 2);
            if (garant > 0)
                xx = xx.Where(le => le.GarantACBID == garant);
            if (!string.IsNullOrEmpty(status))
                xx = xx.Where(le => le.Contragent.Status.Name == status);
            return xx.Include(t => t.Contragent).Include(t => t.Contragent.Status).OrderBy(t => t.Contragent.Name).ToList();
        }

        public List<AttachWeb> GetLinkedDocumentsList(long documentID)
        {
            var xx = from at in Attaches
                where at.StatusID != -1 && at.DocumentID == documentID
                select at;
            return xx.ToList();
        }

        public List<DocumentWeb> GetDocumentsBypropertiesList(DocumentWeb.Types type, string incomingNumber, DateTime? incomingDate)
        {
            if (incomingDate == null)
                return Documents.Where(a=> a.IncomingNumber.Equals(incomingNumber, StringComparison.OrdinalIgnoreCase) && a.TypeID == (long)type && a.StatusID != -1).ToList();
            return Documents.Where(a=> a.IncomingNumber.Equals(incomingNumber, StringComparison.OrdinalIgnoreCase) && a.TypeID == (long)type && a.StatusID != -1 &&
                                       (a.IncomingDate != null && a.IncomingDate.Value.Year == incomingDate.Value.Year)).ToList();
        }

        public bool SaveAndCleanDocument(DocumentWeb document, bool deleteBody)
        {
            try
            {
                if (deleteBody && document.DocFileBody != null)
                {
                    DbHelper.DeleteEntity(document.DocFileBody);
                   // Database.ExecuteSqlCommand($"delete from pfr_basic.file_content where id={document.DocFileBody.FileContentId}");
                   // document.DocFileBody = null;
                }
                DbHelper.SaveEntity(document);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogEx(ex);
                return false;
            }
        }

        public IList<NameValueListItem> GetActiveLegalEntityByType(ContragentWeb.Types type)
        {
            var sType = string.Empty;
            switch (type)
            {
                case ContragentWeb.Types.Bank:
                    sType = "Банк";
                    break;
                case ContragentWeb.Types.BankAgent:
                    sType = "Банк-агент";
                    break;
                case ContragentWeb.Types.GUK:
                    sType = "ГУК";
                    break;
                case ContragentWeb.Types.Npf:
                    sType = "НПФ";
                    break;
                case ContragentWeb.Types.PFR:
                    sType = "ПФР";
                    break;
                case ContragentWeb.Types.SD:
                    sType = "СД";
                    break;
                case ContragentWeb.Types.UK:
                    sType = "УК";
                    break;
            }

            var delStatus = StatusIdentifier.S_DELETED.ToLower();

            return LegalEntities.Include(t => t.Contragent).Include(t => t.Contragent.Status)
                .Where(a => a.Contragent.TypeName == sType && a.Contragent.Status.Name != delStatus)
                .OrderBy(a => a.Contragent.Name)
                .Select(a => new NameValueListItem
                {
                    ID = a.ID,
                    Name = a.FormalizedName,
                    INN = a.INN
                }).ToList();
        }

        public long SaveDocFileBodyForDocument(long id, DocFileBodyWeb filebody, byte[] blob)
        {
            var list = DocFileBodies.Where(a => a.DocumentID == id).ToList();
            foreach (var bodyWeb in list)
            {
                DbHelper.DeleteEntity(bodyWeb);
                if(bodyWeb.FileContentId.HasValue)
                    Database.ExecuteSqlCommand($"delete from pfr_basic.file_content where id={bodyWeb.FileContentId.Value}");
            }

            filebody.FileContentId = SaveFileContent(blob);
            SaveChanges();
            return filebody.ID;
        }

        private long SaveFileContent(byte[] data)
        {
            var fileContent = new FileContentWeb {FileContentData = data};
            DbHelper.SaveEntity(fileContent);
            return fileContent.ID;
        }

        
        /// <summary>
        /// Сохранение финреестров для реестра Возврата СПН
        /// </summary>
        /// <param name="finregisters"></param>
        /// <returns></returns>
        public List<long> SaveReturnFinregisters(IEnumerable<FinregisterWithCorrAndNPFWebListItem> finregisters)
        {
            var frs = new List<FinregisterWeb>();
            finregisters.ForEach(a =>
            {
                if (a.IsNewItem)
                {
                    var fr = new FinregisterWeb();
                    a.Finregister.CopyTo(fr);
                    SpnFinregisters.Add(fr);
                    frs.Add(fr);
                }
                else frs.Add(a.Finregister);
            });
            SaveChanges();
            return frs.Select(a=> a.ID).ToList();
        }

        internal IQueryable GetRejectApplicationList()
        {
            return RejectApplications.AsNoTracking();
        }
    }
}