﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
	public static class ListProvider
	{
	    private static HttpSessionState Session => HttpContext.Current.Session;

	    public static IList<ERZLListItem> GetZlRedistList()
	    {
	        return LoadData(() => {
	            var model = new ZLRedistListViewModel();
	            return model.ZLRedistList;
	        });
        }

	    public static IList<BankAccount> GetNPFAccounts()
		{
			return LoadData(() => {
			        var model = new BankAccountsListViewModel();
			        return model.BankAccountsList;
			    });
		}

	    public static IList<NPFListItem> GetNPFList()
	    {
	        return LoadData(() => new NPFListViewModel().WaitForList());
	    }


        private static IList<T> LoadData<T>(Func<IList<T>> action)
		{
			try
			{
			    return action();
			}
			catch(Exception ex)
			{
                Logger.LogEx(ex, $"{nameof(LoadData)} - {typeof(T).Name}");
				return new List<T>();
			}
		}

	    public static IList<NPFErrorCode> GetErrorCodesList()
	    {
	        return LoadData(() => {
	            var model = new NPFCodeListViewModel(true);
                model.ExecuteRefreshListExternal();
	            return model.List;
	        });
        }

	    public static IList<NPFErrorCode> GetDockCodesList()
	    {
	        return LoadData(() => {
	            var model = new NPFCodeListViewModel(false);
	            model.ExecuteRefreshListExternal();
	            return model.List;
	        });
	    }

	    public static IList<SIListItem> GetSiList()
	    {
	        var model = new SIListViewModel(false);
	        model.ExecuteRefreshListExternal();
	        return model.SIList;
        }

	    public static IList<SIListItem> GetSiContactList()
	    {
	        var model = new SIListViewModel(true);
	        model.ExecuteRefreshListExternal();
	        return model.SIList;
	    }

        public static IEnumerable<SIContractListItem> GetSiContractList()
	    {
	        var model = new SIContractsListViewModel();
            model.ExecuteRefreshListExternal();
	        return model.SIContractsList;
	    }

	    public static IEnumerable<SIContractListItem> GetVrContractList()
	    {
	        var model = new VRContractsListViewModel();
	        model.ExecuteRefreshListExternal();
	        return model.SIContractsList;
        }

	    public static IEnumerable<CategoryF140> GetSiViolationList()
	    {
	        var model = new ViolationCategoriesListViewModel();
            model.ExecuteRefreshListExternal();
	        return model.CategoryF140List;
	    }

	    public static IEnumerable<DeadZLListItem> GetNpfDeadZlList()
	    {
	        var model = new DeadZLListViewModel();
	        model.ExecuteRefreshListExternal();
	        return model.DeadZLList;
	    }

	    public static object Test()
	    {
	       return (object)BusinessLogic.WCFClient.CreateLightClient("1", "1").Client.WebLazyGetSpnRegistersArchiveList();
	    }
	}
}