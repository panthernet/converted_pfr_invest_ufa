﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebSIListViewModel : WebListViewModel<SIListItem>
    {
        public WebSIListViewModel() : base(ListProvider.GetSiList())
        {
            AjaxMain = "~/Views/Menu/Dictionary/SI/_SiListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/Dictionary/SI/_SiListContainerViewPartial.cshtml";
            AjaxController = "Dictionary";
        }
    }
}