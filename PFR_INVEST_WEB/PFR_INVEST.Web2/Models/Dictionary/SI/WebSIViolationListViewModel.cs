﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.Models
{
    public class WebSIViolationListViewModel : WebListViewModel<CategoryF140>
    {
        public WebSIViolationListViewModel() : base(ListProvider.GetSiViolationList())
        {
            AjaxMain = "~/Views/Menu/Dictionary/SI/_SiViolationListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/Dictionary/SI/_SiViolationListContainerViewPartial.cshtml";
            AjaxController = "Dictionary";
        }
    }
}