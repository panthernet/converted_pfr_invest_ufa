﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(Branch), JournalDescription = "Отделение НПФ")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class WebNpfBranchViewModel : PureWebFormViewModel
    {

        public long? LegalEntityID {
            get { return Branch.LegalEntityID; }
            set { Branch.LegalEntityID = value; }
        }

        public WebNpfBranchViewModel() : this(0)
        {            
        }

        public WebNpfBranchViewModel(long id, long leId = 0) : 
            base(id)
        {
            //связь со вьюхой
            WebModelHelperEx.InitializeDefault(this, id);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfBranchViewPartial";
            LegalEntityID = leId;
            Branch = DataContainerFacade.GetByID<Branch>(id) ?? new Branch();
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            return string.IsNullOrEmpty(Validate());
        }

        #region Fields

        [JsonIgnore]
        protected readonly Branch Branch;

        [Display(Name = "Наименование")]
        [Required]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchName
        {
            get 
            { 
                return Branch?.Name;
            }
            set 
            { 
                if (Branch != null)
                    Branch.Name = value;
            }
        }

        [Display(Name = "Фактический адрес")]
        [Required]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchAddress
        {
            get { return Branch?.Address; }
            set 
            { 
                if (Branch != null)
                    Branch.Address = value;
            }
        }

        [Display(Name = "Почтовый адрес")]
        [StringLength(512, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchPosAddress
        {
            get 
            {
                return Branch?.PostAddress;
            }
            set 
            { 
                if (Branch != null)
                    Branch.PostAddress = value;
            }
        }

        [Display(Name = "Телефон")]
        [StringLength(20, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string BranchPhone
        {
            get 
            {
                return Branch?.Phone;
            }
            set 
            { 
                if (Branch != null)
                    Branch.Phone = value;
            }
        }
        #endregion

        #region Execute Implementations


        public override bool WebExecuteDelete(long id)
        {
            DataContainerFacade.Delete(Branch);
            return true;
        }

        public override bool WebExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;

                case ViewModelState.Create:
                case ViewModelState.Edit:
                    Branch.ID = ID = DataContainerFacade.Save(Branch);
                    break;
            }

            return true;
        }

        #endregion

        private const string NOT_NULL_FIELDS = "branchname|branchaddress|branchphone";
        private const string S_ERROR = "Неверный формат данных";

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                if (Branch == null)
                    return S_ERROR;

                switch (columnName.ToLower())
                {
                    case "branchname":
                        return !string.IsNullOrWhiteSpace(Branch.Name) ? Branch.Name.ValidateMaxLength(512) : S_ERROR;
                    case "branchaddress":
                        return !string.IsNullOrWhiteSpace(Branch.Address) ? Branch.Address.ValidateMaxLength(512) : S_ERROR;
                    case "branchphone":
                        return string.IsNullOrWhiteSpace(Branch.Phone) || (!string.IsNullOrEmpty(Branch.Phone) && Branch.Phone.Length <= 20) ? null : "Длина строки больше 20 символов";
                    case "branchposaddress":
                        return Branch.PostAddress.ValidateMaxLength(512);
                    default:
                        return null;
                }
            }
        }

        public override string Validate()
        {
            if (Branch == null)
                return S_ERROR;

            return NOT_NULL_FIELDS.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? S_ERROR : null;
        }

    }
}