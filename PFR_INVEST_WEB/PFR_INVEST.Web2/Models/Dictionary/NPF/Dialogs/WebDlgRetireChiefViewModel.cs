﻿using System;
using System.ComponentModel.DataAnnotations;
using PFR_INVEST.Web2.BusinessLogic;
using WebViewModel = PFR_INVEST.Web2.BusinessLogic.Models.WebViewModel;

namespace PFR_INVEST.Web2.Models
{
    public class WebDlgRetireChiefViewModel : WebViewModel
    {
        [Display(Name = "Укажите дату прекращения полномочий")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime DateRetire { get; set; } = DateTime.Today;

        public WebDlgRetireChiefViewModel()
        {
            //связь со вьюхой
            AjaxMain = "_SharedAjaxFormSimplePartialView";
            AjaxController = "Dictionary";
            AjaxAction = "DlgRetireChief";
            AjaxPartial = "_DlgRetireChiefViewPartial";
            FormId = "dlgRetireChief";
            WebTitle = FormDataHelper.Data[FormId].DisplayName;
        }

        public override long WebGetEntryId()
        {
            return 0;
        }
    }
}