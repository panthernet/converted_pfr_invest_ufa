﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator,DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [ModelEntity(Type = typeof(NPFErrorCode), JournalDescription = "Код ошибки")]
    public class WebNpfDockCodeViewModel : PureWebFormViewModel
    {
        public WebNpfDockCodeViewModel()
            :this(0)
        {       
        }

        public WebNpfDockCodeViewModel(long id): base(id)
        {
            WebModelHelperEx.InitializeDefault(this,id);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfCreateDockCodeViewPartial";

            NPFCode = State == ViewModelState.Create ? new NPFErrorCode { IsErrorCode = 0 } : DataContainerFacade.GetByID<NPFErrorCode>(id);
        }

        #region OLD
         #region Fields

        [JsonIgnore]
        public NPFErrorCode NPFCode { get; }

        [Display(Name = "Код")]
        [Required]
        [StringLength(10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Code
        {
            get { return NPFCode.Code; }
            set
            {
                if (NPFCode.Code == value) return;
                NPFCode.Code = value;
            }
        }

        [Display(Name = "Обоснование")]
        [Required]
        [StringLength(256, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Description
        {
            get { return NPFCode.Description; }
            set
            {
                if (NPFCode.Description == value) return;
                NPFCode.Description = value;
            }
        }

        [Display(Name = "Комментарий")]
        [StringLength(1024, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Comment
        {
            get { return NPFCode.Comment; }
            set
            {
                if (NPFCode.Comment == value) return;
                NPFCode.Comment = value;
            }
        }

        [JsonIgnore]
        public string CodeLabel => NPFCode.IsErrorCode == 1 ? "Код ошибки*:" : "Код стыковки с договором*:";

        #endregion

        #region Execute Implementations

        protected override bool WebBeforeExecuteDeleteCheck()
        {
            var isError = NPFCode.IsErrorCode == 1;
            if (DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal(isError ? "ErrorCode" : "ContractDockCode", NPFCode.Code)
            }) <= 0) return true;
            WebLastModelError = $"Выбранный {(isError ? "код ошибки" : " код стыковки с договором")} удалить нельзя, т.к. он используется в системе!";
            return false;
        }

        public override bool WebExecuteDelete(long id)
        {
            DataContainerFacade.Delete(NPFCode);
            return true;
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            NPFCode.ID = ID;
            var existList = DataContainerFacade.GetListByPropertyConditions<NPFErrorCode>(
                new List<ListPropertyCondition>
                {
                    ListPropertyCondition.Equal("Code", NPFCode.Code),
                    ListPropertyCondition.Equal("IsErrorCode", NPFCode.IsErrorCode)
                }).Where(x => x.ID != NPFCode.ID).ToList();
            if (!existList.Any()) return true;
            var error = existList.First().StatusID == -1
                ? "Указанный код ранее уже был добавлен в систему, но сейчас находится в статусе \"Удален\"."
                : "Указанный код уже используется в системе.";
            WebLastModelError = error;
            return false;
        }

        public override bool WebExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    NPFCode.Code = Code.Trim();
                    DataContainerFacade.Save(NPFCode);
                    break;
                case ViewModelState.Create:
                    NPFCode.Code = Code.Trim();
                    ID = NPFCode.ID = DataContainerFacade.Save(NPFCode);
                    break;
            }

            return true;
        }
#endregion


        public override string Validate()
        {
            const string fields = "code|description|comment";
            foreach (var e in fields.ToLower().Split('|').Select(key => this[key]).Where(e => !string.IsNullOrEmpty(e)))
            {
                return e;
            }
            return string.Empty;
        }

#region Validation

        [JsonIgnore]
        public override string this[string key]
        {
            get
            {
                try
                {
                    switch (key.ToLower())
                    {
						case "code": return NPFCode.Code.TrimmedIsNullOrEmpty() ? DataTools.DATAERROR_NEED_DATA : NPFCode.Code.ValidateMaxLength(10);
                        case "description":
							return NPFCode.Description.TrimmedIsNullOrEmpty()
									   ? DataTools.DATAERROR_NEED_DATA
									   : NPFCode.Description.ValidateMaxLength(256);
                        case "comment":
							return NPFCode.Comment?.ValidateMaxLength(1024);


                        default:
                            return null;
                    }
                }
                catch
                {
                    return DataTools.DATAERROR_NEED_DATA;
                }
            }
        }

#endregion

        [JsonIgnore]
        public bool IsEdit => State == ViewModelState.Edit;
        #endregion
    }
}