﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(Reorganization), JournalDescription = "Реорганизация")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfReorgViewModel : PureWebFormViewModel
    {
        /// <summary>
        /// Сохраняем для подгрузки списка
        /// </summary>
        public long LegalEntityID { get; set; }

        [DisplayName("Вид реорганизации")]
        [Required]
        public long ReorganizationTypeID
        {
            get { return _reorganization.ReorganizationTypeID; }
            set { _reorganization.ReorganizationTypeID = value; }
        }

        public long SourceContragentID
        {
            get { return _reorganization.SourceContragentID; }
            set { _reorganization.SourceContragentID = value; }
        }

        [DisplayName("Правопреемник")]
        [Required]
        public long ReceiverContragentID
        {
            get { return _reorganization.ReceiverContragentID; }
            set { _reorganization.ReceiverContragentID = value; }
        }

        public string ReceiverName
        {
            get { return _reorganization.ReceiverName; }
            set { _reorganization.ReceiverName = value; }
        }

        public string SourceName
        {
            get { return _reorganization.SourceName; }
            set { _reorganization.SourceName = value; }
        }


        public WebNpfReorgViewModel(): this(0) { }

        public WebNpfReorgViewModel(long leId)
            :base(leId == 0 ? 0 : (DataContainerFacade.GetByID<LegalEntity>(leId).ContragentID ?? 0))
        {
            LegalEntityID = leId;
            //связь со вьюхой
            WebModelHelperEx.InitializeDefault(this);
            WebModelHelperEx.UpdateModelState(this);
            AjaxController = "Dictionary";
            AjaxPartial = "_NpfReorgViewPartial";


            if (leId > 0) //web
                UpdateContragents(leId);

            ReorganizationTypeList = DataContainerFacade.GetList<ReorganizationType>();

            if (State == ViewModelState.Create)
            {
                _reorganization = new Reorganization { SourceContragentID = ID, OrderDate = null, ReorganizationDate = DateTime.Today };
                SelectedContragent = ContragentList.FirstOrDefault();
                SelectedReorganizationType = ReorganizationTypeList.FirstOrDefault();
            }
            else
            {
                _reorganization = DataContainerFacade.GetByID<Reorganization>(ID);
                SelectedContragent = ContragentList.FirstOrDefault(c => c.ID == _reorganization.ReceiverContragentID);
                SelectedReorganizationType = ReorganizationTypeList.FirstOrDefault(rt => rt.ID == _reorganization.ReorganizationTypeID);
            }
        }

        public override void WebPostModelCreateAction()
        {
            if(_le == null && LegalEntityID > 0)
                UpdateContragents(LegalEntityID);
        }





        [JsonIgnore]
        protected readonly Reorganization _reorganization;
        [JsonIgnore]
        protected  LegalEntity _le;

        [DisplayName("Номер приказа ФСФР")]
        [StringLength(50, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        [Required]
        public string RegNum
        {
            get {
                return _reorganization.RegNum?.Trim() ?? _reorganization.RegNum;
            }
            set
            {
                _reorganization.RegNum = value;
            }
        }

        [DisplayName("Дата реорганизации")]
        [Required]
        public DateTime ReorganizationDate
        {
            get
            {
                return _reorganization.ReorganizationDate;
            }
            set
            {
                _reorganization.ReorganizationDate = value;
            }
        }

        [DisplayName("Дата приказа ФСФР")]
        [Required]
        public DateTime? OrderDate
        {
            get
            {
                return _reorganization.OrderDate;
            }
            set
            {
                _reorganization.OrderDate = value;
            }
        }

        [JsonIgnore]
        public List<ReorganizationType> ReorganizationTypeList { get; set; }

        private ReorganizationType m_SelectedReorganizationType;
        public ReorganizationType SelectedReorganizationType
        {
            get
            {
                return m_SelectedReorganizationType;
            }
            set
            {
                if (value != null)
                {
                    m_SelectedReorganizationType = value;
                    _reorganization.ReorganizationTypeID = value.ID;
                }
            }
        }

        [JsonIgnore]
        public IList<Contragent> ContragentList { get; set; } = new List<Contragent>();

        private Contragent m_SelectedContragent;
        public Contragent SelectedContragent
        {
            get
            {
                return m_SelectedContragent;
            }
            set
            {
                if (value == null) return;
                m_SelectedContragent = value;
                _reorganization.ReceiverContragentID = value.ID;
            }
        }
        
        public override bool WebExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save(_reorganization);
                    _reorganization.ID = ID;
                    break;
            }

            return true;
        }

        public override bool WebExecuteDelete(long id)
        {
            DataContainerFacade.Delete(_reorganization);
            return true;
        }


        protected void UpdateContragents(long leID)
        {
			StatusFilter sf = StatusFilter.NotDeletedFilter;
            long[] contragentStatusIdsToIgnore = { 2, 3, 4, 5, 6, -1, 2 };
            ContragentList =WCFClient.GetDataServiceFunc().GetFilteredNPFContragentListHib(sf, contragentStatusIdsToIgnore, leID);
            _le = DataContainerFacade.GetByID<LegalEntity>(leID);
        }

        private const string FieldRequiredErrorMessage = "Заполнены не все обязательные поля";

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "RegNum":
                        return string.IsNullOrWhiteSpace(RegNum) ? FieldRequiredErrorMessage : null;
                    case "OrderDate":
                        if (OrderDate == null)
                            return FieldRequiredErrorMessage;
                        if (OrderDate >= ReorganizationDate)
                            return "Дата приказа ФСФР должна быть меньше даты реорганизации";
                        return null;
                    case "ReorganizationDate":
                        if (ReorganizationDate.Date > DateTime.Today)
                            return "Дата реорганизации не может быть больше сегодняшнего дня";
                        if (ReorganizationDate.Date <= OrderDate)
                            return "Дата реорганизации должна быть больше даты приказа ФСФР";
                        return null;
                    default:
                        return null;
                }
            }
        }

        public override string Validate()
        {
            return SelectedContragent != null && 
                   OrderDate != null &&
                   SelectedReorganizationType != null &&
                   !string.IsNullOrWhiteSpace(RegNum) &&
                   OrderDate != null &&
                   OrderDate < ReorganizationDate &&
                   ReorganizationDate.Date <= DateTime.Today ? null : "Обязательное поле не задано!";
        }
    }
}