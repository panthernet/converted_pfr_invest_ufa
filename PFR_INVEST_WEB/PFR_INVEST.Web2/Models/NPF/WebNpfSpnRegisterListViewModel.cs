﻿using System.Linq;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfSpnRegisterListViewModel : WebNpfLazyListViewModel<RegistersListItem>
    {
        private IQueryable _query;

        public override IQueryable Query => _query ?? (_query = WCFClient.GetEFServiceFunc()?.GetSpnList());

        public WebNpfSpnRegisterListViewModel()
        {
            AjaxMain = "_SpnRegisterListViewPartial";
            AjaxPartial = "_SpnRegisterListContainerViewPartial";
            AttachDefaultGridScripts = false;
        }
    }
}