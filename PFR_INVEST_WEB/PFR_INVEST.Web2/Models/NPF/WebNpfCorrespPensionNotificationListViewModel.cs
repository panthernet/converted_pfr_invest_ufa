﻿using System.Data.Entity;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(PensionNotificationWeb), JournalDescription = "Уведомление НЧТП (Корреспонденция)")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfCorrespPensionNotificationListViewModel : WebNpfLazyListViewModel<PensionNotificationWeb>
    {
        public WebNpfCorrespPensionNotificationListViewModel() : base("Npf")
        {
            Query = WCFClient.GetEFServiceFunc()?.PensionNotifications.Where(a=>a.StatusID == 1).Include(a=> a.Sender);
            AjaxMain = "_CorrespondencePensionNotificationListViewPartial";
            AjaxPartial = "_CorrespondencePensionNotificationListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }
}