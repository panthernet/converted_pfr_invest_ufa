﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfSpnStatusRefineViewModel: PureWebDialogViewModel
    {
        private readonly long _id;

        public WebNpfSpnStatusRefineViewModel(long id)
        {
            _id = id;
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnStatusRefineViewPartial");
            WebModelHelperEx.UpdateModelState(this);
        }

        public override bool WebExecuteSave()
        {
            var fr = DbHelper.Get<FinregisterWeb>(_id);
            fr.Status = RegisterIdentifier.FinregisterStatuses.Refine;
            fr.IsTransfered = 0;
            DbHelper.SaveChanges();
            return true;
        }
    }
}