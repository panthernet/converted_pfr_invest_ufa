﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Files;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(DocumentWeb), JournalDescription = "Документ (Корреспонденция)")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfCorrespViewModel: PureWebDialogViewModel
    {
        [JsonIgnore]
        private string _displayFileName;
        [JsonIgnore]
        private string _selectedFileName;
        [JsonIgnore]
        public DocumentWeb Document { get; set; }
        [JsonIgnore]
        public DocumentWeb.Types Type => DocumentWeb.Types.Npf;

        [JsonIgnore]
        public List<DocumentClassWeb> DocumentClassList { get; set; }
        [JsonIgnore]
        public List<LegalEntityWeb> SendersList { get; set; }
        [JsonIgnore]
        public List<string> StoragesList { get; set; }
        [JsonIgnore]
        public bool Checked { get; set; }

        [JsonIgnore]
        private List<string> NumbersOfCurrentYear { get; set; }


        public long ID
        {
            get { return Document.ID; }
            set { Document.ID = value; }
        }

        [DisplayName("Дата поступления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? IncomingDate
        {
            get { return Document.IncomingDate; }
            set { Document.IncomingDate = value; }
        }

        [DisplayName("Входящий номер")]
        public string IncomingNumber
        {
            get { return Document.IncomingNumber; }
            set { Document.IncomingNumber = value; }
        }

        [DisplayName("Отправитель")]
        [Required]
        public long? LegalEntityID
        {
            get { return Document.LegalEntityID; }
            set { Document.LegalEntityID = value; }
        }

        [DisplayName("Дата отправления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? OutgoingDate
        {
            get { return Document.OutgoingDate; }
            set { Document.OutgoingDate = value; }
        }

        [DisplayName("Исходящий номер")]
        public string OutgoingNumber
        {
            get { return Document.OutgoingNumber; }
            set { Document.OutgoingNumber = value; }
        }


        [DisplayName("Классификация документа")]
        [Required]
        public long? DocumentClassID
        {
            get { return Document.DocumentClassID; }
            set { Document.DocumentClassID = value; }
        }

        [DisplayName("Дополнительно по содержанию документа")]
        public string AdditionalInfoText
        {
            get { return Document.AdditionalInfoText; }
            set { Document.AdditionalInfoText = value; }
        }

        [DisplayName("Исполнитель")]
        public string ExecutorName
        {
            get { return Document.ExecutorName; }
            set { Document.ExecutorName = value; }
        }

        [DisplayName("Контрольная дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? ControlDate
        {
            get { return Document.ControlDate; }
            set { Document.ControlDate = value; }
        }

        [DisplayName("Дата исполнения")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? ExecutionDate
        {
            get { return Document.ExecutionDate; }
            set { Document.ExecutionDate = value; }
        }

        [DisplayName("Дополнительно по исполнителю")]
        public string AdditionalExecutionInfo
        {
            get { return Document.AdditionalExecutionInfo; }
            set { Document.AdditionalExecutionInfo = value; }
        }

        [DisplayName("Место хранения оригинала")]
        public string OriginalStoragePlace
        {
            get { return Document.OriginalStoragePlace; }
            set { Document.OriginalStoragePlace = value; }
        }

        [DisplayName("Дополнительная информация")]
        public string Comment
        {
            get { return Document.Comment; }
            set { Document.Comment = value; }
        }

        public string SelectedFileName
        {
            get { return _selectedFileName; }
            set { _selectedFileName = value; }
        }

        [DisplayName("Вложение")]
        public string DisplayFileName
        {
            get
            {
                if (string.IsNullOrEmpty(_displayFileName) && !string.IsNullOrEmpty(SelectedFileName))
                {
                    if (SelectedFileName.StartsWith("."))
                    {
                        _displayFileName = GetTrueFileName(SelectedFileName);
                    }
                    else _displayFileName = SelectedFileName;
                }
                return _displayFileName;
            }
            set { _displayFileName = value; }
        }


        private string GetTrueFileName(string filePath)
        {
            var f = Path.GetFileName(filePath);
            var spl = f.Split('_');
            return $"{spl[0]}{Path.GetExtension(f)}";
        }

        // private DocFileBodyWeb DocFileBody { get; set; }


        [JsonIgnore]
        public List<LinkedDocumentWebItem> LDItems { get; set; } = new List<LinkedDocumentWebItem>();

        public WebNpfCorrespViewModel()
            : this(0)
        {
        }

        public WebNpfCorrespViewModel(long id)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_CorrespondenceItemViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;

            Document = WCFClient.GetEFServiceFunc().Documents.Include(t=> t.DocFileBody).FirstOrDefault(a => a.ID == id) ?? new DocumentWeb { Type = DocumentWeb.Types.Npf, IncomingDate = DateTime.Now, OutgoingDate =  DateTime.Now };
            DocumentClassList = GetDocumentClassList();
            SendersList = GetLegalEntityList();
            StoragesList = GetOriginalStoragePlaces();
            //linked docs
            if (id != 0)
            {
                LDItems = WCFClient.GetEFServiceFunc().GetLinkedDocumentsList(id).Select(a => new LinkedDocumentWebItem(a)).ToList();
                if(SendersList.FirstOrDefault(a=> a.ID == LegalEntityID) == null)
                    SendersList.Add(WCFClient.GetEFServiceFunc().LegalEntities.FirstOrDefault(a=> a.ID == LegalEntityID));
                Checked = Document.IncomingNumber.ToLower().Contains("реестр");
                //!!!!DocFileBody = WCFClient.GetEFServiceFunc().DocFileBodies.FirstOrDefault(a => a.DocumentID == Document.ID);
                DisplayFileName = SelectedFileName = Document.DocFileBody?.FileName;
                SelectedFileName = SelectedFileName ?? ""; //hack!!!
            }
            else
            {
                Document.DocFileBody = new DocFileBodyWeb { DocumentID = id};
            }

            UpdateNumbersOfCurrentYear();
            ValidatableFields = $"{nameof(IncomingDate)}|{nameof(IncomingNumber)}|{nameof(LegalEntityID)}|{nameof(OutgoingDate)}|{nameof(OutgoingNumber)}|{nameof(DocumentClassID)}|{nameof(Comment)}|{nameof(ExecutorName)}|{nameof(AdditionalInfoText)}|{nameof(AdditionalExecutionInfo)}";
        }

        public override long WebGetEntryId()
        {
            return Document.ID;
        }

        protected List<string> GetOriginalStoragePlaces()
        {
            var t = (long) ElementWeb.Types.NpfDocumentStoragePlaces;
            return WCFClient.GetEFServiceFunc().Elements.Where(a=> a.Key == t && a.Visible == 1).Select(e => e.Name).ToList();   
        }

        protected List<LegalEntityWeb> GetLegalEntityList()
        {
            return WCFClient.GetEFServiceFunc().GetActiveNpfList();
        }

        protected List<DocumentClassWeb> GetDocumentClassList()
        {
            return WCFClient.GetEFServiceFunc().DocumentClasses.ToList();
        }

        protected IList<AdditionalDocumentInfo> GetAdditionalInfoList()
        {
            return new List<AdditionalDocumentInfo>();
        }
        
        protected IList<string> GetAdditionalExecutionInfo()
        {
            return new List<string>();
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            UpdateNumbersOfCurrentYear(IncomingDate);

            var docs = !Checked ? WCFClient.GetEFServiceFunc().GetDocumentsBypropertiesList(Type, IncomingNumber, IncomingDate) : null;
            if (ID == 0)
            {
                if (docs != null && docs.Count > 0)
                {
                    WebLastModelError = $"Документ с входящим номером '{Document.IncomingNumber}' уже существует.";
                    return false;
                }

            }
            else
            {
                if (docs != null && (docs.Count > 1 || docs.Count == 1 && docs[0].ID != Document.ID))
                {
                    WebLastModelError = $"Документ с входящим номером '{Document.IncomingNumber}' уже существует.";
                    return false;
                }
            }

            var result = CheckAttachment(SelectedFileName);
            if (result != null)
            {
                WebLastModelError = result;
                return false;
            }

            return true;
        }

        [JsonIgnore]
        public bool HasExecutorList => false;
        [JsonIgnore]
        public bool HasOutgoingNumber => false;
        [JsonIgnore]
        public bool HasAddInfoList => GetAdditionalInfoList().Count > 0;
        [JsonIgnore]
        public bool HasAddExecutionInfoList => GetAdditionalExecutionInfo().Count > 0;


        //HasExecutorList = false
        //HasOutgoingNumber = false
        public override bool WebExecuteSave()
        {
            Document.Type = Type;
            if (HasExecutorList) Document.ExecutorName = null; else Document.ExecutorID = null;
            if (HasAddInfoList)
                AdditionalInfoText = null;
            else
            {
                Document.AdditionalInfo = null;
                Document.AdditionalInfoID = null;
            }
            if (!HasOutgoingNumber)
                Document.OutgoingNumber = null;

            var deleteBody = string.IsNullOrWhiteSpace(SelectedFileName) && Document.DocFileBody != null;

            using (var t = WCFClient.GetEFServiceFunc().Database.BeginTransaction())
            {

                if (!WCFClient.GetEFServiceFunc().SaveAndCleanDocument(Document, deleteBody) || !UpdateAttachment())
                {
                    t.Rollback();
                    return false;
                }
                t.Commit();
            }

            return true;
        }

        private static string CheckAttachment(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                return null;

            var file = HttpContext.Current.Server.MapPath(filePath);
            var fi = new FileInfo(file);
            if (!fi.Exists)
                return $"Файл '{file}' не найден";
            if (StringExtension.Executables.Contains(fi.Extension))
                return $"Недопустимое расширение файла '{fi.Extension}'";
            if (fi.Length > Sizes.Size4Mb)
                return "Файл имеет слишком большой размер!";
            return null;
        }

        private bool UpdateAttachment()
        {
            try
            {
                if(string.IsNullOrWhiteSpace(SelectedFileName) || !SelectedFileName.StartsWith("../") || ID <= 0) return true;

                var fullFilePath = HttpContext.Current.Server.MapPath(SelectedFileName);

                if (!File.Exists(fullFilePath))
                {
                    WebLastModelError = $"Файл не найден! ({fullFilePath})";
                    return false;
                }
                var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read);
                if (fs.Length > 10048576)
                {
                    WebLastModelError = "Файл имеет слишком большой размер!";
                    return false;
                }

                var blob = new byte[fs.Length];
                fs.Read(blob, 0, blob.Length);
                fs.Close();
                //DocFileBody.Body = blob;

                var newBody = new DocFileBodyWeb();

//                if (Document.DocFileBody == null)
  //                  Document.DocFileBody = new DocFileBodyWeb();
                newBody.FileName = GetTrueFileName(fullFilePath);
                newBody.DocumentID = ID;

                try
                {
                    Document.DocFileBody = newBody;
                    WCFClient.GetEFServiceFunc().SaveDocFileBodyForDocument(ID, newBody, blob);
                }
                catch(Exception ex)
                {
                   // RaiseAttachmentUploadFailed();
                    Logger.LogEx(ex);
                    return false;
                }
            }
            catch(Exception ex)
            {
                    Logger.LogEx(ex);
              //  RaiseAttachmentUploadFailed();
                return false;
            }

            return true;
        }

        private void UpdateNumbersOfCurrentYear(DateTime? newDate = null)
        {
            var dt = newDate ?? (Document.IncomingDate ?? DateTime.Now);
            var gDate = new DateTime(dt.Year, 1, 1);
            var lDate = new DateTime(dt.Year + 1, 1, 1);

            NumbersOfCurrentYear = WCFClient.GetEFServiceFunc().Documents.Where(a => a.IncomingDate >= gDate && a.IncomingDate <= lDate
                                                                                                             && a.ID != Document.ID).Select(a => a.IncomingNumber).ToList();            
        }

        private const string WRONG_INCOMING_DATE = "Не указана дата поступления";
        private const string WRONG_INCOMING_NUM = "Не указан входящий номер";
        private const string WRONG_INCOMING_UK = "Не указан отправитель";
        private const string WRONG_OUTGOING_DATE = "Не указана дата отправления";
        private const string WRONG_DOC_CLASS = "Не указана классификация документа";
        private const string WRONG_INCOMING_NUM_EXISTS = "Выбранный входящий номер уже задан в выбранном году";
        private const string REG_STR = "Реестр ";



        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(IncomingDate):
                        return IncomingDate == null ? WRONG_INCOMING_DATE : ((OutgoingDate <= IncomingDate) ? null : "Дата отправления не может быть больше даты поступления");
                    case nameof(IncomingNumber):
                        if (string.IsNullOrEmpty(IncomingNumber))
                            return WRONG_INCOMING_NUM;
                        if (IncomingNumber.StartsWith(REG_STR))
                            return IncomingNumber.Remove(0, REG_STR.Length).ValidateMaxLength(50);
                        else
                        {
                            if (NumbersOfCurrentYear != null && NumbersOfCurrentYear.Any(x =>
                                    x.ToLower().Trim().Equals(IncomingNumber.ToLower().Trim(), StringComparison.InvariantCultureIgnoreCase)))
                                return WRONG_INCOMING_NUM_EXISTS;
                        }

                        return IncomingNumber.ValidateMaxLength(50);
                    case nameof(LegalEntityID): return LegalEntityID == null ? WRONG_INCOMING_UK : null;
                    case nameof(OutgoingDate):
                        return OutgoingDate == null ? WRONG_OUTGOING_DATE : ((OutgoingDate <= IncomingDate) ? null : "Дата отправления не может быть больше даты поступления");
                    case nameof(OutgoingNumber): return OutgoingNumber.ValidateMaxLength(50);
                    case nameof(DocumentClassID): return DocumentClassID == null ? WRONG_DOC_CLASS : null;
                    case nameof(Comment): return Comment.ValidateMaxLength(512);
                    case nameof(ExecutorName): return ExecutorName.ValidateMaxLength(512);
                    case nameof(AdditionalInfoText): return AdditionalInfoText.ValidateMaxLength(256);
                    case nameof(AdditionalExecutionInfo): return AdditionalExecutionInfo.ValidateMaxLength(256);
                    default:
                        return null;
                }
            }
        }
    }
}