﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(DelayedPaymentClaimWeb), JournalDescription = "Отложенные выплаты (Корреспонденция)")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfCorrespDelayedPaymentsListViewModel : WebNpfLazyListViewModel<DelayedPaymentClaimWeb>
    {
        public WebNpfCorrespDelayedPaymentsListViewModel() : base("Npf")
        {
            Query = WCFClient.GetEFServiceFunc()?.GetActiveEntityQuery<DelayedPaymentClaimWeb>();
            AjaxMain = "_CorrespondenceDelayedPaymentsListViewPartial";
            AjaxPartial = "_CorrespondenceDelayedPaymentsListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }
}