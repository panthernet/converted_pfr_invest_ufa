﻿using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfRejectAppListViewModel : WebNpfLazyListViewModel<RejectApplicationWeb>
    {
        private IQueryable _query;

        public override IQueryable Query => _query ?? (_query = WCFClient.GetEFServiceFunc()?.GetRejectApplicationList());

        public WebNpfRejectAppListViewModel()
        {
            AjaxMain = "_RejectApplicationListViewPartial";
            AjaxPartial = "_RejectApplicationListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }
}