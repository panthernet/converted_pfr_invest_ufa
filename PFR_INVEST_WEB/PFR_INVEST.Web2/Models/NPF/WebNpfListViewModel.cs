﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    public abstract class WebNpfListViewModel<T>: WebListViewModel<T>
        where T : class
    {
        public WebNpfListViewModel(IEnumerable<T> list) : base(list)
        {
            AjaxController = "Npf";
        }
    }

    public abstract class WebNpfLazyListViewModel<T>: IWebLazyListViewModel
        where T : class
    {
        public WebNpfLazyListViewModel(string controller = null)
        {
            InitializeDefault();
            AjaxController = controller ?? "Npf";
        }

        public string AjaxPartial { get; protected set; }
        public string AjaxMain { get; protected set; }
        public bool AttachDefaultGridScripts { get; protected set; }
        public string AjaxController { get; protected set; }
        public string Title { get; set; }
        public string ListId { get; set; }
        public IList<NameEntity> FormData { get; } = new List<NameEntity>();
        public IList List => Query.ToListAsync().Result;
        public virtual IQueryable Query { get; protected set; }
        public string AjaxCallbackAction { get; protected set; } = "LoadViewPartialBind";

        private void InitializeDefault()
        {
            var data = FormDataHelper.GetDataByModel(GetType().Name);
            if(data.ChildIds != null && data.ChildIds.Count != 0 && !data.ChildIds.All(string.IsNullOrEmpty))
                data.ChildIds.ForEach(t=> FormData.Add(FormDataHelper.Data[t]));
            ListId = data.Id;
            Title = data.DisplayName;
        }
    }
}