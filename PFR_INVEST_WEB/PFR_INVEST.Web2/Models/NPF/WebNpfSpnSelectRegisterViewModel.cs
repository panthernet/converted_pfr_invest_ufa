﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public abstract class WebSelectRegisterViewModel: PureWebDialogViewModel
    {
        public long? SelectedRegisterID { get; set; }

        public List<CommonRegisterListItemWeb> RegistersList => UpdateRegistersInternal(WebDirection, WebContent);

        protected long WebDirection { get;set; }
        protected long WebEntityID { get;set; }
        protected string WebContent { get;set; }

        protected WebSelectRegisterViewModel(long direction, string content)
        {
            WebDirection = direction;
            WebContent = content;
           // WebEntityID = entityId;
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = false;
            IsDeleteButtonVisible = false;
        }

        protected abstract List<CommonRegisterListItemWeb>  UpdateRegistersInternal(long direction, string content);

        protected List<CommonRegisterListItemWeb>  UpdateRegisters(AsgFinTr.Sections section, long direction, string content = null, bool forPp = false)
        {
            return
                (forPp
                    ? WCFClient.GetDataServiceFunc().GetCommonRegisterListForPP(section, direction)
                    : WCFClient.GetDataServiceFunc().GetCommonRegisterList(section, direction, content))
                .Select(a => a.CopyToRaw<CommonRegisterListItem, CommonRegisterListItemWeb>()).ToList();
        }


    }

    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class WebNpfSpnSelectRegisterViewModel : WebSelectRegisterViewModel
    {
        public WebNpfSpnSelectRegisterViewModel(): this(0, null) { }

        public WebNpfSpnSelectRegisterViewModel(object direction, object content): base(Convert.ToInt64(direction), (string)content)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_NpfSelectRegisterPartialView");
        }

        protected override List<CommonRegisterListItemWeb>  UpdateRegistersInternal(long direction, string content)
        {
            return UpdateRegisters(AsgFinTr.Sections.NPF, direction, content);
        }

      /*  public override bool WebExecuteSave()
        {
            var fr = DbHelper.Get<FinregisterWeb>(WebEntityID);
            fr.SelectedRegisterID = SelectedRegisterID;
            return true;
        }*/
    }
}