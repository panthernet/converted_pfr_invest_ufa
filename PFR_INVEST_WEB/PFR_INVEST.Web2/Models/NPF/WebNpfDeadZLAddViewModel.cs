﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(ERZLNotify), JournalDescription = "")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfDeadZLAddViewModel: PureWebDialogViewModel
    {
        [DisplayName("Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? WebDate { get; set; }

        public WebNpfDeadZLAddViewModel()
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_DzlAddViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            AttachDefaultGridScripts = true;
            RefreshLists();
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            return WebDate.HasValue;
        }

        [JsonIgnore]
        public BindingList<NPFforERZLNotifyListItem> ContragentsList { get; set; }


        #region Execute Implementations


#endregion


        public static List<NPFforERZLNotifyListItem> WebRefreshList(DateTime? date)
        {
            return !date.HasValue ? null : WCFClient.GetDataServiceFunc().GetDeadZLForAllNPF(date.Value);
        }

        public void RefreshLists()
        {
            ContragentsList = new BindingList<NPFforERZLNotifyListItem>(WebRefreshList(WebDate));
            IsDataChanged = false;
        }

        [JsonIgnore]
        public override string this[string columnName] => null;

        public bool WebExecuteSaveX()
        {
            
            foreach (var caLI in ContragentsList)
            {
                if (caLI.ERZLNotify != null)
                {
                    if (caLI.Count == null || caLI.Count == 0)
                    {
                        DataContainerFacade.Delete(caLI.ERZLNotify);
                    }
                    else
                    {
                        caLI.ERZLNotify.Count = caLI.Count;
                        DataContainerFacade.Save(caLI.ERZLNotify);    
                    }
                }
                else if (caLI.Contragent != null && caLI.Count != null)
                {
                    var crAcc = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(caLI.Contragent.ID, AccountIdentifier.MeasureNames.ZL);
                    var dbtAcc = WCFClient.GetDataServiceFunc().GetContragentAccountByMeasure(caLI.Contragent.ID, AccountIdentifier.MeasureNames.DeadZL);

                    if (crAcc == null)
                    {
                        crAcc = new Account
                        {
                            ContragentID = caLI.Contragent.ID,
                            AccountTypeID = AccountIdentifier.MeasureIDs.ZL,
                            Name = AccountIdentifier.MeasureNames.ZL
                        };
                        crAcc.ID = DataContainerFacade.Save(crAcc);
                    }
                    if (dbtAcc == null)
                    {
                        dbtAcc = new Account
                        {
                            ContragentID = caLI.Contragent.ID,
                            AccountTypeID = AccountIdentifier.MeasureIDs.DeadZL,
                            Name = AccountIdentifier.MeasureNames.DeadZL
                        };
                        dbtAcc.ID = DataContainerFacade.Save(dbtAcc);
                    }
                    
                    var erzlNotify = new ERZLNotify
                    {
                        Count = caLI.Count,
                        Date = WebDate,
                        CrAccID = crAcc.ID,
                        DbtAccID = dbtAcc.ID,
                        MainNPF = "0"
                    };
                    erzlNotify.ID = DataContainerFacade.Save<ERZLNotify, long>(erzlNotify);            
                }
            }
            IsDataChanged = false;
            return true;
        }

    }
}