﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfRedistZlListViewModel : WebNpfListViewModel<ERZLListItem>
    {
        public WebNpfRedistZlListViewModel() : base(ListProvider.GetZlRedistList())
        {
            AjaxMain = "_RzlListViewPartial";
            AjaxPartial = "_RzlListContainerViewPartial";
            AttachDefaultGridScripts = true;
        }
    }
}