﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.BusinessLogic.Models;
using ViewModelState = PFR_INVEST.Web2.BusinessLogic.Models.ViewModelState;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(ERZLNotify), JournalDescription = "Уведомление ЕРЗЛ")]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfDeadZlViewModel: PureWebFormViewModel
    {
        public WebNpfDeadZlViewModel():this(0)
        {
        }

        public WebNpfDeadZlViewModel(long id): base(id)
        {
            Load(id);
            WebModelHelperEx.InitializeDefault(this, "Npf", "_DzlEditViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            State = id > 0 ? ViewModelState.Edit : ViewModelState.Create;
        }

        #region OLD

        [DisplayName("Наименование НПФ")]
        public string NPFDisplayName { get; set; } = string.Empty;

        private ERZLNotify _currErzlNotify;
        [DisplayName("Количество")]
        [Required]
        public long? NotifyCount
        {
            get
            {
                return _currErzlNotify?.Count;
            }

            set
            {
                if (_currErzlNotify == null) return;
                _currErzlNotify.Count = value;
            }
        }

        [DisplayName("Дата уведомления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? NotifyDate
        {
            get
            {
                return _currErzlNotify?.Date;
            }

            set
            {
                if (_currErzlNotify == null) return;
                _currErzlNotify.Date = value;
            }
        }


        #region Execute Implementations

        public override bool WebExecuteDelete(long id)
        {
            DataContainerFacade.Delete(_currErzlNotify);
            return true;
        }

        public override bool WebExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    if (State == ViewModelState.Edit)
                    {
                        if (_currErzlNotify != null)
                            DataContainerFacade.Save<ERZLNotify, long>(_currErzlNotify);
                    }
                    break;
            }

            return true;
        }

        #endregion

        public void Load(long id)
        {
            try
            {
                ID = id;
                _currErzlNotify = DataContainerFacade.GetByID<ERZLNotify, long>(id);

                if (_currErzlNotify == null) return;
                try
                {
                    NPFDisplayName = _currErzlNotify.GetCreditAccount().GetContragent().GetLegalEntity().FormalizedNameFull;
                }
                catch
                {
                    NPFDisplayName = "";
                }
            }
            catch { }
        }

        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToLower())
                {
                    case "notifydate": return NotifyDate == null || NotifyDate == DateTime.MinValue ? errorMessage : null;
                    case "notifycount": return NotifyCount == null || NotifyCount == 0 ? errorMessage : null;
                    default: return null;
                }
            }
        }

        public override string Validate()
        {
            const string fieldNames = "notifydate|notifycount";
            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }
        #endregion

    }
}