﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PFR_INVEST.Web2.Database.Classes;
using PFR_INVEST.Web2.DataObjects.Web;
using JournalLogger = PFR_INVEST.Web2.BusinessLogic.JournalLogger;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class WebNpfSpnStatusGiveViewModel: PureWebDialogViewModel
    {
        #region FinMoveType

        [DisplayName("Способ передачи")]
        public string FinMoveType
        {
            get
            {
                return _finregister?.FinMoveType;
            }

            set
            {
                if (_finregister == null) return;
                _finregister.FinMoveType = value;
            }
        }
        #endregion

        #region FinMoveTypes

        public List<string> FinMoveTypes { get; set; }

        #endregion

        #region FinDate
        [DisplayName("Дата передачи финреестра")]
        public DateTime? FinDate
        {
            get
            {
                return _finregister?.FinDate;
            }

            set
            {
                if (_finregister == null) return;
                _finregister.FinDate = value;
            }
        }
        #endregion

        #region FinNum
        [DisplayName("Номер сопроводительного письма")]
        public string FinNum
        {
            get { return _finregister?.FinNum; }

            set
            {
                if (_finregister == null) return;
                _finregister.FinNum = value;
            }
        }
        #endregion

        [JsonIgnore]
        private readonly FinregisterWeb _finregister;

        [JsonIgnore]
        private readonly RegisterWeb _register;

        
        [JsonIgnore]
        public bool IsMassTransfer { get; set; }
        [JsonIgnore]
        public bool IsMassTransferDialog { get; set; }

        public WebNpfSpnStatusGiveViewModel(long id, long parentId)
        {
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnStatusGiveViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;

            if (id == 0 && parentId > 0)
            {
                _register = WCFClient.GetEFServiceFunc().Set<RegisterWeb>().AsNoTracking().FirstOrDefault(a => a.ID == parentId);
                IsMassTransferDialog = RegisterIdentifier.IsToNPF(_register.Kind);
                IsMassTransfer = true;
                _finregister = new FinregisterWeb
                {
                    FinDate = DateTime.Today,
                    FinMoveType = FinMoveTypes.First()
                };
            }
            else
            {
                _finregister = DbHelper.Get<FinregisterWeb>(id);
                ValidatableFields = $"{nameof(FinDate)}|{nameof(FinNum)}|{nameof(FinMoveType)}";
            }
        }


        [JsonIgnore]
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(FinDate):
                        return FinDate.HasValue ? null : DefaultValidationErrorMessage;
                    case nameof(FinMoveType):
                        return !string.IsNullOrEmpty(FinMoveType) ? null : DefaultValidationErrorMessage;
                    default:
                        return null;
                }
            }
        }

        public override bool WebExecuteSave()
        {
            if (IsMassTransfer)
            {
                var frs = DbHelper.GetUncached<RegisterWeb>(_register.ID).Finregisters.Where(ViewModelHelperNpf.IsFinregisterStatusGive).Select(a => a.ID).ToList();
                if (IsMassTransferDialog)
                {
                    var date = _finregister.FinDate;
                    var move = _finregister.FinMoveType;
                    WCFClient.GetDataServiceFunc().SetFinregisterStateTransfered(date, move, frs, RegisterIdentifier.IsFromNPF(_register.Kind));
                }
                else
                {
                    DateTime? date = null;
                    string move = null;
                    WCFClient.GetDataServiceFunc().SetFinregisterStateTransfered(date, move, frs, RegisterIdentifier.IsFromNPF(_register.Kind));
                }
            }
            else
            {
                _finregister.Status = RegisterIdentifier.FinregisterStatuses.Transferred;
                _finregister.IsTransfered = 1;
                DbHelper.SaveChanges();
                JournalLogger.LogArchiveEvent(this);
            }

            return true;
        }
    }
}