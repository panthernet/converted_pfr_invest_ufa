﻿using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Attributes;
using PureWebDialogViewModel = PFR_INVEST.Web2.BusinessLogic.Models.PureWebDialogViewModel;

namespace PFR_INVEST.Web2.Models
{
    [WebEditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [WebDeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class WebNpfSpnSelectPPViewModel: PureWebDialogViewModel
    {
        public long EntityID { get; set; }

        [XmlIgnore]
        [JsonIgnore]
        public IQueryable Query { get; set; }

        [DisplayName("SelectedId")]
        public long SelectedID { get; set; }

        public WebNpfSpnSelectPPViewModel(): this(0)
        {
        }

        public WebNpfSpnSelectPPViewModel(long id)
        {
            EntityID = id;
            Query = WCFClient.GetEFServiceFunc().GetUnlinkedPPForNpf(AsgFinTr.Directions.ToPFR).AsNoTracking();
            WebModelHelperEx.InitializeDefault(this, "Npf", "_SpnSelectPPViewPartial");
            WebModelHelperEx.UpdateModelState(this);
            IsDeleteButtonVisible = false;
        }

        public override string Validate()
        {
            return SelectedID == 0 ? "Не выбрано п/п!" : null;
        }

        public override bool WebExecuteSave()
        {
            var pp = WCFClient.GetEFServiceFunc().AsgFinTrs.FirstOrDefault(a => a.ID == SelectedID);
            pp.ReqTransferID = null;
            pp.FinregisterID = EntityID;
            pp.SectionElID = (long)AsgFinTr.Sections.NPF;
            pp.LinkElID = (long)AsgFinTr.Links.NPF;
            pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.NPF;

            var fr = WCFClient.GetEFServiceFunc().SpnFinregisters.FirstOrDefault(a=> a.ID == EntityID);            
            pp.LinkedRegisterID = fr.RegisterID;
            WCFClient.GetEFServiceFunc().SaveCommonPP(pp);
            return true;
        }

        public override bool WebBeforeExecuteSaveCheck()
        {
            if (SelectedID == 0)
            {
                WebLastModelError = "Необходимо выбрать п/п!";
                return false;
            }
            var pp = WCFClient.GetEFServiceFunc().AsgFinTrs.FirstOrDefault(a => a.ID == SelectedID);
            if (!ViewModelHelperBO.IsClosedPP(pp)) return true;
            WebLastModelError = "Внимание! Сумма выбранного перечисления заполнена. Привязка платежного поручения к выбранному перечислению невозможна.";
            return false;
        }
    }
}