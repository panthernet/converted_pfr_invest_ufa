﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.DataObjects.Web;

namespace PFR_INVEST.Web2.Models
{
    [ModelEntity(Type = typeof(RegisterWeb), JournalDescription = "Временное размещение")]
    public class WebNpfTempAllocViewModel:  WebNpfSpnRegisterViewModel
    {
        [DisplayName("Портфель")]
        public override long? PortfolioID { get { return SelectedPortfolio?.ID; }
            set
            {
                SelectedPortfolio = value.HasValue ? PortfoliosList.FirstOrDefault(a => a.ID == value) : null;
            } }

        [DisplayName("Суммы СПН утвержд. документом")]
        public override long? ApproveDocID
        {
            get { return Register.ApproveDocID; }
            set
            {
                Register.ApproveDoc = ApproveDocsList.FirstOrDefault(a => a.ID == value);
            }
        }

        public WebNpfTempAllocViewModel() : this(0, ViewModelState.Create) { }

        public WebNpfTempAllocViewModel(long entryId) : this(entryId, ViewModelState.Edit) { }


        #region Options

        [JsonIgnore]
        public override bool IsPortfolioVisible => true;
        [JsonIgnore]
	    public override bool IsPortfolioEnabled => true;

        [JsonIgnore]
	    public override bool IsSumCFRVisible => true;
        [JsonIgnore]
	    public override bool IsSumZLVisible => true;
        [JsonIgnore]
	    public override bool IsCompanyVisible => false;
        [JsonIgnore]
	    public override bool IsCompanyRequired => false;
        [JsonIgnore]
		public override bool IsERZLVisible => false;
        [JsonIgnore]
	    public override bool IsPayAssVisible => false;
        #endregion

        #region GridColumnSettings
        [JsonIgnore]
        public override bool IsCFRColumnVisible => true;
        [JsonIgnore]
	    public override bool IsZLColumnEditable => false;
        [JsonIgnore]
	    public override bool IsCountColumnEditable => false;
        [JsonIgnore]
	    public override bool IsCFRCountColumnEditable => false;

	    #endregion

		#region Kind
        [JsonIgnore]
		public override bool IsKindEditable => false;

        [JsonIgnore]
        public override List<string> Kinds { get; } = new List<string>
	    {
			RegisterIdentifier.TempAllocation
		};

	    #endregion

		public WebNpfTempAllocViewModel(long recordID, ViewModelState action)
			: base(recordID)
		{

		    WebModelHelperEx.InitializeDefault(this, "Npf", "_TempAllocationRegisterViewPartial");
		    WebModelHelperEx.UpdateModelState(this);
		    AttachDefaultGridScripts = true;

			RefreshPortfoliosList();
			if (action != ViewModelState.Create)
			{
				if (Register.PortfolioID != null)
					SelectedPortfolio = PortfoliosList.SingleOrDefault(x => x.ID == Register.PortfolioID.Value);
				//Content = Contents.First();
			}
			else
			{
				SelectedPortfolio = PortfoliosList.FirstOrDefault();
			    IsDataChanged = true;
			}						
		}

		public void RefreshPortfoliosList()
		{
			PortfoliosList = new List<Portfolio>(DataContainerFacade.GetList<Portfolio>());
		}

		public override void ExecuteDeleteFR(long frId)
		{
			if (SelectedPortfolio != null)
			{
				var allNPFSPN = WCFClient.GetEFServiceFunc().GetSPNAllocatedFinregistersList(SelectedPortfolio.ID).Where(x => x.NPFName == SelectedFR.NPFName);
				var retNPFSPN = WCFClient.GetEFServiceFunc().GetSPNReturnedFinregistersList(SelectedPortfolio.ID).Where(x => x.NPFName == SelectedFR.NPFName);
				if (allNPFSPN.Sum(x => x.Count) - retNPFSPN.Sum(x => x.Count) - SelectedFR.Count < 0)
				{
				    WebLastModelError = "Нельзя удалить выбранный НПФ, так как сумма изъятия превысит сумму размещения по текущему портфелю для выбранного НПФ";
                    return;
				}

				if (allNPFSPN.Sum(x => x.ZLCount) - retNPFSPN.Sum(x => x.ZLCount) - SelectedFR.ZLCount < 0)
				{
				    WebLastModelError = "Нельзя удалить выбранный НПФ, так как количество ЗЛ изъятия превысит количество ЗЛ размещения по текущему портфелю для выбранного НПФ";
					return;
                }

				base.ExecuteDeleteFR(frId);
			}
		}

		public override bool WebBeforeExecuteSaveCheck()
		{
			if (Register.ID > 0)
			{
				var dbPF = DataContainerFacade.GetObjectProperty<Register, long, long?>(Register.ID, "PortfolioID");
                if (!dbPF.HasValue) return true;
                if (Register.PortfolioID != dbPF)
				{// проверка остатка размещения для портфеля, идентификатор которого заменяется
					var sumErr = ValidateAllocationSum((long)dbPF);
					if (!string.IsNullOrEmpty(sumErr))
					{
					    WebLastModelError = sumErr;
                        return false;
					}
				}
			}

			return base.WebBeforeExecuteSaveCheck();
		}

		protected override bool WebBeforeExecuteDeleteCheck()
		{
		    if (Register.ID <= 0) return base.WebBeforeExecuteDeleteCheck();
		    var dbPF = DataContainerFacade.GetObjectProperty<Register, long, long?>(Register.ID, "PortfolioID");
		    if (!dbPF.HasValue) return true;
		    var sumErr = ValidateAllocationSum((long)dbPF);
		    if (!string.IsNullOrEmpty(sumErr))
		    {
		        WebLastModelError = sumErr;
                return false;
		    }

		    return base.WebBeforeExecuteDeleteCheck();
		}

		private string ValidateAllocationSum(long dbPF)
		{
			if (Register.ID == 0) return null;

			var frList =   WCFClient.GetEFServiceFunc().GetSPNAllocatedFinregistersList(dbPF).Where(x => x.RegisterID != Register.ID).GroupBy(x => x.NPFName);
			var frRetList = WCFClient.GetEFServiceFunc().GetSPNReturnedFinregistersList(dbPF).Where(x => x.RegisterID != Register.ID).GroupBy(x => x.NPFName);

			if (finregWithNPFList != null)
			{
				foreach (var finReg in finregWithNPFList)
				{
					var ret = frRetList.FirstOrDefault(x => x.Key == finReg.NPFName);
					if (ret != null)
					{
						var allc = frList.FirstOrDefault(x => x.Key == finReg.NPFName);
						if (allc == null)
						{
							return $"Нельзя {{0}}, так как по НПФ \"{ret.Key}\" выбранного портфеля были выполнены изъятия";
						}

						if (allc.Sum(x => x.Count) < ret.Sum(x => x.Count))
						{
							return $"Нельзя {{0}}, сумма изъятия НПФ \"{ret.Key}\" будет превышать сумму размещения";
						}

						if (allc.Sum(x => x.ZLCount) < ret.Sum(x => x.ZLCount))
						{
							return $"Нельзя {{0}}, количество ЗЛ изъятия для НПФ \"{ret.Key}\" будет превышать количество ЗЛ в размещении";
						}
					}
				}
			}

			return null;
		}

        [JsonIgnore]
	    public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToLower())
				{
					case "selectedportfolio": return SelectedPortfolio.ValidateRequired();
					default: return base[columnName];
				}
			}
		}
    }
}