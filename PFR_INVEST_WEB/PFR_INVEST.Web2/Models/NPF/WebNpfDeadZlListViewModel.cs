﻿using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Web2.Models
{
    public class WebNpfDeadZlListViewModel : WebNpfListViewModel<DeadZLListItem>
    {
        public WebNpfDeadZlListViewModel() : base(ListProvider.GetNpfDeadZlList())
        {
            AjaxMain = "~/Views/Menu/NPF/DeadZL/_DzlListViewPartial.cshtml";
            AjaxPartial = "~/Views/Menu/NPF/DeadZL/_DzlListContainerViewPartial.cshtml";
            AttachDefaultGridScripts = true;
        }
    }
}