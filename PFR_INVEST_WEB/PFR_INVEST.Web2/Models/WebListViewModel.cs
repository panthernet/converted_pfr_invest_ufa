﻿using System.Collections;
using System.Collections.Generic;
using PFR_INVEST.Web2.BusinessLogic;
using PFR_INVEST.Web2.BusinessLogic.Models;

namespace PFR_INVEST.Web2.Models
{
    /// <summary>
    /// Единая базовая модель для всех веб-моделей данных
    /// </summary>
    /// <typeparam name="T">Тип модели данных их ПТК ДОКИП</typeparam>
    public abstract class WebListViewModel<T> : IWebListViewModel
        where T: class
    {
        /// <summary>
        /// Идентификатор представления списка
        /// </summary>
        public string ListId { get; set; }
        /// <summary>
        /// Набор данных для форм, ассоциированных со списком
        /// </summary>
        public IList<NameEntity> FormData { get; } = new List<NameEntity>();
        /// <summary>
        /// Набор данных списка
        /// </summary>
        public IList List => InternalList;

        private List<T> InternalList { get; set; }
        public string AjaxController { get; protected set; }
        public string AjaxCallbackAction { get; protected set; } = "LoadViewPartialBind";
        public string AjaxPartial { get; protected set; }
        public string AjaxMain { get; protected set; }
        public string Title { get; set; }
        /// <summary>
        /// Загрузить скрипты для грида по умолчанию на форме
        /// </summary>
        public bool AttachDefaultGridScripts { get; protected set; }

        public void InitializeDefault()
        {
            var data = FormDataHelper.GetDataByModel(GetType().Name);
            data.ChildIds?.ForEach(t=> FormData.Add(FormDataHelper.Data[t]));
            ListId = data.Id;
            Title = data.DisplayName;
        }

        public WebListViewModel(IEnumerable<T> list)
        {
            InitializeDefault();
            InternalList = new List<T>(list);
        }
    }
}