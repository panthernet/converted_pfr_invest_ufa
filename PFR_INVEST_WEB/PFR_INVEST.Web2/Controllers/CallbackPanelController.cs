﻿using System.Threading;
using System.Web.Mvc;
using DevExpress.Web.Mvc;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Controllers
{
    public class CallbackPanelController : Controller
    {
        // GET: CallbackPanel
        public ActionResult Index()
        {
            return HistoryPartial();
        }

        public ActionResult HistoryPartial()
        {
            if (DevExpressHelper.IsCallback)
                Thread.Sleep(500);

            var id = Request.Params["FormID"];
            string cache = null;
            if (!string.IsNullOrEmpty(id))
            {
                cache = CookieStore.RemoveFromHistoryCookie(HttpContext.User.Identity.Name, id);
            }

            return PartialView("_HistorySideBar", cache != null ? CookieStore.GetHistoryCookieEntriesFromCache(cache) : CookieStore.GetHistoryCookieEntries(HttpContext.User.Identity.Name));
        }
    }
}