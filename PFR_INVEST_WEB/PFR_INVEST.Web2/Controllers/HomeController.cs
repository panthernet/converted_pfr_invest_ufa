﻿using System;
using System.IO;
using System.Web.Mvc;
using PFR_INVEST.Web2.BusinessLogic;

namespace PFR_INVEST.Web2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ControllerHelper.SetupMenuAndRoles(ViewData);
            return View();
        }

      /*  [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetFile()
        {
            // No need to dispose the stream, MVC does it for you
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "Images", "logo_mid.png");
            var stream = new FileStream(path, FileMode.Open);
            var result = new FileStreamResult(stream, "image/png") {FileDownloadName = "logo.png"};
            return result;

        }*/
    }
}