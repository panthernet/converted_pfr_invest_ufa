﻿
function unloadFormInputCheck(formId) {
    if (!$('#' + formId)) return;
    $('#' + formId).off();
    orig = [];
}

function loadFormInputCheck(formId, btnId) {
    if (!$('#' + btnId) || !$('#' + formId)) return;

    $.fn.getType = function() {
        return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
    }

    $("#" + formId + " :input").each(function() {
        var type = $(this).getType();
        var tmp = {
            'type': type,
            'value': $(this).val()
        };
        if (type == 'radio') {
            tmp.checked = $(this).is(':checked');
        }
        orig[$(this).attr('id')] = tmp;
    });

    $('#' + formId).on('change keyup',
        function() {
            var disable = true;
            $("#" + formId + " :input").each(function() {
                var type = $(this).getType();
                var id = $(this).attr('id');

                if (type == 'text' || type == 'textarea' || type == 'select' || (type == 'hidden' && typeof id !== "undefined" && $.inArray(id, orig, 0)!=-1)) {
                    disable = (orig[id].value == $(this).val());
                } else if (type == 'radio') {
                    disable = (orig[id].checked == $(this).is(':checked'));
                }

                if (!disable) {
                    return false; // break out of loop
                }
            });

            eval(btnId).SetEnabled(!disable);
        });
}

