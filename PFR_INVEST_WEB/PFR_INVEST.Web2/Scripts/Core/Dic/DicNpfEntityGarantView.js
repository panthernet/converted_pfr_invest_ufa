﻿ function onIsGarantedCheckedChanged(s) {
     if (s.GetValue() === true) {
         FundNum.SetEnabled(true);
         FundDate.SetEnabled(true);
         NotifNum.SetEnabled(true);
         NotifDate.SetEnabled(true);
     } else {
         FundNum.SetEnabled(false);
         FundDate.SetEnabled(false);
         NotifNum.SetEnabled(false);
         NotifDate.SetEnabled(false);
     }
 }
