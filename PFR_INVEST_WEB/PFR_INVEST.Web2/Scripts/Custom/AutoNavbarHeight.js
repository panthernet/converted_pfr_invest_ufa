﻿var onResize = function () {
    // apply dynamic padding at the top of the body according to the fixed navbar height
    $("body").css("padding-top", Math.max($(".col-fixed-325").height(), $(".navbar-fixed-top").height()));
    $(".pull-down").each(function () {
        var $this = $(this);
        $this.css("margin-top", Math.max($this.parent().height(), 70) - $this.height());
    });
};

// attach the function to the window resize event
$(window).resize(onResize);

// call it also when the page is ready after load or reload
$(function () {
    onResize();
});

