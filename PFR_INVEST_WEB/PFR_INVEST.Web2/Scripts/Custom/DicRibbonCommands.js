﻿
//список идентификаторов (соотв. кнопкам риббона), для которых требуется идентификатор записи (НЕ родителя)
var ribbonListOnlyEntityId = ["NpfSpnFinregisterItem", "NpfSpnStatusReqItem", "NpfSpnStatusRollbackItem", "NpfSpnStatusGiveItem", "NpfSpnStatusRefineItem", "NpfSpnStatusFixedItem", "NpfSpnStatusNotTrItem",
    "NpfSpnSelectPPItem"];

var ribbonRedirects = ["NpfReports"];

function downloadFile (controller,preaction,action,id) {
    $.ajax(
        {
            url: '/'+controller+'/'+preaction,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            datatype: 'json',
            data: "{ id: "+ id +"}",
            error: function (xhr) {
                alert('Не удалось открыть файл! Ошибка: ' + xhr.statusText);
            },
            success: function (data) {
               // var response = JSON.parse(data);
                window.location =
                    '/' + controller + '/' + action + '?filename=' + encodeURIComponent(data.Filename);
            },
            async: true
        });
}

function OnRibbonCommandExecuted(s, e) {
    var id = e.item.name;
    var parentEntryId = listParentEntryId == null ? 0 : listParentEntryId;
    var parentFormId = listParentEntryName == null ? 0 : listParentEntryName;
    var entryId = listEntryId != null && ribbonListOnlyEntityId.includes(id) ? listEntryId : 0;
    //trim gridview ending to get correct from Id if we go from GridView
    if (parentFormId && parentFormId.endsWith('GridView'))
        parentFormId = parentFormId.substring(0, parentFormId.length - 8);

    debugger;
    if (ribbonRedirects.includes(id)) {
        if (ProcessRedirect(id))
            return;
    }
    var category = s.name.replace('ribbon', '');
    OnRibbonCommandExecutedExternal(id, category, entryId, 0, parentEntryId, parentFormId);
}

function ProcessRedirect(id) {
    switch (id) {
        case 'NpfReports':
            Object.assign(document.createElement('a'), { target: '_blank', href: 'http://mercury.it.ru:9111/cognos10/cgi-bin/cognos.cgi'}).click();
            return true;
    }

    return false;
}

function OnRibbonSubformCommandExecuted(id, category, entryId, parentEntryId) {
    var parentFormId = listParentEntryName == null ? 0 : listParentEntryName;
    //trim gridview ending to get correct from Id if we go from GridView
    if (parentFormId && parentFormId.endsWith('GridView'))
        parentFormId = parentFormId.substring(0, parentFormId.length - 8);
    OnRibbonCommandExecutedExternal(id, category, entryId, 1, parentEntryId, parentFormId);
}


function OnRibbonCommandExecutedExternal(id, category, entryId = 0, layer = 0, parentEntryId = null, parentId = null) {

    if (!id) return;

    if (id.endsWith("EditDelete")) {
        return;
    }

    globalUpdateButtonsVisibility(false, category);
    if (id.toLowerCase().includes('list') && !id.startsWith("dlg")) {
        listParentEntryId = null;
        listEntryId = null;
        OnRibbonCommandExecutedExternalInplace(id, category, entryId);
        return;
    }

    $.ajax({
        url: '/'+category+'/LoadViewPartialBindPopup',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ id: id, entryId: entryId, layerstr: layer, parentEntryId: parentEntryId, parentId: parentId, category: category }),
        async: true,
        success: function (data) {
            if (layer === 0) {
                $("#popupscriptdiv").html(data.Script);
                $("#popupdiv").html(data.Html);
            } else {
                $("#popupscriptdiv"+layer).html(data.Script);                
                $("#popupdiv"+layer).html(data.Html);
            }

            if(!id.startsWith("dlg"))
                updateHistoryPanel();

            var fn = window[data.PopupShowMethod];
            if(typeof fn === 'function') {
                fn(data.EntryId);
            }
            //showPopupx(data.EntryId);
        },
        error: function(xhr, status, error) {
            //var err = xhr.responseText !== '' ? JSON.parse(xhr.responseText) : null;
            window.location.href = "/";
        }
    });
}

function OnRibbonCommandExecutedExternalInplace(id, category, entryId = 0) {

    if (id.endsWith("EditSave")) {
        eval("btnSubmit").DoClick();
        return;
    }

    if (id.endsWith("EditDelete")) {
        return;
    }

    if($("#loadingPanelExt").length>0) 
        loadingPanelExt.Show();

    $.ajax({
        url: '/'+category+'/LoadViewPartial',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ id: id, entryId: entryId, category: category }),
        async: true,
        success: function (data) {
            if($("#loadingPanelExt").length>0) 
                loadingPanelExt.Hide();
            var htmld = data.Html;
            $("#FormHeader").html(data.Title);
            $("#ajaxdiv").html(htmld);

           // ribbon.GetItemByName('DicNpfEditSave').SetEnabled(data.IsSaveEnabled);
            updateHistoryPanel();
        },
        error: function(xhr, status, error) {
            //if($("#loadingPanelExt").length>0) 
            //    loadingPanelExt.Hide();
            //var err = xhr.responseText !== '' ? JSON.parse(xhr.responseText) : null;
            //alert("Ошибка запроса: "+ err.Message);
            window.location.href = "/";

        }
    });

    //UpdateRibbonAccessibility(text);
}


function ExecuteDialogWithCustomParams(id, category, parentId, layer, inputParams) {

    if (!id) return;

    $.ajax({
        url: '/'+category+'/LoadViewPartialBindDialogPopup',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ id: id, parentIdForSubmit: parentId, layerstr: layer, category: category, inputParams: inputParams }),
        async: true,
        success: function (data) {
            if (layer === 0) {
                $("#popupscriptdiv").html(data.Script);
                $("#popupdiv").html(data.Html);
            } else {
                $("#popupscriptdiv"+layer).html(data.Script);                
                $("#popupdiv"+layer).html(data.Html);
            }

            var fn = window[data.PopupShowMethod];
            if(typeof fn === 'function') {
                fn(data.EntryId);
            }
            //showPopupx(data.EntryId);
        },
        error: function(xhr, status, error) {
            //var err = xhr.responseText !== '' ? JSON.parse(xhr.responseText) : null;
            window.location.href = "/";
        }
    });
}

function ExecuteServerSideAction(category, action, data, prmName, handler) {
    var url = '/' + category + '/' + action; //TODO controller
    var arr = {};
    arr[prmName] = data.toString();

    debugger;
    $.post(url, $.param(arr))
        .fail(function() { alert('Не удалось выполнить запрос!'); })
        .done(handler);
}

/*function OnPopupItemClickExecuted(s, e) {
    var text = e.item.name;
    switch (text) {
    case 'CatDic':
        window.location.href = "~/Views/Menu/Dictionary/Index.cshtml";
        return;
    case 'CatNpf':
        window.location.href = "~/Views/Menu/Dictionary/Index.cshtml";
        return;
    case 'CatClose':
        e.item.menu.CloseUp();
        return;
    default:
        alert('Unknown menu item!');
        return;
    }
}*/
