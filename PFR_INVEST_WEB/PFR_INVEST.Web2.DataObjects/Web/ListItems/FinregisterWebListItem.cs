﻿using System;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class FinregisterWebListItem
    {
        
        public string NPFName { get; set; }

        public string NPFStatus { get; set; }

        public bool? HasPP { get; set; }

        public FinregisterWebListItem() {}

        public FinregisterWebListItem(FinregisterWeb fr, RegisterWeb r, bool hasPP,
            string creditNPFName, string debitNPFName, string creditNPFStatus, string debitNPFStatus)
        {
            fr.CopyToRaw(this);
            HasPP = hasPP;
            NPFStatus = RegisterIdentifier.IsFromNPF(r.Kind) ? creditNPFStatus : debitNPFStatus;
            NPFName = RegisterIdentifier.IsFromNPF(r.Kind) ? creditNPFName : debitNPFName;
        }


        //FR
        public int IsTransfered { get; set; }

        public long ID { get; set; }

        public long? RegisterID { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Count { get; set; } = 0;

        public decimal? CFRCount { get; set; }

        public long CrAccID { get; set; }

        public long DbtAccID { get; set; }

        public string Status { get; set; }

        public DateTime? LetterDate { get; set; }

        public string LetterNum { get; set; }

        public long? ZLCount { get; set; }

        public DateTime? FinDate { get; set; }

        public string FinNum { get; set; }

        public string FinMoveType { get; set; }

        public string RegNum { get; set; }

        public long? ParentFinregisterID { get; set; }

        public long? SelectedContentID { get; set; }
        
        public long? SelectedRegisterID { get; set; }

        public string Comment { get; set; }

        public long StatusID { get; set; }
    }
}
