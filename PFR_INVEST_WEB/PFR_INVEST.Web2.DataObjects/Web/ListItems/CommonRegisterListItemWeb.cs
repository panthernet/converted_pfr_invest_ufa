﻿using System;
using System.Globalization;
using System.Threading;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web.ListItems
{
    public class CommonRegisterListItemWeb : IIdentifiable
    {     
        public long ID { get; set; }

        public string Name { get; set; }

        public AsgFinTr.Sections Section { get; set; } = AsgFinTr.Sections.All;

        public CommonRegisterListItemWeb() 
        {
        }

        public CommonRegisterListItemWeb(long ID, DateTime? date, string regnum) 
            : this(ID, date, regnum, null)
        {
        }

        public CommonRegisterListItemWeb(long ID, DateTime? date, string regnum, string approvedoc = null)
            : this()
        {
            var tmp = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            this.ID = ID;
            Name = $"{ID} \tдокумент: {approvedoc}     \r\n\t№ {regnum} от: {date?.ToShortDateString() ?? string.Empty}";
            Thread.CurrentThread.CurrentCulture = tmp;
        }
    }
}

