﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class Def1CWeb: IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual string ExportGroup { get; set; }

        public virtual string Name { get; set; }

        public virtual string Comment { get; set; }

        public class Groups
        {
            public const string UK = "UK";
            public const string VR = "VR";
            public const string NPF = "NPF";
            public const string DEPOSIT = "DEPOSIT";
        }

        public enum IntGroup
        {
            Npf = 0,
            Si,
            Vr,
            Depo,
            All,
            Opfr,
        }

    }

}
