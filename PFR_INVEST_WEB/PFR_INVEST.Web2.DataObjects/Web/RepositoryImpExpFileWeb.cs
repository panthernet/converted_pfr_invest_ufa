﻿using System;
using System.Globalization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class RepositoryImpExpFileWeb: IIdentifiable, IOldStatusEntity
    {
        public virtual long ID { get; set; }

		public virtual DateTime DDate { get; set; }

		public virtual TimeSpan TTime { get; set; }

		public virtual string UserName { get; set; }

		public virtual DateTime? AuctionDate { get; set; }

		public virtual int? AuctionLocalNum { get; set; }

		public virtual string Operacia { get; set; }

		/// <summary>
		/// 1 - Импорт
		/// 0 - Экспорт
		/// </summary>
		public virtual int ImpExp { get; set; }

		public virtual int Key { get; set; }

		public virtual byte[] Repository { get; set; }

		public virtual string Comment { get; set; }

		public virtual int Status { get; set; }

        public enum Keys
        {

            /// <summary>
            /// Сводный реестр заявок
            /// </summary>
            DepClaimImport=1,
            /// <summary>
            /// Сводный реестр заявок, с учетом максимальной суммы размещения
            /// </summary>
            DepClaimImportMax = 2,
            /// <summary>
            /// Импорт сводного реестра заявок от СПВБ
            /// </summary>
            DepClaimImportSPVB = 3,
            /// <summary>
            /// 
            /// </summary>
            DepClaimImportZ = 4,
            /// <summary>
            /// Импорт пакета XML от ММВБ
            /// </summary>
            AucImportPacket = 5,
            /// <summary>
            /// Заключение о результатах проверки
            /// </summary>
            BankVerResolution = 6,
            /// <summary>
            /// Сформировать лимиты
            /// </summary>
            BankVerLimits = 7,
            /// <summary>
            /// Экспорт 1С
            /// </summary>
            OnesExportXml = 199,
            /// <summary>
            /// Импорт 1С
            /// </summary>
            OnesImportXml = 200,
            /// <summary>
            /// Экспорт отчета в ЦБ
            /// </summary>
            CBReportExportXml = 201,
            /// <summary>
            /// Импорт ОПФР
            /// </summary>
            OpfrImport = 202
        }

        public RepositoryImpExpFileWeb()
        {

        }

        public RepositoryImpExpFileWeb(bool noAuc)
        {
            if (noAuc)
            {
                AuctionLocalNum = 999;
                AuctionDate = DateTime.ParseExact("01.01.2099", "dd.MM.yyyy", CultureInfo.InvariantCulture);
                Operacia = "";
            }
        }
    }
}
