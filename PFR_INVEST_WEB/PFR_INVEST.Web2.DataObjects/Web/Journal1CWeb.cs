﻿using System;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class Journal1CWeb : IIdentifiable
    {
        public enum Actions
        {
            General = 0,
            Load = 1,
            Validate = 2,
            Parse = 3,
            ExportComplete = 4,
            Export = 5
        }

        public enum LogTypes: int
        {
            Info = 0,
            Error = 1
        }

        public virtual long ID { get; set; }

        public virtual int Action { get; set; }

        public virtual long SessionID { get; set; }

        public virtual long? ImportID { get; set; }

        public virtual DateTime LogDate { get; set; }

        public virtual int LogType { get; set; }

        public virtual string Message { get; set; }

        public virtual string LogTypeName
        {
            get
            {
                switch (LogType)
                {
                    case (int)LogTypes.Info:
                        return "Информация";
                    case (int)LogTypes.Error:
                        return "Ошибка";
                    default:
                        return "";
                }
            }
        }
    }
}
