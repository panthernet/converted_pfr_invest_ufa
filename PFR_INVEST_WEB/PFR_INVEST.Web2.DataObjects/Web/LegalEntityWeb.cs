﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class LegalEntityWeb: IIdentifiable, IStatusEntity
    {
        public List<PensionNotificationWeb> PensionNotifications { get; set; }
        public List<BankAccountWeb> BankAccounts { get; set; }

        public virtual ContragentWeb Contragent { get; set; }

        public List<DocumentWeb> Documents { get; set; }

        public long StatusID
        {
            get { return Contragent?.StatusID ?? 0; }
            set { Contragent.StatusID = value; }
        }

        public long ID { get; set; }

        public int? NumberForDocuments { get; set; }

		public string PDReason { get; set; }

		public string FullName { get; set; }

		public string INN { get; set; }

		public string HeadPosition { get; set; }

		public string HeadFullName { get; set; }

		public string LegalAddress { get; set; }

		public string StreetAddress { get; set; }

		public string PostAddress { get; set; }

		public string Phone { get; set; }

		public string Fax { get; set; }

		public string Comment { get; set; }

		public string EAddress { get; set; }
        
       // [ForeContragent")]
        public long? ContragentID => Contragent?.ID;

		public DateTime? RegistrationDate { get; set; }

        public DateTime? EndDate { get; set; }

		public DateTime? CloseDate { get; set; }

		public string ShortName { get; set; }

		public string RegistrationNum { get; set; }

		public string FormalizedName { get; set; }

		public string Site { get; set; }

		public string Subsidiaries { get; set; }

		public string Registrator { get; set; }

		public string OKPP { get; set; }

		public string DeclName { get; set; }

		public string LetterWho { get; set; }

		public string TransfDocKind { get; set; }

		public long? LegalStatusID { get; set; }

		public long? PFBA_ID { get; set; }

		public string Info { get; set; }

        /// <summary>
        /// № соглашения с ПФР
        /// </summary>
		public string PFRAGR { get; set; }

        /// <summary>
        /// Дата соглашения с ПФР
        /// </summary>
        public DateTime? PFRAGRDATE { get; set; }

		public string PFRAGRSTAT { get; set; }

		public decimal? Money { get; set; }

		public DateTime? MoneyDate { get; set; }

		public string Fitch { get; set; }

		public DateTime? FitchDate { get; set; }

		public string Standard { get; set; }

		public DateTime? StandardDate { get; set; }

		public string Moody { get; set; }

		public DateTime? MoodyDate { get; set; }

		public string CorrAcc { get; set; }

		public string CorrNote { get; set; }
        
		public string BIK { get; set; }

		public string ForLetter { get; set; }

		public long? Sex { get; set; }

        public string StockCode { get; set; }

	    [IgnoreDataMember]
        public decimal? Limit4Money { get; set; }

        /// <summary>
        /// Признак внесения НПФ в реестр фондов участников (ELEMENT) 
        /// </summary>        
        public long? GarantACBID { get; set; } = (long)LegalEntity.GarantACB.NotIncluded;

        public DateTime? FundDate { get; set; }

        public long? FundNum { get; set; }

        public DateTime? NotifDate { get; set; }

        public string NotifNum { get; set; }

		public string OldName { get; set; }

	    [IgnoreDataMember]
		public string FormalizedNameFull => GetFormalizedNameFull(this.FormalizedName, this.OldName);

	    [IgnoreDataMember]
        public string GarantName
        {
            get
            {
                if (GarantACBID != null && GarantACBID.Value == (long)LegalEntity.GarantACB.Included)
                    return LegalEntity.INCLUDED;
                return LegalEntity.NOT_INCLUDED;
            }
        }
        
        public static string GetFormalizedNameFull(string formalizedName, string oldName)
        {
            return string.IsNullOrEmpty(oldName) ? formalizedName : $"{formalizedName} ({oldName})";
        }
    }
}
