﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class Settings1CWeb: IIdentifiable
    {
        public virtual long ID { get; set; }

        public virtual long SetDefID { get; set; }

        public virtual long OperationContentID { get; set; }

        public virtual long PortfolioTypeID { get; set; }

        public virtual string Comment { get; set; }

    }

}
