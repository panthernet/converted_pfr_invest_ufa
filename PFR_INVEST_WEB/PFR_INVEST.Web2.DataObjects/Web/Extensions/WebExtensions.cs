﻿using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Web2.DataObjects.Web.ListItems;

namespace PFR_INVEST.Web2.DataObjects.Web.Extensions
{
    public static class WebExtensions
    {
        /// <summary>
        /// Распаковывает содержимое файла, меняя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static void Decompress(this ImportFileListWebItem file)
        {
            if (!file.IsCompressed || file.FileContent == null) return;
            file.FileContent = GZipCompressor.Decompress(file.FileContent);
            file.IsCompressed = false;
        }

        /// <summary>
        /// Запаковывает содержимое файла, меняя изначальные данные
        /// </summary>
        /// <param name="file">Файл</param>
        public static void Compress(this ImportFileListWebItem file)
        {
            if (file.IsCompressed || file.FileContent == null) return;
            file.FileContent = GZipCompressor.Compress(file.FileContent);
            file.IsCompressed = true;
        }
    }
}
