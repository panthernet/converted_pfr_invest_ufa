﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class ContragentWeb: IIdentifiable, IStatusEntity
    {
        public IList<AccountWeb> Accounts { get; set; }

        public LegalEntityWeb LegalEntity { get; set; }

        public virtual StatusWeb Status { get; set; }

        public long ID { get; set; }
        
        public string Name { get; set; }

        public string TypeName { get; set; }

        public long StatusID { get; set; }//=> Status?.ID ?? 0;

        public long? LegalEntityID => LegalEntity?.ID;

        public enum Types
        {
            Bank = 1,
            BankAgent=2,
            GUK = 3,
            Npf = 4,
            PFR = 5,
            SD = 6,
            UK = 7
        }
    }
}
