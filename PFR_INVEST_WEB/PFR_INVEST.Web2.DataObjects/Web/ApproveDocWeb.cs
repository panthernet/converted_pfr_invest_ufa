﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Web2.DataObjects.Web
{
    public class ApproveDocWeb: IIdentifiable
    {
        //public virtual RegisterWeb Register { get; set; }

        public long ID { get; set; }
        
        public string Name { get; set; }
    }
}
