﻿namespace PFR_INVEST.Web2.DataObjects.Web
{
    public interface IStatusEntity
    {
        long StatusID { get; set; }
    }

    public interface IOldStatusEntity
    {
        int Status { get; set; }
    }
}