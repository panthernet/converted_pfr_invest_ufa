﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PFR_INVEST.Web2.ClassLibrary.Models
{
    /// <summary>
    /// Провайдер методов работы с авторизацией и проверкой доступа
    /// </summary>
    public static class AuthProvider
    {
#if !WEBCLIENT
        public static AuthProvider Provider;
#else
        public static AuthProvider Provider => GetUserAP();

        public static Func<AuthProvider> GetUserAP;
#endif
    }
}
