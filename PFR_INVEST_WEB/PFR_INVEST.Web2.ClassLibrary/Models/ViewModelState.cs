﻿namespace PFR_INVEST.Web2.ClassLibrary.Models
{
    public enum ViewModelState
    {
        /// <summary>
        /// Состояние - чтение, не допускает удаления и редактирования данных
        /// </summary>
        Read = 0,
        /// <summary>
        /// Состояние правка - допускает удаление и редактирование данных
        /// </summary>
        Edit = 1,
        /// <summary>
        /// Состояние - новый, устанавливается при создании документа до момента его первичного сохранения
        /// </summary>
        Create = 2,
        /// <summary>
        /// Состояние - экспорт
        /// </summary>
        Export = 3
    }
}
