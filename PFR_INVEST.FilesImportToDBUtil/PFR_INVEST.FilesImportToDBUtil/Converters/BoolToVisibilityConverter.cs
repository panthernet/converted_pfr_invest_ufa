﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PFR_INVEST.FilesImportToDBUtil.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool || value is bool?)
                return (bool)value ? Visibility.Visible : Visibility.Collapsed;
            else
                throw new ArgumentException("'value' has incorrect type.", "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
                return (Visibility)value == Visibility.Visible;
            else
                throw new ArgumentException("'value' has incorrect type.", "value");
        }
    }
}
