﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace PFR_INVEST.FilesImportToDBUtil.Converters
{
    public class InverseBoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool || value is bool?)
                return (bool)value ? Visibility.Collapsed : Visibility.Visible;
            else
                throw new ArgumentException("'value' has incorrect type.", "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
                return (Visibility)value == Visibility.Collapsed;
            else
                throw new ArgumentException("'value' has incorrect type.", "value");
        }
    }
}
