﻿using System.Configuration;
using System.Data.Odbc;

namespace PFR_INVEST.FilesImportToDBUtil.Managers
{
    public class DBManager
    {
        private readonly string connectionString;

        public DBManager()
        {
            string schemeName = ConfigurationManager.AppSettings.Get("SchemeName");
            string dsn = ConfigurationManager.AppSettings.Get("DSN");
            string databaseName = ConfigurationManager.AppSettings.Get("DatabaseName");
            string hostName = ConfigurationManager.AppSettings.Get("HostName");
            string port = ConfigurationManager.AppSettings.Get("Port");
            string userName = ConfigurationManager.AppSettings.Get("UserName");
            string password = ConfigurationManager.AppSettings.Get("Password");

            if (string.IsNullOrEmpty(dsn))
            {
                connectionString = string.Format(
                    "Driver=IBM DB2 ODBC DRIVER;Database={0};Hostname={1};Protocol=TCPIP;Port={2};Uid={3};Pwd={4};",
                    databaseName, hostName, port, userName, password);
            }
            else
            {
                connectionString = string.Format("DSN={0}", dsn);
            }
        }

        public bool UpdateDBColumn(string tableName, string columnName, object entityId, byte[] content)
        {
            using (OdbcConnection odbcConnection = new OdbcConnection(connectionString))
            {
                odbcConnection.Open();

                string commandText = string.Format("update {0} set {1} = ? where ID = ?", tableName, columnName);
                using (OdbcCommand command = new OdbcCommand(commandText, odbcConnection))
                {
                    command.Parameters.AddWithValue("@DATA", content);
                    command.Parameters.AddWithValue("@ID", entityId);

                    var result = command.ExecuteNonQuery();
                    return result > 0;
                }
            }
        }
    }
}
