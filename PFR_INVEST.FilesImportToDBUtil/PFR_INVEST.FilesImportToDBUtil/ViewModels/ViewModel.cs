﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace PFR_INVEST.FilesImportToDBUtil.ViewModels
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected internal void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        protected internal void OnPropertyChanged(Expression<Func<object>> propertyExpression)
        {
            string propertyName = RetrieveMemberName(propertyExpression);
            OnPropertyChanged(propertyName);
        }

        private static string RetrieveMemberName<T>(Expression<Func<T>> propertyExpression)
        {
            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression != null)
            {
                return memberExpression.Member.Name;
            }
            else
            {
                UnaryExpression unaryExpression = propertyExpression.Body as UnaryExpression;
                if (unaryExpression != null)
                {
                    memberExpression = unaryExpression.Operand as MemberExpression;
                    if (memberExpression != null)
                        return memberExpression.Member.Name;
                }
            }
            throw new ArgumentException("Invalid expression.", "propertyExpression");
        }
    }
}
