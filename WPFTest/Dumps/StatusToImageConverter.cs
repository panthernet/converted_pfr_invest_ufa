﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Helpers
{
    //[ValueConversion(typeof(DateTime), typeof(BitmapImage))]
   /* public class StatusToImageConverter : DependencyObject, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return new BitmapImage(new Uri("/Images/redlight.png", UriKind.Relative));
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value == null)
                return null;

            return value.ToString();
        }
    }

    public class DiscrepancyToImageConverter : DependencyObject, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            bool flag = false;

            decimal discrepancy = (decimal)value;
            if (discrepancy < 0)
                flag = true;

            BitmapImage imgr;
            if (flag)
                imgr = new BitmapImage(new Uri("/Images/greenlight.png", UriKind.Relative));
            else
                imgr = new BitmapImage(new Uri("/Images/redlight.png", UriKind.Relative));
            return imgr;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value == null)
                return null;

            return value.ToString();
        }
    }*/
}