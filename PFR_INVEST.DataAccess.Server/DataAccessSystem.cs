﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using NHibernate;
using System.Threading;
using NHibernate.Engine;
using Configuration = NHibernate.Cfg.Configuration;
using NHibernate.Metadata;
using NHibernate.Persister.Entity;
using NHibernate.Stat;
using PFR_INVEST.DataAccess.Server.Mock;

//НЕ ИСПОЛЬЗОВАТЬ LINQ - проблема сериализации


namespace PFR_INVEST.DataAccess.Server
{
    public partial class DataAccessSystem
    {
       // private const string DATA_SOURCE_NAME = "DataSourceName";
        public const string DATA_SCHEME = "PFR_BASIC";
        public const string DATA_JOURNAL_SCHEME = "PFR_AUDIT";
        
        private static readonly Mutex SessionMutex = new Mutex();

        private static ISessionFactory _mSessionFactory;

        private static bool IsMockDb = ConfigurationManager.AppSettings.AllKeys.Contains("MockDBConnection") && Convert.ToBoolean(ConfigurationManager.AppSettings["MockDBConnection"]);

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_mSessionFactory == null)
                {
                    try
                    {
                        SessionMutex.WaitOne();
                        var configuration = new Configuration();
                        var dbType = ConfigurationManager.AppSettings.AllKeys.ToList().Contains("DBType") ? Convert.ToString(ConfigurationManager.AppSettings["DBType"]) : "DB2";
                        string configFile;
                        string mapFolder;
                        string connStr;
                        switch (dbType)
                        {
#if WEBCLIENT
                            default:
                                configFile = "hibernate.postgre.cfg.xml";
                                mapFolder = "Database/Mapping/PostgreSQL/PFR_BASIC";
                                break;
#else
                            case "POSTGRES":
                                configFile = "hibernate.postgre.cfg.xml";
                                mapFolder = "Mapping/PostgreSQL/PFR_BASIC";
                                break;
                            default: //DB2
                                configFile = "hibernate.db2.cfg.xml";
                                mapFolder = "Mapping/DB2/PFR_BASIC";
                                break;
#endif
                        }
                        
                        configuration.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFile));
                        switch (dbType)
                        {
                            case "POSTGRES":
                                connStr = ConfigurationManager.AppSettings["DBConnectionString"];
                                break;
                            default: //DB2
                                connStr = ConfigurationManager.AppSettings["DBConnectionString"] + "CurrentSchema=" + configuration.Properties["default_schema"] + ";";
                                break;
                        }
                        configuration.Properties["connection.connection_string"] = connStr;

                        bool showSql;
                        if (ConfigurationManager.AppSettings["SqlQueriesTraceEnabled"] != null && bool.TryParse(ConfigurationManager.AppSettings["SqlQueriesTraceEnabled"], out showSql))
                            configuration.Properties["show_sql"] = showSql.ToString();
                        configuration.AddDirectory(new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, mapFolder)));
                        _mSessionFactory = IsMockDb ? null : configuration.BuildSessionFactory();
                    }
                    finally
                    {
                        SessionMutex.ReleaseMutex();
                    }
                }
                return _mSessionFactory;
            }
        }


        public static ISession OpenSession()
        {
            try
            {
                //Удаляем текущий кеш из треда. Он активно используеться при сериализации
                ThreadCacheManager.Instance.FreeCurrentThreadCache();

                return SessionFactory.OpenSession();
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                throw;
            }
        }

        public static IClassMetadata GetClassMetadata(string entityName)
        {
            try
            {
                return SessionFactory.GetClassMetadata(entityName);
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                throw;
            }
        }

        /// <summary>
        /// Возвращает таблицу для типа, если для того есть маппинг хибернейта
        /// </summary>
        /// <param name="entityClass">Тип сущности</param>
        public static string GetClassTableName(Type entityClass)
        {
            var meta = GetClassMetadataSafe(entityClass) as AbstractEntityPersister;
            if(meta == null || string.IsNullOrEmpty(meta.TableName)) throw new Exception("Не удалось вычислить название таблицы для типа "+ entityClass.Name);
            var value = meta.TableName;
            if (value.Contains("."))
                value = value.Split('.')[1]; //.Replace("\"","")
            return value;
        }

        public static IClassMetadata GetClassMetadata(Type entityClass)
        {
            try
            {
                return SessionFactory.GetClassMetadata(entityClass);
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                throw;
            }
        }

        public static IClassMetadata GetClassMetadataSafe(Type entityClass)
        {
            try
            {
                return SessionFactory.GetClassMetadata(entityClass);
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                return null;
            }
        }

        /// <summary>
        /// Возвращает метаданные класса по имени таблицы, основываясь на маппинге хибера
        /// </summary>
        /// <param name="tableName">Наименование таблицы</param>
        /// <param name="noHib">Не искать хиберовские классы с окончанием Hib</param>
        public static IClassMetadata GetClassMetadataByTable(string tableName, bool noHib = false)
        {
            //НЕ ИСПОЛЬЗОВАТЬ LINQ - проблема сериализации
            foreach (var a in SessionFactory.GetAllClassMetadata())
            {
                var value = a.Value as AbstractEntityPersister;
                if (value != null && value.TableName.Trim('"').EndsWith(tableName) && (!noHib || !value.Name.ToLower().EndsWith("hib")))
                    return a.Value;
            }
            return null;
        }


        public static IStatelessSession OpenStatelessSession()
        {
            try
            {
                //Удаляем текущий кеш из треда. Он активно используеться при сериализации
                ThreadCacheManager.Instance.FreeCurrentThreadCache();

                return SessionFactory.OpenStatelessSession();
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                throw;
            }
        }

#region Journal
       // public static string JournalConfigFile { get; set; }

        public static bool HasJournal
        {
            get
            {
                bool retVal = true;
                //retVal &= !string.IsNullOrEmpty(JournalConfigFile);
                try { var c = SessionFactoryJournal; }
                catch (Exception) { retVal = false; }
                return retVal;
            }
        }

        private static ISessionFactory _mSessionFactoryJournal;

        private static ISessionFactory SessionFactoryJournal
        {
            get
            {
                if (_mSessionFactoryJournal == null)
                {
                    var configuration = new Configuration();
                    var dbType = ConfigurationManager.AppSettings.AllKeys.ToList().Contains("DBType") ? Convert.ToString(ConfigurationManager.AppSettings["DBType"]) : "DB2";
                    string configFile;
                    string mapFolder;
                    string connStr;
                    switch (dbType)
                    {
#if WEBCLIENT
                        default:
                            configFile = "hibernate.postgre.journal.cfg.xml";
                                mapFolder = "Database/Mapping/PostgreSQL/AUDIT";
                            break;
#else
                        case "POSTGRES":
                            configFile = "hibernate.postgre.journal.cfg.xml";
                                mapFolder = "Mapping/PostgreSQL/AUDIT";
                            break;
                        default: //DB2
                            configFile = "hibernate.db2.journal.cfg.xml";
                                mapFolder = "Mapping/DB2/AUDIT";
                            break;
#endif
                    }
                    configuration.Configure(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFile));
                    switch (dbType)
                    {
                        case "POSTGRES":
                            connStr = ConfigurationManager.AppSettings["DBConnectionString"];
                            break;
                        default: //DB2
                            connStr = ConfigurationManager.AppSettings["DBConnectionString"] + "CurrentSchema=" + configuration.Properties["default_schema"] + ";";
                            break;
                    }
                    configuration.Properties["connection.connection_string"] = connStr;
                    configuration.AddDirectory(new DirectoryInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, mapFolder)));
                    _mSessionFactoryJournal = IsMockDb ? null : configuration.BuildSessionFactory();
                }
                return _mSessionFactoryJournal;
            }
        }

        public static ISession OpenSessionJournal()
        {
            try
            {
                //Удаляем текущий кеш из треда. Он активно используеться при сериализации
                ThreadCacheManager.Instance.FreeCurrentThreadCache();
                return SessionFactoryJournal.OpenSession();
            }
            catch (Exception ex)
            {
                if (LogException != null)
                    LogException(ex);
                throw;
            }
        }
#endregion

        //Переделать на интерфейс
        public delegate void LogExceptionDelegate(Exception ex);
        public static LogExceptionDelegate LogException;
    }
}
