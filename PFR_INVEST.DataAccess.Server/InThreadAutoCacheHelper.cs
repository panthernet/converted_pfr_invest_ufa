
using PFR_INVEST.DataAccess.Server;

namespace Common.Caching
{
    public class InThreadAutoCacheHelper
    {

        private static string GetAutoCacheKey(string p_sCacheSlot)
        {
            return "ACH_" + p_sCacheSlot;
        }


        public delegate object InThreadAutoCacheObjectDelegate();
        public static TObjectType InThreadAutoCacheObject<TObjectType, TParameterType>(string p_sAutoCacheSlot, TParameterType p_oParameter, InThreadAutoCacheObjectDelegate p_oValueIfNotCachedDelegate)
        {
            
            string sKey = GetAutoCacheKey(p_sAutoCacheSlot);

            ThreadCache oCache = ThreadCacheManager.Instance.GetCurrentThreadCache();
            ThreadCacheSlot oValueSlot = oCache.GetCacheSlot(sKey, p_oParameter);
            TObjectType oRes;
            lock (oValueSlot.SyncRoot)
            {
                if (oValueSlot.AlreadyRequested)
                {
                    if (oValueSlot.Value != null)
                        oRes = (TObjectType)(oValueSlot.Value);
                    else
                        oRes = default(TObjectType);
                }
                else
                {
                    oRes = (TObjectType)(p_oValueIfNotCachedDelegate());
                    oValueSlot.Value = oRes;
                    oValueSlot.AlreadyRequested = true;
                }
            }

            return oRes;
        }


        public static void ClearCacheSlot(string p_sAutoCacheSlot)
        {
            string sKey = GetAutoCacheKey(p_sAutoCacheSlot);            
            ThreadCache oCache = ThreadCacheManager.Instance.GetCurrentThreadCache();
            oCache.ClearNamedCacheDic(sKey);
            
        }
    }
}
