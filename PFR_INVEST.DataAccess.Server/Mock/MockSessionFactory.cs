﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Engine;
using NHibernate.Impl;
using NHibernate.Mapping;
using NHibernate.Metadata;
using NHibernate.SqlCommand;
using NHibernate.Stat;
using NHibernate.Transaction;
using NHibernate.Transform;
using NHibernate.Type;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.Mock
{
    /*
#if !WEBCLIENT
    public class MockDbCommand : IDbCommand
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>Creates a prepared (or compiled) version of the command on the data source.</summary>
        /// <exception cref="T:System.InvalidOperationException">The <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> is not set.-or- The <see cref="P:System.Data.OleDb.OleDbCommand.Connection" /> is not <see cref="M:System.Data.OleDb.OleDbConnection.Open" />. </exception>
        public void Prepare()
        {
        }

        /// <summary>Attempts to cancels the execution of an <see cref="T:System.Data.IDbCommand" />.</summary>
        public void Cancel()
        {
        }

        /// <summary>Creates a new instance of an <see cref="T:System.Data.IDbDataParameter" /> object.</summary>
        /// <returns>An IDbDataParameter object.</returns>
        public IDbDataParameter CreateParameter()
        {
            return new OdbcParameter();
        }

        /// <summary>Executes an SQL statement against the Connection object of a .NET Framework data provider, and returns the number of rows affected.</summary>
        /// <returns>The number of rows affected.</returns>
        /// <exception cref="T:System.InvalidOperationException">The connection does not exist.-or- The connection is not open. </exception>
        public int ExecuteNonQuery()
        {
            return 0;
        }

        /// <summary>Executes the <see cref="P:System.Data.IDbCommand.CommandText" /> against the <see cref="P:System.Data.IDbCommand.Connection" /> and builds an <see cref="T:System.Data.IDataReader" />.</summary>
        /// <returns>An <see cref="T:System.Data.IDataReader" /> object.</returns>
        public IDataReader ExecuteReader()
        {
            return new DataTableReader(new DataTable());

        }

        /// <summary>Executes the <see cref="P:System.Data.IDbCommand.CommandText" /> against the <see cref="P:System.Data.IDbCommand.Connection" />, and builds an <see cref="T:System.Data.IDataReader" /> using one of the <see cref="T:System.Data.CommandBehavior" /> values.</summary>
        /// <returns>An <see cref="T:System.Data.IDataReader" /> object.</returns>
        /// <param name="behavior">One of the <see cref="T:System.Data.CommandBehavior" /> values. </param>
        public IDataReader ExecuteReader(CommandBehavior behavior)
        {
            return new DataTableReader(new DataTable());
        }

        /// <summary>Executes the query, and returns the first column of the first row in the resultset returned by the query. Extra columns or rows are ignored.</summary>
        /// <returns>The first column of the first row in the resultset.</returns>
        public object ExecuteScalar()
        {
            return 1L;
        }

        /// <summary>Gets or sets the <see cref="T:System.Data.IDbConnection" /> used by this instance of the <see cref="T:System.Data.IDbCommand" />.</summary>
        /// <returns>The connection to the data source.</returns>
        public IDbConnection Connection { get; set; } = new MockDbConnection();

        /// <summary>Gets or sets the transaction within which the Command object of a .NET Framework data provider executes.</summary>
        /// <returns>the Command object of a .NET Framework data provider executes. The default value is null.</returns>
        public IDbTransaction Transaction { get; set; } = new MockDbTransaction();

        /// <summary>Gets or sets the text command to run against the data source.</summary>
        /// <returns>The text command to execute. The default value is an empty string ("").</returns>
        public string CommandText { get; set; } = "";

        /// <summary>Gets or sets the wait time before terminating the attempt to execute a command and generating an error.</summary>
        /// <returns>The time (in seconds) to wait for the command to execute. The default value is 30 seconds.</returns>
        /// <exception cref="T:System.ArgumentException">The property value assigned is less than 0. </exception>
        public int CommandTimeout { get; set; }

        /// <summary>Indicates or specifies how the <see cref="P:System.Data.IDbCommand.CommandText" /> property is interpreted.</summary>
        /// <returns>One of the <see cref="T:System.Data.CommandType" /> values. The default is Text.</returns>
        public CommandType CommandType { get; set; } = CommandType.Text;

        /// <summary>Gets the <see cref="T:System.Data.IDataParameterCollection" />.</summary>
        /// <returns>The parameters of the SQL statement or stored procedure.</returns>
        public IDataParameterCollection Parameters { get; }

        /// <summary>Gets or sets how command results are applied to the <see cref="T:System.Data.DataRow" /> when used by the <see cref="M:System.Data.IDataAdapter.Update(System.Data.DataSet)" /> method of a <see cref="T:System.Data.Common.DbDataAdapter" />.</summary>
        /// <returns>One of the <see cref="T:System.Data.UpdateRowSource" /> values. The default is Both unless the command is automatically generated. Then the default is None.</returns>
        /// <exception cref="T:System.ArgumentException">The value entered was not one of the <see cref="T:System.Data.UpdateRowSource" /> values. </exception>
        public UpdateRowSource UpdatedRowSource { get; set; }
    }

    public class MockDbTransaction : IDbTransaction
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>Commits the database transaction.</summary>
        /// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
        /// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
        public void Commit()
        {
        }

        /// <summary>Rolls back a transaction from a pending state.</summary>
        /// <exception cref="T:System.Exception">An error occurred while trying to commit the transaction. </exception>
        /// <exception cref="T:System.InvalidOperationException">The transaction has already been committed or rolled back.-or- The connection is broken. </exception>
        public void Rollback()
        {
        }

        /// <summary>Specifies the Connection object to associate with the transaction.</summary>
        /// <returns>The Connection object to associate with the transaction.</returns>
        public IDbConnection Connection { get; } = new MockDbConnection();

        /// <summary>Specifies the <see cref="T:System.Data.IsolationLevel" /> for this transaction.</summary>
        /// <returns>The <see cref="T:System.Data.IsolationLevel" /> for this transaction. The default is ReadCommitted.</returns>
        public IsolationLevel IsolationLevel { get; }
    }

    public class MockDbConnection : IDbConnection
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>Begins a database transaction.</summary>
        /// <returns>An object representing the new transaction.</returns>
        public IDbTransaction BeginTransaction()
        {
            return new MockDbTransaction();
        }

        /// <summary>Begins a database transaction with the specified <see cref="T:System.Data.IsolationLevel" /> value.</summary>
        /// <returns>An object representing the new transaction.</returns>
        /// <param name="il">One of the <see cref="T:System.Data.IsolationLevel" /> values. </param>
        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            return new MockDbTransaction();

        }

        /// <summary>Closes the connection to the database.</summary>
        public void Close()
        {
        }

        /// <summary>Changes the current database for an open Connection object.</summary>
        /// <param name="databaseName">The name of the database to use in place of the current database. </param>
        public void ChangeDatabase(string databaseName)
        {
        }

        /// <summary>Creates and returns a Command object associated with the connection.</summary>
        /// <returns>A Command object associated with the connection.</returns>
        public IDbCommand CreateCommand()
        {
            return new MockDbCommand();
        }

        /// <summary>Opens a database connection with the settings specified by the ConnectionString property of the provider-specific Connection object.</summary>
        public void Open()
        {
        }

        /// <summary>Gets or sets the string used to open a database.</summary>
        /// <returns>A string containing connection settings.</returns>
        public string ConnectionString { get; set; }

        /// <summary>Gets the time to wait while trying to establish a connection before terminating the attempt and generating an error.</summary>
        /// <returns>The time (in seconds) to wait for a connection to open. The default value is 15 seconds.</returns>
        public int ConnectionTimeout { get; }

        /// <summary>Gets the name of the current database or the database to be used after a connection is opened.</summary>
        /// <returns>The name of the current database or the name of the database to be used once a connection is open. The default value is an empty string.</returns>
        public string Database { get; }

        /// <summary>Gets the current state of the connection.</summary>
        /// <returns>One of the <see cref="T:System.Data.ConnectionState" /> values.</returns>
        public ConnectionState State { get; } = ConnectionState.Open;
    }

    public class MockSession : ISession
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Force the <c>ISession</c> to flush.
        /// </summary>
        /// <remarks>
        /// Must be called at the end of a unit of work, before commiting the transaction and closing
        /// the session (<c>Transaction.Commit()</c> calls this method). <i>Flushing</i> if the process
        /// of synchronising the underlying persistent store with persistable state held in memory.
        /// </remarks>
        public void Flush()
        {
        }

        /// <summary>
        /// Disconnect the <c>ISession</c> from the current ADO.NET connection.
        /// </summary>
        /// <remarks>
        /// If the connection was obtained by Hibernate, close it or return it to the connection
        /// pool. Otherwise return it to the application. This is used by applications which require
        /// long transactions.
        /// </remarks>
        /// <returns>The connection provided by the application or <see langword="null" /></returns>
        public IDbConnection Disconnect()
        {
            return new MockDbConnection();
        }

        /// <summary>Obtain a new ADO.NET connection.</summary>
        /// <remarks>
        /// This is used by applications which require long transactions
        /// </remarks>
        public void Reconnect()
        {
        }

        /// <summary>Reconnect to the given ADO.NET connection.</summary>
        /// <remarks>This is used by applications which require long transactions</remarks>
        /// <param name="connection">An ADO.NET connection</param>
        public void Reconnect(IDbConnection connection)
        {
        }

        /// <summary>
        /// End the <c>ISession</c> by disconnecting from the ADO.NET connection and cleaning up.
        /// </summary>
        /// <remarks>
        /// It is not strictly necessary to <c>Close()</c> the <c>ISession</c> but you must
        /// at least <c>Disconnect()</c> it.
        /// </remarks>
        /// <returns>The connection provided by the application or <see langword="null" /></returns>
        public IDbConnection Close()
        {
            return new MockDbConnection();
        }

        /// <summary>Cancel execution of the current query.</summary>
        /// <remarks>
        /// May be called from one thread to stop execution of a query in another thread.
        /// Use with care!
        /// </remarks>
        public void CancelQuery()
        {
        }

        /// <summary>
        /// Does this <c>ISession</c> contain any changes which must be
        /// synchronized with the database? Would any SQL be executed if
        /// we flushed this session?
        /// </summary>
        public bool IsDirty()
        {
            return false;
        }

        /// <summary>
        /// Return the identifier of an entity instance cached by the <c>ISession</c>
        /// </summary>
        /// <remarks>
        /// Throws an exception if the instance is transient or associated with a different
        /// <c>ISession</c>
        /// </remarks>
        /// <param name="obj">a persistent instance</param>
        /// <returns>the identifier</returns>
        public object GetIdentifier(object obj)
        {
            return 1L;
        }

        /// <summary>Is this instance associated with this Session?</summary>
        /// <param name="obj">an instance of a persistent class</param>
        /// <returns>true if the given instance is associated with this Session</returns>
        public bool Contains(object obj)
        {
            return false;
        }

        /// <summary>Remove this instance from the session cache.</summary>
        /// <remarks>
        /// Changes to the instance will not be synchronized with the database.
        /// This operation cascades to associated instances if the association is mapped
        /// with <c>cascade="all"</c> or <c>cascade="all-delete-orphan"</c>.
        /// </remarks>
        /// <param name="obj">a persistent instance</param>
        public void Evict(object obj)
        {
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier,
        /// obtaining the specified lock mode.
        /// </summary>
        /// <param name="theType">A persistent class</param>
        /// <param name="id">A valid identifier of an existing persistent instance of the class</param>
        /// <param name="lockMode">The lock level</param>
        /// <returns>the persistent instance</returns>
        public object Load(Type theType, object id, LockMode lockMode)
        {
            return Activator.CreateInstance(theType);
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier,
        /// obtaining the specified lock mode, assuming the instance exists.
        /// </summary>
        /// <param name="entityName">The entity-name of a persistent class</param>
        /// <param name="id">a valid identifier of an existing persistent instance of the class </param>
        /// <param name="lockMode">the lock level </param>
        /// <returns> the persistent instance or proxy </returns>
        public object Load(string entityName, object id, LockMode lockMode)
        {
            return null;
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier,
        /// assuming that the instance exists.
        /// </summary>
        /// <remarks>
        /// You should not use this method to determine if an instance exists (use a query or
        /// <see cref="M:NHibernate.ISession.Get(System.Type,System.Object)" /> instead). Use this only to retrieve an instance
        /// that you assume exists, where non-existence would be an actual error.
        /// </remarks>
        /// <param name="theType">A persistent class</param>
        /// <param name="id">A valid identifier of an existing persistent instance of the class</param>
        /// <returns>The persistent instance or proxy</returns>
        public object Load(Type theType, object id)
        {
            return Activator.CreateInstance(theType);

        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier,
        /// obtaining the specified lock mode.
        /// </summary>
        /// <typeparam name="T">A persistent class</typeparam>
        /// <param name="id">A valid identifier of an existing persistent instance of the class</param>
        /// <param name="lockMode">The lock level</param>
        /// <returns>the persistent instance</returns>
        public T Load<T>(object id, LockMode lockMode)
        {
            return (T)Activator.CreateInstance(typeof(T));
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier,
        /// assuming that the instance exists.
        /// </summary>
        /// <remarks>
        /// You should not use this method to determine if an instance exists (use a query or
        /// <see cref="M:NHibernate.ISession.Get``1(System.Object)" /> instead). Use this only to retrieve an instance that you
        /// assume exists, where non-existence would be an actual error.
        /// </remarks>
        /// <typeparam name="T">A persistent class</typeparam>
        /// <param name="id">A valid identifier of an existing persistent instance of the class</param>
        /// <returns>The persistent instance or proxy</returns>
        public T Load<T>(object id)
        {
            return (T)Activator.CreateInstance(typeof(T));
        }

        /// <summary>
        /// Return the persistent instance of the given <paramref name="entityName" /> with the given identifier,
        /// assuming that the instance exists.
        /// </summary>
        /// <param name="entityName">The entity-name of a persistent class</param>
        /// <param name="id">a valid identifier of an existing persistent instance of the class </param>
        /// <returns> The persistent instance or proxy </returns>
        /// <remarks>
        /// You should not use this method to determine if an instance exists (use <see cref="M:NHibernate.ISession.Get(System.String,System.Object)" />
        /// instead). Use this only to retrieve an instance that you assume exists, where non-existence
        /// would be an actual error.
        /// </remarks>
        public object Load(string entityName, object id)
        {
            return null;
        }

        /// <summary>
        /// Read the persistent state associated with the given identifier into the given transient
        /// instance.
        /// </summary>
        /// <param name="obj">An "empty" instance of the persistent class</param>
        /// <param name="id">A valid identifier of an existing persistent instance of the class</param>
        public void Load(object obj, object id)
        {
        }

        /// <summary>
        /// Persist all reachable transient objects, reusing the current identifier
        /// values. Note that this will not trigger the Interceptor of the Session.
        /// </summary>
        /// <param name="obj">a detached instance of a persistent class</param>
        /// <param name="replicationMode"></param>
        public void Replicate(object obj, ReplicationMode replicationMode)
        {
        }

        /// <summary>
        /// Persist the state of the given detached instance, reusing the current
        /// identifier value.  This operation cascades to associated instances if
        /// the association is mapped with <tt>cascade="replicate"</tt>.
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="obj">a detached instance of a persistent class </param>
        /// <param name="replicationMode"></param>
        public void Replicate(string entityName, object obj, ReplicationMode replicationMode)
        {
        }

        /// <summary>
        /// Persist the given transient instance, first assigning a generated identifier.
        /// </summary>
        /// <remarks>
        /// Save will use the current value of the identifier property if the <c>Assigned</c>
        /// generator is used.
        /// </remarks>
        /// <param name="obj">A transient instance of a persistent class</param>
        /// <returns>The generated identifier</returns>
        public object Save(object obj)
        {
            return obj;
        }

        /// <summary>
        /// Persist the given transient instance, using the given identifier.
        /// </summary>
        /// <param name="obj">A transient instance of a persistent class</param>
        /// <param name="id">An unused valid identifier</param>
        public void Save(object obj, object id)
        {
        }

        /// <summary>
        /// Persist the given transient instance, first assigning a generated identifier. (Or
        /// using the current value of the identifier property if the <tt>assigned</tt>
        /// generator is used.)
        /// </summary>
        /// <param name="entityName">The Entity name.</param>
        /// <param name="obj">a transient instance of a persistent class </param>
        /// <returns> the generated identifier </returns>
        /// <remarks>
        /// This operation cascades to associated instances if the
        /// association is mapped with <tt>cascade="save-update"</tt>.
        /// </remarks>
        public object Save(string entityName, object obj)
        {
            return obj;
        }

        /// <summary>
        /// Either <c>Save()</c> or <c>Update()</c> the given instance, depending upon the value of
        /// its identifier property.
        /// </summary>
        /// <remarks>
        /// By default the instance is always saved. This behaviour may be adjusted by specifying
        /// an <c>unsaved-value</c> attribute of the identifier property mapping
        /// </remarks>
        /// <param name="obj">A transient instance containing new or updated state</param>
        public void SaveOrUpdate(object obj)
        {
        }

        /// <summary>
        /// Either <see cref="M:NHibernate.ISession.Save(System.String,System.Object)" /> or <see cref="M:NHibernate.ISession.Update(System.String,System.Object)" />
        /// the given instance, depending upon resolution of the unsaved-value checks
        /// (see the manual for discussion of unsaved-value checking).
        /// </summary>
        /// <param name="entityName">The name of the entity </param>
        /// <param name="obj">a transient or detached instance containing new or updated state </param>
        /// <seealso cref="M:NHibernate.ISession.Save(System.String,System.Object)" />
        /// <seealso cref="M:NHibernate.ISession.Update(System.String,System.Object)" />
        /// <remarks>
        /// This operation cascades to associated instances if the association is mapped
        /// with <tt>cascade="save-update"</tt>.
        /// </remarks>
        public void SaveOrUpdate(string entityName, object obj)
        {
        }

        /// <summary>
        /// Update the persistent instance with the identifier of the given transient instance.
        /// </summary>
        /// <remarks>
        /// If there is a persistent instance with the same identifier, an exception is thrown. If
        /// the given transient instance has a <see langword="null" /> identifier, an exception will be thrown.
        /// </remarks>
        /// <param name="obj">A transient instance containing updated state</param>
        public void Update(object obj)
        {
        }

        /// <summary>
        /// Update the persistent state associated with the given identifier.
        /// </summary>
        /// <remarks>
        /// An exception is thrown if there is a persistent instance with the same identifier
        /// in the current session.
        /// </remarks>
        /// <param name="obj">A transient instance containing updated state</param>
        /// <param name="id">Identifier of persistent instance</param>
        public void Update(object obj, object id)
        {
        }

        /// <summary>
        /// Update the persistent instance with the identifier of the given detached
        /// instance.
        /// </summary>
        /// <param name="entityName">The Entity name.</param>
        /// <param name="obj">a detached instance containing updated state </param>
        /// <remarks>
        /// If there is a persistent instance with the same identifier,
        /// an exception is thrown. This operation cascades to associated instances
        /// if the association is mapped with <tt>cascade="save-update"</tt>.
        /// </remarks>
        public void Update(string entityName, object obj)
        {
        }

        /// <summary>
        /// Copy the state of the given object onto the persistent object with the same
        /// identifier. If there is no persistent instance currently associated with
        /// the session, it will be loaded. Return the persistent instance. If the
        /// given instance is unsaved, save a copy of and return it as a newly persistent
        /// instance. The given instance does not become associated with the session.
        /// This operation cascades to associated instances if the association is mapped
        /// with <tt>cascade="merge"</tt>.<br />
        /// The semantics of this method are defined by JSR-220.
        /// </summary>
        /// <param name="obj">a detached instance with state to be copied </param>
        /// <returns> an updated persistent instance </returns>
        public object Merge(object obj)
        {
            return obj;
        }

        /// <summary>
        /// Copy the state of the given object onto the persistent object with the same
        /// identifier. If there is no persistent instance currently associated with
        /// the session, it will be loaded. Return the persistent instance. If the
        /// given instance is unsaved, save a copy of and return it as a newly persistent
        /// instance. The given instance does not become associated with the session.
        /// This operation cascades to associated instances if the association is mapped
        /// with <tt>cascade="merge"</tt>.<br />
        /// The semantics of this method are defined by JSR-220.
        /// <param name="entityName">Name of the entity.</param>
        /// <param name="obj">a detached instance with state to be copied </param>
        /// 	<returns> an updated persistent instance </returns>
        /// </summary>
        /// <returns></returns>
        public object Merge(string entityName, object obj)
        {
            return obj;
        }

        /// <summary>
        /// Make a transient instance persistent. This operation cascades to associated
        /// instances if the association is mapped with <tt>cascade="persist"</tt>.<br />
        /// The semantics of this method are defined by JSR-220.
        /// </summary>
        /// <param name="obj">a transient instance to be made persistent </param>
        public void Persist(object obj)
        {
        }

        /// <summary>
        /// Make a transient instance persistent. This operation cascades to associated
        /// instances if the association is mapped with <tt>cascade="persist"</tt>.<br />
        /// The semantics of this method are defined by JSR-220.
        /// </summary>
        /// <param name="entityName">Name of the entity.</param>
        /// <param name="obj">a transient instance to be made persistent</param>
        public void Persist(string entityName, object obj)
        {
        }

        /// <summary>
        /// Copy the state of the given object onto the persistent object with the same
        /// identifier. If there is no persistent instance currently associated with
        /// the session, it will be loaded. Return the persistent instance. If the
        /// given instance is unsaved or does not exist in the database, save it and
        /// return it as a newly persistent instance. Otherwise, the given instance
        /// does not become associated with the session.
        /// </summary>
        /// <param name="obj">a transient instance with state to be copied</param>
        /// <returns>an updated persistent instance</returns>
        public object SaveOrUpdateCopy(object obj)
        {
            return obj;
        }

        /// <summary>
        /// Copy the state of the given object onto the persistent object with the
        /// given identifier. If there is no persistent instance currently associated
        /// with the session, it will be loaded. Return the persistent instance. If
        /// there is no database row with the given identifier, save the given instance
        /// and return it as a newly persistent instance. Otherwise, the given instance
        /// does not become associated with the session.
        /// </summary>
        /// <param name="obj">a persistent or transient instance with state to be copied</param>
        /// <param name="id">the identifier of the instance to copy to</param>
        /// <returns>an updated persistent instance</returns>
        public object SaveOrUpdateCopy(object obj, object id)
        {
            return obj;
        }

        /// <summary>Remove a persistent instance from the datastore.</summary>
        /// <remarks>
        /// The argument may be an instance associated with the receiving <c>ISession</c> or a
        /// transient instance with an identifier associated with existing persistent state.
        /// </remarks>
        /// <param name="obj">The instance to be removed</param>
        public void Delete(object obj)
        { 
        }

        /// <summary>
        /// Remove a persistent instance from the datastore. The <b>object</b> argument may be
        /// an instance associated with the receiving <see cref="T:NHibernate.ISession" /> or a transient
        /// instance with an identifier associated with existing persistent state.
        /// This operation cascades to associated instances if the association is mapped
        /// with <tt>cascade="delete"</tt>.
        /// </summary>
        /// <param name="entityName">The entity name for the instance to be removed. </param>
        /// <param name="obj">the instance to be removed </param>
        public void Delete(string entityName, object obj)
        {
        }

        /// <summary>Delete all objects returned by the query.</summary>
        /// <param name="query">The query string</param>
        /// <returns>Returns the number of objects deleted.</returns>
        public int Delete(string query)
        {
            return 1;
        }

        /// <summary>Delete all objects returned by the query.</summary>
        /// <param name="query">The query string</param>
        /// <param name="value">A value to be written to a "?" placeholer in the query</param>
        /// <param name="type">The hibernate type of value.</param>
        /// <returns>The number of instances deleted</returns>
        public int Delete(string query, object value, IType type)
        {
            return 1;
        }

        /// <summary>Delete all objects returned by the query.</summary>
        /// <param name="query">The query string</param>
        /// <param name="values">A list of values to be written to "?" placeholders in the query</param>
        /// <param name="types">A list of Hibernate types of the values</param>
        /// <returns>The number of instances deleted</returns>
        public int Delete(string query, object[] values, IType[] types)
        {
            return 1;
        }

        /// <summary>
        /// Obtain the specified lock level upon the given object.
        /// </summary>
        /// <param name="obj">A persistent instance</param>
        /// <param name="lockMode">The lock level</param>
        public void Lock(object obj, LockMode lockMode)
        {
        }

        /// <summary>
        /// Obtain the specified lock level upon the given object.
        /// </summary>
        /// <param name="entityName">The Entity name.</param>
        /// <param name="obj">a persistent or transient instance </param>
        /// <param name="lockMode">the lock level </param>
        /// <remarks>
        /// This may be used to perform a version check (<see cref="F:NHibernate.LockMode.Read" />), to upgrade to a pessimistic
        /// lock (<see cref="F:NHibernate.LockMode.Upgrade" />), or to simply reassociate a transient instance
        /// with a session (<see cref="F:NHibernate.LockMode.None" />). This operation cascades to associated
        /// instances if the association is mapped with <tt>cascade="lock"</tt>.
        /// </remarks>
        public void Lock(string entityName, object obj, LockMode lockMode)
        {
        }

        /// <summary>
        /// Re-read the state of the given instance from the underlying database.
        /// </summary>
        /// <remarks>
        /// <para>
        /// It is inadvisable to use this to implement long-running sessions that span many
        /// business tasks. This method is, however, useful in certain special circumstances.
        /// </para>
        /// <para>
        /// For example,
        /// <list>
        /// 	<item>Where a database trigger alters the object state upon insert or update</item>
        /// 	<item>After executing direct SQL (eg. a mass update) in the same session</item>
        /// 	<item>After inserting a <c>Blob</c> or <c>Clob</c></item>
        /// </list>
        /// </para>
        /// </remarks>
        /// <param name="obj">A persistent instance</param>
        public void Refresh(object obj)
        {
        }

        /// <summary>
        /// Re-read the state of the given instance from the underlying database, with
        /// the given <c>LockMode</c>.
        /// </summary>
        /// <remarks>
        /// It is inadvisable to use this to implement long-running sessions that span many
        /// business tasks. This method is, however, useful in certain special circumstances.
        /// </remarks>
        /// <param name="obj">a persistent or transient instance</param>
        /// <param name="lockMode">the lock mode to use</param>
        public void Refresh(object obj, LockMode lockMode)
        {
        }

        /// <summary>Determine the current lock mode of the given object</summary>
        /// <param name="obj">A persistent instance</param>
        /// <returns>The current lock mode</returns>
        public LockMode GetCurrentLockMode(object obj)
        {
            return LockMode.None;
        }

        /// <summary>
        /// Begin a unit of work and return the associated <c>ITransaction</c> object.
        /// </summary>
        /// <remarks>
        /// If a new underlying transaction is required, begin the transaction. Otherwise
        /// continue the new work in the context of the existing underlying transaction.
        /// The class of the returned <see cref="T:NHibernate.ITransaction" /> object is determined by
        /// the property <c>transaction_factory</c>
        /// </remarks>
        /// <returns>A transaction instance</returns>
        public ITransaction BeginTransaction()
        {
            return new MockTransaction();
        }

        /// <summary>
        /// Begin a transaction with the specified <c>isolationLevel</c>
        /// </summary>
        /// <param name="isolationLevel">Isolation level for the new transaction</param>
        /// <returns>A transaction instance having the specified isolation level</returns>
        public ITransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            return new MockTransaction();

        }

        /// <summary>
        /// Creates a new <c>Criteria</c> for the entity class.
        /// </summary>
        /// <typeparam name="T">The entity class</typeparam>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria<T>() where T : class
        {
            return new MockCriteria();
        }

        /// <summary>
        /// Creates a new <c>Criteria</c> for the entity class with a specific alias
        /// </summary>
        /// <typeparam name="T">The entity class</typeparam>
        /// <param name="alias">The alias of the entity</param>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria<T>(string alias) where T : class
        {
            return new MockCriteria();

        }

        /// <summary>
        /// Creates a new <c>Criteria</c> for the entity class.
        /// </summary>
        /// <param name="persistentClass">The class to Query</param>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria(Type persistentClass)
        {
            return new MockCriteria();

        }

        /// <summary>
        /// Creates a new <c>Criteria</c> for the entity class with a specific alias
        /// </summary>
        /// <param name="persistentClass">The class to Query</param>
        /// <param name="alias">The alias of the entity</param>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria(Type persistentClass, string alias)
        {
            return new MockCriteria();

        }

        /// <summary>
        /// Create a new <c>Criteria</c> instance, for the given entity name.
        /// </summary>
        /// <param name="entityName">The name of the entity to Query</param>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria(string entityName)
        {
            return new MockCriteria();

        }

        /// <summary>
        /// Create a new <c>Criteria</c> instance, for the given entity name,
        /// with the given alias.
        /// </summary>
        /// <param name="entityName">The name of the entity to Query</param>
        /// <param name="alias">The alias of the entity</param>
        /// <returns>An ICriteria object</returns>
        public ICriteria CreateCriteria(string entityName, string alias)
        {
            return new MockCriteria();

        }

        /// <summary>
        /// Creates a new <c>IQueryOver&lt;T&gt;</c> for the entity class.
        /// </summary>
        /// <typeparam name="T">The entity class</typeparam>
        /// <returns>An ICriteria&lt;T&gt; object</returns>
        public IQueryOver<T, T> QueryOver<T>() where T : class
        {
            return null;
        }

#if WEBCLIENT
        public IQueryOver<T, T> QueryOver<T>(System.Linq.Expressions.Expression alias) where T : class
        {
            return null;
        }
#endif

        /// <summary>
        /// Creates a new <c>IQueryOver&lt;T&gt;</c> for the entity class.
        /// </summary>
        /// <typeparam name="T">The entity class</typeparam>
        /// <returns>An ICriteria&lt;T&gt; object</returns>
        public IQueryOver<T, T> QueryOver<T>(Expression<Func<T>> alias) where T : class
        {
            return null;

        }

        /// <summary>
        /// Create a new instance of <c>Query</c> for the given query string
        /// </summary>
        /// <param name="queryString">A hibernate query string</param>
        /// <returns>The query</returns>
        public IQuery CreateQuery(string queryString)
        {
            return new MockSqlQery();
        }

        /// <summary>
        /// Create a new instance of <c>Query</c> for the given query expression
        /// <param name="queryExpression">A hibernate query expression</param>
        /// <returns>The query</returns>
        /// </summary>
        public IQuery CreateQuery(IQueryExpression queryExpression)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Create a new instance of <c>Query</c> for the given collection and filter string
        /// </summary>
        /// <param name="collection">A persistent collection</param>
        /// <param name="queryString">A hibernate query</param>
        /// <returns>A query</returns>
        public IQuery CreateFilter(object collection, string queryString)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Obtain an instance of <see cref="T:NHibernate.IQuery" /> for a named query string defined in the
        /// mapping file.
        /// </summary>
        /// <param name="queryName">The name of a query defined externally.</param>
        /// <returns>An <see cref="T:NHibernate.IQuery" /> from a named query string.</returns>
        /// <remarks>
        /// The query can be either in <c>HQL</c> or <c>SQL</c> format.
        /// </remarks>
        public IQuery GetNamedQuery(string queryName)
        {
            return new MockSqlQery();
        }

        /// <summary>
        /// Create a new instance of <see cref="T:NHibernate.ISQLQuery" /> for the given SQL query string.
        /// </summary>
        /// <param name="queryString">a query expressed in SQL</param>
        /// <returns>An <see cref="T:NHibernate.ISQLQuery" /> from the SQL string</returns>
        public ISQLQuery CreateSQLQuery(string queryString)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Completely clear the session. Evict all loaded instances and cancel all pending
        /// saves, updates and deletions. Do not close open enumerables or instances of
        /// <c>ScrollableResults</c>.
        /// </summary>
        public void Clear()
        {
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier, or null
        /// if there is no such persistent instance. (If the instance, or a proxy for the instance, is
        /// already associated with the session, return that instance or proxy.)
        /// </summary>
        /// <param name="clazz">a persistent class</param>
        /// <param name="id">an identifier</param>
        /// <returns>a persistent instance or null</returns>
        public object Get(Type clazz, object id)
        {
            return null;
        }

        /// <summary>
        /// Return the persistent instance of the given entity class with the given identifier, or null
        /// if there is no such persistent instance. Obtain the specified lock mode if the instance
        /// exists.
        /// </summary>
        /// <param name="clazz">a persistent class</param>
        /// <param name="id">an identifier</param>
        /// <param name="lockMode">the lock mode</param>
        /// <returns>a persistent instance or null</returns>
        public object Get(Type clazz, object id, LockMode lockMode)
        {
            return null;

        }

        /// <summary>
        /// Return the persistent instance of the given named entity with the given identifier,
        /// or null if there is no such persistent instance. (If the instance, or a proxy for the
        /// instance, is already associated with the session, return that instance or proxy.)
        /// </summary>
        /// <param name="entityName">the entity name </param>
        /// <param name="id">an identifier </param>
        /// <returns> a persistent instance or null </returns>
        public object Get(string entityName, object id)
        {
            return null;

        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.ISession.Get(System.Type,System.Object)" />
        /// </summary>
        public T Get<T>(object id)
        {
            return default(T);

        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.ISession.Get(System.Type,System.Object,NHibernate.LockMode)" />
        /// </summary>
        public T Get<T>(object id, LockMode lockMode)
        {
            return default(T);

        }

        /// <summary>Return the entity name for a persistent entity</summary>
        /// <param name="obj">a persistent entity</param>
        /// <returns> the entity name </returns>
        public string GetEntityName(object obj)
        {
            return "";
        }

        /// <summary>Enable the named filter for this current session.</summary>
        /// <param name="filterName">The name of the filter to be enabled.</param>
        /// <returns>The Filter instance representing the enabled filter.</returns>
        public IFilter EnableFilter(string filterName)
        {
            return null;
        }

        /// <summary>Retrieve a currently enabled filter by name.</summary>
        /// <param name="filterName">The name of the filter to be retrieved.</param>
        /// <returns>The Filter instance representing the enabled filter.</returns>
        public IFilter GetEnabledFilter(string filterName)
        {
            return null;

        }

        /// <summary>Disable the named filter for the current session.</summary>
        /// <param name="filterName">The name of the filter to be disabled.</param>
        public void DisableFilter(string filterName)
        {
        }

        /// <summary>
        /// Create a multi query, a query that can send several
        /// queries to the server, and return all their results in a single
        /// call.
        /// </summary>
        /// <returns>
        /// An <see cref="T:NHibernate.IMultiQuery" /> that can return
        /// a list of all the results of all the queries.
        /// Note that each query result is itself usually a list.
        /// </returns>
        public IMultiQuery CreateMultiQuery()
        {
            return null;

        }

        /// <summary>Sets the batch size of the session</summary>
        /// <param name="batchSize"></param>
        /// <returns></returns>
        public ISession SetBatchSize(int batchSize)
        {
            return new MockSession();
        }

        /// <summary>Gets the session implementation.</summary>
        /// <remarks>
        /// This method is provided in order to get the <b>NHibernate</b> implementation of the session from wrapper implementions.
        /// Implementors of the <seealso cref="T:NHibernate.ISession" /> interface should return the NHibernate implementation of this method.
        /// </remarks>
        /// <returns>
        /// An NHibernate implementation of the <seealso cref="T:NHibernate.Engine.ISessionImplementor" /> interface
        /// </returns>
        public ISessionImplementor GetSessionImplementation()
        {
            return null;
        }

        /// <summary>
        /// An <see cref="T:NHibernate.IMultiCriteria" /> that can return a list of all the results
        /// of all the criterias.
        /// </summary>
        /// <returns></returns>
        public IMultiCriteria CreateMultiCriteria()
        {
            return null;

        }

        /// <summary>
        /// Starts a new Session with the given entity mode in effect. This secondary
        /// Session inherits the connection, transaction, and other context
        /// information from the primary Session. It doesn't need to be flushed
        /// or closed by the developer.
        /// </summary>
        /// <param name="entityMode">The entity mode to use for the new session.</param>
        /// <returns>The new session</returns>
        public ISession GetSession(EntityMode entityMode)
        {
            return new MockSession();
        }

        /// <summary> The entity mode in effect for this session.</summary>
        public EntityMode ActiveEntityMode { get; }

        /// <summary>
        /// Determines at which points Hibernate automatically flushes the session.
        /// </summary>
        /// <remarks>
        /// For a readonly session, it is reasonable to set the flush mode to <c>FlushMode.Never</c>
        /// at the start of the session (in order to achieve some extra performance).
        /// </remarks>
        public FlushMode FlushMode { get; set; }

        /// <summary> The current cache mode. </summary>
        /// <remarks>
        /// Cache mode determines the manner in which this session can interact with
        /// the second level cache.
        /// </remarks>
        public CacheMode CacheMode { get; set; }

        /// <summary>
        /// Get the <see cref="T:NHibernate.ISessionFactory" /> that created this instance.
        /// </summary>
        public ISessionFactory SessionFactory { get;} = new MockSesionFactory();

        /// <summary>Gets the ADO.NET connection.</summary>
        /// <remarks>
        /// Applications are responsible for calling commit/rollback upon the connection before
        /// closing the <c>ISession</c>.
        /// </remarks>
        public IDbConnection Connection { get; } = new MockDbConnection();

        /// <summary>
        /// Is the <c>ISession</c> still open?
        /// </summary>
        public bool IsOpen { get; } = true;

        /// <summary>
        /// Is the <c>ISession</c> currently connected?
        /// </summary>
        public bool IsConnected { get; } = true;

        /// <summary>
        /// Get the current Unit of Work and return the associated <c>ITransaction</c> object.
        /// </summary>
        public ITransaction Transaction { get; } = new MockTransaction();

        /// <summary> Get the statistics for this session.</summary>
        public ISessionStatistics Statistics { get; }

    }

    public class MockCriteria : ICriteria
    {
        /// <summary>Creates a new object that is a copy of the current instance.</summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public object Clone()
        {
            return this;
        }

        /// <summary>
        /// Used to specify that the query results will be a projection (scalar in
        /// nature).  Implicitly specifies the projection result transformer.
        /// </summary>
        /// <param name="projection">The projection representing the overall "shape" of the
        /// query results.</param>
        /// <returns>This instance (for method chaining)</returns>
        /// <remarks>
        /// <para>
        /// The individual components contained within the given <see cref="T:NHibernate.Criterion.IProjection" />
        /// determines the overall "shape" of the query result.
        /// </para>
        /// </remarks>
        public ICriteria SetProjection(params IProjection[] projection)
        {
            return this;
        }

        /// <summary>
        /// Add an Expression to constrain the results to be retrieved.
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ICriteria Add(ICriterion expression)
        {
            return this;

        }

        /// <summary>An an Order to the result set</summary>
        /// <param name="order"></param>
        public ICriteria AddOrder(Order order)
        {
            return this;

        }

        /// <summary>
        /// Specify an association fetching strategy.  Currently, only
        /// one-to-many and one-to-one associations are supported.
        /// </summary>
        /// <param name="associationPath">A dot seperated property path.</param>
        /// <param name="mode">The Fetch mode.</param>
        /// <returns></returns>
        public ICriteria SetFetchMode(string associationPath, FetchMode mode)
        {
            return this;

        }

        /// <summary>Set the lock mode of the current entity</summary>
        /// <param name="lockMode">the lock mode</param>
        /// <returns></returns>
        public ICriteria SetLockMode(LockMode lockMode)
        {
            return this;

        }

        /// <summary>Set the lock mode of the aliased entity</summary>
        /// <param name="alias">an alias</param>
        /// <param name="lockMode">the lock mode</param>
        /// <returns></returns>
        public ICriteria SetLockMode(string alias, LockMode lockMode)
        {
            return this;

        }

        /// <summary>
        /// Join an association, assigning an alias to the joined entity
        /// </summary>
        /// <param name="associationPath"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public ICriteria CreateAlias(string associationPath, string alias)
        {
            return this;

        }

        /// <summary>
        /// Join an association using the specified join-type, assigning an alias to the joined
        /// association
        /// </summary>
        /// <param name="associationPath"></param>
        /// <param name="alias"></param>
        /// <param name="joinType">The type of join to use.</param>
        /// <returns>this (for method chaining)</returns>
        public ICriteria CreateAlias(string associationPath, string alias, JoinType joinType)
        {
            return this;

        }

        /// <summary>
        /// Join an association using the specified join-type, assigning an alias to the joined
        /// association
        /// </summary>
        /// <param name="associationPath"></param>
        /// <param name="alias"></param>
        /// <param name="joinType">The type of join to use.</param>
        /// <param name="withClause">The criteria to be added to the join condition (ON clause)</param>
        /// <returns>this (for method chaining)</returns>
        public ICriteria CreateAlias(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            return this;

        }

        /// <summary>
        /// Create a new <see cref="T:NHibernate.ICriteria" />, "rooted" at the associated entity
        /// </summary>
        /// <param name="associationPath"></param>
        /// <returns></returns>
        public ICriteria CreateCriteria(string associationPath)
        {
            return this;

        }

        /// <summary>
        /// Create a new <see cref="T:NHibernate.ICriteria" />, "rooted" at the associated entity,
        /// using the specified join type.
        /// </summary>
        /// <param name="associationPath">A dot-seperated property path</param>
        /// <param name="joinType">The type of join to use</param>
        /// <returns>The created "sub criteria"</returns>
        public ICriteria CreateCriteria(string associationPath, JoinType joinType)
        {
            return this;

        }

        /// <summary>
        /// Create a new <see cref="T:NHibernate.ICriteria" />, "rooted" at the associated entity,
        /// assigning the given alias
        /// </summary>
        /// <param name="associationPath"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public ICriteria CreateCriteria(string associationPath, string alias)
        {
            return this;

        }

        /// <summary>
        /// Create a new <see cref="T:NHibernate.ICriteria" />, "rooted" at the associated entity,
        /// assigning the given alias and using the specified join type.
        /// </summary>
        /// <param name="associationPath">A dot-separated property path</param>
        /// <param name="alias">The alias to assign to the joined association (for later reference).</param>
        /// <param name="joinType">The type of join to use.</param>
        /// <returns>The created "sub criteria"</returns>
        public ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType)
        {
            return this;

        }

        /// <summary>
        /// Create a new <see cref="T:NHibernate.ICriteria" />, "rooted" at the associated entity,
        /// assigning the given alias and using the specified join type.
        /// </summary>
        /// <param name="associationPath">A dot-separated property path</param>
        /// <param name="alias">The alias to assign to the joined association (for later reference).</param>
        /// <param name="joinType">The type of join to use.</param>
        /// <param name="withClause">The criteria to be added to the join condition (ON clause)</param>
        /// <returns>The created "sub criteria"</returns>
        public ICriteria CreateCriteria(string associationPath, string alias, JoinType joinType, ICriterion withClause)
        {
            return this;

        }

        /// <summary>
        /// Set a strategy for handling the query results. This determines the
        /// "shape" of the query result set.
        /// <seealso cref="F:NHibernate.Criterion.CriteriaSpecification.RootEntity" />
        /// <seealso cref="F:NHibernate.Criterion.CriteriaSpecification.DistinctRootEntity" />
        /// <seealso cref="F:NHibernate.Criterion.CriteriaSpecification.AliasToEntityMap" />
        /// </summary>
        /// <param name="resultTransformer"></param>
        /// <returns></returns>
        public ICriteria SetResultTransformer(IResultTransformer resultTransformer)
        {
            return this;

        }

        /// <summary>
        /// Set a limit upon the number of objects to be retrieved
        /// </summary>
        /// <param name="maxResults"></param>
        public ICriteria SetMaxResults(int maxResults)
        {
            return this;

        }

        /// <summary>Set the first result to be retrieved</summary>
        /// <param name="firstResult"></param>
        public ICriteria SetFirstResult(int firstResult)
        {
            return this;

        }

        /// <summary> Set a fetch size for the underlying ADO query. </summary>
        /// <param name="fetchSize">the fetch size </param>
        /// <returns> this (for method chaining) </returns>
        public ICriteria SetFetchSize(int fetchSize)
        {
            return this;

        }

        /// <summary>Set a timeout for the underlying ADO.NET query</summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public ICriteria SetTimeout(int timeout)
        {
            return this;

        }

        /// <summary>Enable caching of this query result set</summary>
        /// <param name="cacheable"></param>
        /// <returns></returns>
        public ICriteria SetCacheable(bool cacheable)
        {
            return this;

        }

        /// <summary>Set the name of the cache region.</summary>
        /// <param name="cacheRegion">the name of a query cache region, or <see langword="null" />
        /// for the default query cache</param>
        /// <returns></returns>
        public ICriteria SetCacheRegion(string cacheRegion)
        {
            return this;

        }

        /// <summary> Add a comment to the generated SQL. </summary>
        /// <param name="comment">a human-readable string </param>
        /// <returns> this (for method chaining) </returns>
        public ICriteria SetComment(string comment)
        {
            return this;

        }

        /// <summary> Override the flush mode for this particular query. </summary>
        /// <param name="flushMode">The flush mode to use. </param>
        /// <returns> this (for method chaining) </returns>
        public ICriteria SetFlushMode(FlushMode flushMode)
        {
            return this;

        }

        /// <summary> Override the cache mode for this particular query. </summary>
        /// <param name="cacheMode">The cache mode to use. </param>
        /// <returns> this (for method chaining) </returns>
        public ICriteria SetCacheMode(CacheMode cacheMode)
        {
            return this;

        }

        /// <summary>Get the results</summary>
        /// <returns></returns>
        public IList List()
        {
            return new List<object>();

        }

        /// <summary>
        /// Convenience method to return a single instance that matches
        /// the query, or null if the query returns no results.
        /// </summary>
        /// <returns>the single result or <see langword="null" /></returns>
        /// <exception cref="T:NHibernate.HibernateException">
        /// If there is more than one matching result
        /// </exception>
        public object UniqueResult()
        {
            return null;
        }

        /// <summary>
        /// Get a enumerable that when enumerated will execute
        /// a batch of queries in a single database roundtrip
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> Future<T>()
        {
            return null;

        }

        /// <summary>
        /// Get an IFutureValue instance, whose value can be retrieved through
        /// its Value property. The query is not executed until the Value property
        /// is retrieved, which will execute other Future queries as well in a
        /// single roundtrip
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IFutureValue<T> FutureValue<T>()
        {
            return null;

        }

        /// <summary>
        /// Get the results and fill the <see cref="T:System.Collections.IList" />
        /// </summary>
        /// <param name="results">The list to fill with the results.</param>
        public void List(IList results)
        {

        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.ICriteria.List" />.
        /// </summary>
        public IList<T> List<T>()
        {
            return new List<T>();

        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.ICriteria.UniqueResult" />.
        /// </summary>
        public T UniqueResult<T>()
        {
            return default(T);
        }

        /// <summary>Clear all orders from criteria.</summary>
        public void ClearOrders()
        {
        }

        /// <summary>
        /// Allows to get a sub criteria by path.
        /// Will return null if the criteria does not exists.
        /// </summary>
        /// <param name="path">The path.</param>
        public ICriteria GetCriteriaByPath(string path)
        {
            return this;
        }

        /// <summary>
        /// Alows to get a sub criteria by alias.
        /// Will return null if the criteria does not exists
        /// </summary>
        /// <param name="alias">The alias.</param>
        /// <returns></returns>
        public ICriteria GetCriteriaByAlias(string alias)
        {
            return this;
        }

        /// <summary>
        /// Gets the root entity type if available, throws otherwise
        /// </summary>
        /// <remarks>
        /// This is an NHibernate specific method, used by several dependent
        /// frameworks for advance integration with NHibernate.
        /// </remarks>
        public Type GetRootEntityTypeIfAvailable()
        {
            return null;
        }

        /// <summary>
        /// Get the alias of the entity encapsulated by this criteria instance.
        /// </summary>
        /// <value>The alias for the encapsulated entity.</value>
        public string Alias { get; }
    }

    public class MockTransaction : ITransaction
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Begin the transaction with the default isolation level.
        /// </summary>
        public void Begin()
        {
        }

        /// <summary>
        /// Begin the transaction with the specified isolation level.
        /// </summary>
        /// <param name="isolationLevel">Isolation level of the transaction</param>
        public void Begin(IsolationLevel isolationLevel)
        { 
        }

        /// <summary>
        /// Flush the associated <c>ISession</c> and end the unit of work.
        /// </summary>
        /// <remarks>
        /// This method will commit the underlying transaction if and only if the transaction
        /// was initiated by this object.
        /// </remarks>
        public void Commit()
        {
            WasCommitted = true;
            IsActive = false;
        }

        /// <summary>Force the underlying transaction to roll back.</summary>
        public void Rollback()
        {
            WasRolledBack = true;
            IsActive = false;
        }

        /// <summary>
        /// Enlist the <see cref="T:System.Data.IDbCommand" /> in the current Transaction.
        /// </summary>
        /// <param name="command">The <see cref="T:System.Data.IDbCommand" /> to enlist.</param>
        /// <remarks>It is okay for this to be a no op implementation.</remarks>
        public void Enlist(IDbCommand command)
        {
        }

        /// <summary>
        /// Register a user synchronization callback for this transaction.
        /// </summary>
        /// <param name="synchronization">The <see cref="T:NHibernate.Transaction.ISynchronization" /> callback to register.</param>
        public void RegisterSynchronization(ISynchronization synchronization)
        {
        }

        /// <summary>Is the transaction in progress</summary>
        public bool IsActive { get; set; } = true;

        /// <summary>
        /// Was the transaction rolled back or set to rollback only?
        /// </summary>
        public bool WasRolledBack { get; set; }

        /// <summary>Was the transaction successfully committed?</summary>
        /// <remarks>
        /// This method could return <see langword="false" /> even after successful invocation of <c>Commit()</c>
        /// </remarks>
        public bool WasCommitted { get; set; }
    }

    public class MockSqlQery : ISQLQuery
    {
        /// <summary>
        /// Return the query results as an <see cref="T:System.Collections.IEnumerable" />. If the query contains multiple results
        /// per row, the results are returned in an instance of <c>object[]</c>.
        /// </summary>
        /// <remarks>
        /// <p>
        /// Entities returned as results are initialized on demand. The first SQL query returns
        /// identifiers only.
        /// </p>
        /// <p>
        /// This is a good strategy to use if you expect a high number of the objects
        /// returned to be already loaded in the <see cref="T:NHibernate.ISession" /> or in the 2nd level cache.
        /// </p>
        /// </remarks>
        public IEnumerable Enumerable()
        {
            return new List<object>();
        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.IQuery.Enumerable" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> Enumerable<T>()
        {
            return new List<T>();

        }

        /// <summary>
        /// Return the query results as an <see cref="T:System.Collections.IList" />. If the query contains multiple results per row,
        /// the results are returned in an instance of <c>object[]</c>.
        /// </summary>
        /// <returns>The <see cref="T:System.Collections.IList" /> filled with the results.</returns>
        /// <remarks>
        /// This is a good strategy to use if you expect few of the objects being returned are already loaded
        /// or if you want to fill the 2nd level cache.
        /// </remarks>
        public IList List()
        {
            return new List<object>() { null };

        }

        /// <summary>
        /// Return the query results an place them into the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <param name="results">The <see cref="T:System.Collections.IList" /> to place the results in.</param>
        public void List(IList results)
        {
        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.IQuery.List" />.
        /// </summary>
        public IList<T> List<T>()
        {
            return new List<T>() { default(T) };

        }

        /// <summary>
        /// Convenience method to return a single instance that matches
        /// the query, or null if the query returns no results.
        /// </summary>
        /// <returns>the single result or <see langword="null" /></returns>
        /// <exception cref="T:NHibernate.HibernateException">
        /// Thrown when there is more than one matching result.
        /// </exception>
        public object UniqueResult()
        {
            return new object();
        }

        /// <summary>
        /// Strongly-typed version of <see cref="M:NHibernate.IQuery.UniqueResult" />.
        /// </summary>
        public T UniqueResult<T>()
        {
            return default (T);
        }

        /// <summary>Execute the update or delete statement.</summary>
        /// <returns> The number of entities updated or deleted. </returns>
        public int ExecuteUpdate()
        {
            return 0;
        }

        /// <summary>Set the maximum number of rows to retrieve.</summary>
        /// <param name="maxResults">The maximum number of rows to retreive.</param>
        public IQuery SetMaxResults(int maxResults)
        {
            throw new NotImplementedException();
        }

        /// <summary>Sets the first row to retrieve.</summary>
        /// <param name="firstResult">The first row to retreive.</param>
        public IQuery SetFirstResult(int firstResult)
        {
            return new MockSqlQery();
        }

        /// <summary>
        /// Entities retrieved by this query will be loaded in
        /// a read-only mode where NHibernate will never dirty-check
        /// them or make changes persistent.
        /// </summary>
        public IQuery SetReadOnly(bool readOnly)
        {
            return new MockSqlQery();

        }

        /// <summary>Enable caching of this query result set.</summary>
        /// <param name="cacheable">Should the query results be cacheable?</param>
        public IQuery SetCacheable(bool cacheable)
        {
            return new MockSqlQery();
        }

        ///             Set the name of the cache region.
        ///             <param name="cacheRegion">The name of a query cache region, or <see langword="null" />
        /// for the default query cache</param>
        public IQuery SetCacheRegion(string cacheRegion)
        {
            return new MockSqlQery();
        }

        /// <summary>The timeout for the underlying ADO query</summary>
        /// <param name="timeout"></param>
        public IQuery SetTimeout(int timeout)
        {
            return new MockSqlQery();

        }

        /// <summary> Set a fetch size for the underlying ADO query.</summary>
        /// <param name="fetchSize">the fetch size </param>
        public IQuery SetFetchSize(int fetchSize)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Set the lockmode for the objects idententified by the
        /// given alias that appears in the <c>FROM</c> clause.
        /// </summary>
        /// <param name="alias">alias a query alias, or <c>this</c> for a collection filter</param>
        /// <param name="lockMode"></param>
        public IQuery SetLockMode(string alias, LockMode lockMode)
        {
            return new MockSqlQery();

        }

        /// <summary> Add a comment to the generated SQL.</summary>
        /// <param name="comment">a human-readable string </param>
        public IQuery SetComment(string comment)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Override the current session flush mode, just for this query.
        /// </summary>
        public IQuery SetFlushMode(FlushMode flushMode)
        {
            return new MockSqlQery();

        }

        /// <summary> Override the current session cache mode, just for this query. </summary>
        /// <param name="cacheMode">The cache mode to use. </param>
        /// <returns> this (for method chaining) </returns>
        public IQuery SetCacheMode(CacheMode cacheMode)
        {
            return new MockSqlQery();

        }

        /// <summary>Bind a value to an indexed parameter.</summary>
        /// <param name="position">Position of the parameter in the query, numbered from <c>0</c></param>
        /// <param name="val">The possibly null parameter value</param>
        /// <param name="type">The NHibernate type</param>
        public IQuery SetParameter(int position, object val, IType type)
        {
            return new MockSqlQery();

        }

        /// <summary>Bind a value to a named query parameter</summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">The possibly null parameter value</param>
        /// <param name="type">The NHibernate <see cref="T:NHibernate.Type.IType" />.</param>
        public IQuery SetParameter(string name, object val, IType type)
        {
            return new MockSqlQery();

        }

        /// <summary>Bind a value to an indexed parameter.</summary>
        /// <param name="position">Position of the parameter in the query, numbered from <c>0</c></param>
        /// <param name="val">The possibly null parameter value</param>
        /// <typeparam name="T">The parameter's <see cref="N:NHibernate.Type" /> </typeparam>
        public IQuery SetParameter<T>(int position, T val)
        {
            return new MockSqlQery();

        }

        /// <summary>Bind a value to a named query parameter</summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">The possibly null parameter value</param>
        /// <typeparam name="T">The parameter's <see cref="N:NHibernate.Type" /> </typeparam>
        public IQuery SetParameter<T>(string name, T val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind a value to an indexed parameter, guessing the NHibernate type from
        /// the class of the given object.
        /// </summary>
        /// <param name="position">The position of the parameter in the query, numbered from <c>0</c></param>
        /// <param name="val">The non-null parameter value</param>
        public IQuery SetParameter(int position, object val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind a value to a named query parameter, guessing the NHibernate <see cref="T:NHibernate.Type.IType" />
        /// from the class of the given object.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">The non-null parameter value</param>
        public IQuery SetParameter(string name, object val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind multiple values to a named query parameter. This is useful for binding a list
        /// of values to an expression such as <c>foo.bar in (:value_list)</c>
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="vals">A collection of values to list</param>
        /// <param name="type">The NHibernate type of the values</param>
        public IQuery SetParameterList(string name, ICollection vals, IType type)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind multiple values to a named query parameter, guessing the NHibernate
        /// type from the class of the first object in the collection. This is useful for binding a list
        /// of values to an expression such as <c>foo.bar in (:value_list)</c>
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="vals">A collection of values to list</param>
        public IQuery SetParameterList(string name, ICollection vals)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind multiple values to a named query parameter. This is useful for binding
        /// a list of values to an expression such as <tt>foo.bar in (:value_list)</tt>.
        /// </summary>
        /// <param name="name">the name of the parameter </param>
        /// <param name="vals">a collection of values to list </param>
        /// <param name="type">the NHibernate type of the values </param>
        public IQuery SetParameterList(string name, object[] vals, IType type)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind multiple values to a named query parameter. The NHibernate type of the parameter is
        /// first detected via the usage/position in the query and if not sufficient secondly
        /// guessed from the class of the first object in the array. This is useful for binding a list of values
        /// to an expression such as <tt>foo.bar in (:value_list)</tt>.
        /// </summary>
        /// <param name="name">the name of the parameter </param>
        /// <param name="vals">a collection of values to list </param>
        public IQuery SetParameterList(string name, object[] vals)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind the property values of the given object to named parameters of the query,
        /// matching property names with parameter names and mapping property types to
        /// NHibernate types using heuristics.
        /// </summary>
        /// <param name="obj">Any PONO</param>
        public IQuery SetProperties(object obj)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.String" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.AnsiStringType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.String" />.</param>
        public IQuery SetAnsiString(int position, string val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.String" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.AnsiStringType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.String" />.</param>
        public IQuery SetAnsiString(string name, string val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Byte" /> array to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.BinaryType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Byte" /> array.</param>
        public IQuery SetBinary(int position, byte[] val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Byte" /> array to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.BinaryType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Byte" /> array.</param>
        public IQuery SetBinary(string name, byte[] val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Boolean" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.BooleanType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Boolean" />.</param>
        public IQuery SetBoolean(int position, bool val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Boolean" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.BooleanType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Boolean" />.</param>
        public IQuery SetBoolean(string name, bool val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Byte" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.ByteType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Byte" />.</param>
        public IQuery SetByte(int position, byte val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Byte" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.ByteType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Byte" />.</param>
        public IQuery SetByte(string name, byte val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Char" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.CharType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Char" />.</param>
        public IQuery SetCharacter(int position, char val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Char" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.CharType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Char" />.</param>
        public IQuery SetCharacter(string name, char val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.DateTime" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DateTimeType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
        public IQuery SetDateTime(int position, DateTime val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.DateTime" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DateTimeType" />.
        /// </summary>
        /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
        /// <param name="name">The name of the parameter</param>
        public IQuery SetDateTime(string name, DateTime val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Decimal" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DecimalType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Decimal" />.</param>
        public IQuery SetDecimal(int position, decimal val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Decimal" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DecimalType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Decimal" />.</param>
        public IQuery SetDecimal(string name, decimal val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Double" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DoubleType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Double" />.</param>
        public IQuery SetDouble(int position, double val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Double" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DoubleType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Double" />.</param>
        public IQuery SetDouble(string name, double val)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Bind an instance of a persistent enumeration class to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.PersistentEnumType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a persistent enumeration</param>
        public IQuery SetEnum(int position, Enum val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a persistent enumeration class to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.PersistentEnumType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a persistent enumeration</param>
        public IQuery SetEnum(string name, Enum val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int16" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int16Type" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int16" />.</param>
        public IQuery SetInt16(int position, short val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int16" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int16Type" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int16" />.</param>
        public IQuery SetInt16(string name, short val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int32" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int32Type" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int32" />.</param>
        public IQuery SetInt32(int position, int val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int32" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int32Type" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int32" />.</param>
        public IQuery SetInt32(string name, int val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int64" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int64Type" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int64" />.</param>
        public IQuery SetInt64(int position, long val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Int64" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.Int64Type" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Int64" />.</param>
        public IQuery SetInt64(string name, long val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Single" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.SingleType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Single" />.</param>
        public IQuery SetSingle(int position, float val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Single" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.SingleType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.Single" />.</param>
        public IQuery SetSingle(string name, float val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.String" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.StringType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.String" />.</param>
        public IQuery SetString(int position, string val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.String" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.StringType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.String" />.</param>
        public IQuery SetString(string name, string val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.DateTime" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.DateTimeType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
        public IQuery SetTime(int position, DateTime val)
        { 
            return new MockSqlQery();

     }

    /// <summary>
    /// Bind an instance of a <see cref="T:System.DateTime" /> to a named parameter
    /// using an NHibernate <see cref="T:NHibernate.Type.DateTimeType" />.
    /// </summary>
    /// <param name="name">The name of the parameter</param>
    /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
    public IQuery SetTime(string name, DateTime val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.DateTime" /> to an indexed parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.TimestampType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
        public IQuery SetTimestamp(int position, DateTime val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.DateTime" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.TimestampType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a <see cref="T:System.DateTime" />.</param>
        public IQuery SetTimestamp(string name, DateTime val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Guid" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.GuidType" />.
        /// </summary>
        /// <param name="position">The position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">An instance of a <see cref="T:System.Guid" />.</param>
        public IQuery SetGuid(int position, Guid val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a <see cref="T:System.Guid" /> to a named parameter
        /// using an NHibernate <see cref="T:NHibernate.Type.GuidType" />.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">An instance of a <see cref="T:System.Guid" />.</param>
        public IQuery SetGuid(string name, Guid val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a mapped persistent class to an indexed parameter.
        /// </summary>
        /// <param name="position">Position of the parameter in the query string, numbered from <c>0</c></param>
        /// <param name="val">A non-null instance of a persistent class</param>
        public IQuery SetEntity(int position, object val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Bind an instance of a mapped persistent class to a named parameter.
        /// </summary>
        /// <param name="name">The name of the parameter</param>
        /// <param name="val">A non-null instance of a persistent class</param>
        public IQuery SetEntity(string name, object val)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Set a strategy for handling the query results. This can be used to change
        /// "shape" of the query result.
        /// </summary>
        public IQuery SetResultTransformer(IResultTransformer resultTransformer)
        {
            return new MockSqlQery();

        }

        /// <summary>
        /// Get a enumerable that when enumerated will execute
        /// a batch of queries in a single database roundtrip
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<T> Future<T>()
        {
            return null;
        }

        /// <summary>
        /// Get an IFutureValue instance, whose value can be retrieved through
        /// its Value property. The query is not executed until the Value property
        /// is retrieved, which will execute other Future queries as well in a
        /// single roundtrip
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IFutureValue<T> FutureValue<T>()
        {
            return null;
        }

        /// <summary>The query string</summary>
        public string QueryString { get; } = "";

        /// <summary>The NHibernate types of the query result set.</summary>
        public IType[] ReturnTypes { get; } = new IType[1];

        /// <summary> Return the HQL select clause aliases (if any)</summary>
        /// <returns> an array of aliases as strings </returns>
        public string[] ReturnAliases { get; } = new string[1];

        /// <summary>The names of all named parameters of the query</summary>
        /// <value>The parameter names, in no particular order</value>
        public string[] NamedParameters { get; } = new string[1];

        /// <summary>Declare a "root" entity, without specifying an alias</summary>
        public ISQLQuery AddEntity(string entityName)
        {
            return new MockSqlQery();
        }

        /// <summary>Declare a "root" entity</summary>
        public ISQLQuery AddEntity(string alias, string entityName)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "root" entity, specifying a lock mode</summary>
        public ISQLQuery AddEntity(string alias, string entityName, LockMode lockMode)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "root" entity, without specifying an alias</summary>
        public ISQLQuery AddEntity(Type entityClass)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "root" entity</summary>
        public ISQLQuery AddEntity(string alias, Type entityClass)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "root" entity, specifying a lock mode</summary>
        public ISQLQuery AddEntity(string alias, Type entityClass, LockMode lockMode)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "joined" entity</summary>
        public ISQLQuery AddJoin(string alias, string path)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a "joined" entity, specifying a lock mode</summary>
        public ISQLQuery AddJoin(string alias, string path, LockMode lockMode)
        {
            return new MockSqlQery();

        }

        /// <summary>Declare a scalar query result</summary>
        public ISQLQuery AddScalar(string columnAlias, IType type)
        {
            return new MockSqlQery();

        }

        /// <summary>Use a predefined named ResultSetMapping</summary>
        public ISQLQuery SetResultSetMapping(string name)
        {
            return new MockSqlQery();

        }
    }

    public class MockSesionFactory : ISessionFactory
    {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Open a <c>ISession</c> on the given connection
        /// </summary>
        /// <param name="conn">A connection provided by the application</param>
        /// <returns>A session</returns>
        /// <remarks>
        /// Note that the second-level cache will be disabled if you
        /// supply a ADO.NET connection. NHibernate will not be able to track
        /// any statements you might have executed in the same transaction.
        /// Consider implementing your own <see cref="T:NHibernate.Connection.IConnectionProvider" />.
        /// </remarks>
        public ISession OpenSession(IDbConnection conn)
        {
            return new MockSession();
        }
#if WEBCLIENT
        public System.Threading.Tasks.Task CloseAsync(System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictAsync(Type persistentClass, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictAsync(Type persistentClass, object id, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictEntityAsync(string entityName, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictEntityAsync(string entityName, object id, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictCollectionAsync(string roleName, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictCollectionAsync(string roleName, object id, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictQueriesAsync(System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task EvictQueriesAsync(string cacheRegion, System.Threading.CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public ISessionBuilder WithOptions()
        {
            throw new NotImplementedException();
        }

        public ISession OpenSession(DbConnection connection)
        {
            throw new NotImplementedException();
        }

        public ISession OpenSession(DbConnection conn, IInterceptor sessionLocalInterceptor)
        {
            throw new NotImplementedException();
        }
#endif
        /// <summary>
        /// Create database connection and open a <c>ISession</c> on it, specifying an interceptor
        /// </summary>
        /// <param name="sessionLocalInterceptor">A session-scoped interceptor</param>
        /// <returns>A session</returns>
        public ISession OpenSession(IInterceptor sessionLocalInterceptor)
        {
            return new MockSession();
        }

        /// <summary>
        /// Open a <c>ISession</c> on the given connection, specifying an interceptor
        /// </summary>
        /// <param name="conn">A connection provided by the application</param>
        /// <param name="sessionLocalInterceptor">A session-scoped interceptor</param>
        /// <returns>A session</returns>
        /// <remarks>
        /// Note that the second-level cache will be disabled if you
        /// supply a ADO.NET connection. NHibernate will not be able to track
        /// any statements you might have executed in the same transaction.
        /// Consider implementing your own <see cref="T:NHibernate.Connection.IConnectionProvider" />.
        /// </remarks>
        public ISession OpenSession(IDbConnection conn, IInterceptor sessionLocalInterceptor)
        {
            return new MockSession();
        }

        /// <summary>
        /// Create a database connection and open a <c>ISession</c> on it
        /// </summary>
        /// <returns></returns>
        public ISession OpenSession()
        {
            return new MockSession();
        }

        public IStatelessSession OpenStatelessSession(DbConnection connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the <see cref="T:NHibernate.Metadata.IClassMetadata" /> associated with the given entity class
        /// </summary>
        /// <param name="persistentClass">the given entity type.</param>
        /// <returns>The class metadata or <see langword="null" /> if not found.</returns>
        /// <seealso cref="T:NHibernate.Metadata.IClassMetadata" />
        public IClassMetadata GetClassMetadata(Type persistentClass)
        {
            return null;
        }

        /// <summary> Get the <see cref="T:NHibernate.Metadata.IClassMetadata" /> associated with the given entity name </summary>
        /// <param name="entityName">the given entity name.</param>
        /// <returns>The class metadata or <see langword="null" /> if not found.</returns>
        /// <seealso cref="T:NHibernate.Metadata.IClassMetadata" />
        public IClassMetadata GetClassMetadata(string entityName)
        {
            return null;
        }

        /// <summary>
        /// Get the <c>CollectionMetadata</c> associated with the named collection role
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public ICollectionMetadata GetCollectionMetadata(string roleName)
        {
            return null;

        }

        /// <summary>
        /// Get all <see cref="T:NHibernate.Metadata.IClassMetadata" /> as a <see cref="T:System.Collections.IDictionary" /> from entityname <see langword="string" />
        /// to metadata object
        /// </summary>
        /// <returns> A dictionary from <see langword="string" /> an entity name to <see cref="T:NHibernate.Metadata.IClassMetadata" /> </returns>
        public IDictionary<string, IClassMetadata> GetAllClassMetadata()
        {
            return null;

        }

        /// <summary>
        /// Get all <c>CollectionMetadata</c> as a <c>IDictionary</c> from role name
        /// to metadata object
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, ICollectionMetadata> GetAllCollectionMetadata()
        {
            return null;

        }

        /// <summary>
        /// Destroy this <c>SessionFactory</c> and release all resources
        /// connection pools, etc). It is the responsibility of the application
        /// to ensure that there are no open <c>Session</c>s before calling
        /// <c>close()</c>.
        /// </summary>
        public void Close()
        {
        }

        /// <summary>
        /// Evict all entries from the process-level cache.  This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy.  Use with care.
        /// </summary>
        /// <param name="persistentClass"></param>
        public void Evict(Type persistentClass)
        {
        }

        /// <summary>
        /// Evict an entry from the process-level cache.  This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy.  Use with care.
        /// </summary>
        /// <param name="persistentClass"></param>
        /// <param name="id"></param>
        public void Evict(Type persistentClass, object id)
        {
        }

        /// <summary>
        /// Evict all entries from the second-level cache. This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy. Use with care.
        /// </summary>
        public void EvictEntity(string entityName)
        {
        }

        /// <summary>
        /// Evict an entry from the second-level  cache. This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy. Use with care.
        /// </summary>
        public void EvictEntity(string entityName, object id)
        {
        }

        /// <summary>
        /// Evict all entries from the process-level cache.  This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy.  Use with care.
        /// </summary>
        /// <param name="roleName"></param>
        public void EvictCollection(string roleName)
        {
        }

        /// <summary>
        /// Evict an entry from the process-level cache.  This method occurs outside
        /// of any transaction; it performs an immediate "hard" remove, so does not respect
        /// any transaction isolation semantics of the usage strategy.  Use with care.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="id"></param>
        public void EvictCollection(string roleName, object id)
        {
        }

        /// <summary>
        /// Evict any query result sets cached in the default query cache region.
        /// </summary>
        public void EvictQueries()
        {
        }

        /// <summary>
        /// Evict any query result sets cached in the named query cache region.
        /// </summary>
        /// <param name="cacheRegion"></param>
        public void EvictQueries(string cacheRegion)
        {
        }

        /// <summary> Get a new stateless session.</summary>
        public IStatelessSession OpenStatelessSession()
        {
            return null;
        }

        /// <summary> Get a new stateless session for the given ADO.NET connection.</summary>
        public IStatelessSession OpenStatelessSession(IDbConnection connection)
        {
           return null;
        }

        /// <summary>Obtain the definition of a filter by name.</summary>
        /// <param name="filterName">The name of the filter for which to obtain the definition.</param>
        /// <return>The filter definition.</return>
        public FilterDefinition GetFilterDefinition(string filterName)
        {
            return null;
        }

        /// <summary>Obtains the current session.</summary>
        /// <remarks>
        /// <para>
        /// The definition of what exactly "current" means is controlled by the <see cref="T:NHibernate.Context.ICurrentSessionContext" />
        /// implementation configured for use.
        /// </para>
        /// </remarks>
        /// <returns>The current session.</returns>
        /// <exception cref="T:NHibernate.HibernateException">Indicates an issue locating a suitable current session.</exception>
        public ISession GetCurrentSession()
        {
            return new MockSession();
        }

        /// <summary> Get the statistics for this session factory</summary>
        public IStatistics Statistics { get; } = null;

        /// <summary> Was this <see cref="T:NHibernate.ISessionFactory" /> already closed?</summary>
        public bool IsClosed { get; } = false;

        /// <summary>
        /// Obtain a set of the names of all filters defined on this SessionFactory.
        /// </summary>
        /// <return>The set of filter names.</return>
        public ICollection<string> DefinedFilterNames { get; } = null;
    }
#endif*/
}
    