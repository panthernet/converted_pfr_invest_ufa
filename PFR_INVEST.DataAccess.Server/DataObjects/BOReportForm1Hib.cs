﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class BOReportForm1Hib: BOReportForm1
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<BOReportForm1Data> DataHib { get; set; }
    }
}
