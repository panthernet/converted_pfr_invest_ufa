﻿using System.Collections.Generic;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class SPNOperationHib : SPNOperation
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SIRegisterHib> TransferRegisters { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SPNDirectionHib> Directions { get; set; }
    }
}
