﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
	public class KipLinksHib : KipLinks
    {
		public KipLinksHib() 
		{
			OrganizationCode = string.Empty;
			PortfolioCode = string.Empty;
		}
    }
}
