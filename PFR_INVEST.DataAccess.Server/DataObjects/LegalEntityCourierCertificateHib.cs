﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityCourierCertificateHib : LegalEntityCourierCertificate
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityCourierHib LegalEntityCourier { get; set; }

    }
}
