﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class PaymentDetailHib : PaymentDetail
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual KBK KBK { get; set; }
	}
}
