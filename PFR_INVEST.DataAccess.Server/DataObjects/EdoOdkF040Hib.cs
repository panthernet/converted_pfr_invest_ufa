﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class EdoOdkF040Hib : EdoOdkF040
	{
        //[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<F040Detail> Details { get; set; }

        //[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<F040Rrz>  RRZs{ get; set; }

        [IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual ContractHib Contract { get; set; }
	}
}
