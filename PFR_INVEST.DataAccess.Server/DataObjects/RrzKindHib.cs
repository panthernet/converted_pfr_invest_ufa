﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class RrzKindHib:RrzKind
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual IList<F040RrzHib> F040Rrzs { get; set; }
	}
}
