﻿using System.Runtime.Serialization;
using NHibernate.Criterion;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class SIUKPlanHib : SIUKPlan
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SITransferHib TransferList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ReqTransferHib ReqTransfer { get; set; }
       
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ReqTransferHib ReqTransfer2 { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual MonthHib Month { get; set; }

        /// <summary>
        /// Возвращает критерион условия, чтобы хотя бы один слот переисления был NULL
        /// </summary>
        /// <returns></returns>
        public static ICriterion GetCriterionAnyTransferIsNull()
        {
            return Restrictions.Or(Restrictions.IsNull("p.TransferID"), 
                Restrictions.IsNull("p.Transfer2ID"));
        }

        public static ICriterion GetCriterionContainsTransferId(long transferId)
        {
            return Restrictions.Or(Restrictions.Eq("p.TransferID", transferId),
	            Restrictions.Eq("p.Transfer2ID", transferId));
        }

        //!!!! Работа с несколькими перечислениями также в -->  SIUKPlanExtension.cs - GetTotalSummReqTransfer()
        //!!!! IISServiceHib.cs - DeleteYearPlan()
        //!!!! IISServiceHib.cs - GetUKPaymentsForPlan()
    }
}
