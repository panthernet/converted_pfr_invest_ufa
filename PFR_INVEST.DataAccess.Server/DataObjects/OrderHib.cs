﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class CbOrderHib : CbOrder
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PortfolioHib Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib TradeAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib DEPOAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib TransitAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccountHib CurrentAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib BankAgent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
		public virtual PersonHib Person { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PostHib Post { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<CbInOrderHib> CbInOrderList { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual CbOrderHib Parent { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<CbOrderHib> Children { get; set; }
    }
}
