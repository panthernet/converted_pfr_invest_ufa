﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class AttachHib : Attach
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AttachClassificHib AttachClassific { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PersonHib Executor { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DocumentHib Document { get; set; }
    }
}
