﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class EdoXmlBodyHib : EdoXmlBody
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<EDOLog> EDOLogs { get; set; }
    }
}
