﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;
using NHibernate;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DepClaimOfferHib : DepClaimOffer
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DepClaimSelectParamsHib Auction { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DepClaim2Hib DepClaim { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntity Bank { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<OfferPfrBankAccountSumHib> OfferPfrBankAccountSum { get; set; }

		[IgnoreDataMember]
        [HibExtensionDataProperty]
		public virtual IList<DepositHib> Deposits { get; set; }
		


        public virtual void SetDepositsGenerated(ISession session, bool value)
        {
            session.CreateQuery("update DepClaimOffer set DepositsGenerated = :value where ID = :id")
                .SetParameter("value", (byte)(value ? 1 : 0))
            .SetParameter("id", this.ID)
            .ExecuteUpdate();
        }
    }
}
