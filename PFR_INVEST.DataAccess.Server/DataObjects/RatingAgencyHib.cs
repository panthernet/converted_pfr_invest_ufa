﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    using System.Runtime.Serialization;

    [HibData]
    public class RatingAgencyHib : RatingAgency
    {
        [IgnoreDataMember]
        public virtual StatusHib Status { get; set; }
    }
}
