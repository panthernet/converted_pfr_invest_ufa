﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PortfolioPFRBankAccountHib : PortfolioPFRBankAccount
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Portfolio Portfolio { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PfrBankAccount PfrBankAccount { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Element AssignKind { get; set; }
    }
}
