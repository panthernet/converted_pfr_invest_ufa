﻿using System.Runtime.Serialization;

using PFR_INVEST.DataObjects;
using System.Collections.Generic;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DocumentHib : Document
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DocumentClassHib DocumentClass { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib LegalEntity { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PersonHib Executor { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AdditionalDocumentInfoHib AdditionalInfo { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DocFileBodyHib DocFileBody { get; set; }

     
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AttachHib> Attaches { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AttachHib> AttachesActive { get; set; }
    }
}
