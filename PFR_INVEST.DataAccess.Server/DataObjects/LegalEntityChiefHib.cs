﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class LegalEntityChiefHib : LegalEntityChief
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<LegalEntityHib> LegalEntities { get; set; }
    }
}
