﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
	public class F040RrzHib:F040Rrz
	{
		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual EdoOdkF040 Document { get; set; }

		[IgnoreDataMember]
		[HibExtensionDataProperty]
		public virtual RrzKind RrzKind { get; set; }
	}
}
