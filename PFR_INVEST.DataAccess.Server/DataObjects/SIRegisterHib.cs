﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class SIRegisterHib : SIRegister
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SPNOperationHib Operation { get; set; }
        
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ElementHib OperationType { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SPNDirection Direction { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Month CompanyMonth { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Year CompanyYear { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SITransferHib> TransferLists { get; set; }
    }
}
