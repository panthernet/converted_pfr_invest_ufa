﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class SITransferHib : SITransfer
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SIRegisterHib TransferRegister { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractHib Contract { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SIUKPlanHib> UKPlans { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ReqTransferHib> Transfers { get; set; }
    }
}
