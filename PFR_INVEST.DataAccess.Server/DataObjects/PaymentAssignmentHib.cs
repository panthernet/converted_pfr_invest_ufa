﻿using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
	[HibData]
    public class PaymentAssignmentHib : PaymentAssignment
	{
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Element Element { get; set; }

        [IgnoreDataMember]
        public virtual string ElementName { get; set; }
    }
}
