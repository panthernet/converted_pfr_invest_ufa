﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzeInsuredpersonDataHib : AnalyzeInsuredpersonData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzeInsuredpersonReport AnalyzeInsuredpersonReport { get; set; }
       
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SubjectSPN SubjectSPN { get; set; }

        public AnalyzeInsuredpersonDataHib()
        {
            
        }

        public AnalyzeInsuredpersonDataHib(AnalyzeInsuredpersonReport r, AnalyzeInsuredpersonData data)
        {
            AnalyzeInsuredpersonReport = r;
            this.Year = data.Year;
            this.Kvartal = data.Kvartal;
            this.Id = data.Id;
            this.SubjectId = data.SubjectId;
            this.Total = data.Total;
            this.SubjectName = data.SubjectName;
        }
    }
}
