﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzePensionfundtonpfDataHib : AnalyzePensionfundtonpfData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzePensionfundtonpfReport AnalyzePensionfundtonpfReport { get; set; }
        public virtual Division Division { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual SubjectSPN SubjectSPN { get; set; }

        public AnalyzePensionfundtonpfDataHib()
        {
            
        }

        public AnalyzePensionfundtonpfDataHib(AnalyzePensionfundtonpfReport r, AnalyzePensionfundtonpfData data)
        {
            AnalyzePensionfundtonpfReport = r;
            Id = data.Id;
            Year = data.Year;
            Kvartal = data.Kvartal;
            Total = data.Total;
        }
    }
}
