﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.DataAccess.Server.DataObjects.Analyze
{
    [HibData]
    public class AnalyzePaymentyearrateDataHib : AnalyzePaymentyearrateData
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual PaymentKind PaymentKind { get; set; }
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AnalyzePaymentyearrateReport AnalyzePaymentyearrateReport { get; set; }

    }
}
