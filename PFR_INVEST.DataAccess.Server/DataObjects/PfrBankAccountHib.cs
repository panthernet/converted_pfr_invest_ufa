﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class PfrBankAccountHib: PfrBankAccount
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntity LegalEntity { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual Currency Currency { get; set; }
       
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual AccBankType AccountType { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ReqTransfer> ReqTransfers { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<PortfolioPFRBankAccount> PFRBankAccountToPortfolios { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<AddSPNHib> AddSPNs { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ApsHib> Apss { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ApsHib> Aps_s { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<IncomesecHib> Incomesecs { get; set; }

    }
}
