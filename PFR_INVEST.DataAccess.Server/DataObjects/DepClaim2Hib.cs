﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class DepClaim2Hib : DepClaim2
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib Bank { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DepClaimSelectParamsHib Auction { get; set; }
    }
}
