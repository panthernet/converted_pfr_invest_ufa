﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class NPFPauseHib : NPFPause
    {
        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContragentHib Contragent { get; set; }
    }
}
