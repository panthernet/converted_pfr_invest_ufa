﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataAccess.Server.DataObjects
{
    [HibData]
    public class ContractHib : Contract
    {
		

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SITransferHib> TransferLists { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual LegalEntityHib LegalEntity { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<SupAgreementHib> SupAgreements { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<RewardCostHib> RewardCosts { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual IList<ContrInvestHib> ContrInvests { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual ContractNameHib ContractName { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual DocTypeHib DocType { get; set; }

        [IgnoreDataMember]
        [HibExtensionDataProperty]
        public virtual BankAccountHib BankAccount { get; set; }
    }
}
