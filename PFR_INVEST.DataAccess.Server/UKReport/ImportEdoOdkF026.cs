﻿
namespace PFR_INVEST.DataAccess.Server.UKReport
{
    using PFR_INVEST.DataAccess.Server.DataObjects;
    using PFR_INVEST.DataObjects;

    public class ImportEdoOdkF026 : ImportEdoOdkBase
    {
        public EdoOdkF026Hib F026;
        public long ID { get; set; }
    }
}
