﻿
namespace PFR_INVEST.DataAccess.Server.UKReport
{
    using System.Collections.Generic;

    using PFR_INVEST.DataAccess.Server.DataObjects;
    using PFR_INVEST.DataObjects;

    public class ImportEdoOdkF401402 : ImportEdoOdkBase
    {
        public EdoOdkF401402 F401402;
        public List<F401402UK> F401402UK = new List<F401402UK>();
    }
}
