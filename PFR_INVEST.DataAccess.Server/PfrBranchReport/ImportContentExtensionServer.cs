﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects;
using NHibernate;

namespace PFR_INVEST.DataAccess.Client.ObjectsExtensions.PfrBranchReport
{
    public class ImportContentExtensionServer
    {


        /// <summary>
        /// ServerSide Save (use Session)
        /// </summary>
        public static void Save(PfrBranchReportImportContentBase x, NHibernate.ISession session)
        {
            SaveBase(x, session);

            if (x is PfrBranchReportImportContent_CashExpenditures)
                SaveInternal(x as PfrBranchReportImportContent_CashExpenditures, session);
            else
                if (x is PfrBranchReportImportContent_Application741)
                    SaveInternal(x as PfrBranchReportImportContent_Application741, session);
                else
                    if (x is PfrBranchReportImportContent_InsuredPerson)
                        SaveInternal(x as PfrBranchReportImportContent_InsuredPerson, session);
                    else
                        if (x is PfrBranchReportImportContent_AssigneePayment)
                            SaveInternal(x as PfrBranchReportImportContent_AssigneePayment, session);
                        else
                            if (x is PfrBranchReportImportContent_CertificationAgreement)
                                SaveInternal(x as PfrBranchReportImportContent_CertificationAgreement, session);
                            else
                                if (x is PfrBranchReportImportContent_DeliveryCost)
                                    SaveInternal(x as PfrBranchReportImportContent_DeliveryCost, session);
                                else
                                    if (x is PfrBranchReportImportContent_NpfNotice)
                                        SaveInternal(x as PfrBranchReportImportContent_NpfNotice, session);
                                    else
                                        if (x is PfrBranchReportImportContent_SignatureContract)
                                            SaveInternal(x as PfrBranchReportImportContent_SignatureContract, session);
        }

        public static void SaveBase(PfrBranchReportImportContentBase x, ISession session)
        {
            //x.PfrBranchReport.ID = 
            session.SaveOrUpdate(x.PfrBranchReport);
        }


        public static void SaveInternal(PfrBranchReportImportContent_CashExpenditures x, ISession session)
        {
            x.CashExpenditures.BranchReportID = x.PfrBranchReport.ID;
            session.SaveOrUpdate(x.CashExpenditures);
        }

        public static void SaveInternal(PfrBranchReportImportContent_Application741 x, ISession session)
        {
            x.Application741.ReportId = x.PfrBranchReport.ID;
            session.SaveOrUpdate(x.Application741);
        }

        public static void SaveInternal(PfrBranchReportImportContent_AssigneePayment x, ISession session)
        {
            x.AssigneePayment.ReportId = x.PfrBranchReport.ID;
            session.SaveOrUpdate(x.AssigneePayment);
        }

        public static void SaveInternal(PfrBranchReportImportContent_InsuredPerson x, ISession session)
        {
            foreach (PfrBranchReportInsuredPerson y in x.Report)
            {
                y.ReportId = x.PfrBranchReport.ID;
                session.SaveOrUpdate(y);
            }
        }

        public static void SaveInternal(PfrBranchReportImportContent_CertificationAgreement x, ISession session)
        {
            foreach (PfrBranchReportCertificationAgreement y in x.Report)
            {
                y.ReportId = x.PfrBranchReport.ID;
                session.SaveOrUpdate(y);
            }
        }

        public static void SaveInternal(PfrBranchReportImportContent_DeliveryCost x, ISession session)
        {
            x.DeliveryCost.BranchReportID = x.PfrBranchReport.ID;
            session.SaveOrUpdate(x.DeliveryCost);
        }

        public static void SaveInternal(PfrBranchReportImportContent_NpfNotice x, ISession session)
        {
            foreach (PfrBranchReportNpfNotice y in x.Items)
            {
                y.BranchReportID = x.PfrBranchReport.ID;
                session.SaveOrUpdate(y);
            }
        }

        public static void SaveInternal(PfrBranchReportImportContent_SignatureContract x, ISession session)
        {
            foreach (PfrBranchReportSignatureContract y in x.ItemsCredit)
            {
                y.ReportID = x.PfrBranchReport.ID;
                session.SaveOrUpdate(y);
            }

            foreach (PfrBranchReportSignatureContract y in x.ItemsNpf)
            {
                y.ReportID = x.PfrBranchReport.ID;
                session.SaveOrUpdate(y);
            }
        }

        public static List<PfrBranchReportImportContentBase> GetDuplicate(PfrBranchReportImportContentBase[] items, ISession session)
        {
            List<PfrBranchReportImportContentBase> res = new List<PfrBranchReportImportContentBase>();

            foreach (var i in items)
            {
                var x = i.PfrBranchReport;
                IQuery query;
                query = session.CreateQuery(@"
					FROM PfrBranchReport x 
					WHERE x.BranchID = :bid AND
						  x.ReportTypeID = :tid AND
						  x.PeriodYear = :py AND 
						  COALESCE(x.PeriodMonth,0) = :pm AND
                          x.StatusID <> -1
				").SetInt64("bid", x.BranchID)
             .SetInt64("tid", x.ReportTypeID)
             .SetInt64("py", x.PeriodYear)
             .SetInt64("pm", x.PeriodMonth ?? 0);

                var xL = query.List<PFR_INVEST.DataObjects.PfrBranchReport>();
                if (xL.Count > 0)
                    res.Add(i);
            }

            return res;
        }

        public static void DeleteDuplicate(PfrBranchReportImportContentBase[] items, ISession session)
        {
            foreach (var i in items)
            {
                var x = i.PfrBranchReport;
                IQuery query = session.CreateQuery(@"
					UPDATE PfrBranchReport 
                    set StatusID = -1
					WHERE BranchID = :bid AND
						  ReportTypeID = :tid AND
						  PeriodYear = :py AND 
						  COALESCE(PeriodMonth,0) = :pm AND
                          StatusID <> -1
				").SetInt64("bid", x.BranchID)
                 .SetInt64("tid", x.ReportTypeID)
                 .SetInt64("py", x.PeriodYear)
                 .SetInt64("pm", x.PeriodMonth ?? 0);

                int res = query.ExecuteUpdate();
            }
        }
    }
}
