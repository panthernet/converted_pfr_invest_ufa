﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;
using db2connector.Contract;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Ribbon;
using PFR_INVEST.Core.Settings;
using PFR_INVEST.Helpers;
using PFR_INVEST.Core.Properties;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Statusbar;
using PFR_INVEST.Common.Logger;

namespace PFR_INVEST
{
	internal delegate void Invoker();
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static DashboardManager DashboardManager { get; set; }
		public static RibbonManager RibbonManager { get; set; }
		public static StatusbarManager StatusbarManager { get; set; }
		public static ServerSettings ServerSettings { get; set; }

		public static MainWindow mainWindow;
		public static Log log;
		public static bool ShowMessageBoxOnError = true;

		/// <summary>
		/// Имя файла app.config
		/// </summary>
		public static string AppConfigName => Assembly.GetExecutingAssembly().Location + ".config";

	    /// <summary>
		/// Инстанс менеджера настроек
		/// </summary>
		private static UserSettingsManager m_UserSettingsManager;
		public static UserSettingsManager UserSettingsManager => m_UserSettingsManager ?? (m_UserSettingsManager = new UserSettingsManager(AppSettings.IsUserSettingsEnabled));

	    /// <summary>
		/// Инстанс приложения
		/// </summary>
		public new static App Current => Application.Current as App;

	    public App()
		{
		    ThemesTool.Ping();

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

			SetupEnvironment();
			ApplicationInitialize = _applicationInitialize;
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
			Startup += App_Startup;
		}

	    private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			//string message = string.Format("Не найдена требуемая библиотека '{0}'", args.Name);
			//MessageBox.Show(message);
			return null;
		}

	    private static void App_Startup(object sender, StartupEventArgs e)
		{
			// The following line provides localization for data formats. 
			Thread.CurrentThread.CurrentCulture =
				new CultureInfo("ru-RU");
			// The following line provides localization for the 
			// application's user interface. 
			Thread.CurrentThread.CurrentUICulture =
				new CultureInfo("ru-RU");

			//Установка культуры для элементов WPF
			FrameworkElement.LanguageProperty.OverrideMetadata(typeof(TextBlock),
																new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

			if (Settings.Default.UpgradeSettings)
			{
				Settings.Default.Upgrade();
				Settings.Default.UpgradeSettings = false;
				// копирование значений по умолчанию в пользовательский конфигурационный файл
				foreach (SettingsPropertyValue property in Settings.Default.PropertyValues)
				{
					property.PropertyValue = property.PropertyValue;
				}

				Settings.Default.Save();
			}

		}

		private static bool m_SetupEnvironment;
		public static void SetupEnvironment()
		{
			if (m_SetupEnvironment)
				return;

			var path = (new AppUserSettingsProvider()).GetAppSettingsPath();
			var logFile = Path.Combine(path, "error.log");
			log = new Log(logFile);

			SkinManager.DisableFormSkins();
			UserLookAndFeel.Default.SetSkinStyle("Black");

			Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");

			m_SetupEnvironment = true;
		}

		void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			try
			{
				// Ошибка во внутренней логике GridSplitter, возможно необрабатываемое исключение http://jira.vs.it.ru/browse/PFRPTK-1435 http://support.microsoft.com/kb/955940/de
				if (!(e.Exception is NullReferenceException && e.Exception.StackTrace.Contains("System.Windows.Controls.GridSplitter.RemovePreviewAdorner")))
				{
					MessageBox.Show(PFR_INVEST.Properties.Resources.CommonErrorMessage,
										PFR_INVEST.Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
				}
				log.WriteException(e.Exception);
				e.Handled = true;
			}
			catch { }
		}

		void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs args)
		{
			try
			{
				var e = (Exception)args.ExceptionObject;

				MessageBox.Show(PFR_INVEST.Properties.Resources.CriticalErrorMessage,
									PFR_INVEST.Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
				log.WriteException(e);
			}
			catch { }
		}

		internal delegate void ApplicationInitializeDelegate(Splash splashWindow);
		internal ApplicationInitializeDelegate ApplicationInitialize;

		private void _applicationInitialize(Splash splashWindow)
		{
			//Thread.Sleep(2000);
			TaskHelper.Delay(2000).Wait();
			splashWindow.BeginFadeOut();

			TaskHelper.Delay(1000).Wait();

			//Task.Factory.StartNew(() => {
			//    TaskHelper.Delay(1000).Wait();


			//});


			//Thread.Sleep(1000);
			Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Invoker)(() =>
			{
				mainWindow = new MainWindow();
				if (Application.Current != null)
					mainWindow.Show();
			}));

		}

		//protected override void OnStartup(StartupEventArgs e)
		//{
		//    //
		//}

		//protected override void OnExit(ExitEventArgs e)
		//{
		//    //
		//}
		/// <summary>
		/// Приложение закрывается (был инициалзирован выход из приложения)
		/// </summary>
		public static bool IsClosing { get; set; }
	}
}
