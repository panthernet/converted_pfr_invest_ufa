namespace PFR_INVEST.Helpers
{
    public enum DevExpressColumnSortType
    {
        Int,
        Decimal,
        Month
    }
}