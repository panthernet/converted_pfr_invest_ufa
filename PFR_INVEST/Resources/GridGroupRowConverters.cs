﻿using System;
using System.Text;
using DevExpress.Xpf.Grid;
using System.Windows.Data;
using System.Windows;
using DevExpress.Xpf.Data;
using System.Windows.Media.Imaging;
using PFR_INVEST.DataObjects.Helpers;

namespace PFR_INVEST
{
    public class GroupSummaryControl : AdditionalRowContainerControl
    {
        protected override int RowHandle { get { return DataControlBase.InvalidRowHandle; } }

        public static DependencyProperty CurrentRowHandleProperty = DependencyProperty.Register("CurrentRowHandle", typeof(RowHandle), typeof(GroupSummaryControl));
        public RowHandle CurrentRowHandle
        {
            get { return (RowHandle)GetValue(CurrentRowHandleProperty); }
            set { SetValue(CurrentRowHandleProperty, value); }
        }
    }

    public class SummaryConverterBasicCurrency : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values[1] == null) return string.Empty;

            var view = values[0] as GridViewBase;
            var column = values[2] as GridColumn;
            var sb = new StringBuilder();

            foreach (var item in view.Grid.GroupSummary)
            {
                if (item.FieldName == column.FieldName)
                {
                    if (sb.Length != 0) sb.Append("\n");
                    object val = view.Grid.GetGroupSummaryValue((values[1] as RowHandle).Value, item);
                    if (val == null) continue;
                    if(!string.IsNullOrEmpty(item.DisplayFormat))
                        sb.Append(string.Format(item.DisplayFormat, val));
                    else sb.Append(string.Format("{0}", val));
                    /*if (val is double || val is float || (val is decimal && (val.ToString().Contains(',') || val.ToString().Contains('.'))))
                        sb.Append(string.Format("{0:##,##0.00}", val));
                    else sb.Append(string.Format("{0}", val));*/
                    //sb.Append(item.SummaryType.ToString() + " is " + view.Grid.GetGroupSummaryValue((values[1] as RowHandle).Value, item).ToString());
                }
            }
            return sb.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(); 
        }

        #endregion
    }
    public class MarginConverterBasicCurrencyDeepBlue : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //0 - GroupLevel
            //1 - GroupCount
            //2 - View
            //3 - ExtentWidth
            //4 - HorizontalOffset
            double width = 0;
            var view = values[2] as TableView;
            int hiddenColumnsCount = 0;
            if (view.AllowHorizontalScrollingVirtualization)
            {
                for (int i = 0; i < view.Grid.GroupCount; i++)
                {
                    width += view.VisibleColumns[i].ActualAdditionalRowDataWidth;
                    if (width > Math.Abs((double)values[3]))
                    {
                        break;
                    }
                    hiddenColumnsCount++;
                }
            }
            double left = hiddenColumnsCount > 0 ? -(int)values[0] : (int)values[1] - (int)values[0] - hiddenColumnsCount;
            return new Thickness((left * 23) - (int)values[0], 0, -16, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class MarginConverterBasicCurrencyOffice : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //0 - GroupLevel
            //1 - GroupCount
            //2 - View
            //3 - HorizontalOffset
            //4 - true if for new theme
            double width = 0;
            var view = values[2] as TableView;
            bool isForNew = values.Length > 4 && (bool) values[4];
            int hiddenColumnsCount = 0;
            if (view.AllowHorizontalScrollingVirtualization)
            {
                for (int i = 0; i < view.Grid.GroupCount; i++)
                {
                    width += view.VisibleColumns[i].ActualAdditionalRowDataWidth;
                    if (width > Math.Abs((double)values[3]))
                    {
                        break;
                    }
                    hiddenColumnsCount++;
                }
            }
            double left = hiddenColumnsCount > 0 ? -(int)values[0] : (int)values[1] - (int)values[0] - hiddenColumnsCount;
            if (isForNew) return new Thickness((left * 27) + ((int)values[0] * 27), 0, -16, 0);
            return new Thickness(left * 27 - (int)values[0], 0, -16, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class GRI_ContractsConverter : DependencyObject, IMultiValueConverter
    {

        private static readonly BitmapImage RedLight = new BitmapImage(new Uri("../Images/redlight.png", UriKind.Relative));
        private static readonly BitmapImage GreenLight = new BitmapImage(new Uri("../Images/greenlight.png", UriKind.Relative));

        #region IValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int handle = (int)values[0];
            var view = (TableView)values[1];

            if (view.Grid.IsGroupRowHandle(handle))
                handle = view.Grid.GetDataRowHandleByGroupRowHandle(handle);
            var row = (DataObjects.ListItems.SIContractListItem)view.Grid.GetRow(handle);
            if (row == null) return GreenLight;

            return ContragentHelper.IsContractMarkedRed(row.DissolutionDate) ? RedLight : GreenLight;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
