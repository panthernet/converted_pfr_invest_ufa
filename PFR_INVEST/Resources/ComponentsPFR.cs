﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DevExpress.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using DevExpress.XtraEditors.Repository;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Views;

namespace PFR_INVEST
{
	#region DateEditFixed
	//Оверрайд компонента даты, который исключает значение минимальной даты заменяя его на текущую или на NullValue если задано
	public class DateEditFixed : DateEdit
	{
		public new static readonly DependencyProperty EditValueProperty =
	        DependencyProperty.Register("EditValue", typeof(object), typeof(DateEditFixed), new FrameworkPropertyMetadata(AdjustMyEditValue) { BindsTwoWayByDefault = true });


	    public new DateTime DateTime
	    {
	        get { return string.IsNullOrEmpty(Text) ? DateTime.MinValue : base.DateTime; }
	        set { base.DateTime = value; }
	    }

	    public DateEditFixed()
		{
			if (NullValue == null) NullValue = "";
			Loaded += DateEditFixed_Loaded;
			Spin += DateEditFixed_Spin;
			EditValueChanged += DateEditEx_EditValueChanged;
			RepositoryItem.EditValueChangedFiringDelay = 1;
		}

	    private void DateEditEx_EditValueChanged(object sender, EditValueChangedEventArgs e)
		{
			EditValue = e.NewValue;
		}

	    private static void DateEditFixed_Spin(object sender, SpinEventArgs e)
		{
			e.Handled = true;
		}

	    private void DateEditFixed_Loaded(object sender, RoutedEventArgs e)
		{
			if (DashboardManager.Instance != null)
			{
				var vmc = DashboardManager.Instance.GetActiveViewModel() as ViewModelCard;
				if (vmc != null && (vmc.State == ViewModelState.Edit || vmc.State == ViewModelState.Read)) return;
			}
			if (DateTime.Year == 1)
			{

				if (NullValue != null && !string.Empty.Equals(NullValue))
					EditValue = NullValue;
			}
			else EditValue = DateTime;
		}

		[Category("Common Properties")]
		[TypeConverter(typeof(ObjectConverter))]
		[Description("Gets or sets the editor's value. This is a dependency property.")]
		public new object EditValue
		{
			get
			{
				object x = GetValue(EditValueProperty);
			    var s = x as string;
			    if (s != null && string.IsNullOrEmpty(s))
					return null;
				return x;
			}
			set
			{
				if (value is DateTime && ((DateTime)value) != DateTime.MinValue && ((DateTime)value).Kind != DateTimeKind.Unspecified)
					value = DateTime.SpecifyKind((DateTime)value, DateTimeKind.Unspecified);
				SetValue(EditValueProperty, value);
			}
		}

		public object OldEditValue
		{
			get { return base.EditValue; }
			set { base.EditValue = value; }
		}

		private void SetEditValue(object p)
		{
			OldEditValue = p;
		}

		private static void AdjustMyEditValue(DependencyObject source, DependencyPropertyChangedEventArgs e)
		{
			((DateEditFixed) source).SetEditValue(e.NewValue);
		}

	}

	#endregion

	#region MultiLabel
	//Лейбл, выводит сразу несколько строк с переносом слов
	public class MultiLabel : TextBox
	{
		public MultiLabel()
		{
			TextWrapping = TextWrapping.Wrap;
			IsHitTestVisible = false;
			IsReadOnly = true;
			BorderThickness = new Thickness(0);
			Background = Brushes.Transparent;
		}
	}
	#endregion

	public static class ComponentHelper
	{

	    public static void OnColumnPopupDecimal(object sender, FilterPopupEventArgs e)
	    {
            e.ComboBoxEdit.ItemsSource = ((List<object>)e.ComboBoxEdit.ItemsSource).OrderBy(a =>
            {
                var aItem = a as CustomComboBoxItem;
                if (aItem == null) return 0;
                var value = aItem.EditValue;
                decimal result;
                return !decimal.TryParse(value.ToString(), out result) ? 0 : result;
            });
	    }

        public static void OnColumnPopupInteger(object sender, FilterPopupEventArgs e)
        {
            e.ComboBoxEdit.ItemsSource = ((List<object>)e.ComboBoxEdit.ItemsSource).OrderBy(a =>
            {
                var aItem = a as CustomComboBoxItem;
                if (aItem == null) return 0;
                var value = aItem.EditValue;
                int result;
                return !int.TryParse(value.ToString(), out result) ? 0 : result;
            });
        }

        public static int OnColumnCustomSort(object sender, CustomColumnSortEventArgs e)
		{
			//month sorting
			if (e.Column.Name.ToLower().Contains("custommonth"))
			{
				var v1 = e.Value1?.ToString() ?? string.Empty;
				var v2 = e.Value2?.ToString() ?? string.Empty;
				int index1 = DateUtil.GetMonthIndex(v1.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(), e.SortOrder == ColumnSortOrder.Descending);
				int index2 = DateUtil.GetMonthIndex(v2.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(), e.SortOrder == ColumnSortOrder.Descending);
				return e.SortOrder == ColumnSortOrder.Descending ? index2.CompareTo(index1) : index1.CompareTo(index2);
			}
			return 0;
		}

	    public static List<string> GetMonthList()
	    {
	        return new List<string> {"Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"};
	    }

		public static void OnColumnFilterOptionsSort(object sender, FilterPopupEventArgs e)
		{
			//month sorting
			if (e.Column.Name.ToLower().Contains("custommonth"))
			{
				var source = e.ComboBoxEdit.ItemsSource as List<object>;
				var sortedItems = new List<CustomComboBoxItem>();
				var commonItems = new List<CustomComboBoxItem>();
				var trailingItems = new List<CustomComboBoxItem>();
				foreach (var o in source)
				{
					var item = o as CustomComboBoxItem;
				    var displayvalue = item?.DisplayValue as string;
				    if (displayvalue != null)
				    {
				        if (Regex.IsMatch(displayvalue, "^\\(.*\\)$"))
				        {
				            commonItems.Add(item);
				        }
				        else if (DateUtil.GetMonthIndex(GetFirstWord(displayvalue)) == -1)
				        {
				            if (!trailingItems.Exists(x => displayvalue.Equals(x.DisplayValue as string)))
				                trailingItems.Add(item);
				        }
				        else if (!sortedItems.Exists(x => displayvalue.Equals(x.DisplayValue as string)))
				        {
				            sortedItems.Add(item);
				        }
				    }
				}
				
				sortedItems.Sort((x, y) => DateUtil.GetMonthIndex(GetFirstWord(x.DisplayValue as string), e.Column.SortOrder == ColumnSortOrder.Descending) - DateUtil.GetMonthIndex(GetFirstWord(y.DisplayValue as string), e.Column.SortOrder == ColumnSortOrder.Descending));
				commonItems.AddRange(sortedItems);
				commonItems.AddRange(trailingItems);
				e.ComboBoxEdit.ItemsSource = commonItems;
			}

            //month sort and replace on popup
            if (e.Column.Name.ToLower().Contains("custommonthindex"))
            {
                var source = e.ComboBoxEdit.ItemsSource as List<object>;
                var sourceTyped = e.ComboBoxEdit.ItemsSource as List<CustomComboBoxItem>;
                var sortedItems = new List<CustomComboBoxItem>();
                var commonItems = new List<CustomComboBoxItem>();
                if (source != null)
                {
                    foreach (var o in source)
                    {
                        var item = o as CustomComboBoxItem;
                        if (item != null)
                        {
                            var displayvalue = item.DisplayValue as string;
                            if (displayvalue != null && Regex.IsMatch(displayvalue, "^\\(.*\\)$"))
                            {
                                commonItems.Add(item);
                            }
                            else
                            {
                                if (!sortedItems.Exists(x => item.EditValue.Equals(x.EditValue)))
                                {
                                    sortedItems.Add(item);
                                }
                            }
                        }
                    }
                }

                if (sourceTyped != null)
                {
                    foreach (var item in sourceTyped)
                    {
                        if (item != null)
                        {
                            var displayvalue = item.DisplayValue as string;
                            if (displayvalue != null && Regex.IsMatch(displayvalue, "^\\(.*\\)$"))
                            {
                                commonItems.Add(item);
                            }
                            else
                            {
                                if (!sortedItems.Exists(x => item.EditValue.Equals(x.EditValue)))
                                {
                                    sortedItems.Add(item);
                                }
                            }
                        }
                    }
                }

                sortedItems.Sort((x, y) => Convert.ToInt32(x.EditValue) - Convert.ToInt32(y.EditValue));
                sortedItems.ForEach(x => x.DisplayValue = DateUtil.GetMonthName(Convert.ToInt32(x.EditValue) - 1));
                if (e.Column.SortOrder == ColumnSortOrder.Descending) sortedItems.Reverse();
                commonItems.AddRange(sortedItems);
                e.ComboBoxEdit.ItemsSource = commonItems;
            }
        }

		private static string GetFirstWord(string monthRecord)
		{
		    if (monthRecord == null) return string.Empty;
		    var m = Regex.Match(monthRecord, "^\\w+");
		    return m.Success ? m.Value : string.Empty;
		}
	}
}
