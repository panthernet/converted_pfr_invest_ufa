﻿using System.Windows;
using DevExpress.Xpf.Editors;
using Microsoft.Win32;


namespace PFR_INVEST.Resources.Controls
{
	public class FolderPicker : ButtonEdit
	{
	    private PickerMode _mode = PickerMode.Folder;

        public string FilterString { get; set; }

	    public string DefaultFileName
	    {
            get { return (string)GetValue(DefaultFileNameProperty); }
            set { SetValue(DefaultFileNameProperty, value); }
	    }

        public static readonly DependencyProperty DefaultFileNameProperty =
            DependencyProperty.Register("DefaultFileName", typeof(string), typeof(FolderPicker), new UIPropertyMetadata(string.Empty));


	    public PickerMode Mode
	    {
	        get { return _mode; }
	        set { _mode = value; }
	    }

	    public string PromtText
		{
			get { return (string)GetValue(PromtTextProperty); }
			set { SetValue(PromtTextProperty, value); }
		}

		// Using a DependencyProperty as the backing store for PromtText.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PromtTextProperty =
			DependencyProperty.Register("PromtText", typeof(string), typeof(FolderPicker), new UIPropertyMetadata(string.Empty));

		public FolderPicker()
		{
			IsTextEditable = false;
			DefaultButtonClick += FolderPicker_DefaultButtonClick;
		}

	    private void FolderPicker_DefaultButtonClick(object sender, RoutedEventArgs e)
		{
	        if (_mode == PickerMode.Folder)
	        {
	            var dlg = new System.Windows.Forms.FolderBrowserDialog {Description = PromtText};
	            var result = dlg.ShowDialog();
	            if (result == System.Windows.Forms.DialogResult.OK)
	                EditValue = dlg.SelectedPath;
	            return;
	        }
	        if (_mode == PickerMode.SaveFile)
	        {
	            var dlg = new SaveFileDialog {Filter = FilterString, FileName = DefaultFileName};
	            if (dlg.ShowDialog() == true)
	                EditValue = dlg.FileName;
	            return;
	        }

            if (_mode == PickerMode.SaveFile)
            {
                var dlg = new OpenFileDialog { Filter = FilterString, FileName = DefaultFileName };
                if (dlg.ShowDialog() == true)
                    EditValue = dlg.FileName;
                return;
            }
		}

	    public enum PickerMode
	    {
	        Folder,
            SaveFile,
            OpenFile
	    }
	}
}
