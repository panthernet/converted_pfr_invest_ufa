﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Resources.Controls
{
    /// <summary>
    /// Interaction logic for FIOPicker.xaml
    /// </summary>
    public partial class FIOPicker : UserControl
    {
        public FIOPicker()
        {
            InitializeComponent();
        }



        public FIO SelectedFIO
        {
            get { return (FIO)GetValue(SelectedFIOProperty); }
            set { SetValue(SelectedFIOProperty, value); }
        }

        public static readonly DependencyProperty SelectedFIOProperty =
            DependencyProperty.Register("SelectedFIO", typeof(FIO), typeof(FIOPicker), new UIPropertyMetadata(null, new PropertyChangedCallback((sender, e) =>
            {
                var ctl = sender as FIOPicker;
                if (ctl != null)
                {
                    var fio = e.NewValue as FIO;
                    if (fio == null)
                        ctl.beText.EditValue = string.Empty;
                    else
                        ctl.beText.EditValue = fio.Name;
                }

            })));

        private void beText_DefaultButtonClick(object sender, RoutedEventArgs e)
        {
            var dlg = new LookupFIOGridDlg();
            var dlgVM = new LookupFIOGridDlgViewModel();
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog() == true)
                this.SelectedFIO = dlgVM.SelectedFIO;
        }

        private void cmdClear_Click(object sender, RoutedEventArgs e)
        {
            this.SelectedFIO = null;
        }


    }
}
