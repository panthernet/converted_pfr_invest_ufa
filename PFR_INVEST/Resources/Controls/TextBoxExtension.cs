﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Editors;

namespace PFR_INVEST.Resources.Controls
{
	/// <summary>
	/// Расширение функциональности для TextBox, используется из разметки
	/// </summary>
	public class TextBoxExtension : DependencyObject
	{

		#region FocusToZero

		public static readonly DependencyProperty FocusToZeroProperty = DependencyProperty.RegisterAttached(
																				"FocusToZero",
																				typeof(bool),
																				typeof(TextBoxExtension),
																				new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, FocusToZeroChanged));
		public static void SetFocusToZero(UIElement element, bool value)
		{
			element.SetValue(FocusToZeroProperty, value);
		}
		public static bool GetFocusToZero(UIElement element)
		{
			return (bool)element.GetValue(FocusToZeroProperty);
		}

		private static void FocusToZeroChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var text = (TextEdit)sender;
			if (sender == null) return;

			if ((bool)e.OldValue == false && (bool)e.NewValue)
			{
				text.KeyDown += TextEdit_KeyDown;
			}
			if ((bool)e.OldValue && (bool)e.NewValue == false)
			{
				text.KeyDown -= TextEdit_KeyDown;
			}
		}

		static void TextEdit_KeyDown(object sender, KeyEventArgs e)
		{
			var text = (TextEdit)sender;
			if (sender == null || text.EditValue == null || (text.EditValue is string && string.IsNullOrEmpty((string)text.EditValue))) return;

			//иногда случается баг и цифра идёт текстом в другой локале
            // -> поэтому парсим через NumberFormatInfo
			decimal? val = Convert.ToDecimal(text.EditValue, new NumberFormatInfo());

			if (val == 0)
			{
				var start = text.DisplayText.IndexOf(text.MaskCulture.NumberFormat.CurrencyDecimalSeparator, StringComparison.Ordinal);

			    if (start != 1 || text.SelectionStart > 1) return;
			    text.SelectionStart = 1;
			    text.SelectionLength = 0;
			}
		}

		#endregion FocusToZero


		#region PasteNumberFilter

		public static readonly DependencyProperty PasteNumberFilterProperty = DependencyProperty.RegisterAttached(
																				"PasteNumberFilter",
																				typeof(bool),
																				typeof(TextBoxExtension),
																				new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, PasteNumberFilterChanged));
		public static void SetPasteNumberFilter(UIElement element, bool value)
		{
			element.SetValue(PasteNumberFilterProperty, value);
		}
		public static bool GetPasteNumberFilter(UIElement element)
		{
			return (bool)element.GetValue(PasteNumberFilterProperty);
		}

		private static void PasteNumberFilterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var text = (TextEdit)sender;
			if (sender == null) return;

			if ((bool)e.OldValue == false && (bool)e.NewValue)
			{
				text.EditValueChanging += TextEdit_EditValueChanging;
				text.EditValueChanged += text_EditValueChanged;

			}
			if ((bool)e.OldValue && (bool)e.NewValue == false)
			{
				text.EditValueChanging -= TextEdit_EditValueChanging;
			}
		}

		static void text_EditValueChanged(object sender, EditValueChangedEventArgs e)
		{

		}

		static void TextEdit_EditValueChanging(object sender, EditValueChangingEventArgs e)
		{

		}



		#endregion PasteNumberFilter



	}
}
