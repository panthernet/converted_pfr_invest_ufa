﻿using System;
using System.Windows.Data;
using System.Windows;
using System.Globalization;

namespace PFR_INVEST
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ("invert".Equals(parameter as string, StringComparison.OrdinalIgnoreCase))
                return (!(bool)value) ? Visibility.Visible : Visibility.Hidden;
            return ((bool)value) ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
