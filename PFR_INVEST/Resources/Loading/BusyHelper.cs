﻿using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST
{
    public class BusyHelper:IBusyHelper
    {
        public void Start()
        {
            Loading.ShowWindow();
        }

        public void Start(string message)
        {
            Loading.ShowWindow(message);
        }

        public void Stop()
        {
            Loading.CloseWindow();
        }
    }
}
