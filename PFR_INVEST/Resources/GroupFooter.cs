using DevExpress.Xpf.Data;
using DevExpress.Xpf.Grid;
using System.Windows;
using System.Windows.Controls;

namespace PFR_INVEST
{
    public class GroupFooter : Control
    {
        public static readonly DependencyProperty GroupRowDataProperty =
            DependencyProperty.Register("GroupRowData", typeof(FooterGroupRowData), typeof(GroupFooter), new PropertyMetadata(null));
        public static readonly DependencyProperty RowHandleProperty =
            DependencyProperty.Register("RowHandle", typeof(RowHandle), typeof(GroupFooter), new PropertyMetadata(new RowHandle(DataControlBase.InvalidRowHandle), (d, e) => ((GroupFooter)d).RefreshContent()));

        public GroupFooter()
        {
            DefaultStyleKey = typeof(GroupFooter);
        }

        public FooterGroupRowData GroupRowData
        {
            get { return (FooterGroupRowData)GetValue(GroupRowDataProperty); }
            set { SetValue(GroupRowDataProperty, value); }
        }
        public RowHandle RowHandle
        {
            get { return (RowHandle)GetValue(RowHandleProperty); }
            set { SetValue(RowHandleProperty, value); }
        }

        public TableViewFooterGroup ViewFooterGroup { get { return DataContext == null ? null : (TableViewFooterGroup)((RowData)DataContext).View; } }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            RefreshContent();
        }
        public bool IsLastRowInGroup(int groupRowHandle)
        {
            GridControl gridControl = ViewFooterGroup.DataControl as GridControl;
            if (!gridControl.IsValidRowHandle(groupRowHandle))
                return false;
            int childrenCount = gridControl.GetChildRowCount(groupRowHandle);
            int lastRowHandle = gridControl.GetChildRowHandle(groupRowHandle, childrenCount - 1);
            return gridControl.GetRowVisibleIndexByHandle(lastRowHandle) <= gridControl.GetRowVisibleIndexByHandle(RowHandle.Value);
        }
        public void RefreshContent()
        {
            if (ViewFooterGroup == null)
                return;
            RowHandle groupRowHandle = GetParentRowHandle();
            ViewFooterGroup.AddToCacheGroupFooter(this);
            UpdateVisibility(groupRowHandle);
            if (Visibility == Visibility.Collapsed)
                return;
            UpdateGroupFooterSummaryContent(groupRowHandle);
            UpdateOffset();
        }
        void UpdateOffset()
        {
            double offset = ViewFooterGroup.LeftGroupAreaIndent * ((GridControl)ViewFooterGroup.DataControl).GroupCount;
            Margin = new Thickness(-offset, 0, 0, 0);
        }
        void UpdateGroupFooterSummaryContent(RowHandle groupRowHandle)
        {
            if (GroupRowData == null)
                GroupRowData = ViewFooterGroup.CreateGroupRowData(groupRowHandle);
            else
                ViewFooterGroup.UpdateGroupRowData(GroupRowData, groupRowHandle);
        }
        RowHandle GetParentRowHandle()
        {
            return new RowHandle(((GridControl)ViewFooterGroup.DataControl).GetParentRowHandle(RowHandle.Value));
        }
        void UpdateVisibility(RowHandle groupRowHandle)
        {
            Visibility = IsLastRowInGroup(groupRowHandle.Value) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
