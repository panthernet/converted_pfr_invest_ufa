﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using DevExpress.Xpf.Editors;

namespace PFR_INVEST.Extensions
{
	public static class DispatcherExtensions
	{
		/// <summary>
		/// Вызов проверки и удаления начального нуля при ручном вводе числа в поле на форме
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void RemoveTextLeadingZero(this Dispatcher ds, object sender, EditValueChangedEventArgs e)
		{
			ds.BeginInvoke(new Action(() =>
				{
					var editor = (TextEdit)sender;
					if (editor == null || editor.EditValue == null) return;
					var v = Convert.ToDecimal(editor.EditValue);
					// проверка ввода первой цифры, удаление следующего за ней нуля
					if (v > 9 && Convert.ToDecimal(e.OldValue) == 0 && v < 91 && v % 10 == 0)
					{
						editor.EditValue = System.Convert.ToString(editor.EditValue).Replace("0", "");
						editor.CaretIndex = 1;
					}
				}));
		}
	}
}
