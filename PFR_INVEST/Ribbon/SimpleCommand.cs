﻿using System;
using System.Windows;
using System.Windows.Input;

namespace PFR_INVEST.Ribbon
{
    public sealed class SimpleRoutedCommand : RoutedCommand
    {
        private CommandBinding _commandBinding;
        private CanExecuteRoutedEventHandler _canExecute;
        private ExecutedRoutedEventHandler _executed;

        public SimpleRoutedCommand(string name)
            :base(name, typeof(MainWindow))
        {
        }

        public SimpleRoutedCommand()
            : base(Guid.NewGuid().ToString(), typeof(MainWindow))
        {
        }

        public event ExecutedRoutedEventHandler Executed
        {
            add
            {
                if (_commandBinding == null)
                {
                    _commandBinding = new CommandBinding(this, value, _canExecute);
                    CommandManager.RegisterClassCommandBinding(typeof(MainWindow), _commandBinding);
                }
                else
                {
                    _commandBinding.Executed -= value;
                    _commandBinding.Executed += value;
                }
                _executed = value;
            }
            remove
            {
                if (_commandBinding != null)
                    _commandBinding.Executed -= value;
                _executed = null;
            }
        }

        public event CanExecuteRoutedEventHandler CanExecute
        {
            add
            {
                if (_commandBinding == null)
                {
                    _commandBinding = new CommandBinding(this, _executed, value);
                    CommandManager.RegisterClassCommandBinding(typeof(MainWindow), _commandBinding);
                }
                else
                {
                    _commandBinding.CanExecute -= value;
                    _commandBinding.CanExecute += value;
                }
                _canExecute = value;
            }
            remove
            {
                if (_commandBinding != null)
                    _commandBinding.CanExecute -= value;
                _canExecute = null;
            }
        }
    }

    public sealed class SimpleCommand : ICommand
    {
        public Predicate<object> CanExecute { get; set; }
        public Action<object, ICommand> Executed { get; set; }

        public string Name { get; private set; }

        public SimpleCommand(string name)
        {
            Name = name;
        }

        public SimpleCommand(): this(Guid.NewGuid().ToString())
        {
        }

        #region ICommand Members

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute == null || CanExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute()
        {
            Execute(null);
        }

        public void Execute(object parameter)
        {
            if (Executed == null) return;
            Executed(parameter, this);
        }

        #endregion
    }

}
