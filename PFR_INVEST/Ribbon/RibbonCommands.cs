﻿using Microsoft.Windows.Controls.Ribbon;
using System.Collections.Generic;
using System.Windows.Input;

namespace PFR_INVEST.Ribbon
{
    public static class RibbonCommands
    {
        private static readonly Dictionary<string, ICommand> CommandList = new Dictionary<string, ICommand>(500);

        /// <summary>
        /// Получение SimpleCommand. Используется, если требуется проверка command.CanExecute(null) ручным вызовом. 
        /// Риббон команды могут глючить в этом случае.
        /// </summary>
        /// <param name="name">Название команды</param>
        private static SimpleCommand GetCommand3(string name)
        {
            if (!CommandList.ContainsKey(name))
                CommandList[name] = new SimpleCommand(name);
            return (SimpleCommand)CommandList[name];
        }

        //TODO заменить все риббон комманды на новые вариации
        /// <summary>
        /// Упрощенная ультернатива RibbonCommand
        /// </summary>
        /// <param name="name">Название команды</param>
        private static SimpleRoutedCommand GetCommand2(string name)
        {
            if (!CommandList.ContainsKey(name))
                CommandList[name] = new SimpleRoutedCommand(name);
            return (SimpleRoutedCommand)CommandList[name];
        }

        private static RibbonCommand GetCommand(string name)
        {
            if (!CommandList.ContainsKey(name))
                CommandList[name] = new RibbonCommand(name, typeof(RibbonCommand));
            return (RibbonCommand)CommandList[name];
        }

        public static RibbonCommand ShowOpfrPPList => GetCommand("ShowOpfrPPList");
        public static RibbonCommand CreateOpfrPP => GetCommand("CreateOpfrPP");
        public static RibbonCommand SelectOpfrPP => GetCommand("SelectOpfrPP");



        public static RibbonCommand ShowReportCHFR => GetCommand("ShowReportCHFR");
        public static RibbonCommand RecalcUKReqTransfers => GetCommand("RecalcUKReqTransfers");
        public static RibbonCommand OpfrExport => GetCommand("OpfrExport");
        public static RibbonCommand OpfrImport => GetCommand("OpfrImport");
        public static RibbonCommand CreateOpfrTransfer => GetCommand("CreateOpfrTransfer");
        public static RibbonCommand CreateOpfrRegister => GetCommand("CreateOpfrRegister");
        public static RibbonCommand CreateOpfrRequest => GetCommand("CreateOpfrRequest");
        public static RibbonCommand ShowOpfrList => GetCommand("ShowOpfrList");
        public static RibbonCommand CBReportList => GetCommand("CBReportList");
        public static RibbonCommand CBReportCreate => GetCommand("CBReportCreate");
        public static RibbonCommand CBReportExport => GetCommand("CBReportExport");
        public static RibbonCommand CBReportSettingsPaymentDetail => GetCommand("CBReportSettingsPaymentDetail");
        public static RibbonCommand ShowInvDecl => GetCommand("ShowInvDecl");
        public static RibbonCommand ShowGarantFond => GetCommand("ShowGarantFond");
        public static RibbonCommand PrintYearPayment => GetCommand("PrintYearPayment");
        public static RibbonCommand LetterToUK => GetCommand("LetterToUK");
        public static RibbonCommand LetterToUKs => GetCommand("LetterToUKs");
        public static RibbonCommand LetterToNPF => GetCommand("LetterToNPF");
        public static RibbonCommand LetterToNPFs => GetCommand("LetterToNPFs");
        public static RibbonCommand MFLetterPortfolioStructure => GetCommand("MFLetterPortfolioStructure");
        public static RibbonCommand MFLetterPreSumm => GetCommand("MFLetterPreSumm");
        public static RibbonCommand MFLetterSellPlan => GetCommand("MFLetterSellPlan");
        public static RibbonCommand MFLetterFactYield => GetCommand("MFLetterFactYield");
        public static RibbonCommand MFLetterDepositSumm => GetCommand("MFLetterDepositSumm");
        public static RibbonCommand ShowPortfolioStatusList => GetCommand("ShowPortfolioStatusList");
        public static RibbonCommand ReportImportSI => GetCommand("ReportImportSI");

        public static RibbonCommand ReportExportSI => GetCommand("ReportExportSI");
        public static RibbonCommand RollbackStatus => GetCommand("RollbackStatus");
        public static RibbonCommand CreateBankAgent => GetCommand("CreateBankAgent");
        public static RibbonCommand ShowBankAgentsList => GetCommand("ShowBankAgentsList");
        public static RibbonCommand StopCO => GetCommand("StopCO");
        public static RibbonCommand ResumeCO => GetCommand("ResumeCO");
        public static RibbonCommand StopSI => GetCommand("StopSI");
        public static RibbonCommand ResumeSI => GetCommand("ResumeSI");
        public static RibbonCommand RestoreDocument => GetCommand("RestoreDocument");
        public static RibbonCommand RecalcUKPortfolios => GetCommand("RecalcUKPortfolios");
        public static RibbonCommand RecalcNKD => GetCommand("RecalcNKD");
        public static RibbonCommand ShowTransferWizard => GetCommand("ShowTransferWizard");
        public static RibbonCommand ShowYearPlanWizard => GetCommand("ShowYearPlanWizard");
        public static RibbonCommand ShowPlanCorrectionWizard => GetCommand("ShowPlanCorrectionWizard");
        public static RibbonCommand ShowRopsAsvTransferCreationWizard => GetCommand("ShowRopsAsvTransferCreationWizard");
        public static RibbonCommand ShowMonthTransferCreationWizard => GetCommand("ShowMonthTransferCreationWizard");
        public static RibbonCommand SendEmailsToUK => GetCommand("SendEmailsToUK");
        public static RibbonCommand ShowDBFSettings => GetCommand("ShowDBFSettings");
        public static RibbonCommand ShowBDateXmlImport => GetCommand("ShowBDateXmlImport");
        public static RibbonCommand ShowMarketPricesList => GetCommand("ShowMarketPricesList");
        public static RibbonCommand ShowBoardList => GetCommand("ShowBoardList");
        public static RibbonCommand CreateSIDocument => GetCommand("CreateSIDocument");
        public static RibbonCommand CreateLinkedDocument => GetCommand("CreateLinkedDocument");
        public static RibbonCommand AddMarketPrice => GetCommand("AddMarketPrice");
        public static RibbonCommand FromUKEnterRequirementData => GetCommand("FromUKEnterRequirementData");
        public static RibbonCommand FromUKEnterGetRequirementDate => GetCommand("FromUKEnterGetRequirementDate");
        public static RibbonCommand ToUKEnterTransferActData => GetCommand("ToUKEnterTransferActData");
        public static RibbonCommand FromUKEnterTransferActData => GetCommand("FromUKEnterTransferActData");
        public static RibbonCommand CreatePrintReq => GetCommand("CreatePrintReq");
        public static RibbonCommand PrintAPSIYearPlan => GetCommand("PrintAPSIYearPlan");
        public static RibbonCommand PrintAPSIMonthPlan => GetCommand("PrintAPSIMonthPlan");
        public static RibbonCommand LoadODKReports => GetCommand("LoadODKReports");
        public static RibbonCommand LoadEDOReports => GetCommand("LoadEDOReports");
        public static RibbonCommand Line030Calculation => GetCommand("Line030Calculation");
        public static RibbonCommand ShowMQSettings => GetCommand("ShowMQSettings");
        public static RibbonCommand ShowOVSITransferList => GetCommand("ShowOVSITransferList");
        public static RibbonCommand CreateSIRequest => GetCommand("CreateSIRequest");
        public static RibbonCommand LetterReqTransferSPNOneTime => GetCommand("LetterReqTransferSPNOneTime");
        public static RibbonCommand LetterReqTransferSPNMSK => GetCommand("LetterReqTransferSPNMSK");
        public static RibbonCommand LetterReqTransferSPNAccum => GetCommand("LetterReqTransferSPNAccum");
        public static RibbonCommand LetterReqTransferSPNUrgent => GetCommand("LetterReqTransferSPNUrgent");
        public static RibbonCommand LetterReqTransferSPNFlow => GetCommand("LetterReqTransferSPNFlow");
        public static RibbonCommand LetterReqTransferSPNOwners => GetCommand("LetterReqTransferSPNOwners");
        public static RibbonCommand LetterReqTransferMoneyROPS => GetCommand("LetterReqTransferMoneyROPS");
        public static RibbonCommand LetterReqTransferMoneyASV => GetCommand("LetterReqTransferMoneyASV");
        public static RibbonCommand LetterTransferSPNAccumOneTime => GetCommand("LetterTransferSPNAccumOneTime");
        public static RibbonCommand LetterTransferSPNAccumZero => GetCommand("LetterTransferSPNAccumZero");
        public static RibbonCommand LetterTransferSPNAccumUrgent => GetCommand("LetterTransferSPNAccumUrgent");

        public static RibbonCommand CreateOVSITransfer => GetCommand("CreateOVSITransfer");
        public static RibbonCommand ShowDemandList => GetCommand("ShowDemandList");
        public static RibbonCommand ShowF402PFRActions => GetCommand("ShowF402PFRActions");
        public static RibbonCommand ShowOwnFundsList => GetCommand("ShowOwnFundsList");
        public static RibbonCommand AddOwnFunds => GetCommand("AddOwnFunds");
        public static RibbonCommand ShowF80List => GetCommand("ShowF80List");
        public static RibbonCommand ShowF40List => GetCommand("ShowF40List");
        public static RibbonCommand ShowF402List => GetCommand("ShowF402List");
        public static RibbonCommand ShowF50List => GetCommand("ShowF50List");
        public static RibbonCommand ShowViolationCategoriesList => GetCommand("ShowViolationCategoriesList");
        public static RibbonCommand CreateViolationCategory => GetCommand("CreateViolationCategory");
        public static RibbonCommand CreateDirection => GetCommand("CreateDirection");
        public static RibbonCommand CreateReturn => GetCommand("CreateReturn");
        public static RibbonCommand CreateTransfer => GetCommand("CreateTransfer");
        public static RibbonCommand СreateCosts => GetCommand("СreateCosts");
        //тут, возможно русская С
        public static RibbonCommand ShowSchilsCostsList => GetCommand("ShowSchilsCostsList");
        public static RibbonCommand ShowCostsList => GetCommand("ShowCostsList");
        public static RibbonCommand ShowBalance => GetCommand("ShowBalance");
        public static RibbonCommand ShowYieldsList => GetCommand("ShowYieldsList");
        public static RibbonCommand ShowF60List => GetCommand("ShowF60List");
        public static RibbonCommand ShowF70List => GetCommand("ShowF70List");
        public static RibbonCommand ShowUsersList => GetCommand("ShowUsersList");
        public static RibbonCommand ShowRolesList => GetCommand("ShowRolesList");
        public static RibbonCommand AddUser => GetCommand("AddUser");
        public static RibbonCommand DeleteUser => GetCommand("DeleteUser");
        public static RibbonCommand CreateAccountTransfer => GetCommand("CreateAccountTransfer");
        public static RibbonCommand CreateAccountWithdrawal => GetCommand("CreateAccountWithdrawal");
        public static RibbonCommand CreateEnrollmentOther => GetCommand("CreateEnrollmentOther");
        public static SimpleCommand BanksVerification => GetCommand3("BanksVerification");
        public static SimpleCommand BuildLimits => GetCommand3("BuildLimits");
        public static SimpleCommand ExportDepClaimSelectParams => GetCommand3("ExportDepClaimSelectParams");
        public static RibbonCommand CreateEnrollmentPercents => GetCommand("CreateEnrollmentPercents");
        public static RibbonCommand CreateDueUndistributedAccurateDSV => GetCommand("CreateDueUndistributedAccurateDSV");
        public static RibbonCommand CreateDueUndistributedAccurate => GetCommand("CreateDueUndistributedAccurate");
        public static RibbonCommand CreateDueUndistributed => GetCommand("CreateDueUndistributed");
        public static RibbonCommand CreateDueUndistributedDSV => GetCommand("CreateDueUndistributedDSV");
        public static RibbonCommand CreateBudgetCorrection => GetCommand("CreateBudgetAccurate");
        public static RibbonCommand CreateBudget => GetCommand("CreateBudget");
        public static RibbonCommand CreateDueDeadAccurateDSV => GetCommand("CreateDueDeadAccurateDSV");
        public static RibbonCommand CreateDueDeadAccurate => GetCommand("CreateDueDeadAccurate");
        public static RibbonCommand CreateDueDead => GetCommand("CreateDueDead");
        public static RibbonCommand CreateDueDeadDSV => GetCommand("CreateDueDeadDSV");
        public static RibbonCommand CreateDueExcessAccurateDSV => GetCommand("CreateDueExcessAccurateDSV");
        public static RibbonCommand CreateDueExcessAccurate => GetCommand("CreateDueExcessAccurate");
        public static RibbonCommand CreateDueExcess => GetCommand("CreateDueExcess");
        public static RibbonCommand CreateDueExcessDSV => GetCommand("CreateDueExcessDSV");
        public static RibbonCommand CreatePrepaymentAccurate => GetCommand("CreatePrepaymentAccurate");
        public static RibbonCommand CreatePrepayment => GetCommand("CreatePrepayment");
        public static RibbonCommand CreateMonthAccurateDSV => GetCommand("CreateMonthAccurateDSV");
        public static RibbonCommand CreateDueDSV => GetCommand("CreateDueDSV");
        public static RibbonCommand CreateTreasuryDoc => GetCommand("CreateTreasuryDoc");
        public static RibbonCommand CreateMonthAccurate => GetCommand("CreateMonthAccurate");
        public static RibbonCommand CreateQuarterAccurate => GetCommand("CreateQuarterAccurate");
        public static RibbonCommand CreateQuarterAccurateDSV => GetCommand("CreateQuarterAccurateDSV");
        public static RibbonCommand CreateDue => GetCommand("CreateDue");
        public static RibbonCommand CreateIncomeSecurities => GetCommand("CreateIncomeSecurities");
        public static RibbonCommand CreateInsurance => GetCommand("CreateInsurance");
        public static RibbonCommand CreatePenalty => GetCommand("CreatePenalty");
        // public static RibbonCommand CreatePrecisePenalty { get { return GetCommand("CreatePrecisePenalty"); } }


        public static RibbonCommand ShowTempAllocationList => GetCommand("ShowTempAllocationList");
        public static RibbonCommand ShowIncomeOnSecuritiesList => GetCommand("ShowIncomeOnSecuritiesList");
        public static RibbonCommand ShowInsuranceList => GetCommand("ShowInsuranceList");
        public static RibbonCommand ShowInsuranceArchList => GetCommand("ShowInsuranceArchList");
        public static RibbonCommand ShowTransferToNPFListArchive => GetCommand("ShowTransferToNPFListArchive");
        public static RibbonCommand ShowTransferToNPFList => GetCommand("ShowTransferToNPFList");
        public static RibbonCommand ShowTransferFromNPFList => GetCommand("ShowTransferFromNPFList");
        public static RibbonCommand ShowACCList => GetCommand("ShowACCList");
        public static RibbonCommand ShowUKList => GetCommand("ShowUKList");
        public static RibbonCommand ShowUKListArchive => GetCommand("ShowUKListArchive");

        public static RibbonCommand ShowDueList => GetCommand("ShowDueList");
        public static RibbonCommand ShowDueDSVList => GetCommand("ShowDueDSVList");
        public static RibbonCommand PrintOrder => GetCommand("PrintOrder");
        public static RibbonCommand PrintBankPersons => GetCommand("PrintBankPersons");
        public static RibbonCommand InputFinregisterProps => GetCommand("InputFinregisterProps");
        public static RibbonCommand GiveFinRegister => GetCommand("GiveFinRegister");
        public static RibbonCommand RefineFinregister => GetCommand("RefineFinregister");
        public static RibbonCommand NotTransferredFinregister => GetCommand("NotTransferredFinregister");
        public static RibbonCommand FixFinregister => GetCommand("FixFinregister");
        public static RibbonCommand CreateFinRegister => GetCommand("CreateFinRegister");
        public static RibbonCommand CorrectFinRegister => GetCommand("CorrectFinRegister");
        public static RibbonCommand ShowRejAppList => GetCommand("ShowRejAppList");
        public static RibbonCommand ImportRejApp => GetCommand("ImportRejApp");
        public static RibbonCommand ShowReportErrorTypes => GetCommand("ShowReportErrorTypes");
        public static RibbonCommand ShowReportErrorNpfSpread => GetCommand("ShowReportErrorNpfSpread");
        public static RibbonCommand ShowReportErrorRegionSpread => GetCommand("ShowReportErrorRegionSpread");
        public static RibbonCommand ShowReportErrorMonthSpread => GetCommand("ShowReportErrorMonthSpread");
        public static RibbonCommand ImportNPFRegister => GetCommand("ImportNPF");
        public static RibbonCommand ImportNPFTempRegister => GetCommand("ImportNPFTempRegister");
        public static RibbonCommand ShowNPFTempAllocationList => GetCommand("ShowNPFTempAllocationList");
        public static RibbonCommand AddSPNAllocation => GetCommand("AddSPNAllocation");
        public static RibbonCommand AddSPNReturn => GetCommand("AddSPNReturn");
        public static RibbonCommand AddAllocationNPF => GetCommand("AddAllocationNPF");
        public static RibbonCommand CreatePFRAccount => GetCommand("CreatePFRAccount");
        public static RibbonCommand CreatePortfolio => GetCommand("CreatePortfolio");
        public static RibbonCommand CreateRegister => GetCommand("CreateRegister");
        public static RibbonCommand GenerateRegister => GetCommand("GenerateRegister");
        public static RibbonCommand CreateBankAccount => GetCommand("CreateBankAccount");
        public static RibbonCommand CreateRate => GetCommand("CreateRate");
        public static RibbonCommand CreateSI => GetCommand("CreateSI");
        public static RibbonCommand CreateSIContract => GetCommand("CreateSIContract");
        public static RibbonCommand CreateSIContact => GetCommand("CreateSIContact");
        public static RibbonCommand CreateLegalEntity => GetCommand("CreateLegalEntity");
        public static RibbonCommand CreateBank => GetCommand("CreateBank");
        public static RibbonCommand CreateAccount => GetCommand("CreateAccount");
        public static RibbonCommand CreateAccountKind => GetCommand("CreateAccountKind");
        public static RibbonCommand CreateReorganization => GetCommand("CreateReorganization");
        public static RibbonCommand CreateDeadZL => GetCommand("CreateDeadZL");
        public static RibbonCommand ShowAccountsList => GetCommand("ShowAccountsList");
        public static RibbonCommand ShowPortfoliosList => GetCommand("ShowPortfoliosList");
        public static RibbonCommand ShowDepositsList => GetCommand("ShowDepositsList");
        public static RibbonCommand ShowDepositsByPortfolioList => GetCommand("ShowDepositsByPortfolioList");
        public static RibbonCommand ShowPaymentOrderList => GetCommand("ShowPaymentOrderList");
        public static RibbonCommand ShowArchiveDepositsList => GetCommand("ShowArchiveDepositsList");
        public static RibbonCommand ShowContragentsList => GetCommand("ShowNPFList");
        public static RibbonCommand ShowBanksList => GetCommand("ShowBanksList");
        public static RibbonCommand ShowBanksConclusionList => GetCommand("ShowBanksConclusionList");
        public static RibbonCommand ShowRegistersList => GetCommand("ShowRegistersList");
        public static RibbonCommand ShowArchiveRegistersList => GetCommand("ShowArchiveRegistersList");
        public static RibbonCommand ShowDeadZLList => GetCommand("ShowDeadZLList");
        public static RibbonCommand ShowPFRAccountsList => GetCommand("ShowPFRAccountsList");
        public static RibbonCommand ShowReportsNPF => GetCommand("ShowReportsNPF");
        public static RibbonCommand ShowReportsSI => GetCommand("ShowReportsSI");
        public static RibbonCommand ShowReportsCB => GetCommand("ShowReportsCB");
        public static RibbonCommand ShowReportsBO => GetCommand("ShowReportsBO");
        public static RibbonCommand ShowReportNum1 => GetCommand("ShowReportNum1");

        public static RibbonCommand ShowServerSettings => GetCommand("ShowServerSettings");
        public static RibbonCommand ShowTemplatesManager => GetCommand("ShowTemplatesManager");
        public static RibbonCommand ShowExchangeRateManager => GetCommand("ShowExchangeRateManager");
        public static RibbonCommand CreateYield => GetCommand("CreateYield");
        public static RibbonCommand CreateOrder => GetCommand("CreateOrder");
        public static RibbonCommand CreateOrderRelaying => GetCommand("CreateOrderRelaying");
        public static RibbonCommand CreateOrderToPay => GetCommand("CreateOrderToPay");
        public static RibbonCommand OrderSent => GetCommand("OrderSent");
        public static RibbonCommand OrderDelivered => GetCommand("OrderDelivered");
        public static RibbonCommand OrderPaid => GetCommand("OrderPaid");
        public static RibbonCommand ShowClientThemes => GetCommand("ShowClientThemes");
        public static RibbonCommand ResetClientSettings => GetCommand("ResetClientSettings");
        public static RibbonCommand ShowJournal => GetCommand("ShowJournal");
        public static RibbonCommand SaveCard => GetCommand("SaveCard");
        public static RibbonCommand DeleteCard => GetCommand("DeleteCard");
        public static RibbonCommand AnnulLicence => GetCommand("AnnulLicence");
        public static RibbonCommand ShowZLRedistList => GetCommand("ShowZLRedistList");
        public static RibbonCommand CreateZLRedist => GetCommand("CreateZLRedist");
        public static RibbonCommand CreateRequest => GetCommand("CreateRequest");
        public static RibbonCommand SuspendActivity => GetCommand("SuspendActivity");
        public static RibbonCommand AddNPFBranch => GetCommand("AddNPFBranch");
        public static RibbonCommand SuspendLicence => GetCommand("SuspendLicence");

        public static RibbonCommand CreateLetterToNPF => GetCommand("CreateLetterToNPF");
        public static RibbonCommand CreateERZLNotify => GetCommand("CreateERZLNotify");
        public static RibbonCommand ShowRatesList => GetCommand("ShowRatesList");
        public static RibbonCommand ShowMissedRatesList => GetCommand("ShowMissedRatesList");
        public static RibbonCommand ShowSIList => GetCommand("ShowSIList");
        public static RibbonCommand ShowSIContactList => GetCommand("ShowSIContactList");
        public static RibbonCommand ShowSIContractsList => GetCommand("ShowSIContractsList");
        public static RibbonCommand ShowVRContractsList => GetCommand("ShowVRContractsList");
        public static RibbonCommand CreateSecurity => GetCommand("CreateSecurity");
        public static RibbonCommand CreateBoard => GetCommand("CreateBoard");
        public static RibbonCommand CreateSecurityInOrder => GetCommand("CreateSecurityInOrder");
        public static RibbonCommand ShowSecuritiesList => GetCommand("ShowSecuritiesList");
        public static RibbonCommand ReturnDeposit => GetCommand("ReturnDeposit");
        public static RibbonCommand LoadDBF => GetCommand("LoadDBF");
        public static RibbonCommand ShowINDateList => GetCommand("ShowINDateList");
        public static RibbonCommand ShowTDateList => GetCommand("ShowTDateList");
        public static RibbonCommand ShowBDateList => GetCommand("ShowBDateList");
        public static RibbonCommand ShowYieldOFZList => GetCommand("ShowYieldOFZList");
        public static RibbonCommand EditRate => GetCommand("EditRate");
        public static RibbonCommand EditSIName => GetCommand("EditSIName");
        public static RibbonCommand ShowOrdersList => GetCommand("ShowOrdersList");
        public static RibbonCommand ShowSellReportsList => GetCommand("ShowSellReportsList");
        public static RibbonCommand ShowBuyReportsList => GetCommand("ShowBuyReportsList");
        public static RibbonCommand ShowPFRBranchesList => GetCommand("ShowPFRBranchesList");
        public static RibbonCommand CreatePFRBranch => GetCommand("CreatePFRBranch");
        public static RibbonCommand ShowDivisionList => GetCommand("ShowDivisionList");
        public static RibbonCommand CreateDivision => GetCommand("CreateDivision");
        public static RibbonCommand AddDataFromReport => GetCommand("AddDataFromReport");
        public static RibbonCommand CreatePFRContact => GetCommand("CreatePFRContact");
        public static RibbonCommand CreatePerson => GetCommand("CreatePerson");
        public static RibbonCommand GenerateDeposit => GetCommand("GenerateDeposit");
        public static RibbonCommand FastReportInput => GetCommand("FastReportInput");
        public static RibbonCommand ShowNetWealthsList => GetCommand("ShowNetWealthsList");
        public static RibbonCommand ShowNetWealthsSumList => GetCommand("ShowNetWealthsSumList");
        public static RibbonCommand ShowActivesMarketCost => GetCommand("ShowActivesMarketCost");
        public static RibbonCommand ShowActivesMarketCostScope => GetCommand("ShowActivesMarketCostScope");
        public static RibbonCommand ShowZLTransfersList => GetCommand("ShowZLTransfersList");
        public static RibbonCommand ShowSPNMovementsList => GetCommand("ShowSPNMovementsList");
        public static RibbonCommand ShowSPNMovementsUKList => GetCommand("ShowSPNMovementsUKList");
        public static RibbonCommand CreateOVSIReqTransfer => GetCommand("CreateOVSIReqTransfer");
        public static RibbonCommand ShowArchiveOVSITransfersList => GetCommand("ShowArchiveOVSITransfersList");
        public static RibbonCommand ShowViolationsList => GetCommand("ShowViolationsList");
        public static RibbonCommand ShowAssignPaymentsList => GetCommand("ShowAssignPaymentsList");
        public static RibbonCommand ShowAssignPaymentsArchiveList => GetCommand("ShowAssignPaymentsArchiveList");
        public static RibbonCommand CreateUKPaymentPlan => GetCommand("CreateAPSIRegister");
        public static RibbonCommand ShowCorrespondenceList => GetCommand("ShowCorrespondenceList");
        public static RibbonCommand ShowArchiveList => GetCommand("ShowArchiveList");
        public static RibbonCommand ShowSIControlList => GetCommand("ShowSIControlList");
        public static RibbonCommand ShowSuccessLoadedODKList => GetCommand("ShowSuccessLoadedODKList");
        public static RibbonCommand ShowSuccessLoadedODKListAll => GetCommand("ShowSuccessLoadedODKListAll");
        public static RibbonCommand ShowErrorLoadedODKList => GetCommand("ShowErrorLoadedODKList");
        public static RibbonCommand ShowRatingsList => GetCommand("ShowRatingsList");
        public static RibbonCommand AddRating => GetCommand("AddRating");
        public static RibbonCommand ImportKO761 => GetCommand("ImportKO761");
        public static RibbonCommand ImportOwnCapital => GetCommand("ImportOwnCapital");
        public static RibbonCommand ShowAgencyRatingsList => GetCommand("ShowAgencyRatingsList");
        public static RibbonCommand AddAgencyRating => GetCommand("AddAgencyRating");
        public static RibbonCommand ShowRatingAgenciesList => GetCommand("ShowRatingAgenciesList");
        public static RibbonCommand AddRatingAgency => GetCommand("AddRatingAgency");
        public static RibbonCommand ShowDepClaimSelectParamsList => GetCommand("ShowDepClaimSelectParamsList");
        public static RibbonCommand AddDepClaimSelectParams => GetCommand("AddDepClaimSelectParams");
        public static RibbonCommand ShowDepClaimMaxList => GetCommand("ShowDepclaimMaxList");
        public static RibbonCommand ShowDepClaimList => GetCommand("ShowDepclaimList");
        public static SimpleCommand ShowExportsDepclaimMax => GetCommand3("ShowExportsDepclaimMax");
        public static SimpleCommand ShowExportsDepclaim => GetCommand3("ShowExportsDepclaim");
        public static SimpleCommand ImportDepclaimMaxList => GetCommand3("ImportDepclaimMaxList");
        public static SimpleCommand ImportDepclaimList => GetCommand3("ImportDepclaimList");
        public static SimpleCommand ShowDepClaim2List => GetCommand3("ShowDepClaim2List");
        public static SimpleCommand ImportDepClaimBatchList => GetCommand3("ImportDepClaimBatchList");
        public static SimpleCommand ImportDepClaim2List => GetCommand3("ImportDepClaim2List");
        public static SimpleCommand ExportDepClaim2List => GetCommand3("ExportDepClaim2List");
        public static SimpleCommand ExportCutoffRate => GetCommand3("ExportCutoffRate");
        public static SimpleCommand ImportDepClaim2ConfirmList => GetCommand3("ImportDepClaim2ConfirmList");
        public static SimpleCommand ExportDepClaim2ConfirmList => GetCommand3("ExportDepClaim2ConfirmList");
        public static RibbonCommand ShowDepClaim2ConfirmList => GetCommand("ShowDepClaim2ConfirmList");
        public static SimpleCommand CreateDepClaim2Offer => GetCommand3("CreateDepClaim2Offer");
        public static RibbonCommand ShowDepClaim2Offer => GetCommand("ShowDepClaim2Offer");
        public static SimpleCommand ExportAllDepClaim2Offer => GetCommand3("ExportAllDepClaim2Offer");
        public static SimpleCommand ExportContractsRegister => GetCommand3("ExportContractsRegister");
        public static SimpleCommand ExportSingleDepClaim2Offer => GetCommand3("ExportSingleDepClaim2Offer");
        public static SimpleCommand ExportNotAcceptedDepClaimOffer => GetCommand3("ExportNotAcceptedDepClaimOffer");
        public static SimpleCommand ExportDepClaimAllocation => GetCommand3("ExportDepClaimAllocation");
        public static SimpleCommand ExportGroupDepClaim2Offer => GetCommand3("ExportGroupDepClaim2Offer");
        public static RibbonCommand RegionReportListInsuredPerson => GetCommand("RegionReportListInsuredPerson");
        public static RibbonCommand RegionReportListNpfNotice => GetCommand("RegionReportListNpfNotice");
        public static RibbonCommand RegionReportListAssigneePayment => GetCommand("RegionReportListAssigneePayment");
        public static RibbonCommand RegionReportListCashExpenditures => GetCommand("RegionReportListCashExpenditures");
        public static RibbonCommand RegionReportListApplications741 => GetCommand("RegionReportListApplications741");
        public static RibbonCommand RegionReportListDeliveryCost => GetCommand("RegionReportListDeliveryCost");
        public static RibbonCommand RegionReportImportExcel => GetCommand("RegionReportImportExcel");
        public static RibbonCommand RegionReportImportXml => GetCommand("RegionReportImportXml");
        public static RibbonCommand RegionReportExport => GetCommand("RegionReportExport");
        public static RibbonCommand ShowNpfCorrespondenceList => GetCommand("ShowNpfCorrespondenceList");
        public static RibbonCommand ShowNpfControlList => GetCommand("ShowNpfControlList");
        public static RibbonCommand ShowNpfArchiveList => GetCommand("ShowNpfArchiveList");
        public static RibbonCommand CreateNpfDocument => GetCommand("CreateNpfDocument");
        public static RibbonCommand ShowDelayedPaymentClaimList => GetCommand("ShowDelayedPaymentClaimList");
        public static RibbonCommand ShowPensionNotificationList => GetCommand("ShowPensionNotificationList");
        public static RibbonCommand CreatePensionNotification => GetCommand("CreatePensionNotification");
        public static RibbonCommand CreateDelayedPaymentClaim => GetCommand("CreateDelayedPaymentClaim");
        public static RibbonCommand UKReportImport => GetCommand("UKReportImport");

        #region ВР

        public static RibbonCommand ShowTransferVRWizard => GetCommand("ShowTransferVRWizard");
        public static RibbonCommand ShowArchiveVRTransfersList => GetCommand("ShowArchiveVRTransfersList");
        public static RibbonCommand ShowVRTransferList => GetCommand("ShowVRTransferList");
        public static RibbonCommand CreateVRTransfer => GetCommand("CreateVRTransfer");
        public static RibbonCommand CreateVRReqTransfer => GetCommand("CreateVRReqTransfer");
        public static RibbonCommand ShowZLTransfersVRList => GetCommand("ShowZLTransfersVRList");
        public static RibbonCommand ShowSPNMovementsVRList => GetCommand("ShowSPNMovementsVRList");
        public static RibbonCommand ShowSPNMovementsUKVRList => GetCommand("ShowSPNMovementsUKVRList");
        public static RibbonCommand RecalcUKVRPortfolios => GetCommand("RecalcUKVRPortfolios");
        public static RibbonCommand CreatePrintReqVR => GetCommand("CreatePrintReqVR");
        public static RibbonCommand ToUKEnterTransferActDataVR => GetCommand("ToUKEnterTransferActDataVR");
        public static RibbonCommand CreateVRRequest => GetCommand("CreateVRRequest");
        public static RibbonCommand FromUKEnterRequirementDataVR => GetCommand("FromUKEnterRequirementDataVR");
        public static RibbonCommand FromUKEnterGetRequirementDateVR => GetCommand("FromUKEnterGetRequirementDateVR");
        public static RibbonCommand FromUKEnterTransferActDataVR => GetCommand("FromUKEnterTransferActDataVR");
        public static RibbonCommand RollbackStatusVR => GetCommand("RollbackStatusVR");
        public static RibbonCommand ShowNetWealthsVRList => GetCommand("ShowNetWealthsVRList");
        public static RibbonCommand ShowNetWealthsVRSumList => GetCommand("ShowNetWealthsVRSumList");
        public static RibbonCommand ShowActivesMarketVRCost => GetCommand("ShowActivesMarketVRCost");
        public static RibbonCommand ShowActivesMarketVRCostScope => GetCommand("ShowActivesMarketVRCostScope");
        public static RibbonCommand ShowF40VRList => GetCommand("ShowF40VRList");
        public static RibbonCommand ShowF50VRList => GetCommand("ShowF50VRList");
        public static RibbonCommand ShowF60VRList => GetCommand("ShowF60VRList");
        public static RibbonCommand ShowYieldsVRList => GetCommand("ShowYieldsVRList");
        public static RibbonCommand ShowOwnFundsVRList => GetCommand("ShowOwnFundsVRList");
        public static RibbonCommand ShowF70VRList => GetCommand("ShowF70VRList");
        public static RibbonCommand CreateYieldVR => GetCommand("CreateYieldVR");
        public static RibbonCommand ShowF80VRList => GetCommand("ShowF80VRList");
        public static RibbonCommand ShowViolationsVRList => GetCommand("ShowViolationsVRList");
        public static RibbonCommand ShowSuccessLoadedODKListVR => GetCommand("ShowSuccessLoadedODKListVR");

        // Страхование

        public static RibbonCommand ShowInsuranceListVR => GetCommand("ShowInsuranceListVR");
        public static RibbonCommand ShowInsuranceArchListVR => GetCommand("ShowInsuranceArchListVR");
        public static RibbonCommand CreateInsuranceVR => GetCommand("CreateInsuranceVR");
        public static RibbonCommand ShowVRCorrespondenceList => GetCommand("ShowVRCorrespondenceList");
        public static RibbonCommand ShowVRArchiveList => GetCommand("ShowVRArchiveList");
        public static RibbonCommand ShowVRControlList => GetCommand("ShowVRControlList");
        public static RibbonCommand CreateVRDocument => GetCommand("CreateVRDocument");
        public static RibbonCommand CreateVRLinkedDocument => GetCommand("CreateVRLinkedDocument");

        // Оплата услус СД

        public static RibbonCommand ShowVRF402List => GetCommand("ShowVRF402List");
        public static RibbonCommand ShowVRDemandList => GetCommand("ShowVRDemandList");
        public static RibbonCommand ShowVRF402PFRActions => GetCommand("ShowVRF402PFRActions");
        public static RibbonCommand CreateVROrderToPay => GetCommand("CreateVROrderToPay");
        public static RibbonCommand VROrderSent => GetCommand("VROrderSent");
        public static RibbonCommand VROrderDelivered => GetCommand("VROrderDelivered");
        public static RibbonCommand VROrderPaid => GetCommand("VROrderPaid");

        // Выплаты правопреемникам

        public static RibbonCommand ShowVRYearPlanWizard => GetCommand("ShowVRYearPlanWizard");
        public static RibbonCommand ShowVRForYearPlanWizard => GetCommand("ShowVRForYearPlanWizard");
        public static RibbonCommand ShowVRPlanCorrectionWizard => GetCommand("ShowVRPlanCorrectionWizard");
        public static RibbonCommand ShowVRMonthTransferCreationWizard => GetCommand("ShowVRMonthTransferCreationWizard");
        public static RibbonCommand ShowVRAssignPaymentsList => GetCommand("ShowVRAssignPaymentsList");
        public static RibbonCommand ShowVRAssignPaymentsArchiveList => GetCommand("ShowVRAssignPaymentsArchiveList");
        public static RibbonCommand CreateVRUKPaymentPlan => GetCommand("CreateVRUKPaymentPlan");
        public static RibbonCommand PrintAPVRMonthPlan => GetCommand("PrintAPVRMonthPlan");
        public static RibbonCommand PrintAPVRYearPlan => GetCommand("PrintAPVRYearPlan");
        public static RibbonCommand PrintVRYearPayment => GetCommand("PrintVRYearPayment");
        public static RibbonCommand ShowVRReports => GetCommand("ShowVRReports");
        public static RibbonCommand ShowVRInvDecl => GetCommand("ShowVRInvDecl");
        public static RibbonCommand ShowSPNReport01 => GetCommand("ShowSPNReport01");
        public static RibbonCommand ShowSPNReport02 => GetCommand("ShowSPNReport02");
        public static RibbonCommand ShowSPNReport03 => GetCommand("ShowSPNReport03");
        public static RibbonCommand ShowSPNReport19 => GetCommand("ShowSPNReport19");
        public static RibbonCommand ShowSPNReport17 => GetCommand("ShowSPNReport17");
        public static RibbonCommand ShowSPNReport18 => GetCommand("ShowSPNReport18");
        public static RibbonCommand ShowSPNReport20 => GetCommand("ShowSPNReport20");
        public static RibbonCommand ShowSPNReport40 => GetCommand("ShowSPNReport40");
        public static RibbonCommand ShowSPNReport39 => GetCommand("ShowSPNReport39");
        public static RibbonCommand ReportImportVR => GetCommand("ReportImportVR");
        public static RibbonCommand ReportExportVR => GetCommand("ReportExportVR");

        #endregion

        public static RibbonCommand KBKList => GetCommand("KBKList");
        public static RibbonCommand CreateKBK => GetCommand("CreateKBK");
        public static RibbonCommand AddNPFErrorCode => GetCommand("AddNPFErrorCode");
        public static RibbonCommand AddNPFMatchCode => GetCommand("AddNPFMatchCode");
        public static RibbonCommand ShowNPFErrorCodeList => GetCommand("ShowNPFErrorCodeList");
        public static RibbonCommand ShowNPFMatchCodeList => GetCommand("ShowNPFMatchCodeList");


        public static RibbonCommand ShowDKMonthList => GetCommand("ShowDKMonthList");
        public static RibbonCommand ShowDKYearList => GetCommand("ShowDKYearList");
        public static RibbonCommand ShowReportIPUK => GetCommand("ShowReportIPUK");
        public static RibbonCommand ShowReportIPUKTotal => GetCommand("ShowReportIPUKTotal");
        public static RibbonCommand ShowReportBalanceUK => GetCommand("ShowReportBalanceUK");
        public static RibbonCommand ShowCommonPPList => GetCommand("ShowCommonPPList");
        public static RibbonCommand ShowPaymentDetailList => GetCommand("ShowPaymentDetailList");
        public static RibbonCommand ShowCreatePaymentDetail => GetCommand("ShowCreatePaymentDetail");
        public static RibbonCommand ShowCreateCommonPP => GetCommand("ShowCreateCommonPP");
        public static RibbonCommand ShowSelectCommonPP => GetCommand("ShowSelectCommonPP");
        public static RibbonCommand ShowSplitPP => GetCommand("ShowSplitPP");

        public static RibbonCommand ShowSelectCommonPPNPF => GetCommand("ShowSelectCommonPPNPF");
        public static RibbonCommand ShowReportDeposits => GetCommand("ShowReportDeposits");
        public static RibbonCommand ShowUKAccountsReport => GetCommand("ShowUKAccountsReport");
        public static RibbonCommand ShowUKListReport => GetCommand("ShowUKListReport");
        public static RibbonCommand ShowReportRSASCA => GetCommand("ShowReportRSASCA");
        public static RibbonCommand ShowReportF060 => GetCommand("ShowReportF060");
        public static RibbonCommand ShowReportF070 => GetCommand("ShowReportF070");
        public static RibbonCommand ShowReportProfitability => GetCommand("ShowReportProfitability");
        public static RibbonCommand ShowReportOwnedFounds => GetCommand("ShowReportOwnedFounds");
        public static RibbonCommand ShowReportInvestRatio => GetCommand("ShowReportInvestRatio");


        public static RibbonCommand OneS_ViewImportJournal => GetCommand("OneS_ViewImportJournal");
        public static RibbonCommand OneS_ViewExportJournal => GetCommand("OneS_ViewExportJournal");
        public static RibbonCommand OneS_ViewErrorList => GetCommand("OneS_ViewErrorList");
        public static RibbonCommand OneS_DataExchange => GetCommand("OneS_DataExchange");
        public static RibbonCommand OneS_Settings => GetCommand("OneS_Settings");
        public static RibbonCommand OneS_OpSettings => GetCommand("OneS_OpSettings");

        public static RibbonCommand OnesDataVR => GetCommand("OnesDataVR");
        public static RibbonCommand OnesDataNPF => GetCommand("OnesDataNPF");
        public static RibbonCommand OnesDataSI => GetCommand("OnesDataSI");
        public static RibbonCommand OnesDataDEPO => GetCommand("OnesDataDEPO");


        public static RibbonCommand PKIP_ViewJournal => GetCommand("PKIP_ViewJournal");
        public static RibbonCommand PKIP_ViewErrorList => GetCommand("PKIP_ViewErrorList");
        public static RibbonCommand PKIP_DataExchange => GetCommand("PKIP_DataExchange");
        public static RibbonCommand PKIP_Settings => GetCommand("PKIP_Settings");

        public static RibbonCommand RollbackFinRegStatus => GetCommand("RollbackFinRegStatus");

        public static RibbonCommand ShowROPSSumList => GetCommand("ShowROPSSumList");
        public static RibbonCommand CreateROPSSum => GetCommand("CreateROPSSum");
        public static RibbonCommand CBReportWizard => GetCommand("CBReportWizard");
        public static RibbonCommand OpfrWizard => GetCommand("OpfrWizard");
        public static RibbonCommand DepositCountWizard => GetCommand("DepositCountWizard");
        public static RibbonCommand ShowSpnDepositWizard => GetCommand("ShowSpnDepositWizard");

        #region Analyze commands

        public static RibbonCommand ShowPensionplacementPivot => GetCommand("ShowPensionplacementPivot");
        public static RibbonCommand ShowPensionplacementList => GetCommand("ShowPensionplacementList");

        public static RibbonCommand ShowPensionfundtonpfPivot => GetCommand("ShowPensionfundtonpfPivot");
        public static RibbonCommand ShowPensionfundtonpfWithSubPivot => GetCommand("ShowPensionfundtonpfWithSubPivot");
        public static RibbonCommand ShowPensionfundtonpfList => GetCommand("ShowPensionfundtonpfList");
        public static RibbonCommand ShowPensionfundtonpfSubtrList => GetCommand("ShowPensionfundtonpfSubtrList");
        public static RibbonCommand AddPensionfundtonpfReport => GetCommand("AddPensionfundtonpfReport");
        public static RibbonCommand AddPensionfundtonpfSubtrReport => GetCommand("AddPensionfundtonpfSubtrReport");

        public static RibbonCommand ShowIncomingstatementPivot => GetCommand("ShowIncomingstatementPivot");
        public static RibbonCommand ShowIncomingstatementList => GetCommand("ShowIncomingstatementList");


        public static RibbonCommand ShowInsurepdersonPivot => GetCommand("ShowInsurepdersonPivot");
        public static RibbonCommand ShowYieldfunds107Pivot => GetCommand("ShowYieldfunds107Pivot");
        public static RibbonCommand ShowYieldfunds140Pivot => GetCommand("ShowYieldfunds140Pivot");
        public static RibbonCommand ShowPaymentyearratePivot => GetCommand("ShowPaymentyearratePivot");

        //Команды создания отчетов
        public static RibbonCommand ShowAddPensionplacementEditor => GetCommand("ShowAddPensionplacementEditor");


        public static RibbonCommand ShowPaymentyearrateAddNew => GetCommand("ShowPaymentyearrateAddNew");
        public static RibbonCommand ShowPaymentyearrateList => GetCommand("ShowPaymentyearrateList");
        public static RibbonCommand ShowYieldfunds140AddNew => GetCommand("ShowYieldfunds140AddNew");
        public static RibbonCommand ShowYieldfunds140List => GetCommand("ShowYieldfunds140List");
        public static RibbonCommand ShowYieldfunds107AddNew => GetCommand("ShowYieldfunds107AddNew");
        public static RibbonCommand ShowYieldfunds107List => GetCommand("ShowYieldfunds107List");
        public static RibbonCommand ShowIncomingstatementAddNew => GetCommand("ShowIncomingstatementAddNew");
        public static RibbonCommand ShowInsurepdersonAddNew => GetCommand("ShowInsurepdersonAddNew");
        public static RibbonCommand ShowInsurepdersonList => GetCommand("ShowInsurepdersonList");

        public static RibbonCommand ShowAnalyzeConditionEditor => GetCommand("ShowAnalyzeConditionEditor");

        public static RibbonCommand InfoToCBList => GetCommand("InfoToCBList");
        public static RibbonCommand AddInfoToCBList => GetCommand("AddInfoToCBList");
        public static RibbonCommand ExportInfoToCBList => GetCommand("ExportInfoToCBList");
        public static RibbonCommand ShowStocksList => GetCommand("ShowStocksList");

        public static RibbonCommand ShowAggregSPN => GetCommand("ShowAggregSPN");
        public static RibbonCommand OpsFromNpfReport => GetCommand("OpsFromNpfReport");

        #endregion
    }
}
