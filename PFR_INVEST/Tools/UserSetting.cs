using System;
using System.Collections.Generic;
using System.Windows;

namespace PFR_INVEST.Tools
{
    [Serializable]
    public class UserSetting
    {
        public string PanelName { get; set; }
        public ContainerType PanelType { get; set; }
        public Dictionary<string, string> GridSettingsList { get; private set; }
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }
        public Size Size { get; set; }
        public Point Point { get; set; }

        public UserSetting()
        {
            GridSettingsList = new Dictionary<string, string>();
            Size = Size.Empty;
            Point = new Point();
        }
    }
}
