﻿namespace PFR_INVEST.Views.Interface
{
    internal interface IListView<T>
    {
        T SelectedItem { get; }
    }
}
