﻿using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views
{
    public interface ISearchGrid
    {
        GridControl GetGridControl();
    }
}
