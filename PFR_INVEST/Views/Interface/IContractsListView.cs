﻿namespace PFR_INVEST.Views
{
    public interface IContractsListView
    {
        ContractsGridView GridControl { get; }
    }
}
