﻿namespace PFR_INVEST.Views.Interface
{
    /// <summary>
    /// Интерфейс-маркер, указывает что помеченный объект реализует вычисление размера окна-родителя самостоятельно
    /// См. Описания основных методик работы с кодом.txt п. 1
    /// </summary>
    public interface IAutoParentPanelSize
    {
        /// <summary>
        /// Если отлично от 0, задает ширину окна по умолчанию
        /// </summary>
        double DesiredWidth { get; }
    }

}
