﻿using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OnesImportJournalListView.xaml
    /// </summary>
    public partial class OnesImportJournalListView
    {
        public OnesImportJournalListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, listGrid);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            listGrid.CustomColumnDisplayText += listGrid_CustomColumnDisplayText;
        }

        public OnesImportJournalListViewModel Model => DataContext as OnesImportJournalListViewModel;

        private void listGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "Сессия номер: {0}";
            if (!listGrid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName != "SessionID")
                return;

            var currRow = (e.Row as OnesFileImport);
            if (currRow != null)
                e.DisplayText = string.Format(dispText, currRow.SessionID);
        }
    }
}
