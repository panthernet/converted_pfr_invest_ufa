﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DemandView.xaml
    /// </summary>
    public partial class DemandView : UserControl
    {
        public DemandView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
