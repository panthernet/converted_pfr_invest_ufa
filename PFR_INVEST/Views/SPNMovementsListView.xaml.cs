﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SPNMovementsListView.xaml
    /// </summary>
    public partial class SPNMovementsListView
    {
        private bool _mGridRowMouseDoubleClicked;

        public SPNMovementsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (IsTransferSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedTrID();
                if (id <= 0)
                    return;
                if (DataContext is SPNMovementsSIListViewModel)
                    App.DashboardManager.OpenNewTab(typeof(TransferSIView), ViewModelState.Edit, id, "Список перечислений СИ");
                else
                    App.DashboardManager.OpenNewTab(typeof(TransferVRView), ViewModelState.Edit, id, "Список перечислений ВР");
            }
            else if (IsReqTransferSelected())
            {
                _mGridRowMouseDoubleClicked = true;
                var id = GetSelectedReqID();
                if (id <= 0) return;
                if (DataContext is SPNMovementsSIListViewModel)
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, id, "Перечисление СИ");
                else
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, id, "Перечисление ВР");
            }
        }

        public bool IsTransferSelected()
        {
            var lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return (Grid.GetGroupedColumns()[lvl].FieldName == "ContractNumber");
            return false;
        }

        public bool IsReqTransferSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        public long GetSelectedTrID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "TransferID"));
        }

        public long GetSelectedReqID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ReqTransferID"));
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }
    }
}
