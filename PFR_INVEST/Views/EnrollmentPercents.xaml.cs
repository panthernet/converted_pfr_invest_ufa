﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for EnrollmentPercents.xaml
    /// </summary>
    public partial class EnrollmentPercents
    {
        public EnrollmentPercents()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private EnrollmentPercentsViewModel Model => DataContext as EnrollmentPercentsViewModel;

        private void ButtonInfo_Click(object sender, RoutedEventArgs e)
        {
            Model?.GetCourceManuallyOrFromCBRSite();
        }
    }
}
