﻿using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for BanksVerificationDlg.xaml
    /// </summary>
    public partial class BanksVerificationDlg : DXWindow
    {
        public BanksVerificationDlg()
        {
            InitializeComponent();
        }

        private BanksVerificationViewModel Model
        {
            get { return DataContext as BanksVerificationViewModel; }

        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ButtonEdit_FilePathButtonClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var dlg = new System.Windows.Forms.FolderBrowserDialog {Description = @"Выберите папку для экспорта"};
            var result = dlg.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Model.FilePath = dlg.SelectedPath;
            }
        }
    }
}
