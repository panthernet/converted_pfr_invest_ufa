﻿using System.Windows;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class ReportErrorTypesDlg
    {
        public ReportErrorTypesDlg(ErrorReportType type = ErrorReportType.Tipology)
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForLoadingIndicator(this);
            ModelInteractionHelper.SignUpForCloseRequest(this);
            ModelInteractionHelper.SubscribeForTopmostRequest(this);

            if(type == ErrorReportType.Month)
            {
                dpPeriod.Visibility = Visibility.Collapsed;
                dpYear.Visibility = Visibility.Visible;
            }
            else
            {
                dpPeriod.Visibility = Visibility.Visible;
                dpYear.Visibility = Visibility.Collapsed;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
