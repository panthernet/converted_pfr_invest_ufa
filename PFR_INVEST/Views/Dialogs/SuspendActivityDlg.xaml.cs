﻿using System;
using System.Windows;
using DevExpress.Xpf.Core;
using System.ComponentModel;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SuspendActivityDlg.xaml
    /// </summary>
    public partial class SuspendActivityDlg
    {
        private DataContentContainer _item;

        public SuspendActivityDlg()
        {
            InitializeComponent();

            _item = new DataContentContainer
            {
                DateStart = DateTime.Now.Date,
                DateEnd = DateTime.Now.Date,
                DateDoc = DateTime.Now.Date
            };
            scaleGrid.DataContext = _item;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _item = scaleGrid.DataContext as DataContentContainer;
            if (SuspendDateStart.Text + SuspendDateEnd.Text == "" || DocDate.Text == "" || string.IsNullOrEmpty(_item?.RegNum))
            {
                DXMessageBox.Show("Введите недостающие значения", "Ошибка ввода данных", MessageBoxButton.OK,
                                MessageBoxImage.Warning);
                return;
            }

            if (SuspendDateStart.DateTime != DateTime.MinValue && SuspendDateEnd.DateTime != DateTime.MinValue &&
                SuspendDateStart.DateTime > SuspendDateEnd.DateTime)
            {
                DXMessageBox.Show("Дата начала должна быть меньше даты конца!", "Ошибка ввода данных", MessageBoxButton.OK,
                                MessageBoxImage.Warning);
                return;
            }

            if (_item.RegNum.Length > 12)
            {
                DXMessageBox.Show("Превышена допустимая длина строки", "Ошибка ввода данных", MessageBoxButton.OK,
                                MessageBoxImage.Warning);
                return;
            }

            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public class DataContentContainer : INotifyPropertyChanged, IDataErrorInfo
        {
            private string _regNum;

            public string RegNum
            {
                get { return _regNum; }
                set
                {

                    _regNum = value?.Trim();
                    OnPropertyChanged("RegNum");
                    OnPropertyChanged("IsOkEnabled");
                }
            }

            public DateTime? _DateStart;
            public DateTime? _DateEnd;
            public DateTime? _DateDoc;

            public DateTime? DateStart
            {
                get
                {
                    return _DateStart;
                }
                set
                {
                    _DateStart = value;
                    OnPropertyChanged("DateStart");
                    OnPropertyChanged("DateEnd");
                    OnPropertyChanged("IsOkEnabled");
                }
            }

            public DateTime? DateEnd
            {
                get
                {
                    return _DateEnd;
                }
                set
                {
                    _DateEnd = value;
                    OnPropertyChanged("DateStart");
                    OnPropertyChanged("DateEnd");
                    OnPropertyChanged("IsOkEnabled");
                }
            }

            public DateTime? DateDoc
            {
                get
                {
                    return _DateDoc;
                }
                set
                {
                    _DateDoc = value;
                    OnPropertyChanged("DateDoc");
                    OnPropertyChanged("IsOkEnabled");
                }
            }

            public bool IsOkEnabled => IsValid();


            public event PropertyChangedEventHandler PropertyChanged;

            protected void OnPropertyChanged(string name)
            {
                var handler = PropertyChanged;
                handler?.Invoke(this, new PropertyChangedEventArgs(name));
            }

            public virtual string Error => string.Empty;

            public bool IsValid()
            {
                return this["RegNum"] == null && this["DateStart"] == null && this["DateEnd"] == null && this["DateDoc"] == null;
            }

            public string this[string columnName]
            {
                get
                {
                    switch (columnName)
                    {
                        case "RegNum":
                            if (string.IsNullOrEmpty(RegNum))
                                return "Номер документа является обязательным";
                            if (RegNum.Length > 12)
                                return "Превышена допустимая длина строки";
                            break;

                        case "DateStart":
                            if (DateStart == DateTime.MinValue)
                                return "Введите недостающие значения";
                            if (DateEnd != DateTime.MinValue && DateStart > DateEnd)
                                return "Дата начала должна быть меньше даты конца!";
                            break;

                        case "DateEnd":
                            if (DateEnd == DateTime.MinValue)
                                return "Введите недостающие значения";
                            if (DateStart != DateTime.MinValue && DateStart > DateEnd)
                                return "Дата начала должна быть меньше даты конца!";
                            break;
                        case "DateDoc":
                            if (DateDoc == DateTime.MinValue)
                                return "Введите недостающие значения";
                            break;

                    }
                    return null;
                }
            }
        }
    }
}
