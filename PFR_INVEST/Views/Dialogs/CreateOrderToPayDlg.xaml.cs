﻿namespace PFR_INVEST.Views.Dialogs
{
	/// <summary>
	/// Interaction logic for CreateOrderToPayDlg.xaml
	/// </summary>
	public partial class CreateOrderToPayDlg
	{
		public CreateOrderToPayDlg()
		{
			InitializeComponent();
		}

		private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
