﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ERZLNotifyDlg.xaml
    /// </summary>
    public partial class ERZLDlg
    {
        public ERZLDlg()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            ((ERZLDialogViewModel)DataContext).SelectedItem = gControl.GetFocusedRow() as ERZLDlgItem;
            DialogResult = true;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}