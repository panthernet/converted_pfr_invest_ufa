﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SelectAccountDlg.xaml
    /// </summary>
    public partial class SelectAccountDlg
    {
        public SelectAccountDlg()
        {
            InitializeComponent();
            Tools.ThemesTool.SetCurrentTheme(this);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
