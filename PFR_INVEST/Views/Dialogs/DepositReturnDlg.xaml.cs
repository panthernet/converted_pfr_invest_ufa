﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class DepositReturnDlg
    {
        public DepositReturnDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }
    }
}
