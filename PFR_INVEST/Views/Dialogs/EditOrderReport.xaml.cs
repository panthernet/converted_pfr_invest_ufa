﻿using System;
using System.Windows;
using System.Windows.Threading;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditSaleReport.xaml
    /// </summary>
    public partial class EditOrderReport
    {
        private OrderReportViewModelBase Model => DataContext as OrderReportViewModelBase;

        public EditOrderReport()
        {
            InitializeComponent();
            var mTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            mTimer.Tick += tick;
            mTimer.Start();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void tick(object state, EventArgs e)
        {
            if (Model != null)
                OKButton.IsEnabled = Model.Validate();
        }
    }
}
