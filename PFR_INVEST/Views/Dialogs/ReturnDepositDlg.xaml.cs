﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ReturnDepositDlg.xaml
    /// </summary>
    public partial class ReturnDepositDlg
    {
        public ReturnDepositDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
