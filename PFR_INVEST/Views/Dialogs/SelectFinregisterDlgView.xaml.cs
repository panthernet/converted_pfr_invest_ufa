﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class SelectFinregisterDlgView
    {
		public SelectFinregisterDlgView()
        {
            InitializeComponent();
            tableView.Loaded += tableView_Loaded;
            grid.CustomColumnDisplayText += Grid_CustomColumnDisplayText;
            grid.CustomGroupDisplayText += Grid_CustomGroupDisplayText;
            grid.View.ShowFilterPopup += View_ShowFilterPopup;
        }

        private static void View_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName == "HasPP" || e.Column.FieldName == "HasPP2")
            {
                ((List<object>)e.ComboBoxEdit.ItemsSource).ForEach(item =>
                {
                    var a = item as CustomComboBoxItem;
                    if(a == null) return;
                    if (a.EditValue is bool && (bool)a.EditValue) a.DisplayValue = e.Column.FieldName == "HasPP2" ? "Привязаны платежные поручения" : "Да";
                    if (a.EditValue is bool && !(bool)a.EditValue) a.DisplayValue = e.Column.FieldName == "HasPP2" ? "Не привязаны платежные поручения" : "Нет";
                });
            }
        }

        private void tableView_Loaded(object sender, RoutedEventArgs e)
        {
            tableView.SearchControl?.Focus();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {            
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

		private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if(grid.CurrentItem != null)
				DialogResult = true;
		}

        private void Grid_CustomGroupDisplayText(object sender, CustomGroupDisplayTextEventArgs e)
        {
            if (!grid.IsValidRowHandle(e.RowHandle))
                return;
            if (e.Column.FieldName == "HasPP2")
                e.DisplayText = (bool?)e.Value == true ? "Привязаны платежные поручения" : "Не привязаны платежные поручения";
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (!grid.IsValidRowHandle(e.RowHandle))
                return;
            if (e.Column.FieldName == "HasPP")
                e.DisplayText = (bool?)e.Value == true ? "Да" : "Нет";
        }
    }
}
