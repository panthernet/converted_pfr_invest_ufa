﻿using System;
using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class RequestPercentDlg
    {
        public bool DlgContinue { get; private set; }
        public decimal FixedPercent { get; private set; }

        public RequestPercentDlg()
        {
            FixedPercent = -1;
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            FixedPercent = Convert.ToDecimal(FP.Text);
            DlgContinue = true;
            Close();  
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DlgContinue = false;
            Close();
        }
    }
}
