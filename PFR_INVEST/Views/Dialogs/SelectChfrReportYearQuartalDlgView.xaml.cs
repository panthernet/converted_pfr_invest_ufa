﻿using System.Windows;
using PFR_INVEST.Helpers;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SuspendActivityDlg.xaml
    /// </summary>
    public partial class SelectChfrReportYearQuartalDlgView
    {
        protected SelectChfrReportYearQuartalDlgViewModel Model => (SelectChfrReportYearQuartalDlgViewModel)DataContext;

        public SelectChfrReportYearQuartalDlgView()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
