﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for OwnCapitalHistoryDlg.xaml
    /// </summary>
    public partial class OwnCapitalHistoryDlg
    {
        public OwnCapitalDlgModel Model => DataContext as OwnCapitalDlgModel;

        public OwnCapitalHistoryDlg()
        {
            InitializeComponent();
        }
    }
}
