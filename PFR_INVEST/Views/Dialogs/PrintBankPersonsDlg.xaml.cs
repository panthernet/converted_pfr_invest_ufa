﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for PrintBankPersonsDlg.xaml
    /// </summary>
    public partial class PrintBankPersonsDlg
    {
        public bool IsAllPersonsSelected { get; } = true;

        public PrintBankPersonsDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
