﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.RegionsReport
{
    /// <summary>
    /// Interaction logic for RegionReportExportView.xaml
    /// </summary>
    public partial class SelectSILettersYearMonthDlg
    {
        public SILettersSelectYearMonthDlgViewModel Model => DataContext as SILettersSelectYearMonthDlgViewModel;

        public SelectSILettersYearMonthDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var viewYear = (int?) cbYear.EditValue;
            if (viewYear.HasValue && viewYear.Value != Model.SelectedYear)
                Model.SelectedYear = viewYear.Value;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
