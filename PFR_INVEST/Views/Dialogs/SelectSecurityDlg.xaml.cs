﻿using System.Windows;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects;
using System;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for SelectSecurityDlg.xaml
    /// </summary>
    public partial class SelectSecurityDlg
    {
        public DlgSecurityListItem Security { get; set; }
        public ICommand OkCommand { get; protected set; }


        public SelectSecurityDlg()
        {
            OkCommand = new DelegateCommand(p => CanExecuteOkCommand(), p => ExecuteOkCommand());
            InitializeComponent();
            var list = DataContainerFacade.GetList<Security>().FindAll(a=> a.Status!=-1).OrderBy(a=> a.Name).ToList();
            cboxSec.ItemsSource = list;
            if (list.Count > 0) cboxSec.SelectedItem = list[0];
            okButton.Command = OkCommand;
        }

        private bool CanExecuteOkCommand()
        {
            return cboxSec.SelectedItem != null && txtSumm.EditValue != null;
        }

        private void ExecuteOkCommand()
        {
            Security = new DlgSecurityListItem() { Security = cboxSec.SelectedItem as Security, Summ = Convert.ToDecimal(txtSumm.EditValue) };
            DialogResult = true;
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
