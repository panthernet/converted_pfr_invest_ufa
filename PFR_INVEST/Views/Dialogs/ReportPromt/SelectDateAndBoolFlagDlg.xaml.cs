﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;

namespace PFR_INVEST.Views.Dialogs.ReportPromt
{
    /// <summary>
    /// Interaction logic for SelectDateAndBoolFlagDlg.xaml
    /// </summary>
    public partial class SelectDateAndBoolFlagDlg
    {
        public SelectDateAndBoolFlagViewModel VM => DataContext as SelectDateAndBoolFlagViewModel;
        public SelectDateAndBoolFlagDlg()
        {
            InitializeComponent();
        }
    }
}
