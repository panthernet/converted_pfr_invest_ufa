﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RegionReportImportView.xaml
    /// </summary>
    public partial class ImportNPFDlg
    {
        public ImportNPFDlg()
        {
            InitializeComponent();            
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }
    }
}
