﻿using System;
using System.Windows;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ReqTransferClaimInputDlg.xaml
    /// </summary>
    public partial class ReqTransferClaimInputDlg
    {
        public ReqTransferClaimInputDlg()
        {
            InitializeComponent();
            DataContextChanged += ReqTransferClaimInput_DataContextChanged;
        }

        private void ReqTransferClaimInput_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is IRequestCloseViewModel)
                ((IRequestCloseViewModel)e.NewValue).RequestClose += ReqTransferClaimInput_RequestClose;
        }

        private void ReqTransferClaimInput_RequestClose(object sender, EventArgs e)
        {
            DialogResult = true;
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
