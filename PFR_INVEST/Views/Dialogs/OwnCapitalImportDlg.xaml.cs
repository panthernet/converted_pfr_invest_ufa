﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for OwnCapitalImportDlg.xaml
    /// </summary>
    public partial class OwnCapitalImportDlg
    {
        public OwnCapitalImportDlg()
        {
            InitializeComponent();
        }

        private void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
