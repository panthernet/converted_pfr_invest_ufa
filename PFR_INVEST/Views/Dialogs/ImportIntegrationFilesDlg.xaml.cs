﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ImportIntegrationFilesDlg.xaml
    /// </summary>
    public partial class ImportIntegrationFilesDlg
    {
        public ImportIntegrationFilesDlgViewModel Model => DataContext as ImportIntegrationFilesDlgViewModel;

        public ImportIntegrationFilesDlg()
        {
            InitializeComponent();
            Closing += ImportIntegrationFilesDlg_Closing;
            Loaded += ImportIntegrationFilesDlg_Loaded;
            DataContextChanged += ImportIntegrationFilesDlg_DataContextChanged;
        }

        private void ImportIntegrationFilesDlg_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var model = e.NewValue as ImportIntegrationFilesDlgViewModel;
            if(model != null)
                model.UploadFinished += ImportIntegrationFilesDlg_UploadFinished;
        }

        private void ImportIntegrationFilesDlg_UploadFinished(object sender, System.EventArgs e)
        {
            DialogResult = true;
        }

        private void ImportIntegrationFilesDlg_Loaded(object sender, RoutedEventArgs e)
        {
            Model.UploadFiles();
        }

        private void ImportIntegrationFilesDlg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_isManualClose) e.Cancel = true;
        }

        private bool _isManualClose;
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            _isManualClose = true;
            Close();
        }
    }
}
