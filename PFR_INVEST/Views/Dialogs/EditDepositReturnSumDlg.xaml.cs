﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class EditDepositReturnSumDlg
    {
        public EditDepositReturnSumDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
