﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;

namespace PFR_INVEST.Views.Dialogs.Common
{
    /// <summary>
    /// Interaction logic for SelectUKView.xaml
    /// </summary>
    public partial class SelectYearMeasureView
    {
        public SelectYearMeasureViewModel VM => DataContext as SelectYearMeasureViewModel;

        public SelectYearMeasureView()
        {
            InitializeComponent();
        }
    }
}
