﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class UploadTemplateDlg
    {
        private List<string> DocsList { get; set; }
        private List<Department> DepList { get; set; }

        public Department SelectedDepartment { get; private set; }

        public UploadTemplateDlg(List<Department> dl)
        {
            InitializeComponent();
            DocsList = new List<string>();
            DepList = dl;
            but_clear.DataContext = but_rem.DataContext = OKButton.DataContext = this;
            lbDocs.ItemsSource = DocsList;
            cboxDepartment.ItemsSource = DepList;
            DeleteCommand = new DelegateCommand(o => CanExecuteDelete(), o => ExecuteDelete());
            ClearCommand = new DelegateCommand(o => CanExecuteClear(), o => ExecuteClear());
            UploadCommand = new DelegateCommand(o => CanExecuteUpload(), o => ExecuteUpload());

            cboxDepartment.SelectedIndex = DepList != null && DepList.Count > 0 ? 0 : -1;
        }

        #region Commands
        public ICommand DeleteCommand { get; protected set; }
        private bool CanExecuteDelete()
        {
            return DocsList.Count > 0 && lbDocs.SelectedIndex != -1;
        }

        private void ExecuteDelete()
        {
            foreach (var item in lbDocs.SelectedItems)
                DocsList.Remove(item.ToString());
            lbDocs.ItemsSource = null; lbDocs.ItemsSource = DocsList;
        }

        public ICommand ClearCommand { get; protected set; }
        private bool CanExecuteClear()
        {
            return DocsList.Count > 0;
        }

        private void ExecuteClear()
        {
            DocsList.Clear();
            lbDocs.ItemsSource = null; lbDocs.ItemsSource = DocsList;
        }

        public ICommand UploadCommand { get; protected set; }
        private bool CanExecuteUpload()
        {
            return DocsList.Count > 0;
        }

        private void ExecuteUpload()
        {
            SelectedDepartment = (Department)cboxDepartment.SelectedItem;
            FileStream fs = null;
            var tlist = WCFClient.Client.GetTemplatesList();
            int dubCounter = 0;
            foreach (string doc in DocsList)
            {
                var exist = tlist.FirstOrDefault(a => a.FileName == Path.GetFileName(doc));
                if (exist != null)
                {
                    dubCounter++;
                    DataContainerFacade.Delete<Template>(exist.ID);
                    //continue;
                }
                string fileName = Path.GetFileName(doc);
                try
                {
                    fs = File.OpenRead(doc);
                    var bytes = new byte[fs.Length];
                    fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                    fs.Close();

                    var t = new Template
                    {
                        DepartmentID = SelectedDepartment.ID,
                        Body = bytes,
                        FileName = fileName,
                        Date = DateTime.Today,
                        UNID = 0
                    };
                    DataContainerFacade.Save(t);
                }
                catch
                {
                    fs?.Close();
                    var message = $"Не удалось прочитать файл {fileName}. Убедитесь, что файл доступен для чтения и не открыт в данный момент.";
                    DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            if(dubCounter > 0)
                DXMessageBox.Show($"{dubCounter} файлов дубликатов было перезаписано.", Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Information);

            DialogResult = true;
        }
        #endregion

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void but_add_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog { DefaultExt = ".doc", Filter = "Шаблоны отчётов(.doc, .docx, .dot, .xls, .xlsx, .rdlc)|*.dot;*.xls;*.xlsx;*.doc;*.docx;*.rdlc", CheckPathExists = true, Multiselect = true };
            if (dlg.ShowDialog() != true) return;
            //optimize!
            foreach (string file in dlg.FileNames)
                DocsList.Add(file);
            lbDocs.ItemsSource = null;
            lbDocs.ItemsSource = DocsList;
        }
    }
}
