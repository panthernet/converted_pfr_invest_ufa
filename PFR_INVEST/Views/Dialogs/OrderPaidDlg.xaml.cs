﻿namespace PFR_INVEST.Views.Dialogs
{
	/// <summary>
	/// Interaction logic for OrderPaidDlg.xaml
	/// </summary>
	public partial class OrderPaidDlg
	{
		public OrderPaidDlg()
		{
			InitializeComponent();
		}

		private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
