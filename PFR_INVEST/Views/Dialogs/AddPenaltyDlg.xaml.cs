﻿using System.Windows;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddPenaltyDlg.xaml
    /// </summary>
    public partial class AddPenaltyDlg : DXWindow
    {
        public AddPenaltyDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as PrecisePenaltyViewModel;
            if (vm != null)
            {
                //vm.SaveCard.Execute(null);
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
