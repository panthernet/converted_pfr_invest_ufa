﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class LegalEntityCourierDlg
    {
        private bool _isDialogCompleted;
        public LegalEntityCourierDlgViewModel Model => DataContext as LegalEntityCourierDlgViewModel;

        public LegalEntityCourierDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = Model.IsValidLetterOfAttorney();
            DialogResult = _isDialogCompleted;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = true;
            DialogResult = false;
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_isDialogCompleted)//Skip asking if flag is already set
                return;

            bool? x = Model.AskSaveOnClose();

            switch (x)
            {
                case null:
                    e.Cancel = true;
                    break;
                case false:
                    DialogResult = false;
                    break;
                default:
                    if (Model.CanSaveOnClose())
                        DialogResult = true;
                    else
                        e.Cancel = true;
                    break;
            }
        }
    }
}
