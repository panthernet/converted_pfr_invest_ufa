﻿using System;
using System.Windows;
using System.Windows.Threading;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for FromUKEnterGetRequirementDateDlg.xaml
    /// </summary>
    public partial class FromUKEnterGetRequirementDateDlg
    {
        private readonly DispatcherTimer _timer;

        private ReqTransferViewModel Model => DataContext as ReqTransferViewModel;

        public FromUKEnterGetRequirementDateDlg()
        {
            InitializeComponent();
            dtRequestDeliveryDate.EditValue = DateTime.Now;
            dtSetSPNTransferDate.EditValue = DateTime.Now.AddDays(7);
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            _timer.Tick += Tick;
            _timer.Start();
        }

        public void SetTitle(string ukFormalName, string dogNum)
        {
            Title = $"Дата вручения требования в {ukFormalName}, договор № {dogNum}";
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            if (Model != null)
            {
                Model.RequestDeliveryDate = dtRequestDeliveryDate.EditValue == null ? null : (DateTime?)Convert.ToDateTime(dtRequestDeliveryDate.EditValue);
                Model.SetSPNTransferDate = dtSetSPNTransferDate.EditValue == null ? null : (DateTime?)Convert.ToDateTime(dtSetSPNTransferDate.EditValue);
                Model.TransferStatus = TransferStatusIdentifier.sDemandReceivedUK;
            }
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            DialogResult = false;
        }

        private void Tick(object state, EventArgs e)
        {
            OKButton.IsEnabled = dtRequestDeliveryDate.EditValue != null && dtSetSPNTransferDate.EditValue != null;
        }

        private void dtRequestDeliveryDate_EditValueChanged(object sender, DevExpress.Xpf.Editors.EditValueChangedEventArgs e)
        {
            var dt = dtRequestDeliveryDate.EditValue == null ? null : (DateTime?)Convert.ToDateTime(dtRequestDeliveryDate.EditValue);
            if (dt.HasValue)
                dtSetSPNTransferDate.EditValue = dt.Value.AddDays(7);
        }
    }
}
