﻿using System.Windows;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddNpfDlg.xaml
    /// </summary>
    public partial class AddNpfDlg
    {
        public AddNpfDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this, o => this.DialogResult = true);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
