﻿using System;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class LookupFIOGridDlg
    {
        public LookupFIOGridDlg()
        {
            InitializeComponent();            
        }

        private void gridView_DoubleClick(object sender, EventArgs e) {
            DialogResult = true;
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
