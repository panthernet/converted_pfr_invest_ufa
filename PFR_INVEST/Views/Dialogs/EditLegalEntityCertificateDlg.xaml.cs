﻿using System.Windows;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for EditLegalEntityCertificateDlg.xaml
    /// </summary>
    public partial class EditLegalEntityCertificateDlg
    {
        private bool _isDialogCompleted;
        public LegalEntityCertificateDlgViewModel Model => DataContext as LegalEntityCertificateDlgViewModel;

        public EditLegalEntityCertificateDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = Model.IsDataChanged;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _isDialogCompleted = true;
            DialogResult = false;
        }

        public string SelectedStockName
        {
            get
            {
                var value = (Element)StockCombo.SelectedItem;
                return value.Name ?? string.Empty;
            }
        }

        private void DXWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_isDialogCompleted)//Skip asking if flag is already set
                return;

            bool? x = Model.AskSaveOnClose();
            switch (x)
            {
                case null:
                    e.Cancel = true;
                    break;
                case false:
                    DialogResult = false;
                    break;
                default:
                    if (Model.CanSaveOnClose())
                        DialogResult = true;
                    else
                        e.Cancel = true;
                    break;
            }
        }

    }
}
