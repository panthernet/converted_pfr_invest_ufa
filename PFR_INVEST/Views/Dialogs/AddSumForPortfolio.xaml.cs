﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core.Native;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddSumForPortfolio.xaml
    /// </summary>
    public partial class AddSumForPortfolio
    {
        private AddSumForPortfolioViewModel Model => (AddSumForPortfolioViewModel)DataContext;

        public AddSumForPortfolio(bool onlyInteger = false)
        {
            InitializeComponent();
            Loaded += AddSumForPortfolio_Loaded;
            if (onlyInteger)
            {
                labelSum.Content = "Сумма, млн рублей";
                textField.Mask = "###,###,###,###,###;";
            }
        }

        protected void AddSumForPortfolio_Loaded(object sender, RoutedEventArgs e)
        {

            var button = (Button)LayoutHelper.FindElementByName(this, ButtonParts.PART_Minimize.ToString());
            button.IsHitTestVisible = false;
            button.Opacity = 0;
            // FocusedRowChanged не обрабатывает переходы по заголовкам групп
            KeyUp += SelectedItemChanged;
            MouseUp += SelectedItemChanged;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as AddSumForPortfolioViewModel;
            if (vm != null)
            {
                DialogResult = true;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        public void SelectedItemChanged(object sender, EventArgs e)
        {
            Grid_OnCurrentItemChanged(null, null);
        }

        private bool IsPortfolioSelected()
        {
            int lvl = Grid.View.FocusedRowData.Level;
            if (lvl < Grid.GetGroupedColumns().Count)
                return (Grid.GetGroupedColumns()[lvl].FieldName == "PFName");
            return false;
        }

        private long GetPortfolioId()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "PortfolioID"));
        }

        public bool IsAccountSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        private long GetAccountId()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "AccountID"));
        }

        private void Grid_OnCurrentItemChanged(object sender, CurrentItemChangedEventArgs e)
        {
            if (IsPortfolioSelected())
                Model.SetSelectionByPortfolio(GetPortfolioId());
            else if (IsAccountSelected())
                Model.SetSelectionByAccount(GetPortfolioId(), GetAccountId());
            else
                Model.SelectedItem = null;
        }
    }
}
