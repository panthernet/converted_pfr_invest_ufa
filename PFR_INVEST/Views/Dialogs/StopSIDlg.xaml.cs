﻿using System.Windows;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddSIContactDlg.xaml
    /// </summary>
    public partial class StopSIDlg
    {
        public StopSIDlg(StopSIDialogModel.StopType type)
        {
            InitializeComponent();
            Title = type == StopSIDialogModel.StopType.StopСontract ? "Расторжение договора" : "Прекращение деятельности";
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
