﻿using System.Windows;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for RenameSI.xaml
    /// </summary>
    public partial class SelectCommonPPDlgView
    {
		public SelectCommonPPDlgView()
        {
            InitializeComponent();
			DataContextChanged += SelectCommonPPDlgView_DataContextChanged;
            tableView.Loaded += tableView_Loaded;
        }

        private void tableView_Loaded(object sender, RoutedEventArgs e)
        {
            tableView.SearchControl?.Focus();
        }

        private void SelectCommonPPDlgView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
		    var viewModel = DataContext as IDialogViewModel;
		    if(viewModel != null)
				viewModel.OnCloseRequested += SelectCommonPPDlgView_OnCloseRequested;
		}

        private void SelectCommonPPDlgView_OnCloseRequested(object sender, EventArgs<bool?> e)
		{
			DialogResult = e.Value;
		}

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {            
            //DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

		private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			
		}
    }
}
