﻿namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
    public partial class ExportDepClaim2Dlg
    {
        public ExportDepClaim2Dlg()
        {
            InitializeComponent();           
        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
