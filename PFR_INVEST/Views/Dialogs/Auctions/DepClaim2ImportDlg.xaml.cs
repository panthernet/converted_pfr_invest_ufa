﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for DepClaim2ImportDlg.xaml
    /// </summary>
    public partial class DepClaim2ImportDlg
    {
        public DepClaim2ImportDlg(string title)
        {

            InitializeComponent();
            Title = title;
        }

        private void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
