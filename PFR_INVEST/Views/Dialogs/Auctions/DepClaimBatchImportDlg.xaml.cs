﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for DepClaim2ImportDlg.xaml
    /// </summary>
    public partial class DepClaimBatchImportDlg
    {
        public DepClaimBatchImportDlg(string title)
        {

            InitializeComponent();
            Title = title;
        }

        private void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
