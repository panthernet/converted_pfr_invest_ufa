﻿using System;
using System.Windows;
using System.Windows.Threading;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for FromUKEnterRequirementDataDlg.xaml
    /// </summary>
    public partial class FromUKEnterRequirementDataDlg
    {
        private readonly DispatcherTimer _timer;

        private ReqTransferViewModel Model => DataContext as ReqTransferViewModel;

        public FromUKEnterRequirementDataDlg()
        {
            InitializeComponent();
            dtRequestFormingDate.EditValue = DateTime.Now;
            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };
            _timer.Tick += Tick;
            _timer.Start();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            if (Model != null)
            {
                Model.RequestFormingDate = dtRequestFormingDate.EditValue == null ? null : (DateTime?)Convert.ToDateTime(dtRequestFormingDate.EditValue);
                Model.RequestNumber = txtRequestNumber.Text;
                Model.TransferStatus = TransferStatusIdentifier.sDemandSentInUK;
            }
            DialogResult = true;
        }

        public void setTitle(string ukFormalName, string dogNum)
        {
            Title = $"Реквизиты требования для {ukFormalName}, договор № {dogNum}";
        }

        public void setTitle(string message)
        {
            Title = message;
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            DialogResult = false;
        }

        private void Tick(object state, EventArgs e)
        {
            OKButton.IsEnabled = txtRequestNumber.Text.Length > 0 && dtRequestFormingDate.EditValue != null;
        }
    }
}
