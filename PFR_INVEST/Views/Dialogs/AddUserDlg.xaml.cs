﻿using System.Windows;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AddUserDlg.xaml
    /// </summary>
    public partial class AddUserDlg
    {
        public AddUserDlg(bool isdelete)
        {
            InitializeComponent();

            if (isdelete)
                Title = "Удаление пользователей";
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
