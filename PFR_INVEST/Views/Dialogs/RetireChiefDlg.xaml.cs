﻿using System.Windows;
using DevExpress.Xpf.Core;
using System;

namespace PFR_INVEST.Views.Dialogs
{
    /// <summary>
    /// Interaction logic for AnnulLicenceDlg.xaml
    /// </summary>
    public partial class RetireChiefDlg
    {
        public DateTime? InaugurationDate {get;set;}

        public RetireChiefDlg()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (retireDate.EditValue == null)
            {
                DXMessageBox.Show("Укажите дату прекращения полномочий!", "Ошибка ввода данных!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (InaugurationDate != null && InaugurationDate > (DateTime)retireDate.EditValue)
            {
                DXMessageBox.Show("Дата прекращения полномочий меньше даты вступления", "Ошибка ввода данных!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
