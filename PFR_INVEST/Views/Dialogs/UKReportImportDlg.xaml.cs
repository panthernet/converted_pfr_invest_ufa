﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.RegionsReport
{
    using DataAccess.Server.UKReport;
    using DevExpress.Xpf.Grid;
    using System;

    /// <summary>
    /// Interaction logic for RegionReportImportView.xaml
    /// </summary>
    public partial class UKReportImportDlg
    {

        private UKReportImportDlgViewModel Model => DataContext as UKReportImportDlgViewModel;


        public UKReportImportDlg()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCloseRequest(this);
        }

        private void TableView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            var item = ImportItem.SelectedItem as IUKReportImportItem;
            Model.SelectedRemovedItems = item != null ? new List<IUKReportImportItem> { item } : null;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "ReportOnDateNative") return;

            var source = Model.Items;

            foreach (var o in (List<object>) e.ComboBoxEdit.ItemsSource)
            {
                var item = o as CustomComboBoxItem;
                var editvalue = item?.EditValue as DateTime?;
                if (editvalue != null)
                {
                    var si = source.FirstOrDefault(x => x.ReportOnDateNative.HasValue && x.ReportOnDateNative.Value.Equals(editvalue.Value));
                    if (si != null)
                        item.DisplayValue = si.ReportOnDate;
                }
            }
        }

        private void OnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName != "ReportOnDateNative") return;
            var item = e.Row as IUKReportImportItem;
            if (item != null)
                e.DisplayText = item.ReportOnDate;
        }
    }
}
