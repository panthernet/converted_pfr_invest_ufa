﻿namespace PFR_INVEST.Views.Dialogs
{
    using DevExpress.Xpf.Core;
    using System.Linq;
    using PFR_INVEST.BusinessLogic.ViewModelsList;
    using PFR_INVEST.Constants.Identifiers;
    using PFR_INVEST.DataObjects.ListItems;

    /// <summary>
    /// Interaction logic for ExportDepClaimMaxListDlg.xaml
    /// </summary>
    public partial class ExportDepClaimMaxListDlg : DXWindow
    {
        public ExportDepClaimMaxListDlg()
        {
            InitializeComponent();
            if (new DepClaimMaxListViewModel(DepClaimMaxListItem.Types.Max).DepClaimMaxList.Any())
            {
                this.OKButton.IsEnabled = true;
                this.Message.Content = "Реестр сформирован:";
                this.DateEdit.SetVisible(true);
            }
            else
            {
                this.OKButton.IsEnabled = false;
                this.Message.Content = "Нет данных для экспорта";
                this.DateEdit.SetVisible(false);
            }

        }

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
