﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.Views.Dialogs
{
    internal enum AuctionCbInOrderType
    {
        AuctionCbInOrderTypeNone = 0,
        AuctionCbInOrderTypeCount,
        AuctionCbInOrderTypeSumMoney
    }

    /// <summary>
    /// Interaction logic for AddSecurityDlg.xaml
    /// </summary>
    public partial class SecurityInOrderDialog
    {
        private const string NO_AVAILABLE_SECURITIES_FOUND_MSG = "В текущее поручение больше нельзя добавить ценные бумаги. В портфеле нет доступных бумаг, все доступные бумаги добавлены в поручение, либо их количество недостаточно для продажи.";

        private AuctionCbInOrderType _auctionCbInOrderType;

        private SecurityInOrderViewModel Model => DataContext as SecurityInOrderViewModel;

        public SecurityInOrderDialog()
        {
            InitializeComponent();
            Loaded += SecurityInOrderDialog_Loaded;
        }

        private void SecurityInOrderDialog_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model == null || Model.SecurityInOrderType != SecurityInOrderType.BuyRoublesOnAuction) return;
            if (!Model.AlreadyAddedSecurities.Any()) return;
            var cbinorders = Model.AlreadyAddedSecurities;
            if (cbinorders != null && cbinorders.Any())
            {
                var cbinorder = cbinorders.First();
                _auctionCbInOrderType = (cbinorder.Count ?? 0) == 0
                    ? AuctionCbInOrderType.AuctionCbInOrderTypeSumMoney
                    : AuctionCbInOrderType.AuctionCbInOrderTypeCount;
            }
        }

        private void CloseDialog(bool result)
        {
            DialogResult = result;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            CloseDialog(false);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Model.RemoveSecurity(auctionSecuritiesGrid.SelectedItem as CbInOrderListItem);
        }

        private void DXWindow_ContentRendered(object sender, EventArgs e)
        {
            if (Model == null || (OrderTypeIdentifier.IsSellOrder(Model.Order.Type) &&  Model.NoAvailableSecuritiesFound))
            {
                DXMessageBox.Show(NO_AVAILABLE_SECURITIES_FOUND_MSG, "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
                CloseDialog(false);
            }

            if (Model.SecurityInOrderType == SecurityInOrderType.BuyRoublesOnAuction)
                contentGrid.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star);

            Title = $"Добавление ЦБ в поручение {Model.Order.RegNum} ({Model.SecurityInOrderType.GetDescription()})";

            Model.OnCbInOrderListItemCreated += Model_OnCbInOrderListItemCreated;
            Model.OnNeedUpdateControlsValidationState += Model_OnNeedUpdateControlsValidationState;
        }

        private enum TextEditPairEnabledState
        {
            TextEditPairEnabledStateFirst,
            TextEditPairEnabledStateSecond,
            TextEditPairEnabledStateBoth
        }
        private static void SetTextEditPairEnabledState(UIElement firstTe, TextEdit secondTe, TextEditPairEnabledState enabledState)
        {
            switch (enabledState)
            {
                case TextEditPairEnabledState.TextEditPairEnabledStateFirst:
                    firstTe.IsEnabled = true;
                    secondTe.IsEnabled = false;
                    return;
                case TextEditPairEnabledState.TextEditPairEnabledStateSecond:
                    firstTe.IsEnabled = false;
                    secondTe.IsEnabled = true;
                    return;
                case TextEditPairEnabledState.TextEditPairEnabledStateBoth:
                    firstTe.IsEnabled = true;
                    secondTe.IsEnabled = true;
                    return;
            }
        }

        protected void EnsureContentGridControlsEnabledState(SecurityInOrderType pOrderType)
        {
            switch (pOrderType)
            {
                case SecurityInOrderType.BuyCurrency:
                    if (Model.TotalNomValue > 0)
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMaxCostValue, TextEditPairEnabledState.TextEditPairEnabledStateFirst);
                    else if (Model.MaxCostValue > 0)
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMaxCostValue, TextEditPairEnabledState.TextEditPairEnabledStateSecond);
                    else
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMaxCostValue, TextEditPairEnabledState.TextEditPairEnabledStateBoth);
                    return;
                case SecurityInOrderType.SaleCurrency:
                    if (Model.TotalNomValue > 0)
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMinCostValue, TextEditPairEnabledState.TextEditPairEnabledStateFirst);
                    else if (Model.MinCostValue > 0)
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMinCostValue, TextEditPairEnabledState.TextEditPairEnabledStateSecond);
                    else
                        SetTextEditPairEnabledState(txtTotalNomValue, txtMinCostValue, TextEditPairEnabledState.TextEditPairEnabledStateBoth);
                    return;
                case SecurityInOrderType.BuyRublesOnSecondMarket:
                    if (Model.Count > 0)
                        SetTextEditPairEnabledState(txtCount, txtSumMoney, TextEditPairEnabledState.TextEditPairEnabledStateFirst);
                    else if (Model.SumMoney > 0)
                        SetTextEditPairEnabledState(txtCount, txtSumMoney, TextEditPairEnabledState.TextEditPairEnabledStateSecond);
                    else
                        SetTextEditPairEnabledState(txtCount, txtSumMoney, TextEditPairEnabledState.TextEditPairEnabledStateBoth);
                    return;
                default:
                    return;
            }
        }

        private void Model_OnNeedUpdateControlsValidationState(object sender, EventArgs e)
        {
            var textBoxes = (from child in contentGrid.Children.Cast<UIElement>().ToList()
                             where child.GetType().IsAssignableFrom(typeof(TextEdit))
                             select child).ToList();
            textBoxes.ForEach
            (
                txt => BindingOperations.GetBindingExpression(txt, BaseEdit.EditValueProperty).UpdateTarget()
            );

            EnsureContentGridControlsEnabledState(Model.SecurityInOrderType);
        }

        private void Model_OnCbInOrderListItemCreated(object sender, EventArgs e)
        {
            CloseDialog(true);
        }

        private void AuctionSecuritiesTableView_ShowingEditor(object sender, ShowingEditorEventArgs e)
        {
            auctionSecuritiesTableView.PostEditor();
            var item = e.Row as CbInOrderListItem;
            if (_auctionCbInOrderType == AuctionCbInOrderType.AuctionCbInOrderTypeNone)
            {
                if (e.Column.FieldName == "Count" && item.SumMoney > 0)
                {
                    e.Cancel = true;
                    _auctionCbInOrderType = AuctionCbInOrderType.AuctionCbInOrderTypeSumMoney;
                }
                else if (e.Column.FieldName == "SumMoney" && item.Count > 0)
                {
                    e.Cancel = true;
                    _auctionCbInOrderType = AuctionCbInOrderType.AuctionCbInOrderTypeCount;
                }
            }
            else if (e.Column.FieldName == "Count" && _auctionCbInOrderType == AuctionCbInOrderType.AuctionCbInOrderTypeSumMoney)
                e.Cancel = true;
            else if (e.Column.FieldName == "SumMoney" && _auctionCbInOrderType == AuctionCbInOrderType.AuctionCbInOrderTypeCount)
                e.Cancel = true;
        }

        private void AuctionSecuritiesTableView_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            auctionSecuritiesTableView.PostEditor();
        }

        private void AuctionSecuritiesTableView_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            var item = e.Row as CbInOrderListItem;
            if (_auctionCbInOrderType == AuctionCbInOrderType.AuctionCbInOrderTypeNone)
            {
                if (e.Column.FieldName == "SumMoney" && item.SumMoney > 0)
                    _auctionCbInOrderType = AuctionCbInOrderType.AuctionCbInOrderTypeSumMoney;
                else if (e.Column.FieldName == "Count" && item.Count > 0)
                    _auctionCbInOrderType = AuctionCbInOrderType.AuctionCbInOrderTypeCount;
            }
            else
            {
                var cbinorders = Model.AlreadyAddedSecurities;
                if (cbinorders == null || !cbinorders.Any())
                    if (item.SumMoney.GetValueOrDefault() == 0 && item.Count.GetValueOrDefault() == 0)
                        _auctionCbInOrderType = AuctionCbInOrderType.AuctionCbInOrderTypeNone;
            }
        }
    }

    [ValueConversion(typeof(SecurityInOrderType), typeof(GridLength))]
    public class SecurityInOrderTypeToContentGridGridLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return (SecurityInOrderType)value == SecurityInOrderType.SaleRoubles ? GridLength.Auto : new GridLength(1, GridUnitType.Star);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return null;
        }
    }

    [ValueConversion(typeof(SecurityInOrderType), typeof(Visibility))]
    public class SecurityInOrderTypeToHintVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return (SecurityInOrderType)value == SecurityInOrderType.SaleRoubles ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return null;
        }
    }
}
