﻿using System.Windows;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views.Dialogs
{
    public partial class InvDeclDlg
    {
        public bool ShowOldNames { get; set; }
        public bool ShowEndedAgreements { get; set; }

        public InvDeclDlg()
        {
            InitializeComponent();
            ThemesTool.SetCurrentTheme(this);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ShowOldNames = ck_old.IsChecked ?? false;
            ShowEndedAgreements = ck_dog.IsChecked ?? false;
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
