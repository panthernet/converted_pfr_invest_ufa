﻿namespace PFR_INVEST.Views.Dialogs
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ImportDepClaimMaxDlg.xaml
    /// </summary>
    public partial class ImportDepClaimMaxDlg
    {
        public ImportDepClaimMaxDlg(string title)
        {
            InitializeComponent();
            Title = title;
        }

        private void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
