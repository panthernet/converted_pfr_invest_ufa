﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DevExpress.Xpf.Grid;
    using DataObjects;

    /// <summary>
    /// Interaction logic for DepclaimMaxListView.xaml
    /// </summary>
    public partial class DepClaim2ConfirmListView
    {
        public DepClaim2ConfirmListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            tableView.FocusedRowHandleChanged += tableView_FocusedRowHandleChanged;
        }

        private void tableView_FocusedRowHandleChanged(object sender, FocusedRowHandleChangedEventArgs e)
        {
            if (tableView.FocusedRowData.Row is DepClaim2)
            {
                var dc = ((DepClaim2) tableView.FocusedRowData.Row);
                SelectedAuctionIDHelper.Value = dc.AuctionID;
                SelectedAuctionsStatusHelper.Value = dc.AuctionStatus;
            }
            else
            {
                SelectedAuctionIDHelper.Value = null;
                SelectedAuctionsStatusHelper.Value = null;
            }
        }

    }
}
