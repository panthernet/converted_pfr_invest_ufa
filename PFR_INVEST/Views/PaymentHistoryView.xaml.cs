﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for IncomeSecurityView.xaml
    /// </summary>
    public partial class PaymentHistoryView : UserControl
    {
        public PaymentHistoryView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
