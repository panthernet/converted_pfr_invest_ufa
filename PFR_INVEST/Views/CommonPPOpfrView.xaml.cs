﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for EnterTransferPPDataView.xaml
    /// </summary>
    public partial class CommonPPOpfrView : IModelBindableWithParams
    {
        public Type ModelType => typeof(CommonPPViewModel);
        public object[] Params => new object[] { "$id", "$state", true };
        public CommonPPViewModel ViewModel => DataContext as CommonPPViewModel;

        public CommonPPOpfrView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OpfrTransferView), ViewModelState.Edit,
                ViewModel.SelectedOpfrTransfer.ID, ViewModel.SelectedOpfrTransfer.OpfrRegisterID, "Перечисление");
        }

        private void gridControl_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Num")
                e.Value = e.ListSourceRowIndex + 1;
        }
    }
}
