﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Helpers;
using PFR_INVEST.DataObjects.ListItems;
using System.Linq;
using System.Windows.Controls;
using DevExpress.Data;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OnesImportJournalListView.xaml
    /// </summary>
    public partial class OpfrListView
    {
        private bool _isMouseDoubleClick;
        public OpfrListViewModel Model => DataContext as OpfrListViewModel;
        private int _currentGroupHandle = 0;
        public OpfrListView()
        {
            InitializeComponent();
            //ModelInteractionHelper.SubscribeForGridRefreshHelper(this, listGrid);
            //ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, listGrid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            listGrid.GroupRowCollapsing += (o, e) => { e.Allow = !_isMouseDoubleClick; _isMouseDoubleClick = false; };
            listGrid.GroupRowExpanding += (o, e) => { e.Allow = !_isMouseDoubleClick; _isMouseDoubleClick = false; };
            listGrid.CustomColumnSort += ListGrid_CustomColumnSort;
            listGrid.View.ShowFilterPopup += View_ShowFilterPopup;

            if (!App.ServerSettings.IsOpfrTransfersEnabled)
                ppColumn.Visible = false;

            Loaded += ViewLoaded;
        }
        
        private void View_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "RegisterName") return;

            var x = (e.ComboBoxEdit.ItemsSource as List<object>);
            x.RemoveAll(a => a is Separator);
            var list = x.ConvertAll(a => (CustomComboBoxItem)a);
            list = list.OrderBy(a =>
            {
                var lst = a.DisplayValue.ToString().Split('\t').FirstOrDefault()?.Trim();
                if (lst == null) return 0L;
                long result;
                return long.TryParse(lst, out result) ? result : 0L;
            }).ToList();
            e.ComboBoxEdit.ItemsSource = list;
            e.Handled = true;
        }

        private void ListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName != "RegisterName") return;
            var source = listGrid.ItemsSource as ObservableCollection<OpfrRegisterListItem>;
            var r1 = source[e.ListSourceRowIndex1].ID;
            var r2 = source[e.ListSourceRowIndex2].ID;
            e.Result = e.SortOrder == ColumnSortOrder.Ascending ? r1.CompareTo(r2) : r2.CompareTo(r1);
            e.Handled = true;
        }

        private void ViewLoaded(object sender, RoutedEventArgs e)
        {
            if (Model == null)
                return;
            listGrid.CustomColumnDisplayText += Grid_CustomColumnDisplayText;
            //Сохранение и возобнавление фокуса строки в гриде с группировками
            Model.PropertyChanged += (a, b) =>
            {
                if (b.PropertyName.Equals("ListRefreshing"))
                {
                    var grid = listGrid;
                    _currentGroupHandle = grid.View.FocusedRowData.RowHandle.Value;
                }
                else if (b.PropertyName.Equals("ListRefreshed"))
                {
                    listGrid.View.FocusedRowHandle = _currentGroupHandle;
                }
            };
        }

        private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            if (!listGrid.IsValidRowHandle(e.RowHandle))
                return;

            if ((e.Column.FieldName == "Kosgu" || e.Column.FieldName == "OpfrName") && string.IsNullOrEmpty(e.DisplayText))
                e.DisplayText = " (Отсутствуют перечисления)";
        }

        public bool IsRegisterSelected()
        {
            var row = listGrid.GetFocusedRow() as OpfrRegisterListItem;
            var x = listGrid.GetGroupRowValue(listGrid.View.FocusedRowHandle) ?? "";
            return listGrid.View.FocusedRowData.Level < listGrid.GetGroupedColumns().Count && x.ToString() == row?.RegisterName; //.StartsWith("№");
        }

        public bool IsRegisterHaveTransfers()
        {
            var isReg = IsRegisterSelected();
            if (!isReg) return false;
            var regId = GetRegisterID();
            return Model.List.FirstOrDefault(a => a.TransferID != null && a.TransferID != 0 && a.ID == regId) != null;
        }

        public bool IsRegisterAllowedForExport()
        {
            var isReg = IsRegisterSelected();
            if (!isReg) return false;
            var d = (listGrid.View.FocusedRowData.Row as OpfrRegisterListItem);
            return d.TrancheDate.HasValue && !string.IsNullOrEmpty(d.TrancheNum);
        }

        public bool IsTransferSelected()
        {
            int lvl = listGrid.View.FocusedRowData.Level;
            var gCols = listGrid.GetGroupedColumns();
            return lvl == gCols.Count && (listGrid.CurrentItem != null && Convert.ToInt64(listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "TransferID")) != 0);
        }

        public long GetRegisterID()
        {
            return Convert.ToInt64(listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "ID"));
        }

        public AsgFinTr.Directions GetTransferDirection()
        {
            return ((string)listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "TypeName")).ToLower().StartsWith("из пфр") ? AsgFinTr.Directions.FromPFR : AsgFinTr.Directions.ToPFR;
        }


        public long GetTransferID()
        {
            return Convert.ToInt64(listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "TransferID"));
        }


        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }


        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            _isMouseDoubleClick = true;
            if (IsRegisterSelected())
            {
                long regId = GetRegisterID();
                App.DashboardManager.OpenNewTab(typeof(OpfrRegisterView), ViewModelState.Edit,
                    regId, "Распоряжение");
            }
            else if (IsTransferSelected())
            {
                App.DashboardManager.OpenNewTab(typeof(OpfrTransferView), ViewModelState.Edit,
                    GetTransferID(), (object)GetRegisterID(), "Перечисление");
            }
            else _isMouseDoubleClick = false;
        }

        public bool GetRegisterHasTransfers()
        {
            return listGrid.GetCellValue(listGrid.View.FocusedRowHandle, "TransferID") != null;
        }
    }
}
