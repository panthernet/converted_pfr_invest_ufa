﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RecalcBuyOrSaleReportsNKDView.xaml
    /// </summary>
    public partial class RecalcBuyOrSaleReportsNKDView : UserControl
    {
        //cached for threads
        private RecalcBuyOrSaleReportsNKDViewModel m_model;
        public RecalcBuyOrSaleReportsNKDViewModel Model => m_model ?? (m_model = DataContext as RecalcBuyOrSaleReportsNKDViewModel);


        public RecalcBuyOrSaleReportsNKDView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        protected void btnStartRecalc_Click(object sender, EventArgs e)
        {
            Model.LoadReports();
            btnStartRecalc.IsEnabled = false;
            btnStopRecalc.IsEnabled = true;

            progress.Maximum = Model.ReportsCount + 1;
            progress.Minimum = 0;
            progress.Value = 0;
            ProcessNextReport();
        }

        private void ProcessNextReport()
        {
            ThreadPool.QueueUserWorkItem(ProcessNextContrtactAsync);
        }

        private void ProcessNextContrtactAsync(object state)
        {
            try
            {
                Model.RecalcNextReport();
            }
            catch
            {

            }

            UpdateProgressStatus();

            if (Model.Canceled)
            {
                Dispatcher.Invoke(new Action(RecalcualtionCancelled));
                return;
            }

            if (Model.IsRecalcuationFinished())
            {

                Dispatcher.Invoke(new Action(RecalcualtionFinished));
            }
            else
            {
                ProcessNextReport();
            }
        }

        private void UpdateProgressStatus()
        {
            Dispatcher.Invoke(new Action(delegate()
            {
                progress.Value = Model.CurrentReportIndex;
            }));
        }

        private void RecalcualtionFinished()
        {
            progress.Value = progress.Maximum;
            DXMessageBox.Show($"Пересчет завершен. Всего {Model.ReportsCount} Ошибок {Model.ErrorsCount}");
            btnStartRecalc.IsEnabled = true;
            btnStopRecalc.IsEnabled = false;
            progress.Value = progress.Minimum;
        }

        private void RecalcualtionCancelled()
        {
            DXMessageBox.Show("Пересчет отменен");
            btnStartRecalc.IsEnabled = true;
            btnStopRecalc.IsEnabled = false;
        }

        private void btnStopRecalc_Click(object sender, RoutedEventArgs e)
        {
            Model.Canceled = true;
        }
    }
}
