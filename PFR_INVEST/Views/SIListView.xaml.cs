﻿using System;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIListView.xaml
    /// </summary>
    public partial class SIListView : ISISource
    {
        public SIListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, SIGrid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SIGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsLegalEntitySelected())
                return;
            var lID = GetSelectedLegalEntityID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(SIView), ViewModelState.Edit, lID, "Субъект");
        }

        public bool IsLegalEntitySelected()
        {
            return SIGrid.View.FocusedRowData.Level >= SIGrid.GetGroupedColumns().Count && SIGrid.SelectedItem != null;
        }

        public long GetSelectedLegalEntityID()
        {
            return Convert.ToInt64(SIGrid.GetCellValue(SIGrid.View.FocusedRowHandle, "LegalEntityID"));
        }

        public string GetSelectedLegalEntityName()
        {
            return (string)SIGrid.GetCellValue(SIGrid.View.FocusedRowHandle, "ContragentName");
        }

        private DateTime? GetSelectedCloseDate()
        {
            var value = SIGrid.GetCellValue(SIGrid.View.FocusedRowHandle, "CloseDate");
            return value == null ? null : (DateTime?)Convert.ToDateTime(value);
        }

        public bool CanRestoreSI()
        {
            var closeDate = GetSelectedCloseDate();
            if (GetSelectedLegalEntityID() <= 0)
                return false;
            return closeDate != null && closeDate != DateTime.MinValue;
        }

        public bool CanStopSI()
        {
            var closeDate = GetSelectedCloseDate();
            if (GetSelectedLegalEntityID() <= 0)
                return false;
            return closeDate == null || closeDate == DateTime.MinValue;
        }

        public long GetSIID()
        {
            return GetSelectedLegalEntityID();
        }
    }
}
