﻿using System;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BranchView.xaml
    /// </summary>
    public partial class StockView : IModelBindableWithId
    {
        public Type ModelType => typeof(StockViewModel);

        public StockView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public bool IsIdOnly => true;
    }
}
