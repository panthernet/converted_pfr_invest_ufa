﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for TempAllocationListView.xaml
    /// </summary>
    public partial class TempAllocationListView
    {
        public TempAllocationListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
        }

        private bool IsDocSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        public TempAllocationListItem GetSelectedItem()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return Grid.SelectedItem as TempAllocationListItem;
            return null;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

            if (!IsDocSelected())
                return;

            var item = GetSelectedItem();

            if (item == null)
                return;

            if (item.IsExtra)
            {
                MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
                return;
            }

            switch (item.OperationType)
            {
                case TempAllocationListItem.enOperationType.PaperBuy:
                    App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Edit, item.ID, OrderReportsHelper.BUYING_REPORT);
                    break;
                case TempAllocationListItem.enOperationType.PaperSell:
                    App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Edit, item.ID, OrderReportsHelper.SELLING_REPORT);
                    break;

                case TempAllocationListItem.enOperationType.DepositAllocate:
                case TempAllocationListItem.enOperationType.DepositReturn:
                    OpenForm<DepositView>(item.ID, item.OperationType == TempAllocationListItem.enOperationType.DepositAllocate ? "Размещение депозита" : "Возврат депозита");
                    break;
            }
        }

        private static void OpenForm<T>(long id, string title)
        {
            App.DashboardManager.OpenNewTab(typeof(T), ViewModelState.Read, id, title);
        }

        public long GetSelectedID()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return (long)Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");

            return 0;
        }

        public string GetSelectedOperation()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return Grid.GetCellValue(Grid.View.FocusedRowHandle, "Operation").ToString();

            return string.Empty;
        }

        private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }
}
