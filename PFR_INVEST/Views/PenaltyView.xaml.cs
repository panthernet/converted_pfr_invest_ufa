﻿using System;
using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueView.xaml
    /// </summary>
    public partial class PenaltyView : UserControl
    {
        public PenaltyView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            c.MouseDoubleClick += (sender, e) =>
            {
                if (c.View.GetRowElementByMouseEventArgs(e) == null)
                    return;

                var id =  Convert.ToInt64(c.GetCellValue(c.View.FocusedRowHandle, "ID"));
                if(id > 0)
                    App.DashboardManager.OpenNewTab(typeof(PrecisePenaltyView), ViewModelState.Edit, id, "Уточнение пени и штрафов");
            };
        }
    }
}
