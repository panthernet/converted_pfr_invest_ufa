﻿using System;
using System.Globalization;
using System.Windows.Data;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for LoadedODKLogSuccessListView.xaml
    /// </summary>
    public partial class LoadedODKLogSuccessListView
    {
        public LoadedODKLogSuccessListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }
    }

    public class DocFormToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string docForm = value as string;
            //if (docForm == "EDO_ODKF401" || docForm == "EDO_ODKF402")
            //    return false;
            //else
                return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
