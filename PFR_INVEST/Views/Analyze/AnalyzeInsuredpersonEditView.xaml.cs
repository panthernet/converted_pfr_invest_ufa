﻿using System;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzeInsuredpersonEditView.xaml
    /// </summary>
    public partial class AnalyzeInsuredpersonEditView : UserControl
    {
        public AnalyzeInsuredpersonEditViewModel VM => (AnalyzeInsuredpersonEditViewModel)DataContext;
        public AnalyzeInsuredpersonEditView(AnalyzeInsuredpersonEditViewModel vm)
        {
            InitializeComponent();
            DataContext = vm;
            ModelInteractionHelper.SignUpForCloseRequest(this);
            Loaded += OnLoaded;
        }
        private void OnLoaded(object sender, EventArgs e)
        {
            if (VM.Years.Count > 0)
                return;
            Visibility = Visibility.Hidden;
            DXMessageBox.Show(@"Все отчеты на доступные периоды уже созданы!", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            DashboardManager.CloseActiveView();
        }
        
    }
}
