﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.PivotGrid;
using DevExpress.Xpf.PivotGrid.Internal;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Views.Dialogs;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzeInsuredpersonPivotView.xaml
    /// </summary>
    public partial class AnalyzePaymentyearratePivotView : UserControl
    {
        AnalyzePaymentyearratePivotViewModel VM { get { return DataContext as AnalyzePaymentyearratePivotViewModel; } }
        public AnalyzePaymentyearratePivotView(AnalyzePaymentyearratePivotViewModel vm)
        {
            DataContext = vm;
            InitializeComponent();
            Loaded += AnalyzeInsuredpersonPivotView_Loaded;
            
            pivot7.FieldValueDisplayText += Pivot7_FieldValueDisplayText;
            pivot7.CustomCellValue += Pivot7_CustomCellValue;
            pivot7.AllowExpandOnDoubleClick = false;
            pivot7.Loaded += Pivot7_Loaded;
        }

        private double mousePath = 0;
        private void Pivot7_Loaded(object sender, RoutedEventArgs e)
        {
            pivot7.Visibility = Visibility.Hidden;
            int firstIndex = pivot7.GetRowIndex("Средний размер выплат за счет СПН (накопленным итогом), руб.");
            int secondIndex = pivot7.GetRowIndex("Выплаты правоприемникам застрахованных лиц (с 2007 года)");
            pivot7.ScrollTo(new System.Drawing.Point(0, firstIndex));
            pivot7.ScrollTo(new System.Drawing.Point(0, secondIndex));
            pivot7.ScrollTo(new System.Drawing.Point(0, 0));
            pivot7.Visibility = Visibility.Visible;

        }

        private List<int> indexesForRemove = new List<int>();
        private int _fieldIndexForClearSum = -1;

        private void Pivot7_CustomCellValue(object sender, PivotCellValueEventArgs e)
        {
            if (e.Value == null || e.RowValueType != FieldValueType.Total)
                return;

            if (indexesForRemove.Any(x => x == e.RowFieldIndex))
            {
                e.Value = string.Empty;
            }
        }

        private void Pivot7_FieldValueDisplayText(object sender, DevExpress.Xpf.PivotGrid.PivotFieldDisplayTextEventArgs e)
        {
            if ((e.DisplayText.StartsWith("Средний размер выплат", StringComparison.CurrentCultureIgnoreCase) ||
                 e.DisplayText.StartsWith("Выплаты правоприемникам застрахованных лиц",
                     StringComparison.CurrentCultureIgnoreCase)) &&
                e.DisplayText.EndsWith("всего", StringComparison.CurrentCultureIgnoreCase))
            {
                e.DisplayText = e.DisplayText.Replace(" всего", "");

                if (!indexesForRemove.Any(x => e.FieldIndex == x))
                {
                    indexesForRemove.Add(e.FieldIndex);
                }
            }
        }

        private void AnalyzeInsuredpersonPivotView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.OnDataRefreshing += VM_OnDataRefreshing;
            
        }

        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot7.ReloadData();
            pivot7.UpdateLayout();
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            AnalyzeReportHelper.SaveReport(pivot7, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitle);
        }
    }
    public class CellTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            Window mainWindow = App.mainWindow;

            CellsAreaItem viewField = item as CellsAreaItem;

            if (viewField.Value == null || viewField.Value != null && viewField.RowValue == null)
                return mainWindow.FindResource("DefaultCellTemplate") as DataTemplate;

            if (viewField.RowValue.ToString().Contains("шт."))
                return mainWindow.FindResource("Dec2IntCellTemplate") as DataTemplate;

            return mainWindow.FindResource("DefaultCellTemplate") as DataTemplate;

        }
    }
}
