﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzePensionPlacementReportListView.xaml
    /// </summary>
    public partial class AnalyzeYieldfundsListView : UserControl
    {
        public AnalyzeYieldfundsListViewModel VM => (AnalyzeYieldfundsListViewModel)DataContext;
        private RibbonStateBL _ribbonState;//для корректного отображения полей ввода параметров, если позже был выбран другой раздел
        public DelegateCommand OpenEditorCommand { get; private set; }
        public AnalyzeYieldfundsListView()
        {
            InitializeComponent();
            _ribbonState = App.RibbonManager.GetCurrentPageRibbonState();
            OpenEditorCommand = new DelegateCommand(x => VM != null && VM.Current != null, x => OpenEditorAction());

            this.SizeChanged += (sender, args) =>
            {
                gridReports.Height = args.NewSize.Height - (headerRow.ActualHeight + 20);
            };
            
            //gridReports.GroupRowExpanded += (sender, args) =>
            //{
            //    gridReports.Height = _gridHeight;
            //    //gridReports.Height = gridReports.ActualHeight - headerRow.ActualHeight + 10;
            //};
            //gridReports.GroupRowExpanding += (sender, args) =>
            //{
            //    _gridHeight = gridReports.Height;
            //    //gridReports.Height = gridReports.ActualHeight - headerRow.ActualHeight + 10;
            //};
            Loaded += AnalyzeYieldfundsListView_Loaded;
        }

        private double _gridHeight;
        private void AnalyzeYieldfundsListView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null && !VM.IsQuarkReport)
            {
                quarkColumn.GroupIndex = -1;
                quarkColumn.Visible = false;
            }
        }

        void OpenEditorAction()
        {
            if (VM.Current == null)
                return;
            var state = !VM.IsArchiveMode ? ViewModelState.Edit : ViewModelState.Read;
            var vm = new AnalyzeYieldfundsEditViewModel(VM.Current, _ribbonState,state, false, VM.IsArchiveMode);
            var view = new AnalyzeYieldfundsEditView(vm);
            App.DashboardManager.OpenNewTab("Детализация отчета", view, state, vm);
        }

        private void grid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (tableView.GetRowHandleByMouseEventArgs(e) == DataControlBase.InvalidRowHandle) return;
            OpenEditorCommand.Execute(null);
        }

    }
}
