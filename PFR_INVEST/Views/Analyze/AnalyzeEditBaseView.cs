﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.Views.Analyze
{
    public abstract class AnalyzeEditBaseView<T> : UserControl where T : YearReportBase
    {
        private AnalyzeEditorDialogBase<T> VM => (AnalyzeEditorDialogBase<T>)DataContext;

        protected AnalyzeEditBaseView(AnalyzeEditorDialogBase<T> vm)
        {
            DataContext = vm;
            Loaded += OnLoaded;
            VM.RequestClose += VM_RequestClose ;
        }

        private void OnLoaded(object sender, EventArgs e)
        {
            if (VM.Years.Count > 0)
                return;
            Visibility = Visibility.Hidden;
            DXMessageBox.Show(@"Все отчеты на доступные периоды уже созданы!", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            VM_RequestClose(null, EventArgs.Empty);
        }
        protected abstract void VM_RequestClose(object sender, EventArgs e);
       
    }
}
