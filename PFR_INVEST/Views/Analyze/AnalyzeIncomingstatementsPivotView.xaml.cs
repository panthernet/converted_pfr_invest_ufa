﻿using System;
using System.Collections;
using System.Windows;
using DevExpress.Xpf.PivotGrid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views.Analyze
{
    /// <summary>
    /// Interaction logic for AnalyzePensionplacementPivotView.xaml
    /// </summary>
    public partial class AnalyzeIncomingstatementsPivotView : UserControl
    {
        AnalyzeIncomingstatementsPivotViewModel VM
        {
            get
            {
                return DataContext as AnalyzeIncomingstatementsPivotViewModel;
            }
        }

        public AnalyzeIncomingstatementsPivotView()
        {
            InitializeComponent();
            Loaded += AnalyzeIncomingstatementsPivotView_Loaded;
            pivot4.AllowExpandOnDoubleClick = false;
            pivot4.FieldValueDisplayText += Pivot4_FieldValueDisplayText;
            pivot4.FieldValueExpanding += Pivot4_FieldValueExpanding;
            pivot4.CustomFieldSort += Pivot4_CustomFieldSort;
        }

        private void Pivot4_CustomFieldSort(object sender, PivotCustomFieldSortEventArgs e)
        {
            if (e.Field.FieldName == "StatementName")
            {
                object orderValue1 = e.GetListSourceColumnValue(e.ListSourceRowIndex1, "StatementOrderId"),
                   orderValue2 = e.GetListSourceColumnValue(e.ListSourceRowIndex2, "StatementOrderId");
                e.Result = Comparer.Default.Compare(orderValue1, orderValue2);
                e.Handled = true;
            }
            else if(e.Field.FieldName == "FilingName")
            {
                    object orderValue1 = e.GetListSourceColumnValue(e.ListSourceRowIndex1, "FilingOrderId"),
                        orderValue2 = e.GetListSourceColumnValue(e.ListSourceRowIndex2, "FilingOrderId");
                    e.Result = Comparer.Default.Compare(orderValue1, orderValue2);
                e.Handled = true;

            }
            else if (e.Field.FieldName == "ChangeTypeName")
            {
                object orderValue1 = e.GetListSourceColumnValue(e.ListSourceRowIndex1, "Changetype"),
                   orderValue2 = e.GetListSourceColumnValue(e.ListSourceRowIndex2, "Changetype");
                e.Result = Comparer.Default.Compare(orderValue1, orderValue2);
                e.Handled = true;
            }
           

        }

        private void Pivot4_FieldValueExpanding(object sender, PivotFieldValueCancelEventArgs e)
        {
            if (e.Field.FieldName == "FilingName" && e.Value.ToString() != "Территориальными органами ПФР")
            {
                e.Cancel = true;
            }
        }

        private void Pivot4_FieldValueDisplayText(object sender, PivotFieldDisplayTextEventArgs e)
        {
            if (e.ValueType == FieldValueType.GrandTotal && e.DisplayText == "Итого" && !e.IsColumn)
                e.DisplayText = "Всего по Российской Федерации";

            if (e.ValueType == FieldValueType.GrandTotal && e.DisplayText == "Итого" && e.IsColumn)
                e.DisplayText = "Количество принятых заявлений с начала года";
        }

        private void AnalyzeIncomingstatementsPivotView_Loaded(object sender, RoutedEventArgs e)
        {
            if (VM != null)
                VM.OnDataRefreshing += VM_OnDataRefreshing;
        }

        private void VM_OnDataRefreshing(object sender, EventArgs e)
        {
            pivot4.ReloadData();
            //скрыть группы в строках
            pivot4.Fields["StatementName"].CollapseValue("Об отказе от 6%-ой накопительной пенсии");
            pivot4.Fields["StatementName"].CollapseValue("Об отзыве заявления об отказе");
            //pivot4.Fields["StatementName"].CollapseValue("О выборе инвестпортфеля (УК)");
            pivot4.Fields["StatementName"].CollapseValue("О выборе инвестпортфеля (УК)");
            pivot4.Fields["StatementName"].CollapseValue("О распределении средств пенсионных накоплений");
            //скрыть группы в колонках
            pivot4.Fields["FilingName"].CollapseValue("ЕПГУ");
            pivot4.Fields["FilingName"].CollapseValue("Личный кабинет");
            pivot4.Fields["FilingName"].CollapseValue("В электронном виде");
            pivot4.Fields["FilingName"].CollapseValue("МФЦ");
            pivot4.Fields["StatementName"].AllowExpand = false;
            pivot4.Fields["FilingName"].AllowExpand = true;
            
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            AnalyzeReportHelper.SaveReport(pivot4, VM, (DataTemplate)Resources["PageHeader"], VM.FormTitle);
        }
    }
}

