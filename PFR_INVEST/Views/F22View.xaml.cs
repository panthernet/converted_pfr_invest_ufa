﻿using System;
using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    [Obsolete("Устаревший, использовать F25View", false)]
    public partial class F22View : UserControl
    {
        public F22View()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
