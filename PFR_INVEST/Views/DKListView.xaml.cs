﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    using DataObjects;

	/// <summary>
	/// Interaction logic for CostsListView.xaml
	/// </summary>
	public partial class DKListView
	{
		public DKListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        public KDoc SelectedItem => Grid.CurrentItem as KDoc;

	    private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var item = Grid.SelectedItem as KDoc;
			if (item == null)
				return;
            switch ((KDoc.DocVersion)item.DocType) 
			{
				case KDoc.DocVersion.Migrated:
					ViewModelBase.DialogHelper.ShowAlert("Данные мигрированы. Форма не подлежит открытию");
					break;
				case KDoc.DocVersion.Version1:
					App.DashboardManager.OpenNewTab(typeof(TreasurityView), ViewModelState.Edit, item.ID, "Документ казначейства");
					break;
			    case KDoc.DocVersion.Version2:
					App.DashboardManager.OpenNewTab(typeof(Treasurity2View), ViewModelState.Edit, item.ID, "Документ казначейства");
					break;
                case KDoc.DocVersion.Version3:
                    App.DashboardManager.OpenNewTab(typeof(Treasurity3View), ViewModelState.Edit, item.ID, "Документ казначейства");
                    break;

                default: //Latest - v4 as of 10.04.2017
                    App.DashboardManager.OpenNewTab(typeof(Treasurity4View), ViewModelState.Edit, item.ID, "Документ казначейства");
                    break;
			}
			
		}
	}
}
