﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIView.xaml
    /// </summary>
    public partial class SIVRRegisterView : UserControl
    {
        public SIVRRegisterView()
        {
            InitializeComponent();
            Loaded += SIVRRegisterView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        void SIVRRegisterView_Loaded(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is SIVRRegisterViewModel)) return;
            cbMonths.Visibility = ((SIVRRegisterViewModel) DataContext).SelectedMonth == null
                ? Visibility.Hidden
                : Visibility.Visible;
        }


        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            long id = GetSelectedTransferListID();
            if (id > 0)
            {
                if (DataContext is SIVRRegisterSIViewModel)
                {
                    App.DashboardManager.OpenNewTab(typeof(TransferSIView), ViewModelState.Edit, id, "Список перечислений СИ");
                }
                else
                {
                    App.DashboardManager.OpenNewTab(typeof(TransferVRView), ViewModelState.Edit, id, "Список перечислений ВР");
                }

            }
        }

        public long GetSelectedTransferListID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }


        private void CbDirections_OnSelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            if (cbDirections.SelectedItem != null &&
                (cbDirections.SelectedItem as SPNDirection).isFromUKToPFR.Value)
            {
                investDohod.Visibility = Visibility.Visible;
                cInvestmentIncome.Visible = true;
            }
            else
            {
                investDohod.Visibility = Visibility.Collapsed;
                cInvestmentIncome.Visible = false;
            }
        }
    }
}
