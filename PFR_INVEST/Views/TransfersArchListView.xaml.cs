﻿using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	public partial class TransfersArchListView
	{
	    private bool _mGridRowMouseDoubleClicked = true;

		private bool IsModelSI => DataContext is TransfersSIArchListViewModel;

	    public TransfersArchListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

		private void Grid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
		{
			const string dispText = "{0} \tдокумент: {1}     \r\n\t№ {2} от: {3}";

			if (!Grid.IsValidRowHandle(e.RowHandle))
				return;

			if (e.Column.FieldName != "RegisterID")
				return;

			var currRow = e.Row as TransferListItem;

			if (currRow != null)
			{
				e.DisplayText = string.Format(dispText,
					currRow.RegisterID,
					currRow.Kind,
					currRow.RegisterNumber,
					currRow.RegisterDate?.ToShortDateString() ?? string.Empty
				);
			}
			else
				e.DisplayText = string.Empty;
		}

		public bool IsRegisterSelected()
		{
			var group = Grid.GetFocusedGroupFieldName();
			var detailGroup = Grid.GetFocusedDetailGroupFieldName();
			if (detailGroup != null)
				return false;

			return group == "RegisterID" || group == null;
		}

		public long? GetSelectedRegisterID()
		{
			var group = Grid.GetFocusedGroupFieldName();
			var detailGroup = Grid.GetFocusedDetailGroupFieldName();
			if (detailGroup != null)
				return null;
			var item = Grid.GetFocusedItem<TransferListItem>();

		    return (group == "RegisterID" || group == null) && item != null ? (long?) item.RegisterID : null;
		}

		public bool IsTransferSelected()
		{
		    var group = Grid.GetFocusedDetailGroupFieldName();
		    return group == "ContractNumber";
		}

	    public long? GetSelectedTransferID()
		{
			var group = Grid.GetFocusedDetailGroupFieldName();
			var item = Grid.GetFocusedItem<TransferListItem>();

	        return group == "ContractNumber" && item != null ? (long?) item.TransferID : null;
		}

		private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
		{
			e.Allow = !_mGridRowMouseDoubleClicked;
			_mGridRowMouseDoubleClicked = false;
		}

		private void tableView_RowDoubleClick(object sender, RowDoubleClickEventArgs e)
		{
			var item = Grid.GetFocusedItem<TransferListItem>();
			if (item == null)
				return;

			var group = Grid.GetFocusedGroupFieldName();
		    if (group != "RegisterID" && group != null)
                return;
		    App.DashboardManager.OpenNewTab(IsModelSI ? typeof(SIVRRegisterSIView) : typeof(SIVRRegisterVRView), ViewModelState.Edit, item.RegisterID, "Реестр перечислений " + (IsModelSI ? "СИ" : "ВР"));
		    _mGridRowMouseDoubleClicked = true;
		}

		private void tableViewDetail_RowDoubleClick(object sender, RowDoubleClickEventArgs e)
		{
			var item = Grid.GetFocusedDetailItem<TransferListItem>();
			if (item == null)
				return;

			var group = Grid.GetFocusedDetailGroupFieldName();
			if (group == "ContractNumber")
			{
				App.DashboardManager.OpenNewTab(typeof(TransferView), ViewModelState.Edit, item.TransferID, "Список перечислений");
				_mGridRowMouseDoubleClicked = true;
			}
			else if (group == null)
			{
				App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Edit, item.ReqTransferID, "Перечисление");
				_mGridRowMouseDoubleClicked = true;
			}

		}
	}
}
