﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for MarketPricesListView.xaml
    /// </summary>
    public partial class MarketPricesListView
    {
        public MarketPricesListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null || Grid.View.FocusedRowHandle < 0) return;
            long selectedID = GetSelectedID();
            App.DashboardManager.OpenNewTab(typeof(MarketPriceView), ViewModelState.Edit, selectedID, "Рыночная цена");
        }

        public long GetSelectedID()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return (long)Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");

            return 0;
        }

    }
}
