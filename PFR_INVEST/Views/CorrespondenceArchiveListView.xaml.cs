﻿using System.Windows.Input;
using PFR_INVEST.DataObjects.ListItems;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIArchiveView.xaml
    /// </summary>
    public abstract partial class CorrespondenceArchiveListView
    {
        protected CorrespondenceArchiveListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        protected abstract void OpenItem(CorrespondenceListItemNew item);

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = Grid.CurrentItem as CorrespondenceListItemNew;
            if (item != null)
                OpenItem(item);        
        }

        private void Grid_CustomColumnSort(object sender, DevExpress.Xpf.Grid.CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
