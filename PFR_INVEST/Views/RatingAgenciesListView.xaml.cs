﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace PFR_INVEST.Views
{
    using BusinessLogic;

    /// <summary>
    /// Interaction logic for RatingAgenciesList.xaml
    /// </summary>
    public partial class RatingAgenciesListView : UserControl
    {
        public RatingAgenciesListView()
        {
            InitializeComponent();
        }

        public long GetSelectedRateID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        public bool IsRateSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (IsRateSelected())
            {
                long lID = GetSelectedRateID();
                if (lID > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(RatingAgencyView), ViewModelState.Edit, lID, "Рейтинговое агентство");
                }
                   
            }
        }
    }
}
