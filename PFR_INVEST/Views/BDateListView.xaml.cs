﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for INDateListView.xaml
    /// </summary>
    public partial class BDateListView
    {
        public BDateListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, BDateGrid);
        }
    }
}
