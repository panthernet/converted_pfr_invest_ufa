﻿using System;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SellReportsListView.xaml
    /// </summary>
    public partial class SaleReportsListView
    {
        public SaleReportsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ReportsGrid);
        }

        public bool IsReportSelected()
        {
            return ReportsGrid.View.FocusedRowData.Level >= ReportsGrid.GetGroupedColumns().Count && ReportsGrid.SelectedItem != null;
        }

        public long GetSelectedReportID()
        {
            return Convert.ToInt64(ReportsGrid.GetCellValue(ReportsGrid.View.FocusedRowHandle, "ID"));
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ReportsGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsReportSelected())
                return;
            var lID = GetSelectedReportID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Edit, lID, OrderReportsHelper.SELLING_REPORT);
        }
    }
}
