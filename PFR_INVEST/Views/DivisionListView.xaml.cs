﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DivisionListView.xaml
    /// </summary>
    public partial class DivisionListView : UserControl
    {
        public DivisionListView()
        {
            InitializeComponent();
        }

        public long GetSelectedRateID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        public bool IsRateSelected()
        {
            return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (IsRateSelected())
            {
                long lID = GetSelectedRateID();
                if (lID > 0)
                {
                    App.DashboardManager.OpenNewTab(typeof(DivisionView), ViewModelState.Edit, lID, "Подразделение");
                }
            }
        }
        
    }
}
