﻿using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    public partial class ReqTransferView
    {
        public ReqTransferView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void gridCommonPP_CustomUnboundColumnData(object sender, DevExpress.Xpf.Grid.GridColumnDataEventArgs e)
		{
			if (e.Column.FieldName == "Num")
				e.Value = e.ListSourceRowIndex + 1;
		}
    }
}
