﻿using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UserStatisticChose1.xaml
    /// </summary>
    public partial class UserStatisticChoose : UserControl
    {
        public UserStatisticChoose()
        {
            InitializeComponent();
        }

        public UserStatisticChooseViewModel Model => DataContext as UserStatisticChooseViewModel;

        public Audit GetSelectedListItem()
        {
            return Grid.GetFocusedRow() as Audit;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Model?.RefreshEntryList();
        }
    }
}
