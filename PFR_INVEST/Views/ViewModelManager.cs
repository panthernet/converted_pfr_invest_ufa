﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
	public class ViewModelManager : IViewModelManager
	{
		public void RefreshCardViewModel(Type type, long cardID)
		{
			var list = App.DashboardManager.FindViewModelsList(type);
			list.Where(vm => vm is IRefreshableCardViewModel).Cast<IRefreshableCardViewModel>().ToList()
				.ForEach(vm =>
				{
					if (vm.CardID == cardID)
						vm.RefreshModel();
				});
		}

		public void RefreshViewModels(params Type[] modelTypes)
		{
			if (modelTypes != null && modelTypes.Any())
				App.DashboardManager.RefreshListViewModels(modelTypes);
		}

		public void UpdateDataInAllModels(Type type, long id)
		{
			UpdateDataInAllModels(new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(type, id) });
		}

		public void UpdateDataInAllModels(IList<KeyValuePair<Type, long>> updatedData)
		{
			var list = App.DashboardManager.FindViewModelsList<IUpdateListenerModel>();

			foreach (var data in updatedData)
			{
				OnDataUpdate(data);
				list.Where(m => m.UpdateListenTypes.Contains(data.Key)).ToList().ForEach(m => m.OnDataUpdate(data.Key, data.Value));
			}
		}

		/// <summary>
		/// Дополнительные специфические действия клиента, которые нельзя задать через нормальное поведение
		/// TODO: вынести в подписное событие
		/// </summary>
		/// <param name="data"></param>
		private void OnDataUpdate(KeyValuePair<Type, long> data)
		{
			if (data.Key == typeof(ROPSSum)) 
			{
				var msg = DataContainerFacade.GetClient().GetROPSLimitMessage(true, true);
				App.StatusbarManager.SetErrorMessage(msg);
			}
		}
	}
}
