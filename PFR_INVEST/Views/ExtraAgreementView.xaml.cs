﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ExtraAgreementView.xaml
    /// </summary>
    public partial class ExtraAgreementView : UserControl
    {
        public ExtraAgreementView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
