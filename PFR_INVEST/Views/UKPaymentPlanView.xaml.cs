﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UKPaymentPlanView.xaml
    /// </summary>
    public partial class UKPaymentPlanView : UserControl
    {
        private UKPaymentPlanViewModel Model => DataContext as UKPaymentPlanViewModel;

        public UKPaymentPlanView()
        {
            InitializeComponent();
            Loaded += UKPaymentPlanView_Loaded;
        }

        private void UKPaymentPlanView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Model.OnNeedReloadTransferDataDelegate += Model_OnNeedReloadTransferDataDelegate;
        }

        private void Model_OnNeedReloadTransferDataDelegate()
        {
            Grid.RefreshData();
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (IsSavedTransferSelected())
            {
                ViewModelBase.DialogHelper.SelectReqTransferFromAssPay(GetSavedSelectedUkPlanID());
            }
        }

        private void TableView_ShowingEditor(object sender, DevExpress.Xpf.Grid.ShowingEditorEventArgs e)
        {
            e.Cancel = IsSavedTransferSelected();
        }

        private bool IsSavedTransferSelected()
        {
            return GetSavedSelectedUkPlanID() > 0;
        }

        private long GetSavedSelectedUkPlanID()
        {
            return Convert.ToInt64(Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID"));
        }

        private void Grid_CustomColumnSort(object sender, DevExpress.Xpf.Grid.CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void TableView_HiddenEditor(object sender, DevExpress.Xpf.Grid.EditorEventArgs e)
        {
            Model.RecalcSums();
        }

		private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
		{
			ComponentHelper.OnColumnFilterOptionsSort(sender, e);
		}
	}
}
