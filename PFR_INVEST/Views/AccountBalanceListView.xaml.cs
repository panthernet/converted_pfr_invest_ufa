﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using DevExpress.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Helpers;
using PFR_INVEST.Proxy.DataContracts;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for BalanceView.xaml
	/// </summary>
	public partial class AccountBalanceListView
	{
		public AccountBalanceListView()
		{
			InitializeComponent();
			ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
		}

	    private static void OpenForm<T>(DB2BalanceListItem item)
		{
			var operation = item.KIND.Split('/')[0].Trim();
			App.DashboardManager.OpenNewTab(typeof(T),
											ViewModelState.Read, item.ID, operation);
		}

	    private static void OpenForm<T>(DB2BalanceListItem item, ViewModelState action)
		{
			var operation = item.KIND.Split('/')[0].Trim();

			App.DashboardManager.OpenNewTab(typeof(T),
											action, item.ID, operation);
		}

	    private static void OpenForm<T>(DB2BalanceListItem item, ViewModelState action, object param)
		{
			var operation = item.KIND.Split('/')[0].Trim();

			App.DashboardManager.OpenNewTab(typeof(T), action, item.ID, param, operation);
		}

	    private bool IsDocSelected()
		{
			return Grid.View.FocusedRowData.Level >= Grid.GetGroupedColumns().Count && Grid.SelectedItem != null;            
		}

	    private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;

			if (!IsDocSelected())
				return;

			var item = Grid.CurrentItem as BalanceListItem;

			if (item == null)
				return;

			if (item.ForeignKey < 0)
			{
				MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
				return;
			}

			var part = (SaldoParts)item.Ord;
			bool isHandled = true;

			// Новая система привязки по типу записи, а не по тексту
			switch (part)
			{
				case SaldoParts.Req_Transfer:
				case SaldoParts.Asg_Fin_Tr:
					OpenForm<CommonPPView>(item, ViewModelState.Read);
					break;
				case SaldoParts.Cost:
					OpenForm<CostsView>(item, ViewModelState.Read);
					break;
				default:
					isHandled = false;
					break;
			}

			// старая система привязки формы по тексту содержания
			if (!isHandled)
			{
				string operation = item.KIND.Split('/')[0].Trim();

				switch (operation)
				{
					#region accurate quarter
					case DueDocKindIdentifier.AccurateQuarter:
					case DueDocKindIdentifier.DueDSVAccurateYear:
						App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Read,
															item.ID, (object) operation, operation);
						break;

					case DueDocKindIdentifier.PenaltyAccurateQuarter:
						App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Read,
															item.ID, operation);
						break;

					#endregion

					#region due
					case DueDocKindIdentifier.Due:
						App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						break;
					case DueDocKindIdentifier.DueDSV:
						App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;
					case DueDocKindIdentifier.Penalty:
						OpenForm<PenaltyView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.Prepayment:
						//OpenForm<PrepaymentView>(item, ViewModelState.Edit);
						App.DashboardManager.OpenNewTab(typeof(PrepaymentView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						break;

					case DueDocKindIdentifier.PrepaymentDSV:
						App.DashboardManager.OpenNewTab(typeof(PrepaymentView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;

					case DueDocKindIdentifier.DueDead:
						App.DashboardManager.OpenNewTab(typeof(DueDeadView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//                    OpenForm<DueDeadView>(item);
						break;

					case DueDocKindIdentifier.DueDeadDSV:
						App.DashboardManager.OpenNewTab(typeof(DueDeadView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						break;

					case DueDocKindIdentifier.DueExcess:
						App.DashboardManager.OpenNewTab(typeof(DueExcessView), ViewModelState.Read,
														  item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//                  OpenForm<DueExcessView>(item);
						break;

					case DueDocKindIdentifier.DueExcessDSV:
						App.DashboardManager.OpenNewTab(typeof(DueExcessView), ViewModelState.Read,
														  item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						//                  OpenForm<DueExcessView>(item);
						break;

					case DueDocKindIdentifier.DueUndistributed:
						App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.SPN, operation);
						//OpenForm<DueUndistributedView>(item);
						break;

					case DueDocKindIdentifier.DueUndistributedDSV:
						App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Read,
															item.ID, PortfolioIdentifier.PortfolioTypes.DSV, operation);
						//OpenForm<DueUndistributedView>(item);
						break;

					case DueDocKindIdentifier.PrepaymentAccurate:
					case DueDocKindIdentifier.PrepaymentAccurateDSV:
						OpenForm<PrepaymentAccurate>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.PenaltyAccurate:
						OpenForm<PrecisePenaltyView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueAccurate:
					case DueDocKindIdentifier.DueDSVAccurate:
						OpenForm<MonthAccurateView>(item, ViewModelState.Read, operation);
						break;

					case DueDocKindIdentifier.DueDeadAccurate:
					case DueDocKindIdentifier.DueDeadAccurateDSV:
						OpenForm<DueDeadAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueExcessAccurate:
					case DueDocKindIdentifier.DueExcessAccurateDSV:
						OpenForm<DueExcessAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.DueUndistributedAccurate:
					case DueDocKindIdentifier.DueUndistributedAccurateDSV:
						OpenForm<DueUndistributedAccurateView>(item, ViewModelState.Read);
						break;

					case DueDocKindIdentifier.Treasurity:
					case DueDocKindIdentifier.PenaltyTreasurity:
						OpenForm<TreasurityView>(item, ViewModelState.Read);
						break;
					#endregion

					#region accoperations;
					case AccDocKindIdentifier.EnrollmentPercents:
						OpenForm<EnrollmentPercents>(item, ViewModelState.Edit);
						break;
					case AccDocKindIdentifier.EnrollmentOther:
						OpenForm<EnrollmentOtherView>(item, ViewModelState.Edit);
						break;
					case AccDocKindIdentifier.Transfer:
						OpenForm<TransferToAccount>(item, ViewModelState.Edit);
						break;
					case AccDocKindIdentifier.Withdrawal:
						OpenForm<WithdrawalView>(item, ViewModelState.Edit);
						break;
					#endregion

					#region CB & commissions
					case BalanceIdentifier.s_BuyingCB:
						App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Read, item.ID, OrderReportsHelper.BUYING_REPORT);
						break;
					case BalanceIdentifier.s_SellingCB:
						App.DashboardManager.OpenNewTab(typeof(OrderReportView), ViewModelState.Read, item.ID, OrderReportsHelper.SELLING_REPORT);
						break;
					case CostIdentifier.s_CBFee:
					case CostIdentifier.s_TradingCommission:
					case CostIdentifier.s_DepositServicesCommission:
					case CostIdentifier.s_CurrencyBuySellCommission:
					case CostIdentifier.s_DirectDebiting:
						OpenForm<CostsView>(item, ViewModelState.Read);
						break;
					case IncomeSecIdentifier.s_CouponYield:
					case IncomeSecIdentifier.s_SecurityRedemption:
						OpenForm<IncomeSecurityView>(item, ViewModelState.Read);
						break;
					case IncomeSecIdentifier.s_DepositsPercents:
						if (item.ForeignKey == 0)//PaymentHistory
							OpenForm<PaymentHistoryView>(item, ViewModelState.Read);
						else
							OpenForm<IncomeSecurityView>(item, ViewModelState.Read);
						break;
					#endregion CB

					#region UK transfer

					case BalanceIdentifier.s_TransferFromPFRToUK:
					case BalanceIdentifier.s_ReturnFromUKToPFR:
					case BalanceIdentifier.s_TransferFromPFRToUK_VR:
					case BalanceIdentifier.s_ReturnFromUKToPFR_VR:
						OpenForm<CommonPPView>(item, ViewModelState.Read);
						break;

					#endregion

					#region Schils

					case BalanceIdentifier.s_TransferCostsILS:
						OpenForm<TransferSchilsView>(item, ViewModelState.Read);
						break;
					case BalanceIdentifier.s_ReturnCostsILS:
						OpenForm<ReturnView>(item, ViewModelState.Read);
						break;

					#endregion

					#region deposit

					case BalanceIdentifier.s_DepositPlacement:
					case BalanceIdentifier.s_DepositReturn:
						//case BalanceIdentifier.s_DepositPercent:
						if (item.ForeignKey > 0)
							OpenForm<DepositView>(item);
						else
							MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
						break;

					#endregion

					default:
						string lowerKind = operation.ToLower();

						#region Register

						if (lowerKind.Contains(RegisterIdentifier.NPFtoPFR.ToLower()) || lowerKind.Contains(RegisterIdentifier.PFRtoNPF.ToLower()))
						{
							OpenForm<CommonPPView>(item, ViewModelState.Read);

						}

						#endregion

						break;
				}
			}

			var model = App.DashboardManager.GetActiveViewModel() as ViewModelCard;
			if (model != null)
			{
				model.IsDataChanged = true; // маскируем для CanExecuteSave чтобы узнать результат Validate
				model.IsDataChanged = !model.CanExecuteSave(); // если есть невалидные поля при закрытии отображаем диалог
			}

		}

		private void Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
		{
			if (e.Value1 == null)
			{
				if (e.Value2 == null)
					//оба null
					e.Result = 0;
				else
					//Value1 null Value2 не null - Value2 > Value1
					e.Result = -1;
			}
			else
			{
				if (e.Value2 == null)
					//Value1 не null Value2 - Value1 > Value2
					e.Result = 1;
				else
				{
					// оба не null - сравниваем как месяцы
					List<string> months = new List<string>(DateTools.Months);
					int idx1 = months.IndexOf(e.Value1.ToString().Trim());
					int idx2 = months.IndexOf(e.Value2.ToString().Trim());
					e.Result = Comparer<int>.Default.Compare(idx1, idx2);
				}
			}
			e.Handled = true;
		}

	    private readonly Dictionary<int, decimal> _sumgroup = new Dictionary<int, decimal>();
		private bool _bStart;

		private void Grid_CustomSummary(object sender, CustomSummaryEventArgs e)
		{
			if (e.GroupLevel == 0 && !_sumgroup.ContainsKey(e.GroupLevel))
				_sumgroup.Add(e.GroupLevel, 0);

			var row = Grid.GetRow(e.RowHandle) as BalanceListItem;

			if (row == null)
				return;

			switch (e.SummaryProcess.ToString().ToLower())
			{
				case "start":
					var item = e.Item as GridSummaryItem;

					if (item != null)
					{
						if (item.FieldName.Equals("Start"))
							//e.TotalValue = row.Start;
							_bStart = true;
						if (item.FieldName.Equals("Minus"))
							_sumgroup[1] = 0;
						if (item.FieldName.Equals("Plus"))
							_sumgroup[2] = 0;

						if (e.GroupLevel == 0 && item.FieldName.Equals("Finish"))
							_sumgroup[0] = 0;
					}

					break;
				case "calculate":
					item = e.Item as GridSummaryItem;
					if (e.GroupLevel == 0 && item != null && item.FieldName.Equals("Finish"))
						_sumgroup[0] += row.Plus + row.Minus;//(decimal)e.FieldValue;

					if (_bStart)
					{
						_bStart = false;
						e.TotalValue = row.Start;
					}
					if (item != null && item.FieldName.Equals("Minus"))
						_sumgroup[1] += (decimal)e.FieldValue;

					if (item != null && item.FieldName.Equals("Plus"))
						_sumgroup[2] += (decimal)e.FieldValue;

					break;
				case "finalize":
					item = e.Item as GridSummaryItem;

					if (item != null && item.FieldName.Equals("Finish"))
					{
						e.TotalValue = row.Finish;

						//                        if (e.GroupLevel == 0)
						if (Grid.GetGroupedColumns()[e.GroupLevel].FieldName == "PORTFOLIO")
							e.TotalValue = String.Empty;
						//                            e.TotalValue = sumgroup[0];//e.FieldValue;
					}
					if (item != null && (item.FieldName.Equals("Start")))
					{
						e.TotalValue = (decimal)e.FieldValue;

						if (Grid.GetGroupedColumns()[e.GroupLevel].FieldName == "PORTFOLIO")
							e.TotalValue = String.Empty;
					}
					if (item != null && (item.FieldName.Equals("Minus")))
					{
						e.TotalValue = _sumgroup[1];

						if (Grid.GetGroupedColumns()[e.GroupLevel].FieldName == "PORTFOLIO")
							e.TotalValue = String.Empty;
					}
					if (item != null && (item.FieldName.Equals("Plus")))
					{
						e.TotalValue = _sumgroup[2];

						if (Grid.GetGroupedColumns()[e.GroupLevel].FieldName == "PORTFOLIO")
							e.TotalValue = String.Empty;
					}
					break;
			}
		}


	}
	public class RedBgColorConverter : MarkupExtension, IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return ((bool)value) ? Brushes.LightPink : Brushes.White;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion

		public override object ProvideValue(System.IServiceProvider serviceProvider)
		{
			return this;
		}
	}
	public class GreenBgColorConverter : MarkupExtension, IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return ((bool)value) ? Brushes.LightGreen : Brushes.White;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion

		public override object ProvideValue(System.IServiceProvider serviceProvider)
		{
			return this;
		}
	}

	public class OperationTypeToColorConverter : MarkupExtension, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (string.IsNullOrEmpty(value as string))
				return Brushes.Black;
			var operation = ((string)value).Split('/')[0].Trim();
			switch (operation)
			{
				case DueDocKindIdentifier.DueDSV:
				case DueDocKindIdentifier.Due:
				case DueDocKindIdentifier.Penalty:
				case DueDocKindIdentifier.Prepayment:
				case DueDocKindIdentifier.PrepaymentDSV:
				case DueDocKindIdentifier.DueDead:
				case DueDocKindIdentifier.DueDeadDSV:
				case DueDocKindIdentifier.DueExcess:
				case DueDocKindIdentifier.DueExcessDSV:
				case DueDocKindIdentifier.DueUndistributed:
				case DueDocKindIdentifier.DueUndistributedDSV:
				case DueDocKindIdentifier.PrepaymentAccurate:
				case DueDocKindIdentifier.PrepaymentAccurateDSV:
				case DueDocKindIdentifier.PenaltyAccurate:
				case DueDocKindIdentifier.DueAccurate:
				case DueDocKindIdentifier.DueDSVAccurate:
				case DueDocKindIdentifier.DueDeadAccurate:
				case DueDocKindIdentifier.DueDeadAccurateDSV:
				case DueDocKindIdentifier.DueExcessAccurate:
				case DueDocKindIdentifier.DueExcessAccurateDSV:
				case DueDocKindIdentifier.DueUndistributedAccurate:
				case DueDocKindIdentifier.DueUndistributedAccurateDSV:
				case DueDocKindIdentifier.Treasurity:
				case DueDocKindIdentifier.PenaltyTreasurity:
				case AccDocKindIdentifier.EnrollmentPercents:
				case AccDocKindIdentifier.EnrollmentOther:
				case AccDocKindIdentifier.Transfer:
				case AccDocKindIdentifier.Withdrawal:
				case CostIdentifier.s_CBFee:
				case CostIdentifier.s_TradingCommission:
				case CostIdentifier.s_DepositServicesCommission:
				case CostIdentifier.s_CurrencyBuySellCommission:
				case CostIdentifier.s_DirectDebiting:
				case IncomeSecIdentifier.s_CouponYield:
				case IncomeSecIdentifier.s_SecurityRedemption:
				case IncomeSecIdentifier.s_DepositsPercents:
				case BalanceIdentifier.s_TransferCostsILS:
				case BalanceIdentifier.s_ReturnCostsILS:
					return Brushes.Black;
				case BalanceIdentifier.s_BuyingCB:
				case BalanceIdentifier.s_SellingCB:
				case BalanceIdentifier.s_DepositPlacement:
				case BalanceIdentifier.s_DepositReturn:
					//case BalanceIdentifier.s_DepositPercent:
					return Brushes.DarkGreen;
				case BalanceIdentifier.s_TransferFromPFRToUK:
				case BalanceIdentifier.s_ReturnFromUKToPFR:
					return Brushes.Chocolate;
				default:
					if (operation.ToLower().Contains(RegisterIdentifier.NPFtoPFR.ToLower()) || operation.ToLower().Contains(RegisterIdentifier.PFRtoNPF.ToLower()))
						return Brushes.Blue;
					break;
			}
			return Brushes.Black;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(System.IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
