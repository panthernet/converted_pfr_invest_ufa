﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    internal class GridRefreshHelper
    {
        public GridControl Grid { get; private set; }
        public UserControl Parent { get; private set; }

        public GridRefreshHelper(UserControl parent, GridControl grid)
        {
            if (grid == null)
                throw new ArgumentNullException(nameof(grid));
            if (parent == null)
                throw new ArgumentNullException(nameof(parent));
            Grid = grid;
            Parent = parent;

            Parent.Unloaded += Parent_Unloaded;
            Parent.DataContextChanged += Parent_DataContextChanged;
            Grid.TargetUpdated += Grid_TargetUpdated;
        }

        private void Parent_Unloaded(object sender, RoutedEventArgs e)
        {
            //при нормальной отработке логики не вызовется, но в случае исключения может подстраховать
            OnClose();
        }

        private void Parent_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldModel = e.OldValue as ViewModelList;
            var newModel = e.NewValue as ViewModelList;
            if (oldModel != null)
                oldModel.OnDataRefreshing -= ViewModel_UpdateEvent;
	        if (newModel != null)
	        {
		        newModel.OnDataRefreshing += ViewModel_UpdateEvent;
	        }
        }

        /// <summary>
        /// Очистка (тут желательно удалять привязки на эвенты во избежании утечек памяти)
        /// </summary>
        public void OnClose()
        {
            if (LayoutStream != null)
                try
                {
                    LayoutStream.Close();
                    LayoutStream.Dispose();
                }
                finally
                {
                    LayoutStream = null;
                }
            if (GroupRowsData != null)
            {
                GroupRowsData.Clear();
                GroupRowsData = null;
            }

            if (Grid != null)
            {
                Grid.TargetUpdated -= Grid_TargetUpdated;
                Grid = null;
            }
            //Unbind control event
            if (Parent != null)
            {
                var model = Parent.DataContext as ViewModelList;
                if (model != null)
                    model.OnDataRefreshing -= ViewModel_UpdateEvent;
                Parent.DataContextChanged -= Parent_DataContextChanged;
                Parent.Unloaded -= Parent_Unloaded;
            }
            Parent = null;
        }

        #region Restore On Refresh
        /// <summary>
        /// Сохраненный поток лэйаута
        /// </summary>
        private MemoryStream LayoutStream { get; set; }
        /// <summary>
        /// Сххраненные данные о группировке
        /// </summary>
        private Dictionary<int, bool> GroupRowsData { get; set; }
        /// <summary>
        /// Сохраненный текущий выбранный элемент
        /// </summary>
        //private int FocusedRow { get; set; }
		/// <summary>
		/// Сохраненный выбранный элемент
		/// </summary>
	    private object SelectedRow { get; set; }

	    /// <summary>
        /// Сохранение данных при чтении данных из источника
        /// </summary>
	    private void ViewModel_UpdateEvent(object sender, EventArgs e)
        {
            if (Grid == null) return;
            if (LayoutStream != null)
            {
                try
                {
                    LayoutStream.Close();
                    LayoutStream.Dispose();
                }
                finally { LayoutStream = null; }
            }
            LayoutStream = new MemoryStream();
            Grid.SaveLayoutToStream(LayoutStream);
	        Debug.WriteLine($"FocusedRowHandle ViewModel_UpdateEvent: Grid.View.FocusedRowHandle [{Grid.View.FocusedRowHandle}] FocusedRow [{SelectedRow}]");
		    SelectedRow = Grid.SelectedItem;
			
			// FocusedRow = Grid.View.FocusedRowHandle;


            GroupRowsData = new Dictionary<int, bool>();
            if (Grid.VisibleRowCount > 0)
            {
                for (int i = 0; i < Grid.VisibleRowCount; i++)
                {
                    int rowHandle = Grid.GetRowHandleByVisibleIndex(i);
                    if (Grid.IsGroupRowHandle(rowHandle))
                        GroupRowsData.Add(rowHandle, Grid.IsGroupRowExpanded(rowHandle));
                }
            }

            Grid.RefreshData();
        }

        /// <summary>
        /// Восстановление данных после обновления таблицы
        /// </summary>
        private void Grid_TargetUpdated(object sender, DataTransferEventArgs dataTransferEventArgs)
        {
            if (Grid == null || !Grid.IsLoaded) return;

            if (LayoutStream == null) return;
            LayoutStream.Position = 0;
            Grid.RestoreLayoutFromStream(LayoutStream);

            if (Grid.GroupCount > 0)
            {
                //Баг в гриде Devexpress
                try
                {
                    Grid.CollapseAllGroups();
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            foreach (var obj in GroupRowsData.Keys)
            {
                if (GroupRowsData[obj])
                    Grid.ExpandGroupRow(obj, false);
            }

            Debug.WriteLine($"FocusedRowHandle Grid_TargetUpdated: Grid.View.FocusedRowHandle [{Grid.View.FocusedRowHandle}] FocusedRow [{SelectedRow}]");
            if (SelectedRow != null)
            {
                Grid.SelectedItem = SelectedRow;
            }
            // Grid.View.FocusedRowHandle = FocusedRow;

            GroupRowsData.Clear();
            LayoutStream.Close(); 
            LayoutStream.Dispose();
            LayoutStream = null;
        }
        #endregion
    }
}
