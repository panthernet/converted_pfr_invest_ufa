﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PrepaymentAccurate.xaml
    /// </summary>
    public partial class PrepaymentAccurate : UserControl
    {
        public PrepaymentAccurate()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }
    }
}
