﻿using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OpfrTransferView.xaml
    /// </summary>
    public partial class OpfrTransferView : UserControl
    {
        public OpfrTransferView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void gridControl_CustomUnboundColumnData(object sender, GridColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "Num")
                e.Value = e.ListSourceRowIndex + 1;
        }
    }
}
