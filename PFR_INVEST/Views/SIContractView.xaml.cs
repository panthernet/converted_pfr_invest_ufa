﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using DevExpress.Xpf.Docking;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SIContractView.xaml
    /// </summary>
    public partial class SIContractView : UserControl
    {
        private SIContractViewModel Model => DataContext as SIContractViewModel;

        public SIContractView()
        {
            InitializeComponent();
            Loaded += SIContractView_Loaded;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private bool _isLoaded;
        void SIContractView_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null && !_isLoaded)
            {
                _isLoaded = true;
                Model.OnNeedRefreshSupAgreementsList += Model_OnNeedRefreshSupAgreementsList;
                Model.OnCloseDeleteSupAgreementWindow += Model_OnCloseDeleteSupAgreementWindow;
                Model_OnNeedRefreshSupAgreementsList();

                string titleSuffix = (Model.ContractType == Document.Types.SI) ? " СИ" : (Model.ContractType == Document.Types.VR) ? " ВР" : string.Empty;
                LayoutPanel tabPanel = Parent as LayoutPanel;
                if (tabPanel != null && Model.State == ViewModelState.Edit)
                    tabPanel.Caption = "Договор" + titleSuffix;
            }
        }

        void Model_OnCloseDeleteSupAgreementWindow(long id)
        {
            var panels = App.DashboardManager.FindPanels<SupAgreementView>();
            var models = App.DashboardManager.FindViewModelsList<SupAgreementViewModel>();
            models.ForEach(
                model =>
                {
                    if (model.ID == id) model.CloseWindow();
                });
        }

        void Model_OnNeedRefreshSupAgreementsList()
        {
            SupAgreementsGrid.ItemsSource = Model.SupAgreementsList;
            SupAgreementsGrid.RefreshData();
            txtContractSupAgreementEndDate.GetBindingExpression(BaseEdit.EditValueProperty).UpdateTarget();
        }

        private void AddSupAgreement_Click(object sender, RoutedEventArgs e)
        {
            long lID = GetContractID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(SupAgreementView), ViewModelState.Create, lID, "Доп. соглашение");
        }

        private long GetContractID()
        {
            return Model?.ID ?? 0;
        }

        public long GetSelectedSupAgreementID()
        {
            return Convert.ToInt64(SupAgreementsGrid.GetCellValue(SupAgreementsGrid.View.FocusedRowHandle, "ID"));
        }

        private void SIContractsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SupAgreementsGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            long lID = GetSelectedSupAgreementID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(SupAgreementView), ViewModelState.Edit, lID, "Доп. соглашение");
        }

        private void TableView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Model.SelectedSupAgreement = SupAgreementsGrid.GetFocusedRow() as SupAgreement;
        }

        private void ContractNameComboBoxEditSelectedIndexChanged(object sender, RoutedEventArgs e)
        {
            var tabPanel = Parent as LayoutPanel;
            if (tabPanel == null) return;
            string titleSuffix = (Model.ContractType == Document.Types.SI) ? " СИ" : (Model.ContractType == Document.Types.VR) ? " ВР" : string.Empty;
            switch (Model.State) { 
                case ViewModelState.Create:
                    tabPanel.Caption = "Создать договор" + titleSuffix;
                    Model.NewCaption = "Договор" + titleSuffix;
                    break;
                case ViewModelState.Edit:
                    tabPanel.Caption = "Договор" + titleSuffix;
                    break;
            }
        }
    }
}
