﻿using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : DXWindow
    {
        public static LoginView Instance { get; private set; }

        public LoginView()
        {
            Instance = this;
            InitializeComponent();

            if (LoadLastLogin())
            {
                txtPassword.Focus();
                cbSaveLogin.IsChecked = true;
            }
            else
            {
                txtLogin.Focus();
                cbSaveLogin.IsChecked = false;
            }

#if ITSTAND            
            //txtDomain.Text = "pfrinvst-dev.local";
            txtLogin.Text = "ptkdokiptechuser";
            txtPassword.Password = "p@ssw0rd";
#endif
        }

        private bool ValidateData()
        {
            return (LoginText != string.Empty) &&
                   //(txtDomain.Text.Trim() != string.Empty) &&
                   (PwdText != string.Empty);
        }

        private string LoginText => txtLogin.Text?.Trim();
        private string PwdText => txtPassword.Password?.Trim();

        private void DumpLoginIfNeeded()
        {
            if (cbSaveLogin.IsChecked.GetValueOrDefault())
                DumpLastLogin(LoginText);
            else
                DropLastLogin();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                DumpLoginIfNeeded();
                DialogResult = true;
            }
            else
                DXMessageBox.Show("Необходимо заполнить все поля!", "Внимание", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

       /* private void btneTokenLogin_Click(object sender, RoutedEventArgs e)
        {
            if (eTokenManager.DriverPresent)
            {
                eTokenLoginView eTLoginView = new eTokenLoginView();
                Tools.ThemesTool.SetCurrentTheme(eTLoginView);
                if (eTLoginView.ShowDialog().GetValueOrDefault() == true)
                {
                    txtDomain.Text = eTLoginView.Domain;
                    txtLogin.Text = eTLoginView.UserName;
                    DumpLoginIfNeeded();
                    DialogResult = true;
                }
            }
            else
            {
                string message = Properties.Resources.eTokenDriverNotPresentErrorString;
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }*/

        private void loginView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnOK_Click(null, null);

            if (e.Key == Key.Escape)
                btnCancel_Click(null, null);
        }

        private bool LoadLastLogin()
        {
            if (!string.IsNullOrEmpty(Core.Properties.Settings.Default.StoredLogin))
            {
                txtLogin.Text = Core.Properties.Settings.Default.StoredLogin?.Trim();
                return true;
            }
            return false;
        }

        private static void DumpLastLogin(string login)
        {
            Core.Properties.Settings.Default.StoredLogin = login?.Trim();
            Core.Properties.Settings.Default.Save();
        }

        private static void DropLastLogin()
        {
            DumpLastLogin(null);
        }
    }
}
