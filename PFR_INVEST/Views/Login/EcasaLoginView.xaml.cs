﻿using System;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using mshtml;
using PFR_INVEST.Common.Logger;
using SHDocVw;

namespace PFR_INVEST.Views.Login
{
    /// <summary>
    /// Interaction logic for ReportsView.xaml
    /// </summary>
    public partial class EcasaLoginView
    {
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetCookieEx(
            string url,
            string cookieName,
            StringBuilder cookieData,
            ref int size,
            int dwFlags,
            IntPtr lpReserved);

        private const int INTERNET_COOKIE_HTTPONLY = 0x2000;

        private bool _mAuthorized;

        private Guid _webBrowserAppGuid = new Guid("0002DF05-0000-0000-C000-000000000046");
        private Guid _webBrowser2ClassGuid = typeof(IWebBrowser2).GUID;
        private readonly string _url;
        public string UserName { get; private set; }
        public string SessionId { get; private set; }

        public ICommand OkCommand { get; set; }

        public EcasaLoginView(string url)
        {
            InitializeComponent();
            DataContext = this;
            _url = url;

            webBrowser.Navigated += webBrowser_Navigated;
            webBrowser.LoadCompleted += WebBrowser_LoadCompleted;
            webBrowser.Loaded += WebBrowser_Loaded;
            webBrowser.Navigating += WebBrowserOnNavigating;
            PreCheckCookies();
        }

        private void WebBrowserOnNavigating(object sender, NavigatingCancelEventArgs e)
        {
            //если не страница проверки аутентификации
            if (!e.Uri.ToString().Contains("auth_cred_submit"))
                return;
            //выбираем введенное имя юзера
            var doc = webBrowser.Document as HTMLDocument;
            //если страница логина, ищем поле username и вставляем данные о последнем логине
            var collection = doc?.getElementsByName("username");
            if (collection == null)
                return;
            foreach (IHTMLElement a in collection)
            {
                _el = a as HTMLInputElement;
                if (_el == null) continue;
                UserName = _el.value;
                break;
            }
        }

        private void WebBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            var activeX = webBrowser.GetType().InvokeMember("ActiveXInstance",
                BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, webBrowser, new object[] {}) as WebBrowser;
            if (activeX != null)
            {
                //при закрытии страницы скриптом, закрываем окно
                activeX.WindowClosing += (bool window, ref bool cancel) =>
                {
                   DialogResult = false;
                };
                //вырубаем ошибки скриптов и генерацию доп. окон в целом
                activeX.Silent = true;
            }
        }

        private HTMLInputElement _el;

        private void WebBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            const string script = "document.body.style.overflow ='hidden'";
            webBrowser.InvokeScript("execScript", script, "JavaScript");
            webBrowser.InvokeScript("execScript", "window.alert = function () { }", "JavaScript");

            //если не страница логина - выходим
            if (!e.Uri.ToString().Contains("obrareq") && !e.Uri.ToString().Contains("auth_cred_submit"))
                return;
            var doc = webBrowser.Document as HTMLDocument;
            //если страница логина, ищем поле username и вставляем данные о последнем логине
            var collection = doc?.getElementsByName("username");
            if (collection == null)
                return;            
            foreach (IHTMLElement a in collection)
            {
                _el = a as HTMLInputElement;
                if (_el == null) continue;
                if(string.IsNullOrEmpty(_el.value))
                    _el.value = LoadLastLogin();
                break;
            }
        }

        private static string LoadLastLogin()
        {
            return !string.IsNullOrEmpty(Core.Properties.Settings.Default.StoredLogin) ? Core.Properties.Settings.Default.StoredLogin : null;
        }

        private static void DumpLastLogin(string login)
        {
            Core.Properties.Settings.Default.StoredLogin = login;
            Core.Properties.Settings.Default.Save();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (webBrowser.Document != null)
                return;
            //идем на локалхост втихую, контрол пока скрыт. Предотвращает баг контрола с кривыми вызовами событий.
            webBrowser.Navigate("http://localhost/");
        }

        private static void wbEvents2_NewWindow2(ref object ppDisp, ref bool cancel)
        {
            var wnd = new IEPopUpWindow
            {
                Visibility = Visibility.Hidden,
                Top = -10000,
                Left = -10000
            };
            wnd.Show();
            ppDisp = wnd.ActiveXWebBrowser.Application;
        }

        private bool _mHookInitialized;
        private int _counter;

        private void webBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            if (webBrowser.Source == null)
                return;

            if (!_mHookInitialized)
            {
                var serviceProvider = (IServiceProvider) webBrowser.Document;
                var myWebBrowser2 =
                    (IWebBrowser2) serviceProvider.QueryService(ref _webBrowserAppGuid, ref _webBrowser2ClassGuid);
                myWebBrowser2.RegisterAsBrowser = true;
                var wbEvents2 = (DWebBrowserEvents2_Event) myWebBrowser2;
                wbEvents2.NewWindow2 += wbEvents2_NewWindow2;
                _mHookInitialized = true;
            }

            if (e.Uri.ToString().Contains("localhost"))
            {
                webBrowser.Navigate(_url);
                return;
            }
            webBrowser.Visibility = Visibility.Visible;
            txt.Visibility = Visibility.Collapsed;


            if (_mAuthorized)
                return;
            if (e.Uri.ToString() == _url)
            {
                if (_counter < 1)
                {
                    _counter++;
                    return;
                }
                _mAuthorized = true;
                DialogResult = true;
                PostCheckCookies();
                DumpLastLogin(UserName);
            }else
            {
                if (!_isNewLoad)
                {
                    //log fail
                    try
                    {
                        WCFClient.CreatLightClient(UserName);
                        WCFClient.Client.SysLog(UserName, "Авторизация в системе", "Ошибка аутентификации пользователя", $"Ошибка аутентификации пользователя {UserName}", Environment.MachineName, LogSeverity.Error);
                        WCFClient.CloseLightClient();
                    }
                    catch
                    {
                        Logger.Instance.Error("Ошибка логирования аутентификации через Syslog");
                    }
                }
                _isNewLoad = false;
            }
        }

        private bool _isNewLoad = true;

        private void PreCheckCookies()
        {
         //   var uri = new Uri(_url);
         //   var c = GetUriCookieContainer(uri);
          //  if (c == null)
           //     return;
            //c.GetCookies(uri).ToList()
            
        }

        private void PostCheckCookies()
        {
            var uri = new Uri(_url);
            var c = GetUriCookieContainer(uri);
            foreach (Cookie cookie in c.GetCookies(uri))
            {
                switch (cookie.Name)
                {
                    case "OAM_REMOTE_USER":
                        UserName = cookie.Value.Split('=')[1];
                        break;
                    case "ObSSOCookie":
                        SessionId = cookie.Value.Split('=')[1];
                        break;
                }
            }
        }

        private static CookieContainer GetUriCookieContainer(Uri uri)
        {
            var cookies = new CookieContainer();
            // Determine the size of the cookie
            const int datasize = 8192*16;
            string[] cnames = {"OAM_REMOTE_USER", "ObSSOCookie", "OAMSESSIONID", "OAM_ID", "OAM_REQ_0", "OAM_REQ_COUNT"};

            foreach (var name in cnames)
            {
                var result = GetCookie(datasize, uri.ToString(), name);
                if (result != null && result.Length > 0)
                    cookies.Add(new Cookie(name, result.ToString().Replace(';', ','), "/", uri.Host) {HttpOnly = true});
            }

            return cookies.Count > 0 ? cookies : null;
        }

        private static StringBuilder GetCookie(int datasize, string uri, string name = null)
        {
            var cookieData = new StringBuilder(datasize);
            if (InternetGetCookieEx(uri, name, cookieData, ref datasize, INTERNET_COOKIE_HTTPONLY, IntPtr.Zero))
                return cookieData;
            if (datasize < 0)
                return null;
            // Allocate stringbuilder large enough to hold the cookie
            cookieData = new StringBuilder(datasize);
            return !InternetGetCookieEx(
                uri,
                null, cookieData,
                ref datasize,
                INTERNET_COOKIE_HTTPONLY,
                IntPtr.Zero)
                ? null
                : cookieData;
        }
    }

}