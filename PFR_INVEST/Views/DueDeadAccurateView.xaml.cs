﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DueDeadAccurateView.xaml
    /// </summary>
    public partial class DueDeadAccurateView : UserControl
    {
        public DueDeadAccurateView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SignUpForCloseRequest(this, true);
        }
    }
}
