﻿using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OnesImportJournalListView.xaml
    /// </summary>
    public partial class OnesExportJournalListView
    {
        public OnesExportJournalListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, listGrid);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            listGrid.CustomColumnDisplayText += listGrid_CustomColumnDisplayText;
        }

        public OnesExportJournalListViewModel Model => DataContext as OnesExportJournalListViewModel;

        private void listGrid_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            const string dispText = "Сессия номер {0} от {1}";
            if (!listGrid.IsValidRowHandle(e.RowHandle))
                return;

            if (e.Column.FieldName == "SessionID")
            {
                var currRow = (e.Row as OnesJournal);
                if (currRow != null)
                    e.DisplayText = string.Format(dispText, currRow.SessionID, currRow.LogDate.ToShortDateString());
            }

            if (e.Column.FieldName == "FileID")
            {
                var currRow = (e.Row as OnesJournal);
                if (currRow != null && currRow.ImportID == 0)
                    e.DisplayText = string.Format(dispText, "Не указан", "");
            }
        }
    }
}
