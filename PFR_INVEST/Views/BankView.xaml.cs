﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{

    public partial class BankView : UserControl
    {
        public BankView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public BankViewModel Model => DataContext as BankViewModel;

        private void DepositListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DepositListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (DepositListGrid.View.FocusedRowHandle >= 0)
            {
                App.DashboardManager.OpenNewTab(typeof(DepositView), ViewModelState.Edit,
                    GetSelectedDepositID(), "Депозит");
            }
        }

        public long GetSelectedDepositID()
        {
            if (DepositListGrid.View.FocusedRowHandle < 0) return 0;
            try
            {
                return (long)DepositListGrid.GetCellValue(DepositListGrid.View.FocusedRowHandle, "ID");
            }
            catch
            {
                return 0;
            }
        }

        private void PersonsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var x = PersonGrid.GetFocusedRow() as LegalEntityCourier;
            if (x == null)
                return;

            Model.EditPerson(x);
        }
    }
}