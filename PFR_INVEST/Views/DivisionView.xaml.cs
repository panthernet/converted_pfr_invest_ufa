﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DivisionView.xaml
    /// </summary>
    public partial class DivisionView : UserControl
    {
        public DivisionView()
        {
            InitializeComponent();
            c.MouseDoubleClick += c_MouseDoubleClick;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void c_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (c.View.GetRowElementByMouseEventArgs(e) == null) return;
            App.DashboardManager.OpenNewTab(typeof(PersonView), ViewModelState.Edit, this.GetSelectedID(), "Сотрудник");
        }

        public long GetSelectedID()
        {
            if (c.View.FocusedRowHandle >= 0 && c.View.FocusedRowHandle > -1000)
                return (long)c.GetCellValue(c.View.FocusedRowHandle, "ID");

            return 0;
        }

        public DivisionViewModel Model => DataContext as DivisionViewModel;
    }
}
