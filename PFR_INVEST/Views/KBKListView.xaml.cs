﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for KBKListView.xaml
    /// </summary>
    public partial class KBKListView
    {
        public KBKListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grdKBKList);
        }

        protected void tvKBKList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (grdKBKList.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (grdKBKList.View.FocusedRowHandle >= 0)
            {
                OpenItem((long)grdKBKList.GetCellValue(grdKBKList.View.FocusedRowHandle, "ID"));
            }
        }

        protected virtual void OpenItem(long id){
            App.DashboardManager.OpenNewTab(typeof(KBKView), ViewModelState.Edit,id, "Наименование КБК");}
    }
}
