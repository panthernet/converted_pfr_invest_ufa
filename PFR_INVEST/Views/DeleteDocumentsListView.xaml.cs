﻿using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for OwnFundsListView.xaml
    /// </summary>
    public partial class DeleteDocumentsListView
    {
        public DeleteDocumentsListView()
        {
            InitializeComponent();
            Loaded += DeleteDocumentsListView_Loaded;
            Unloaded += DeleteDocumentsListView_Unloaded;
            DocsGrid.CurrentItemChanged +=DocsGridOnCurrentItemChanged;
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, DocsGrid);
        }

        private void DocsGridOnCurrentItemChanged(object sender, CurrentItemChangedEventArgs e)
        {
            if (tableView.FocusedRowHandle >= 0)
            {
                object id = GetSelectedID();
                object otype = DocsGrid.GetCellValue(DocsGrid.View.FocusedRowHandle, "DocumentType");
                object delID = DocsGrid.GetCellValue(DocsGrid.View.FocusedRowHandle, "ID");
                resBut.CommandParameter = new[] { otype, id, delID };
            }
            else { resBut.CommandParameter = null; }
        }

        protected DeleteDocumentsListViewModel Model => DataContext as DeleteDocumentsListViewModel;


        private void DeleteDocumentsListView_Unloaded(object sender, RoutedEventArgs e)
        {
            CommandBindings.Remove(Model.RestoreDocumentBinding);
            Model.RestoreError -= Model_RestoreError;
        }

        private void DeleteDocumentsListView_Loaded(object sender, RoutedEventArgs e)
        {
            Model.RestoreError += Model_RestoreError;
            CommandBindings.Add(Model.RestoreDocumentBinding);
            DocsGridOnCurrentItemChanged(null, null);
        }

        private static void Model_RestoreError(object sender, System.EventArgs e)
        {
            ViewModelBase.DialogHelper.ShowError(sender.ToString());
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (DocsGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            StartCommand();
        }

        private void StartCommand()
        {
            if (DXMessageBox.Show("Восстановить выбранный элемент?", "Восстановление", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;
            ((DeleteDocumentsListViewModel)DataContext).RestoreDocument.Execute(resBut.CommandParameter, resBut);
        }

        public long? GetSelectedID()
        {
            if (DocsGrid.View.FocusedRowHandle <= -10000)
                return 0;
            if (DocsGrid.View.FocusedRowHandle >= 0)
                return (long?)DocsGrid.GetCellValue(DocsGrid.View.FocusedRowHandle, "DocumentID");
            return 0;
        }

        private void resBut_Click(object sender, RoutedEventArgs e)
        {
            if (tableView.FocusedRowHandle >= 0)
                StartCommand();
        }
    }
}
