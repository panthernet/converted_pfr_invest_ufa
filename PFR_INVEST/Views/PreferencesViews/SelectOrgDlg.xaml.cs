﻿using System.Windows;

namespace PFR_INVEST.Views
{

    public partial class SelectOrgDlg : Window //UserControl
    {
        public SelectOrgDlg()
        {
            InitializeComponent();

            Tools.ThemesTool.SetCurrentTheme(this);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
