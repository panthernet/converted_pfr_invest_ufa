﻿using System.Windows.Controls;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views.PreferencesViews
{
	/// <summary>
	/// Interaction logic for ClientThemesView.xaml
	/// </summary>
	public partial class ClientThemesView : UserControl
	{
		public ClientThemesView()
		{
			InitializeComponent();
            cbThemes.SelectedIndex = ThemesTool.GetActualTheme();
            cbThemes.SelectedIndexChanged += cbThemes_SelectedIndexChanged;
		}

        void cbThemes_SelectedIndexChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            if (cbThemes.SelectedIndex == -1) return;
            App.UserSettingsManager.SetThemeIndex(cbThemes.SelectedIndex);
            Core.Properties.Settings.Default.ClientTheme = cbThemes.SelectedIndex;
            Core.Properties.Settings.Default.Save();
            ThemesTool.ApplyGlobalTheme(cbThemes.SelectedIndex);
            if (MainWindow.Instance.ThemeFeature != null)
                MainWindow.Instance.ThemeFeature.Dispose();
            MainWindow.Instance.ThemeFeature =  ThemesTool.UpdateFeature(MainWindow.Instance.featCanvas);
        }
	}
}
