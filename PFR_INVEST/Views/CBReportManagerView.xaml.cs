﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Editors.Helpers;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CBReportManagerView.xaml
    /// </summary>
    public partial class CBReportManagerView
    {
        private CBReportManagerViewModel Model => (CBReportManagerViewModel)DataContext;
        private readonly bool _isCentralized;

        public CBReportManagerView(bool centralized = false)
        {
            InitializeComponent();
            if (centralized)
            {
                _isCentralized = true;
                ctrlPanel.Visibility = Visibility.Collapsed;
                //grid.Columns.Insert(3, new GridColumn { FieldName = "OKUD", Header = "ОКУД", SortOrder = ColumnSortOrder.Ascending});                
            }
            else okudColumn.Visible = false;

            ModelInteractionHelper.SignUpForCustomAction(this, o =>
            {
                if (!(o is long)) return;
                var id = (long)o;
                SelectRow(id);
            });

            grid.View.ShowFilterPopup += View_ShowFilterPopup;
        }

        private List<CustomComboBoxItem> _customStatusFilterItems;

        private void View_ShowFilterPopup(object sender, DevExpress.Xpf.Grid.FilterPopupEventArgs e)
        {
            if (e.Column.FieldName != "IsApproved") return;

            if (_customStatusFilterItems != null){ e.ComboBoxEdit.ItemsSource = _customStatusFilterItems; return; }
            
            var list = (e.ComboBoxEdit.ItemsSource as List<object>)?.ConvertAll(a => a as CustomComboBoxItem);
            list.Where(a => !a.DisplayValue.ToString().StartsWith("(")).ForEach(a => a.DisplayValue = BOReportForm1.GetStatusName(int.Parse(a.DisplayValue.ToString())));
            e.ComboBoxEdit.ItemsSource = _customStatusFilterItems = list;
        }

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Model?.SelectedReport == null || _isCentralized) return;
            //Отчет в Банк России (форма №1) за .... квартал .... года
            App.DashboardManager.OpenNewTab(typeof(BOReportForm1View), ViewModelState.Edit, Model.SelectedReport.ID,
                $"Отчет в Банк России (форма №1) за {Model.SelectedReport.QuartalText} {Model.SelectedReport.Year} года");
        }

        private void SelectRow(long id)
        {
            try
            {
                grid.RefreshData();
                var item = Model.Reports.FirstOrDefault(a => a.ID == id);
                if (item == null) return;
                grid.ExpandFocusRow(grid.GetRowHandleByListIndex(Model.Reports.IndexOf(item)));
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex);
            }
        }

        private void Image_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            dynamic obj = sender;
            ViewModelBase.DialogHelper.OpenCBReportApprovementStatus((long?)obj.Tag);
        }
    }
}
