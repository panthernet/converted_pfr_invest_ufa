﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PFRBranchView.xaml
    /// </summary>
    public partial class PFRBranchView
    {
        public PFRBranchView()
        {
            InitializeComponent();
            c.MouseDoubleClick += c_MouseDoubleClick;
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        private void c_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (c.View.GetRowElementByMouseEventArgs(e) == null) return;
            App.DashboardManager.OpenNewTab(typeof(PFRContactView), ViewModelState.Edit, GetSelectedID(), "Контакт ПФР");
        }

        public long GetSelectedID()
        {
            if (c.View.FocusedRowHandle >= 0 && c.View.FocusedRowHandle > -1000)
                return (long)c.GetCellValue(c.View.FocusedRowHandle, "ID");

            return 0;
        }

        public PFRBranchViewModel Model => DataContext as PFRBranchViewModel;
    }
}
