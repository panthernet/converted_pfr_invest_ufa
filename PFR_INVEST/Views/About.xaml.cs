﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using DevExpress.Xpf.Core;
using PFR_INVEST.Tools;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : DXWindow
    {
        /// <summary>
        /// Хранилище валидных сборок
        /// </summary>
        public List<AInfo> AIList = new List<AInfo>();

        /// <summary>
        /// Имя компании
        /// </summary>
        private const string COMPANY_NAME = "© 2011, ЗАО «Фирма АйТи». Информационные технологии»";
        /// <summary>
        /// Интернет адрес компании
        /// </summary>
        private const string COMPANY_URL = "http://www.it.ru";
        /// <summary>
        /// Путь запуска соглашения
        /// </summary>
        private const string EULAURL = "EULA//EndUserLicenseAgreement.pdf";
        private const string MANUALURL = "EULA//manual.docx";
        /// <summary>
        /// Части имен сборок для исключения из списка
        /// </summary>
        private readonly List<string> _excludeAssemblyNames = new List<string>() { "vshost", ".resources" };

        public About()
        {
            InitializeComponent();
            defaultContent.Visibility = Visibility.Visible;
            proContent.Visibility = Visibility.Collapsed;

            var ai = new AInfo(Assembly.GetExecutingAssembly());
            versionname.Text = ai.Product;
            versionbox.Text = "Версия: " + ai.Version;
            devbox.Text = COMPANY_NAME;
            GetReferencedAssemblies();
            DataContext = AIList;
            ThemesTool.SetCurrentTheme(this);
            Topmost = true;
            Closed += (sender, e) => _sp?.Stop();
        }

        /// <summary>
        /// Заполнить список валидными сборками
        /// </summary>
        private void GetReferencedAssemblies()
        {
            var domain = Thread.GetDomain();
            Assembly[] asms = domain.GetAssemblies();
            foreach (var assem in asms)
            {
                //исключать глобальные сборки
                if (assem.GlobalAssemblyCache) continue;
                //проверять на исключения имен
                if (!IsAssemblyNameValid(assem)) continue;
                AIList.Add(new AInfo(assem));
            }
        }

        /// <summary>
        /// Проверка на валидность имени сборки
        /// </summary>
        /// <param name="ass">Сборка</param>
        private bool IsAssemblyNameValid(Assembly ass)
        {
            return _excludeAssemblyNames.Count == 0 || _excludeAssemblyNames.All(str => !ass.FullName.Contains(str));
        }

        private void butClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Process.Start(COMPANY_URL);
            }
            catch { DXMessageBox.Show("Не найден браузер по умолчанию. Проверьте системные настройки!","Ошибка", MessageBoxButton.OK, MessageBoxImage.Error); }
        }

        /// <summary>
        /// Return path to EULA
        /// </summary>
        public static string GetPathToEULA()
        {
            return String.Format("{0}//{1}", Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")), EULAURL);
        }

        /// <summary>
        /// Return path to Manual
        /// </summary>
        public static string GetPathToManual()
        {
            return String.Format("{0}//{1}", Path.GetDirectoryName(Environment.CommandLine.Replace("\"", "")), MANUALURL);
        }
        /// <summary>
        /// Get version number
        /// </summary>
        public static string GetVersionNumber()
        {
            return new AInfo(Assembly.GetExecutingAssembly()).Version;
        }


        private void butULA_Click(object sender, RoutedEventArgs e)
        {
            string path = GetPathToEULA();
            try
            {
                Process.Start(path);
            }
            catch
            {
                DXMessageBox.Show(string.Format("При попытке открыть соглашение возникла ошибка!\nПроверьте наличие установленного Acrobat Reader или откройте соглашение вручную по адресу:\n{0}", path), "Внимание!");
            }
        }

        private void butManual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(GetPathToManual());
            }
            catch { DXMessageBox.Show("Невозможно открыть руководство пользователя! Проверьте наличие в системе установленного  MS Word.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error); }

        }

        private SoundPlayer _sp;
        private int _cCount = 0;
        private void UIElement_OnMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var t = ((Image) sender).RenderTransform as RotateTransform;
            t.Angle += 45;
            _cCount++;

            if (_cCount > 7)
            {
                _cCount = 0;
                //Canvas.SetTop(sliderContent, 250);

                proContent.Visibility = Visibility.Visible;
                defaultContent.Visibility = Visibility.Collapsed;
                var sb = new Storyboard();
                var anim = new DoubleAnimation(250, -500, new Duration(TimeSpan.FromSeconds(18)));
                Timeline.SetDesiredFrameRate(anim,15);
                Storyboard.SetTarget(anim, sliderContent);
                Storyboard.SetTargetProperty(anim, new PropertyPath("(Canvas.Top)"));
                sb.Children.Add(anim);
                sb.Completed += (o, e1) =>
                {
                    _sp?.Stop();
                    defaultContent.Visibility = Visibility.Visible;
                    proContent.Visibility = Visibility.Collapsed;
                };
                sb.Begin();
                try
                {
                    _sp = new SoundPlayer(Assembly.GetExecutingAssembly().GetManifestResourceStream("PFR_INVEST.Images.play.wav"));
                    _sp?.Play();
                }
                catch
                {
                    //ignore
                }
            }
        }
    }

    #region AInfo - информация о сборке

    public class AInfo
    {
        public string Version { get; private set; }
        public string Name { get; private set; }
        public string Company { get; private set; }
        public string Copyright { get; private set; }
        public string Product { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }

        public string CustomData { get; set; }

        public AInfo(Assembly ass)
        {
            Name = ass.GetName().Name;
            Version = ass.GetName().Version.ToString();
            object[] objs = ass.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            Copyright = objs.Length > 0 ? ((AssemblyCopyrightAttribute)objs[0]).Copyright : "";

            objs = ass.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
            Company = objs.Length > 0 ? ((AssemblyCompanyAttribute)objs[0]).Company : "";

            objs = ass.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            Product = objs.Length > 0 ? ((AssemblyProductAttribute)objs[0]).Product : "";

            objs = ass.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            Title = objs.Length > 0 ? ((AssemblyTitleAttribute)objs[0]).Title : "";

            objs = ass.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            Description = objs.Length > 0 ? ((AssemblyDescriptionAttribute)objs[0]).Description : "";
        }
    } 
    #endregion
}
