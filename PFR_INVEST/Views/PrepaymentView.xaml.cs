﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PrepaymentView.xaml
    /// </summary>
    public partial class PrepaymentView : UserControl
    {
        private PrepaymentViewModel Model => DataContext as PrepaymentViewModel;

        public PrepaymentView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        public string GetSelectedDop()
        {
            if (Dops.View.FocusedRowHandle >= 0 && Dops.View.FocusedRowHandle > -1000)
                return Dops.GetCellValue(Dops.View.FocusedRowHandle, "ID").ToString();

            return string.Empty;
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Dops.View.GetRowElementByMouseEventArgs(e) == null) return;
            long dopID = -1;
            try
            {
                dopID = Convert.ToInt64(GetSelectedDop());
            }
            catch
            {
                // ignored
            }
            if (dopID > 0)
            {
                var vm =
                    (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.OFPR_manager) ||
                    AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
                    && Model.State != ViewModelState.Read
                    ? ViewModelState.Edit : ViewModelState.Read;
                App.DashboardManager.OpenNewTab(typeof(PrepaymentAccurate), vm, dopID, DueDocKindIdentifier.PrepaymentAccurate);
            }
        }
    }
}
