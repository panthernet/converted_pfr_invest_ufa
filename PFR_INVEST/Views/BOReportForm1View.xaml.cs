﻿using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for BOReportForm1View.xaml
    /// </summary>
    public partial class BOReportForm1View
    {
        public BOReportForm1ViewModel Model => (BOReportForm1ViewModel) DataContext;

        public BOReportForm1View()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCustomAction(this, o =>
            {
                if (grid.ItemsSource == null)
                    grid.ItemsSource = Model.Report.SumList;
                grid.RefreshData();
            });
        }
    }
}
