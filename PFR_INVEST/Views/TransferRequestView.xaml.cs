﻿using System.Windows;
using System.Windows.Controls;
using PFR_INVEST.Views.Interface;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for TransferRequestView.xaml
    /// </summary>
    public partial class TransferRequestView : UserControl, IAutoParentPanelSize
    {
        #region IAutoParentPanelSize
        public double DesiredWidth => 320;

        protected override Size MeasureOverride(Size constraint)
        {
            return DevExpressHelper.MeasureParentPanel(this, base.MeasureOverride, DesiredWidth);
        }
        #endregion

        public TransferRequestView(bool hideInn = false)
        {
            InitializeComponent();
            if (hideInn)
                inndp.Visibility = Visibility.Collapsed;

            ModelInteractionHelper.SubscribeForLoadingIndicator(this);
            ModelInteractionHelper.SignUpForCloseRequest(this);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

    }
}
