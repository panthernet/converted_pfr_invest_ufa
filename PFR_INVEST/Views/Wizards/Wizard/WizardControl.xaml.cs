﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;

namespace PFR_INVEST.Views.Wizards
{
	/// <summary>
	/// Interaction logic for Wizard.xaml
	/// </summary>
	public partial class WizardControl : UserControl, IWizard, IDisposable
	{
		private readonly Dictionary<string, IWizardStep> _steps;

		public int ActiveStepIndex { get; private set; }
		public List<KeyValuePair<string, Type>> WizardSteps { get; private set; }
		public IWizardStep ActiveStep { get; private set; }


		public static readonly RoutedEvent CompleteEvent =
			EventManager.RegisterRoutedEvent("CompleteEvent",
			RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WizardControl));

		public event RoutedEventHandler Complete
		{
			add { AddHandler(CompleteEvent, value); }
			remove { RemoveHandler(CompleteEvent, value); }
		}

		protected void OnComplete()
		{
			RaiseEvent(new RoutedEventArgs(CompleteEvent));

		}

		public static readonly RoutedEvent CancelEvent =
			EventManager.RegisterRoutedEvent("CancelEvent",
			RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WizardControl));

		public event RoutedEventHandler Cancel
		{
			add { AddHandler(CancelEvent, value); }
			remove { RemoveHandler(CancelEvent, value); }
		}

		protected void OnCancel()
		{
			RaiseEvent(new RoutedEventArgs(CancelEvent));
		}

		public WizardControl()
		{
			InitializeComponent();
			_steps = new Dictionary<string, IWizardStep>();
			WizardSteps = new List<KeyValuePair<string, Type>>();
        }

		private void ShowStepContent()
		{

			var activeStepInfo = WizardSteps[ActiveStepIndex];

			if (!_steps.ContainsKey(activeStepInfo.Key))
			{
				if (activeStepInfo.Value.GetInterface("IWizardStep") == null)
					throw new InvalidCastException("Wizard step control must implement IWizardStep interface");

				var step = (IWizardStep)Activator.CreateInstance(activeStepInfo.Value);
				_steps.Add(activeStepInfo.Key, step);
			}

			ActiveStep = _steps[activeStepInfo.Key];
			ActiveStep.Wizard = this;

			dckStepContent.Children.Clear();
			dckStepContent.Children.Add((UIElement)ActiveStep);

			((FrameworkElement)ActiveStep).DataContext = DataContext;

            using (Loading.StartNew("Загрузка данных"))
            {
                ActiveStep.OnShow();
			}

			EnsureControlState();
		}

		public void EnsureControlState()
		{
			if (ActiveStepIndex == WizardSteps.Count - 1 && ActiveStep.NextButtonState != WizardNavigationButtonState.Hidden)
				btnFinish.Visibility = Visibility.Visible;
			else
				btnFinish.Visibility = Visibility.Collapsed;

			btnFinish.IsEnabled = ActiveStep.NextButtonState != WizardNavigationButtonState.Disabled;
			btnNext.Visibility = !btnFinish.IsVisible && ActiveStep.NextButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnNext.IsEnabled = ActiveStep.NextButtonState != WizardNavigationButtonState.Disabled;
			btnPrev.Visibility = ActiveStep.PrevButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnPrev.IsEnabled = ActiveStepIndex != 0 && ActiveStep.PrevButtonState != WizardNavigationButtonState.Hidden;
			btnCancel.Visibility = ActiveStep.CancelButtonState != WizardNavigationButtonState.Hidden ? Visibility.Visible : Visibility.Collapsed;
			btnCancel.IsEnabled = ActiveStep.CancelButtonState != WizardNavigationButtonState.Disabled;
		}


		public void UpdateNavigationControlsState()
		{
			EnsureControlState();
		}


		public void SetDimensions(int width, int height, int maxWidth = 0, int maxHeight = 0)
		{
			MinWidth = width;
			MinHeight = height;

			MaxHeight = Math.Max(height, maxHeight);
			MaxWidth = Math.Max(width, maxWidth);
		}

		public void SetFreeDimensions(int width, int height, bool setMin = true)
		{
            if (setMin)
            {
                MinWidth = width;
                MinHeight = height;
            }

            MaxWidth = double.PositiveInfinity;
			MaxHeight = double.PositiveInfinity;
			HorizontalAlignment = HorizontalAlignment.Stretch;
		}


		public void ShowNextStep()
		{
			if (ActiveStepIndex == WizardSteps.Count - 1)
			{
				OnComplete();
				return;
			}

			ActiveStepIndex++;
			ShowStepContent();
		}

		public void ShowStep(int stepIndex)
		{
			if (stepIndex >= WizardSteps.Count)
				throw new IndexOutOfRangeException();

			ActiveStepIndex = stepIndex;
			ShowStepContent();
		}

		public void ShowPreviousStep()
		{
			if (ActiveStepIndex > 0)
			{
				ActiveStepIndex--;
				ShowStepContent();
			}
		}



		private void btnNext_Click(object sender, RoutedEventArgs e)
		{
			bool b = ActiveStep.OnMoveNext();
			if (b)
				ShowNextStep();
		}

        private void btnPrev_Click(object sender, RoutedEventArgs e)
		{
			bool b = ActiveStep.OnMovePrev();
			if (b)
				ShowPreviousStep();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			if (DXMessageBox.Show("Вы уверены?", "Закрыть мастер", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.Cancel)
				return;
			DoCancel();
		}


		public void DoCancel()
		{
			OnCancel();
		}

		public void Dispose()
		{
			if (_steps != null)
				_steps.Clear();
			WizardSteps.Clear();
		}
	}
}
