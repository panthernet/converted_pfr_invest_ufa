﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    /// <summary>
    /// Interaction logic for SpnDepositWizardView.xaml
    /// </summary>
    public partial class SpnDepositWizardView 
    {
        private SpnDepositWizardViewModel Model => DataContext as SpnDepositWizardViewModel;

        public SpnDepositWizardView()
        {
            InitializeComponent();
            InitWizardSteps();
        }

        private void InitWizardSteps()
        {
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositCreateDepClaim", typeof(SpnDepositCreateDepClaim)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositCreateDepClaim2", typeof(SpnDepositCreateDepClaim2)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositPrintDocuments", typeof(SpnDepositPrintDocuments)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositImportXML", typeof(SpnDepositImportXML)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("Export1", typeof(SpnDepositPrintDocuments)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositExportCutoffRate", typeof(SpnDepositExportCutoffRate)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositImportDepClaimConfirmXML", typeof(SpnDepositImportDepClaimConfirmXML)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("Export2", typeof(SpnDepositPrintDocuments)));
            wizard.WizardSteps.Add(new KeyValuePair<string, Type>("SpnDepositGenerateOffers", typeof(SpnDepositGenerateOffers)));

            wizard.btnFinish.Content = "Завершить";

            wizard.Cancel += (sender, args) =>
            {
                if (!Model.OnCancel()) return;
                wizard.Close(false);
            };
            wizard.Complete += (sender, args) =>
            {
                if (!Model.FinalAction()) return;
                wizard.Close();
            };
        }
    }
}
