﻿using System.Windows;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.SpnDeposit
{
    public class SpnDepositExportCutoffRateUserControl : GenericWizardStepEx<SpnDepositWizardViewModel> { }
    /// <summary>
    /// Interaction logic for SpnDepositExportCutoffRate.xaml
    /// </summary>
    public partial class SpnDepositExportCutoffRate
    {    
        public SpnDepositExportCutoffRate()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Вызывается каждый раз при отображении шага
        /// </summary>
        public override void OnShow()
        {
            NextButtonText = Model.IsMoscowStock ? "Сохранить" : "Экспортировать";
            dpSave.Visibility = Model.IsMoscowStock ? Visibility.Collapsed : Visibility.Visible;
            gbHeader.Header = Model.IsMoscowStock ? "Ввод максимальной ставки отсечения" : "Экспорт максимальной ставки отсечения";
            Model.OnShow();
        }
    }
}
