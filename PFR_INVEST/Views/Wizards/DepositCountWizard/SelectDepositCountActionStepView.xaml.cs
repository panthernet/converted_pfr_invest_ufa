﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;

namespace PFR_INVEST.Views.Wizards.DepositCountWizard
{
    public class SelectDepositCountActionStepViewUserControl : GenericWizardStepEx<DepositCountWizardViewModel> { }
    /// <summary>
    /// Interaction logic for SelectDepositCountActionStepView.xaml
    /// </summary>
    public partial class SelectDepositCountActionStepView
    {
        public SelectDepositCountActionStepView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Вызывается при успешном переходе на следующий шаг, до самого перехода
        /// </summary>
        public override bool OnMoveNext()
        {
            var result = Model.OnMoveNext();
            if (!result) return false;
            if(Wizard.WizardSteps.Count > 0)
                Wizard.WizardSteps.Remove(Wizard.WizardSteps.Last());
            Wizard.WizardSteps.Add(Model.SelectedAction == 0
                ? new KeyValuePair<string, Type>("ShowDepositsCreateStepView", typeof(ShowDepositsCreateStepView))
                : new KeyValuePair<string, Type>("ShowDepositsReturnStepView", typeof(ShowDepositsReturnStepView)));
            return true;
        }

        /// <summary>
        /// Вызывается каждый раз при отображении шага
        /// </summary>
        public override void OnShow()
        {
            if (Wizard.WizardSteps.Count > 1 && Wizard.WizardSteps.Last().Key != "Fake")
                Wizard.WizardSteps.Remove(Wizard.WizardSteps.Last());
            if (Wizard.WizardSteps.Count == 1)
                Wizard.WizardSteps.Add(new KeyValuePair<string, Type>("Fake", GetType()));
            Model.OnShow();
        }
    }
}
