﻿using System;
using System.Collections.Generic;
using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.PlanCorrectionWizard
{
    /// <summary>
    /// Interaction logic for PlanCorrectionWizardView.xaml
    /// </summary>
    public partial class PlanCorrectionWizardView
    {
        private PlanCorrectionWizardViewModel<PlanContractRecord> Model => DataContext as PlanCorrectionWizardViewModel<PlanContractRecord>;

        public PlanCorrectionWizardView()
        {
            InitializeComponent();
            InitWizardSteps();
            ctlWizard.SizeChanged += ctlWizard_SizeChanged;
        }

        public void ctlWizard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            /*if (e.PreviousSize.Width != 0 || e.PreviousSize.Height != 0)
            {
				var sz = new Size(ctlWizard.MinWidth + 50, ctlWizard.MinHeight + 40);
				var max = new Size(ctlWizard.MaxWidth + 50, ctlWizard.MaxHeight + 40);
				DashboardManager.ResizeActiveView(sz, max);
            }*/
        }

        public void InitWizardSteps()
        {

            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("OpTypeSelect", typeof(OpTypeSelectView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("YearMonth", typeof(YearMonthSelectView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SumsEdit", typeof(UKPlanSumsEditView)));

            Loaded += PlanCorrectionWizardView_Loaded;
            Wizard.Cancel += Wizard_Cancel;
            Wizard.Complete += Wizard_Complete;

        }

        private void Wizard_Complete(object sender, RoutedEventArgs e)
        {
            Model.UpdateUKPlanSums();
            DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

        private void Wizard_Cancel(object sender, RoutedEventArgs e)
        {
            DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

        private void PlanCorrectionWizardView_Loaded(object sender, RoutedEventArgs e)
        {
            ctlWizard.ShowStep(0);
            ctlWizard.btnFinish.Content = "Готово";
        }

        public IWizard Wizard => ctlWizard;
    }
}
