﻿using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.CBReport;

namespace PFR_INVEST.Views.Wizards.CBReportWizard
{
    /// <summary>
    /// Interaction logic for SelectCBReportView.xaml
    /// </summary>
    public partial class SelectCBReportView : IWizardStep
    {
        private CBReportWizardWindowViewModel Model => DataContext as CBReportWizardWindowViewModel;
        public IWizard Wizard { get; set; }

        public SelectCBReportView()
        {
            InitializeComponent();
            DataContextChanged +=(sender, args) => Model.InitializeSelectCB();
        }

        public bool OnMoveNext()
        {
            var result = Model.SelectedYear != null && Model.SelectedYear !=0 && Model.SelectedQuarter != null && Model.SelectedOkud != null;
            if (!result)
                ViewModelBase.DialogHelper.ShowWarning("Необходимо заполнить все поля!");
            if (!WCFClient.Client.CBReportCanApprove(Model.GenerateAppItem()))
            {
                ViewModelBase.DialogHelper.ShowError("Для одного или нескольких отчетов не созданы файлы для согласования!\nСоздайте файл экспорта для отчета или выберите другой отчет!");
                return false;
            }
            return result;
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
            
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;
        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Disabled;
        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;
    }
}
