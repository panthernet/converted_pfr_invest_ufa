﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.CBReport;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views.Wizards.CBReportWizard
{
    /// <summary>
    /// Interaction logic for CBReportWizardWindow.xaml
    /// </summary>
    public partial class CBReportWizardWindow
    {
        private CBReportWizardWindowViewModel Model => DataContext as CBReportWizardWindowViewModel;

        public CBReportWizardWindow()
        {
            InitializeComponent();
            InitWizardSteps();
            Closing += (sender, args) =>
            {
                if (!DialogResult.HasValue && !ViewModelBase.DialogHelper.ShowConfirmation("Вы уверены, что хотите закрыть мастер?"))
                    args.Cancel = true;
            };
            ModelInteractionHelper.ForceDialogWindowToForeground(this);
        }

        public void InitWizardSteps()
        {
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectCBReport", typeof(SelectCBReportView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SelectApprovers", typeof(SelectApproversView)));

            Loaded += (sender, args) =>
            {
                ctlWizard.ShowStep(0);
                ctlWizard.btnFinish.Content = "Отправить на согласование";
            };
            ctlWizard.Cancel += (sender, args) =>
            {
                DialogResult = false;
                ctlWizard.Dispose();
            };
            ctlWizard.Complete += (sender, args) =>
            {
                Model.ExecuteApprovement();
                DialogResult = true;
                ctlWizard.Dispose();
            };
        }
    }
}
