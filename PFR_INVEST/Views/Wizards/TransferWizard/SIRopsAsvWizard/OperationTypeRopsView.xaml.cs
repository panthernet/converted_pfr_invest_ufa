﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI.RopsAsvWizard;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Views.Dialogs;

namespace PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard
{
    /// <summary>
    /// Interaction logic for OperationTypeRopsView.xaml
    /// </summary>
    public partial class OperationTypeRopsView : UserControl, IWizardStep
    {
        public OperationTypeRopsView()
        {
            InitializeComponent();
            Loaded += OperationTypeRopsView_Loaded;
        }

        private void OperationTypeRopsView_Loaded(object sender, RoutedEventArgs e)
        {
            if(Model == null)
                return;

            Model.PropertyChanged += (o, args) =>
            {
                if (new[] {"StartDate", "EndDate"}.Contains(args.PropertyName))
                    SetNextButtonState();
            };
        }

        public TransferSIRopsAsvWizardViewModel Model => DataContext as TransferSIRopsAsvWizardViewModel;

        public IWizard Wizard
        {
            get;
            set;
        }

        public bool OnMoveNext()
        {
            var error = string.Empty;

            if (!(Model.Comments == null || Model.Comments.Length <= 1000))
                error = "Размер поле комментарий должно быть не более 1000 символов.";

            if(!Model.StartDate.HasValue && !Model.EndDate.HasValue)
                error += "\r\nПериод задан не верно.";

            if (Model.IsShowRatio && !(Model.RatioASV.HasValue && Model.RatioEval.HasValue))
                error += "r\nНе верно заданы коэффициенты АСВ";

            if (!Model.IsShowRatio && !Model.RatioROPS.HasValue)
                error += "r\nНе верно задан коэффициент РОПС";

            if (string.IsNullOrEmpty(error))
                return true;
            else
            {
                    new DialogHelper().ShowError(error);
            }
            return false;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            //Wizard.SetDimensions(600, 310);
        }

        void SetNextButtonState()
        {
            NextButtonState = (Model.StartDate != null && Model.EndDate != null) ? WizardNavigationButtonState.Visible :
            WizardNavigationButtonState.Disabled;
            
        }

        public WizardNavigationButtonState NextButtonState { get; private set; }

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        protected void cmbTransferDirection_SelectionChanged(object sender, RoutedEventArgs e)
        {
            cmbTransferType.SelectedIndex = 0;
        }
        
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!Model.CreateSITransfers)
                lblRegisterDate.Content = "Дата создания:";
            
        }

       
    }
}
