﻿using System.Windows;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.TransferWizard
{
    /// <summary>
    /// Interaction logic for OperationTypeCheck.xaml
    /// </summary>
    public partial class OperationTypeCheckView : IWizardStep
    {
        public OperationTypeCheckView()
        {
            InitializeComponent(); 
            
        }

        public TransferWizardViewModel Model => DataContext as TransferWizardViewModel;

        public IWizard Wizard
        {
            get;
            set;
        }

        public bool OnMoveNext()
        {
            if (Model.Comments == null || Model.Comments.Length <= 1000)
                return true;
            new DialogHelper().ShowError("Размер поле комментарий должно быть не более 1000 символов.");
            return false;
        }

        public bool OnMovePrev()
        {
            return true;
        }

        public void OnShow()
        {
            //Wizard.SetDimensions(750, 310);
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Visible;

        protected void cmbTransferDirection_SelectionChanged(object sender, RoutedEventArgs e)
        {
            cmbTransferType.SelectedIndex = 0;
        }

        protected void cmbTransferType_SelectionChanged(object sender, RoutedEventArgs e)
        {
            ShowMonth();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!Model.CreateSITransfers)
                lblRegisterDate.Content = "Дата создания:";
            ShowMonth();
        }

        private void ShowMonth() { 
        //var op = this.cmbTransferType.SelectedItem as SPNOperation;
            if (Model is TransferVR13YearWizardViewModel || Model is TransferSI8YearWizardViewModel || Model is TransferVR8YearWizardViewModel)
            {
                cmbMonth.Visibility= Visibility.Hidden;
            }
        }

    }
}
