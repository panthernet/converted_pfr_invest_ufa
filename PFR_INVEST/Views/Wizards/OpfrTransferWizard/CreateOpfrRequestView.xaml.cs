﻿using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    public class CreateOpfrRequestViewUserControl : GenericWizardStepEx<OpfrTransferWizardViewModel> { }
    /// <summary>
    /// Interaction logic for CreateOpfrRequestView.xaml
    /// </summary>
    public partial class CreateOpfrRequestView
    {
        public CreateOpfrRequestView()
        {
            InitializeComponent();
        }
    }
}
