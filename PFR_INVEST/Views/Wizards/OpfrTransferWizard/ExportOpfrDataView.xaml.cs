﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using DevExpress.XtraGrid;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    public class ExportOpfrDataViewUserControl : GenericWizardStepEx<OpfrTransferWizardViewModel> { }
    /// <summary>
    /// Interaction logic for ExportOpfrDataView.xaml
    /// </summary>
    public partial class ExportOpfrDataView
    {        
        public ExportOpfrDataView()
        {
            InitializeComponent();
            c1.Header = "Направление";
            grid.Columns.Add(new GridColumn { FieldName = "Item2", Header = "Распоряжение", Name = "c1_1", SortMode = ColumnSortMode.Value });
            view.ShowFilterPopup += view_ShowFilterPopup;
        }

        #region FromWindow

        void view_ShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
        }

        private void UpdateSelectionForAll(bool newIsSelected)
        {
            var list = GetDataRowHandles();
            for (int i = 0; i < list.Count; i++)
            {
                int rowHandle = grid.GetRowHandleByListIndex(i);
                grid.SetCellValue(rowHandle, "Selected", newIsSelected);
            }
        }

        private bool _mGridRowCheckBoxClicked;

        private void CheckEdit_Checked(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                UpdateSelectionForAll(true);
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Indeterminate(object sender, RoutedEventArgs e)
        {
            if (!_mGridRowCheckBoxClicked)
                _headerCheckEdit.IsChecked = false;
            _mGridRowCheckBoxClicked = false;
        }

        private void CheckEdit_Unchecked(object sender, RoutedEventArgs e)
        {
            UpdateSelectionForAll(false);
            _mGridRowCheckBoxClicked = false;
        }

        private List<int> GetDataRowHandles()
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                int rowHandle = grid.GetRowHandleByVisibleIndex(i);
                if (grid.IsGroupRowHandle(rowHandle))
                {
                    if (!grid.IsGroupRowExpanded(rowHandle))
                    {
                        rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                    }
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        private List<int> GetDataRowHandlesInGroup(int groupRowHandle)
        {
            var rowHandles = new List<int>();
            for (int i = 0; i < grid.GetChildRowCount(groupRowHandle); i++)
            {
                int rowHandle = grid.GetChildRowHandle(groupRowHandle, i);
                if (grid.IsGroupRowHandle(rowHandle))
                {
                    rowHandles.AddRange(GetDataRowHandlesInGroup(rowHandle));
                }
                else
                    rowHandles.Add(rowHandle);
            }
            return rowHandles;
        }

        CheckBox _headerCheckEdit;
        private void CheckEdit_Loaded(object sender, RoutedEventArgs e)
        {
            _headerCheckEdit = sender as CheckBox;
        }

        #endregion
    }
}
