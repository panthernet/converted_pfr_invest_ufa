﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views.Wizards.OpfrTransferWizard
{
    /// <summary>
    /// Interaction logic for OpfrTransferWizardView.xaml
    /// </summary>
    public partial class OpfrTransferWizardView
    {
        private OpfrTransferWizardViewModel Model => DataContext as OpfrTransferWizardViewModel;
        public OpfrTransferWizardView()
        {
            InitializeComponent();
            InitWizardSteps();
        }

        private void InitWizardSteps()
        {
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("CreateOpfrRegisterView", typeof(CreateOpfrRegisterView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("ImportOpfrTransfersView", typeof(ImportOpfrTransfersView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("EditOpfrTransfersView", typeof(EditOpfrTransfersView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("CreateOpfrRequestView", typeof(CreateOpfrRequestView)));
            ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("ExportOpfrDataView", typeof(ExportOpfrDataView)));

            Loaded += (sender, args) =>
            {
                ctlWizard.ShowStep(0);
                ctlWizard.btnFinish.Content = "Завершить";
            };
            ctlWizard.Cancel += (sender, args) =>
            {
                ctlWizard.Close(false);
            };
            ctlWizard.Complete += (sender, args) =>
            {
                if(!Model.FinalAction()) return;
                App.mainWindow.OnesExecuteExportWrapper(OnesSettingsDef.IntGroup.Opfr, new List<long> { Model.ID });
                ctlWizard.Close();
            };
        }
    }
}
