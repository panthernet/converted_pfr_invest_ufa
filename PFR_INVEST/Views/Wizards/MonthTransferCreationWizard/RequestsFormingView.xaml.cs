﻿using System.Windows;
using System.Windows.Forms;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.MonthTransferCreationWizard
{
    /// <summary>
    /// Interaction logic for FinishScreenView.xaml
    /// </summary>
    public partial class RequestsFormingView : IWizardStep
    {
        private const string SELECT_FOLDER_DIALOG_DESC = "Выберите папку для сохранения требований.";
        private const string INVALID_SELECTED_FOLDER = "Неверно указана папка для сохранения требований!";
        private const string WARNING = "Внимание! Если выбранная папка не существует, она будет создана. В случае, если выбранная папка существует, все существующие требования в ней будут перезаписаны!";
        private const string INITIAL_SELECTED_FOLDER = "C:\\";

        public RequestsFormingView()
        {
            InitializeComponent();
            txtSelectedFolder.EditValue = INITIAL_SELECTED_FOLDER;
            txtWarnig.Text = WARNING;
        }

        public IWizard Wizard
        {
            get;
            set;
        }

        public MonthTransferCreationWizardModel Model => DataContext as MonthTransferCreationWizardModel;

        public bool OnMoveNext()
        {
            string folder = txtSelectedFolder.Text;
            if (!MonthTransferCreationWizardModel.CheckDirectoryName(folder))
            {
                DXMessageBox.Show(INVALID_SELECTED_FOLDER, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else
            {
                Model.SelectedFolder = folder;
                return true;
            }
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
            Wizard.UpdateNavigationControlsState();
            Wizard.SetDimensions(500, 310);
        }

        public WizardNavigationButtonState NextButtonState
        {
            get
            {
                if (Model.RegisterCreated)
                    return WizardNavigationButtonState.Visible;
                else
                    return WizardNavigationButtonState.Disabled;
            }
        }

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Hidden;

        private void SelecFolderButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog() { Description = SELECT_FOLDER_DIALOG_DESC })
            {

                if (MonthTransferCreationWizardModel.DirectoryExists(txtSelectedFolder.Text))
                    dlg.SelectedPath = txtSelectedFolder.Text;

                if (dlg.ShowDialog() == DialogResult.OK)
                    txtSelectedFolder.EditValue = dlg.SelectedPath;
            }
        }
    }
}
