﻿using System;
using System.Collections.Generic;
using System.Windows;
using PFR_INVEST.Views.PlanCorrectionWizard;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.MonthTransferCreationWizard
{
	/// <summary>
	/// Interaction logic for MonthTransferCreationWizardView.xaml
	/// </summary>
	public partial class MonthTransferCreationWizardView
	{
	    public MonthTransferCreationWizardView()
		{
			InitializeComponent();
			InitWizardSteps();
			ctlWizard.SizeChanged += ctlWizard_SizeChanged;
		}

		public void ctlWizard_SizeChanged(object sender, SizeChangedEventArgs e)
		{
		    /*if (e.PreviousSize.Width == 0 && e.PreviousSize.Height == 0)
                return;
		    var sz = new Size(ctlWizard.MinWidth + 10, ctlWizard.MinHeight + 40);
		    var max = new Size(ctlWizard.MaxWidth + 10, ctlWizard.MaxHeight + 40);
		    DashboardManager.ResizeActiveView(sz, max);*/
		}

		public void InitWizardSteps()
		{
			ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("OpTypeSelect", typeof(OpTypeSelectMTView)));
			ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("YearMonth", typeof(YearMonthSelectView)));
			ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("SumsEdit", typeof(UKFactSumsEditView)));
			//ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("PrintRequests", typeof(RequestsFormingView)));
			ctlWizard.WizardSteps.Add(new KeyValuePair<string, Type>("FinishScreen", typeof(FinishScreenView)));

			Loaded += PlanCorrectionWizardView_Loaded;
			Wizard.Cancel += Wizard_Cancel;
			Wizard.Complete += Wizard_Complete;
		}

		void Wizard_Complete(object sender, RoutedEventArgs e)
		{
			//Model.OpenCreatedFolder();
			DashboardManager.CloseActiveView();
			(ctlWizard.ActiveStep as FinishScreenView).OpenCreated();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

		void Wizard_Cancel(object sender, RoutedEventArgs e)
		{
			DashboardManager.CloseActiveView();
            Wizard.Cancel -= Wizard_Cancel;
            Wizard.Complete -= Wizard_Complete;
            ctlWizard.SizeChanged -= ctlWizard_SizeChanged;
            ctlWizard.Dispose();
        }

		void PlanCorrectionWizardView_Loaded(object sender, RoutedEventArgs e)
		{
			ctlWizard.ShowStep(0);
			ctlWizard.btnFinish.Content = "Готово";
		}

		public IWizard Wizard => ctlWizard;
	}
}
