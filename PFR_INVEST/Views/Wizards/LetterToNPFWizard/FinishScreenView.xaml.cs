﻿using System;
using System.Windows;
using System.Windows.Media;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Views.Wizards;

namespace PFR_INVEST.Views.LetterToNPFWizard
{
    /// <summary>
    /// Interaction logic for FinishScreenView.xaml
    /// </summary>
    public partial class FinishScreenView : IWizardStep
    {
        private const string INFO_SUCCESS = "Создание перечислений и выгрузка требований прошли успешно.";

        public FinishScreenView()
        {
            InitializeComponent();
        }

        public IWizard Wizard
        {
            get;
            set;
        }

        public LetterToNPFWizardViewModel Model => DataContext as LetterToNPFWizardViewModel;

        public bool OnMoveNext()
        {
            return true;
        }

        public bool OnMovePrev()
        {
            return false;
        }

        public void OnShow()
        {
            PrintLetters();
            txtInfo.Foreground = Brushes.Black;
            txtInfo.Content = INFO_SUCCESS;
            txtCount.EditValue = Model.PrintedCount;
            Wizard.UpdateNavigationControlsState();
        }

        public WizardNavigationButtonState NextButtonState => WizardNavigationButtonState.Visible;

        public WizardNavigationButtonState PrevButtonState => WizardNavigationButtonState.Hidden;

        public WizardNavigationButtonState CancelButtonState => WizardNavigationButtonState.Hidden;

        private void PrintLetters()
        {
            bool shouldRequest = true;

            while (shouldRequest)
            {
                try
                {
                    LetterToNPFWizardViewModel.PrintResult res;
                    using (Loading.StartNew("Смена пароля пользователя..."))
                    {
                       res = Model.PrintLetters();
                    }

                    string message = string.Empty;
                    switch (res)
                    {
                        case LetterToNPFWizardViewModel.PrintResult.CannotCreateDirectory:
                            message = "Невозможно создать папку для выгрузки.\n" +
                                      "Возможно у вас нет прав для записи в выбранную папку.\n" +
                                      "Выбрать другую папку?";
                            break;
                        case LetterToNPFWizardViewModel.PrintResult.CannotCreateFile:
                            message = "Невозможно создать файл с письмом.\n" +
                                      "Возможно у вас нет прав для записи в выбранную папку.\n" +
                                      "Выбрать другую папку?";
                            break;
                        case LetterToNPFWizardViewModel.PrintResult.TemplateNotFound:
                            message = "Файл с шаблоном для выгрузки не найден.\n" +
                                      "Загрузите файл шаблона и попробуйте еще раз.\n" +
                                      "Вернуться на предыдущую страницу мастера?";
                            break;
                    }

                    if (res != LetterToNPFWizardViewModel.PrintResult.OK)
                    {
                        txtInfo.Content = "Произошла ошибка при выгрузке писем.";
                        txtInfo.Foreground = Brushes.Red;
                        if (DXMessageBox.Show(message, "Ошибка при выгрузке писем", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            Wizard.ShowPreviousStep();
                    }

                    shouldRequest = false;
                }
                catch (Exception ex)
                {
                    App.log.WriteException(ex);
                    var res = DXMessageBox.Show("Произошла неизвестная ошибка.\nПовторить операцию?", "Неизвестная ошибка", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    shouldRequest = res == MessageBoxResult.Yes;
                    if (!shouldRequest)
                        Wizard.DoCancel();
                }
            }
        }
    }
}
