﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CBPaymentDetailListView.xaml
    /// </summary>
    public partial class CBPaymentDetailListView
    {
        public CBPaymentDetailListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, PaymentDetailGrid, "GridList");
		}

        private void PaymentDetailGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PaymentDetailGrid.View.GetRowElementByMouseEventArgs(e) == null) return;

            var item = PaymentDetailGrid.GetFocusedRow() as CBPaymentDetailListItem;
            if (EditPaymentDetailHelper.Command != null && EditPaymentDetailHelper.Command.CanExecute(item))
                EditPaymentDetailHelper.Command.Execute(item);
        }
    }
}
