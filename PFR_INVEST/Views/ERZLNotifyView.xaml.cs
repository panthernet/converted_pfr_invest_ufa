﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;


namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ERZLNotifyView.xaml
    /// </summary>
    public partial class ERZLNotifyView : UserControl
    {
        public ERZLNotifyView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
