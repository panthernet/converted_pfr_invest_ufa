﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using DevExpress.Xpf.Grid;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepClaimOfferView.xaml
    /// </summary>
    public partial class DepClaimOfferView : UserControl
    {
        private DepClaimOfferViewModel Model => DataContext as DepClaimOfferViewModel;

        public DepClaimOfferView()
        {
            InitializeComponent();
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Model.State == BusinessLogic.ViewModelState.Read)
                return;

            int rowHandle = tableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == DataControlBase.InvalidRowHandle) return;
            if (!Grid.IsGroupRowHandle(rowHandle))
            {
                var x = (OfferPfrBankAccountSum)Grid.GetRow(rowHandle);
                Model.EditSum(x);
            }
        }
    }
}
