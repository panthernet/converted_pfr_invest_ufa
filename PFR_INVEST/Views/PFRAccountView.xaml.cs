﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PFRAccountView.xaml
    /// </summary>
    public partial class PFRAccountView : UserControl
    {
        public PFRAccountView()
        {
            InitializeComponent();
            cboxacc.Items.Add("Активный");
            cboxacc.Items.Add("Закрыт");
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
