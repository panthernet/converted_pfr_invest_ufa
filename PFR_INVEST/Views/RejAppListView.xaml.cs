﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for BalanceView.xaml
	/// </summary>
	public partial class RejAppListView : UserControl
	{
        public RejAppListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "GridList");
        }

	    private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
			
			/*var item = Grid.CurrentItem as PPListItem;
			if(item != null)
				DashboardManager.Instance.OpenNewTab(typeof(CommonPPView), ViewModelState.Edit, item.ID,"Платежное поручение");*/
		}
	}
}
