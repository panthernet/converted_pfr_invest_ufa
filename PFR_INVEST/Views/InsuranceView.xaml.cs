﻿using System.Windows;
using PFR_INVEST.Helpers;
using PFR_INVEST.Views.Interface;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for InsuranceView.xaml
    /// </summary>
    public partial class InsuranceView : IAutoParentPanelSize
    {
        public InsuranceView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }

        #region IAutoParentPanelSize
        public double DesiredWidth { get { return 600; } }

        protected override Size MeasureOverride(Size constraint)
        {
            return DevExpressHelper.MeasureParentPanel(this, base.MeasureOverride, DesiredWidth);
        }
        #endregion
    }
}
