﻿using System;
using System.Collections.Generic;
using System.Windows;
using DevExpress.Xpf.Docking;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Views.LetterToNPFWizard;
using PFR_INVEST.Views.MonthTransferCreationWizard;
using PFR_INVEST.Views.PlanCorrectionWizard;
using PFR_INVEST.Views.PreferencesViews;
using PFR_INVEST.Views.TransferWizard;
using PFR_INVEST.Views.Wizards.DepositCountWizard;
using PFR_INVEST.Views.Wizards.OpfrTransferWizard;
using PFR_INVEST.Views.Wizards.SpnDeposit;
using PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard;

namespace PFR_INVEST.Views
{
	public sealed partial class DashboardManager
	{

		/// <summary>
		/// Файл для хранения размеров окон
		/// </summary>
		public const string FILE_SIZES_STORAGE = "winsizes.xml";
		/// <summary>
		/// Размер по умолчанию для лейаут окон
		/// </summary>
		public static Size DefaultLayoutWindowSize = new Size(823, 600);
		/// <summary>
		/// Минимальный размер по умолчанию для лейаут окон
		/// </summary>
		public static Size DefaultLayoutMinWindowSize = new Size(800, 600);
		/// <summary>
		/// Хранилище последних размеров окно: Тип окна, данные
		/// </summary>
		private static Dictionary<Type, SizeStorage> WinSizeDatabase { get; }
		private static readonly object Storagelock = new object();

		public class SizeStorage
		{
			/// <summary>
			/// Размер окна
			/// </summary>
			public Size Size { get; }
			/// <summary>
			/// Минимальный размер окна
			/// </summary>
			public Size MinSize { get; }
			/// <summary>
			/// Плавает ли окно
			/// </summary>
			public bool IsFloat { get; }
            public bool IsCenterParent { get; set; }
            public bool IsResizable { get; }

		    /// <summary>
		    /// Хранилище данных об окне
		    /// </summary>
		    /// <param name="size">Размер</param>
		    /// <param name="minsize">Мин. размер</param>
		    /// <param name="isfloat">Является ли окно всплывающим</param>
		    /// <param name="isResizable"></param>
		    public SizeStorage(Size size, Size minsize, bool isfloat, bool isResizable = true)
			{
				Size = size; MinSize = minsize; IsFloat = isfloat;
			    IsResizable = isResizable;
                IsCenterParent = true;
            }
        }

        #region Проверка на наличие типа в хранилище и получение структуры
        /// <summary>
        /// Получение структуры размера окна по типу
        /// </summary>
        /// <param name="type">Тип</param>
        public static SizeStorage GetSizeStorageItem(Type type)
		{
			lock (Storagelock)
			{
				return WinSizeDatabase.ContainsKey(type) ? WinSizeDatabase[type] : null;
			}
		}

		#endregion

		#region Добавление и обновление элемента в хранилище

	    /// <summary>
	    /// Добавить элемент в хранилище размеров окон
	    /// </summary>
	    /// <param name="type">Тип</param>
	    /// <param name="size">Размер</param>
	    /// <param name="minsize">Мин. размер окна</param>
	    /// <param name="isfloat">Является ли окно плавающим</param>
	    /// <param name="isResizable"></param>
	    public static SizeStorage AddSizeStorageItem(Type type, Size size, Size minsize, bool isfloat, bool isResizable = true)
		{
			lock (Storagelock)
			{
				var sz = GetSizeStorageItem(type);
				if (sz == null)
				{
					sz = new SizeStorage(size, minsize, isfloat, isResizable);
					WinSizeDatabase.Add(type, sz);
					return sz;
				}

				sz = new SizeStorage(size, minsize, isfloat);
				WinSizeDatabase[type] = sz;
				return sz;
			}
		}

		/// <summary>
		/// Добавить элемент в хранилище размеров окон c размером по умолчанию
		/// </summary>
		/// <param name="type">Тип</param>
		public static SizeStorage AddSizeStorageItem(Type type)
		{
			return AddSizeStorageItem(type, DefaultLayoutWindowSize, DefaultLayoutMinWindowSize, true);
		}

		/// <summary>
		/// Добавить элемент в хранилище размеров окон c размером по умолчанию
		/// </summary>
		/// <param name="type">Тип</param>
		/// <param name="isfloat">Является ли окно плавающим</param>
		public static SizeStorage AddSizeStorageItem(Type type, bool isfloat)
		{
			return AddSizeStorageItem(type, DefaultLayoutWindowSize, DefaultLayoutMinWindowSize, isfloat);
		}

		/// <summary>
		/// Обновление типа в хранилище по панели
		/// </summary>
		/// <param name="panel">Панель</param>
		public static void UpdateSizeStorageItem(LayoutPanel panel)
		{
			lock (Storagelock)
			{
				if (panel.Content == null) return;
				var type = panel.Content.GetType();

				if (WinSizeDatabase.ContainsKey(type))
					WinSizeDatabase[type] = new SizeStorage(panel.Parent.FloatSize, panel.Parent.ActualMinSize, panel.IsFloating);
				else WinSizeDatabase.Add(type, new SizeStorage(panel.Parent.FloatSize, panel.Parent.ActualMinSize, panel.IsFloating));
			}
		}

		#endregion

		#region Загрузка значений из файла
		/// <summary>
		/// Загрузка размеров из файла
		/// </summary>
		public static void LoadSizes()
		{
			lock (Storagelock)
			{
				//Загрузка умолчаний
				AddSizeStorageItem(typeof(PensionNotificationView), new Size(560, 290), new Size(560, 290), true);
				AddSizeStorageItem(typeof(DelayedPaymentClaimView), new Size(660, 520), new Size(420, 520), true);
                AddSizeStorageItem(typeof(CBReportManagerView), new Size(660, 520), new Size(420, 520), true);
                AddSizeStorageItem(typeof(BOReportForm1View), new Size(800, 600), new Size(420, 520), true);
                AddSizeStorageItem(typeof(OpfrTransferWizardView), new Size(600, 400), new Size(600, 400), true, false);
                AddSizeStorageItem(typeof(DepositCountWizardView), new Size(750, 400), new Size(750, 400), true, false);
                AddSizeStorageItem(typeof(SpnDepositWizardView), new Size(600, 600), new Size(600, 600), true, false);

                AddSizeStorageItem(typeof(StockListView), new Size(400, 200), new Size(400, 200), true);
                AddSizeStorageItem(typeof(StockView), new Size(400, 300), new Size(400, 300), true);
                

                AddSizeStorageItem(typeof(OpfrRegisterView), new Size(620, 520), new Size(420, 520), true);
                AddSizeStorageItem(typeof(OpfrTransferView), new Size(520, 430), new Size(520, 430), true);
                AddSizeStorageItem(typeof(OpfrListView), new Size(620, 330), new Size(420, 200), true);

                AddSizeStorageItem(typeof(SIAssignPaymentsListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(VRAssignPaymentsListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(BalanceView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(BankAccountsList), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(BankAccountView), new Size(515, 340), new Size(515, 340), true);
				AddSizeStorageItem(typeof(BankAgentView), new Size(690, 400), new Size(690, 400), true);
				AddSizeStorageItem(typeof(BankAgentsList), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(BanksList), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(BankView), new Size(690, 400), new Size(690, 400), true);
				AddSizeStorageItem(typeof(BranchView), new Size(450, 170), new Size(450, 170), true);
				AddSizeStorageItem(typeof(ContactView), new Size(450, 140), new Size(450, 140), true);
				AddSizeStorageItem(typeof(BudgetCorrectionView), new Size(660, 260), new Size(660, 260), true);
				AddSizeStorageItem(typeof(BudgetView), new Size(670, 450), new Size(670, 450), true);
				AddSizeStorageItem(typeof(BuyReportsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ClientThemesView), new Size(450, 150), new Size(450, 150), true);
				AddSizeStorageItem(typeof(NPFListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceControlListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceSIControlListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceNpfControlListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceSIListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceNpfListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CostsListView), new Size(580, 330), new Size(580, 300), true);
				AddSizeStorageItem(typeof(CostsView), new Size(670, 747), new Size(670, 522), true);
				AddSizeStorageItem(typeof(DBFSettingsView), new Size(600, 120), new Size(600, 120), true);
				AddSizeStorageItem(typeof(DeadZLEditView), new Size(420, 150), new Size(420, 150), true);
				AddSizeStorageItem(typeof(DeadZLListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DeadZLAddView), new Size(500, 400), new Size(400, 200), true);
				AddSizeStorageItem(typeof(DeleteDocumentsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SIDemandListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(VRDemandListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DemandView), new Size(820, 600), new Size(820, 600), true);
				AddSizeStorageItem(typeof(DepositsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DepositsListByPortfolioView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DepositsArchiveListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DepositView), new Size(800, 650), new Size(800, 620), true);

				AddSizeStorageItem(typeof(DirectionView), new Size(600, 485), new Size(660, 485), true);
				AddSizeStorageItem(typeof(MonthAccurateView), new Size(590, 570), new Size(590, 570), true);
				AddSizeStorageItem(typeof(QuarterAccurateView), new Size(610, 450), new Size(610, 450), true);
				AddSizeStorageItem(typeof(PenaltyView), new Size(600, 520), new Size(600, 520), true);
				AddSizeStorageItem(typeof(PrecisePenaltyView), new Size(600, 400), new Size(600, 400), true);
                //AddSS(typeof(DraftView), new Size(420, 280), new Size(420, 280), true);
                AddSizeStorageItem(typeof(DsvListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DueDeadView), new Size(575, 740), new Size(575, 430), true);
				AddSizeStorageItem(typeof(DueExcessView), new Size(575, 740), new Size(575, 430), true);
				AddSizeStorageItem(typeof(DueListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(DueUndistributedView), new Size(575, 740), new Size(575, 410), true);
				AddSizeStorageItem(typeof(DueView), new Size(570, 700), new Size(570, 700), true);
				AddSizeStorageItem(typeof(EnrollmentOtherView), new Size(630, 460), new Size(630, 490), true);
				AddSizeStorageItem(typeof(EnrollmentPercents), new Size(640, 435), new Size(640, 435), true);
				AddSizeStorageItem(typeof(ERZLNotifyView), new Size(630, 380), new Size(630, 380), true);
				AddSizeStorageItem(typeof(ExchangeRateView), new Size(900, 350), new Size(900, 350), true);
				AddSizeStorageItem(typeof(F40ListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(F50ListView), new Size(660, 300), new Size(420, 200), true);
                AddSizeStorageItem(typeof(F60ListView), new Size(660, 300), new Size(420, 200), true);
                AddSizeStorageItem(typeof(F70ListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(F80ListView), new Size(660, 300), new Size(420, 200), true);
                AddSizeStorageItem(typeof(F80SIListView), new Size(660, 300), new Size(420, 200), true);
                
				AddSizeStorageItem(typeof(F50View), new Size(830, 640), new Size(830, 640), true);
                AddSizeStorageItem(typeof(F60View), new Size(750, 640), new Size(750, 640), true);
                AddSizeStorageItem(typeof(F80View), new Size(600, 490), new Size(600, 490), true);
				AddSizeStorageItem(typeof(SIF402ListView), new Size(800, 330), new Size(800, 200), true);
				AddSizeStorageItem(typeof(VRF402ListView), new Size(800, 330), new Size(800, 200), true);
				AddSizeStorageItem(typeof(F402ListView), new Size(800, 330), new Size(800, 200), true);
				AddSizeStorageItem(typeof(F402View), new Size(800, 800), new Size(800, 800), true);
				AddSizeStorageItem(typeof(FastReportInputView), new Size(790, 400), new Size(790, 400), true);
				AddSizeStorageItem(typeof(FinRegisterView), new Size(500, 600), new Size(500, 600), true);

				AddSizeStorageItem(typeof(GivenFinRegisterView), new Size(420, 150), new Size(420, 150), true);
				AddSizeStorageItem(typeof(IncomeSecurityListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(IncomeSecurityView), new Size(510, 520), new Size(510, 520), true);
				AddSizeStorageItem(typeof(PaymentHistoryView), new Size(510, 520), new Size(510, 520), true);

				AddSizeStorageItem(typeof(InsuranceArchiveListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(InsuranceListView), new Size(620, 330), new Size(420, 200), true);
				//AddSizeStorageItem(typeof(InsuranceView), new Size(620, 420), new Size(620, 420), true);
				AddSizeStorageItem(typeof(INDateListView), new Size(800, 600), new Size(800, 600), true);
				AddSizeStorageItem(typeof(TDateListView), new Size(800, 600), new Size(800, 600), true);
				AddSizeStorageItem(typeof(BDateListView), new Size(800, 600), new Size(800, 600), true);
				AddSizeStorageItem(typeof(NPFView), new Size(850, 600), new Size(850, 550), true);
				AddSizeStorageItem(typeof(MarketCostListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(MarketPricesListView), new Size(620, 330), new Size(420, 200), true);
				//AddSizeStorageItem(typeof(MarketPriceView), new Size(580, 480), new Size(580, 480), true);
				AddSizeStorageItem(typeof(MarketCostScopeListView), new Size(676, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(NetWealthsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(NetWealthsSumListView), new Size(676, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(OrdersListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(OrderView), new Size(800, 750), new Size(800, 750), true);
				AddSizeStorageItem(typeof(OrderReportView), new Size(800, 650), new Size(800, 650), true);
				AddSizeStorageItem(typeof(TransferView), new Size(640, 550), new Size(640, 550), true);
				AddSizeStorageItem(typeof(ReqTransferView), new Size(600, 600), new Size(600, 600), true);
				AddSizeStorageItem(typeof(TransfersArchListView), new Size(750, 330), new Size(750, 200), true);
				AddSizeStorageItem(typeof(TransfersListView), new Size(750, 330), new Size(750, 200), true);
				AddSizeStorageItem(typeof(OwnFundsView), new Size(740, 600), new Size(740, 600), true);
				AddSizeStorageItem(typeof(OwnFundsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(PFRAccountView), new Size(500, 305), new Size(500, 305), true);
				AddSizeStorageItem(typeof(PFRAccountsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(PFRBranchesListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(PFRBranchView), new Size(600, 375), new Size(600, 375), true);
				//AUTO AddSizeStorageItem(typeof(PFRContactView), new Size(400, 370), new Size(400, 370), true);
				AddSizeStorageItem(typeof(PortfoliosListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(PortfolioStatusListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(PortfolioView), new Size(500, 400), new Size(500, 400), true);
				AddSizeStorageItem(typeof(PrepaymentView), new Size(575, 705), new Size(575, 430), true);
				AddSizeStorageItem(typeof(PrepaymentAccurate), new Size(575, 480), new Size(575, 440), true);
				AddSizeStorageItem(typeof(RatesListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(RateView), new Size(310, 180), new Size(310, 180), true);
				//AddSS(typeof(RecalcUKPortfoliosView), new Size(500, 200), new Size(500, 200), true);
				AddSizeStorageItem(typeof(RecalcBuyOrSaleReportsNKDView), new Size(500, 200), new Size(500, 200), true);
				AddSizeStorageItem(typeof(RegistersListView), new Size(980, 330), new Size(550, 200), true);
				AddSizeStorageItem(typeof(RegistersArchiveListView), new Size(980, 330), new Size(550, 200), true);
				AddSizeStorageItem(typeof(RegisterView), new Size(560, 660), new Size(560, 660), true);
				AddSizeStorageItem(typeof(ReorganizationView), new Size(460, 230), new Size(460, 230), true);
				AddSizeStorageItem(typeof(ReportsView), new Size(800, 600), new Size(445, 190), true);
				AddSizeStorageItem(typeof(ReturnView), new Size(580, 550), new Size(580, 550), true);
				AddSizeStorageItem(typeof(ADRolesListView), new Size(450, 250), new Size(350, 150), true);
				AddSizeStorageItem(typeof(SchilsCostsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SecuritiesListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SecurityView), new Size(570, 600), new Size(570, 600), true);
				AddSizeStorageItem(typeof(SaleReportsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ServerSettingsView), new Size(550, 395), new Size(550, 395), true);
				AddSizeStorageItem(typeof(CorrespondenceSIArchiveListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(CorrespondenceNpfArchiveListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SIContractsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(VRContractsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SIContractView), new Size(580, 560), new Size(580, 560), true);
                AddSizeStorageItem(typeof(DocumentSIView), new Size(560, 400), new Size(560, 400), true);
                AddSizeStorageItem(typeof(DocumentVRView), new Size(560, 400), new Size(560, 400), true);
				AddSizeStorageItem(typeof(DocumentNpfView), new Size(560, 400), new Size(560, 400), true);
				AddSizeStorageItem(typeof(LinkedDocumentView), new Size(500, 450), new Size(500, 450), true);
				AddSizeStorageItem(typeof(SIListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SIView), new Size(600, 620), new Size(600, 620), true);
                AddSizeStorageItem(typeof(SIVRRegisterView), new Size(560, 650), new Size(560, 650), true);
                AddSizeStorageItem(typeof(SIVRRegisterSIView), new Size(560, 650), new Size(560, 650), true);
                AddSizeStorageItem(typeof(SIVRRegisterVRView), new Size(560, 650), new Size(560, 650), true);
                AddSizeStorageItem(typeof(SPNMovementsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SPNMovementsUKListView), new Size(620, 450), new Size(420, 200), true);
				AddSizeStorageItem(typeof(SupAgreementView), new Size(560, 360), new Size(560, 360), true);
				AddSizeStorageItem(typeof(LoadedODKLogSuccessListView), new Size(895, 400), new Size(895, 400), true);
				AddSizeStorageItem(typeof(LoadedODKLogSuccessSIListView), new Size(885, 400), new Size(885, 400), true);
				AddSizeStorageItem(typeof(LoadedODKLogSuccessVRListView), new Size(885, 400), new Size(885, 400), true);
				AddSizeStorageItem(typeof(LoadedODKLogErrorListView), new Size(770, 400), new Size(770, 400), true);
				AddSizeStorageItem(typeof(TempAllocationListView), new Size(650, 330), new Size(650, 200), true);
				AddSizeStorageItem(typeof(TemplatesListView), new Size(740, 310), new Size(700, 310), true);
				//auto AddSizeStorageItem(typeof(TransferRequestView), new Size(320, 160), new Size(320, 160), true);
				AddSizeStorageItem(typeof(TransferSchilsView), new Size(580, 550), new Size(580, 550), true);
				AddSizeStorageItem(typeof(TransferToNPFListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(TransferFromNPFListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(TransferFromOrToNPFArchiveListView), new Size(580, 330), new Size(580, 300), true);
				AddSizeStorageItem(typeof(TransferToAccount), new Size(650, 705), new Size(650, 735), true);
				AddSizeStorageItem(typeof(TransferWizardView), new Size(650, 350), new Size(650, 350), true);
				AddSizeStorageItem(typeof(TransferSIWizardView), new Size(650, 350), new Size(650, 350), true);
				AddSizeStorageItem(typeof(TransferVRWizardView), new Size(650, 350), new Size(650, 350), true);
				AddSizeStorageItem(typeof(TransferSI8YearWizardView), new Size(650, 350), new Size(650, 350), true);
				AddSizeStorageItem(typeof(TransferVR8YearWizardView), new Size(650, 350), new Size(650, 350), true);
				AddSizeStorageItem(typeof(TransferVR13YearWizardView), new Size(650, 350), new Size(650, 350), true);
                AddSizeStorageItem(typeof(TransferSIRopsAsvWizardView), new Size(650, 450), new Size(650, 450), true);


                AddSizeStorageItem(typeof(SIPlanCorrectionWizardView), new Size(500, 310), new Size(500, 310), true);
				AddSizeStorageItem(typeof(VRPlanCorrectionWizardView), new Size(500, 310), new Size(500, 310), true);
				AddSizeStorageItem(typeof(SIMonthTransferCreationWizardView), new Size(500, 310), new Size(500, 310), true);
				AddSizeStorageItem(typeof(VRMonthTransferCreationWizardView), new Size(500, 310), new Size(500, 310), true);
				AddSizeStorageItem(typeof(LetterToNPFWizardView), new Size(450, 310), new Size(450, 310), true);
				AddSizeStorageItem(typeof(TreasurityView), new Size(660, 580), new Size(660, 580), true);
				AddSizeStorageItem(typeof(UKPaymentPlanView), new Size(700, 590), new Size(700, 590), true);
				AddSizeStorageItem(typeof(UKTransfersListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ADUserView), new Size(620, 400), new Size(520, 300), true);
				AddSizeStorageItem(typeof(ADUsersListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(UserStatisticChoose), new Size(700, 550), new Size(700, 550), true);
				AddSizeStorageItem(typeof(ViolationCategoriesListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ViolationsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ViolationCategoryView), new Size(600, 245), new Size(600, 245), true);
				AddSizeStorageItem(typeof(WBISettingsView), new Size(600, 175), new Size(600, 175), true);
				AddSizeStorageItem(typeof(WithdrawalView), new Size(630, 595), new Size(630, 595), true);
				AddSizeStorageItem(typeof(YearPlanView), new Size(600, 570), new Size(600, 570), true);
				AddSizeStorageItem(typeof(YieldsListView), new Size(620, 530), new Size(420, 200), true);
				AddSizeStorageItem(typeof(YieldView), new Size(800, 800), new Size(800, 800), true);
				AddSizeStorageItem(typeof(ZLRedistListView), new Size(700, 420), new Size(600, 350), true);
				AddSizeStorageItem(typeof(ZLMovementsListView), new Size(620, 330), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ZLRedistView), new Size(800, 400), new Size(800, 400), true);
				
				AddSizeStorageItem(typeof(RatingsListView), new Size(330, 300), new Size(140, 200), true);
				AddSizeStorageItem(typeof(RatingView), new Size(515, 120), new Size(515, 120), true);
				AddSizeStorageItem(typeof(RatingAgenciesListView), new Size(500, 330), new Size(500, 330), true);
				AddSizeStorageItem(typeof(RatingAgencyView), new Size(510, 115), new Size(510, 115), true);
				AddSizeStorageItem(typeof(BankConclusionView), new Size(610, 270), new Size(610, 270), true);
				AddSizeStorageItem(typeof(DivisionView), new Size(610, 250), new Size(610, 250), true);
				AddSizeStorageItem(typeof(PersonView), new Size(610, 311), new Size(610, 311), true);
				AddSizeStorageItem(typeof(NPFChiefView), new Size(800, 295), new Size(800, 295), true);
				AddSizeStorageItem(typeof(DepClaimOfferView), new Size(800, 800), new Size(800, 295), true);
				AddSizeStorageItem(typeof(KBKView), new Size(530, 310), new Size(530, 200), true);
				AddSizeStorageItem(typeof(KBKListView), new Size(510, 510), new Size(510, 510), true);
				AddSizeStorageItem(typeof(AgencyRatingView), new Size(630, 165), new Size(630, 165), true);
				AddSizeStorageItem(typeof(PaymentDetailView), new Size(550, 135), new Size(550, 135), true);
				AddSizeStorageItem(typeof(CommonPPView), new Size(600, 580), new Size(600, 320), true);
				AddSizeStorageItem(typeof(CommonPPSplitView), new Size(600, 450), new Size(600, 450), true);
                
				AddSizeStorageItem(typeof(NPFCodeView), new Size(530, 310), new Size(530, 200), true);
				AddSizeStorageItem(typeof(NPFCodeListView), new Size(510, 510), new Size(510, 510), true);
				AddSizeStorageItem(typeof(SPNAllocationView), new Size(560, 660), new Size(560, 660), true);
				AddSizeStorageItem(typeof(SPNReturnView), new Size(560, 660), new Size(560, 660), true);
				AddSizeStorageItem(typeof(AllocationNPFView), new Size(500, 600), new Size(500, 600), true);
				AddSizeStorageItem(typeof(RecalcUKSIPortfoliosView), new Size(410, 105), new Size(410, 100), true);
				AddSizeStorageItem(typeof(RecalcUKVRPortfoliosView), new Size(410, 105), new Size(410, 100), true);
				AddSizeStorageItem(typeof(ROPSSumListView), new Size(660, 300), new Size(420, 200), true);
				AddSizeStorageItem(typeof(ROPSSumView), new Size(660, 200), new Size(420, 200), true);
			    AddSizeStorageItem(typeof (BoardView), new Size(350, 100), new Size(350,100), true);
                AddSizeStorageItem(typeof(XMLImportView), new Size(407, 130), new Size(407, 130), true);
                //AddSS(typeof(LoginMessagesDlg), new Size(630, 250), new Size(550, 250), true);

            }
        }
		#endregion

	    private SizeStorage ProcessCustomViewSizes(SizeStorage sz, Type type, object param)
	    {
	       /* if (type == typeof(QuarterAccurateView))
	        {
		        if ((param + "") != DueDocKindIdentifier.DueDSVAccurateYear)
		        {
					// Значит форма открылась в режиме СВ
					sz = new SizeStorage(new Size(480, 410), new Size(480, 410), sz.IsFloat);
		        }
	        } */
			if (type == typeof (MonthAccurateView))
			{
				if ((param + "") != DueDocKindIdentifier.DueDSVAccurate)
				{
					// Значит форма открылась в режиме СВ
					sz = new SizeStorage(new Size(550, 415), new Size(550, 415), sz.IsFloat);
				}
			}

		    return sz;
	    }
	}
}
