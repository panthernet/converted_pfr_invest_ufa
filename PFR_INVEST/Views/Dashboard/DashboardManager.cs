﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Docking.Base;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Layout.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.UKReport;
using PFR_INVEST.Helpers;
using PFR_INVEST.Tools;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Views.Interface;
using PFR_INVEST.Views.LetterToNPFWizard;
using PFR_INVEST.Views.MonthTransferCreationWizard;
using PFR_INVEST.Views.PlanCorrectionWizard;
using PFR_INVEST.Views.TransferWizard;
using Binding = System.Windows.Data.Binding;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using UserControl = System.Windows.Controls.UserControl;

namespace PFR_INVEST.Views
{
    public sealed partial class DashboardManager
    {
        /// <summary>
        /// Статичная копия менеджера
        /// </summary>
        public static DashboardManager Instance { get; private set; }
        /// <summary>
        /// Менеджер раскладки окон приложения
        /// </summary>
        public DockLayoutManager DockManager { get; }

        private Action<ViewModelBase> OnActivate { get; }

        public DashboardManager(DockLayoutManager dockManager, Action<ViewModelBase> onActivate = null)
        {
            OnActivate = onActivate;


            DockManager = dockManager;
            DockManager.ClosingBehavior = ClosingBehavior.ImmediatelyRemove;
            DockManager.DockItemClosing += DockItemClosing;
            DockManager.DockItemClosed += DockItemClosed;
            DockManager.DockItemActivated += DockManager_DockItemActivated;

            ViewModelBase.DialogHelper = new DialogHelper();
            ViewModelBase.BusyHelper = new BusyHelper();
            ViewModelBase.ViewModelManager = new ViewModelManager();
            ViewModelBase.Logger = App.log;
            ViewModelBase.DoNotThrowExceptionOnError = true;
            ViewModelBase.GetViewModel = FindViewModel;
            ViewModelBase.GetViewModelsList = FindViewModelsList;
            ViewModelBase.OnGetDataError += ViewModel_OnGetDataError;
            ViewModelBase.OnStartedPrinting += ViewModel_OnStartedPrinting;
            ViewModelBase.OnFinishedPrinting += ViewModel_OnFinishedPrinting;
            ViewModelBase.OnErrorLoadingTemplate += ViewModel_OnErrorLoadingTemplate;
            ViewModelCard.OnSaveCardError += ViewModelCard_OnSaveCardError;
            ViewModelCard.RefreshListViewModels = RefreshListViewModels;
            UKReportImportHandlerBase.GetViewModelsList = FindViewModelsList;

            Instance = this;
        }

        private void DockManager_DockItemActivated(object sender, DockItemActivatedEventArgs ea)
        {
            OnActivate?.Invoke(ea.Item.DataContext as ViewModelBase);
        }

        static DashboardManager()
        {
            WinSizeDatabase = new Dictionary<Type, SizeStorage>();
            LoadSizes();
        }

        #region Get list view model by view model
        private static ViewModelBase FindViewModelInGroups(BaseLayoutItem[] items, Type vmListType)
        {
            var list = FindViewModelsListInGroups(items, vmListType);
            return list?.FirstOrDefault();
        }

        private static List<ViewModelBase> FindViewModelsListInGroups(BaseLayoutItem[] items, Type vmListType)
        {
            var list = new List<ViewModelBase>();
            foreach (var item in items.OfType<LayoutPanel>())
            {
                var activePanel = item;// as LayoutPanel;
                var window = activePanel.Content as UserControl;
                if (window == null) continue;
                if (vmListType.IsInterface)
                {
                    if (vmListType.IsInstanceOfType(window.DataContext))
                        list.Add(window.DataContext as ViewModelBase);
                }
                else
                {
                    if (window.DataContext.GetType() == vmListType)
                        list.Add(window.DataContext as ViewModelBase);
                }
            }

            return list.Count == 0 ? null : list;
        }


        public T FindViewModel<T>() where T : ViewModelBase
        {
            return FindViewModel(typeof(T)) as T;
        }

        public ViewModelBase FindViewModel(Type vmType)
        {
            if (DockManager.LayoutRoot == null || vmType == null) return null;

            var result = FindViewModelInGroups(DockManager.GetItems(), vmType);
            return result;
        }

        public List<LayoutPanel> FindPanels<T>() where T : UserControl
        {
            var rootItems = FindPanels<T>(DockManager.LayoutRoot.Items);
            var floatItems = FindPanels<T>(DockManager.FloatGroups.GetItems());
            var hideItems = FindPanels<T>(DockManager.AutoHideGroups.GetItems());

            var allItems = new List<LayoutPanel>();
            allItems.AddRange(rootItems);
            allItems.AddRange(floatItems);
            allItems.AddRange(hideItems);

            return allItems;
        }

        private static IEnumerable<LayoutPanel> FindPanels<T>(IEnumerable<BaseLayoutItem> items) where T : UserControl
        {
            var panels = new List<LayoutPanel>();
            foreach (var item in items)
            {
                var panel = item as LayoutPanel;
                var view = panel?.Content as T;
                if (view == null)
                    continue;

                panels.Add(panel);
            }
            return panels;
        }

        public List<T> FindViewModelsList<T>() //where T : ViewModelBase
        {
            var list = FindViewModelsList(typeof(T));
            return (list ?? new List<ViewModelBase>()).Cast<T>().ToList();
        }

        public List<ViewModelBase> FindViewModelsList(Type vmType)
        {
            if (DockManager.LayoutRoot == null || vmType == null) return null;
            var retVal = new List<ViewModelBase>();
            var result = FindViewModelsListInGroups(DockManager.GetItems(), vmType);
            if (result != null)
                retVal.AddRange(result);

            return retVal;
        }

        public void RefreshListViewModels(IEnumerable<Type> viewModelListTypes)
        {
            foreach (var type in viewModelListTypes)
            {
                if (type == null) continue;

                var models = FindViewModelsList(type);
                if (models == null)
                    continue;

                foreach (var m in models)
                {
                    var viewModel = m as IRefreshableViewModel;
                    if (viewModel != null)
                        viewModel.RefreshModel();
                    else
                    {
                        var model = m as ViewModelList;
                        model?.RefreshList?.Execute(model.RefreshParameter);
                    }
                }
            }
        }
        #endregion

        #region Opening new tab implementation

        #region CheckIfOpen() CheckPanelList()

        private bool CheckIfOpen(Type formType, long recordId, object param = null)
        {
            // определение открытой формы по свойству ID из param, если recordId не задана
            if (recordId < 1 && param != null)
            {
                var prop = param.GetType().GetProperty("ID");
                if (prop != null)
                {
                    var id = (long?)prop.GetValue(param, null);
                    if (id.HasValue && id.Value > 0)
                        recordId = id.Value;
                }
            }

            // тут определяем есть ли уже такая открытая вкладка
            bool present;
            var compareId = !formType.ToString().Contains("List") && formType.Name != "ServerSettingsView";

            foreach (var group in DockManager.AutoHideGroups) // среди скрытых вкладок
            {
                present = CheckPanelList(group.Items, compareId, recordId, formType);
                if (present) return true;
            }

            foreach (var group in DockManager.FloatGroups) // среди не развернутых вкладок
            {
                present = CheckPanelList(@group.ItemsInternal, compareId, recordId, formType);
                if (present) return true;
            }

            present = CheckPanelList(DockManager.LayoutRoot.Items, compareId, recordId, formType);

            return present;
        }

        private bool CheckPanelList(IEnumerable items, bool compareId, long recordId, Type formType)
        {
            bool present = false;
            foreach (var item in items)
            {
                if (item.GetType() == typeof(LayoutPanel))
                {
                    if (compareId)
                    {
                        if (((ViewModelBase)((UserControl)((LayoutPanel)item).Content).DataContext).ID != recordId)
                        {
                            continue;
                        }
                    }
                    if (((LayoutPanel)item).Content.GetType() != formType) continue;
                    present = true;

                    var baseLayoutItem = item as BaseLayoutItem;
                    DockManager.BringToFront(baseLayoutItem);
                    DockManager.LayoutController.Activate(baseLayoutItem);
                    break;
                }

                if (item.GetType() == typeof(TabbedGroup))
                    present = CheckPanelList(((TabbedGroup)item).Items, compareId, recordId, formType);
                //do nothing
            }

            return present;
        }
        #endregion

        #region OpenNewTab() - получение параметров запуска окна


        /// <summary>
        /// Parameterless window opener
        /// </summary>
        /// <param name="viewType">Type</param>
        /// <param name="action">Action</param>
        /// <param name="caption">Caption</param>
        /// <param name="newCaption">use when creating entity, it's the entity type (caption window)</param>
        public void OpenNewTab(Type viewType, ViewModelState action, string caption, string newCaption = null)
        {
            OpenNewTab(viewType, action, -1, caption, newCaption);
        }



        public void OpenNewTab(Type viewType, ViewModelState action, object param, string caption, string newCaption = null)
        {
            OpenNewTab(viewType, action, -1, param, caption, newCaption);
        }

        /// <summary>
        /// Parameterless window opener
        /// </summary>
        /// <param name="viewType">Type</param>
        /// <param name="action">Action</param>
        /// <param name="recordId">Record ID</param>
        /// <param name="caption">Caption</param>
        /// <param name="newCaption">use when creating entity, it's the entity type (caption window)</param>
        public void OpenNewTab(Type viewType, ViewModelState action, long recordId, string caption, string newCaption = null)
        {
            OpenNewTab(viewType, action, recordId, null, caption, newCaption);
        }

        /// <summary>
        /// Parameterized window opener
        /// </summary>
        /// <param name="viewType">Type</param>
        /// <param name="action">Action</param>
        /// <param name="recordId">Record ID</param>
        /// <param name="param">Parameter</param>
        /// <param name="caption">Caption</param>
        /// <param name="newCaption">use when creating entity, it's the entity type (caption window)</param>
        public void OpenNewTab(Type viewType, ViewModelState action, long recordId, object param, string caption, string newCaption = null)
        {
            using (Loading.StartNew())
            {
                ViewModelBase newViewModel = null;
                try
                {
                    if (action != ViewModelState.Create && CheckIfOpen(viewType, recordId, param))
                        return;
                    UserControl newWindow;
                    PrepareModels(viewType, action, recordId, param, caption, out newWindow, out newViewModel);

                    var card = newViewModel as ViewModelCard;
                    if (card != null)
                        card.NewCaption = newCaption;

                    if (newViewModel.IsOnlyOneInstanceAllowed)
                    {
                        if (FindViewModel(newViewModel.GetType()) != null)
                            return;
                    }

                    if (IsModelContainCorrectData(newViewModel))
                        OpenNewTab(caption, newWindow, action, newViewModel, param);
                }
                catch (ViewModelInitializeException)
                {
                    ViewModel_OnGetDataError(newViewModel, null);
                }
                catch (ViewModelReadAccessException)
                {
                    DXMessageBox.Show(Properties.Resources.AccessDeniedErrorString, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (Exception ex)
                {
                    DXMessageBox.Show(Properties.Resources.UnknownCardOpenErrorString, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);

                    App.log.WriteException(ex);
                }
            }
        }
        #endregion

        private static void ViewModelCard_OnSaveCardError(object sender, EventArgs e)
        {
            DXMessageBox.Show(Properties.Resources.SaveDataErrorString, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private static void ViewModel_OnStartedPrinting(object sender, PrintingEventArgs e)
        {
            Loading.ShowWindow(e.Message ?? string.Empty);
        }

        private static void ViewModel_OnFinishedPrinting(object sender, EventArgs e)
        {
            Loading.CloseWindow();
        }

        private static void ViewModel_OnErrorLoadingTemplate(object sender, ErrorLoadingTemplateEventArgs e)
        {
            Loading.CloseWindow();
            var message = string.Format(Properties.Resources.LoadingTemplateErrorString, e.TemplateName);
            DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private static void ViewModel_OnGetDataError(object sender, EventArgs e)
        {
            DXMessageBox.Show(Properties.Resources.GetDataErrorString, Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #region OpenNewTab() - открытие окна
        /// <summary>
        /// Открыть новое окно
        /// </summary>
        /// <typeparam name="TView">Тип вьюхи</typeparam>
        /// <typeparam name="TModel">Тип модели</typeparam>
        /// <param name="caption">Заголовок</param>
        /// <param name="action">Тип действия: чтение, редактирование, создание</param>
        /// <param name="modelParams">Параметры конструктора модели</param>
        public void OpenNewTab<TView, TModel>(string caption, ViewModelState action, params object[] modelParams)
            where TView : UserControl
            where TModel : ViewModelBase
        {
            if (action != ViewModelState.Create && CheckIfOpen(typeof(TView), modelParams.FirstOrDefault() as long? ?? -1))
            {
                return;
            }

            using(Loading.StartNew())
            {
                var view = (TView)Activator.CreateInstance(typeof(TView));
                var model = (TModel)Activator.CreateInstance(typeof(TModel), modelParams);
                OpenNewTab(caption, view, action, model);
            }
        }

        /// <summary>
        /// Открыть новое окно
        /// </summary>
        /// <typeparam name="TView">Тип вьюхи</typeparam>
        /// <typeparam name="TModel">Тип модели</typeparam>
        /// <param name="caption">Заголовок</param>
        /// <param name="action">Тип действия: чтение, редактирование, создание</param>
        public void OpenNewTab<TView, TModel>(string caption, ViewModelState action)
            where TView : UserControl
            where TModel : ViewModelBase
        {
            if (action != ViewModelState.Create && CheckIfOpen(typeof(TView), -1))
            {
                return;
            }

            using(Loading.StartNew())
            {
                var view = (TView)Activator.CreateInstance(typeof(TView));
                var model = (TModel)Activator.CreateInstance(typeof(TModel));
                OpenNewTab(caption, view, action, model);
            }
        }

        /// <summary>
        /// Открыть новое окно
        /// </summary>
        /// <param name="caption">Заголовок</param>
        /// <param name="newWindow">Содержимое окна</param>
        /// <param name="action">Тип действия: чтение, редактирование, создание</param>
        /// <param name="newViewModel">Модель данных</param>
        /// <param name="param"></param>
        public void OpenNewTab(string caption, UserControl newWindow, ViewModelState action, ViewModelBase newViewModel, object param = null)
        {
            var vmList = newViewModel as ViewModelList;
            if (vmList != null)
                vmList.AllowBusyIndicator = true;

            if (newViewModel == null) return;
            newViewModel.State = action;
            newViewModel.LogCaption = caption;
            newWindow.DataContext = newViewModel;
            newWindow.Loaded += newWindow_Loaded;

            string actname;
            switch (newViewModel.State)
            {
                case ViewModelState.Create:
                    actname = "Создание";
                    break;
                case ViewModelState.Edit:
                    actname = "Редактирование";
                    break;
                case ViewModelState.Read:
                    actname = "Чтение";
                    break;
                default:
                    throw new Exception("Неизвестное состояние модели!");
            }

            newWindow.PreviewMouseWheel += newWindow_PreviewMouseWheel;

            //загрузка размера из базы и создание панели
            var sz = GetSizeStorageItem(newWindow.GetType()) ?? AddSizeStorageItem(newWindow.GetType());

            sz = ProcessCustomViewSizes(sz, newWindow.GetType(), param);

            //загрузка размера с помощью сервиса
            Size size; Point point; bool isFloat;
            GetUserSettings(newWindow, out size, out point, out isFloat);

            var isAutoSize = newWindow is IAutoParentPanelSize;
            var asControl = newWindow as IAutoParentPanelSize;

            if (size == Size.Empty)
                size = isAutoSize ? new Size(asControl.DesiredWidth < 0 ? 0 : asControl.DesiredWidth, 0) : sz.Size;

            if (point.Equals(new Point())) point = CustomizePosition(newViewModel, size);//new Point(10, 10);
            var tName = newWindow.GetType().Name;
            MainWindow.DebugPrintText(string.Format("{0} ({1}) [{2}]", tName, actname, newViewModel.ID, size), tName);
            MainWindow.DebugPrintWindowSize(size);


            //создание панели, учитывая нужные размеры
            LayoutPanel panel;
            //if (!(newWindow is ReportsView)) 
            var expandList = false;
            if (newViewModel is ViewModelList && DockManager.LayoutRoot.Items.Count == 0 && DockManager.FloatGroups.Count == 0)
            {
                expandList = true;
                panel = new LayoutPanel { FloatSize = size };
            }
            else
                panel = DockManager.DockController.AddPanel(point, size);

            panel.Content = newWindow;
            newWindow.IsVisibleChanged += newWindow_IsVisibleChanged;
            newWindow.Tag = panel;


            if (!isAutoSize)
            {
                panel.MinWidth = sz.MinSize.Width;
                panel.MinHeight = sz.MinSize.Height;

                if (!sz.IsResizable)
                {
                    panel.MaxWidth = sz.MinSize.Width;
                    panel.MaxHeight = sz.MinSize.Height;
                    panel.ShowMaximizeButton = false;
                    panel.ShowPinButton = false;
                    panel.ShowRestoreButton = false;

                }
            }

            //ручная установка названия формы для некоторых форм
            if (newViewModel.GetType() == typeof(NPFViewModel))
            {
                string name = ((NPFViewModel)newViewModel).LegalEntity.FormalizedName;
                panel.Caption = string.IsNullOrEmpty(name) ? caption : name;
            }
            else panel.Caption = caption;

            //newWindow is ReportsView
            if (!isFloat)
            {
                //if (newWindow is ReportsView) panel.AllowFloat = false;
                DockManager.LayoutRoot.Items.Add(panel);
            }

            if (newWindow is TransferWizardView || newWindow is MonthTransferCreationWizardView || newWindow is PlanCorrectionWizardView || newWindow is LetterToNPFWizardView)
            {
                panel.AllowDock = false;
                panel.AllowHide = false;
                panel.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            }

            if (expandList)
                DockManager.LayoutRoot.Items.Add(panel);

            //активация настроек и панели			
            //Tools.ThemesTool.SetCurrentTheme(newWindow);

            //передаётся имя панели в модель, нужно для логирования
            newViewModel.PostOpen((string)panel.Caption);

            //Делаем биндинг подписи окна на Header модели
            panel.DataContext = newViewModel;
            panel.SetBinding(BaseLayoutItem.CaptionProperty, new Binding("Header"));


            DockManager.Activate(panel);

            DockManager.BringToFront(panel);

            //newWindow.Loaded += OnPanelLoaded;
            newWindow.Unloaded += OnPanelUnloaded;
            if (AppSettings.DisplayWindowClassName)
                newWindow.SizeChanged += newWindow_SizeChanged;
        }

        private Point CustomizePosition(ViewModelBase newViewModel, Size size)
        {
            if (newViewModel is CommonPPViewModel)
            {
                var maxWidth = MainWindow.Instance.ActualWidth;
                var panelWidth = size.Width;
                return new Point(maxWidth * .5 - panelWidth * .5, 0);
            }
            return new Point((Screen.PrimaryScreen.WorkingArea.Width - size.Width) / 2,
                (Screen.PrimaryScreen.WorkingArea.Height - (size.Height > 240 ? size.Height * 1.5  : size.Height > 170 ? size.Height * 2 : size.Height * 3)) / 2);
            //return new Point(10, 10);
        }

        private void newWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainWindow.DebugPrintWindowSize(e.NewSize);
        }

        /*private void OnPanelLoaded(object sender, RoutedEventArgs e)
		{
		}*/

        private void OnPanelUnloaded(object sender, RoutedEventArgs e)
        {
            //Логика для вкл/выкл загрузки по частям, если окно закрыли/перенесли
            if ((sender as UserControl)?.DataContext is IPagedLoadListModel)
                ((IPagedLoadListModel)((UserControl)sender).DataContext).IsClosed = true;

#warning HACK PGorbunov PFR00002014 После закрытия карточки "Поручения" автоматически переходим на ПЕРВУЮ ПОПАВШУЮСЯ карточку "Список поручений"

            var reqTransferView = sender as ReqTransferView;
            if (reqTransferView != null)
            {
                var reqTransfer = (ReqTransferViewModel)reqTransferView.DataContext;
                var panels = FindPanels<TransferView>();
                foreach (var panel in panels)
                {
                    var view = (TransferView)panel.Content;
                    var transfer = (TransferViewModel)view.DataContext;
                    if (transfer.TransferRegister.ID == reqTransfer.TransferRegister.ID)
                    {
                        DockManager.Activate(panel);
                        DockManager.BringToFront(panel);
                        break;
                    }
                }
            }

            var prepaymentAccurateView = sender as PrepaymentAccurate;
            var prepaymentAccurate = (PrepaymentAccurateViewModel) prepaymentAccurateView?.DataContext;
            if (prepaymentAccurate?.Dopaps != null)
            {
                var panels = FindPanels<PrepaymentView>();
                foreach (var panel in panels)
                {
                    var view = (PrepaymentView)panel.Content;
                    var prep = (PrepaymentViewModel)view.DataContext;
                    foreach (var dopaps in prep.DopAPSs)
                    {
                        if (prepaymentAccurate.Dopaps.ApsID != dopaps.ApsID) continue;
                        DockManager.Activate(panel);
                        DockManager.BringToFront(panel);
                        break;
                    }
                }
            }
            //2042
            var finRegisterView = sender as FinRegisterView;
            if (finRegisterView != null)
            {
                var finRegisterVm = (FinRegisterViewModel)finRegisterView.DataContext;
                var panels = FindPanels<RegisterView>();
                foreach (var panel in panels)
                {
                    var view = (RegisterView)panel.Content;
                    var register = (RegisterViewModel)view.DataContext;
                    foreach (var finreg in register.FinregWithNPFList)
                    {
                        if (finRegisterVm.FinregisterID != finreg.Finregister.ID) continue;
                        DockManager.Activate(panel);
                        DockManager.BringToFront(panel);
                        break;
                    }
                }
            }

            //очищаем модели после закрытия если требуется
            ((sender as UserControl)?.DataContext as IDisposable)?.Dispose();
        }

        private void newWindow_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl)) return;
            var vm = GetActiveViewModel() as ViewModelCard;
            vm?.Scale(1.0f + (double)e.Delta / 3000.0f);
        }

        private void newWindow_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (((UserControl)sender).Visibility == Visibility.Visible) return;
            var tag = ((UserControl)sender).Tag as LayoutPanel;
            if (tag != null)
            {
                try
                {
                    DockManager.DockController.Close(tag);
                }
                catch
                {
                    // ignored
                }
            }
        }


        private static void newWindow_Loaded(object sender, RoutedEventArgs e)
        {
            #region PanelLoaded
            //Логика для вкл/выкл загрузки по частям, если окно закрыли/перенесли
            if ((sender as UserControl)?.DataContext is IPagedLoadListModel)
                ((IPagedLoadListModel)((UserControl)sender).DataContext).IsClosed = false;
            #endregion

            var window = (UserControl)sender;
            var userSetting = App.UserSettingsManager.GetUserSetting(ContainerType.View, window.GetType().Name);

            if (userSetting == null)
                return;

            var children = Util.FindVisualChildren<GridControl>(window);

            foreach (var grid in children)
            {
                if (!userSetting.GridSettingsList.ContainsKey(grid.Name)) continue;
                var value = userSetting.GridSettingsList[grid.Name];
                var buffer = Encoding.UTF8.GetBytes(value);

                using (var memoryStream = new MemoryStream(buffer.Length))
                {
                    foreach (var bufferByte in buffer)
                        memoryStream.WriteByte(bufferByte);

                    memoryStream.Seek(0, SeekOrigin.Begin);
                    grid.RestoreLayoutFromStream(memoryStream);
                }
            }
            var card = window.DataContext as ViewModelCard;
            if (card != null)
            {
                card.ScaleX = userSetting.ScaleX;
                card.ScaleY = userSetting.ScaleY;
            }
            Loading.CloseWindow();
        }

        #endregion
        #endregion

        #region DockItemClosing(), DockItemClosed()

        private void DockItemClosing(object sender, ItemCancelEventArgs e)
        {
            DockManager.Activate(e.Item);
            //Size debuggging
            e.Cancel = false;
            var vm = GetActiveViewModel();
            var wm = vm as IWizardExModel;

            if (wm != null && wm.Step != 0)
            //старые пока опускаем, нужна спец логика
            {
                if (!ViewModelBase.DialogHelper.ShowConfirmation("Вы уверены?", "Закрыть мастер"))
                {
                    e.Cancel = true;
                    return;
                }
                wm.IsClosed = true;
            }
            var model = vm as ViewModelCard;

            if (model != null)
            {
                SaveUI(e);
                bool res = model.IsDataChanged;
                MainWindow.Instance.InvalidateRibbon();
                if (model.IsDataChanged != res) model.IsDataChanged = res;

                //if (!model.IsDataChanged)
                if (!model.SaveCard.CanExecute(null))
                    return;

                if (model.Deleting || model.IsNotSavable)
                    return;

                var result = MessageBoxResult.No;
                if(res)
                    result = DXMessageBox.Show(Properties.Resources.CardChangedMessageString,
                                               Properties.Resources.ApplicationName,
                                               MessageBoxButton.YesNoCancel,
                                               MessageBoxImage.Question);

                switch (result)
                {
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.No:
                        break;
                    case MessageBoxResult.Yes:
                        if (model.SaveCard.CanExecute(null))
                            model.SaveCard.Execute(null);
                        else
                        {
                            string message = Properties.Resources.CardSaveValidationError;
                            var savingResult = DXMessageBox.Show(message,
                                                                 Properties.Resources.ApplicationName,
                                                                 MessageBoxButton.YesNo,
                                                                 MessageBoxImage.Error);
                            if (savingResult == MessageBoxResult.Yes)
                                e.Cancel = true;
                        }
                        break;
                }
            }
            if (e.Cancel)
            {
                model?.RaiseBeforeCloseSaveCancelled();
                return;
            }

            SaveUI(e);

            MainWindow.Instance.InvalidateRibbon();
        }

        private void SaveUI(ItemEventArgs e)
        {
            switch (e.Item.ItemType)
            {
                case LayoutItemType.Panel:
                    {
                        var panel = e.Item as LayoutPanel;
                        SaveUserSettings(panel);
                    }
                    break;
                case LayoutItemType.FloatGroup:
                    var floatGroup = e.Item as FloatGroup;
                    foreach (var layoutBaseItem in floatGroup?.Items)
                    {
                        if (layoutBaseItem is LayoutPanel)
                            SaveUserSettings(layoutBaseItem as LayoutPanel);

                        if (layoutBaseItem is LayoutGroup)
                            SaveUserSettingsRecursively(layoutBaseItem as LayoutGroup);
                    }
                    break;
                case LayoutItemType.AutoHideGroup:
                case LayoutItemType.Group:
                case LayoutItemType.TabPanelGroup:
                case LayoutItemType.Document:
                case LayoutItemType.DocumentPanelGroup:
                case LayoutItemType.AutoHideContainer:
                case LayoutItemType.AutoHidePanel:
                case LayoutItemType.ControlItem:
                case LayoutItemType.Splitter:
                    break;
            }
        }

        private void DockItemClosed(object sender, ItemEventArgs e)
        {
            var document = e.Item as LayoutPanel;
            if (document != null)
            {
                var uc = document.Content as UserControl;
                //отписка для GridRefreshHelper
                ModelInteractionHelper.UnsubscribeForGridRefreshHelper(uc);

                if (uc != null)
                {
                    uc.IsVisibleChanged -= newWindow_IsVisibleChanged;
                    uc.PreviewMouseWheel -= newWindow_PreviewMouseWheel;
                    uc.Loaded -= newWindow_Loaded;
                }
                DockManager.ActiveDockItem = null;
                DockManager.ActiveLayoutItem = null;
            }

            GC.Collect(2, GCCollectionMode.Forced);
        }
        #endregion

        #region Saving...
        private static void SaveUserSettings(ContentItem panel)
        {
            if (panel?.Content == null) return;
            var setting = new UserSetting
            {
                PanelName = panel.Content.GetType().Name,
                PanelType = ContainerType.View,
                ScaleX = (((UserControl) panel.Content).DataContext as ViewModelCard)?.ScaleX ?? 1.0,
                ScaleY = (((UserControl) panel.Content).DataContext as ViewModelCard)?.ScaleY ?? 1.0,
                Size = panel.Parent is FloatGroup ? panel.Parent.FloatSize : Size.Empty,
                Point = (panel.Parent as FloatGroup)?.FloatLocation ?? new Point(),
            };

            var children = Util.FindVisualChildren<GridControl>((UserControl)panel.Content);

            foreach (var grid in children)
            {
                using (var memoryStream = new MemoryStream())
                {
                    grid.SaveLayoutToStream(memoryStream);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    using (var sr = new StreamReader(memoryStream))
                    {
                        var gridData = sr.ReadToEnd();
                        if (grid.Name.Trim() != string.Empty && !setting.GridSettingsList.ContainsKey(grid.Name))
                            setting.GridSettingsList.Add(grid.Name, gridData);
                    }
                }
            }

            if (setting.GridSettingsList.Count > 0)
                App.UserSettingsManager.AppendSetting(setting);
        }

        private static void GetUserSettings(UserControl window, out Size size, out Point point, out bool isFloat)
        {
            size = Size.Empty;
            point = new Point();
            isFloat = true;
            var userSetting = App.UserSettingsManager.GetUserSetting(ContainerType.View, window.GetType().Name);

            if (userSetting == null)
                return;

            size = userSetting.Size;
            point = userSetting.Point;
        }

        public void SaveActiveCard()
        {
            var vw = GetActiveViewModel() as ViewModelCard;
            if (vw == null) return;
            var create = vw.State == ViewModelState.Create;

            vw.SaveCard.Execute(null);

            if (create && !(vw is NPFViewModel))
            {
                var panel = GetActivePanel();
                panel.Caption = string.IsNullOrEmpty(vw.NewCaption) ? panel.Caption : vw.NewCaption;
            }

            var npfViewModel = vw as NPFViewModel;
            if (npfViewModel != null)
            {
                var panel = GetActivePanel();
                var name = npfViewModel.LegalEntity.FormalizedName;
                panel.Caption = string.IsNullOrEmpty(name) ? panel.Caption : name;
            }

            //Если модель сохранилась и требует закрытия после сохранения - то закрываем.
            if (vw is ICloseViewModelAfterSave && !vw.IsDataChanged)
                CloseActiveView();
        }


        public void DeleteActiveCard()
        {
            var vw = GetActiveViewModel() as ViewModelCard;
            var panel = GetActivePanel();
            if (vw == null) return;

            //предварительная проверка перед удалением
            if (!vw.BeforeExecuteDeleteCheck())
                return;
            //удаление карточки
            vw.DeleteCard.Execute(null);

            if (panel != null)
                DockManager.DockController.Close(panel);
            var dl = GetDLViewModel();
            dl?.RefreshList.Execute(null);
        }

        public DeleteDocumentsListViewModel GetDLViewModel()
        {
            return DockManager.GetItems()
                .OfType<LayoutPanel>()
                .Select(item => (item).Content as UserControl)
                .OfType<DeleteDocumentsListView>()
                .Select(win => win.DataContext as DeleteDocumentsListViewModel)
                .FirstOrDefault();
        }

        public bool IsDeletableCardActive()
        {
            //формы
            var vw = GetActiveViewModel() as ViewModelCard;
            return vw != null && vw.DeleteCard.CanExecute(null);
        }

        public bool IsSavableCardActive()
        {
            var vm = GetActiveViewModel();
            var model = vm as ViewModelCard;

            if (model?.SaveCard == null)
                return false;

            //Если модель без функции сохранения - кнопка неактивна
            if (model.IsNotSavable)
                return false;

            //В остальных случаях доступность определяется командой model.SaveCard
            return model.SaveCard.CanExecute(null);
        }

        public void SaveDockLayout()
        {
            using (var memoryStream = new MemoryStream())
            {
                DockManager.SaveLayoutToStream(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                App.UserSettingsManager.SetDockLayoutManagerData(memoryStream);
            }
        }

        public void FitPanelsToParentSize(Size parentSize)
        {
            foreach (var group in DockManager.FloatGroups) // среди не развернутых вкладок
            {
                if (group.FloatSize.Height > parentSize.Height)
                    group.FloatSize = new Size(group.FloatSize.Width, parentSize.Height);

                if (group.FloatSize.Width > parentSize.Width)
                    group.FloatSize = new Size(parentSize.Width, group.FloatSize.Height);
            }
        }

        private static void SaveUserSettingsRecursively(LayoutGroup layoutGroup)
        {
            foreach (var layoutBaseItem in layoutGroup.Items)
            {
                if (layoutBaseItem is LayoutPanel)
                    SaveUserSettings(layoutBaseItem as LayoutPanel);
                else if (layoutBaseItem is LayoutGroup)
                    SaveUserSettingsRecursively(layoutBaseItem as LayoutGroup);
            }
        }
        #endregion

        #region GetActive...
        public Type GetActiveViewType()
        {
            var activePanel = DockManager.ActiveDockItem as LayoutPanel;
            return activePanel?.Content?.GetType();
        }
        /// <summary>
        /// Получить активную модель формы
        /// </summary>
        public ViewModelBase GetActiveViewModel()
        {
            var activePanel = DockManager.ActiveDockItem as LayoutPanel;
            var window = activePanel?.Content as UserControl;
            return window?.DataContext as ViewModelBase;
        }

        /// <summary>
        /// Закрыть активную форму
        /// </summary>
        public static void CloseActiveView()
        {
            var activePanel = Instance.DockManager.ActiveDockItem as LayoutPanel;
            if (activePanel != null)
                Instance.DockManager.DockController.Close(activePanel);
        }

        /// <summary>
        /// Изменить размер активной формы
        /// </summary>
        public static void ResizeActiveView(Size minSize, Size maxSize)
        {
            var activePanel = Instance.DockManager.ActiveDockItem as LayoutPanel;
            if (activePanel != null)
                ResizeView(minSize, maxSize, activePanel);
        }

        public static void ResizeView(Size minSize, Size maxSize, LayoutPanel panel)
        {
            panel.MinWidth = minSize.Width;
            panel.MinHeight = minSize.Height;

            panel.MaxWidth = maxSize.Width;
            panel.MaxHeight = maxSize.Height;

            if (double.IsPositiveInfinity(panel.MaxHeight))
            {
                panel.HorizontalContentAlignment = HorizontalAlignment.Stretch;
                panel.VerticalContentAlignment = VerticalAlignment.Stretch;
            }
            Instance.DockManager.Update();
        }

        /// <summary>
        /// Получить активную форму
        /// </summary>
        public UserControl GetActiveView()
        {
            var activePanel = DockManager.ActiveDockItem as LayoutPanel;
            return activePanel?.Content as UserControl;
        }

        public LayoutPanel GetActivePanel()
        {
            return DockManager.ActiveDockItem as LayoutPanel;
        }

        /// <summary>
        /// Получить активную модель списка
        /// </summary>
        public ViewModelList GetActiveListVM()
        {
            var activePanel = DockManager.ActiveDockItem as LayoutPanel;
            var window = activePanel?.Content as UserControl;
            return window?.DataContext as ViewModelList;
        }

        #endregion

        private static bool IsModelContainCorrectData(ViewModelBase viewModel)
        {
            if (viewModel is VRPlanCorrectionWizardViewModel || viewModel is SIPlanCorrectionWizardViewModel)
            {
                if (((PlanCorrectionWizardViewModel<PlanContractRecord>)viewModel).IsModelContainCorrectData()) return true;
                DXMessageBox.Show("\"Годовые планы\" отсутствуют.");
                return false;
            }
            if (viewModel is SIMonthTransferCreationWizardModel || viewModel is VRMonthTransferCreationWizardModel)
            {
                if (((MonthTransferCreationWizardModel)viewModel).IsModelContainCorrectData()) return true;
                DXMessageBox.Show("\"Годовые планы\" отсутствуют.");
                return false;
            }
            var model = viewModel as RelayingReportViewModel;
            if (model != null)
            {
                if (model.IsReportCanCreate) return true;
                DXMessageBox.Show("Отчеты по всем ЦБ созданы.");
                return false;
            }

            return true;
        }
    }
}