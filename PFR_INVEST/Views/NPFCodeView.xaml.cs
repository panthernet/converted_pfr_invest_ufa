﻿using System.Windows.Controls;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class NPFCodeView : UserControl
    {
        protected NPFCodeViewModel Model => DataContext as NPFCodeViewModel;

        public NPFCodeView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
