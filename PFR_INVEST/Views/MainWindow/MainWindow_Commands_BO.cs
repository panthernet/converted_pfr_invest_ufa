﻿using System;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Tools;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.ViewModelsPrint;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Wizards.DepositCountWizard;
using PFR_INVEST.Views.Wizards.OpfrTransferWizard;

namespace PFR_INVEST
{
    public partial class MainWindow
    {
        private void BackOfficeCommandsConstructor()
        {
            RibbonCommands.ShowCommonPPList.Executed += ShowCommonPPList_Executed;
            RibbonCommands.ShowCreateCommonPP.Executed += ShowCreateCommonPP_Executed;
            RibbonCommands.ShowSelectCommonPP.CanExecute += ShowSelectCommonPP_CanExecute;
            RibbonCommands.ShowSelectCommonPP.Executed += ShowSelectCommonPP_Executed;
            RibbonCommands.ShowSplitPP.CanExecute += ShowSplitPP_CanExecute;
            RibbonCommands.ShowSplitPP.Executed += ShowSplitPP_Executed;

            RibbonCommands.ShowOpfrPPList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowOpfrPPList.Executed += ShowOpfrPpListOnExecuted;
            RibbonCommands.CreateOpfrPP.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateOpfrPP.Executed += CreateOpfrPpOnExecuted;
            RibbonCommands.SelectOpfrPP.CanExecute += SelectOpfrPpOnCanExecute;
            RibbonCommands.SelectOpfrPP.Executed += SelectOpfrPpOnExecuted;
        }      

        #region QuarterAccurate - Уточнение за квартал СВ/ДСВ


        private void CreateQuarterAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long addSPNID = 0;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null)
                addSPNID = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as DueListView;
                if (vList != null)
                    addSPNID = vList.QuarterAddSPNID ?? 0;
            }

            if (addSPNID > 0)
                App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Create, addSPNID, DueDocKindIdentifier.DueAccurateQuarter);
        }

        private void CreateQuarterAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null && rlVM.Type == PortfolioIdentifier.PortfolioTypes.SPN)
            {
                e.CanExecute = (rlVM.State == ViewModelState.Edit) //&& !rlVM.IsReadOnly 
                    && !rlVM.IsItemClosedByKDoc;
                return;
            }

            var vList = App.DashboardManager.GetActiveView() as DueListView;
            var selected = vList?.QuarterAddSPNID != null;
            if (selected)
                e.CanExecute = vList.SelectedItem != null && !(vList.SelectedItem.IsClosedByKDoc.HasValue && vList.SelectedItem.IsClosedByKDoc.Value);
        }

        private void CreateQuarterAccurateDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long addSPNID = 0;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null)
                addSPNID = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as DsvListView;
                if (vList != null)
                    addSPNID = vList.QuarterAddSPNID ?? 0;
            }

            if (addSPNID <= 0) return;
            if (DueHelper.GetIsQuarterDopOpBlocked(addSPNID))
            {
                ViewModelBase.DialogHelper.ShowError("Невозможно добавить новое уточнение за квартал, т.к. присутствует уточнение за вышестоящий квартал!");
                return;
            }
            App.DashboardManager.OpenNewTab(typeof(QuarterAccurateView), ViewModelState.Create, addSPNID, (object) DueDocKindIdentifier.DueDSVAccurateYear, DueDocKindIdentifier.DueDSVAccurateYear, "");
        }

        private void CreateQuarterAccurateDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null && (rlVM.Type == PortfolioIdentifier.PortfolioTypes.DSV || rlVM.Type == PortfolioIdentifier.PortfolioTypes.ALL))
            {
                // IsReadOnly выставляется также при наличии ДК и при наличии уточнений
                e.CanExecute = (rlVM.State == ViewModelState.Edit) //&& !rlVM.IsReadOnly 
                    && !rlVM.IsItemClosedByKDoc;
                return;
            }
            var vList = App.DashboardManager.GetActiveView() as DsvListView;
            var selected = vList?.QuarterAddSPNID != null;
            if (selected)
                e.CanExecute = vList.SelectedItem != null && !(vList.SelectedItem.IsClosedByKDoc.HasValue && vList.SelectedItem.IsClosedByKDoc.Value);
        }

        #endregion QuarterAccurate

        #region MonthAccurate - Уточнение за месяц СВ/ДСВ

        private void CreateMonthAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long addSPNID = 0;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null)
                addSPNID = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as DueListView;
                if (vList != null)
                    addSPNID = vList.SelectedItem?.ID ?? 0;
            }

            if (addSPNID > 0)
                App.DashboardManager.OpenNewTab(typeof(MonthAccurateView),
                                                ViewModelState.Create, addSPNID, DueDocKindIdentifier.DueAccurate);
        }

        private void CreateMonthAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null && rlVM.Type == PortfolioIdentifier.PortfolioTypes.SPN)
            {
                e.CanExecute = (rlVM.State == ViewModelState.Edit) //&& !rlVM.IsReadOnly 
                    && !rlVM.IsItemClosedByKDoc;
                return;
            }

            var vList = App.DashboardManager.GetActiveView() as DueListView;
            var selected = vList != null && vList.IsDSVSelected();
            if (selected && vList.SelectedItem != null)
                e.CanExecute = !(vList.SelectedItem.IsClosedByKDoc.HasValue && vList.SelectedItem.IsClosedByKDoc.Value);
        }

        private void CreateMonthAccurateDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long addSPNID = 0;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null)
                addSPNID = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as DsvListView;
                if (vList != null)
                    addSPNID = vList.SelectedItem?.ID ?? 0;
            }

            if (addSPNID <= 0) return;
            if (DueHelper.GetIsAccurateMonthOpBlocked(addSPNID))
            {
                ViewModelBase.DialogHelper.ShowError("Невозможно добавить новое уточнение за месяц, т.к. присутствует уточнение за квартал, перекрывающее дату уточнения!");
                return;
            }
            App.DashboardManager.OpenNewTab(typeof(MonthAccurateView),ViewModelState.Create, addSPNID, (object) DueDocKindIdentifier.DueDSVAccurate, DueDocKindIdentifier.DueDSVAccurate, "");
        }

        private void CreateMonthAccurateDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueViewModel;
            if (rlVM != null && (rlVM.Type == PortfolioIdentifier.PortfolioTypes.DSV || rlVM.Type == PortfolioIdentifier.PortfolioTypes.ALL))
            {
                e.CanExecute = (rlVM.State == ViewModelState.Edit) //&& !rlVM.IsReadOnly 
                    && !rlVM.IsItemClosedByKDoc;
                return;
            }
            var vList = App.DashboardManager.GetActiveView() as DsvListView;
            var selected = vList != null && vList.IsDSVSelected();
            if (selected && vList.SelectedItem != null)
                e.CanExecute = !(vList.SelectedItem.IsClosedByKDoc.HasValue && vList.SelectedItem.IsClosedByKDoc.Value);
        }
        #endregion

        #region Due - Страховые взносы СВ/ДСВ
        private void CreateDue_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Create,
                                            PortfolioIdentifier.PortfolioTypes.SPN, DueDocKindIdentifier.Due);
        }

        private void CreateDue_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueView), ViewModelState.Create,
                                            PortfolioIdentifier.PortfolioTypes.DSV, DueDocKindIdentifier.DueDSV);
        }

        private void CreateDueDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

        #region Отчеты

        private static void ShowReportsBO_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			App.DashboardManager.OpenNewTab(typeof(ReportsView), ViewModelState.Read, ReportFolderType.BO, "Отчеты");
		}

        private static void ShowReportNum1_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (App.DashboardManager.FindViewModelsList(typeof(CBReportManagerViewModel)).Cast<CBReportManagerViewModel>().Count(a => !a.IsCentralized) > 0) return;
            App.DashboardManager.OpenNewTab(typeof(CBReportManagerView), ViewModelState.Read, "Отчеты в Банк России (форма №1)");
        }

        private void ShowReportChfrOnExecuted(object sender, ExecutedRoutedEventArgs executedRoutedEventArgs)
        {
            ViewModelBase.DialogHelper.OpenChfrReportDialog();
        }


        #endregion

        #region Перечисления

        private void ShowSelectCommonPP_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var uc = App.DashboardManager.GetActiveView();
            if (uc == null)
                return;
            var type = uc.GetType();

            if (type == typeof(UKTransfersListView))
            {
                e.CanExecute = ((UKTransfersListView)uc).IsTransferSelected();
            }
            else if (type == typeof(TransferToNPFListView))
            {
                if (((TransferToNPFListView)uc).IsTransferSelected())
                    e.CanExecute = true;
            }
            else if (type == typeof(TransferFromNPFListView))
            {
                if (((TransferFromNPFListView)uc).IsTransferSelected())
                    e.CanExecute = true;
            }

        }

        private void ShowSelectCommonPP_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var uc = App.DashboardManager.GetActiveView();

            if (uc == null)
                return;

            long? trID = null;
            var section = AsgFinTr.Sections.NPF;
            AsgFinTr.Directions direction = AsgFinTr.Directions.ToPFR;

            //Выставляем связь
            if (uc.GetType() == typeof(UKTransfersListView))
            {
                var rt = ((UKTransfersListView)uc).GetSelectedTransfer();
                trID = rt.ID;
                if (TransferDirectionIdentifier.IsFromPFRToUK(rt.SPNDirectionName))
                    direction = AsgFinTr.Directions.FromPFR;
                else if (TransferDirectionIdentifier.IsFromUKToPFR(rt.SPNDirectionName))
                    direction = AsgFinTr.Directions.ToPFR;
                section = AsgFinTr.Sections.SI;
            }
            else if (uc.GetType() == typeof(TransferToNPFListView))
            {
                trID = ((TransferToNPFListView)uc).GetCurrFinRegID();
                direction = AsgFinTr.Directions.FromPFR;
            }
            else if (uc.GetType() == typeof(TransferFromNPFListView))
            {
                trID = ((TransferFromNPFListView)uc).GetCurrFinRegID();
                direction = AsgFinTr.Directions.ToPFR;
            }

            ViewModelBase.DialogHelper.SelectUnlinkedCommonPP(trID, section, direction);
        }

        void ShowCreateCommonPP_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CommonPPView), ViewModelState.Create, "Добавить платежное поручение", "Платежное поручение");
        }

        void ShowCommonPPList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CommonPPListView), ViewModelState.Read, "Платежные поручения УК/НПФ");
        }

        private void ShowSplitPP_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as CommonPPListView;
            if (vm == null) return;
            e.CanExecute = vm.GetSelectedDate() != null;
        }

        private void ShowSplitPP_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as CommonPPListView;
            var param = vm.GetSelectedDate();
            App.DashboardManager.OpenNewTab(typeof(CommonPPSplitView), ViewModelState.Create,
                                            param, "Разнести платежные поручения");
        }

        #endregion

        #region ОПФР

        private void ShowOpfrPpListOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CommonPPListView), ViewModelState.Read, true, "Платежные поручения ОПФР");

        }

        private void CreateOpfrPpOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CommonPPOpfrView), ViewModelState.Create, true, "Добавить платежное поручение ОПФР", "Платежное поручение ОПФР");
        }

        private void SelectOpfrPpOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            if (vm == null) return;

            ViewModelBase.DialogHelper.SelectUnlinkedCommonPP(vm.GetTransferID(), AsgFinTr.Sections.BackOffice, vm.GetTransferDirection());
        }

        private void SelectOpfrPpOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            if (vm == null) return;
            e.CanExecute = vm.IsTransferSelected();
        }



        private static void OpfrWizardOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OpfrTransferWizardView), ViewModelState.Create, "Мастер перечислений ОПФР");
        }

        private static void CreateOpfrRequest_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            var regid = vm.GetRegisterID();
            App.DashboardManager.OpenNewTab(typeof(TransferRequestView), ViewModelState.Edit, new object[] { true, regid }, "Создать приложение к Распоряжению");
        }

        private static void CreateOpfrRequest_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            e.CanExecute = vm != null && vm.IsRegisterHaveTransfers();
            if (!e.CanExecute) return;

            var tvmList = App.DashboardManager.FindViewModelsList(typeof(OpfrTransferRequestViewModel));
            if (tvmList == null || tvmList.Count <= 0) return;
            var rId = vm.GetRegisterID();
            e.CanExecute = tvmList.All(a => ((OpfrTransferRequestViewModel) a).RegisterID != rId);
        }

        private static void OpfrExport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.Administrator);
        }

        private void OpfrExportOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var idList = ViewModelBase.DialogHelper.ShowOnesSelectExportDataDlg(DataObjects.OnesSettingsDef.IntGroup.Opfr);
            if (idList == null) return;
            OnesExecuteExportWrapper(DataObjects.OnesSettingsDef.IntGroup.Opfr, idList);
        }

        private void OpfrImportOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            e.CanExecute = vm != null && vm.IsRegisterSelected() && !vm.GetRegisterHasTransfers();
        }

        private void OpfrImportOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            ViewModelBase.DialogHelper.ImportOpfr(vm.GetRegisterID());
        }

        private void CreateOpfrTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            e.CanExecute = vm != null && vm.IsRegisterSelected();
        }


        private void CreateOpfrTransferOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveView() as OpfrListView;
            DashboardManager.Instance.OpenNewTab(typeof(OpfrTransferView), ViewModelState.Create, (object)vm.GetRegisterID(), "Перечисление");
        }


        private void CreateOpfrRegisterOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateOpfrRegisterOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as OpfrListViewModel;
            App.DashboardManager.OpenNewTab(typeof(OpfrRegisterView), ViewModelState.Create, "Распоряжение");
        }

        private void ShowOpfrListOnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OpfrListView), ViewModelState.Read, "Распоряжения Правления");
        }
        #endregion

        private static void DepositCountWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepositCountWizardView), ViewModelState.Create, "Мастер учета размещения СПН на депозиты в кредитных организациях и возврата депозитов от кредитных организаций");

        }
    }
}
