﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Core;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Tools;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.DBFLoader;
using PFR_INVEST.Core.Properties;
using PFR_INVEST.Core.Settings;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Analyze;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.Views.Interface;
using PFR_INVEST.Views.LetterToNPFWizard;
using PFR_INVEST.Views.MonthTransferCreationWizard;
using PFR_INVEST.Views.PlanCorrectionWizard;
using PFR_INVEST.Views.PreferencesViews;
using PFR_INVEST.Views.RegionsReport;
using PFR_INVEST.Views.TransferWizard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Common;
using PFR_INVEST.Views.Dialogs.ReportPromt;
using PFR_INVEST.Views.Wizards.TransferWizard.SIRopsAsvWizard;

namespace PFR_INVEST
{
    public partial class MainWindow
    {
        #region BindRibbonServerCommands()

        private void BindRibbonServerCommands()
        {
            NPFConstructor();
            BindCbAndDepositCommands();
            BackOfficeCommandsConstructor();

            RibbonCommands.ShowReportCHFR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportCHFR.Executed += ShowReportChfrOnExecuted;

            RibbonCommands.ShowSpnDepositWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSpnDepositWizard.Executed += ShowSpnDepositWizardOnExecuted;

            RibbonCommands.DepositCountWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.DepositCountWizard.Executed += DepositCountWizard_Executed;

            RibbonCommands.CBReportWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CBReportWizard.Executed += CBReportWizardOnExecuted;

            RibbonCommands.OpfrWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OpfrWizard.Executed += OpfrWizardOnExecuted;

            RibbonCommands.RecalcUKReqTransfers.CanExecute += CanExecuteIsTrue;
            RibbonCommands.RecalcUKReqTransfers.Executed += RecalcUKReqTransfers_Executed;
            RibbonCommands.ShowOpfrList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateOpfrRegister.CanExecute += CreateOpfrRegisterOnCanExecute;
            RibbonCommands.CreateOpfrTransfer.CanExecute += CreateOpfrTransfer_CanExecute;
            RibbonCommands.OpfrImport.CanExecute += OpfrImportOnCanExecute;
            RibbonCommands.OpfrExport.CanExecute += OpfrExport_CanExecute;
            RibbonCommands.ShowOpfrList.Executed += ShowOpfrListOnExecuted;
            RibbonCommands.CreateOpfrRegister.Executed += CreateOpfrRegisterOnExecuted;
            RibbonCommands.CreateOpfrTransfer.Executed += CreateOpfrTransferOnExecuted;
            RibbonCommands.OpfrImport.Executed += OpfrImportOnExecuted;
            RibbonCommands.OpfrExport.Executed += OpfrExportOnExecuted;
            RibbonCommands.CreateOpfrRequest.CanExecute += CreateOpfrRequest_CanExecute;
            RibbonCommands.CreateOpfrRequest.Executed += CreateOpfrRequest_Executed;

            RibbonCommands.OneS_DataExchange.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OneS_Settings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OneS_OpSettings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OneS_ViewErrorList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OneS_ViewImportJournal.CanExecute += CanExecuteIsTrue;
            RibbonCommands.OneS_ViewExportJournal.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PKIP_DataExchange.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PKIP_Settings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PKIP_ViewErrorList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PKIP_ViewJournal.CanExecute += CanExecuteIsTrue;

            RibbonCommands.CBReportList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CBReportList.Executed += CBReportList_Executed;
            RibbonCommands.CBReportCreate.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CBReportCreate.Executed += CBReportCreate_Executed;
            RibbonCommands.CBReportExport.CanExecute += CBReportExport_CanExecute;
            RibbonCommands.CBReportExport.Executed += CBReportExport_Executed;
            RibbonCommands.CBReportSettingsPaymentDetail.CanExecute += CBReportSettingsPaymentDetail_CanExecute;
            RibbonCommands.CBReportSettingsPaymentDetail.Executed += CBReportSettingsPaymentDetail_Executed;

            RibbonCommands.OneS_DataExchange.Executed += OneS_DataExchange_Executed;
            RibbonCommands.OneS_ViewErrorList.Executed += OneS_ViewErrorList_Executed;
            RibbonCommands.OneS_ViewImportJournal.Executed += OneS_ViewImportJournal_Executed;
            RibbonCommands.OneS_ViewExportJournal.Executed += OneS_ViewExportJournal_Executed;

            RibbonCommands.PKIP_DataExchange.Executed += PKIP_DataExchange_Executed;
            RibbonCommands.PKIP_ViewErrorList.Executed += PKIP_ViewErrorList_Executed;
            RibbonCommands.PKIP_ViewJournal.Executed += PKIP_ViewJournal_Executed;

            RibbonCommands.PKIP_Settings.Executed += PKIP_Settings_Executed;
            RibbonCommands.OneS_Settings.Executed += OneS_Settings_Executed;
            RibbonCommands.OneS_OpSettings.Executed += OneS_OpSettings_Executed;


            RibbonCommands.ShowInvDecl.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowInvDecl.Executed += ShowSIInvDecl_Executed;

            RibbonCommands.ShowGarantFond.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowGarantFond.Executed += ShowGarantFond_Executed;

            RibbonCommands.PrintYearPayment.CanExecute += PrintYearPayment_CanExecute;
            RibbonCommands.PrintYearPayment.Executed += PrintSIYearPayment_Executed;
            RibbonCommands.ShowPortfolioStatusList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPortfolioStatusList.Executed += ShowPortfolioStatusList_Executed;
            RibbonCommands.RollbackStatus.CanExecute += RollbackStatus_CanExecute;
            RibbonCommands.RollbackStatus.Executed += RollbackStatus_Executed;

            RibbonCommands.ReportImportSI.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ReportImportSI.Executed += ReportImportSI_Executed;
            RibbonCommands.OnesDataSI.CanExecute += OnesDataSI_CanExecute;
            RibbonCommands.OnesDataSI.Executed += OnesDataSIOnExecuted;

            RibbonCommands.LetterReqTransferSPNMSK.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNMSK.Executed += LetterReqTransferSPNMSK_Executed;
            RibbonCommands.LetterReqTransferSPNAccum.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNAccum.Executed += LetterReqTransferSPNAccum_Executed;
            RibbonCommands.LetterReqTransferSPNUrgent.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNUrgent.Executed += LetterReqTransferSPNUrgent_Executed;
            RibbonCommands.LetterReqTransferSPNFlow.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNFlow.Executed += LetterReqTransferSPNFlow_Executed;
            RibbonCommands.LetterTransferSPNAccumZero.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterTransferSPNAccumZero.Executed += LetterTransferSPNAccumZero_Executed;
            RibbonCommands.LetterTransferSPNAccumUrgent.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterTransferSPNAccumUrgent.Executed += LetterTransferSPNAccumUrgent_Executed;

            RibbonCommands.LetterReqTransferSPNOneTime.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNOneTime.Executed += LetterReqTransferMoneyASV_Executed;
            RibbonCommands.LetterTransferSPNAccumOneTime.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterTransferSPNAccumOneTime.Executed += LetterReqTransferMoneyASV_Executed;
            RibbonCommands.LetterReqTransferSPNOwners.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferSPNOwners.Executed += LetterReqTransferMoneyASV_Executed;
            RibbonCommands.LetterReqTransferMoneyROPS.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferMoneyROPS.Executed += LetterReqTransferMoneyASV_Executed;
            RibbonCommands.LetterReqTransferMoneyASV.CanExecute += CanLettersSIIsTrue;
            RibbonCommands.LetterReqTransferMoneyASV.Executed += LetterReqTransferMoneyASV_Executed;

            //Ribbon.RibbonCommands.ReportExportSI.CanExecute += CanExecuteIsTrue;
            //Ribbon.RibbonCommands.ReportExportSI.Executed += ReportExportSI_Executed;


            RibbonCommands.RestoreDocument.CanExecute += RestoreDocument_CanExecute;
            RibbonCommands.RestoreDocument.Executed += RestoreDocument_Executed;
            RibbonCommands.StopCO.CanExecute += StopCO_CanExecute;
            RibbonCommands.StopCO.Executed += StopCO_Executed;
            RibbonCommands.ResumeCO.CanExecute += ResumeCO_CanExecute;
            RibbonCommands.ResumeCO.Executed += ResumeCO_Executed;


            RibbonCommands.StopSI.CanExecute += StopSI_CanExecute;
            RibbonCommands.StopSI.Executed += StopSI_Executed;
            RibbonCommands.ResumeSI.CanExecute += ResumeSI_CanExecute;
            RibbonCommands.ResumeSI.Executed += ResumeSI_Executed;
            RibbonCommands.CreateRate.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateRate.Executed += CreateRate_Executed;

            RibbonCommands.CreateLegalEntity.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateLegalEntity.Executed += CreateLegalEntity_Executed;
            RibbonCommands.CreateBank.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateBank.Executed += CreateBank_Executed;
            RibbonCommands.CreateBankAgent.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateBankAgent.Executed += CreateBankAgent_Executed;
            RibbonCommands.ShowBankAgentsList.CanExecute += ShowBanksList_CanExecute;
            RibbonCommands.ShowBankAgentsList.Executed += ShowBankAgentsList_Executed;
            RibbonCommands.CreatePortfolio.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreatePortfolio.Executed += CreatePortfolio_Executed;
            RibbonCommands.CreateBankAccount.CanExecute += CreateBankAccount_CanExecute;
            RibbonCommands.CreateBankAccount.Executed += CreateBankAccount_Executed;
            RibbonCommands.CreatePFRAccount.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreatePFRAccount.Executed += CreatePFRAccount_Executed;

            RibbonCommands.CreateReorganization.CanExecute += CreateReorganization_CanExecute;
            RibbonCommands.CreateReorganization.Executed += CreateReorganization_Executed;
            RibbonCommands.CreateDeadZL.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateDeadZL.Executed += CreateDeadZL_Executed;
            RibbonCommands.CreateFinRegister.CanExecute += CreateFinRegister_CanExecute;
            RibbonCommands.CreateFinRegister.Executed += CreateFinRegister_Executed;

            RibbonCommands.CreateRegister.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateRegister.Executed += CreateRegister_Executed;
            RibbonCommands.CreateZLRedist.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateZLRedist.Executed += CreateZLRedist_Executed;
            RibbonCommands.CreateLetterToNPF.CanExecute += CreateLetterToNPF_CanExecute;
            RibbonCommands.CreateLetterToNPF.Executed += CreateLetterToNPF_Executed;
            RibbonCommands.LetterToNPF.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LetterToNPF.Executed += LetterToNPF_Executed;
            RibbonCommands.LetterToNPFs.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LetterToNPFs.Executed += LetterToNPFs_Executed;
            RibbonCommands.LetterToUK.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LetterToUK.Executed += LetterToUK_Executed;
            RibbonCommands.LetterToUKs.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LetterToUKs.Executed += LetterToUKs_Executed;
            RibbonCommands.CreateERZLNotify.CanExecute += CreateERZLNotify_CanExecute;
            RibbonCommands.CreateERZLNotify.Executed += CreateERZLNotify_Executed;

            RibbonCommands.CreateRequest.CanExecute += CreateRequest_CanExecute;
            RibbonCommands.CreateRequest.Executed += CreateRequest_Executed;
            RibbonCommands.CreateSecurity.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateSecurity.Executed += CreateSecurity_Executed;
            RibbonCommands.CreateBoard.Executed += CreateBoard_Executed;
            RibbonCommands.CreateOrder.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateOrder.Executed += CreateOrder_Executed;
            RibbonCommands.CreateOrderRelaying.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateOrderRelaying.Executed += CreateOrderRelaying_Executed;

            RibbonCommands.CreateOrderToPay.CanExecute += CreateSIOrderToPay_CanExecute;
            RibbonCommands.CreateOrderToPay.Executed += CreateSIOrderToPay_Executed;
            RibbonCommands.OrderSent.CanExecute += SIOrderSent_CanExecute;
            RibbonCommands.OrderSent.Executed += SIOrderSent_Executed;
            RibbonCommands.OrderDelivered.CanExecute += SIOrderDelivered_CanExecute;
            RibbonCommands.OrderDelivered.Executed += SIOrderDelivered_Executed;
            RibbonCommands.OrderPaid.CanExecute += SIOrderPaid_CanExecute;
            RibbonCommands.OrderPaid.Executed += SIOrderPaid_Executed;

            RibbonCommands.CreateSecurityInOrder.CanExecute += CreateSecurityInOrder_CanExecute;
            RibbonCommands.CreateSecurityInOrder.Executed += CreateSecurityInOrder_Executed;

            RibbonCommands.AddDataFromReport.CanExecute += AddDataFromReport_CanExecute;
            RibbonCommands.AddDataFromReport.Executed += AddDataFromReport_Executed;
            RibbonCommands.FastReportInput.CanExecute += FastReportInput_CanExecute;
            RibbonCommands.FastReportInput.Executed += FastReportInput_Executed;
            //Ribbon.RibbonCommands.FastDepositInput.CanExecute += FastDepositInput_CanExecute;
            //Ribbon.RibbonCommands.FastDepositInput.Executed += FastDepositInput_Executed;

            RibbonCommands.CreatePFRBranch.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreatePFRBranch.Executed += CreatePFRBranch_Executed;
            RibbonCommands.CreatePFRContact.CanExecute += CreatePFRContact_CanExecute;
            RibbonCommands.CreatePFRContact.Executed += CreatePFRContact_Executed;

            RibbonCommands.CreateIncomeSecurities.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateIncomeSecurities.Executed += CreateIncomeSecurities_Executed;
            RibbonCommands.CreateInsurance.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateInsurance.Executed += CreateInsurance_Executed;
            RibbonCommands.CreateDue.CanExecute += CreateDue_CanExecute;
            RibbonCommands.CreateDue.Executed += CreateDue_Executed;
            RibbonCommands.CreateMonthAccurate.CanExecute += CreateMonthAccurate_CanExecute;
            RibbonCommands.CreateMonthAccurate.Executed += CreateMonthAccurate_Executed;
            RibbonCommands.CreateQuarterAccurate.CanExecute += CreateQuarterAccurate_CanExecute;
            RibbonCommands.CreateQuarterAccurate.Executed += CreateQuarterAccurate_Executed;

            RibbonCommands.CreateDueDSV.CanExecute += CreateDueDSV_CanExecute;
            RibbonCommands.CreateDueDSV.Executed += CreateDueDSV_Executed;
            RibbonCommands.CreateMonthAccurateDSV.CanExecute += CreateMonthAccurateDSV_CanExecute;
            RibbonCommands.CreateMonthAccurateDSV.Executed += CreateMonthAccurateDSV_Executed;
            RibbonCommands.CreateQuarterAccurateDSV.CanExecute += CreateQuarterAccurateDSV_CanExecute;
            RibbonCommands.CreateQuarterAccurateDSV.Executed += CreateQuarterAccurateDSV_Executed;

            RibbonCommands.CreateTreasuryDoc.CanExecute += CreateTreasuryDoc_CanExecute;
            RibbonCommands.CreateTreasuryDoc.Executed += CreateTreasuryDoc_Executed;

            RibbonCommands.CreatePrepayment.Executed += CreatePrepayment_Executed;
            RibbonCommands.CreatePrepayment.CanExecute += CreatePrepayment_CanExecute;
            RibbonCommands.CreatePrepaymentAccurate.Executed += CreatePrepaymentAccurate_Executed;
            RibbonCommands.CreatePrepaymentAccurate.CanExecute += CreatePrepaymentAccurate_CanExecute;

            RibbonCommands.CreateDueDead.Executed += CreateDueDead_Executed;
            RibbonCommands.CreateDueDead.CanExecute += CreateDueDead_CanExecute;
            RibbonCommands.CreateDueDeadDSV.Executed += CreateDueDeadDSV_Executed;
            RibbonCommands.CreateDueDeadDSV.CanExecute += CreateDueDeadDSV_CanExecute;
            RibbonCommands.CreateDueDeadAccurate.Executed += CreateDueDeadAccurate_Executed;
            RibbonCommands.CreateDueDeadAccurate.CanExecute += CreateDueDeadAccurate_CanExecute;
            RibbonCommands.CreateDueDeadAccurateDSV.Executed += CreateDueDeadAccurateDSV_Executed;
            RibbonCommands.CreateDueDeadAccurateDSV.CanExecute += CreateDueDeadAccurateDSV_CanExecute;

            RibbonCommands.CreateDueExcess.Executed += CreateDueExcess_Executed;
            RibbonCommands.CreateDueExcess.CanExecute += CreateDueExcess_CanExecute;
            RibbonCommands.CreateDueExcessDSV.Executed += CreateDueExcessDSV_Executed;
            RibbonCommands.CreateDueExcessDSV.CanExecute += CreateDueExcessDSV_CanExecute;
            RibbonCommands.CreateDueExcessAccurate.Executed += CreateDueExcessAccurate_Executed;
            RibbonCommands.CreateDueExcessAccurate.CanExecute += CreateDueExcessAccurate_CanExecute;
            RibbonCommands.CreateDueExcessAccurateDSV.Executed += CreateDueExcessAccurateDSV_Executed;
            RibbonCommands.CreateDueExcessAccurateDSV.CanExecute += CreateDueExcessAccurateDSV_CanExecute;

            RibbonCommands.CreateDueUndistributed.Executed += CreateDueUndistributed_Executed;
            RibbonCommands.CreateDueUndistributed.CanExecute += CreateDueUndistributed_CanExecute;
            RibbonCommands.CreateDueUndistributedDSV.Executed += CreateDueUndistributedDSV_Executed;
            RibbonCommands.CreateDueUndistributedDSV.CanExecute += CreateDueUndistributedDSV_CanExecute;
            RibbonCommands.CreateDueUndistributedAccurate.Executed += CreateDueUndistributedAccurate_Executed;
            RibbonCommands.CreateDueUndistributedAccurate.CanExecute += CreateDueUndistributedAccurate_CanExecute;
            RibbonCommands.CreateDueUndistributedAccurateDSV.Executed +=
                CreateDueUndistributedAccurateDSV_Executed;
            RibbonCommands.CreateDueUndistributedAccurateDSV.CanExecute +=
                CreateDueUndistributedAccurateDSV_CanExecute;

            RibbonCommands.CreateEnrollmentPercents.Executed += CreateEnrollmentPercents_Executed;
            RibbonCommands.CreateEnrollmentPercents.CanExecute += CreateEnrollmentPercents_CanExecute;
            RibbonCommands.CreateEnrollmentOther.Executed += CreateEnrollmentOther_Executed;
            RibbonCommands.CreateEnrollmentOther.CanExecute += CreateEnrollmentOther_CanExecute;
            RibbonCommands.CreateAccountWithdrawal.Executed += CreateAccountWithdrawal_Executed;
            RibbonCommands.CreateAccountWithdrawal.CanExecute += CreateAccountWithdrawal_CanExecute;
            RibbonCommands.CreateAccountTransfer.Executed += CreateAccountTransfer_Executed;
            RibbonCommands.CreateAccountTransfer.CanExecute += CreateAccountTransfer_CanExecute;

            RibbonCommands.BanksVerification.Executed += BanksVerification_Executed;
            RibbonCommands.BanksVerification.CanExecute += BanksVerification_CanExecute;

            //СЧ ИЛС
            RibbonCommands.ShowSchilsCostsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSchilsCostsList.Executed += ShowSchilsCostsList_Executed;
            RibbonCommands.CreateBudget.Executed += CreateBudget_Executed;
            RibbonCommands.CreateBudget.CanExecute += CreateBudget_CanExecute;
            RibbonCommands.CreateBudgetCorrection.Executed += CreateBudgetCorrection_Executed;
            RibbonCommands.CreateBudgetCorrection.CanExecute += CreateBudgetCorrection_CanExecute;
            RibbonCommands.CreateDirection.Executed += CreateDirection_Executed;
            RibbonCommands.CreateDirection.CanExecute += CreateDirection_CanExecute;
            RibbonCommands.CreateReturn.Executed += CreateReturn_Executed;
            RibbonCommands.CreateReturn.CanExecute += CreateReturn_CanExecute;
            RibbonCommands.CreateTransfer.Executed += CreateTransfer_Executed;
            RibbonCommands.CreateTransfer.CanExecute += CreateTransfer_CanExecute;

            RibbonCommands.СreateCosts.Executed += CreateCosts_Executed;
            RibbonCommands.СreateCosts.CanExecute += CreateCosts_CanExecute;

            RibbonCommands.CreateYield.Executed += CreateYield_Executed;
            RibbonCommands.CreateYield.CanExecute += CanExecuteIsTrue;

            RibbonCommands.CreateOVSITransfer.Executed += CreateOVSITransfer_Executed;
            RibbonCommands.CreateOVSITransfer.CanExecute += CreateOVSITransfer_CanExecute;

            RibbonCommands.ShowYieldsList.Executed += ShowYieldsList_Executed;
            RibbonCommands.ShowYieldsList.CanExecute += CanExecuteIsTrue;

            RibbonCommands.ShowF40List.Executed += ShowF40List_Executed;
            RibbonCommands.ShowF40List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF50List.Executed += ShowF50List_Executed;
            RibbonCommands.ShowF50List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF60List.Executed += ShowF60List_Executed;
            RibbonCommands.ShowF60List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF70List.Executed += ShowF70List_Executed;
            RibbonCommands.ShowF70List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF402List.Executed += ShowSIF402List_Executed;
            RibbonCommands.ShowF402List.CanExecute += CanExecuteIsTrue;

            RibbonCommands.ShowF80List.Executed += ShowF80List_Executed;
            RibbonCommands.ShowF80List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowOwnFundsList.Executed += ShowOwnFundsList_Executed;
            RibbonCommands.ShowOwnFundsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDemandList.Executed += ShowSIDemandList_Executed;
            RibbonCommands.ShowDemandList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF402PFRActions.Executed += ShowSIF402PFRActions_Executed;
            RibbonCommands.ShowF402PFRActions.CanExecute += ShowSIF402PFRActions_CanExecute;
            RibbonCommands.AddOwnFunds.Executed += AddOwnFunds_Executed;
            RibbonCommands.AddOwnFunds.CanExecute += CanExecuteIsTrue;


            RibbonCommands.ShowUsersList.Executed += ShowUsersList_Executed;
            RibbonCommands.ShowUsersList.CanExecute += ShowUsersList_CanExecute;
            RibbonCommands.ShowRolesList.Executed += ShowRolesList_Executed;
            RibbonCommands.ShowRolesList.CanExecute += ShowRolesList_CanExecute;
            RibbonCommands.AddUser.Executed += AddUser_Executed;
            RibbonCommands.AddUser.CanExecute += AddUser_CanExecute;
            RibbonCommands.DeleteUser.Executed += DeleteUser_Executed;
            RibbonCommands.DeleteUser.CanExecute += DeleteUser_CanExecute;

            RibbonCommands.GenerateRegister.CanExecute += GenerateRegister_CanExecute;
            RibbonCommands.GenerateRegister.Executed += GenerateRegister_Executed;

            RibbonCommands.ShowCostsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowCostsList.Executed += ShowCostsList_Executed;
            RibbonCommands.ShowContragentsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowContragentsList.Executed += ShowContragentsList_Executed;
            RibbonCommands.ShowBanksList.CanExecute += ShowBanksList_CanExecute;
            RibbonCommands.ShowBanksList.Executed += ShowBanksList_Executed;
            //Ribbon.RibbonCommands.ShowBanksConclusionList.CanExecute += ShowBanksConclusionList_CanExecute;
            //Ribbon.RibbonCommands.ShowBanksConclusionList.Executed += ShowBanksConclusionList_Executed;
            RibbonCommands.ShowAccountsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowAccountsList.Executed += ShowAccountsList_Executed;
            RibbonCommands.ShowPFRAccountsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPFRAccountsList.Executed += ShowPFRAccountsList_Executed;
            RibbonCommands.ShowPortfoliosList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPortfoliosList.Executed += ShowPortfoliosList_Executed;
            RibbonCommands.ShowRegistersList.CanExecute += CanExecuteIsTrue;

            RibbonCommands.RollbackFinRegStatus.CanExecute += RollbackFinRegStatus_CanExecute;
            RibbonCommands.RollbackFinRegStatus.Executed += RollbackFinRegStatus_Executed;

            RibbonCommands.ShowRegistersList.Executed += ShowRegistersList_Executed;
            RibbonCommands.ShowArchiveRegistersList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowArchiveRegistersList.Executed += ShowArchiveRegistersList_Executed;
            RibbonCommands.ShowDeadZLList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDeadZLList.Executed += ShowDeadZLList_Executed;
            RibbonCommands.ShowZLRedistList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowZLRedistList.Executed += ShowZLRedistList_Executed;
            RibbonCommands.ShowRatesList.CanExecute += ShowRatesList_CanExecute;
            RibbonCommands.ShowRatesList.Executed += ShowRatesList_Executed;
            RibbonCommands.ShowMissedRatesList.CanExecute += ShowMissedRatesList_CanExecute;
            RibbonCommands.ShowMissedRatesList.Executed += ShowMissedRatesList_Executed;
            RibbonCommands.ShowSecuritiesList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSecuritiesList.Executed += ShowSecuritiesList_Executed;
            RibbonCommands.ShowDepositsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDepositsList.Executed += ShowDepositsList_Executed;
            RibbonCommands.ShowDepositsByPortfolioList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDepositsByPortfolioList.Executed += ShowDepositsByPortfolioList_Executed;
            RibbonCommands.ShowPaymentOrderList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPaymentOrderList.Executed += ShowPaymentOrderList_Executed;

            RibbonCommands.ShowArchiveDepositsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowArchiveDepositsList.Executed += ShowArchiveDepositsList_Executed;
            RibbonCommands.ShowOrdersList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowOrdersList.Executed += ShowOrdersList_Executed;
            RibbonCommands.ShowSellReportsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSellReportsList.Executed += ShowSellReportsList_Executed;
            RibbonCommands.ShowBuyReportsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowBuyReportsList.Executed += ShowBuyReportsList_Executed;
            RibbonCommands.RecalcNKD.CanExecute += CanExecuteIsTrue;
            RibbonCommands.RecalcNKD.Executed += RecalcNKD_Executed;
            RibbonCommands.ShowPFRBranchesList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPFRBranchesList.Executed += ShowPFRBranchesList_Executed;
            RibbonCommands.ShowDueList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDueList.Executed += ShowDueList_Executed;
            RibbonCommands.ShowDueDSVList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDueDSVList.Executed += ShowDueDSVList_Executed;
            RibbonCommands.ShowACCList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowACCList.Executed += ShowACCList_Executed;
            RibbonCommands.ShowBalance.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowBalance.Executed += ShowBalance_Executed;
            RibbonCommands.ShowTempAllocationList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTempAllocationList.Executed += ShowTempAllocationList_Executed;
            RibbonCommands.ShowIncomeOnSecuritiesList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowIncomeOnSecuritiesList.Executed += ShowIncomeOnSecuritiesList_Executed;
            RibbonCommands.ShowInsuranceList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowInsuranceList.Executed += ShowInsuranceList_Executed;
            RibbonCommands.ShowInsuranceArchList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowInsuranceArchList.Executed += ShowInsuranceArchList_Executed;
            RibbonCommands.ShowTransferToNPFListArchive.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTransferToNPFListArchive.Executed += ShowTransferToNPFListArchive_Executed;
            RibbonCommands.ShowTransferToNPFList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTransferToNPFList.Executed += ShowTransferToNPFList_Executed;
            RibbonCommands.ShowTransferFromNPFList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTransferFromNPFList.Executed += ShowTransferFromNPFList_Executed;
            RibbonCommands.ShowCorrespondenceList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowCorrespondenceList.Executed += ShowCorrespondenceList_Executed;
            RibbonCommands.ShowArchiveList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowArchiveList.Executed += ShowArchiveList_Executed;
            RibbonCommands.ShowSIControlList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSIControlList.Executed += ShowSIControlList_Executed;

            RibbonCommands.ShowReportsNPF.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportsNPF.Executed += ShowReportsNPF_Executed;
            RibbonCommands.ShowReportsSI.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportsSI.Executed += ShowReportsSI_Executed;
            RibbonCommands.ShowReportsCB.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportsCB.Executed += ShowReportsCB_Executed;
            RibbonCommands.ShowReportsBO.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportsBO.Executed += ShowReportsBO_Executed;
            RibbonCommands.ShowReportNum1.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportNum1.Executed += ShowReportNum1_Executed;


            RibbonCommands.DeleteCard.CanExecute += DeleteCard_CanExecute;
            RibbonCommands.DeleteCard.Executed += DeleteCard_Executed;
            RibbonCommands.SaveCard.CanExecute += SaveCard_CanExecute;
            RibbonCommands.SaveCard.Executed += SaveCard_Executed;
            RibbonCommands.AnnulLicence.CanExecute += AnnulLicence_CanExecute;
            RibbonCommands.AnnulLicence.Executed += AnnulLicence_Executed;

            RibbonCommands.InputFinregisterProps.CanExecute += InputFinregisterProps_CanExecute;
            RibbonCommands.InputFinregisterProps.Executed += InputFinregisterProps_Executed;
            RibbonCommands.GiveFinRegister.CanExecute += GiveFinRegister_CanExecute;
            RibbonCommands.GiveFinRegister.Executed += GiveFinRegister_Executed;
            RibbonCommands.RefineFinregister.CanExecute += RefineFinregister_CanExecute;
            RibbonCommands.RefineFinregister.Executed += RefineFinregister_Executed;
            RibbonCommands.FixFinregister.CanExecute += FixFinregister_CanExecute;
            RibbonCommands.FixFinregister.Executed += FixFinregister_Executed;
            RibbonCommands.NotTransferredFinregister.CanExecute += NotTransferredFinRegister_CanExecute;
            RibbonCommands.NotTransferredFinregister.Executed += NotTransferredFinRegister_Executed;
            RibbonCommands.ImportNPFRegister.CanExecute += ImportNPF_CanExecute;
            RibbonCommands.ImportNPFRegister.Executed += ImportNPF_Executed;
            RibbonCommands.ImportNPFTempRegister.CanExecute += ImportNPFTemp_CanExecute;
            RibbonCommands.ImportNPFTempRegister.Executed += ImportNPFTemp_Executed;
            RibbonCommands.ImportRejApp.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ImportRejApp.Executed += ImportRejApp_Executed;
            RibbonCommands.ShowRejAppList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowRejAppList.Executed += ShowRejAppList_Executed;
            RibbonCommands.ShowReportErrorTypes.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorTypes.Executed += ShowReportErrorTypes_Executed;
            RibbonCommands.ShowReportErrorNpfSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorNpfSpread.Executed += ShowReportErrorNpfSpread_Executed;
            RibbonCommands.ShowReportErrorRegionSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorRegionSpread.Executed += ShowReportErrorRegionSpread_Executed;
            RibbonCommands.ShowReportErrorMonthSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorMonthSpread.Executed += ShowReportErrorMonthSpread_Executed;

            RibbonCommands.ShowNPFTempAllocationList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowNPFTempAllocationList.Executed += ShowNPFTempAllocationList_Executed;
            RibbonCommands.AddSPNAllocation.CanExecute += CanExecuteIsTrue;
            RibbonCommands.AddSPNAllocation.Executed += AddSPNAllocation_Executed;
            RibbonCommands.AddSPNReturn.CanExecute += CanExecuteIsTrue;
            RibbonCommands.AddSPNReturn.Executed += AddSPNReturn_Executed;
            RibbonCommands.AddAllocationNPF.CanExecute += AddAllocationNPF_CanExecute;
            RibbonCommands.AddAllocationNPF.Executed += AddAllocationNPF_Executed;

            RibbonCommands.ImportRejApp.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ImportRejApp.Executed += ImportRejApp_Executed;
            RibbonCommands.ShowRejAppList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowRejAppList.Executed += ShowRejAppList_Executed;
            RibbonCommands.ShowReportErrorTypes.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorTypes.Executed += ShowReportErrorTypes_Executed;
            RibbonCommands.ShowReportErrorNpfSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorNpfSpread.Executed += ShowReportErrorNpfSpread_Executed;
            RibbonCommands.ShowReportErrorRegionSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorRegionSpread.Executed += ShowReportErrorRegionSpread_Executed;
            RibbonCommands.ShowReportErrorMonthSpread.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowReportErrorMonthSpread.Executed += ShowReportErrorMonthSpread_Executed;

            RibbonCommands.SuspendActivity.CanExecute += SuspendActivity_CanExecute;
            RibbonCommands.SuspendActivity.Executed += SuspendActivity_Executed;

            RibbonCommands.AddNPFBranch.CanExecute += AddNPFBranch_CanExecute;
            RibbonCommands.AddNPFBranch.Executed += AddNPFBranch_Executed;

            RibbonCommands.ReturnDeposit.CanExecute += ReturnDeposit_CanExecute;
            RibbonCommands.ReturnDeposit.Executed += ReturnDeposit_Executed;

            RibbonCommands.LoadDBF.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LoadDBF.Executed += LoadDBF_Executed;
            RibbonCommands.ShowINDateList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowINDateList.Executed += ShowINDateList_Executed;
            RibbonCommands.ShowTDateList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTDateList.Executed += ShowTDateList_Executed;
            RibbonCommands.ShowBDateList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowBDateList.Executed += ShowBDateList_Executed;

            RibbonCommands.ShowYieldOFZList.Executed += ShowYieldOFZList_Executed;

            RibbonCommands.ShowTemplatesManager.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTemplatesManager.Executed += ShowTemplatesManager_Executed;

            RibbonCommands.ShowExchangeRateManager.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowExchangeRateManager.Executed += ShowExchangeRateManager_Executed;

            RibbonCommands.EditRate.CanExecute += EditRate_CanExecute;
            RibbonCommands.EditRate.Executed += EditRate_Executed;

            RibbonCommands.PrintOrder.CanExecute += PrintOrder_CanExecute;
            RibbonCommands.PrintOrder.Executed += PrintOrder_Executed;

            RibbonCommands.PrintBankPersons.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintBankPersons.Executed += PrintBankPersons_Executed;

            RibbonCommands.CreatePenalty.CanExecute += CreatePenalty_CanExecute;
            RibbonCommands.CreatePenalty.Executed += CreatePenalty_Executed;

            RibbonCommands.ShowNetWealthsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowNetWealthsList.Executed += ShowNetWealthsList_Executed;

            RibbonCommands.ShowNetWealthsSumList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowNetWealthsSumList.Executed += ShowNetWealthsSumList_Executed;

            RibbonCommands.ShowActivesMarketCost.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowActivesMarketCost.Executed += ShowActivesMarketCost_Executed;
            RibbonCommands.ShowActivesMarketCostScope.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowActivesMarketCostScope.Executed += ShowActivesMarketCostScope_Executed;
            RibbonCommands.ShowZLTransfersList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowZLTransfersList.Executed += ShowZLTransfersList_Executed;
            RibbonCommands.ShowViolationsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowViolationsList.Executed += ShowViolationsList_Executed;
            RibbonCommands.ShowSPNMovementsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSPNMovementsList.Executed += ShowSPNMovementsList_Executed;
            RibbonCommands.ShowSPNMovementsUKList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSPNMovementsUKList.Executed += ShowSPNMovementsUKList_Executed;
            RibbonCommands.ShowOVSITransferList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowOVSITransferList.Executed += ShowOVSITransferList_Executed;
            RibbonCommands.ShowOVSITransferList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowArchiveOVSITransfersList.Executed += ShowArchiveOVSITransfersList_Executed;
            RibbonCommands.CreateOVSIReqTransfer.CanExecute += CreateOVSIReqTransfer_CanExecute;
            RibbonCommands.CreateOVSIReqTransfer.Executed += CreateOVSIReqTransfer_Executed;

            RibbonCommands.ShowMQSettings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowMQSettings.Executed += ShowMQSettings_Executed;
            RibbonCommands.LoadODKReports.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LoadODKReports.Executed += LoadODKReports_Executed;
            RibbonCommands.LoadEDOReports.CanExecute += CanExecuteIsTrue;
            RibbonCommands.LoadEDOReports.Executed += LoadEDOReports_Executed;
            RibbonCommands.Line030Calculation.CanExecute += CanExecuteIsTrue;
            RibbonCommands.Line030Calculation.Executed += Line030Calculation_Executed;

            RibbonCommands.ShowJournal.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowJournal.Executed += ShowJournal_Executed;

            //Работа с СИ
            RibbonCommands.CreateSI.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateSI.Executed += CreateSI_Executed;
            RibbonCommands.CreateSIContract.CanExecute += CreateSIContract_CanExecute;
            RibbonCommands.CreateSIContract.Executed += CreateSIContract_Executed;
            RibbonCommands.CreateSIContact.CanExecute += CreateSIContact_CanExecute;
            RibbonCommands.CreateSIContact.Executed += CreateSIContact_Executed;
            RibbonCommands.CreateViolationCategory.Executed += CreateViolationCategory_Executed;
            RibbonCommands.CreateViolationCategory.CanExecute += CreateViolationCategory_CanExecute;
            RibbonCommands.ShowSIList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSIList.Executed += ShowSIList_Executed;
            RibbonCommands.ShowSIContactList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSIContactList.Executed += ShowSIContactList_Executed;
            RibbonCommands.ShowSIContractsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSIContractsList.Executed += ShowSIContractsList_Executed;
            RibbonCommands.ShowVRContractsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRContractsList.Executed += ShowVRContractsList_Executed;
            RibbonCommands.ShowViolationCategoriesList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowViolationCategoriesList.Executed += ShowViolationCategoriesList_Executed;
            RibbonCommands.EditSIName.CanExecute += EditSIName_CanExecute;
            RibbonCommands.EditSIName.Executed += EditSIName_Executed;
            RibbonCommands.SendEmailsToUK.CanExecute += CanExecuteIsTrue;
            RibbonCommands.SendEmailsToUK.Executed += SendEmailsToUK_Executed;

            //Работа с ВР 
            RibbonCommands.ShowTransferVRWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTransferVRWizard.Executed += ShowTransferVRWizard_Executed;

            RibbonCommands.ShowArchiveVRTransfersList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowArchiveVRTransfersList.Executed += ShowArchiveVRTransfersList_Executed;
            RibbonCommands.ShowVRTransferList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRTransferList.Executed += ShowVRTransferList_Executed;
            RibbonCommands.CreateVRTransfer.Executed += CreateVRTransfer_Executed;
            RibbonCommands.CreateVRTransfer.CanExecute += CreateVRTransfer_CanExecute;
            RibbonCommands.CreateVRReqTransfer.Executed += CreateVRReqTransfer_Executed;
            RibbonCommands.CreateVRReqTransfer.CanExecute += CreateVRReqTransfer_CanExecute;

            RibbonCommands.ShowZLTransfersVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowZLTransfersVRList.Executed += ShowZLTransfersVRList_Executed;
            RibbonCommands.ShowSPNMovementsVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSPNMovementsVRList.Executed += ShowSPNMovementsVRList_Executed;
            RibbonCommands.ShowSPNMovementsUKVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowSPNMovementsUKVRList.Executed += ShowSPNMovementsUKVRList_Executed;
            RibbonCommands.RecalcUKVRPortfolios.CanExecute += CanExecuteIsTrue;
            RibbonCommands.RecalcUKVRPortfolios.Executed += RecalcUKVRPortfolios_Executed;
            RibbonCommands.CreatePrintReqVR.Executed += CreatePrintReqVR_Executed;
            RibbonCommands.CreatePrintReqVR.CanExecute += CreatePrintReqVR_CanExecute;
            RibbonCommands.ToUKEnterTransferActDataVR.CanExecute += ToUKEnterTransferActDataVR_CanExecute;
            RibbonCommands.ToUKEnterTransferActDataVR.Executed += EnterTransferActDataVR_Executed;
            RibbonCommands.CreateVRRequest.Executed += CreateVRRequest_Executed; // Формирование требования
            RibbonCommands.CreateVRRequest.CanExecute += CreateVRRequest_CanExecute;
            RibbonCommands.FromUKEnterRequirementDataVR.CanExecute += FromUKEnterRequirementDataVR_CanExecute;
            RibbonCommands.FromUKEnterRequirementDataVR.Executed += FromUKEnterRequirementDataVR_Executed;
            RibbonCommands.FromUKEnterGetRequirementDateVR.CanExecute += FromUKEnterGetRequirementDateVR_CanExecute;
            RibbonCommands.FromUKEnterGetRequirementDateVR.Executed += FromUKEnterGetRequirementDateVR_Executed;
            RibbonCommands.FromUKEnterTransferActDataVR.CanExecute += FromUKEnterTransferActDataVR_CanExecute;
            RibbonCommands.FromUKEnterTransferActDataVR.Executed += EnterTransferActDataVR_Executed;
            RibbonCommands.RollbackStatusVR.CanExecute += RollbackStatusVR_CanExecute;
            RibbonCommands.RollbackStatusVR.Executed += RollbackStatus_Executed;

            RibbonCommands.ShowNetWealthsVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowNetWealthsVRList.Executed += ShowNetWealthsVRList_Executed;
            RibbonCommands.ShowNetWealthsVRSumList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowNetWealthsVRSumList.Executed += ShowNetWealthsVRSumList_Executed;
            RibbonCommands.ShowActivesMarketVRCost.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowActivesMarketVRCost.Executed += ShowActivesMarketVRCost_Executed;
            RibbonCommands.ShowActivesMarketVRCostScope.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowActivesMarketVRCostScope.Executed += ShowActivesMarketVRCostScope_Executed;
            RibbonCommands.ShowF40VRList.Executed += ShowF40VRList_Executed;
            RibbonCommands.ShowF40VRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF50VRList.Executed += ShowF50VRList_Executed;
            RibbonCommands.ShowF50VRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF60VRList.Executed += ShowF60VRList_Executed;
            RibbonCommands.ShowF60VRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowYieldsVRList.Executed += ShowYieldsVRList_Executed;
            RibbonCommands.ShowYieldsVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowOwnFundsVRList.Executed += ShowOwnFundsVRList_Executed;
            RibbonCommands.ShowOwnFundsVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF70VRList.Executed += ShowF70VRList_Executed;
            RibbonCommands.ShowF70VRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateYieldVR.Executed += CreateYieldVR_Executed;
            RibbonCommands.CreateYieldVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowF80VRList.Executed += ShowF80VRList_Executed;
            RibbonCommands.ShowF80VRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowViolationsVRList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowViolationsVRList.Executed += ShowViolationsVRList_Executed;






            RibbonCommands.ShowInsuranceListVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowInsuranceListVR.Executed += ShowInsuranceListVR_Executed;
            RibbonCommands.ShowInsuranceArchListVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowInsuranceArchListVR.Executed += ShowInsuranceArchListVR_Executed;
            RibbonCommands.CreateInsuranceVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateInsuranceVR.Executed += CreateInsuranceVR_Executed;
            RibbonCommands.ShowVRCorrespondenceList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRCorrespondenceList.Executed += ShowVRCorrespondenceList_Executed;
            RibbonCommands.ShowVRArchiveList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRArchiveList.Executed += ShowVRArchiveList_Executed;
            RibbonCommands.ShowVRControlList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRControlList.Executed += ShowVRControlList_Executed;
            RibbonCommands.CreateVRDocument.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateVRDocument.Executed += CreateVRDocument_Executed;
            RibbonCommands.CreateVRLinkedDocument.CanExecute += CreateVRLinkedDocument_CanExecute;
            RibbonCommands.CreateVRLinkedDocument.Executed += CreateVRLinkedDocument_Executed;

            RibbonCommands.ShowVRF402List.Executed += ShowVRF402List_Executed;
            RibbonCommands.ShowVRF402List.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRDemandList.Executed += ShowVRDemandList_Executed;
            RibbonCommands.ShowVRDemandList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRF402PFRActions.Executed += ShowVRF402PFRActions_Executed;
            RibbonCommands.ShowVRF402PFRActions.CanExecute += ShowVRF402PFRActions_CanExecute;
            RibbonCommands.CreateVROrderToPay.CanExecute += CreateVROrderToPay_CanExecute;
            RibbonCommands.CreateVROrderToPay.Executed += CreateVROrderToPay_Executed;
            RibbonCommands.VROrderSent.CanExecute += VROrderSent_CanExecute;
            RibbonCommands.VROrderSent.Executed += VROrderSent_Executed;
            RibbonCommands.VROrderDelivered.CanExecute += VROrderDelivered_CanExecute;
            RibbonCommands.VROrderDelivered.Executed += VROrderDelivered_Executed;
            RibbonCommands.VROrderPaid.CanExecute += VROrderPaid_CanExecute;
            RibbonCommands.VROrderPaid.Executed += VROrderPaid_Executed;

            RibbonCommands.ShowVRYearPlanWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRYearPlanWizard.Executed += ShowVRYearPlanWizard_Executed;

            RibbonCommands.ShowVRForYearPlanWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRForYearPlanWizard.Executed += ShowVRYForYearPlanWizard_Executed;

            RibbonCommands.ShowVRPlanCorrectionWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRPlanCorrectionWizard.Executed += ShowVRPlanCorrectionWizard_Executed;
            RibbonCommands.ShowVRMonthTransferCreationWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRMonthTransferCreationWizard.Executed += ShowVRMonthTransferCreationWizard_Executed;
            RibbonCommands.ShowVRAssignPaymentsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRAssignPaymentsList.Executed += ShowVRAssignPaymentsList_Executed;
            RibbonCommands.ShowVRAssignPaymentsArchiveList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRAssignPaymentsArchiveList.Executed += ShowVRAssignPaymentsArchiveList_Executed;
            RibbonCommands.CreateVRUKPaymentPlan.CanExecute += CreateVRUKPaymentPlan_CanExecute;
            RibbonCommands.CreateVRUKPaymentPlan.Executed += CreateVRUKPaymentPlan_Executed;
            RibbonCommands.PrintAPVRMonthPlan.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintAPVRMonthPlan.Executed += PrintAPVRMonthPlan_Executed;
            RibbonCommands.PrintAPVRYearPlan.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintAPVRYearPlan.Executed += PrintAPVRYearPlan_Executed;
            RibbonCommands.PrintVRYearPayment.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintVRYearPayment.Executed += PrintVRYearPayment_Executed;

            RibbonCommands.ShowVRReports.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRReports.Executed += ShowVRReports_Executed;
            RibbonCommands.ShowVRInvDecl.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowVRInvDecl.Executed += ShowVRInvDecl_Executed;

            RibbonCommands.ShowSPNReport01.Executed += ShowSPNReport01_Executed;
            RibbonCommands.ShowSPNReport02.Executed += ShowSPNReport02_Executed;
            RibbonCommands.ShowSPNReport03.Executed += ShowSPNReport03_Executed;
            RibbonCommands.ShowSPNReport17.Executed += ShowSPNReport17_Executed;
            RibbonCommands.ShowSPNReport18.Executed += ShowSPNReport18_Executed;
            RibbonCommands.ShowSPNReport19.Executed += ShowSPNReport19_Executed;
            RibbonCommands.ShowSPNReport20.Executed += ShowSPNReport20_Executed;
            RibbonCommands.ShowSPNReport40.Executed += ShowSPNReport40_Executed;
            RibbonCommands.ShowSPNReport39.Executed += ShowSPNReport39_Executed;

            RibbonCommands.ReportImportVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ReportImportVR.Executed += ReportImportVR_Executed;

            RibbonCommands.ReportExportVR.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ReportExportVR.Executed += ReportExportVR_Executed;
            RibbonCommands.OnesDataVR.CanExecute += OnesDataVR_CanExecute;
            RibbonCommands.OnesDataVR.Executed += OnesDataVR_Executed;


            //Пересчет портфелей УК
            RibbonCommands.RecalcUKPortfolios.CanExecute += CanExecuteIsTrue;
            RibbonCommands.RecalcUKPortfolios.Executed += RecalcUKPortfolios_Executed;
            //Мастеры пеерчислений
            RibbonCommands.ShowTransferWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowTransferWizard.Executed += ShowTransferWizard_Executed;
            RibbonCommands.ShowRopsAsvTransferCreationWizard.Executed += ShowRopsAsvTransferCreationWizard_Executed;
            RibbonCommands.ShowPlanCorrectionWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPlanCorrectionWizard.Executed += ShowSIPlanCorrectionWizard_Executed;
            RibbonCommands.ShowMonthTransferCreationWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowMonthTransferCreationWizard.Executed += ShowSIMonthTransferCreationWizard_Executed;
            RibbonCommands.ShowYearPlanWizard.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowYearPlanWizard.Executed += ShowSIYearPlanWizard_Executed;
            //Печать планов правопреемников
            RibbonCommands.PrintAPSIMonthPlan.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintAPSIMonthPlan.Executed += PrintAPSIMonthPlan_Executed;
            RibbonCommands.PrintAPSIYearPlan.CanExecute += CanExecuteIsTrue;
            RibbonCommands.PrintAPSIYearPlan.Executed += PrintAPSIYearPlan_Executed;
            //Перевод перечислений по статусам в УК
            RibbonCommands.CreatePrintReq.Executed += CreatePrintReq_Executed;
            RibbonCommands.CreatePrintReq.CanExecute += CreatePrintReq_CanExecute;
            //Перевод перечислений по статусам из УК
            RibbonCommands.CreateSIRequest.Executed += CreateSIRequest_Executed; // Формирование требования
            RibbonCommands.CreateSIRequest.CanExecute += CreateSIRequest_CanExecute;
            RibbonCommands.FromUKEnterRequirementData.CanExecute += FromUKEnterRequirementData_CanExecute;
            //Реквизиты требования
            RibbonCommands.FromUKEnterRequirementData.Executed += FromUKEnterRequirementData_Executed;
            RibbonCommands.FromUKEnterGetRequirementDate.CanExecute += FromUKEnterGetRequirementDate_CanExecute;
            //Дата вручения требования
            RibbonCommands.FromUKEnterGetRequirementDate.Executed += FromUKEnterGetRequirementDate_Executed;
            //Перевод перечислений по статусам - Акт передачи и в УК и из УК
            RibbonCommands.FromUKEnterTransferActData.CanExecute += FromUKEnterTransferActData_CanExecute;
            //Акт передачи
            RibbonCommands.FromUKEnterTransferActData.Executed += EnterTransferActData_Executed;
            RibbonCommands.ToUKEnterTransferActData.CanExecute += ToUKEnterTransferActData_CanExecute;
            //Акт передачи
            RibbonCommands.ToUKEnterTransferActData.Executed += EnterTransferActData_Executed;
            //Перевод перечислений по статусам - ввод данных платежного поручения
            RibbonCommands.ShowUKList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowUKList.Executed += ShowUKList_Executed;
            RibbonCommands.ShowUKListArchive.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowUKListArchive.Executed += ShowUKListArchive_Executed;


            RibbonCommands.ShowAssignPaymentsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowAssignPaymentsList.Executed += ShowSIAssignPaymentsList_Executed;
            RibbonCommands.ShowAssignPaymentsArchiveList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowAssignPaymentsArchiveList.Executed += ShowSIAssignPaymentsArchiveList_Executed;
            RibbonCommands.CreateUKPaymentPlan.CanExecute += CreateSIUKPaymentPlan_CanExecute;
            RibbonCommands.CreateUKPaymentPlan.Executed += CreateSIUKPaymentPlan_Executed;

            RibbonCommands.ShowBoardList.Executed += ShowBoardList_Executed;
            RibbonCommands.ShowMarketPricesList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowMarketPricesList.Executed += ShowMarketPricesList_Executed;
            RibbonCommands.AddMarketPrice.CanExecute += AddMarketPrice_CanExecute;
            RibbonCommands.AddMarketPrice.Executed += AddMarketPrice_Executed;
            RibbonCommands.ShowDBFSettings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowDBFSettings.Executed += ShowDBFSettings_Executed;
            RibbonCommands.ShowBDateXmlImport.Executed += ShowBDateXmlImport_Executed;
            RibbonCommands.CreateSIDocument.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateSIDocument.Executed += CreateSIDocument_Executed;
            RibbonCommands.CreateLinkedDocument.CanExecute += CreateLinkedDocument_CanExecute;
            RibbonCommands.CreateLinkedDocument.Executed += CreateLinkedDocument_Executed;

            RibbonCommands.ShowPensionplacementPivot.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowPensionplacementPivot.Executed += ShowPensionplacementPivotList_Executed;

            RibbonCommands.ShowPensionfundtonpfPivot.Executed += ShowPensionpfundtonpfPivot_Executed;
            RibbonCommands.ShowPensionfundtonpfWithSubPivot.Executed += ShowPensionpfundtonpfWithSubPivot_Executed;

            RibbonCommands.ShowPensionfundtonpfList.Executed += ShowPensionfundtonpfList_Executed;
            RibbonCommands.ShowPensionfundtonpfSubtrList.Executed += ShowPensionfundtonpfSubtrList_Executed;
            RibbonCommands.AddPensionfundtonpfReport.Executed += ShowAddPensionfundtonpfEditor_Executed;
            RibbonCommands.AddPensionfundtonpfSubtrReport.Executed += ShowAddPensionfundtonpfWithSubtrEditor_Executed;

            RibbonCommands.ShowInsurepdersonPivot.Executed += ShowInsurepdersonPivot_Executed;

            RibbonCommands.ShowIncomingstatementPivot.Executed += ShowIncomingstatementPivot_Executed;
            RibbonCommands.ShowIncomingstatementList.Executed += ShowIncomingstatementList_Executed;


            RibbonCommands.ShowYieldfunds107Pivot.Executed += ShowYieldfundsPivot107_Executed;
            RibbonCommands.ShowYieldfunds140Pivot.Executed += ShowYieldfundsPivot140_Executed;
            RibbonCommands.ShowPaymentyearratePivot.Executed += ShowAnalyzePaymentyearratePivot_Executed;
            RibbonCommands.ShowPensionplacementList.Executed += ShowPensionplacementReportList_Executed;
            RibbonCommands.ShowAddPensionplacementEditor.Executed += ShowAddPensionplacementEditor_Executed;

            RibbonCommands.ShowPaymentyearrateAddNew.Executed += ShowPaymentyearrateAddNew_Executed;
            RibbonCommands.ShowPaymentyearrateList.Executed += ShowPaymentyearrateList_Executed;
            RibbonCommands.ShowYieldfunds140AddNew.Executed += ShowYieldfunds140AddNew_Executed;
            RibbonCommands.ShowYieldfunds140List.Executed += ShowYieldfunds140List_Executed;
            RibbonCommands.ShowYieldfunds107AddNew.Executed += ShowYieldfunds107AddNew_Executed;
            RibbonCommands.ShowYieldfunds107List.Executed += ShowYieldfunds107List_Executed;
            RibbonCommands.ShowIncomingstatementAddNew.Executed += ShowIncomingstatementAddNew_Executed;
            RibbonCommands.ShowInsurepdersonAddNew.Executed += ShowInsurepdersonAddNew_Executed;
            RibbonCommands.ShowInsurepdersonList.Executed += ShowInsurepdersonList_Executed;
            RibbonCommands.ShowAnalyzeConditionEditor.Executed += ShowAnalyzeConditionEditor_Executed;


            CommandBindings.Add(new CommandBinding(RibbonCommands.ShowSuccessLoadedODKListAll,
                                                   ShowSuccessODKListAll_Executed));
            CommandBindings.Add(new CommandBinding(RibbonCommands.ShowSuccessLoadedODKList,
                                                   ShowSuccessODKList_Executed));

            CommandBindings.Add(new CommandBinding(RibbonCommands.ShowSuccessLoadedODKListVR,
                                                              ShowSuccessODKListVR_Executed));

            CommandBindings.Add(new CommandBinding(RibbonCommands.ShowErrorLoadedODKList,
                                                   ShowErrorODKList_Executed));

            CommandBindings.Add(new CommandBinding(SharedCommands.OpenEdoXmlBody, OpenEdoXmlBodyExecuted));

            CommandBindings.Add(new CommandBinding(SharedCommands.OpenDepClaimXml, OpenDepClaimXmlExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenRejAppFile, OpenRejAppFileExecuted));

            CommandBindings.Add(new CommandBinding(SharedCommands.OpenOnesImportXmlBody, OpenOnesImportXmlBodyExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenOnesExportXmlBody, OpenOnesExportXmlBodyExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.RemoveOnesExportEntry, RemoveOnesExportEntryExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenCBReportExportXmlBody, OpenCBReportExportXmlBodyExecuted));

            CommandBindings.Add(new CommandBinding(SharedCommands.OpenKIPXml, OpenKIPXmlExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenMessage, OpenMessageExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenCBReportApprovementForm, OpenCBReportApprovementFormExecuted));




            CommandBindings.Add(new CommandBinding(SharedCommands.OpenSIRegisterGrid, OpenSIRegisterGridExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenVRRegisterGrid, OpenVRRegisterGridExecuted));
            CommandBindings.Add(new CommandBinding(SharedCommands.OpenNPFRegisterGrid, OpenNPFRegisterGridExecuted));



            RibbonCommands.MFLetterDepositSumm.CanExecute += CanExecuteIsTrue;
            RibbonCommands.MFLetterDepositSumm.Executed += MFLetterDepositSumm_Executed;
            RibbonCommands.MFLetterPortfolioStructure.CanExecute += CanExecuteIsTrue;
            RibbonCommands.MFLetterPortfolioStructure.Executed += MFLetterPortfolioStructure_Executed;
            RibbonCommands.MFLetterFactYield.CanExecute += CanExecuteIsTrue;
            RibbonCommands.MFLetterFactYield.Executed += MFLetterFactYield_Executed;
            RibbonCommands.MFLetterPreSumm.CanExecute += CanExecuteIsTrue;
            RibbonCommands.MFLetterPreSumm.Executed += MFLetterPreSumm_Executed;
            RibbonCommands.MFLetterSellPlan.CanExecute += CanExecuteIsTrue;
            RibbonCommands.MFLetterSellPlan.Executed += MFLetterSellPlan_Executed;


            RibbonCommands.ShowRatingsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowRatingsList.Executed += ShowRatingsList_Executed;
            //RibbonCommands.AddRating.CanExecute += CanExecuteIsTrue;
            RibbonCommands.AddRating.CanExecute += AddRating_CanExecute;
            RibbonCommands.AddRating.Executed += AddRating_Executed;

            RibbonCommands.ImportKO761.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ImportKO761.Executed += ImportKO761_Executed;
            RibbonCommands.ImportOwnCapital.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ImportOwnCapital.Executed += ImportOwnCapital_Executed;


            RibbonCommands.ShowAgencyRatingsList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowAgencyRatingsList.Executed += ShowAgencyRatingsList_Executed;
            RibbonCommands.AddAgencyRating.CanExecute += CanExecuteIsTrue;
            RibbonCommands.AddAgencyRating.Executed += AddAgencyRating_Executed;
            RibbonCommands.ShowRatingAgenciesList.CanExecute += ShowRatingAgenciesList_CanExecute;
            RibbonCommands.ShowRatingAgenciesList.Executed += ShowRatingAgenciesList_Executed;
            RibbonCommands.AddRatingAgency.CanExecute += AddRatingAgency_CanExecute;
            RibbonCommands.AddRatingAgency.Executed += AddRatingAgency_Executed;

            RibbonCommands.ShowDepClaimSelectParamsList.CanExecute += ShowDepClaimSelectParamsList_CanExecute;
            RibbonCommands.ShowDepClaimSelectParamsList.Executed += ShowDepClaimSelectParamsList_Executed;
            RibbonCommands.AddDepClaimSelectParams.CanExecute += AddDepClaimSelectParams_CanExecute;
            RibbonCommands.AddDepClaimSelectParams.Executed += AddDepClaimSelectParams_Executed;

            RibbonCommands.ShowDepClaimMaxList.CanExecute += ShowDepClaimMaxList_CanExecute;
            RibbonCommands.ShowDepClaimMaxList.Executed += ShowDepClaimMaxList_Executed;

            RibbonCommands.ShowDepClaimList.CanExecute += ShowDepClaimList_CanExecute;
            RibbonCommands.ShowDepClaimList.Executed += ShowDepClaimList_Executed;


            RibbonCommands.ShowExportsDepclaimMax.CanExecute += ShowExportsDepclaimMax_CanExecute;
            RibbonCommands.ShowExportsDepclaimMax.Executed += ShowExportsDepclaimMax_Executed;

            RibbonCommands.ImportDepclaimMaxList.CanExecute += ImportDepclaimMaxList_CanExecute;
            RibbonCommands.ImportDepclaimMaxList.Executed += ImportDepclaimMaxList_Executed;

            RibbonCommands.ImportDepclaimList.CanExecute += ImportDepclaimList_CanExecute;
            RibbonCommands.ImportDepclaimList.Executed += ImportDepclaimList_Executed;

            RibbonCommands.ShowExportsDepclaim.CanExecute += ShowExportsDepclaim_CanExecute;
            RibbonCommands.ShowExportsDepclaim.Executed += ShowExportsDepclaim_Executed;

            RibbonCommands.ShowDepClaim2List.CanExecute += ShowDepClaim2List_CanExecute;
            RibbonCommands.ShowDepClaim2List.Executed += ShowDepClaim2List_Executed;

            RibbonCommands.ShowDepClaim2ConfirmList.CanExecute += ShowDepClaim2ConfirmList_CanExecute;
            RibbonCommands.ShowDepClaim2ConfirmList.Executed += ShowDepClaim2ConfirmList_Executed;

            RibbonCommands.ImportDepClaimBatchList.CanExecute += ImportDepClaimBatchList_CanExecute;
            RibbonCommands.ImportDepClaimBatchList.Executed += ImportDepClaimBatchList_Executed;

            RibbonCommands.ImportDepClaim2List.CanExecute += ImportDepClaim2List_CanExecute;
            RibbonCommands.ImportDepClaim2List.Executed += ImportDepClaim2List_Executed;

            RibbonCommands.ExportDepClaim2List.CanExecute += ExportDepClaim2List_CanExecute;
            RibbonCommands.ExportDepClaim2List.Executed += ExportDepClaim2List_Executed;

            RibbonCommands.ImportDepClaim2ConfirmList.CanExecute += ImportDepClaim2ConfirmList_CanExecute;
            RibbonCommands.ImportDepClaim2ConfirmList.Executed += ImportDepClaim2ConfirmList_Executed;

            RibbonCommands.ExportDepClaim2ConfirmList.CanExecute += ExportDepClaim2ConfirmList_CanExecute;
            RibbonCommands.ExportDepClaim2ConfirmList.Executed += ExportDepClaim2ConfirmList_Executed;

            RibbonCommands.CreateDepClaim2Offer.CanExecute += CreateDepClaim2Offer_CanExecute;
            RibbonCommands.CreateDepClaim2Offer.Executed += CreateDepClaim2Offer_Executed;

            RibbonCommands.ShowDepClaim2Offer.CanExecute += ShowDepClaim2Offer_CanExecute;
            RibbonCommands.ShowDepClaim2Offer.Executed += ShowDepClaim2Offer_Executed;

            #region Export Offer

            RibbonCommands.ExportAllDepClaim2Offer.CanExecute += ExportAllDepClaim2Offer_CanExecute;
            RibbonCommands.ExportAllDepClaim2Offer.Executed += ExportAllDepClaim2Offer_Executed;

            RibbonCommands.ExportSingleDepClaim2Offer.CanExecute += ExportSingleDepClaim2Offer_CanExecute;
            RibbonCommands.ExportSingleDepClaim2Offer.Executed += ExportSingleDepClaim2Offer_Executed;

            RibbonCommands.ExportNotAcceptedDepClaimOffer.CanExecute += ExportNotAcceptedDepClaimOffer_CanExecute;
            RibbonCommands.ExportNotAcceptedDepClaimOffer.Executed += ExportNotAcceptedDepClaimOffer_Executed;


            RibbonCommands.ExportDepClaimAllocation.CanExecute += ExportDepClaimAllocation_CanExecute;
            RibbonCommands.ExportDepClaimAllocation.Executed += ExportDepClaimAllocation_Executed;


            RibbonCommands.ExportGroupDepClaim2Offer.CanExecute += ExportGroupDepClaim2Offer_CanExecute;

            RibbonCommands.ExportContractsRegister.Executed += ExportContractsRegister_Executed;
            RibbonCommands.ExportContractsRegister.CanExecute += ExportContractsRegister_CanExecute;

            #endregion Export Offer

            RibbonCommands.ShowDivisionList.CanExecute += ShowDivisionList_CanExecute;
            RibbonCommands.ShowDivisionList.Executed += ShowDivisionList_Executed;
            RibbonCommands.CreateDivision.CanExecute += CreateDivision_CanExecute;
            RibbonCommands.CreateDivision.Executed += CreateDivision_Executed;
            RibbonCommands.CreatePerson.CanExecute += CreatePerson_CanExecute;
            RibbonCommands.CreatePerson.Executed += CreatePerson_Executed;

            RibbonCommands.ExportDepClaimSelectParams.CanExecute += ExportDepClaimSelectParams_CanExecute;
            RibbonCommands.ExportDepClaimSelectParams.Executed += ExportDepClaimSelectParams_Executed;


            RibbonCommands.ExportCutoffRate.CanExecute += ExportCutoffRate_CanExecute;
            RibbonCommands.ExportCutoffRate.Executed += ExportCutoffRate_Executed;

            RibbonCommands.BuildLimits.CanExecute += BuildLimits_CanExecute;
            RibbonCommands.BuildLimits.Executed += BuildLimits_Executed;

            RibbonCommands.RegionReportListInsuredPerson.CanExecute += RegionReportListInsuredPerson_CanExecute;
            RibbonCommands.RegionReportListInsuredPerson.Executed += RegionReportListInsuredPerson_Executed;

            RibbonCommands.RegionReportListNpfNotice.CanExecute += RegionReportListNpfNotice_CanExecute;
            RibbonCommands.RegionReportListNpfNotice.Executed += RegionReportListNpfNotice_Executed;

            RibbonCommands.RegionReportListAssigneePayment.CanExecute += RegionReportListAssigneePayment_CanExecute;
            RibbonCommands.RegionReportListAssigneePayment.Executed += RegionReportListAssigneePayment_Executed;

            RibbonCommands.RegionReportListCashExpenditures.CanExecute += RegionReportListCashExpenditures_CanExecute;
            RibbonCommands.RegionReportListCashExpenditures.Executed += RegionReportListCashExpenditures_Executed;

            RibbonCommands.RegionReportListApplications741.CanExecute += RegionReportListApplications741_CanExecute;
            RibbonCommands.RegionReportListApplications741.Executed += RegionReportListApplications741_Executed;

            RibbonCommands.RegionReportListDeliveryCost.CanExecute += RegionReportListDeliveryCost_CanExecute;
            RibbonCommands.RegionReportListDeliveryCost.Executed += RegionReportListDeliveryCost_Executed;

            RibbonCommands.RegionReportImportExcel.CanExecute += RegionReportImportExcel_CanExecute;
            RibbonCommands.RegionReportImportExcel.Executed += RegionReportImportExcel_Executed;

            RibbonCommands.RegionReportImportXml.CanExecute += RegionReportImportXml_CanExecute;
            RibbonCommands.RegionReportImportXml.Executed += RegionReportImportXml_Executed;

            RibbonCommands.UKReportImport.CanExecute += CanExecuteIsTrue;
            RibbonCommands.UKReportImport.Executed += UKReportImport_Executed;

            RibbonCommands.RegionReportExport.CanExecute += RegionReportExport_CanExecute;
            RibbonCommands.RegionReportExport.Executed += RegionReportExport_Executed;

            RibbonCommands.ShowNpfCorrespondenceList.CanExecute += ShowNpfCorrespondenceList_CanExecute;
            RibbonCommands.ShowNpfCorrespondenceList.Executed += ShowNpfCorrespondenceList_Executed;

            RibbonCommands.ShowNpfControlList.CanExecute += ShowNpfControlList_CanExecute;
            RibbonCommands.ShowNpfControlList.Executed += ShowNpfControlList_Executed;

            RibbonCommands.ShowNpfArchiveList.CanExecute += ShowNpfArchiveList_CanExecute;
            RibbonCommands.ShowNpfArchiveList.Executed += ShowNpfArchiveList_Executed;


            RibbonCommands.CreateNpfDocument.CanExecute += CreateNpfDocument_CanExecute;
            RibbonCommands.CreateNpfDocument.Executed += CreateNpfDocument_Executed;

            RibbonCommands.ShowDelayedPaymentClaimList.CanExecute += ShowDelayedPaymentClaimList_CanExecute;
            RibbonCommands.ShowDelayedPaymentClaimList.Executed += ShowDelayedPaymentClaimList_Executed;

            RibbonCommands.ShowPensionNotificationList.CanExecute += ShowPensionNotificationList_CanExecute;
            RibbonCommands.ShowPensionNotificationList.Executed += ShowPensionNotificationList_Executed;

            RibbonCommands.CreateDelayedPaymentClaim.CanExecute += CreateDelayedPaymentClaim_CanExecute;
            RibbonCommands.CreateDelayedPaymentClaim.Executed += CreateDelayedPaymentClaim_Executed;

            RibbonCommands.CreatePensionNotification.CanExecute += CreatePensionNotification_CanExecute;
            RibbonCommands.CreatePensionNotification.Executed += CreatePensionNotification_Executed;

            RibbonCommands.GenerateDeposit.CanExecute += GenerateDeposit_CanExecute;
            RibbonCommands.GenerateDeposit.Executed += GenerateDeposit_Executed;

            RibbonCommands.KBKList.Executed += ShowKBKList_Executed;
            RibbonCommands.KBKList.CanExecute += KBKList_CanExecute;
            RibbonCommands.CreateKBK.Executed += CreateKBK_Executed;

            RibbonCommands.AddNPFErrorCode.Executed += AddNPFErrorCode_Executed;
            RibbonCommands.AddNPFErrorCode.CanExecute += CanExecuteIsTrue;

            RibbonCommands.AddNPFMatchCode.Executed += AddNPFMatchCode_Executed;
            RibbonCommands.AddNPFMatchCode.CanExecute += CanExecuteIsTrue;

            RibbonCommands.ShowNPFErrorCodeList.Executed += ShowNPFErrorCodeList_Executed;
            RibbonCommands.ShowNPFErrorCodeList.CanExecute += CanExecuteIsTrue;

            RibbonCommands.ShowNPFMatchCodeList.Executed += ShowNPFMatchCodeList_Executed;
            RibbonCommands.ShowNPFMatchCodeList.CanExecute += CanExecuteIsTrue;

            RibbonCommands.ShowDKMonthList.Executed += ShowDKMonthList_Executed;
            RibbonCommands.ShowDKYearList.Executed += ShowDKYearList_Executed;

            RibbonCommands.ShowReportIPUKTotal.Executed += ShowReportIPUKTotal_Executed;
            RibbonCommands.ShowReportIPUK.Executed += ShowReportIPUK_Executed;
            RibbonCommands.ShowReportBalanceUK.Executed += ShowReportBalanceUK_Executed;

            RibbonCommands.ShowPaymentDetailList.Executed += ShowPaymentDetailList_Executed;
            RibbonCommands.ShowCreatePaymentDetail.Executed += ShowCreatePaymentDetail_Executed;



            RibbonCommands.ShowSelectCommonPPNPF.CanExecute += ShowSelectCommonPPNPF_CanExecute;
            RibbonCommands.ShowSelectCommonPPNPF.Executed += ShowSelectCommonPPNPF_Executed;

            RibbonCommands.ShowReportDeposits.Executed += ShowReportDeposits_Executed;
            RibbonCommands.ShowReportRSASCA.Executed += ShowReportRSASCA_Executed;

            RibbonCommands.ShowUKAccountsReport.Executed += ShowUKAccountsReport_Executed;
            RibbonCommands.ShowUKListReport.Executed += ShowUKListReport_Executed;

            RibbonCommands.ShowReportF060.Executed += ShowReportF060_Executed;
            RibbonCommands.ShowReportF070.Executed += ShowReportF070_Executed;
            RibbonCommands.ShowReportProfitability.Executed += ShowReportProfitability_Executed;
            RibbonCommands.ShowReportOwnedFounds.Executed += ShowReportOwnedFounds_Executed;
            RibbonCommands.ShowReportInvestRatio.Executed += ShowReportInvestRatio_Executed;

            RibbonCommands.OnesDataNPF.CanExecute += OnesDataNPF_CanExecute;
            RibbonCommands.OnesDataNPF.Executed += OnesDataNPF_Executed;
            RibbonCommands.OnesDataDEPO.CanExecute += OnesDataDEPO_CanExecute;
            RibbonCommands.OnesDataDEPO.Executed += OnesDataDEPO_Executed;

            #region ROPS
            RibbonCommands.ShowROPSSumList.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowROPSSumList.Executed += ShowROPSSumList_Executed;

            RibbonCommands.CreateROPSSum.CanExecute += CanExecuteIsTrue;
            RibbonCommands.CreateROPSSum.Executed += CreateROPSSum_Executed;
            #endregion ROPS
        }


        private void OpenCBReportApprovementFormExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var item = e.Parameter as LoginMessageItem;
            if (item != null)
                ViewModelBase.DialogHelper.OpenCBReportApprovementStatus(item.refID, true);
        }


        private void LetterReqTransferMoneyASV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var reqTransId = ((TransfersSIListView)App.DashboardManager.GetActiveView()).GetSelectedReqTransferID();

                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
                var register = transfer.GetTransferRegister();

                var selectDate = true;
                string defString;
                string aboutString;
                if (e.Command == RibbonCommands.LetterReqTransferSPNOneTime)
                {
                    defString = "Требование о перечислении СПН (Единовременная)";
                    aboutString = "Создание требования о перечислении СПН (Единовременная)";
                }
                else if (e.Command == RibbonCommands.LetterTransferSPNAccumOneTime)
                {
                    defString = "Письмо о передаче СПН (нуль Единовременная)";
                    aboutString = "Создание письма о передаче СПН (нуль Единовременная)";
                }
                else if (e.Command == RibbonCommands.LetterReqTransferSPNOwners)
                {
                    defString = "Требование о перечислении СПН (Правопреемники)";
                    aboutString = "Создание требования о перечислении СПН (Правопреемники)";
                }
                else if (e.Command == RibbonCommands.LetterReqTransferMoneyROPS)
                {
                    defString = "Требование о перечислении средств (РОПС)";
                    aboutString = "Создание требования о перечислении средств (РОПС)";
                    selectDate = false;
                }
                else if (e.Command == RibbonCommands.LetterReqTransferMoneyASV)
                {
                    defString = "Требование о перечислении средств (АСВ)";
                    aboutString = "Создание требования о перечислении средств (АСВ)";
                    selectDate = false;
                }
                else throw new Exception("Неизвестная команда для формирования письма СИ!");

                var dlg = new SelectSILettersYearMonthDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                dlg.DataContext = new SILettersSelectYearMonthDlgViewModel(
                    register.RegisterDate?.Year ?? DateTime.Now.Year,
                    register.RegisterDate?.Month ?? DateTime.Now.Month, selectDate);
                if (dlg.ShowDialog() != true) return;

                using (Loading.StartNew($"{aboutString} ..."))
                {
                    var year = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedYear ?? 0;
                    var month = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedMonth ?? 0;
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        $"{defString}.doc",
                        aboutString,
                        year,
                        month);
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }



        #endregion

        #region BindRibbonOtherCommands()

        private void BindRibbonOtherCommands()
        {
            RibbonCommands.ShowServerSettings.CanExecute += CanExecuteIsTrue;
            RibbonCommands.ShowServerSettings.Executed += ShowServerSettings_Executed;


            RibbonCommands.ShowClientThemes.CanExecute += ShowClientThemes_CanExecute;
            RibbonCommands.ShowClientThemes.Executed += ShowClientThemes_Executed;

            RibbonCommands.ResetClientSettings.CanExecute += (s, e) => e.CanExecute = true;
            RibbonCommands.ResetClientSettings.Executed += (s, e) =>
            {
                if (DXMessageBox.Show(
                    "Удалить пользовательские настройки?",
                    "Удаление пользовательских настроек",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question) ==
                    MessageBoxResult.Yes)
                    App.UserSettingsManager.Reset();
            };
        }

        #endregion

        #region Show implementations

        private static void ShowSuccessODKListAll_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LoadedODKLogSuccessListView), ViewModelState.Read, "Журнал регистрации документов ЭДО");
        }

        private static void ShowSuccessODKList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LoadedODKLogSuccessSIListView), ViewModelState.Read, "Журнал регистрации документов ЭДО СИ");
        }

        private static void ShowErrorODKList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LoadedODKLogErrorListView), ViewModelState.Read, "Список ошибок при загрузке");
        }

        private static void ShowRatesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatesListView), ViewModelState.Read, "Курсы валют");
        }

        private static void ShowMissedRatesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MissedRatesListView), ViewModelState.Read, "Пропущенные даты");
        }

        private static void ShowSecuritiesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SecuritiesListView), ViewModelState.Read, "Ценные бумаги");
        }

        private static void ShowSellReportsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SaleReportsListView), ViewModelState.Read, "Отчеты по продажам");
        }

        private static void ShowBuyReportsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BuyReportsListView), ViewModelState.Read, "Отчеты по покупкам");
        }

        private static void ShowPFRBranchesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PFRBranchesListView), ViewModelState.Read, "Справочник отделений ПФР");
        }

        private static void ShowDueList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueListView), ViewModelState.Read, "Страховые взносы");
        }

        private static void ShowACCList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AccountBalanceListView), ViewModelState.Read, "Счета");
        }

        private static void ShowBalance_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BalanceView), ViewModelState.Read, new List<string> { "Сальдо", "Бэк-офис - Сальдо - Сальдо" }, "Сальдо");
        }

        private static void ShowROPSSumList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ROPSSumListView), ViewModelState.Read, "Распоряжения по РОПС");
        }
        private static void CreateROPSSum_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ROPSSumView), ViewModelState.Create, "Распоряжение по РОПС");
        }

        private static void ShowTempAllocationList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TempAllocationListView), ViewModelState.Read, "Временное размещение/реализация активов");
        }

        private static void ShowIncomeOnSecuritiesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(IncomeSecurityListView), ViewModelState.Read, "Поступления по ЦБ и депозитам");
        }

        private static void ShowInsuranceList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // App.dashboardManager.OpenNewTab(typeof(InsuranceListView), ViewModelState.Read, "Договора страхования");
            App.DashboardManager.OpenNewTab<InsuranceSIListView, InsuranceSIListViewModel>("Договора страхования СИ", ViewModelState.Read);
        }

        private static void ShowInsuranceArchList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // App.dashboardManager.OpenNewTab(typeof(InsuranceArchiveListView), ViewModelState.Read, "Архив страховых договоров");
            App.DashboardManager.OpenNewTab<InsuranceArchiveSIListView, InsuranceArchiveSIListViewModel>("Архив страховых договоров СИ", ViewModelState.Read);

        }

        private static void ShowTransferToNPFListArchive_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferFromOrToNPFArchiveListView), ViewModelState.Read, "Архив перечислений НПФ");
        }

        private static void ShowTransferToNPFList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferToNPFListView), ViewModelState.Read, "Передача средств в НПФ");
        }

        private static void ShowTransferFromNPFList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferFromNPFListView), ViewModelState.Read, "Возврат средств из НПФ");
        }

        private static void ShowDueDSVList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DsvListView), ViewModelState.Read, "Страховые взносы ДСВ");
        }

        private static void ShowOrdersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OrdersListView), ViewModelState.Read, "Список поручений");
        }

        private static void ShowDepositsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepositsListView), ViewModelState.Read, (object)(long)1, "Перечень депозитов");
        }

        private static void ShowDepositsByPortfolioList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepositsListByPortfolioView), ViewModelState.Read, (object)((long)1), "Перечень депозитов");
        }

        private static void ShowPaymentOrderList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PaymentOrderListView), ViewModelState.Read, (object)((long)1), "Загруженные п/п по депозитам");
        }

        private static void ShowArchiveDepositsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepositsArchiveListView), ViewModelState.Read, (object)((long)2), "Архив депозитов");
        }

        private static void ShowContragentsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFListView), ViewModelState.Read, "Перечень НПФ");
        }

        private static void ShowBanksList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BanksList), ViewModelState.Read, "Перечень банков");
        }

        private static void ShowBanksList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private static void ShowViolationCategoriesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ViolationCategoriesListView), ViewModelState.Read, "Справочник нарушений");
        }

        private static void ShowCostsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CostsListView), ViewModelState.Read, "Расходы по сделкам с ЦБ");
        }

        private static void ShowSchilsCostsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SchilsCostsListView), ViewModelState.Read, "Расходы по ведению СЧ ИЛС");
        }

        private static void ShowAccountsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BankAccountsList), ViewModelState.Read, "Перечень счетов");
        }

        private static void ShowDeadZLList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DeadZLListView), ViewModelState.Read, "Перечень умерших ЗЛ");
        }

        private static void ShowPFRAccountsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PFRAccountsListView), ViewModelState.Read, "Перечень счетов ПФР");
        }

        private static void ShowPortfoliosList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PortfoliosListView), ViewModelState.Read, "Перечень портфелей ПФР");
        }

        private static void ShowRegistersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RegistersListView), ViewModelState.Read, "Перечень реестров");
        }

        private static void ShowArchiveRegistersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RegistersArchiveListView), ViewModelState.Read, "Архив реестров");
        }

        private static void ShowReportsNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ReportsView), ViewModelState.Read, ReportFolderType.NPF, "Отчеты");
        }
        private static void ShowReportsSI_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ReportsView), ViewModelState.Read, ReportFolderType.SI, "Отчеты");
        }
        private static void ShowReportsCB_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ReportsView), ViewModelState.Read, ReportFolderType.CB, "Отчеты");
        }

        private static void ShowServerSettings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ServerSettingsView), ViewModelState.Edit, "Настройки сервера");
        }

        private static void ShowTemplatesManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TemplatesListView), ViewModelState.Edit, "Управление шаблонами");
        }

        private static void ShowExchangeRateManager_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ExchangeRateView), ViewModelState.Edit, "Управление курсами валют");
        }

        private static void ShowZLRedistList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ZLRedistListView), ViewModelState.Read, "Перечень перераспределений ЗЛ");
        }

        private void ImportNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long ownerId;

            var view = App.DashboardManager.GetActiveView() as RegistersListView;
            if (view != null && view.IsRegisterSelected())
                ownerId = view.GetSelectedRegisterID();
            else
                ownerId = 0;

            if (App.DashboardManager.GetActiveViewType() == typeof(RegisterView))
            {
                var vm = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;
                if (vm != null)
                {
                    if (vm.ID > 0)
                        ownerId = vm.ID;
                    else
                        ownerId = -1;
                }
            }

            if (ownerId > 0)
            {
                var dlg = new ImportNPFDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                using (Loading.StartNew())
                {
                    var vm = new ImportNPFDlgViewModel(ownerId);
                    dlg.DataContext = vm;
                    vm.PostOpen(dlg.Title);
                }
                dlg.ShowDialog();
            }
            else
            {
                DXMessageBox.Show("Произошла ошибка при определении реестра НПФ.", "Ошибка выбора реестра");
            }
        }

        private static void ImportNPF_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var regListUC = App.DashboardManager.GetActiveView() as RegistersListView;
            var rVM = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;

            if (regListUC != null)
            {
                if (regListUC.IsRegisterSelected())
                {
                    e.CanExecute = true;
                    return;
                }
            }

            if (rVM != null && rVM.ID > 0)
            {
                e.CanExecute = true;
                return;
            }

            e.CanExecute = false;
        }

        private static void ImportNPFTemp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long ownerId;

            var view = App.DashboardManager.GetActiveView() as NPFTempAllocationListView;
            if (view != null && view.IsRegisterSelected() && view.GetSelectedTempAllocationType() == RegisterIdentifier.TempAllocation)
                ownerId = view.GetSelectedTempAllocationID();
            else
                ownerId = 0;

            var card = App.DashboardManager.GetActiveViewModel() as SPNAllocationViewModel;

            if (card != null && card.Kind == RegisterIdentifier.TempAllocation)
            {
                ownerId = card.ID > 0 ? card.ID : -1;
            }

            if (ownerId > 0)
            {
                ImportNPFDlg dlg;
                using (Loading.StartNew())
                {
                    var vm = new ImportNPFDlgViewModel(ownerId);
                    dlg = DialogHelper.PrepareDialog(new ImportNPFDlg(), vm);
                    vm.PostOpen(dlg.Title);
                }
                dlg.ShowDialog();
            }
            else
            {
                DXMessageBox.Show("Произошла ошибка при определении реестра НПФ.", "Ошибка выбора реестра");
            }
        }

        private static void ImportNPFTemp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var regListUC = App.DashboardManager.GetActiveView() as NPFTempAllocationListView;
            var rVm = App.DashboardManager.GetActiveViewModel() as SPNAllocationViewModel;

            if (regListUC != null)
            {
                if (regListUC.IsRegisterSelected() && regListUC.GetSelectedTempAllocationType() == RegisterIdentifier.TempAllocation)
                {
                    e.CanExecute = true;
                    return;
                }
            }

            if (rVm != null && rVm.ID > 0 && rVm.Kind == RegisterIdentifier.TempAllocation)
            {
                e.CanExecute = true;
                return;
            }

            e.CanExecute = false;
        }

        private void ShowNPFTempAllocationList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFTempAllocationListView), ViewModelState.Create, "Временное размещение");
        }

        private void AddSPNAllocation_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNAllocationView), ViewModelState.Create, "Реестр");
        }

        private void AddSPNReturn_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNReturnView), ViewModelState.Create, "Реестр");
        }

        private void AddAllocationNPF_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var listUC = App.DashboardManager.GetActiveView() as NPFTempAllocationListView;

            if (listUC != null)
            {
                if (listUC.IsRegisterSelected())
                {
                    e.CanExecute = true;
                    return;
                }
            }

            var allocation = App.DashboardManager.GetActiveView() as SPNAllocationView;

            if (allocation != null)
            {
                var model = allocation.DataContext as SPNAllocationViewModel;
                if (model != null && model.ID > 0)
                {
                    e.CanExecute = true;
                }
                return;
            }

            var ret = App.DashboardManager.GetActiveView() as SPNReturnView;
            if (ret != null)
            {
                e.CanExecute = true;
                return;
            }

            e.CanExecute = false;
        }

        private static void AddAllocationNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var listUC = App.DashboardManager.GetActiveView() as NPFTempAllocationListView;

            long? ownerID = null;

            if (listUC != null)
            {
                if (listUC.IsTempAllocationSelected())
                {
                    ownerID = listUC.GetSelectedTempAllocationID();
                }
                else
                {
                    var id = listUC.GetSelectedTempAllocationID();
                    var mr = DataContainerFacade.GetListByProperty<Register>("ReturnID", id).FirstOrDefault();
                    if (mr != null)
                    {
                        ownerID = mr.ID;
                    }
                }
            }

            var allocation = App.DashboardManager.GetActiveView() as SPNAllocationView;

            if (ownerID == null && allocation != null)
            {
                var context = allocation.DataContext as SPNAllocationViewModel;
                if (context != null)
                {
                    ownerID = context.ID;
                }
            }

            if (ownerID != null && ownerID > 0)
            {
                App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), ViewModelState.Create, ownerID.Value, "Добавление НПФ");
                return;
            }

            var ret = App.DashboardManager.GetActiveView() as SPNReturnView;

            if (ret == null) return;
            {
                var context = ret.DataContext as SPNReturnViewModel;
                if (context != null)
                {
                    ownerID = context.ID;
                }

                if (ownerID == null) return;
                if (ownerID > 0)
                {
                    var mr = DataContainerFacade.GetListByProperty<Register>("ReturnID", ownerID.Value).FirstOrDefault();
                    if (mr != null)
                    {
                        App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), ViewModelState.Create, mr.ID, context, "Добавление НПФ");
                    }
                }
                else
                {
                    App.DashboardManager.OpenNewTab(typeof(AllocationNPFView), ViewModelState.Create, 0, "Добавление НПФ");
                }
            }
        }

        private static void CreateZLRedist_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ZLRedistView), ViewModelState.Create, "Создать перераспределение ЗЛ", "Перераспределение ЗЛ");
        }

        private static void ShowSIList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIListView), ViewModelState.Read, "Перечень субъектов");
        }

        private static void ShowSIContactList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIContactListView), ViewModelState.Read, "Перечень контактов");
        }

        private static void ShowSIContractsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIContractsListView), ViewModelState.Read, "Перечень договоров СИ");
        }

        private static void ShowVRContractsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(VRContractsListView), ViewModelState.Read, "Перечень договоров ВР");
        }

        private static void PrintOrder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var oVM = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
                if (oVM != null)
                {
                    if (oVM.ID != 0)
                    {
                        var printOrder = oVM.NoncompetitiveRequestVisible ? new PrintOrder(oVM.ID, oVM.NoncompetitiveRequest) : new PrintOrder(oVM.ID, -1);
                        printOrder.print();
                    }
                    else
                        DXMessageBox.Show("Карточка не сохранена");
                }

                if (App.DashboardManager.GetActiveViewType() == typeof(OrdersListView))
                {
                    var vm = App.DashboardManager.GetActiveViewModel() as OrdersListViewModel;
                    if (vm == null) return;
                    long orID = ((OrdersListView)App.DashboardManager.GetActiveView()).GetSelectedOrderID();
                    if (orID == 0) return;

                    /*RequestPercentDlg dlg = new RequestPercentDlg();
						ThemesTool.SetCurrentTheme(dlg);
						if (((OrdersListView)App.dashboardManager.GetActiveView()).IsSelectedOrderAuction())
						{
							dlg.ShowDialog();
						}*/

                    var printOrder = ((OrdersListView)App.DashboardManager.GetActiveView()).IsSelectedOrderAuction()
                        ? new PrintOrder(orID, ((OrdersListView)App.DashboardManager.GetActiveView()).GetSelectedOrderNonCompetitiveRequest())
                        : new PrintOrder(orID, -1);

                    printOrder.print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void PrintBankPersons_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var dlg = new PrintBankPersonsDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                var vm = new PrintBankPersonsDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);

                if (dlg.ShowDialog() == true)
                {
                    using (Loading.StartNew("Экспорт уполномоченных лиц..."))
                    {
                        new PrintBankPersons(vm.GetExportType(), vm.SelectedStock, vm.IsWithoutCertificates, vm.SelectedBankIndex == 1).Print();
                    }
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                    Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void PrintOrder_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {

            var oVM = App.DashboardManager.GetActiveViewModel() as OrderViewModel;

            if (oVM != null && oVM.ID > 0)
                e.CanExecute = true;
            else
                e.CanExecute = false;

            if (App.DashboardManager.GetActiveViewType() != typeof(OrdersListView)) return;

            var vm = App.DashboardManager.GetActiveViewModel() as OrdersListViewModel;
            if (vm == null) return;

            var view = App.DashboardManager.GetActiveView() as OrdersListView;

            if (view != null)
            {
                long orID = view.GetSelectedOrderID();
                if (orID != 0 && view.IsOrderSelected()) e.CanExecute = true;
            }
        }

        #endregion

        private void OpenEdoXmlBodyExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            long id = (e.Parameter as long?) ?? 0;
            if (id > 0)
            {
                var doc = DataContainerFacade.GetByID<EdoXmlBody, long>(id);
                if (doc != null)
                {
                    string file = string.Empty;
                    do
                    {
                        file = Path.Combine(Path.GetTempPath(), $"{"EDO"}_{id}_{Guid.NewGuid()}.xml");
                    } while (File.Exists(file));
                    if (doc.Body != null && doc.Body.Length > 0)
                        File.WriteAllBytes(file, doc.Body);
                    else
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                        return;
                    }
                    try
                    {
                        Process.Start(file);
                    }
                    catch
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                    }
                }
                else
                    DXMessageBox.Show($"Документ отсутствует в базе. (Id:{e.Parameter})");
            }
            else
            {
                DXMessageBox.Show("Документ отсутствует в базе.");
            }
            e.Handled = true;
        }




        private void OpenDepClaimXmlExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            long id = (e.Parameter as long?) ?? 0;
            if (id > 0)
            {
                var doc = DataContainerFacade.GetByID<RepositoryImpExpFile, long>(id);
                if (doc != null)
                {
                    string file;
                    do
                    {
                        file = Path.Combine(Path.GetTempPath(), $"DepClaimRepository_{id}_{Guid.NewGuid()}.xml");
                    } while (File.Exists(file));

                    if (doc.Repository != null && doc.Repository.Length > 0)
                        File.WriteAllBytes(file, GZipCompressor.Decompress(doc.Repository));
                    else
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                        return;
                    }
                    try
                    {
                        Process.Start(file);
                    }
                    catch
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                    }
                }
                else
                    DXMessageBox.Show($"Документ отсутствует в базе. (Id:{e.Parameter})");
            }
            e.Handled = true;
        }


        private void OpenKIPXmlExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            long id = (e.Parameter as long?) ?? 0;
            if (id > 0)
            {
                var doc = DataContainerFacade.GetByID<RepositoryImpExpFile, long>(id);
                if (doc != null)
                {
                    string file;
                    do
                    {
                        file = Path.Combine(Path.GetTempPath(), $"KIP_{id}_{Guid.NewGuid()}.xml");
                    } while (File.Exists(file));

                    if (doc.Repository != null && doc.Repository.Length > 0)
                        File.WriteAllBytes(file, GZipCompressor.Decompress(doc.Repository));
                    else
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                        return;
                    }
                    try
                    {
                        Process.Start(file);
                    }
                    catch
                    {
                        DXMessageBox.Show("Неизвестная ошибка при открытии документа!", "Ошибка", MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                    }
                }
                else
                    DXMessageBox.Show($"Документ отсутствует в базе. (Id:{e.Parameter})");
            }
            e.Handled = true;
        }

        private void OpenMessageExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var msg = (e.Parameter as LoginMessageItem);
            if (msg != null)
                new DialogHelper().ShowLoginMessages(true, msg);
            e.Handled = true;
        }


        private void OpenSIRegisterGridExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            long registerID = (e.Parameter as long?) ?? 0;

            var vm = (TransfersSIListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersSIListViewModel));
            if (vm != null)
            {
                App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersSIListViewModel) });
                vm.RaiseSelectGridRow(registerID);
            }
            else
            {
                App.DashboardManager.OpenNewTab(typeof(TransfersSIListView), ViewModelState.Read, registerID, "Перечисления СИ");
            }
        }

        private void OpenVRRegisterGridExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            long registerID = (e.Parameter as long?) ?? 0;
            var vm = (TransfersVRListViewModel)App.DashboardManager.FindViewModel(typeof(TransfersVRListViewModel));
            if (vm != null)
            {
                App.DashboardManager.RefreshListViewModels(new[] { typeof(TransfersVRListViewModel) });
                vm.RaiseSelectGridRow(registerID);
            }
            else
            {
                App.DashboardManager.OpenNewTab(typeof(TransfersVRListView), ViewModelState.Read, registerID, "Перечисления ВР");
            }
        }

        private void OpenNPFRegisterGridExecuted(object sender, ExecutedRoutedEventArgs e)
        {

        }



        private void ReturnDeposit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            new DialogHelper().ReturnDeposit();
        }

        private void ReturnDeposit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }


        private void SuspendActivity_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
            if (leVM != null)
            {
                if (leVM.SuspendActivity())
                {
                    var clVM = App.DashboardManager.FindViewModel<NPFListViewModel>();
                    clVM?.RefreshList.Execute(null);
                }
            }

            if (clV == null) return;
            var leID = (long)clV.npfListGrid.GetCellValue(clV.npfListGrid.View.FocusedRowHandle, "LegalEntity.ID");
            if (leID > 0)
                leVM = new NPFViewModel(ViewModelState.Edit, leID);
            else
                return;
            leVM.PostOpen("Перечень НПФ");
            if (leVM.SuspendActivity())
            {
                leVM.State = ViewModelState.Edit;
                leVM.IsDataChanged = true;
                leVM.SaveCard.Execute(null);
                leVM.RefreshNPFPauseList();
            }
        }

        private void SuspendActivity_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;

            var item = clV?.GetSelectedDB2NPFListItem();
            if (item != null)
            {
                e.CanExecute = StatusIdentifier.IsNPFStatusActivityCarries(item.Status.Name);
                return;
            }

            if (leVM != null)
                e.CanExecute = StatusIdentifier.IsNPFStatusActivityCarries(leVM.NPFStatus) && leVM.IsInDatabase();
        }

        private void AddNPFBranch_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            NPFViewModel leVM;
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            if (clV != null)
            {
                leVM = new NPFViewModel(ViewModelState.Edit, clV.GetSelectedDB2NPFListItem().LegalEntity.ID);
            }
            else
            {
                leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
                if (leVM == null)
                    return;
            }

            leVM.AddBranch.Execute(null);
            leVM.RefreshBranchesList();
        }

        private void AddNPFBranch_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            if (clV != null)
            {
                e.CanExecute = true;
                return;
            }

            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
            if (leVM != null)
                e.CanExecute = leVM.IsInDatabase();
        }

        private void AnnulLicence_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
            leVM?.ExecuteAnnulLicence();
        }

        private void AnnulLicence_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var activePanelType = App.DashboardManager.GetActiveViewType();
            if (activePanelType != null && activePanelType == typeof(NPFView))
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        private void ShowReportInvestRatio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var type = Document.Types.SI;
            switch (e.Parameter as string)
            {
                case "SI": type = Document.Types.SI; break;
                case "VR": type = Document.Types.VR; break;
            }

            var prompt = ViewModelBase.DialogHelper.ShowReportInvestRatioPrompt(type);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportInvestRatio(prompt);
                SaveReport(file);
            }
        }

        private void ShowReportF060_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var type = Document.Types.SI;
            switch (e.Parameter as string)
            {
                case "SI": type = Document.Types.SI; break;
                case "VR": type = Document.Types.VR; break;
            }

            var prompt = ViewModelBase.DialogHelper.ShowReportF060Prompt(type);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportF060(prompt);
                SaveReport(file);
            }
        }

        private void ShowReportF070_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var type = Document.Types.SI;
            switch (e.Parameter as string)
            {
                case "SI": type = Document.Types.SI; break;
                case "VR": type = Document.Types.VR; break;
            }

            var prompt = ViewModelBase.DialogHelper.ShowReportF070Prompt(type);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportF070(prompt);
                SaveReport(file);
            }

        }

        private void ShowReportProfitability_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var type = Document.Types.SI;
            switch (e.Parameter as string)
            {
                case "SI": type = Document.Types.SI; break;
                case "VR": type = Document.Types.VR; break;
            }

            var prompt = ViewModelBase.DialogHelper.ShowReportProfitabilityPrompt(type);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportProfitability(prompt);
                SaveReport(file);
            }

        }

        private void ShowReportOwnedFounds_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var prompt = ViewModelBase.DialogHelper.ShowReportOwnedFoundsPrompt(Document.Types.SI);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportOwnedFounds(prompt);
                SaveReport(file);
            }
        }

        private void ShowReportRSASCA_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var type = Document.Types.SI;
            switch (e.Parameter as string)
            {
                case "SI": type = Document.Types.SI; break;
                case "VR": type = Document.Types.VR; break;
            }

            var prompt = ViewModelBase.DialogHelper.ShowReportRSASCAPrompt(type);
            if (prompt != null)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportRSASCA(prompt);
                    SaveReport(file);
                }
            }
        }

        private void ShowReportErrorRegionSpread_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.ShowErrorReport(ErrorReportType.Region);
        }

        private void ShowReportErrorMonthSpread_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.ShowErrorReport(ErrorReportType.Month);
        }

        private void ShowReportErrorNpfSpread_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.ShowErrorReport(ErrorReportType.Npf);
        }

        private void ShowReportErrorTypes_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ViewModelBase.DialogHelper.ShowErrorReport(ErrorReportType.Tipology);
        }

        private void ShowRejAppList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RejAppListView), ViewModelState.Create, -1, true, "Отказные заявления");
        }

        private void ImportRejApp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new ImportRejAppDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ImportRejAppDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
        }

        private void ShowReportDeposits_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var prompt = ViewModelBase.DialogHelper.ShowReportDepositsPrompt();
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportDeposits(prompt, ReportFile.Types.Excel);
                SaveReport(file);
            }
        }

        private void ShowUKAccountsReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ukVm = new SelectUKForReportViewModel();
            if (DialogHelper.ShowControlValidateAsDialogBox(ukVm, new SelectUKForReportView(), "Выбор УК для отчета",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportUKAccounts(ukVm.UKList.Where(u => u.IsSelected).Select(u => u.Contragent.ID).ToList());
                    SaveReport(file);
                }
            }
        }

        private void ShowUKListReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var propVm = new SelectDateAndBoolFlagViewModel(true);
            if (DialogHelper.ShowControlValidateAsDialogBox(propVm, new SelectDateAndBoolFlagDlg(),
                    "Выбор параметров отчета",
                    okButtonText: "Сформировать отчет") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Генерация отчёта"))
                {
                    var file = WCFClient.Client.CreateReportUKList(propVm.DateValue.Value, propVm.FlagValue);
                    SaveReport(file);
                }
            }
        }


        private static void ShowSelectCommonPPNPF_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var uc = App.DashboardManager.GetActiveView();
            if (uc == null)
                return;
            var type = uc.GetType();

            if (type != typeof(RegistersListView)) return;
            var view = (uc as RegistersListView);
            if (view.IsFinRegisterSelected() && view.SelectedItem != null)
            {
                e.CanExecute = RegisterIdentifier.IsFromNPF(view.SelectedItem.RegisterKind)
                               && RegisterIdentifier.FinregisterStatuses.IsIssued(view.SelectedItem.Status)
                               && view.SelectedItem.Count != view.SelectedItem.DraftSum;
            }
        }

        private void ShowSelectCommonPPNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var uc = App.DashboardManager.GetActiveView();

            if (uc == null)
                return;

            long? finregisterID;
            //По условиям ShowSelectCommonPPNPF_CanExecute команда активна только для возвратов НПФ
            const AsgFinTr.Directions direction = AsgFinTr.Directions.ToPFR;

            //Выставляем связь
            if (uc.GetType() == typeof(RegistersListView))
                finregisterID = ((RegistersListView)uc).SelectedItem.ID;
            else
                return;
            ViewModelBase.DialogHelper.SelectUnlinkedCommonPP(finregisterID, AsgFinTr.Sections.NPF, direction);
        }

        private void AddNPFErrorCode_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFCodeView), ViewModelState.Create, -1, true, "Добавить код ошибки", "Код ошибки");
        }

        private void AddNPFMatchCode_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFCodeView), ViewModelState.Create, -1, false, "Добавить код стыковки с договором", "Код стыковки с договором");
        }

        private void ShowNPFErrorCodeList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFCodeListView), ViewModelState.Create, -1, true, "Коды ошибок");
        }

        private void ShowNPFMatchCodeList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFCodeListView), ViewModelState.Create, -1, false, "Коды стыковки с договором");
        }

        private void KBKList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }


        private void ShowCreatePaymentDetail_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PaymentDetailView), ViewModelState.Create, "Добавить назначение платежа", "Назначение платежа");
        }

        private void ShowPaymentDetailList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PaymentDetailListView), ViewModelState.Read, "Назначения платежей");
        }


        private void ShowReportBalanceUK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var prompt = ViewModelBase.DialogHelper.ShowReportBalanceUKPrompt();
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportBalanceUK(prompt, ReportFile.Types.Excel);
                SaveReport(file);
            }
        }


        private void ShowReportIPUKTotal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var prompt = ViewModelBase.DialogHelper.ShowReportIPUKPrompt(true);
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportIPUKTotal(prompt);
                SaveReport(file);
            }
        }

        private void ShowReportIPUK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var prompt = ViewModelBase.DialogHelper.ShowReportIPUKPrompt();
            if (prompt == null) return;
            using (Loading.StartNew("Генерация отчёта"))
            {
                var file = WCFClient.Client.CreateReportIPUK(prompt, ReportFile.Types.Excel);
                SaveReport(file);
            }
        }

        private static void SaveXML(XMLFileResult file, string folder = null, bool openExplorer = true)
        {
            if (!file.IsSuccesss)
                Loading.CloseWindow();
            else
            {
                var name = DepClaimHelper.SaveXML(file, folder);
                if (!openExplorer) return;
                var argument = @"/select, " + name;
                Process.Start("explorer.exe", argument);
            }
        }

        private void SaveReport(ReportFile file)
        {
            if (file == null) return;
            if (file.HasError)
            {
                Loading.CloseWindow();
                ViewModelBase.DialogHelper.ShowError(file.Error);
            }
            else
            {
                var fname = Path.GetFileNameWithoutExtension(file.FileName);
                var ext = Path.GetExtension(file.FileName);
                var name = Path.Combine(Path.GetTempPath(), file.FileName);

                var index = 1;
                while (File.Exists(name))
                {
                    index++;
                    name = Path.Combine(Path.GetTempPath(), $"{fname} ({index}){ext}");
                }
                File.WriteAllBytes(name, file.FileBody);
                Process.Start(name);
            }
        }

        #region DKList

        private void ShowDKYearList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DKYearListView), ViewModelState.Read, "Документы казначейства (Годовые)");
        }

        private void ShowDKMonthList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DKMonthListView), ViewModelState.Read, "Документы казначейства (Месячные)");
        }

        #endregion

        #region GenerateDeposit

        private void GenerateDeposit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void GenerateDeposit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            new DialogHelper().GenerateDeposit();

        }
        #endregion

        #region ExportContractsRegister

        private bool ExportContractsRegister_CanExecute(object sender)
        {
            var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;

            var statuses = new DepClaimSelectParams.Statuses?[]
            {
                DepClaimSelectParams.Statuses.OfferCreated,
                DepClaimSelectParams.Statuses.OfferSigned,
                DepClaimSelectParams.Statuses.OfferAcceptedOrNo,
                DepClaimSelectParams.Statuses.DepositSettled
            };
            return view?.AuctionID != null && statuses.Contains(view.AuctionStatus);
        }

        private void ExportContractsRegister_Executed(object sender, ICommand command)
        {
            if (!command.CanExecute(null)) return;
            var view = App.DashboardManager.GetActiveViewModel() as IDepClaimOfferProvider;
            var dlgVM = new ExportDepClaimOfferAcceptedViewModel(view.AuctionID.Value);
            var dlg = DialogHelper.PrepareDialog(new ExportDepClaimOfferAcceptedDlg(), dlgVM);
            if (!dlgVM.ValidateCanExport())
                return;
            dlg.ShowDialog();
        }

        #endregion ExportContractsRegister

        #region Regions

        private void RegionReportListInsuredPerson_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListInsuredPerson_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportInsuredPersonListView), ViewModelState.Read, "Отчет по заявлениям");
        }

        private void RegionReportListNpfNotice_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListNpfNotice_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportNpfNoticeListView), ViewModelState.Read, "Отчет о поступивших в ПФР уведомлениях НПФ");
        }

        private void RegionReportListAssigneePayment_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListAssigneePayment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportAssigneePaymentListView), ViewModelState.Read, "Отчет о средствах пенсионных накоплений, подлежащих выплате");
        }

        private void RegionReportListCashExpenditures_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListCashExpenditures_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportCashExpendituresListView), ViewModelState.Read, "Отчет о кассовых расходах");
        }

        private void RegionReportListApplications741_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListApplications741_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportApplication741ListView), ViewModelState.Read, "Отчет о поступивших в территориальные органы ПФР заявлениях");
        }

        private void RegionReportListDeliveryCost_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportListDeliveryCost_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PfrBranchReportDeliveryCostListView), ViewModelState.Read, "Отчет о расходах по доставке ЕВ");
        }

        private void RegionReportImportExcel_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportImportExcel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new RegionReportImportDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                dlg.DataContext = new RegionReportImportDlgViewModel();
            }
            dlg.ShowDialog();
        }

        private void RegionReportImportXml_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportImportXml_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new RegionReportImportXmlDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                dlg.DataContext = new RegionReportImportXmlDlgViewModel();
            }
            dlg.ShowDialog();
        }

        private void RegionReportExport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RegionReportExport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new RegionReportExportDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                dlg.DataContext = new RegionReportExportDlgViewModel();
            }
            dlg.ShowDialog();
        }
        #endregion

        #region ShowNpfCorrespondenceList
        private void ShowNpfCorrespondenceList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowNpfCorrespondenceList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceNpfListView, CorrespondenceNpfListViewModel>("Корреспонденция ОКИП", ViewModelState.Read);
        }

        #endregion

        #region ShowNpfControlList
        private void ShowNpfControlList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowNpfControlList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceNpfControlListView, CorrespondenceNpfControlListViewModel>("Контроль корреспонденции ОКИП", ViewModelState.Read);
        }

        #endregion

        #region ShowNpfArchiveList
        private void ShowNpfArchiveList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowNpfArchiveList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceNpfArchiveListView, CorrespondenceNpfArchiveListViewModel>("Архив", ViewModelState.Read);
        }

        #endregion

        #region CreateNpfDocument
        private void CreateNpfDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateNpfDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<DocumentNpfView, DocumentNpfViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Create);
        }

        #endregion

        #region ShowDelayedPaymentClaimList
        private void ShowDelayedPaymentClaimList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowDelayedPaymentClaimList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<DelayedPaymentClaimListView, DelayedPaymentClaimListViewModel>("Задержанные выплаты", ViewModelState.Read);
        }

        #endregion

        private void ShowPensionNotificationList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowPensionNotificationList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PensionNotificationListView), ViewModelState.Read, "Уведомления о назначении НЧТП");
        }

        #region CreateDelayedPaymentClaim
        private void CreateDelayedPaymentClaim_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDelayedPaymentClaim_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<DelayedPaymentClaimView, DelayedPaymentClaimViewModel>("Задержанная выплата", ViewModelState.Create);
        }

        #endregion

        private void CreatePensionNotification_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreatePensionNotification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PensionNotificationView), ViewModelState.Create, 0, "Уведомление о назначении НЧТП", "Уведомление о назначении НЧТП");
        }

        private void CreatePerson_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var relatedModel = App.DashboardManager.GetActiveViewModel();
            long divisionId = -1;
            if (relatedModel == null || relatedModel.State == ViewModelState.Create) return;
            var model = relatedModel as DivisionViewModel;
            if (model != null)
                divisionId = model.Division.ID;
            else if (relatedModel is DivisionListViewModel)
                divisionId = ((DivisionListViewModel)relatedModel).SelectedDivision.ID;

            if (divisionId > 0)
                App.DashboardManager.OpenNewTab(typeof(PersonView), ViewModelState.Create, divisionId, "Создать нового сотрудника", "Новый сотрудник");
        }

        private void CreatePerson_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DivisionViewModel;
            e.CanExecute = (rlVM != null && rlVM.CanExecuteAddContact);
            if (e.CanExecute) return;

            var listViewModel = App.DashboardManager.FindViewModel<DivisionListViewModel>();
            if (listViewModel != null)
                e.CanExecute = listViewModel.SelectedDivision != null;
            else
                e.CanExecute = false;
        }

        private void CreateDivision_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DivisionView), ViewModelState.Create, 0, "Создать новое подразделение", "Новое подразделение");
        }

        private void CreateDivision_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowDivisionList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DivisionListView), ViewModelState.Read, "Справочник подразделений");
        }

        private void ShowDivisionList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowDepClaimSelectParamsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepClaimSelectParamsListView), ViewModelState.Read, "Уведомления о параметрах отбора заявок");
        }

        private void ShowDepClaimSelectParamsList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AddDepClaimSelectParams_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DepClaimSelectParamsView), ViewModelState.Create, "Создать уведомление о параметрах отбора заявок", "Уведомление о параметрах отбора заявок");
        }

        private void AddDepClaimSelectParams_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowRatingAgenciesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatingAgenciesListView), ViewModelState.Read, "Рейтинговые агентства");
        }

        private void ShowRatingAgenciesList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AddRatingAgency_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatingAgencyView), ViewModelState.Create, "Создать рейтинговое агентство", "Рейтинговое агентство");
        }

        private void AddRatingAgency_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var model = App.DashboardManager.FindViewModel(typeof(RatingAgencyViewModel));
            e.CanExecute = model == null;
        }

        private void ShowInvDecl(InvDeclDlgViewModel viewModel)
        {
            var dialog = new InvDeclDlg { Owner = this };

            if (dialog.ShowDialog() != true) return;
            viewModel.PostOpen(dialog.Title);
            using (Loading.StartNew())
            {
                viewModel.Print(dialog.ShowOldNames, dialog.ShowEndedAgreements);
            }
        }

        private void ShowGarantFond()
        {
            var dialog = new GarantFondDlg { Owner = this };
            var model = new GarantFondDialogViewModel();
            dialog.DataContext = model;

            if (dialog.ShowDialog() != true) return;
            model.PostOpen(dialog.Title);
            using (Loading.StartNew())
            {
                model.Print();
            }
        }

        private void ShowSIInvDecl_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowInvDecl(new SIInvDeclDlgViewModel());
        }

        private void ShowGarantFond_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowGarantFond();
        }

        private void PrintSIYearPayment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Получаем наличие планов на год по каждому типу операции (пока их 2)
            List<Element> operationTypes;
            bool hasYears;
            using (Loading.StartNew())
            {
                operationTypes = WCFClient.Client.GetElementByType(Element.Types.RegisterOperationType);
                hasYears = operationTypes.Select(operationType => WCFClient.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.SI, operationType.ID))
                    .Any(years => years.Any());
            }

            if (hasYears)
            {
                var dlg = new YearPlanPaymentDlg { Owner = this, Title = "Письмо о выплатах на год" };
                using (Loading.StartNew())
                {
                    var m = new SIYearPlanPaymentDlgViewModel();
                    dlg.DataContext = m;
                    m.PostOpen(dlg.Title);
                }

                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew())
                {
                    var dialogViewModel = dlg.DataContext as YearPlanPaymentDlgViewModel;
                    dialogViewModel?.Print(dlg.SelectedFolder);
                }
            }
            else
                new DialogHelper().ShowError("\"Планы\" не созданы!");
        }

        private void PrintYearPayment_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void MFLetterSellPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExecuteMFDialog(MFLettersViewModel.TYPE_MF22);
        }

        private void MFLetterPreSumm_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExecuteMFDialog(MFLettersViewModel.TYPE_MF21);
        }

        private void MFLetterFactYield_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExecuteMFDialog(MFLettersViewModel.TYPE_MF23);
        }

        private void MFLetterPortfolioStructure_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExecuteMFDialog(MFLettersViewModel.TYPE_MF20);
        }

        private void MFLetterDepositSumm_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExecuteMFDialog(MFLettersViewModel.TYPE_MF19);
        }

        private void ExecuteMFDialog(int type)
        {
            var dlg = new MFLettersDlg(type);
            using (Loading.StartNew())
            {
                dlg.DataContext = new MFLettersViewModel(type);
            }

            if (dlg.ShowDialog() != true) return;
            string res;
            using (Loading.StartNew())
            {
                res = ((MFLettersViewModel) dlg.DataContext).Print();
            }

            if (!string.IsNullOrEmpty(res))
                DXMessageBox.Show($"Файл шаблона '{res}' не найден!", "Ошибка!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ShowPortfolioStatusList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PortfolioStatusListViewModel),
                                            ViewModelState.Read, "Состояние портфелей");
        }

        private void ShowRatingsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatingsListView), ViewModelState.Read, "Рейтинги");
        }

        private void AddRating_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RatingView), ViewModelState.Create, "Создать рейтинг", "Рейтинг");
        }
        private void AddRating_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var model = App.DashboardManager.FindViewModel(typeof(RatingViewModel));
            e.CanExecute = model == null;
        }

        private void ImportKO761_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var m = App.DashboardManager.GetActiveViewModel();
            var dlg = new BankConclusionImportDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ImportBankConclusionDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen("Импорт спискa кредитных организаций");
            }
            dlg.ShowDialog();
        }

        private void ImportOwnCapital_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OwnCapitalImportDlg dlg;
            using (Loading.StartNew())
            {
                dlg = new OwnCapitalImportDlg {Owner = this};
                ThemesTool.SetCurrentTheme(dlg);
                var vm = new ImportOwnCapitalDlgViewModel((new AppUserSettingsProvider()).GetAppSettingsPath());
                dlg.DataContext = vm;
                if (!vm.ValidateCanImport())
                    return;
                vm.PostOpen("Импорт собственных средств кредитных организаций");
            }
            dlg.ShowDialog();
        }

        private void ShowAgencyRatingsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AgencyRatingsListView), ViewModelState.Read, "Рейтинги агентств");
        }

        private void AddAgencyRating_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AgencyRatingView), ViewModelState.Create, "Добавить рейтинг агентству", "Рейтинг");
        }

        private void ShowJournal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(UserStatisticChoose), ViewModelState.Read, "Действия пользователей");
        }

        private void StopCO_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long id;
            var vm = App.DashboardManager.GetActiveViewModel() as SIContractViewModel;
            if (vm == null)
            {
                var view = App.DashboardManager.GetActiveView() as IContractsListView;
                if (view == null)
                    return;
                id = view.GridControl.GetSelectedContractID();
            }
            else
                id = vm.ID;

            var dlg = new StopSIDlg(StopSIDialogModel.StopType.StopСontract);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = this;
            using (Loading.StartNew())
            {
                var viewModel = new StopSIDialogModel(id, StopSIDialogModel.StopType.StopСontract);
                dlg.DataContext = viewModel;
            }

            if (dlg.ShowDialog() != true) return;
            vm?.ExecuteStopCO();
            if(vm == null)
                SIContractViewModel.ExecuteResumeCOExternal(id);
            RefreshViewModelList<SIContractsListViewModel>();
            RefreshViewModelList<VRContractsListViewModel>();
            RefreshViewModelList<SIListViewModel>();
        }

        private void StopCO_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var form = App.DashboardManager.GetActiveViewModel() as SIContractViewModel;
            if ((form != null) && form.ID != 0 &&
                (form.DissolutionDate == null || form.DissolutionDate == DateTime.MinValue))
            {
                e.CanExecute = true;
                return;
            }
            var view = App.DashboardManager.GetActiveView() as IContractsListView;
            if (view != null)
                e.CanExecute = view.GridControl.CanStopCO();
        }

        private void ResumeCO_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var form = App.DashboardManager.GetActiveViewModel() as SIContractViewModel;
            var view = App.DashboardManager.GetActiveView() as IContractsListView;
            if (form != null)
            {
                if (
                    DXMessageBox.Show($"Заключить договор c {form.FormalizedName} ?", "Внимание!",
                                      MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

                var co = DataContainerFacade.GetByID<Contract, long>(form.ID);
                co.DissolutionDate = null;
                co.DissolutionBase = null;
                DataContainerFacade.Save<Contract, long>(co);
                form.ExecuteResumeCO();
                RefreshViewModelList<SIContractsListViewModel>();
                RefreshViewModelList<VRContractsListViewModel>();
                RefreshViewModelList<SIListViewModel>();
                return;
            }

            if (view == null) return;
            if (
                DXMessageBox.Show($"Заключить договор c {view.GridControl.GetSelectedContractName()} ?",
                    "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                MessageBoxResult.No) return;

            var id = view.GridControl.GetSelectedContractID();
            if (id <= 0) return;
            var co2 = DataContainerFacade.GetByID<Contract, long>(id);
            co2.DissolutionDate = null;
            co2.DissolutionBase = null;
            DataContainerFacade.Save<Contract, long>(co2);
            ((ContractsListViewModel)view.GridControl.DataContext).RefreshList.Execute(null);
            //var lvm = (SIContractViewModel)App.DashboardManager.FindViewModel(typeof(SIContractViewModel));
            //lvm?.ExecuteResumeCO();
            SIContractViewModel.ExecuteResumeCOExternal(id);
            RefreshViewModelList<SIContractsListViewModel>();
            RefreshViewModelList<VRContractsListViewModel>();
            RefreshViewModelList<SIListViewModel>();
        }

        private void ShowKBKList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(KBKListView), ViewModelState.Read, "Справочник КБК");
        }

        private void CreateKBK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(KBKView), ViewModelState.Create, "Добавить наименование доходов");
        }

        private void RefreshViewModelList<T>() where T : ViewModelList
        {
            var viewModel = (ViewModelList)App.DashboardManager.FindViewModel(typeof(T));
            viewModel?.RefreshList.Execute(null);
        }

        private void ResumeCO_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var form = App.DashboardManager.GetActiveViewModel() as SIContractViewModel;
            if ((form != null) && form.ID != 0 &&
                (form.DissolutionDate != null && form.DissolutionDate != DateTime.MinValue))
            {
                e.CanExecute = true;
                return;
            }

            var view = App.DashboardManager.GetActiveView() as IContractsListView;
            if (view != null)
                e.CanExecute = view.GridControl.CanRestoreCO();
        }

        private void StopSI_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long id;
            var vm = App.DashboardManager.GetActiveViewModel() as SIViewModel;
            if (vm == null)
            {
                var view = App.DashboardManager.GetActiveView() as SIListView;
                if (view == null) return;
                id = view.GetSelectedLegalEntityID();
            }
            else id = vm.ID;

            var dlg = new StopSIDlg(StopSIDialogModel.StopType.StopActivities);
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = this;

            using (Loading.StartNew())
            {
                var viewModel = new StopSIDialogModel(id, StopSIDialogModel.StopType.StopActivities);
                dlg.DataContext = viewModel;
            }

            if (dlg.ShowDialog() != true) return;
            (dlg.DataContext as StopSIDialogModel)?.ExecuteSaveCommand();
            var lvm = (SIListViewModel)App.DashboardManager.FindViewModel(typeof(SIListViewModel));
            lvm?.RefreshList.Execute(null);
            vm?.ExecuteStopLicense();
        }

        private void StopSI_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var form = App.DashboardManager.GetActiveViewModel() as SIViewModel;
            if ((form != null) && (form.ID != 0) && (form.CloseDate == null || form.CloseDate == DateTime.MinValue))
            {
                e.CanExecute = true;
                return;
            }
            var view = App.DashboardManager.GetActiveView() as SIListView;
            if (view != null)
                e.CanExecute = view.CanStopSI();
        }

        private void ResumeSI_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var form = App.DashboardManager.GetActiveViewModel() as SIViewModel;
            var view = App.DashboardManager.GetActiveView() as SIListView;
            if (form != null)
            {
                if (
                    DXMessageBox.Show($"Возобновить деятельность {form.ShortName} ?", "Внимание!",
                                      MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

                var _le = DataContainerFacade.GetByID<LegalEntity, long>(form.ID);
                _le.CloseDate = null;
                _le.PDReason = null;
                DataContainerFacade.Save<LegalEntity, long>(_le);
                var ca = DataContainerFacade.GetByID<Contragent, long>(_le.ContragentID);
                ca.StatusID = StatusIdentifier.Identifier.Active.ToLong();
                DataContainerFacade.Save<Contragent, long>(ca);

                var lvm = (SIListViewModel)App.DashboardManager.FindViewModel(typeof(SIListViewModel));
                lvm?.RefreshList.Execute(null);
                form.ExecuteResumeLicense();
                return;
            }

            if (view == null) return;
            {
                if (
                    DXMessageBox.Show(
                        $"Возобновить деятельность {view.GetSelectedLegalEntityName()} ?", "Внимание!",
                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No) return;

                var id = view.GetSelectedLegalEntityID();
                if (id <= 0) return;
                var le = DataContainerFacade.GetByID<LegalEntity, long>(id);
                le.CloseDate = null;
                le.PDReason = null;
                DataContainerFacade.Save<LegalEntity, long>(le);
                var ca = DataContainerFacade.GetByID<Contragent, long>(le.ContragentID);
                ca.StatusID = StatusIdentifier.Identifier.Active.ToLong();
                DataContainerFacade.Save<Contragent, long>(ca);
                ((SIListViewModel)view.DataContext).RefreshList.Execute(null);
                var lvm = (SIViewModel)App.DashboardManager.FindViewModel(typeof(SIViewModel));
                lvm?.ExecuteResumeLicense();
            }
        }

        private void ResumeSI_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var form = App.DashboardManager.GetActiveViewModel() as SIViewModel;
            if ((form != null) && (form.ID != 0) && (form.CloseDate != null && form.CloseDate != DateTime.MinValue))
            {
                e.CanExecute = true;
                return;
            }

            var view = App.DashboardManager.GetActiveView() as SIListView;
            if (view != null)
                e.CanExecute = view.CanRestoreSI();
        }

        private void ReportImportSI_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new ReportImportSIDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                var vm = new ReportImportSIDlgViewModel();
                dlg.DataContext = vm;
                vm.PostOpen(dlg.Title);
            }

            dlg.ShowDialog();
        }

        #region Executed / CanExecute functions

        private void CanLettersSIIsTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view == null) return;
            if (view.IsReqTransferSelected())
                e.CanExecute = true;
        }

        private static void LetterReqTransferSPNMSK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");
                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;

                using (Loading.StartNew("Создание требования о перечислении СПН (МСК) ..."))
                {
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Требование о перечислении СПН (МСК).doc",
                        "Требование о перечислении СПН (МСК)");
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void LetterReqTransferSPNAccum_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");
                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
                var register = transfer.GetTransferRegister();

                var dlg = new SelectSILettersYearMonthDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                dlg.DataContext = new SILettersSelectYearMonthDlgViewModel(
                    register.RegisterDate?.Year ?? DateTime.Now.Year,
                    register.RegisterDate?.Month ?? DateTime.Now.Month);

                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew("Создание требования о перечислении СПН (Накопительная) ..."))
                {
                    var year = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedYear ?? 0;
                    var month = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedMonth ?? 0;
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Требование о перечислении СПН (Накопительная).doc",
                        "Требование о перечислении СПН (Накопительная)",
                        year,
                        month);
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void LetterReqTransferSPNUrgent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");

                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
                var register = transfer.GetTransferRegister();


                var dlg = new SelectSILettersYearMonthDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                dlg.DataContext = new SILettersSelectYearMonthDlgViewModel(
                    register.RegisterDate?.Year ?? DateTime.Now.Year,
                    register.RegisterDate?.Month ?? DateTime.Now.Month);

                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew("Создание требования о перечислении СПН (Срочная) ..."))
                {
                    var year = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedYear ?? 0;
                    var month = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedMonth ?? 0;
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Требование о перечислении СПН (Срочная).doc",
                        "Требование о перечислении СПН (Срочная)",
                        year,
                        month);
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void LetterReqTransferSPNFlow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");

                using (Loading.StartNew("Создание требования о перечислении СПН (Переток) ..."))
                {
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Требование о перечислении СПН (Переток).doc",
                        "Требование о перечислении СПН (Переток)");
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void LetterTransferSPNAccumZero_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");
                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
                var register = transfer.GetTransferRegister();

                var dlg = new SelectSILettersYearMonthDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                dlg.DataContext = new SILettersSelectYearMonthDlgViewModel(
                    register.RegisterDate?.Year ?? DateTime.Now.Year,
                    register.RegisterDate?.Month ?? DateTime.Now.Month);

                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew("Создание письма о передаче СПН (нуль Накопительная) ..."))
                {
                    var year = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedYear ?? 0;
                    var month = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedMonth ?? 0;
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Письмо о передаче СПН (нуль Накопительная).doc",
                        "Письмо о передаче СПН (нуль Накопительная)",
                        year,
                        month);
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void LetterTransferSPNAccumUrgent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                var reqTransId = view?.GetSelectedReqTransferID() ?? 0;
                if (reqTransId == 0)
                    throw new Exception("Не найден идентификатор перечисления!");
                var req = DataContainerFacade.GetByID<ReqTransfer, long>(reqTransId);
                var transfer = req != null ? DataContainerFacade.GetByID<SITransfer, long>(req.TransferListID) : null;
                var register = transfer.GetTransferRegister();

                var dlg = new SelectSILettersYearMonthDlg { Owner = this };
                ThemesTool.SetCurrentTheme(dlg);
                dlg.DataContext = new SILettersSelectYearMonthDlgViewModel(
                    register.RegisterDate?.Year ?? DateTime.Now.Year,
                    register.RegisterDate?.Month ?? DateTime.Now.Month);

                if (dlg.ShowDialog() != true) return;
                using (Loading.StartNew("Создание письма о передаче СПН (нуль Срочная) ..."))
                {
                    var year = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedYear ?? 0;
                    var month = ((SILettersSelectYearMonthDlgViewModel) dlg.DataContext)?.SelectedMonth ?? 0;
                    var printLetter = new PrintSILetters(
                        reqTransId,
                        "Письмо о передаче СПН (нуль Срочная).doc",
                        "Письмо о передаче СПН (нуль Срочная)",
                        year,
                        month);
                    printLetter.Print();
                }
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                Properties.Resources.ApplicationName, MessageBoxButton.OK, MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void CanExecuteIsTrue(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateBankAgent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BankAgentView), ViewModelState.Create, "Создать банк-агент", "Банк-агент");
        }

        private void ShowBankAgentsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BankAgentsList), ViewModelState.Read, "Перечень банков-агентов");
        }

        private void OnAbout(object sender, ExecutedRoutedEventArgs e)
        {
            new About().ShowDialog();
        }

        private void rc_EULA_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Process.Start(About.GetPathToEULA());
            }
            catch
            {
                DXMessageBox.Show(Properties.Resources.EULAOpenErrorString,
                                  Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
        }

        private void rc_Manual_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Process.Start(About.GetPathToManual());
            }
            catch
            {
                DXMessageBox.Show(Properties.Resources.EULAOpenErrorString,
                                  Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
        }

        private void rc_Support_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string text = $"mailto:sed@it.ru?Subject=Запрос в техническую поддержку ПТК ДОКИП <{About.GetVersionNumber()}><{WCFClient.ClientCredentials.Windows.ClientCredential.UserName}>";
            try
            {
                Process.Start(text);
            }
            catch
            {
                DXMessageBox.Show(
                    "Неизвестная ошибка при попытке отправки письма, проверьте настройки почты по умолчанию!", "Ошибка",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void RecalcUKPortfolios_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RecalcUKSIPortfoliosView),
                                            ViewModelState.Read, "Пересчет портфелей УК СИ");
        }

        private void RecalcNKD_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RecalcBuyOrSaleReportsNKDView),
                                            ViewModelState.Read, "Пересчет НКД по сделкам ЦБ");
        }

        private void ShowTransferWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferSIWizardView),
                                            ViewModelState.Read, 0, "Мастер перечислений СИ");
        }

        private void ShowSIYearPlanWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferSI8YearWizardView),
                                            ViewModelState.Read, 1, "Мастер годового плана СИ");
        }

        private void ShowSIPlanCorrectionWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIPlanCorrectionWizardView),
                                            ViewModelState.Read, 8, "Мастер корректировки плана СИ");
        }

        private void ShowSIMonthTransferCreationWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIMonthTransferCreationWizardView),
                                            ViewModelState.Read, 8, "Мастер создания перечислений на месяц СИ");
        }

        private void ShowRopsAsvTransferCreationWizard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferSIRopsAsvWizardView),
                                           ViewModelState.Read, 8, "Мастер перечислений СИ в РОПС, АСВ");
        }
        private void SendEmailsToUK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new SendEmailsToUKDlg();
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = this;

            using (Loading.StartNew())
            {
                dlg.DataContext = new SendEmailsToUKDialogModel();
            }

            dlg.ShowDialog();
        }

        private void LoadDBF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                using (var loader = new DBFLoader(Settings.Default.DBFPath))
                {
                    string message;
                    long importedTotal = 0;
                    long importedBDate = 0;
                    long importedTDate = 0;
                    long importedINDate = 0;
                    const int partSize = 500;
                    using (Loading.StartNew("Импорт из DBF файлов..."))
                    {

                        var loadedItems = loader.LoadDBF();
                        if (loadedItems == null)
                        {
                            Loading.CloseWindow();
                            message = "Произошла неизвестная ошибка при импорте из DBF файлов.";
                            DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                MessageBoxImage.Error);
                            return;
                        }

                        if (loadedItems.Count == 0)
                        {
                            Loading.CloseWindow();
                            message = $"В папке {Settings.Default.DBFPath} не найдены файлы для импорта либо они пустые.";
                            DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                MessageBoxImage.Error);
                            return;
                        }

                        loadedItems.OfType<BDate>()
                            .Select((x, i) => new {Index = i, Value = x})
                            .GroupBy(x => x.Index / partSize)
                            .Select(x => x.Select(v => v.Value).ToList())
                            .ToList().ForEach(list => { importedBDate += WCFClient.Client.ImportBDate(list); });

                        loadedItems.OfType<TDate>()
                            .Select((x, i) => new {Index = i, Value = x})
                            .GroupBy(x => x.Index / partSize)
                            .Select(x => x.Select(v => v.Value).ToList())
                            .ToList().ForEach(list => { importedTDate += WCFClient.Client.ImportTDate(list); });

                        loadedItems.OfType<INDate>()
                            .Select((x, i) => new {Index = i, Value = x})
                            .GroupBy(x => x.Index / partSize)
                            .Select(x => x.Select(v => v.Value).ToList())
                            .ToList().ForEach(list => { importedINDate += WCFClient.Client.ImportINDate(list); });
                        importedTotal = importedBDate + importedINDate + importedTDate;
                        App.DashboardManager.RefreshListViewModels(new[]
                        {
                            typeof(INDateListViewModel),
                            typeof(BDateListViewModel),
                            typeof(TDateListViewModel)
                        });
                    }

                    message =
                        $"Импорт завершен успешно.\r\nИмпортировано:\r\n\tИтоги торгов за день\t{importedBDate}\r\n\tТорги по времени в течение дня\t{importedTDate}\r\n\tИндекс ММВБ10\t{importedINDate}\r\n\tВсего\t{importedTotal}";
                    DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                      MessageBoxImage.Information);
                }
            }
            catch (DirectoryNotFoundException)
            {
                const string message = "Папка для загрузки DBF не найдена.\n" + "Задайте другую папку и попробуйте снова.";
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
            catch (NullReferenceException)
            {
                const string message = "Список Security пуст. Невозможно выполнить импорт.";
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
            catch (Exception)
            {
                const string message = "Произошла неизвестная ошибка при импорте из DBF файлов.";
                DXMessageBox.Show(message, Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
            }
        }

        private void ShowINDateList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(INDateListView), ViewModelState.Read, "Индекс ММВБ10");
        }

        private void ShowTDateList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TDateListView),
                                            ViewModelState.Read, "Торги по времени в течение дня");
        }

        private void ShowBDateList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BDateListView), ViewModelState.Read, "Итоги торгов за день");
        }

        private void ShowYieldOFZList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(YieldOFZListView), ViewModelState.Read, "Доходность по ОФЗ");
        }

        private void ShowArchiveList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<CorrespondenceSIArchiveListView, CorrespondenceSIArchiveListViewModel>("Архив", ViewModelState.Read);
        }

        private void ShowSIControlList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CorrespondenceControlListView), ViewModelState.Read, "Контроль");
        }

        private void ShowCorrespondenceList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CorrespondenceListView), ViewModelState.Read, "Корреспонденция");
        }

        private void ShowDBFSettings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DBFSettingsView), ViewModelState.Create, "Загрузка DBF", "DBF");
        }
        private void ShowBDateXmlImport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = new XMLImportViewModel(typeof(YieldOFZListViewModel));

            if (DialogHelper.ShowControlValidateAsDialogBox(vm, new XMLImportView(), "Импорт итогов торгов (XML)",
                okButtonText: "Импортировать ") == MessageBoxResult.OK)
            {
                using (Loading.StartNew("Сохранение в базу данных"))
                {
                    vm.SaveCard.Execute(null);
                }
                DXMessageBox.Show($"Импортированно записей: {vm.ImporterDataCount}", "Информация");
                vm.RequestCloseView(true);
            }
        }
        private void CreateSIDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab<DocumentSIView, DocumentSIViewModel>(CorrespondenceListItem.s_Doc, ViewModelState.Create);
        }

        private void CreateLinkedDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as CorrespondenceSIListView;
            if (view != null)
            {
                var row = view.Grid.CurrentItem;
                if (row == null || ((CorrespondenceListItemNew)row).IsAttach)
                    return;
                e.CanExecute = true;
            }
            else
            {
                var model = App.DashboardManager.GetActiveViewModel() as DocumentSIViewModel;
                if (model == null)
                    return;
                e.CanExecute = model.ID > 0;
            }
        }

        private void CreateLinkedDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as CorrespondenceSIListView;
            if (view != null)
            {
                var row = view.Grid.CurrentItem as CorrespondenceListItemNew;
                if (row == null || row.IsAttach)
                    return;
                App.DashboardManager.OpenNewTab(typeof(LinkedDocumentView),
                                                ViewModelState.Create, row.ID, CorrespondenceListItem.s_AttachDoc);
            }
            else
            {
                var model = App.DashboardManager.GetActiveViewModel() as DocumentSIViewModel;
                if (model == null)
                    return;
                App.DashboardManager.OpenNewTab(typeof(LinkedDocumentView),
                                                ViewModelState.Create, model.ID, CorrespondenceListItem.s_AttachDoc);
            }
        }

        private void AddMarketPrice_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketPriceView), ViewModelState.Create, "Создать рыночную цену", "Рыночная цена");
        }

        private void ShowMarketPricesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketPricesListView), ViewModelState.Read, "Рыночные цены");
        }
        private void ShowBoardList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (BoardListViewModel)App.DashboardManager.FindViewModel(typeof(BoardListViewModel));
            if (lvm != null)
                return;
            var vm = new BoardListViewModel();
            var view = new BoardListEditView(vm);
            App.DashboardManager.OpenNewTab("Режимы торгов", view, ViewModelState.Read, vm);
        }

        private void ShowSIAssignPaymentsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIAssignPaymentsListView),
                                            ViewModelState.Read, "Отзыв средств");
        }

        private void ShowSIAssignPaymentsArchiveList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIAssignPaymentsArchiveListView),
                                            ViewModelState.Read, "Архив отзыва средств");
        }

        private void LoadODKReports_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var result = WCFClient.Client.LoadODKReports(Settings.Default.MQManagerName,
                                                          Settings.Default.MQQueueName,
                                                          Settings.Default.MQXMLPath);
            DXMessageBox.Show(result != -1 ? $"Передано {result} файлов в очередь" : "Ошибка в настройках. Обратитесь к администратору.",
                Properties.Resources.ApplicationName);
        }

        private void LoadEDOReports_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            const string path = "C:\\EDO";
            var result = WCFClient.Client.LoadEDOReports(path);
            if (result >= 0)
                DXMessageBox.Show($"Загружено {result} файлов из папки {path}",
                                  Properties.Resources.ApplicationName);
            else
                DXMessageBox.Show(
                    result == -2 ? $"Папка {path} не найдена! Убедитесь что папка присутствует и повторите попытку!" : "Ошибка в настройках. Обратитесь к администратору.",
                    Properties.Resources.ApplicationName);
        }

        private void Line030Calculation_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new Line030CalculationView();
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = this;

            using (Loading.StartNew())
            {
                dlg.DataContext = new Line030CalculationViewModel();
            }

            dlg.ShowDialog();
        }


        private void ShowMQSettings_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(WBISettingsView), ViewModelState.Edit, "Интеграция с ОДК");
        }

        private void CreateOVSIReqTransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as TransfersListView;
            if (view != null)
                App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Create,
                                                view.GetSelectedTransferID(), "Создать перечисление", "Перечисление");
            else
            {
                var view2 = App.DashboardManager.GetActiveView() as TransfersArchListView;
                if (view2 != null)
                    App.DashboardManager.OpenNewTab(typeof(ReqTransferView), ViewModelState.Create,
                                                    view2.GetSelectedTransferID() ?? 0L, "Создать перечисление", "Перечисление");
                else
                {
                    var vm = App.DashboardManager.GetActiveViewModel() as TransferViewModel;
                    if (vm.ID > 0)
                        App.DashboardManager.OpenNewTab(typeof(ReqTransferView),
                                                        ViewModelState.Create, vm.ID, "Создать перечисление", "Перечисление");
                    else //если Список перечислений еще не сохранен
                    {
                        if (vm.SelectedContract != null && vm.SelectedUK != null)
                        {
                            var param = new object[] { vm.TransferList, null, vm };
                            App.DashboardManager.OpenNewTab(typeof(ReqTransferView),
                                ViewModelState.Create, param, "Создать перечисление", "Перечисление");
                        }
                        else
                        {
                            new DialogHelper().ShowAlert("Для добавления перечисления необходимо выбрать УК и номер договора!");
                        }
                    }
                }
            }
        }

        private void AddMarketPrice_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as SecuritiesListView;
            if (view != null)
            {
                e.CanExecute = (view.GetSelectedSecurityID() > 0);
            }
            else
            {
                var form = App.DashboardManager.GetActiveViewModel() as SecurityViewModel;
                e.CanExecute = form != null && form.ID > 0;
            }
        }

        private void CreatePrintReq_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                e.CanExecute = TransferDirectionIdentifier.IsFromPFRToUK(view.GetSelectedDirection())
                    && ((view.IsRegisterSelected() && view.SelectedRegisterHasInitialSateTransfers())
                               || (view.IsReqTransferSelected() && TransferStatusIdentifier.IsStatusInitialState(view.GetSelectedStatus())));
                return;
            }

            var regVM = App.DashboardManager.GetActiveViewModel() as SIVRRegisterSIViewModel;
            if (regVM != null)
            {
                e.CanExecute = regVM.SelectedDirection.isFromUKToPFR != null &&
                               !regVM.SelectedDirection.isFromUKToPFR.Value &&
                               regVM.RegisterHasInitialSateTransfers;
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                e.CanExecute = trVM.Direction.isFromUKToPFR != null &&
                    !trVM.Direction.isFromUKToPFR.Value &&
                    trVM.IsTransferSaved &&
                    trVM.DocumentType == Document.Types.SI &&
                    TransferStatusIdentifier.IsStatusInitialState(trVM.TransferStatus);
            }
        }

        private void CreatePrintReq_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SIRegister register = null;
            ReqTransfer rTransfer = null;

            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                    register = DataContainerFacade.GetByID<SIRegister, long>(view.GetSelectedRegisterID());
                else
                    rTransfer = DataContainerFacade.GetByID<ReqTransfer>(view.GetSelectedReqTransferID());
            }

            //SIVRRegisterSIViewModel regVM = null;
            if (register == null && rTransfer == null)
            {
                var regVM = App.DashboardManager.GetActiveViewModel() as SIVRRegisterSIViewModel;
                if (regVM != null)
                    register = regVM.TransferRegister;
            }

            if (register == null && rTransfer == null)
            {
                var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
                if (trVM != null)
                {
                    rTransfer = trVM.Transfer;
                }
            }

            if (register == null && rTransfer == null)
                return;

            var vmi = new ReqTransferClaimInputDlgViewModel(false, register == null ? rTransfer.GetTransferList().GetTransferRegister().RegisterNumber : register.RegisterNumber);
            var dlg = new ReqTransferClaimInputDlg { DataContext = vmi, Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            if (dlg.ShowDialog() != true) return;

            var printReqTransfers = register == null ? new PrintReqTransfers(rTransfer, false, vmi.TrancheNumber, vmi.TrancheDate) : new PrintReqTransfers(register, false, vmi.TrancheNumber, vmi.TrancheDate);
            if (!printReqTransfers.Print()) return;
            App.DashboardManager.RefreshListViewModels(new[]
            {
                typeof (TransfersSIListViewModel),
                typeof (SIAssignPaymentsListViewModel),
                typeof (SIAssignPaymentsArchiveListViewModel)
            });

            var reqvm = App.DashboardManager.FindViewModel<ReqTransferViewModel>();
            if (reqvm != null && reqvm.ID > 0)
            {
                var alreadyChanged = reqvm.IsDataChanged;
                reqvm.TransferDate = DateTime.Today;
                reqvm.LoadCardData(reqvm.ID);
                reqvm.IsDataChanged = alreadyChanged;
            }

            var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
            vm?.RefreshTransfers();
            var tvm = App.DashboardManager.FindViewModel<UKTransfersListViewModel>();
            tvm?.RefreshList.Execute(null);
            string msg;
            if (register != null)
            {
                msg = $"Заявка на перечисления реестра № {register.ID}, {register.RegisterKind} сформирована";
                JournalLogger.LogChangeStateEvent(typeof(ReqTransfer), msg, null, $"Для всех перечислений реестра {register.ID}\n");
            }
            else
            {
                msg = $"Заявка на перечисление № {rTransfer.ID}, {rTransfer.GetTransferList().GetTransferRegister().RegisterKind} сформирована";
                JournalLogger.LogChangeStateEvent(rTransfer.GetType(), msg, rTransfer.ID);
            }
        }

        private void ToUKEnterTransferActData_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromPFRToUK(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusSPNTransfered(view.GetSelectedStatus());
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromPFRToUK)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.SI &&
                        TransferStatusIdentifier.IsStatusSPNTransfered(trVM.Transfer.TransferStatus) &&
                                   trVM.IsTransferFromPFRToUK;
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
            if (trListVM?.SelectedTransferRequest != null)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusSPNTransfered(trListVM.SelectedTransferRequest.TransferStatus) &&
                    !trListVM.IsTransferFromUKToPFR;
        }

        private void CreateSIRequest_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    //Одино перечисление
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusInitialState(view.GetSelectedStatus());
                    return;
                }
                if (view.IsRegisterSelected())
                {
                    //Массовая выгрузка
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.SelectedRegisterHasInitialSateTransfers();
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    //Одно перечисление
                    e.CanExecute = trVM.DocumentType == Document.Types.SI &&
                        TransferStatusIdentifier.IsStatusInitialState(trVM.Transfer.TransferStatus) &&
                                   trVM.Transfer.Sum.HasValue && !trVM.IsDataChanged;
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
            if (trListVM == null) return;
            if (trListVM.IsTransferFromUKToPFR)
            {
                //Одно перечисление
                e.CanExecute = trListVM.SelectedTransferRequest != null &&
                               trListVM.SelectedTransferRequest.ID > 0 &&
                               TransferStatusIdentifier.IsStatusInitialState(
                                   trListVM.SelectedTransferRequest.TransferStatus);
            }
        }

        private void PrintOneRequest(ReqTransferViewModel trVM, bool isVr)
        {
            var opId = trVM?.Operation.ID;
            var showDates = !isVr && trVM != null && (opId == (int)SIRegister.Operations.WithdrawNCTP ||
                                      opId == (int)SIRegister.Operations.WithdrawEV || opId == (int)SIRegister.Operations.WithdrawSPV ||
                                      opId == (int)SIRegister.Operations.DeadZL);
            var siRequestDlg = new SIRequestDlg(showDates)
            {
                Owner = this,
                Title =
                    $"Требование для {trVM.LegalEntity.FormalizedName}, договор {trVM.Contract.ContractNumber}"
            };
            ThemesTool.SetCurrentTheme(siRequestDlg);

            siRequestDlg.ShowDialog();
            if (siRequestDlg.DialogResult != true) return;
            using (Loading.StartNew())
            {
                bool result;
                var sirtIsDataChangeBeforeStatusChange = false;
                var sirtvm = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
                if (sirtvm != null)
                    sirtIsDataChangeBeforeStatusChange = sirtvm.IsDataChanged;

                try
                {
                    var printRequest = new PrintRequest(trVM.Operation, siRequestDlg.SelectedType, isVr, siRequestDlg.Year, siRequestDlg.Month);
                    result = printRequest.Print(trVM.Transfer, trVM.Contract);
                }
                catch (Exception ex)
                {
                    throw new MSOfficeException(ex.Message);
                }

                if (result)
                {
                    trVM.TransferStatus = TransferStatusIdentifier.sDemandGenerated;
                    trVM.RequestCreationDate = DateTime.Today;
                    var ed = trVM.Transfer.ExtensionData;
                    trVM.Transfer.ExtensionData = null;
                    DataContainerFacade.Save<ReqTransfer, long>(trVM.Transfer);
                    trVM.Transfer.ExtensionData = ed;

                    var msg = $"Требование сформировано для {trVM.LegalEntity.FormalizedName}, договор {trVM.Contract.ContractNumber}";
                    JournalLogger.LogChangeStateEvent(trVM, msg);

                    App.DashboardManager.RefreshListViewModels(new[]
                    {
                        typeof(TransfersSIListViewModel),
                        typeof(TransfersVRListViewModel),
                        typeof(SIAssignPaymentsListViewModel),
                        typeof(VRAssignPaymentsListViewModel),
                        typeof(SIAssignPaymentsArchiveListViewModel),
                        typeof(VRAssignPaymentsArchiveListViewModel)
                    });

                    var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
                    vm?.RefreshTransfers();

                    var vmVR = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                    vmVR?.RefreshTransfers();

                    var regVM = App.DashboardManager.FindViewModel<SIVRRegisterSIViewModel>();
                    regVM?.RefreshTransferLists();

                    var regVmvr = App.DashboardManager.FindViewModel<SIVRRegisterVRViewModel>();
                    regVmvr?.RefreshTransferLists();

                    if (sirtvm != null) sirtvm.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
                }
            }
        }

        private void PrintManyRequests(ReqTransferViewModel trVM, long pRegisterID, string pRegisterNumber, long pNumberOfReqTransfers, bool isVr)
        {
            var opId = trVM?.Operation.ID ?? DataContainerFacade.GetByID<SIRegister>(pRegisterID)?.OperationID;

            var showDates = !isVr && (opId == (int)SIRegister.Operations.WithdrawNCTP ||
                          opId == (int)SIRegister.Operations.WithdrawEV || opId == (int)SIRegister.Operations.WithdrawSPV ||
                          opId == (int)SIRegister.Operations.DeadZL);

            var siRequestDlg = new SIRequestMassDlg(showDates)
            {
                Owner = this,
                Title =
                    $"Выгрузка требований для реестра {pRegisterNumber}, {pNumberOfReqTransfers} перечислений(е)"
            };
            ThemesTool.SetCurrentTheme(siRequestDlg);

            siRequestDlg.ShowDialog();
            if (siRequestDlg.DialogResult == true)
            {
                using (Loading.StartNew())
                {
                    bool result;
                    try
                    {
                        var printRequest = new PrintRequest(pRegisterID, siRequestDlg.SelectedType, siRequestDlg.SelectedFolder, isVr,
                            siRequestDlg.Year < 2000 ? (siRequestDlg.Year + 2000) : siRequestDlg.Year, siRequestDlg.Month);
                        result = printRequest.PrintMass();
                    }
                    catch (Exception ex)
                    {
                        throw new MSOfficeException(ex.Message);
                    }

                    if (result)
                    {
                        var msg =
                            $"Требования сформированы для реестра {(string.IsNullOrEmpty(pRegisterNumber) ? pRegisterID.ToString() : pRegisterNumber)}, {pNumberOfReqTransfers} перечислений(е)\n";
                        if (trVM != null)
                            JournalLogger.LogChangeStateEvent(trVM, msg);
                        else
                            JournalLogger.LogChangeStateEvent(typeof(ReqTransfer), "Требования сформированы", null, msg);

                        App.DashboardManager.RefreshListViewModels(new[]
                        {
                            typeof(TransfersSIListViewModel),
                            typeof(TransfersVRListViewModel),
                            typeof(SIAssignPaymentsListViewModel),
                            typeof(VRAssignPaymentsListViewModel),
                            typeof(SIAssignPaymentsArchiveListViewModel),
                            typeof(VRAssignPaymentsArchiveListViewModel)
                        });

                        var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
                        vm?.RefreshTransfers();

                        var vm1 = App.DashboardManager.FindViewModel<TransferVRViewModel>();
                        vm1?.RefreshTransfers();

                        var regVM = App.DashboardManager.FindViewModel<SIVRRegisterSIViewModel>();
                        regVM?.RefreshTransferLists();

                        var regVM1 = App.DashboardManager.FindViewModel<SIVRRegisterVRViewModel>();
                        regVM1?.RefreshTransferLists();

                        Loading.CloseWindow();
                        DXMessageBox.Show("Выгрузка завершена успешно.", Properties.Resources.ApplicationName,
                            MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        Loading.CloseWindow();
                        DXMessageBox.Show("Во время выгрузки произошла ошибка!", Properties.Resources.ApplicationName,
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void CreateSIRequest_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ReqTransferViewModel trVM = null;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                {
                    //Массовая выгрузка
                    var lID = view.GetSelectedRegisterID();
                    if (lID <= 0) return;
                    try
                    {
                        PrintManyRequests(trVM, lID, view.GetSelectedRegisterNumber(),
                            view.GetNumberOfInitialSateTransfersInSelectedRegister(), false);
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                    return;
                }
                if (view.IsReqTransferSelected())
                {
                    //Одиночный реквест
                    var lID = view.GetSelectedReqTransferID();
                    if (lID > 0)
                    {
                        using (Loading.StartNew())
                        {
                            trVM = new ReqTransferViewModel(lID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
            {
                var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
                if (trListVM != null)
                {
                    using (Loading.StartNew())
                    {
                        trVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                    }
                }
            }

            if (trVM == null)
                trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;

            if (trVM == null) return;
            try
            {
                PrintOneRequest(trVM, false);
            }
            catch (Exception ex)
            {
                CheckMSOException(ex);
            }
        }

        private void FromUKEnterTransferActData_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsRegisterSelected())
                {
                    //Массовый ввод даты
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.SelectedRegisterHasReceivedByUKTransfers();
                    return;
                }

                e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                               view.IsReqTransferSelected() &&
                               TransferStatusIdentifier.IsStatusRequestReceivedByUK(view.GetSelectedStatus());
                return;
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.SI &&
                        TransferStatusIdentifier.IsStatusRequestReceivedByUK(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
            if (trListVM?.SelectedTransferRequest == null) return;
            if (trListVM.IsTransferFromUKToPFR)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusRequestReceivedByUK(
                        trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void EnterTransferActData_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sirtIsDataChangeBeforeStatusChange = false;
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                if (view != null)
                {
                    if (view.IsRegisterSelected())
                    {
                        var regN = view.GetSelectedRegisterNumber();
                        var l = view.GetTransfersInSelectedRegister();
                        if (!l.All(x => TransferStatusIdentifier.IsStatusRequestReceivedByUK(x.Status)))
                        {
                            new DialogHelper().ShowAlert("Не все перечисления выбранного реестра еще находятся в статусе \"Требование получено УК\"");
                        }
                        else
                        {
                            ProcessEnterTransferActDataForMultipleRegisters(l.Where(x => TransferStatusIdentifier.IsStatusRequestReceivedByUK(x.Status)).ToList());
                            new DialogHelper().ShowAlert("Даты Актов передачи введены всем перечислениям реестра {0}. Реестр перемещен в архив перечислений.", regN);
                        }
                        return;
                    }

                    if (view.GetSelectedReqTransferID() > 0)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                        }
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
                    if (trListVM?.SelectedTransferRequest != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            var contract = dlgVM?.Transfer.GetTransferList().GetContract();
            var legalEntity = contract.GetLegalEntity();

            var dlg = new EnterTransferActDateDlg();
            dlg.SetTitle(legalEntity.FormalizedName, contract?.ContractNumber);

            if (dlgVM?.Direction.isFromUKToPFR != null && dlgVM.Direction.isFromUKToPFR.Value)
            {
                if (dlgVM.Transfer.SPNDebitDate != null)
                {
                    dlg.SetSPNDebitDate(dlgVM.Transfer.SPNDebitDate.Value);
                }
            }

            dlg.DataContext = dlgVM;
            dlg.Owner = this;
            ThemesTool.SetCurrentTheme(dlg);
            dlgVM.State = ViewModelState.Edit;

            if (dlg.ShowDialog() == true)
            {
                ProcessEnterTransferActDate(dlgVM, dlg.Title);
                JournalLogger.LogChangeStateEvent(dlgVM, "Введена дата акта передачи");
                RefreshOnEnterTransferActDate();
                var contractVM = App.DashboardManager.FindViewModel<SIContractViewModel>();
                if (contractVM != null)
                    contractVM.OperateSum = dlgVM.Contract.OperateSum.Value;
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void RefreshOnEnterTransferActDate()
        {
            //Обновляем списки и карточки
            App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersSIListViewModel),
                                                                   typeof (TransfersSIArchListViewModel),
                                                                   typeof (SIAssignPaymentsListViewModel),
                                                                   typeof (SIAssignPaymentsArchiveListViewModel),
                                                                   typeof (SPNMovementsSIListViewModel),
                                                                   typeof (SPNMovementsVRListViewModel)
                                                               });

            var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
            vm?.RefreshTransfers();
            var regVM = App.DashboardManager.FindViewModel<SIVRRegisterSIViewModel>();
            regVM?.RefreshTransferLists();
        }

        private void ProcessEnterTransferActDataForMultipleRegisters(IEnumerable<TransferListItem> l)
        {
            using (Loading.StartNew())
            {
                foreach (var item in l)
                {
                    var model = new ReqTransferViewModel(item.ReqTransferID, ViewModelState.Edit);
                    if (model.Direction.isFromUKToPFR != null && model.Direction.isFromUKToPFR.Value)
                    {
                        if (model.Transfer.SPNDebitDate != null)
                            model.TransferActDate = model.Transfer.SPNDebitDate.Value;
                    }

                    model.TransferActNumber = EnterTransferActDateDlg.GenerateActNumber(model.TransferActDate, model.TransferRegister.GetOperation().Name);
                    model.TransferStatus = TransferStatusIdentifier.sActSigned;
                    ProcessEnterTransferActDate(model, null);
                    JournalLogger.LogChangeStateEvent(model, "Введена дата акта передачи");
                }
            }

            RefreshOnEnterTransferActDate();
        }

        private void ProcessEnterTransferActDate(ReqTransferViewModel dlgVM, string title)
        {
            //Обновляем статус
            dlgVM.SaveCard.Execute(null);
            //Сначала статус потом название
            dlgVM.PostOpen(title);

            WCFClient.Client.UpdateTransferByActSigned(
                    dlgVM.Contract,
                    dlgVM.Transfer,
                    dlgVM.IsTransferFromPFRToUK,
                    TransferStatusIdentifier.ActionTransfer.EnterTransferActData);
        }

        private void FromUKEnterRequirementData_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected() || view.IsRegisterSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   view.GetSelectedStatuses().Any(TransferStatusIdentifier.IsStatusRequestFormed);
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.SI &&
                        TransferStatusIdentifier.IsStatusRequestFormed(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferViewModel;
            if (trListVM?.SelectedTransferRequest == null) return;
            if (trListVM.IsTransferFromUKToPFR)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusRequestFormed(trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void FromUKEnterRequirementData_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            var listReqTransferID = new List<long>();
            var sirtIsDataChangeBeforeStatusChange = false;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                if (view != null && view.IsRegisterSelected())
                {
                    listReqTransferID.AddRange(view.GetReqTransferIDBySelectedRegisterID(TransferStatusIdentifier.sDemandGenerated));
                    if (listReqTransferID.Any())
                    {
                        var id = listReqTransferID.First();
                        listReqTransferID.Remove(id);
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(id, ViewModelState.Edit);
                        }
                    }
                    else return;
                }
                else if (view != null && view.IsReqTransferSelected() && view.GetSelectedReqTransferID() > 0)
                {
                    using (Loading.StartNew())
                    {
                        dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
                    if (trListVM != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            dlgVM.RequestCreationDate = DateTime.Now;

            var dlg = new FromUKEnterRequirementDataDlg { Owner = this, DataContext = dlgVM };
            var c = dlgVM.Transfer.GetTransferList().GetContract();
            if (listReqTransferID.Any())
            {
                dlg.setTitle($"Реквизиты требований для реестра № {dlgVM.TransferRegister.ID}");
            }
            else
            {
                dlg.setTitle(c.GetLegalEntity().FormalizedName, c.ContractNumber);
            }


            ThemesTool.SetCurrentTheme(dlg);

            if (dlg.ShowDialog() == true)
            {
                var ed = dlgVM.Transfer.ExtensionData;
                dlgVM.Transfer.ExtensionData = null;
                DataContainerFacade.Save(dlgVM.Transfer);
                dlgVM.Transfer.ExtensionData = ed;
                foreach (var id in listReqTransferID)
                {
                    var reqTransfer = DataContainerFacade.GetByID<ReqTransfer, long>(id);
                    reqTransfer.RequestFormingDate = dlgVM.Transfer.RequestFormingDate;
                    reqTransfer.RequestNumber = dlgVM.Transfer.RequestNumber;
                    reqTransfer.TransferStatus = dlgVM.Transfer.TransferStatus;
                    DataContainerFacade.Save(reqTransfer);
                }

                JournalLogger.LogChangeStateEvent(dlgVM, "Введены реквизиты требования");
                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersSIListViewModel),
                                                                   typeof (SIAssignPaymentsListViewModel),
                                                                   typeof (SIAssignPaymentsArchiveListViewModel),
                                                                   typeof (UKTransfersListViewModel)
                                                               });

                var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
                vm?.RefreshTransfers();
                var regVM = App.DashboardManager.FindViewModel<SIVRRegisterSIViewModel>();
                regVM?.RefreshTransferLists();
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void FromUKEnterGetRequirementDate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
            {
                if (view.IsReqTransferSelected())
                {
                    e.CanExecute = TransferDirectionIdentifier.IsFromUKToPFR(view.GetSelectedDirection()) &&
                                   TransferStatusIdentifier.IsStatusSPNTransfered(view.GetSelectedStatus());
                    return;
                }
            }

            var trVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (trVM != null)
            {
                if (trVM.IsTransferFromUKToPFR)
                {
                    e.CanExecute = trVM.DocumentType == Document.Types.SI &&
                        TransferStatusIdentifier.IsStatusSPNTransfered(trVM.Transfer.TransferStatus);
                    return;
                }
            }

            var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
            if (trListVM?.SelectedTransferRequest == null) return;
            if (trListVM.IsTransferFromUKToPFR)
                e.CanExecute =
                    TransferStatusIdentifier.IsStatusSPNTransfered(trListVM.SelectedTransferRequest.TransferStatus);
        }

        private void FromUKEnterGetRequirementDate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var sirtIsDataChangeBeforeStatusChange = false;
            var dlgVM = App.DashboardManager.GetActiveViewModel() as ReqTransferViewModel;
            if (dlgVM == null)
            {
                var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
                if (view != null && view.GetSelectedReqTransferID() > 0)
                {
                    using (Loading.StartNew())
                    {
                        dlgVM = new ReqTransferViewModel(view.GetSelectedReqTransferID(), ViewModelState.Edit);
                    }
                }
                else
                {
                    var trListVM = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
                    if (trListVM?.SelectedTransferRequest != null)
                    {
                        using (Loading.StartNew())
                        {
                            dlgVM = new ReqTransferViewModel(trListVM.SelectedTransferRequest.ID, ViewModelState.Edit);
                        }
                    }
                }
                ;
            }
            else
                sirtIsDataChangeBeforeStatusChange = dlgVM.IsDataChanged;

            var contract = dlgVM?.Transfer.GetTransferList().GetContract();
            var legalEntity = contract.GetLegalEntity();

            var dlg = new FromUKEnterGetRequirementDateDlg();
            dlg.SetTitle(legalEntity.FormalizedName, contract?.ContractNumber);
            dlg.Owner = this;
            dlg.DataContext = dlgVM;
            dlgVM.State = ViewModelState.Edit;
            ThemesTool.SetCurrentTheme(dlg);

            if (dlg.ShowDialog() == true)
            {
                DataContainerFacade.Save(dlgVM.Transfer);
                JournalLogger.LogChangeStateEvent(dlgVM, "Введена дата вручения требования");

                App.DashboardManager.RefreshListViewModels(new[]
                                                               {
                                                                   typeof (TransfersSIListViewModel),
                                                                   typeof (SIAssignPaymentsListViewModel),
                                                                   typeof (SIAssignPaymentsArchiveListViewModel)
                                                               });

                var vm = App.DashboardManager.FindViewModel<TransferSIViewModel>();
                vm?.RefreshTransfers();
                var regVM = App.DashboardManager.FindViewModel<SIVRRegisterSIViewModel>();
                regVM?.RefreshTransferLists();
            }
            dlgVM.IsDataChanged = sirtIsDataChangeBeforeStatusChange;
        }

        private void ShowUKList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(UKTransfersListView), ViewModelState.Read,
                                            false, "Список перечислений УК");
        }

        private void ShowUKListArchive_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(UKTransfersListArchiveView),
                                            ViewModelState.Read, true, "Архив перечислений УК");
        }

        private void CreateOVSIReqTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (view != null)
                e.CanExecute = ((view.IsTransferSelected()) && (view.GetSelectedTransferID() > 0));
            else
            {
                var view2 = App.DashboardManager.GetActiveView() as TransfersSIArchListView;
                if (view2 != null)
                    e.CanExecute = ((view2.IsTransferSelected()) && (view2.GetSelectedTransferID() > 0));
                else
                {
                    var vm = App.DashboardManager.GetActiveViewModel() as TransferSIViewModel;
                    e.CanExecute = vm != null;
                }
            }
        }

        private void PrintAPSIYearPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new PrintAPYearPlanDlg();
            var vm = new SIPrintAPYearPlanDlgViewModel();
            if (!vm.HasYearsListByAllTypes())
                return;

            dlg.DataContext = vm;
            ThemesTool.SetCurrentTheme(dlg);
            vm.PostOpen(dlg.Title);
            if (dlg.ShowDialog() != true) return;
            try
            {
                vm.Print();
            }
            catch (Exception ex)
            {
                CheckMSOException(ex);
            }
        }

        private void PrintAPSIMonthPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new PrintAPMonthPlanDlg();
            var vm = new SIPrintAPMonthPlanDlgViewModel();
            if (!vm.HasYearsListByAllOperations())
                return;

            dlg.DataContext = vm;
            ThemesTool.SetCurrentTheme(dlg);
            vm.PostOpen(dlg.Title);
            if (dlg.ShowDialog() != true) return;
            try
            {
                vm.Print();
            }
            catch (Exception ex)
            {
                CheckMSOException(ex);
            }
        }

        private void CheckMSOException(Exception ex)
        {
            if (!(ex is MSOfficeException)) throw ex;
            Loading.CloseWindow();
            DXMessageBox.Show(Properties.Resources.MSOfficeError,
                Properties.Resources.ApplicationName, MessageBoxButton.OK,
                MessageBoxImage.Error);
            App.log.WriteException(ex);
        }

        private void ShowViolationsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ViolationsSIListView), ViewModelState.Read, "Нарушения УК СИ");
        }

        private void ShowOVSITransferList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransfersSIListView), ViewModelState.Read, "Перечисления СИ");
        }

        private void ShowArchiveOVSITransfersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransfersSIArchListView), ViewModelState.Read, "Архив перечислений СИ");
        }

        private void ShowSPNMovementsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNMovementsSIListView), ViewModelState.Read, "Журнал движения СПН СИ");
        }

        private void ShowSPNMovementsUKList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SPNMovementsUKSIListView),
                                            ViewModelState.Read, "Журнал движения СПН СИ (УК)");
        }

        private void ShowZLTransfersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ZLMovementsSIListView), ViewModelState.Read, "Журнал движения ЗЛ СИ");
        }

        private void ShowSIF402List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIF402ListView), ViewModelState.Read, "Список оплат");
        }

        private void ShowF70List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F70SIListView), ViewModelState.Read, "Отчеты о доходах СИ");
        }

        private void ShowNetWealthsSumList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NetWealthsSumListView),
                                            ViewModelState.Read, new List<string> { "Чистые активы(СЧА) СИ совокупно", "Работа с СИ - Отчеты УК - Чистые активы(СЧА) совокупно" }, "Чистые активы(СЧА) СИ совокупно");
        }

        private void ShowNetWealthsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NetWealthsSIListView), ViewModelState.Read, "Чистые активы(СЧА) СИ");
        }

        private void ShowActivesMarketCost_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketCostSIListView),
                                            ViewModelState.Read, "Рыночная стоимость активов (РСА) СИ");
        }

        private void ShowActivesMarketCostScope_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(MarketCostScopeListView), ViewModelState.Read, "Рыночная стоимость активов СИ (РСА) совокупно");
        }

        private void ShowF40List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F40SIListView), ViewModelState.Read, "Отчет по сделкам СИ");
        }

        private void ShowF50List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F50SIListView), ViewModelState.Read, "Отчеты по ЦБ СИ");
        }

        private void ShowF60List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F60SIListView), ViewModelState.Read, "Отчеты об инвестировании СИ");
        }

        private void ShowF80List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(F80SIListView), ViewModelState.Read, "Отчет о продаже покупке ЦБ СИ");
        }

        private void ShowSIDemandList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIDemandListView), ViewModelState.Read, "Требования на оплату");
        }

        private void ShowSIF402PFRActions_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowF402PFRActions<SIF402ListView>();
        }



        private void ShowF402PFRActions<T>() where T : F402ListView
        {
            var dlg = new F402PFRActions();
            ThemesTool.SetCurrentTheme(dlg);
            dlg.Owner = this;

            var vm = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (vm == null)
            {
                var list = App.DashboardManager.GetActiveView() as T;
                if (list == null || !list.IsF402Selected())
                    return;
                vm = new F401402UKViewModel(list.GetSelectedF402ID(), false);
            }

            dlg.DataContext = vm;
            vm.PostOpen(dlg.Title, true);
            if (dlg.ShowDialog() != true) return;
            DataContainerFacade.Save(vm.Detail);
            JournalLogger.LogModelEvent(vm, JournalEventType.INSERT);
        }

        private bool CanShowF402PFRActions<T>() where T : F402ListView
        {
            var activeListView = App.DashboardManager.GetActiveView() as T;
            var activeViewModel = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            return (activeViewModel != null || (activeListView != null && activeListView.IsF402Selected()));
        }

        private void ShowSIF402PFRActions_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.SI && CanShowF402PFRActions<SIF402ListView>();
            else
                e.CanExecute = CanShowF402PFRActions<SIF402ListView>();
        }

        private void ShowOwnFundsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OwnFundsListView), ViewModelState.Read, "Собственные средства СИ");
        }

        private void AddOwnFunds_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OwnFundsView),
                                            ViewModelState.Create, "Добавить собственные средства", "Собственные средства");
        }

        private void ShowYieldsList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(YieldsSIListView), ViewModelState.Read, "Доходность СИ");
        }

        private void CreateYield_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(YieldSIView), ViewModelState.Create, "Создать доходность УК СИ", "Доходность УК СИ");
        }


        private void UKReportImport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new UKReportImportDlg { Owner = this };
            ThemesTool.SetCurrentTheme(dlg);
            using (Loading.StartNew())
            {
                dlg.DataContext = new UKReportImportDlgViewModel();
            }

            dlg.ShowDialog();
        }

        private void EditRate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (App.DashboardManager.GetActiveViewType() != typeof(RatesListView)) return;
            var vm = App.DashboardManager.GetActiveViewModel() as RatesListViewModel;
            if (vm == null) return;
            var ownerID = ((RatesListView)App.DashboardManager.GetActiveView()).GetSelectedRateID();
            App.DashboardManager.OpenNewTab(typeof(RateView), ViewModelState.Edit,
                ownerID, "Редактировать курс");
        }

        private void EditRate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as RatesListView;
            e.CanExecute = view != null && view.IsRateSelected();
        }

        private void ShowRatesList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowMissedRatesList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreatePenalty_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PenaltyView), ViewModelState.Create, "Создать поступление пени и штрафов", "Поступление пени и штрафов");
        }

        private void CreatePenalty_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void EditSIName_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var siViewModel = App.DashboardManager.GetActiveViewModel() as SIViewModel;
            siViewModel?.Rename();

            var silV = App.DashboardManager.GetActiveView() as SIListView;
            if (silV == null) return;
            var lID = silV.GetSelectedLegalEntityID();
            if (lID <= 0) return;
            siViewModel = new SIViewModel(lID, ViewModelState.Edit);
            siViewModel.Rename();
        }

        private void EditSIName_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (App.DashboardManager.GetActiveViewType() == typeof(SIView))
            {
                e.CanExecute = true;
                return;
            }
            var silV = App.DashboardManager.GetActiveView() as SIListView;
            if (silV?.GetSelectedLegalEntityID() > 0)
                e.CanExecute = true;
        }

        private void FastReportInput_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var view = App.DashboardManager.GetActiveView() as OrdersListView;
            if (view != null && view.IsOrderSelected())
            {
                var s = view.GetSelectedOrderStatus();
                var x = view.GetSelectedItem();
                if (x == null || x.IsReadOnly)
                {
                    e.CanExecute = false;
                    return;
                }
                e.CanExecute = (OrderStatusIdentifier.IsStatusExecuted(s) ||
                               OrderStatusIdentifier.IsStatusPartiallyExecuted(s)) && !x.IsSecurityListEmpty;
            }
            if (e.CanExecute) return;
            {
                var vm = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
                if (vm?.InitialStatus == null) return;
                if (vm.Order.IsReadOnly)
                {
                    e.CanExecute = false;
                    return;
                }
                var s = vm.InitialStatus;
                e.CanExecute = (OrderStatusIdentifier.IsStatusExecuted(s) ||
                                OrderStatusIdentifier.IsStatusPartiallyExecuted(s))
                               && !vm.CbInOrderListEmpty;
            }
        }

        private void FastReportInput_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as OrdersListView;
            if (view != null)
            {
                if (view.IsOrderSelected())
                {
                    var orderID = view.GetSelectedOrderID();
                    var id = view.GetSelectedRelayingOrderID(orderID);
                    if (id.HasValue && id.Value > 0)
                    {
                        MessageBox.Show("Выбранное поручение составлено для «Перекладки ЦБ».\nВоспользуйтесь функционалом «Добавить данные из отчета»");
                        return;
                    }
                }
                App.DashboardManager.OpenNewTab(typeof(FastReportInputView),
                                                ViewModelState.Edit, view.GetSelectedOrderID(), "Быстрый ввод отчетов");
            }

            var oVM = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
            if (oVM != null)
                App.DashboardManager.OpenNewTab(typeof(FastReportInputView),
                                                ViewModelState.Edit, oVM.ID, "Быстрый ввод отчетов");
        }


        private void ShowClientThemes_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ClientThemesView), ViewModelState.Edit, "Цветовая гамма");
        }

        private void ShowClientThemes_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void InvalidateRibbon()
        {
            RibbonControl.Focusable = true;
            RibbonControl.Focus();
            RibbonControl.InvalidateArrange();
        }

        private void CreateBank_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BankView), ViewModelState.Create, "Создать банк", "Банк");
        }

        private void CreateRate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RateView), ViewModelState.Create, "Добавить курс", "Курс");
        }

        private void CreateSecurity_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SecurityView), ViewModelState.Create, 0, "Создать ценную бумагу", "Ценная бумага");
        }

        private void CreateBoard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BoardView), ViewModelState.Create, 0, "Добавить режим торгов", "Режим торгов");
        }
        private void AddDataFromReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var caption = OrderReportsHelper.SELLING_REPORT;
            var viewType = typeof(OrderReportView);
            long orderID = 0;

            var view = App.DashboardManager.GetActiveView() as OrdersListView;
            if (view != null)
            {
                if (view.IsOrderSelected())
                {
                    orderID = view.GetSelectedOrderID();
                    var id = view.GetSelectedRelayingOrderID(orderID);
                    if (id != null)
                    {
                        caption = OrderReportsHelper.RELAYING_REPORT;
                        orderID = id.Value;
                    }
                    else
                        if (OrderTypeIdentifier.IsBuyOrder(view.GetSelectedOrderType()))
                        caption = OrderReportsHelper.BUYING_REPORT;
                }
            }
            else
            {
                var vm = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
                var vmr = App.DashboardManager.GetActiveViewModel() as OrderRelayingViewModel;
                if (vm != null)
                {
                    if (OrderTypeIdentifier.IsBuyOrder(vm.SelectedType))
                        caption = OrderReportsHelper.BUYING_REPORT;
                    orderID = vm.ID;
                }
                else if (vmr != null)
                {
                    caption = OrderReportsHelper.RELAYING_REPORT;
                    orderID = vmr.ID;
                }
            }

            if (orderID > 0)
                App.DashboardManager.OpenNewTab(viewType, ViewModelState.Create, orderID, caption);
        }

        private void AddDataFromReport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as OrdersListView;
            if (view != null && view.IsOrderSelected())
            {
                var s = view.GetSelectedOrderStatus();
                var x = view.GetSelectedItem();
                if (x == null || x.IsReadOnly)
                {
                    e.CanExecute = false;
                    return;
                }
                e.CanExecute = (OrderStatusIdentifier.IsStatusExecuted(s) ||
                               OrderStatusIdentifier.IsStatusPartiallyExecuted(s)) && !x.IsSecurityListEmpty;
            }
            else
            {
                var vm = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
                var vmr = App.DashboardManager.GetActiveViewModel() as OrderRelayingViewModel;
                if (vm != null)
                {
                    if (vm.Order.IsReadOnly)
                    {
                        e.CanExecute = false;
                        return;
                    }
                    var s = vm.InitialStatus;
                    e.CanExecute = (OrderStatusIdentifier.IsStatusExecuted(s) ||
                                    OrderStatusIdentifier.IsStatusPartiallyExecuted(s)) && !vm.CbInOrderListEmpty;
                }
                else if (vmr != null)
                {
                    if (vmr.Order.IsReadOnly)
                    {
                        e.CanExecute = false;
                        return;
                    }
                    string s = vmr.InitialStatus;
                    e.CanExecute = (OrderStatusIdentifier.IsStatusExecuted(s) ||
                                    OrderStatusIdentifier.IsStatusPartiallyExecuted(s)) && !vmr.CbInOrderListEmpty && !vmr.IsDataChanged;
                }
            }
        }

        private void CreateERZLNotify_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var listView = App.DashboardManager.GetActiveView() as ZLRedistListView;
            if (listView != null)
            {
                var vm = new ZLRedistViewModel { State = ViewModelState.Create };
                vm.LoadZLRedist(listView.GetZLRedistID());
                vm.AddNotify.Execute(null);
            }
            else
            {
                var vm = App.DashboardManager.GetActiveViewModel() as ZLRedistViewModel;
                if (vm == null) return;
                vm.State = ViewModelState.Create;
                vm.AddNotify.Execute(null);
            }
        }

        private void CreateERZLNotify_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var listView = App.DashboardManager.GetActiveView() as ZLRedistListView;
            if (listView != null)
                e.CanExecute = listView.GetZLRedistID() > 0;
            else
            {
                var vm = App.DashboardManager.GetActiveViewModel() as ZLRedistViewModel;
                e.CanExecute = vm != null && vm.ID > 0;
            }
        }

        private void LetterToNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new LetterToNPFDlg();
            long lID = 0;
            var npfList = App.DashboardManager.GetActiveView() as NPFListView;
            if (npfList != null)
                lID = npfList.GetSelectedDB2NPFListItem().LegalEntity.ID;
            var dlgVM = new LetterToNPFViewModel(lID);
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);
            dlg.ShowDialog();
        }

        private void LetterToNPFs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LetterToNPFWizardView), ViewModelState.Read,
                                            false, "Отправка писем в НПФ");
        }

        private void LetterToUK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new LetterToUKDlg();
            long lID = 0;
            var siList = App.DashboardManager.GetActiveView() as SIListView;
            if (siList != null)
                lID = siList.GetSelectedLegalEntityID();
            var dlgVM = new LetterToUKViewModel(lID);
            dlg.DataContext = dlgVM;
            dlg.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dlg);
            dlg.ShowDialog();
        }

        private void LetterToUKs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(LetterToNPFWizardView), ViewModelState.Read,
                                            true, "Отправка писем в УК");
        }

        private long? GetSelectedFinregisterId()
        {
            var rAlV = App.DashboardManager.GetActiveView() as RegistersArchiveListView;
            var rlV = App.DashboardManager.GetActiveView() as RegistersListView;
            if (rAlV != null || rlV != null)
            {
                if (rAlV != null)
                {
                    try
                    {
                        return rAlV.GetCurrFinRegID();
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                }
                if (rlV != null)
                {
                    try
                    {
                        return rlV.GetCurrFinRegID();
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                }
            }
            else
            {
                var rv = App.DashboardManager.GetActiveView() as RegisterView;
                if (rv != null)
                {
                    var frNPFLi = rv.grid.GetFocusedRow() as FinregisterWithCorrAndNPFListItem;
                    if (frNPFLi?.Finregister == null) return null;
                    try
                    {
                        return frNPFLi.Finregister.ID;
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                }
                else
                {
                    var frVM = App.DashboardManager.GetActiveViewModel() as FinRegisterViewModel;
                    if (frVM?.FinregisterID == null || !(frVM.FinregisterID > 0)) return null;
                    try
                    {
                        return frVM.FinregisterID.Value;
                    }
                    catch (Exception ex)
                    {
                        CheckMSOException(ex);
                    }
                }
            }
            return null;
        }

        private void CreateLetterToNPF_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var id = GetSelectedFinregisterId();
            if (id.HasValue)
                new PrintLetterToNPF(id.Value);
        }

        private void CreateLetterToNPF_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rAlV = App.DashboardManager.GetActiveView() as RegistersArchiveListView;
            var rlV = App.DashboardManager.GetActiveView() as RegistersListView;

            if (rAlV != null || rlV != null)
            {
                RegistersListItem rLi;
                bool ifrs;
                if (rAlV != null)
                {
                    rLi = rAlV.registersListGrid.GetFocusedRow() as RegistersListItem;
                    ifrs = rAlV.IsFinRegisterSelected();
                }
                else
                {
                    rLi = rlV.registersListGrid.GetFocusedRow() as RegistersListItem;
                    ifrs = rlV.IsFinRegisterSelected();
                }

                if (rLi != null && ifrs &&
                    rLi.Status != null &&
                    (RegisterIdentifier.FinregisterStatuses.IsIssued(rLi.Status.ToLower()) ||
                     RegisterIdentifier.FinregisterStatuses.IsTransferred(rLi.Status.ToLower()) ||
                     RegisterIdentifier.FinregisterStatuses.IsRrefine(rLi.Status.ToLower())))
                {
                    e.CanExecute = true;
                    return;
                }
            }
            else
            {
                var rv = App.DashboardManager.GetActiveView() as RegisterView;
                if (rv != null)
                {
                    var frNPFLi = rv.grid.GetFocusedRow() as FinregisterWithCorrAndNPFListItem;

                    if (frNPFLi != null && frNPFLi.Finregister != null && frNPFLi.Finregister.Status != null &&
                        (RegisterIdentifier.FinregisterStatuses.IsIssued(frNPFLi.Finregister.Status.ToLower()) ||
                         RegisterIdentifier.FinregisterStatuses.IsTransferred(frNPFLi.Finregister.Status.ToLower()) ||
                         RegisterIdentifier.FinregisterStatuses.IsRrefine(frNPFLi.Finregister.Status.ToLower())))
                    {
                        e.CanExecute = true;
                        return;
                    }
                }
                else
                {
                    var frVM = App.DashboardManager.GetActiveViewModel() as FinRegisterViewModel;
                    if (frVM?.FinregisterID > 0)
                    {
                        if (frVM.Status != null &&
                            (RegisterIdentifier.FinregisterStatuses.IsIssued(frVM.Status.ToLower()) ||
                             RegisterIdentifier.FinregisterStatuses.IsTransferred(frVM.Status.ToLower()) ||
                             RegisterIdentifier.FinregisterStatuses.IsRrefine(frVM.Status.ToLower())))
                        {
                            e.CanExecute = true;
                            return;
                        }
                    }
                }
            }
            e.CanExecute = false;
        }

        private void CreatePFRContact_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as PFRBranchViewModel;
            if (rlVM != null)
                App.DashboardManager.OpenNewTab(typeof(PFRContactView), ViewModelState.Create, rlVM.ID, "Создать контакт ПФР", "Контакт ПФР");
        }

        private void CreatePFRContact_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as PFRBranchViewModel;
            e.CanExecute = (rlVM != null && rlVM.CanExecuteAddContact);
        }

        private void CreateEnrollmentPercents_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(EnrollmentPercents),
                                            ViewModelState.Create, AccDocKindIdentifier.EnrollmentPercents);
        }

        private void CreateEnrollmentPercents_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateEnrollmentOther_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(EnrollmentOtherView),
                                            ViewModelState.Create, AccDocKindIdentifier.EnrollmentOther);
        }

        private void CreateEnrollmentOther_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateAccountWithdrawal_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(WithdrawalView),
                                            ViewModelState.Create, AccDocKindIdentifier.Withdrawal);
        }

        private void CreateAccountWithdrawal_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateAccountTransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(TransferToAccount), ViewModelState.Create, AccDocKindIdentifier.Transfer);
        }

        private void CreateAccountTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void BanksVerification_Executed(object sender, ICommand command)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionSourceProvider;
            ShowDialogForBankExport(true, vm?.AuctionID);
        }

        private static void ShowDialogForBankExport(bool isBankVerification, long? auctionId = null)
        {
            DepClaimSelectParams auction = null;
            if (auctionId.HasValue)
                auction = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionId);
            if (auction != null)
            {

            }
            var dlgVM = new BanksVerificationViewModel(isBankVerification, auction);
            dlgVM.PostOpen("Расчет лимитов");
            var dlg = DialogHelper.PrepareDialog(new BanksVerificationDlg(), dlgVM);

            if (dlg.ShowDialog() != true) return;
            if (isBankVerification || auction.StockId != (long)Stock.StockID.SPVB) return;
            var folder = dlgVM.FilePath;
            using (Loading.StartNew("Экспорт XML 'Лимиты' для биржи СПВБ"))
            {
                var xml = DataContainerFacade.ExportBankLimitsInfo(auction.ID);
                SaveXML(xml, folder);
            }
        }

        private bool BanksVerification_CanExecute(object sender)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as IDepClaimAuctionSourceProvider;
            var vmS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
            if (vm == null || vmS == null || vm.AuctionStatus != DepClaimSelectParams.Statuses.New) return false;
            return vmS.IsMoscowStockSelected || vmS.IsSPVBStockSelected;
        }

        private void AddUser_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as ADUsersListViewModel;
            rlVM?.AddUser.Execute(null);
        }

        private void AddUser_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as ADUsersListViewModel;
            e.CanExecute = (rlVM != null);
        }

        private void DeleteUser_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as ADUsersListViewModel;
            if (rlVM != null)
                rlVM.DeleteUser.Execute(null);
            else
                ViewModelBase.DialogHelper.DeleteUser();
        }

        private void DeleteUser_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as ADUsersListViewModel;
            e.CanExecute = (rlVM != null);
        }

        private void ShowRolesList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ADRolesListView), ViewModelState.Read, "Список ролей");
        }

        private void ShowRolesList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ShowUsersList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ADUsersListView),
                                            ViewModelState.Read, "Список пользователей ПТК ДОКИП");
        }

        private void ShowUsersList_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueUndistributed_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Create,
                                            PortfolioIdentifier.PortfolioTypes.NOT_DSV, "Создать поступление страховых взносов, не разнесенных по лицевым счетам", "Поступление страховых взносов, не разнесенных по лицевым счетам");
        }

        private void CreateDueUndistributed_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueUndistributedDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueUndistributedView), ViewModelState.Create,
                                            PortfolioIdentifier.PortfolioTypes.NOT_SPN, "Создать поступление страховых взносов ДСВ, не разнесенных по лицевым счетам", "Поступление страховых взносов ДСВ, не разнесенных по лицевым счетам");
        }

        private void CreateDueUndistributedDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueUndistributedAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity = 0;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueUndistributedViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList == null || vList.SelectedItem == null)
                    return;
                identity = vList.SelectedItem.ID;
                rlVM = new DueUndistributedViewModel(PortfolioIdentifier.PortfolioTypes.NOT_DSV);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueUndistributedAccurateView), ViewModelState.Create, rlVM, DueDocKindIdentifier.DueUndistributedAccurate);
        }

        private void CreateDueUndistributedAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueUndistributedViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueUndistributed)
                    e.CanExecute = true;
            }
        }

        private void CreateDueUndistributedAccurateDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueUndistributedViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList == null || vList.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new DueUndistributedViewModel(PortfolioIdentifier.PortfolioTypes.DSV);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueUndistributedAccurateView), ViewModelState.Create, rlVM, DueDocKindIdentifier.DueUndistributedAccurateDSV);
        }

        private void CreateDueUndistributedAccurateDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueUndistributedViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueUndistributedDSV)
                    e.CanExecute = true;
            }
        }

        private void CreateDueDead_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueDeadView),
                                            ViewModelState.Create, PortfolioIdentifier.PortfolioTypes.NOT_DSV, "Создать поступление страховых взносов умерших ЗЛ", "Поступление страховых взносов умерших ЗЛ");
        }

        private void CreateDueDead_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueDeadDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueDeadView),
                                            ViewModelState.Create, PortfolioIdentifier.PortfolioTypes.NOT_SPN, "Создать поступление страховых взносов умерших ЗЛ ДСВ", "Поступление страховых взносов умерших ЗЛ ДСВ");
        }

        private void CreateDueDeadDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueDeadAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueDeadViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList?.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new DueDeadViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueDeadAccurateView),
                                                ViewModelState.Create, rlVM, DueDocKindIdentifier.DueDeadAccurate);
        }

        private void CreateDueDeadAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueDeadViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueDead)
                    e.CanExecute = true;
            }
        }

        private void CreateDueDeadAccurateDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueDeadViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList?.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new DueDeadViewModel(PortfolioIdentifier.PortfolioTypes.DSV);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueDeadAccurateView),
                                                ViewModelState.Create, rlVM, DueDocKindIdentifier.DueDeadAccurateDSV);
        }

        private void CreateDueDeadAccurateDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueDeadViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueDeadDSV)
                    e.CanExecute = true;
            }
        }

        private void CreateDueExcess_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueExcessView),
                                            ViewModelState.Create, PortfolioIdentifier.PortfolioTypes.NOT_DSV, "Создать поступление излишне учтенных страховых взносов", "Поступление излишне учтенных страховых взносов");
        }

        private void CreateDueExcess_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueExcessDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DueExcessView),
                                            ViewModelState.Create, PortfolioIdentifier.PortfolioTypes.NOT_SPN, "Создать поступление излишне учтенных страховых взносов ДСВ", "Поступление излишне учтенных страховых взносов ДСВ");
        }

        private void CreateDueExcessDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateDueExcessAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueExcessViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList?.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueExcessAccurateView),
                                                ViewModelState.Create, rlVM, DueDocKindIdentifier.DueExcessAccurate);
        }

        private void CreateDueExcessAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueExcessViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueExcess)
                    e.CanExecute = true;
            }
        }

        private void CreateDueExcessAccurateDSV_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueExcessViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList?.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.DSV);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(DueExcessAccurateView), ViewModelState.Create, rlVM, DueDocKindIdentifier.DueExcessAccurateDSV);
        }

        private void CreateDueExcessAccurateDSV_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as DueExcessViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.DueExcessDSV)
                    e.CanExecute = true;
            }
        }

        private void CreatePrepayment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PrepaymentView),
                                            ViewModelState.Create, PortfolioIdentifier.PortfolioTypes.NOT_DSV, "Поступление авансового платежа", "Авансовый платеж");
        }

        private void CreatePrepayment_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreatePrepaymentAccurate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long identity;
            var rlVM = App.DashboardManager.GetActiveViewModel() as PrepaymentViewModel;
            if (rlVM != null)
                identity = rlVM.ID;
            else
            {
                var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;
                if (vList?.SelectedItem == null)
                    return;

                identity = vList.SelectedItem.ID;
                rlVM = new PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
                rlVM.Load(identity);
            }
            if (identity > 0)
                App.DashboardManager.OpenNewTab(typeof(PrepaymentAccurate),
                                                ViewModelState.Create, rlVM, DueDocKindIdentifier.PrepaymentAccurate);
        }

        private void CreatePrepaymentAccurate_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var rlVM = App.DashboardManager.GetActiveViewModel() as PrepaymentViewModel;
            var vList = App.DashboardManager.GetActiveView() as IListView<DueListItemClient>;

            if ((rlVM != null)
                && (rlVM.ID > 0))
                e.CanExecute = true;
            else if (vList?.SelectedItem != null)
            {
                var item = vList.SelectedItem;
                if (string.IsNullOrEmpty(item.Precision)
                    && item.Operation == DueDocKindIdentifier.Prepayment)
                    e.CanExecute = true;
            }
        }

        private void CreateTreasuryDoc_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long id = 0;
            Type itemType = null;

            var vDueList = App.DashboardManager.GetActiveView() as DueListView;
            if (vDueList != null)
            {
                var due = vDueList.SelectedItem;
                if (due != null)
                {
                    id = due.ID;
                    itemType = due.SourceObject.GetType();
                }
            }

            var vDSVList = App.DashboardManager.GetActiveView() as DsvListView;
            if (vDSVList != null)
            {
                var due = vDSVList.SelectedItem;
                if (due != null)
                {
                    id = due.ID;
                    itemType = due.SourceObject.GetType();
                }
            }

            var vDkm = App.DashboardManager.GetActiveView() as DKListView;
            if (vDkm != null)
            {
                var due = vDkm.SelectedItem;
                if (due != null)
                {
                    id = due.ID;
                    itemType = typeof(KDoc);
                }
            }

            App.DashboardManager.OpenNewTab(typeof(Treasurity4View), ViewModelState.Create,
                                            id, itemType, "Создать документ казначейства", "Документ казначейства");
        }

        private void CreateTreasuryDoc_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var aV = App.DashboardManager.GetActiveView();
            e.CanExecute = aV is DueListView || aV is DKMonthListView || aV is DKYearListView || aV is DsvListView;
        }

        private void CreateOVSITransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lv = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (lv != null)
            {
                if ((lv.IsRegisterSelected()) && (lv.GetSelectedRegisterID() > 0))
                    App.DashboardManager.OpenNewTab(typeof(TransferSIView),
                                                    ViewModelState.Create, lv.GetSelectedRegisterID(), "Создать список перечислений СИ", "Список перечислений СИ");
                return;
            }

            var lv2 = App.DashboardManager.GetActiveView() as TransfersSIArchListView;
            if (lv2 != null)
            {
                if ((lv2.IsRegisterSelected()) && (lv2.GetSelectedRegisterID() > 0))
                    App.DashboardManager.OpenNewTab(typeof(TransferSIView),
                                                    ViewModelState.Create, (long)(lv2.GetSelectedRegisterID() ?? 0), "Создать список перечислений СИ", "Список перечислений СИ");
                return;
            }

            var vm = App.DashboardManager.GetActiveViewModel() as SIVRRegisterSIViewModel;
            if (vm != null)
                App.DashboardManager.OpenNewTab(typeof(TransferSIView), ViewModelState.Create,
                                                vm.ID, "Создать список перечислений СИ", "Список перечислений СИ");
        }

        private void CreateOVSITransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var lv = App.DashboardManager.GetActiveView() as TransfersSIListView;
            if (lv != null)
            {
                e.CanExecute = ((lv.IsRegisterSelected()) && (lv.GetSelectedRegisterID() > 0));
                return;
            }

            var lv2 = App.DashboardManager.GetActiveView() as TransfersSIArchListView;
            if (lv2 != null)
            {
                e.CanExecute = ((lv2.IsRegisterSelected()) && (lv2.GetSelectedRegisterID() > 0));
                return;
            }

            var vm = App.DashboardManager.GetActiveViewModel() as SIVRRegisterSIViewModel;
            e.CanExecute = vm != null && vm.ID > 0;
        }

        private void CreateUKPaymentPlan<T>(int contractType) where T : AssignPaymentsListView
        {
            var lv = App.DashboardManager.GetActiveView() as T;
            if (lv != null)
            {
                if (lv.IsRegisterSelected())
                {
                    var id = lv.GetSelectedRegID();
                    if (id > 0)
                    {
                        if (!CheckUkPaymentPlanAvail(id, contractType))
                        {
                            ViewModelBase.DialogHelper.ShowError("У выбранного плана нет доступных УК для создания плана выплат!");
                            return;
                        }
                        App.DashboardManager.OpenNewTab(typeof(UKPaymentPlanView),
                            ViewModelState.Create, id, "Создать план выплат по УК", "План выплат по УК");
                    }
                }
            }

            var vm = App.DashboardManager.GetActiveViewModel() as YearPlanViewModel;
            if (vm == null) return;
            var id2 = vm.ID;
            if (id2 <= 0) return;
            if (!CheckUkPaymentPlanAvail(id2, contractType))
            {
                ViewModelBase.DialogHelper.ShowError("У выбранного плана нет доступных УК для создания плана выплат!");
                return;
            }
            App.DashboardManager.OpenNewTab(typeof(UKPaymentPlanView),
                ViewModelState.Create, id2, "Создать план выплат по УК", "План выплат по УК");
        }

        private bool CheckUkPaymentPlanAvail(long id, int type)
        {
            return WCFClient.Client.GetAvailableUkListForYearPlan(id, type, true).Any();
        }

        private void CreateSIUKPaymentPlan_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CreateUKPaymentPlan<SIAssignPaymentsListView>((int)Document.Types.SI);
        }

        private bool CanCreateUKPaymentPlan<T>() where T : AssignPaymentsListView
        {
            var lv = App.DashboardManager.GetActiveView() as T;
            if (lv != null)
            {
                return ((lv.IsRegisterSelected()) && (lv.GetSelectedRegID() > 0));
            }
            var vm = App.DashboardManager.GetActiveViewModel() as YearPlanViewModel;
            return (vm != null) && (vm.ID > 0);
        }

        private void CreateSIUKPaymentPlan_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = CanCreateUKPaymentPlan<SIAssignPaymentsListView>();
        }

        private void CreateViolationCategory_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(ViolationCategoryView), ViewModelState.Create, "Создать категорию нарушения", "Категория нарушения");
        }

        private void CreateViolationCategory_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateCosts_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(CostsView), ViewModelState.Create, "Добавить расходы", "Расходы");
        }

        private void CreateCosts_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CreateBudget_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(BudgetView), ViewModelState.Create, "Добавить бюджет", "Бюджет");
        }

        private void CreateBudget_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
            if (sv == null)
                e.CanExecute = true;
            else
                e.CanExecute = sv.IsBudgetSelected() || sv.IsPortfolioSelected();
        }

        private void CreateBudgetCorrection_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var bvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (bvm != null)
            {
                if (bvm.ID > 0)
                    App.DashboardManager.OpenNewTab(typeof(BudgetCorrectionView),
                                                    ViewModelState.Create, bvm.ID, "Добавить уточнение бюджета", "Уточнение бюджета");
            }
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv == null) return;
                if (!sv.IsBudgetSelected()) return;
                var lID = sv.GetSelectedBudgetID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(BudgetCorrectionView),
                        ViewModelState.Create, lID, "Добавить уточнение бюджета", "Уточнение бюджета");
            }
        }

        private void CreateBudgetCorrection_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var bvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (bvm != null)
                e.CanExecute = !bvm.BudgetIsNotSaved;
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv != null)
                    e.CanExecute = sv.IsBudgetSelected();
            }
        }

        private void CreateDirection_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var bvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (bvm != null)
                e.CanExecute = !bvm.BudgetIsNotSaved;
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv != null)
                    e.CanExecute = sv.IsBudgetSelected();
            }
        }

        private void CreateDirection_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var bvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (bvm != null)
            {
                if (bvm.ID > 0)
                    App.DashboardManager.OpenNewTab(typeof(DirectionView), ViewModelState.Create,
                                                    bvm.ID, "Создать распоряжение", "Распоряжение");
            }
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv == null) return;
                if (!sv.IsBudgetSelected()) return;
                var lID = sv.GetSelectedBudgetID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(DirectionView),
                        ViewModelState.Create, lID, "Создать распоряжение", "Распоряжение");
            }
        }

        private void CreateTransfer_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dvm = App.DashboardManager.GetActiveViewModel() as DirectionViewModel;
            if (dvm != null)
            {
                if (dvm.ID > 0)
                    App.DashboardManager.OpenNewTab(typeof(TransferSchilsView), ViewModelState.Create,
                                                    dvm.ID, "Создать перечисление", "Перечисление");
            }
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv == null) return;
                if (!sv.IsDirectionSelected()) return;
                var lID = sv.GetSelectedDirectionID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(TransferSchilsView),
                        ViewModelState.Create, lID, "Создать перечисление", "Перечисление");
            }
        }

        private void CreateTransfer_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var dvm = App.DashboardManager.GetActiveViewModel() as DirectionViewModel;
            if (dvm != null)
                e.CanExecute = dvm.ID > 0;
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv != null)
                    e.CanExecute = sv.IsDirectionSelected();
            }
        }

        private void CreateReturn_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (dvm != null)
            {
                if (dvm.ID > 0)
                    App.DashboardManager.OpenNewTab(typeof(ReturnView), ViewModelState.Create, dvm.ID, "Добавить возврат", "Возврат");
            }
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv == null) return;
                if (!sv.IsBudgetSelected()) return;
                var lID = sv.GetSelectedBudgetID();
                if (lID > 0)
                    App.DashboardManager.OpenNewTab(typeof(ReturnView), ViewModelState.Create, lID, "Добавить возврат", "Возврат");
            }
        }

        private void CreateReturn_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            var dvm = App.DashboardManager.GetActiveViewModel() as BudgetViewModel;
            if (dvm != null)
            {
                e.CanExecute = dvm.ID > 0;
            }
            else
            {
                var sv = App.DashboardManager.GetActiveView() as SchilsCostsListView;
                if (sv != null)
                {
                    e.CanExecute = sv.IsBudgetSelected();
                }
            }
        }

        private void CreateLegalEntity_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(NPFView), ViewModelState.Create, "Создать НПФ", "НПФ");
        }

        private void CreatePFRAccount_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PFRAccountView), ViewModelState.Create, "Создать счет ПФР", "Счет ПФР");
        }

        private void CreateBankAccount_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            if (clV?.GetSelectedDB2NPFListItem() != null)
            {
                e.CanExecute = true;
                return;
            }

            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
            if (leVM != null)
            {
                e.CanExecute = leVM.IsInDatabase();
                return;
            }

            e.CanExecute = false;
        }

        private void CreateBankAccount_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            var leVM = App.DashboardManager.GetActiveViewModel() as NPFViewModel;

            if (clV?.GetSelectedDB2NPFListItem() != null)
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Create,
                                                clV.GetSelectedDB2NPFListItem().LegalEntity.ID, "Создать расчетный счет", "Расчетный счет");
            if (leVM != null)
                App.DashboardManager.OpenNewTab(typeof(BankAccountView), ViewModelState.Create,
                                                leVM.ID, "Создать расчетный счет", "Расчетный счет");
        }

        private void CreatePortfolio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PortfolioView), ViewModelState.Create, "Создать портфель ПФР", "Портфель ПФР");
        }

        private void CreateOrder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(OrderView), ViewModelState.Create, "Создать поручение", "Поручение");
        }

        private void CreateOrderRelaying_Executed(object sender, ExecutedRoutedEventArgs args)
        {
            App.DashboardManager.OpenNewTab(typeof(OrderRelayingView), ViewModelState.Create, "Переложить ценные бумаги", "Переложить ценные бумаги");
        }

        private F402Status? GetActiveF402Status<T>() where T : F402ListView
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null && !f402Form.IsDemand)
                return f402Form.OriginalStatus;
            //return (F402Status?)f402form.Detail.StatusID;

            var f402List = App.DashboardManager.GetActiveView() as T;
            if (f402List != null && f402List.IsF402Selected())
                return f402List.GetSelectedF402Status();

            return null;
        }

        private bool CanCreateOrderToPay<T>() where T : F402ListView
        {
            return GetActiveF402Status<T>() == F402Status.Started;
        }

        private bool CanOrderBeSent<T>() where T : F402ListView
        {
            return GetActiveF402Status<T>() == F402Status.Built;
        }

        private bool CanOrderBeDelivered<T>() where T : F402ListView
        {
            return GetActiveF402Status<T>() == F402Status.Sent;
        }

        private bool CanOrderBePaid<T>() where T : F402ListView
        {
            return GetActiveF402Status<T>() == F402Status.Delivered;
        }

        private void CreateSIOrderToPay_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.SI && CanCreateOrderToPay<SIF402ListView>();
            else
                e.CanExecute = CanCreateOrderToPay<SIF402ListView>();
        }

        private void SIOrderSent_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.SI && CanOrderBeSent<SIF402ListView>();
            else
                e.CanExecute = CanOrderBeSent<SIF402ListView>();
        }

        private void SIOrderDelivered_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.SI && CanOrderBeDelivered<SIF402ListView>();
            else
                e.CanExecute = CanOrderBeDelivered<SIF402ListView>();
        }

        private void SIOrderPaid_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var f402Form = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            if (f402Form != null)
                e.CanExecute = f402Form.DocumentType == Document.Types.SI && CanOrderBePaid<SIF402ListView>();
            else
                e.CanExecute = CanOrderBePaid<SIF402ListView>();
        }

        private void ShowChangeOrderStateDialog<T>(Func<DXWindow> dialogCreator, Func<long, ViewModelCard> viewModelCreator) where T : F402ListView
        {
            long id = 0;
            var f402ViewModel = App.DashboardManager.GetActiveViewModel() as F401402UKViewModel;
            var f402ListView = App.DashboardManager.GetActiveView() as T;
            if (f402ViewModel != null)
            {
                id = f402ViewModel.Detail.ID;
                if (f402ViewModel.SaveCard != null && f402ViewModel.SaveCard.CanExecute(null)
                    &&
                    MessageBox.Show("Не сохраненные данные будут утеряны. Продолжить?", "Подтверждение",
                                    MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.No)
                    return;
            }
            else if (f402ListView != null)
                id = f402ListView.GetSelectedF402ID();

            if (id <= 0) return;
            var dialog = dialogCreator();
            var viewModel = viewModelCreator(id);
            viewModel.State = ViewModelState.Create;

            dialog.DataContext = viewModel;
            dialog.Owner = App.mainWindow;
            ThemesTool.SetCurrentTheme(dialog);
            dialog.ShowDialog();
        }

        private void CreateSIOrderToPay_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<SIF402ListView>(() => new CreateOrderToPayDlg(), id =>
            {
                var vm = new CreateOrderToPayViewModel(id);
                vm.PostOpen("Создание поручения на оплату");
                return vm;
            });
        }

        private void SIOrderSent_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<SIF402ListView>(() => new OrderSentDlg(), id =>
            {
                var vm = new OrderSentViewModel(id);
                vm.PostOpen("Поручение отправлено");
                return vm;
            });
        }

        private void SIOrderDelivered_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<SIF402ListView>(() => new OrderDeliveredDlg(), id =>
            {
                var vm = new OrderDeliveredViewModel(id);
                vm.PostOpen("Поручение вручено");
                return vm;
            }
        );
        }

        private void SIOrderPaid_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShowChangeOrderStateDialog<SIF402ListView>(() => new OrderPaidDlg(), id =>
            {
                var vm = new OrderPaidViewModel(id);
                vm.PostOpen("Поручение оплачено");
                return vm;
            });
        }

        private void CreateSecurityInOrder_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;

            var vm = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
            var vmr = App.DashboardManager.GetActiveViewModel() as OrderRelayingViewModel;
            if (vm != null)
                e.CanExecute = vm.CanExecuteAddSecurities();
            else if (vmr != null)
                e.CanExecute = vmr.CanExecuteAddSecurities();
            else
            {
                var view = App.DashboardManager.GetActiveView() as OrdersListView;
                if (view != null && OrderStatusIdentifier.IsStatusOnSigning(view.GetSelectedOrderStatus()) &&
                    view.IsOrderSelected())
                    e.CanExecute = true;
            }
        }

        private void CreateSecurityInOrder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as OrderViewModel;
            var vmr = App.DashboardManager.GetActiveViewModel() as OrderRelayingViewModel;

            if (vm == null && vmr == null)
            {
                var view = App.DashboardManager.GetActiveView() as OrdersListView;
                if (view != null)
                {
                    var lID = view.GetSelectedOrderID();
                    if (lID > 0)
                    {
                        var id = view.GetSelectedRelayingOrderID(lID);
                        if (id == null)
                            App.DashboardManager.OpenNewTab(typeof(OrderView), ViewModelState.Edit, lID, "Поручение");
                        else
                            App.DashboardManager.OpenNewTab(
                                         typeof(OrderRelayingView), ViewModelState.Edit, id.Value, "Поручение на перекладку");
                    }
                }
            }

            if (vm != null && vm.CanExecuteAddSecurities())
            {
                var dlgVM = new SecurityInOrderViewModel(vm.Order, vm.SecurityInOrderType, vm.CbInOrderList);
                var dlg = new SecurityInOrderDialog { DataContext = dlgVM, Owner = App.mainWindow };
                ThemesTool.SetCurrentTheme(dlg);
                if (dlg.ShowDialog() == true)
                    vm.AddSecurities(dlgVM.CreatedItems);
            }
            else if (vmr != null && vmr.CanExecuteAddSecurities())
            {
                var dlgVM = new SecurityInOrderViewModel(vmr.Order, vmr.SecurityInOrderType, vmr.CbInOrderList);
                var dlg = new SecurityInOrderDialog { DataContext = dlgVM, Owner = App.mainWindow };
                ThemesTool.SetCurrentTheme(dlg);

                if (dlg.ShowDialog() == true)
                    vmr.AddSecurities(dlgVM.CreatedItems);
            }
        }

        private void CreatePFRBranch_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(PFRBranchView), ViewModelState.Create, "Создать отделение ПФР", "Отделение ПФР");
        }

        private void CreateSI_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(SIView), ViewModelState.Create, "Создать субъект", "Субъект");
        }

        private void CreateSIContact_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as SIContactListView;
            long siid = 0;

            if (view != null)
                siid = view.GetSelectedSIID();

            if (siid <= 0) return;
            var dialog = new AddSIContactDlg();
            var dialogVM = new SIContactDlgViewModel(siid) { State = ViewModelState.Create };
            dialog.DataContext = dialogVM;
            ThemesTool.SetCurrentTheme(dialog);

            if (dialog.ShowDialog() != true) return;
            var model = App.DashboardManager.GetActiveViewModel() as ViewModelList;
            model?.RefreshList.Execute(model.RefreshParameter);
        }

        private void CreateSIContract_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var source = (App.DashboardManager.GetActiveView() as ISISource) ??
                         (App.DashboardManager.GetActiveViewModel() as ISISource);
            long ownerID = 0;
            if (source != null)
                ownerID = source.GetSIID();

            if (ownerID > 0)
                App.DashboardManager.OpenNewTab(typeof(SIContractView), ViewModelState.Create,
                                                ownerID, "Создать договор СИ", "Договор СИ");
        }

        private void CreateSIContact_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as SIContactListView;
            e.CanExecute = view != null && view.IsSISelected();
        }

        private void CreateSIContract_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var source = (App.DashboardManager.GetActiveView() as ISISource)
                         ?? (App.DashboardManager.GetActiveViewModel() as ISISource);
            if (source != null && source.GetSIID() > 0)
                e.CanExecute = ViewModelBase.CheckGlobalAccess(typeof(SIContractViewModel),
                    typeof(EditAccessAttribute));
            else
                e.CanExecute = false;
        }

        private void CreateReorganization_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long lID = 0;
            long leID = 0;

            var leVM = App.DashboardManager.FindViewModel(typeof(NPFViewModel)) as NPFViewModel;

            if (leVM == null)
            {
                var clV = App.DashboardManager.GetActiveView() as NPFListView;
                if (clV != null)
                {
                    var item = clV.GetSelectedDB2NPFListItem();
                    lID = item.Contragent.ID;
                    leID = item.LegalEntity.ID;
                }
            }
            else
            {
                lID = leVM.ContragentID;
                leID = leVM.LegalEntity.ID;
            }

            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(ReorganizationView),
                                                ViewModelState.Create, lID, (object)leID, "Добавить реорганизацию", "Реорганизация");
        }

        private void CreateReorganization_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var vm = App.DashboardManager.GetActiveViewModel() as NPFViewModel;
            if (vm != null)
            {
                e.CanExecute = (vm.State == ViewModelState.Edit && vm.Status.ID == 1);
                return;
            }
            var clV = App.DashboardManager.GetActiveView() as NPFListView;
            if (clV != null)
            {
                var item = clV.GetSelectedDB2NPFListItem();
                e.CanExecute = item != null && item.Status.ID == 1;
                return;
            }

            e.CanExecute = false;
        }

        private void CreateDeadZL_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DeadZLAddView), ViewModelState.Create, "Добавить умерших ЗЛ", "Умершие ЗЛ");
        }

        private void CreateRegister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(RegisterView), ViewModelState.Create, "Добавить реестр", "Реестр");
        }

        private void CreateIncomeSecurities_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(IncomeSecurityView),
                                            ViewModelState.Create, "Создать поступления по временному размещению", "Поступления по временному размещению");
        }

        private void CreateInsurance_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(InsuranceSIView),
                                            ViewModelState.Create, (long)Document.Types.SI, "Добавить договор страхования СИ", "Договор страхования СИ");
        }

        private void GenerateRegister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var vm = App.DashboardManager.GetActiveViewModel() as ZLRedistListViewModel;

                Dictionary<bool, long> bResult = null;
                if (vm != null)
                    bResult = WCFClient.Client.GenerateRegisterByERZL(vm.ID);
                else
                {
                    var vm2 = App.DashboardManager.GetActiveViewModel() as ZLRedistViewModel;
                    if (vm2 != null)
                        bResult = WCFClient.Client.GenerateRegisterByERZL(vm2.ID);
                }
                if (bResult.ContainsKey(true))
                {
                    String num = bResult[true].ToString(CultureInfo.InvariantCulture) + ".";
                    JournalLogger.LogEvent("Реестр перечислений", JournalEventType.INSERT, null, "Реестр перечислений сгенерирован", $"Номер :{num} ");

                    DXMessageBox.Show("Финреестры успешно сгенерированы под номером " + num);
                    var vmRl = App.DashboardManager.FindViewModel<RegistersListViewModel>();
                    vmRl?.RefreshList.Execute(null);
                }
                else
                    DXMessageBox.Show("Во время генерации финреестров произошла ошибка!");
            }
            catch (Exception ex)
            {
                DXMessageBox.Show(Properties.Resources.GetDataErrorString,
                                  Properties.Resources.ApplicationName, MessageBoxButton.OK,
                                  MessageBoxImage.Error);
                App.log.WriteException(ex);
            }
        }

        private void GenerateRegister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var zlrlV = App.DashboardManager.GetActiveView() as ZLRedistListView;
            if (zlrlV != null)
            {
                if (zlrlV.IsZLRedistSelected())
                {
                    e.CanExecute = true;
                    return;
                }
            }
            else
            {
                var zlrVM = App.DashboardManager.GetActiveViewModel() as ZLRedistViewModel;
                if (zlrVM != null && zlrVM.State != ViewModelState.Create)
                {
                    e.CanExecute = true;
                    return;
                }
            }
            e.CanExecute = false;
        }

        private void CreateFinRegister_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            long ownerID;

            var view = App.DashboardManager.GetActiveView() as RegistersListView;
            if (view != null && view.IsRegisterSelected())
                ownerID = view.GetSelectedRegisterID();
            else
                ownerID = 0;

            if (App.DashboardManager.GetActiveViewType() == typeof(RegisterView))
            {
                var vm = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;
                if (vm != null)
                    ownerID = vm.ID > 0 ? vm.ID : -1;
            }
            if (ownerID > 0)
                App.DashboardManager.OpenNewTab(typeof(FinRegisterView), ViewModelState.Create, ownerID, "Добавить НПФ", "НПФ");
        }

        private void CreateFinRegister_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var regListUC = App.DashboardManager.GetActiveView() as RegistersListView;
            var rVM = App.DashboardManager.GetActiveViewModel() as RegisterViewModel;

            if (regListUC != null)
            {
                if (regListUC.IsRegisterSelected())
                {
                    e.CanExecute = true;
                    return;
                }
            }

            if (rVM != null && rVM.ID > 0)
            {
                e.CanExecute = true;
                return;
            }
            e.CanExecute = false;
        }


        private void RollbackStatus_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as ReqTransferView;
            if (view != null &&
                ((ReqTransferViewModel)view.DataContext).DocumentType == Document.Types.SI &&
                    !TransferStatusIdentifier.IsStatusInitialState(((ReqTransferViewModel)view.DataContext).Transfer.TransferStatus.ToLower()))
                e.CanExecute = true;
        }

        private void RollbackStatusVR_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var view = App.DashboardManager.GetActiveView() as ReqTransferView;
            if (view != null &&
                ((ReqTransferViewModel)view.DataContext).DocumentType == Document.Types.VR &&
                    !TransferStatusIdentifier.IsStatusInitialState(((ReqTransferViewModel)view.DataContext).Transfer.TransferStatus.ToLower()))
                e.CanExecute = true;
        }

        private void RollbackStatus_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (DXMessageBox.Show("Произвести откат статуса документа? Все данные текущего статуса будут удалены!", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;
            var view = App.DashboardManager.GetActiveView() as ReqTransferView;
            if (view == null) return;
            var panel = DashboardManager.Instance.GetActivePanel();
            if (panel == null) return;
            var vm = (ReqTransferViewModel)view.DataContext;
            var oldstatus = vm.Transfer.TransferStatus;

            string vmNewStatus;
            Type[] viewModelsToRefresh;
            //для акта подписан требуется вызов сервиса, поэтому пока вынесен отдельно
            if (TransferStatusIdentifier.IsStatusActSigned(vm.Transfer.TransferStatus))
            {
                WCFClient.Client.UpdateTransferByActSigned(
                               vm.Contract,
                               vm.Transfer,
                               vm.IsTransferFromPFRToUK,
                               TransferStatusIdentifier.ActionTransfer.RollbackStatus);
                viewModelsToRefresh = new[]
                {
                    typeof (UKTransfersListViewModel),
                    typeof (TransfersSIArchListViewModel),
                    typeof (TransfersVRArchListViewModel),
                    typeof (TransfersVRListViewModel),
                    typeof (TransfersSIListViewModel),
                    typeof (SIAssignPaymentsListViewModel),
                    typeof (VRAssignPaymentsListViewModel),
                    typeof (SPNMovementsSIListViewModel),
                    typeof (SPNMovementsVRListViewModel)
                };
                vmNewStatus = DataContainerFacade.GetByID<ReqTransfer>(vm.Transfer.ID).TransferStatus;
            }
            else
            {
                ReqTranserStatusHelper.RollbackStatus(vm, out viewModelsToRefresh);
                vmNewStatus = vm.Transfer.TransferStatus;
            }
            App.DashboardManager.RefreshListViewModels(viewModelsToRefresh);

            DashboardManager.Instance.DockManager.DockController.Close(panel);
            JournalLogger.LogRollbackEvent(vm, string.Format("{1} <- {0}", oldstatus, vmNewStatus));
        }

        #region Analyze DOKIP

        private void ShowPensionplacementPivotList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzePensionplacementPivotView), ViewModelState.Read, "Сводный отчет");
        }
        private void ShowPensionplacementReportList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzePensionPlacementReportListView), ViewModelState.Read, "Список отчетов");
        }

        private void ShowAddPensionplacementEditor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (PensionPlacementEditViewModel)App.DashboardManager.FindViewModel(typeof(PensionPlacementEditViewModel));
            if (lvm != null && lvm.State == ViewModelState.Create)
                return;

            var vm = new PensionPlacementEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzePensionplacementEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }

        private void ShowPensionpfundtonpfPivot_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzePensionfundtonpfPivotViewModel));

            if (lvm != null && lvm.Any(m => !((AnalyzePensionfundtonpfPivotViewModel)m).IsWithSubtraction))
                return;
            var vm = new AnalyzePensionfundtonpfPivotViewModel();
            var view = new AnalyzePensionfundtonpfPivotView();
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);
        }
        private void ShowPensionpfundtonpfWithSubPivot_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzePensionfundtonpfPivotViewModel));

            if (lvm != null && lvm.Any(m => ((AnalyzePensionfundtonpfPivotViewModel)m).IsWithSubtraction))
                return;

            var vm = new AnalyzePensionfundtonpfPivotViewModel(true);
            var view = new AnalyzePensionfundtonpfPivotView();
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);
        }

        private void ShowIncomingstatementPivot_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzeIncomingstatementsPivotView), ViewModelState.Read, "Сводный отчет");
        }

        private void ShowIncomingstatementList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzeIncomingstatementsListView), ViewModelState.Read, "Список отчетов");
        }


        private void ShowInsurepdersonPivot_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (AnalyzeInsuredpersonPivotViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzeInsuredpersonPivotViewModel));
            if (lvm != null)
                return;
            var vm = new AnalyzeInsuredpersonPivotViewModel();
            var view = new AnalyzeInsuredpersonPivotView(vm);
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);
            //App.DashboardManager.OpenNewTab(typeof(AnalyzeInsuredpersonPivotView), ViewModelState.Read, "Сводный отчет");
        }
        private void ShowAnalyzePaymentyearratePivot_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (AnalyzePaymentyearratePivotViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzePaymentyearratePivotViewModel));
            if (lvm != null)
                return;
            var vm = new AnalyzePaymentyearratePivotViewModel();
            var view = new AnalyzePaymentyearratePivotView(vm);
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);


        }
        private void ShowYieldfundsPivot107_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzeYieldfundsPivotViewModel));

            if (lvm != null && lvm.Any(m => ((AnalyzeYieldfundsPivotViewModel)m).ActID == 107))
                return;

            var vm = new AnalyzeYieldfundsPivotViewModel(107);
            var view = new AnalyzeYieldfundsPivotView(vm);
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);


        }
        private void ShowYieldfundsPivot140_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzeYieldfundsPivotViewModel));

            if (lvm != null && lvm.Any(m => ((AnalyzeYieldfundsPivotViewModel)m).ActID == 140))
                return;
            var vm = new AnalyzeYieldfundsPivotViewModel(140);
            var view = new AnalyzeYieldfundsPivotView(vm);
            App.DashboardManager.OpenNewTab("Сводный отчет", view, ViewModelState.Read, vm);


        }

        private void ShowAddPensionfundtonpfEditor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzePensionfundtonpfEditViewModel));

            if (lvm != null && lvm.Any(m => m.State == ViewModelState.Create && !((AnalyzePensionfundtonpfEditViewModel)m).WithSubtract))
                return;

            var vm = new AnalyzePensionfundtonpfEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, false, true);
            var view = new AnalyzePensionfundtonpfEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowAddPensionfundtonpfWithSubtrEditor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzePensionfundtonpfEditViewModel));

            if (lvm != null && lvm.Any(m => m.State == ViewModelState.Create && ((AnalyzePensionfundtonpfEditViewModel)m).WithSubtract))
                return;

            var vm = new AnalyzePensionfundtonpfEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true, true);
            var view = new AnalyzePensionfundtonpfEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowPensionfundtonpfList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = new AnalyzePensionfundtonpfListViewModel();
            var view = new AnalyzePensionfundtonpfListView();
            App.DashboardManager.OpenNewTab(@"Список отчетов", view, ViewModelState.Edit, vm);
        }
        private void ShowPensionfundtonpfSubtrList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = new AnalyzePensionfundtonpfWithReturnListViewModel();
            var view = new AnalyzePensionfundtonpfListView();
            App.DashboardManager.OpenNewTab(@"Список отчетов", view, ViewModelState.Edit, vm);

        }

        private void ShowPaymentyearrateAddNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (AnalyzePaymentyearrateEditViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzePaymentyearrateEditViewModel));
            if (lvm != null && lvm.State == ViewModelState.Create)
                return;

            var vm = new AnalyzePaymentyearrateEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzePaymentyearrateEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowPaymentyearrateList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzePaymentyearrateListView), ViewModelState.Read, "Список отчетов");
        }
        private void ShowInsurepdersonList_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(AnalyzeInsuredpersonListView), ViewModelState.Read, "Список отчетов");
        }

        private void ShowYieldfunds140List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Используется ViewModel с параметром
            var vm = new AnalyzeYieldfundsListViewModel(140);
            var view = new AnalyzeYieldfundsListView();
            App.DashboardManager.OpenNewTab(@"Список отчетов", view, ViewModelState.Edit, vm);

        }
        private void ShowYieldfunds107List_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var vm = new AnalyzeYieldfundsListViewModel(107);
            var view = new AnalyzeYieldfundsListView();
            App.DashboardManager.OpenNewTab(@"Список отчетов", view, ViewModelState.Edit, vm);
        }

        //private RibbonStateBL CheckRibbonPageState(object ribbon)
        //{
        //    var rbc = (RibbonControl)ribbon;
        //    if(rbc == null)
        //        throw new Exception("Ошибка определения ribbon control");
        //    var result = RibbonStateBL.ListsState;
        //    Enum.TryParse(rbc.SelectedPage.SerializationName, true, out result);
        //    return result;
        //}
        private void ShowYieldfunds140AddNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzeYieldfundsEditViewModel));

            if (lvm != null && lvm.Any(m => m.State == ViewModelState.Create && ((AnalyzeYieldfundsEditViewModel)m).ActID == 140))
                return;

            var vm = new AnalyzeYieldfundsEditViewModel(QuarkReportBase.InitNewReport(new AnalyzeYieldfundsReport { Act = 140 }), App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzeYieldfundsEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowYieldfunds107AddNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = App.DashboardManager.FindViewModelsList(typeof(AnalyzeYieldfundsEditViewModel));

            if (lvm != null && lvm.Any(m => m.State == ViewModelState.Create && ((AnalyzeYieldfundsEditViewModel)m).ActID == 107))
                return;

            var vm = new AnalyzeYieldfundsEditViewModel(QuarkReportBase.InitNewReport(new AnalyzeYieldfundsReport { Act = 107 }), App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzeYieldfundsEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }

        private void ShowIncomingstatementAddNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (AnalyzeIncomingstatementsEditViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzeIncomingstatementsEditViewModel));
            if (lvm != null && lvm.State == ViewModelState.Create)
                return;

            var vm = new AnalyzeIncomingstatementsEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzeIncomingstatementsEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowInsurepdersonAddNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            var lvm = (AnalyzeInsuredpersonEditViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzeInsuredpersonEditViewModel));
            if (lvm != null && lvm.State == ViewModelState.Create)
                return;

            var vm = new AnalyzeInsuredpersonEditViewModel(null, App.RibbonManager.GetCurrentPageRibbonState(), ViewModelState.Create, true);
            var view = new AnalyzeInsuredpersonEditView(vm);
            App.DashboardManager.OpenNewTab("Создание отчета", view, ViewModelState.Create, vm);
        }
        private void ShowAnalyzeConditionEditor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var lvm = (AnalyzeConditionEditorViewModel)App.DashboardManager.FindViewModel(typeof(AnalyzeConditionEditorViewModel));
            if (lvm != null)
                return;

            var vm = new AnalyzeConditionEditorViewModel();
            var view = new AnalyzeConditionEditorView(vm);
            App.DashboardManager.OpenNewTab("Редактор правил проверки заполнения отчетов аналитических показателей", view, ViewModelState.Edit, vm);
        }


        #endregion

        #endregion
    }
}
