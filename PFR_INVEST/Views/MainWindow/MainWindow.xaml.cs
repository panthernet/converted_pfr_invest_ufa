﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Docking;
using DevExpress.Xpf.Docking.Base;
using DevExpress.Xpf.Ribbon;
using DXCommonThemes;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Core.Settings;
using PFR_INVEST.Helpers.Localizers;
using PFR_INVEST.Ribbon;
using PFR_INVEST.Tools;
using PFR_INVEST.Views;
using PFR_INVEST.Views.Dialogs;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Views.Statusbar;
using PFR_INVEST.DataObjects.ListItems;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.Helpers;
using PFR_INVEST.Integration.XBRLProcessor;


namespace PFR_INVEST
{
    public partial class MainWindow
    {
        private bool _mRetryToConnect = true;
        private bool _bShutdown;
        public static MainWindow Instance { get; private set; }
        private const string TITLE = "ПТК ДОКИП / ";

        /// <summary>
        /// Логин юзера для системных операций
        /// </summary>
        public string ActualUserLogin => WCFClient.ClientCredentials.Windows.ClientCredential.UserName;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private double _frameCounter;
        private readonly DispatcherTimer _activityTimer;
        private Point _inactiveMousePosition = new Point(0, 0);
        public IDXThemeFeature ThemeFeature { get; set; }

        #region MainWindow() constructor

        public MainWindow()
        {
            InitializeComponent();

           // var x = new XbrlParser(@"C:\! 4 кв. 2018\22-03У004_ep_nso_aif_uk_m_q_10d_spec_dep_20180930.xml", App.log);
           // var c = x.GenerateF060XML();
          //  var c2 = x.GenerateF070XML();

             //   PFR_INVEST.Integration

            //DataContext = this;
            // RightsParser.Execute(defCat,barManager);
            // var x = (System.Windows.Media.RenderCapability.Tier >> 16).ToString();
            try
            {
                ThemeFeature = ThemesTool.UpdateFeature(featCanvas);
                _activityTimer = new DispatcherTimer(
                    TimeSpan.FromMinutes(1),
                    DispatcherPriority.ApplicationIdle,
                    (s, e2) =>
                    {
                        _inactiveMousePosition = Mouse.GetPosition(featCanvas);
                        ThemeFeature?.Start();
                    },
                    Application.Current.Dispatcher);
                InputManager.Current.PreProcessInput += OnActivity;
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex, "ОШИБКА инициализации feature темы!");
            }

#if UseWorkWithRegions
            bRegionReportExport.IsVisible = true;
            bRegions.IsVisible = true;
#else
            bRegionReportExport.IsVisible = false;
            bRegions.IsVisible = false;
#endif

            DockingLocalizer.Active = new DLLocalizer();
            if (AppSettings.ShowFPS)
            {
                bFPS.IsVisible = true;
                CompositionTarget.Rendering += CompositionTarget_Rendering;
            }

            #region GridSearchCommand

            GridSearchCommand = new RoutedCommand("GridSearchCommand", typeof(MainWindow));

            var binding = new CommandBinding() {Command = GridSearchCommand};
            binding.Executed += GridSearchExecuted;
            binding.CanExecute += GridSearchCanExecute;

            CommandBindings.Add(binding);

            #endregion

            Instance = this;
            bUserInfo.Content = Properties.Resources.UnauthorizedNotification;
            bUserName.Content = "[anonymous]";

            var connected = true;
            while (_mRetryToConnect)
            {
                connected = OpenConnection(!connected);
                if (_bShutdown)
                {
                    Visibility = Visibility.Hidden;
                    return;
                }
                if (connected)
                    break;
            }
            if (!connected)
                return;

            using (Loading.StartNew("Установка соединения..."))
            {
                App.ServerSettings = WCFClient.Client.OpenDBConnection();

                if (!App.ServerSettings.IsOpfrTransfersEnabled)
                    opfr1.IsVisible = opfr2.IsVisible = opfr3.IsVisible = false;

                //обновляем логгер, для отображения БД в имени файла
                var logFile = Path.Combine(new AppUserSettingsProvider().GetAppSettingsPath(), $"error_{App.ServerSettings.DbName}.log");
                App.log.Dispose();
                App.log = new Common.Logger.Log(logFile);

                Loading.SetPublicMessage("Загрузка настроек...");
                App.UserSettingsManager.Initialize();

                Loading.SetPublicMessage("Загрузка интерфейса....");
                App.DashboardManager = new DashboardManager(dockManager);
                App.RibbonManager = new RibbonManager(RibbonControl, barManager);
                App.StatusbarManager = new StatusbarManager(barStatus);
                BindRibbonServerCommands();
                BindRibbonOtherCommands();
                App.RibbonManager.State = RibbonStates.ListsState;
                Title = TITLE + bSpravochniki.Content;
                Closing += Window_Closed;



                AppUserSettingsProvider.User = ActualUserLogin;
                AppUserSettingsProvider.Domain = WCFClient.ClientCredentials.Windows.ClientCredential.Domain;

                bUserInfo.Content = ActualUserLogin;
                bUserName.Content = $"[{WCFClient.ClientCredentials.Windows.ClientCredential.Domain}\\{WCFClient.ClientCredentials.Windows.ClientCredential.UserName}]";
                if (!AppSettings.DisplayWindowClassName) bDebug.IsVisible = bDebugSize.IsVisible = false;
                else
                {
                    bDebug.IsVisible = bDebugSize.IsVisible = true;
                    bDebug.ItemClick += bDebug_ItemClick;
                    bDebugSize.ItemClick += bDebugSize_ItemClick;
                }

                if (!AppSettings.DisplayReqTransferRecalcButton)
                    butRecalcReqTr.IsVisible = false;



                Loading.SetPublicMessage("Применяем цветовую схему...");
                ThemesTool.SetCurrentTheme(this);
            }

            //Включение/выключение отображения кнопки настроек оповещений (db2connector web.config)
            if (!WCFClient.Client.IsInfoAlertsEnabled())
                ribAlertSettings.Tag = "NO_VIEWERS";

            OnShowMainWindow();
            //// WCFClient.Client.TestKIP(File.ReadAllBytes(@"D:/Projects/PFR/ДЛЯ ИМПОРТА/KIP/I_RECALL_MSK_UK-1481094333378.zip"));
        }

        void bDebugSize_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                Clipboard.SetData(DataFormats.UnicodeText, (string)e.Item.Content);
            }
            catch
            {
                // ignored
            }
        }

        void bDebug_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            try
            {
                Clipboard.SetData(DataFormats.Text, (string)e.Item.Tag);
            }
            catch
            {
                // ignored
            }
        }
        #endregion

        private void OnActivity(object sender, PreProcessInputEventArgs e)
        {
            try
            {
                if (!IsLoaded || !IsVisible) return;
                var inputEventArgs = e.StagingItem.Input;
                if (!(inputEventArgs is MouseEventArgs) && !(inputEventArgs is KeyboardEventArgs)) return;
                var mouseEventArgs = e.StagingItem.Input as MouseEventArgs;

                // no button is pressed and the position is still the same as the application became inactive
                if (mouseEventArgs?.LeftButton == MouseButtonState.Released &&
                    mouseEventArgs.RightButton == MouseButtonState.Released &&
                    mouseEventArgs.MiddleButton == MouseButtonState.Released &&
                    mouseEventArgs.XButton1 == MouseButtonState.Released &&
                    mouseEventArgs.XButton2 == MouseButtonState.Released &&
                    _inactiveMousePosition == mouseEventArgs.GetPosition(featCanvas))
                    return;

                ThemeFeature?.Stop();
                if (_activityTimer == null) return;
                _activityTimer.Stop();
                _activityTimer.Start();
            }
            catch (Exception ex)
            {
                App.log.WriteException(ex, "ОШИБКА при проверке активности приложения!");
            }
        }

        #region GridSearchCommand

        public static readonly DependencyProperty GridSearchCommandProperty =
            DependencyProperty.Register("GridSearchCommand", typeof(RoutedCommand), typeof(MainWindow));

        public static RoutedCommand GridSearchCommand { get; private set; }

        private UserControl SavedVM { get; set; }
        private int SavedCounter { get; set; }
        private bool DeadLock { get; set; }

        private void GridSearchExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (string.IsNullOrEmpty((string)e.Parameter)) return;
            var searchString = e.Parameter.ToString();
            var view = App.DashboardManager.GetActiveView();
            if (view == null) return;
            var counter = SavedCounter;
            if (!Equals(view, SavedVM))
            {
                counter = SavedCounter = 0;
                SavedVM = view;
            }

            var vm = view as ISearchGrid;
            if (vm == null) return;

            var ftarget = e.OriginalSource as DevExpress.Xpf.Editors.ButtonEdit;
            var grid = vm.GetGridControl();

            while (counter < grid.VisibleRowCount)
            {
                var rowhandle = grid.GetRowHandleByListIndex(counter);
                if (grid.IsGroupRowHandle(rowhandle)) continue;
                if ((from column in grid.Columns where column.Visible select Convert.ToString(grid.GetCellValue(rowhandle, column))).Any(cellvalue => cellvalue.ToLower().Contains(searchString.ToLower())))
                {
                    grid.View.FocusedRowHandle = rowhandle;
                    SavedCounter = counter + 1;
                    DeadLock = false;
                    ftarget?.Focus();
                    return;
                }

                counter++;
            }
            if (SavedCounter != 0) //если уже были совпадения
            {
                SavedCounter = 0; //сбрасываем счетчик
                if (!DeadLock) //не дедлочим?
                {
                    GridSearchExecuted(sender, e); //прогоняем поиск с 0 счетчиком
                    DeadLock = false; //сбрасываем дедлок
                }
            }
            ftarget?.Focus();
        }

        private static void GridSearchCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (App.DashboardManager.GetActiveView() as ISearchGrid) != null;
        }

        #endregion

        #region Card implementations

        static void DeleteCard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //DX bug hack - сбрасывает текущий фокус окна после вызова мессадж бокса :(
            var vm = DashboardManager.Instance.DockManager.ActiveDockItem;
            var model = DashboardManager.Instance.GetActiveViewModel() as ViewModelCard;
            if (model == null) return;

            var message = "Удалить выбранный элемент?";
            //если переопределено сообщение на удаление,  берем его
            if (!string.IsNullOrEmpty(model.CustomDeleteMessage))
                message = model.CustomDeleteMessage;

            if (DXMessageBox.Show(message, "Удаление", MessageBoxButton.YesNo, MessageBoxImage.Question) ==
                MessageBoxResult.No)
            {
                DashboardManager.Instance.DockManager.ActiveDockItem = vm;
                return;
            }
            DashboardManager.Instance.DockManager.ActiveDockItem = vm;
            App.DashboardManager.DeleteActiveCard();
        }

        static void DeleteCard_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = App.DashboardManager.IsDeletableCardActive();
        }

        static void SaveCard_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.SaveActiveCard();
        }

        static void SaveCard_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = App.DashboardManager.IsSavableCardActive();
        }

        static void RestoreDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            App.DashboardManager.OpenNewTab(typeof(DeleteDocumentsListView), ViewModelState.Read, "Список удаленных документов");
        }

        static void RestoreDocument_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }


        #endregion

        #region MainMenu implementations
        private void OnCloseApplication(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }

        private void OnShowLists(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.ListsState;
            Title = TITLE + bSpravochniki.Content;
        }

        private void OnShowPreferences(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.SettingsState;
            Title = TITLE + bSettings.Content;
        }

        private void OnShowNPF(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.NPFState;
            Title = TITLE + bNPF.Content;
        }

        private void OnShowSI(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.SIState;
            Title = TITLE + bSI.Content;
        }

        private void OnShowVR(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.VRState;
            Title = TITLE + bVR.Content;
        }

        private void OnShowCB(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.CBState;
            Title = TITLE + bCB.Content;
        }

        private void OnShowRegions(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.RegionsState;
            Title = TITLE + bRegions.Content;
        }

        private void OnShowBackOffice(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.BackOfficeState;
            Title = TITLE + bOffice.Content;
        }
        private void OnShowAnalyze(object sender, ExecutedRoutedEventArgs e)
        {
            App.RibbonManager.State = RibbonStates.AnalyzeState;
            Title = TITLE + bAnalyze.Content;
        }
        #endregion

        #region Closing...
        private void Window_Closed(object sender, EventArgs e)
        {
            App.IsClosing = true;
            Loading.CloseWindow();
            if (WCFClient.Client != null)
                WCFClient.CloseClient();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            while (dockManager.FloatGroups.Count > 0)
            {
                var floatGroup = dockManager.FloatGroups[0];
                while (floatGroup.Items.Count > 0)
                {
                    var layoutBaseItem = floatGroup.Items[0];
                    if (layoutBaseItem is LayoutPanel)
                    {
                        dockManager.DockController.Close(layoutBaseItem);
                        continue;
                    }
                    if (layoutBaseItem is LayoutGroup)
                        CloseLayoutGroupRecursively(layoutBaseItem as LayoutGroup);
                }
            }

            while (dockManager.AutoHideGroups.Count > 0)
            {
                var autoHideGroup = dockManager.AutoHideGroups[0];
                while (autoHideGroup.Items.Count > 0)
                    dockManager.DockController.Close(autoHideGroup.Items[0]);
            }

            while (dockManager.LayoutRoot.Items.Count > 0)
            {
                var layoutBaseItem = dockManager.LayoutRoot.Items[0];
                if (layoutBaseItem is LayoutPanel)
                    dockManager.DockController.Close(layoutBaseItem);
                else if (layoutBaseItem is LayoutGroup)
                    CloseLayoutGroupRecursively(layoutBaseItem as LayoutGroup);
            }

            App.UserSettingsManager.Save();
        }

        private void CloseLayoutGroupRecursively(LayoutGroup layoutGroup)
        {
            while (layoutGroup.Items.Count > 0)
            {
                var layoutBaseItem = layoutGroup.Items[0];
                if (layoutBaseItem is LayoutPanel)
                    dockManager.DockController.Close(layoutBaseItem);
                else if (layoutBaseItem is LayoutGroup)
                    CloseLayoutGroupRecursively((LayoutGroup)layoutBaseItem);
            }
        }
        #endregion

        #region OnShowMainWindow

        private static void OnShowMainWindow()
        {
            if(AppSettings.DisplayLoginMessages)
                ShowLoginMesages();
        }

        /// <summary>
        /// Показ сообщений юзеру при старте клиента после авторизации
        /// в т.ч. о просроченых ЭЦП и довереностях для банков
        /// </summary>
        private static void ShowLoginMesages()
        {
            try
            {
                BlUserSettings.Current.Load();

                var messages = new List<LoginMessageItem>();
                if (AP.Provider.CurrentUserSecurity.CheckUserInAnyRole(DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.Administrator))
                    messages.AddRange(DataContainerFacade.GetLoginMessageItems());

                /* Проверка заполняемости отчетов аналитических показателей */
                if (WCFClient.Client.IsInfoAlertsEnabled())
                {
                    AnalyzeConditionCollection col = new AnalyzeConditionCollection();
                    var roles = AP.Provider.CurrentUserSecurity.GetRoles();
                    var msgs = col.CheckUnfilledReports(roles);
                    if (msgs.Any())
                        messages.AddRange(msgs);
                }

                var rops = DevExpressHelper.ROPSRetriesWrapper(10, () => DataContainerFacade.GetClient().GetROPSLimitMessage(false, true));
                if (rops != null)
                {
                    App.StatusbarManager.SetErrorMessage(rops);
                    messages.Add(rops);
                }

                var msg = WCFClient.Client.GetCBReportForApproveMessage();
                if (msg != null)
                {
                    App.StatusbarManager.SetCBReportAppMessage(msg);
                    messages.Add(msg);
                }

                new DialogHelper().ShowLoginMessages(messages.ToArray());
            }
            catch (Exception ex)
            {
                DXMessageBox.Show("Произошла ошибка при проверке сроков лицензий уполномоченных лиц банков");
                App.log.WriteException(ex);
            }
        }

        private static IEnumerable<LoginMessageItem> GetAnalyzeReportMessages(DOKIP_ROLE_TYPE role, AnalyzeConditionCollection col)
        {
            var result = new List<LoginMessageItem>();
            foreach (var con in DataContainerFacade.GetList<AnalyzeCondition>().Where(c => c.Status == 1 && ((DOKIP_ROLE_TYPE)c.UserRoleFlags).HasFlag(role)))
            {
                var checkClass = col[con.ClassName];
                if (checkClass != null)
                {
                    var conMessages = new List<LoginMessageItem>();
                    if (checkClass.CheckCondition(conMessages, con))
                        result.AddRange(conMessages);
                }
            }
            return result;
        }

        #endregion

        #region Debug methods
        public static void DebugPrintText(string text, string tName = null)
        {
            Instance.bDebug.Content = "DEBUG: " + text;
            Instance.bDebug.Tag = tName;
        }
        public static void DebugPrintWindowSize(Size size)
        {
            Instance.bDebugSize.Content = "Size: " + size;
        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            if (_frameCounter++ == 0)
                _stopwatch.Start();

            if (!(_stopwatch.Elapsed.TotalSeconds > 1)) return;
            bFPS.Content = $"FPS: {_frameCounter}";
            _frameCounter = 0;
            _stopwatch.Reset();
        }

        #endregion

        private void bImportDEPT_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HideShowAuctionItems();
        }
        private void bImportDEPT_Popup(object sender, EventArgs e)
        {
            HideShowAuctionItems();
        }

        private void bExportDEPT_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HideShowAuctionItems();
        }

        private void HideShowAuctionItems()
        {
            var spvb = "bImportDepClaim2_SPVB|bExportCutoffRate_SPVB|bImportDepClaim2Confirm_SPVB".Split('|');
            var msk = "bImportDepClaimBatch|bImportDepclaimList|bImportDepclaimMaxList|bImportDepclaimMaxList|bImportDepClaim2|bImportDepClaim2Confirm|bExportDepclaimList|bShowExportsDepclaimMaxList|bExportDepClaim2|bExportDepClaim2Confirm".Split('|');

            var vmS = App.DashboardManager.GetActiveViewModel() as IDepClaimStockProvider;
            if (vmS?.AuctionID != null && vmS.IsMoscowStockSelected)
            {
                foreach (var s in spvb)
                    App.RibbonManager.SetVisible(s, false);
                foreach (var s in msk)
                    App.RibbonManager.SetVisible(s, true);
            }
            else if (vmS?.AuctionID != null && vmS.IsSPVBStockSelected)
            {
                App.RibbonManager.SetVisible("bImportDepClaim2", false);
                foreach (var s in spvb)
                    App.RibbonManager.SetVisible(s, true);
                foreach (var s in msk)
                    App.RibbonManager.SetVisible(s, false);
            }
            else
            {
                //Во всех других случаях - показываем МСК, как и было раньше
                foreach (var s in spvb)
                    App.RibbonManager.SetVisible(s, false);
                foreach (var s in msk)
                    App.RibbonManager.SetVisible(s, true);
            }
        }
    }
}