﻿using System.Collections.Generic;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for CostsListView.xaml
    /// </summary>
    public partial class CostsListView
    {
        public CostsListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, Grid, "List");
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (Grid.View.GetRowElementByMouseEventArgs(e) == null) return;
            var selectedID = GetSelectedID();
            if (selectedID > 0)
                App.DashboardManager.OpenNewTab(typeof(CostsView), ViewModelState.Edit, selectedID, "Расходы");

            var modelC = App.DashboardManager.GetActiveViewModel() as CostsViewModel;
            if (modelC != null)
                modelC.IsDataChanged = modelC.IsDataChanged || modelC.Rate == 0;
        }

        public long GetSelectedID()
        {
            if (Grid.View.FocusedRowHandle >= 0)
                return (long)Grid.GetCellValue(Grid.View.FocusedRowHandle, "ID");

            return 0;
        }

        public string GetSelectedOperation()
        {
            if (Grid.View.FocusedRowHandle >= 0 && Grid.View.FocusedRowHandle > -1000)
                return Grid.GetCellValue(Grid.View.FocusedRowHandle, "Operation").ToString();

            return string.Empty;
        }

        // TODO: проверить, почему не всегда работает приведение к типу List<object>
        private void CostsListViewShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);

            if (e.Column.FieldName != "MonthValue") return;

            var sortedItems = new List<CustomComboBoxItem>();
            var commonItems = new List<CustomComboBoxItem>();

            var filterTypedItems = e.ComboBoxEdit.ItemsSource as List<CustomComboBoxItem>;
            var filterItems = e.ComboBoxEdit.ItemsSource as List<object>;

            if (filterTypedItems != null)
            {
                foreach (var item in filterTypedItems)
                {
                    var editvalue = item.EditValue as int?;
                    if (editvalue.HasValue)
                        sortedItems.Add(item);
                    else
                        commonItems.Add(item);
                }
            }

            if (filterItems != null)
            {
                foreach (var o in filterItems)
                {
                    var item = o as CustomComboBoxItem;
                    if (item == null)
                        continue;
                    var editvalue = item.EditValue as int?;
                    if (editvalue.HasValue)
                        sortedItems.Add(item);
                    else
                        commonItems.Add(item);
                }
            }

            sortedItems.Sort((x, y) => ((int?) x.EditValue).Value - ((int?) y.EditValue).Value);
            sortedItems.ForEach(x => x.DisplayValue = DateTools.GetMonthInWordsForDate(((int?) x.EditValue).Value));
            commonItems.AddRange(sortedItems);
            e.ComboBoxEdit.ItemsSource = commonItems;
        }
    }
}
