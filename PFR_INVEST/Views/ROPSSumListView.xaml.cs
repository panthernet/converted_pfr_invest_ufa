﻿using System.Windows.Controls;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RatingsList.xaml
    /// </summary>
    public partial class ROPSSumListView : UserControl
    {
		public ROPSSumListView()
        {
            InitializeComponent();           
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
			var item = Grid.SelectedItem as ROPSSum;
			if(item != null)
			{
				App.DashboardManager.OpenNewTab(typeof(ROPSSumView), ViewModelState.Edit, item.ID, "Распоряжение по РОПС");				
			} 
        }		       
    }
}
