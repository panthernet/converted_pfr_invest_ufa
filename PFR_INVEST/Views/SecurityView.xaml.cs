﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for SecurityView.xaml
    /// </summary>
    public partial class SecurityView : UserControl
    {
        private SecurityViewModel Model => DataContext as SecurityViewModel;

        public SecurityView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }



        /*private void Delete_Click(object sender, RoutedEventArgs e)
        {
            SecurityViewModel secVM = this.DataContext as SecurityViewModel;
            if (secVM != null)
            {
                secVM.DeletePayment.Execute(null);
            }
            this.PaymentsGrid.RefreshData();
        }*/

        private void PaymentsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PaymentsGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            SecurityViewModel vm = DataContext as SecurityViewModel;
            if (vm != null)
            {
                long paymentID = GetSelectedPaymentID();
                vm.ViewPayment(paymentID);
            }
            PaymentsGrid.RefreshData();
        }

        public long GetSelectedPaymentID()
        {
            return PaymentsGrid.View.FocusedRowHandle >= 0 ? Convert.ToInt64(PaymentsGrid.GetCellValue(PaymentsGrid.View.FocusedRowHandle, "ID")) : 0;
        }

        private void but_addmp_Click(object sender, RoutedEventArgs e)
        {

        }

       /* private void but_delmp_Click(object sender, RoutedEventArgs e)
        {
            SecurityViewModel secVM = this.DataContext as SecurityViewModel;
            if (secVM != null)
            {
                secVM.DeletePayment.Execute(null);
            }
            this.PaymentsGrid.RefreshData();
        }*/

        private void PaymentsGrid_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            PaymentsGrid.RefreshData();
        }

        private void PriceGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PriceGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            if (PriceGrid.View.FocusedRow is MarketPrice)
            {
                long identity = (PriceGrid.View.FocusedRow as MarketPrice).ID;
                MarketPriceViewModel vmMP = new MarketPriceViewModel(Model, identity);
                MarketPriceView vMP = new MarketPriceView();
                App.DashboardManager.OpenNewTab("Рыночная цена", vMP, ViewModelState.Edit, vmMP);
            }
        }
    }
}
