﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PFRBranchesListView.xaml
    /// </summary>
    public partial class PFRBranchesListView
    {
        public PFRBranchesListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, Grid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {            
            var item = Grid.SelectedItem as PFRBranchListItem;
            if(item != null)
                App.DashboardManager.OpenNewTab(typeof(PFRBranchView), ViewModelState.Edit, item.ID, "Отделение ПФР");
        }     
    }
}
