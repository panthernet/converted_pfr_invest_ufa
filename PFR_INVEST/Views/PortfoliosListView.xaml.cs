﻿using System;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for PortfoliosListView.xaml
    /// </summary>
    public partial class PortfoliosListView
    {
        private bool _mGridRowMouseDoubleClicked;

        public PortfoliosListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, PortfoliosGrid);
        }

        private void TableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (PortfoliosGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsPortfolioSelected())
                return;
            _mGridRowMouseDoubleClicked = true;
            var lID = GetSelectedPortfolioID();
            if (lID > 0)
                App.DashboardManager.OpenNewTab(typeof(PortfolioView), ViewModelState.Edit, lID, "Портфель ПФР");
        }

        public bool IsPortfolioSelected()
        {
            var lvl = PortfoliosGrid.View.FocusedRowData.Level;
            if (lvl < PortfoliosGrid.GetGroupedColumns().Count)
                return (PortfoliosGrid.GetGroupedColumns()[lvl].FieldName == "PFName");
            return false;
        }

        public long GetSelectedPortfolioID()
        {
            return Convert.ToInt64(PortfoliosGrid.GetCellValue(PortfoliosGrid.View.FocusedRowHandle, "PortfolioID"));
        }

        private void ProcessCollapsingOrExpanding(object sender, RowAllowEventArgs e)
        {
            e.Allow = !_mGridRowMouseDoubleClicked;
            _mGridRowMouseDoubleClicked = false;
        }
    }
}
