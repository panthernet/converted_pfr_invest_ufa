﻿using System;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for RoleView.xaml
    /// </summary>
    public partial class ADRoleView
    {
        private ADRoleViewModel Model => DataContext as ADRoleViewModel;

        public ADRoleView()
        {
            InitializeComponent();
            Loaded += ViewLoaded;
        }

        private void ViewLoaded(object sender, RoutedEventArgs e)
        {
            Model.OnListUpdate += ListUpdate;
        }

        private void ListUpdate(object sender, EventArgs e)
        {
            RefreshDataContext();
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RefreshDataContext();
        }

        private void RefreshDataContext()
        {
            //var role = Model.Role;

            Model.UpdateUsersMembership();

            listGrid1.DataContext = Model.CurrentUsers;
            listGrid2.DataContext = Model.AllUsers;
        }

        private void ButtonAddUser_Click(object sender, RoutedEventArgs e)
        {
            var rm = DataContext as ADRoleViewModel;

            if (rm == null)
                return;

            rm.IsDataChanged = true;

            var addedUsers = rm.AddedUsers;
            var removedUsers = rm.RemovedUsers;

            //  Добавим пользователей
            var i = listGrid2.SelectedItems.Count - 1;
            while (i >= 0)
            {
                var u = listGrid2.SelectedItems[i] as User;

                if (u != null)
                {
                    if (removedUsers.Contains(u))
                        removedUsers.Remove(u);
                    else
                        addedUsers.Add(u);

                    Model.AllUsers.Remove(u);
                    Model.CurrentUsers.Add(u);
                }
                i--;
            }
        }

        private void ButtonRemoveUser_Click(object sender, RoutedEventArgs e)
        {
            var rm = DataContext as ADRoleViewModel;

            if (rm == null)
                return;

            rm.IsDataChanged = true;

            var addedUsers = rm.AddedUsers;
            var removedUsers = rm.RemovedUsers;

            //  Удалим пользователей
            var i = listGrid1.SelectedItems.Count - 1;
            while (i >= 0)
            {
                var u = listGrid1.SelectedItems[i] as User;

                if (u != null)
                {
                    if (addedUsers.Contains(u))
                        addedUsers.Remove(u);
                    else
                        removedUsers.Add(u);

                    Model.AllUsers.Add(u);
                    Model.CurrentUsers.Remove(u);
                }
                i--;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Model.SaveCard.Execute(null);
        }
    }
}