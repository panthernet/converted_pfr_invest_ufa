﻿using System;
using System.Windows.Input;
using DevExpress.Data;
using PFR_INVEST.BusinessLogic;
using DevExpress.Xpf.Grid;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for F40ListView.xaml
    /// </summary>
    public partial class F40ListView
    {
        public F40ListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, f40Grid);
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
            ModelInteractionHelper.SubscribeForPageLoadModel(this);
            ModelInteractionHelper.SubscribeForGridRefreshData(this, f40Grid, "List");
        }

        #region Events

        private void tableView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (f40Grid.View.GetRowElementByMouseEventArgs(e) == null)
                return;
            if (f40Grid.View.FocusedRowHandle < 0)
                return;
            object value = f40Grid.GetCellValue(f40Grid.View.FocusedRowHandle, "EdoID");

            App.DashboardManager.OpenNewTab(typeof(F40View),
                ViewModelState.Edit,
                (long)value, "Отчёт по сделкам");
        }

        #endregion

        #region Sort
        
        private void f40Grid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName.ToLower().Contains("month"))
            {
                string s1 = e.Value1.ToString().Trim();
                string s2 = e.Value2.ToString().Trim();
                int m1 = DateUtil.GetMonthIndex(s1, e.SortOrder == ColumnSortOrder.Descending);
                int m2 = DateUtil.GetMonthIndex(s2, e.SortOrder == ColumnSortOrder.Descending);
                e.Result = e.SortOrder == DevExpress.Data.ColumnSortOrder.Descending ? m2.CompareTo(m1) : m1.CompareTo(m2);
                e.Handled = true;
            }
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }

        #endregion

        [Obsolete]
        public long GetSelectedID()
        {
            if (f40Grid.View.FocusedRowHandle > -10000)
            {
                if (f40Grid.View.FocusedRowHandle >= 0)
                {
                    return (long)f40Grid.GetCellValue(f40Grid.View.FocusedRowHandle, "EdoID");
                }
            }
            return 0;
        }
                

    }
}
