﻿using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for KBKListView.xaml
	/// </summary>
	public partial class PaymentDetailListView
	{

		public PaymentDetailListView()
		{
			InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, grdList);
        }

		protected void tvKBKList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var item = grdList.SelectedItem as PaymentDetail;
			if (item != null)
				App.DashboardManager.OpenNewTab(typeof(PaymentDetailView), ViewModelState.Edit, item.ID, "Назначение платежа");
		}
	}
}
