﻿using System.Windows;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for DepositsListView.xaml
    /// </summary>
    public partial class DepositsListByPortfolioView
    {
        public DepositsListByPortfolioView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, ListGrid);
        }

        public DepositListItem GetSelectedItem()
        {
            if (ListGrid.View.FocusedRowHandle >= 0 && ListGrid.View.FocusedRowHandle > -1000)
                return ListGrid.SelectedItem as DepositListItem;
            return null;
        }

        private void ListGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListGrid.View.GetRowElementByMouseEventArgs(e) == null) return;
            var x = GetSelectedItem();

            if (x == null) return;
            if (!x.IsExtra)
                App.DashboardManager.OpenNewTab(typeof(DepositView), ViewModelState.Edit, x.ID, "Депозит");
            else
                MessageBox.Show("Данные мигрированы. Форма не подлежит открытию");
        }

        public long GetSelectedID()
        {
            if (ListGrid.View.FocusedRowHandle < 0) return 0;
            try
            {
                return (long)ListGrid.GetCellValue(ListGrid.View.FocusedRowHandle, "ID");
            }
            catch
            {
                return 0;
            }
        }

        public long GetSelectedPortfolioID()
        {
            if (ListGrid.View.FocusedRowHandle <= -10000) return 0;
            try
            {
                return (long)ListGrid.GetCellValue(ListGrid.View.FocusedRowHandle, "PortfolioID");
            }
            catch
            {
                return 0;
            }
        }
        
        private void ListGrid_CustomColumnSort(object sender, CustomColumnSortEventArgs e)
        {
            e.Result = ComponentHelper.OnColumnCustomSort(sender, e);
            e.Handled = true;
        }

        private void OnShowFilterPopup(object sender, FilterPopupEventArgs e)
        {
            ComponentHelper.OnColumnFilterOptionsSort(sender, e);
        }
    }

}
