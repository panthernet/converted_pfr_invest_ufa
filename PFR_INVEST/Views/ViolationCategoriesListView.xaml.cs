﻿using System;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ViolationCategoriesListView.xaml
    /// </summary>
    public partial class ViolationCategoriesListView
    {
        public ViolationCategoriesListView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, CategoryF140ListGrid);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CategoryF140ListGrid.View.GetRowElementByMouseEventArgs(e) == null)
                return;

            if (!IsViolationCategorySelected())
                return;
            var lID = GetSelectedViolationCategoryID();
            if (lID != 0)
                App.DashboardManager.OpenNewTab(typeof(ViolationCategoryView), ViewModelState.Edit, lID, "Категория нарушения");
        }

        private bool IsViolationCategorySelected()
        {
            return CategoryF140ListGrid.View.FocusedRowData.Level >= CategoryF140ListGrid.GetGroupedColumns().Count && CategoryF140ListGrid.SelectedItem != null;
        }

        private long GetSelectedViolationCategoryID()
        {
            return Convert.ToInt64(CategoryF140ListGrid.GetCellValue(CategoryF140ListGrid.View.FocusedRowHandle, "ID"));
        }
    }
}
