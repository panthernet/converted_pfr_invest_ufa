﻿using System.Windows.Controls;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for EnterTransferPPDataView.xaml
    /// </summary>
    public partial class CommonPPSplitView : UserControl
    {
        public CommonPPSplitView()
        {
            InitializeComponent();
            ModelInteractionHelper.SignUpForCustomAction(this, a => DashboardManager.CloseActiveView());
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
