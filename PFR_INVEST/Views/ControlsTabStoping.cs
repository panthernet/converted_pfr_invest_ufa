﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace PFR_INVEST.Views
{
    public static class ControlsTabStoping
    {
        /// <summary>
        /// Обработать все дочерние контролы и подконтролы родителя, 
        /// с целью упорядочивания перехода между контролами по табу
        /// </summary>
        /// <param name="parent">Родительский контрол</param>
        public static void Apply(DependencyObject parent)
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                var labelControl = child as Label;
                if (labelControl != null)
                    labelControl.IsTabStop = false;
                else
                {
                    var multiLabel = child as MultiLabel;
                    if (multiLabel != null)
                    {
                        multiLabel.IsTabStop = false;
                        multiLabel.IsReadOnly = true;
                    }
                    else
                    {
                        var tbControl = child as TextBox;
                        if (tbControl != null)
                            tbControl.IsTabStop = false;
                        else
                        {
                            var cbControl = child as ComboBox;
                            if (cbControl != null)
                                cbControl.IsTabStop = true;
                        }   
                    }
                }

                if (VisualTreeHelper.GetChildrenCount(child) > 0)
                    Apply(child);
            }
        }
    }
}
