﻿using System.Windows;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
    /// <summary>
    /// Interaction logic for ReturnDepositView.xaml
    /// </summary>
    public partial class ReturnDepositView
    {
        public ReturnDepositView()
        {
            InitializeComponent();
            ModelInteractionHelper.SubscribeForControlTabStops(this, Content);
        }
    }
}
