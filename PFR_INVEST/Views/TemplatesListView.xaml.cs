﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Helpers;

namespace PFR_INVEST.Views
{
	/// <summary>
	/// Interaction logic for TemplatesView.xaml
	/// </summary>
	public partial class TemplatesListView
	{
		public TemplatesListView()
		{
			InitializeComponent();
			tblDoc.MouseDoubleClick += tblDoc_MouseDoubleClick;
			// tblDoc.FocusedRowChanged += tblDoc_FocusedRowChanged;
			DocGrid.CurrentItemChanged += Grid_CurrentItemChanged;
			Loaded += TemplatesView_Loaded;
			Unloaded += TemplatesView_Unloaded;
			UpdateCommandParameters(null);
            ModelInteractionHelper.SubscribeForGridRefreshHelper(this, DocGrid);
        }

	    private void TemplatesView_Unloaded(object sender, RoutedEventArgs e)
		{
	        if (DataContext == null) return;
	        CommandBindings.Remove(((TemplatesListViewModel)DataContext).DeleteCommandBinding);
	        CommandBindings.Remove(((TemplatesListViewModel)DataContext).OpenCommandBinding);
		}

	    private void Grid_CurrentItemChanged(object sender, DevExpress.Xpf.Grid.CurrentItemChangedEventArgs e)
		{
			var item = e.NewItem as TemplateListItem;
			UpdateCommandParameters(item);
		}

	    private void TemplatesView_Loaded(object sender, RoutedEventArgs e)
		{
			var model = DataContext as TemplatesListViewModel;
			if (model != null)
			{
				CommandBindings.Add(model.DeleteCommandBinding);
				CommandBindings.Add(model.OpenCommandBinding);
			}
		}

	    private void tblDoc_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (tblDoc.GetRowElementByMouseEventArgs(e) == null) return;

			if (tblDoc.FocusedRowHandle >= 0)
			{
				object id = DocGrid.GetCellValue(tblDoc.FocusedRowHandle, "ID");
				btnOpen.Command.Execute(id);
			}
		}

		private void UpdateCommandParameters(TemplateListItem item)
		{
			bool haveItems = DocGrid.VisibleRowCount > 0;

			if (item != null)
			{
				btnDelete.CommandParameter = new object[] { new List<long> { item.ID }, haveItems };
				btnOpen.CommandParameter = item.ID;

			    if (DocGrid.SelectedItems.Count <= 1)
                    return;
			    var ids = DocGrid.SelectedItems.Cast<TemplateListItem>().Select(t => t.ID).ToList();
			    btnDelete.CommandParameter = new object[] { ids, haveItems };
			}
			else
			{
				btnDelete.CommandParameter = null;
				btnOpen.CommandParameter = null;
			}
		}
	}
}
