﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using DevExpress.Xpf.Editors.Helpers;

namespace PFR_INVEST.Converters
{
    public class DecimalToPercentNonRoundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object
           parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                decimal step = (decimal)Math.Pow(10, 2);
                int tmp = (int)Math.Truncate(step * value.TryConvertToDecimal());

                if (tmp / step == 0)//При значениях менее 0,01 вывести 0,00
                    return "0,00 %";

                var res = $"{tmp / step}";
                var parts = res.Split(',');

                if (parts.Length == 1)//Если число целое то добавить 2 нуля справа
                    return $"{res},00 %";

                if (parts[1].Length < 2)//Если в десятичной части одна цифра то дополнить нулем справа
                    res = res.PadRight(res.Length + 1, '0');
                return $"{res} %";
            }
            catch (Exception)
            {
                return value;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
