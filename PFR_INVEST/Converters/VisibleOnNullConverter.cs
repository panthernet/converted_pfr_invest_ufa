﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;

namespace PFR_INVEST.Converters
{
    public class VisibleOnNullConverter : MarkupExtension, IValueConverter
    {

        public VisibleOnNullConverter() { }
        public VisibleOnNullConverter(bool invert)
            : this()
        {
            this.Invert = invert;
        }
        [ConstructorArgument("invert")]
        public bool Invert { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!Invert)
                return value == null ? Visibility.Visible : Visibility.Collapsed;
            else
                return value == null ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
