﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using PFR_INVEST.BusinessLogic.Helper;

namespace PFR_INVEST.Helpers
{
    [ValueConversion(typeof(int), typeof(string))]
    public class IntToQuarkConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return QuarkDisplayHelper.GetQuarkDisplayValue((int)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
