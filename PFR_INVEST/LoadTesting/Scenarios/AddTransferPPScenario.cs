﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddTransferPPScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Перечисление средств - Перечисления УК - Ввод данных платежного поручения"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new UKTransfersListViewModel(false);

			var item = modelList.List.Skip(rnd.Next(modelList.List.Count - 1)).First();

            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new EnterTransferPPDataViewModel(ViewModelState.Edit, item);

            RaiseStepChanged(0.3, "Заполнение формы");
            model.PPNumber = rnd.Next(1000).ToString();
            var dialog = new PortfoliosListViewModel();
            var portfolios = dialog.PortfolioList;
            model.Account = portfolios.Skip(rnd.Next(portfolios.Count - 1)).First();
            model.Profit = 0.01M * rnd.Next(10000);

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);

            base.RaiseScenarioFinished();
        }
    }
}
