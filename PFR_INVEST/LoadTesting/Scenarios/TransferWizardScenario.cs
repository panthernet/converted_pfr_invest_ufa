﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    using PFR_INVEST.DataObjects;

    public class TransferWizardScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Мастер перечислений"; }
        }

        public override void Run()
        {
            try
            {
                Login();
                var rnd = new Random();

                RaiseFinishedPercentChanged(0.1);

                RaiseStepChanged(0.1, "Загрузка модели");
                var model = new TransferWizardViewModel(Document.Types.SI,false);

                RaiseStepChanged(new StepChangedEventArgs("Выбор случайного направления перечисления"));
                model.SelectedTransferDirection = model.TransferDirections[rnd.Next(model.TransferDirections.Count)];
                RaiseStepChanged(new StepChangedEventArgs(string.Format("Выбранное направление: {0}", model.SelectedTransferDirection.Name)));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.2));

                RaiseStepChanged(new StepChangedEventArgs("Выбор случайного типа перечисления"));
                model.SelectedTransferType = model.CurrentTransferTypes[rnd.Next(model.CurrentTransferTypes.Count)];
                RaiseStepChanged(new StepChangedEventArgs(string.Format("Выбранный тип: {0}", model.SelectedTransferType.Name)));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.4));

                RaiseStepChanged(new StepChangedEventArgs("Выбор случайного года перечислений"));
                model.SelectedYear = model.YearsList[rnd.Next(model.YearsList.Count)];
                RaiseStepChanged(new StepChangedEventArgs(string.Format("Выборан {0} год", model.SelectedYear.Name)));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.6));

                if (model.SelectedTransferType.ShowMonth ?? false)
                {
                    RaiseStepChanged(new StepChangedEventArgs("Выбор случайного месяца перечислений"));
                    model.SelectedMonth = model.MonthsList[rnd.Next(model.MonthsList.Count)];
                    RaiseStepChanged(new StepChangedEventArgs(string.Format("Выборан месяц {0}", model.SelectedMonth.Name)));
                }
                model.Comments = "Создано при нагрузочном тестировании";
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));

                RaiseStepChanged(new StepChangedEventArgs("Загрузка доступных договоров"));
                model.EnsureAvailableList();
                RaiseStepChanged(new StepChangedEventArgs(string.Format("Загружено {0} договоров", model.AvailableContractsList.Count)));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.8));

                RaiseStepChanged(new StepChangedEventArgs("Имитация выбора договоров для создания перечислений"));
                model.SelectedContractsList = new List<TransferWizardViewModel.ModelContractRecord>();
                model.AvailableContractsList.ForEach(
                    contract =>
                    {
                        if (rnd.Next(2) == 1)
                            model.SelectedContractsList.Add(contract);
                    }
                );
                RaiseStepChanged(new StepChangedEventArgs(string.Format("Выбрано {0} договоров", model.SelectedContractsList.Count)));

                RaiseStepChanged(new StepChangedEventArgs("Имитация заполнения списка договоров суммами"));
                model.SelectedContractsList.ForEach(
                    contract =>
                    {
                        if (model.SelectedTransferType.ShowInsuredPersonsCount ?? false)
                            contract.QuantityZL = rnd.Next(1000);

                        if (model.SelectedTransferDirection.isFromUKToPFR ?? false)
                            contract.InvestIncome = rnd.Next(100000);

                        contract.Sum = rnd.Next(50000000);
                    }
                );
                RaiseStepChanged(new StepChangedEventArgs("Заполнено"));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));

                RaiseStepChanged(new StepChangedEventArgs("Сохранение реестра"));
                model.CreateRegisterEx();
                RaiseStepChanged(new StepChangedEventArgs("Сохранено"));
                RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1));

            }
            catch (Exception ex) { RaiseException(ex); }
            finally { try { RaiseScenarioFinished(new EventArgs()); } catch { } }
        }
    }
}
