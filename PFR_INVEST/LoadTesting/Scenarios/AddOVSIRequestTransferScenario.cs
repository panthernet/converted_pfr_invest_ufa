﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddOVSIRequestTransferScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Перечисления - Добавить Перечисление"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new TransfersSIListViewModel();

            var list = modelList.List;
            var item = list.Skip(rnd.Next(list.Count - 1)).First();

            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new ReqTransferViewModel(item.TransferID, ViewModelState.Create) { State = ViewModelState.Edit };

            RaiseStepChanged(0.3, "Заполнение формы");
            model.TransferSum = 12;

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);

            base.RaiseScenarioFinished();
        }
    }
}
