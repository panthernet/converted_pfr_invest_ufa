﻿using System;

namespace PFR_INVEST.LoadTesting.Scenarios
{
	public class LoginScenario : ScenarioBase
	{        
		public override string ScenarioName
		{
			get { return "Подключение"; }
		}

		public override void Run()
		{
			Login();
			RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1));
			RaiseScenarioFinished(new EventArgs());
		}
	}
}
