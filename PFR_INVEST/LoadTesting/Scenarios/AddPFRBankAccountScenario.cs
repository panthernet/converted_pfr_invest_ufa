﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddPFRBankAccountScenario : ScenarioBase
    {

        public override string ScenarioName
        {
            get { return "Справочники - Добавление банковского счета"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new PFRAccountsListViewModel();


            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.4));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели банк. счета"));
            var model = new PFRAccountViewModel(0) { State = ViewModelState.Create };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.6));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение доп. соглашения"));

            model.SelectedLegalEntity = model.LegalEntitiesList[rnd.Next(0, model.LegalEntitiesList.Count - 1)];
            model.SelectedType = model.TypesList[rnd.Next(0, model.TypesList.Count - 1)];
            model.SelectedCurrency = model.CurrencyList[rnd.Next(0, model.CurrencyList.Count - 1)];
            model.BA.AccountNumber = "12345678901234567890";
            model.SelectedStatus = "Активный";

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.8));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение банк.счета"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Проверка на сохранение не пройдена!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списков"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
