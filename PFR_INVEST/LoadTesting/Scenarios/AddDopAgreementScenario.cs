﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddDopAgreementScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Справочники - Добавление доп.соглашения"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new SIContractsListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Выбор случайного договора"));
            //TODO: отсев договоров
            var contracts = lmodel.SIContractsList.FindAll(a => a.SupAgreementDate == null); ;
            var contract = contracts[rnd.Next(0, contracts.Count - 1)];


            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.4));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели договора"));
            var cmodel = new SIContractViewModel(contract.ContractID, ViewModelState.Read);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Создание модели доп. соглашения"));
            var model = new SupAgreementViewModel(0, ViewModelState.Create) { IsDataChanged = true };

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.6));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение доп. соглашения"));

            model.SelectedSupAgreementKind = model.SupAgreementKinds[rnd.Next(0, model.SupAgreementKinds.Count - 1)];
            model.SupAgreementDate = DateTime.Now;
            model.SupAgreementSuspendDate = DateTime.Now.AddDays(1);
            model.SupAgreementExtention = 10;

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.8));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение доп. соглашения"));
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else throw new Exception("Проверка на сохранение не пройдена!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списков"));
            lmodel.RefreshList.Execute(null);
            cmodel.RefreshSupAgreements();

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
