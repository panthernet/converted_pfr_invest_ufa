﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddOVSITransferScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисления средств - Перечисления - Добавить список перечеслений"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new TransfersSIListViewModel();

            var list = modelList.List;
            var item = list.Skip(rnd.Next(list.Count - 1)).First();

            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new TransferViewModel(item.RegisterID, ViewModelState.Create) { State = ViewModelState.Create };

            RaiseStepChanged(0.3, "Заполнение формы");
            model.InsuredPersonsCount = 12;

            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);


            base.RaiseScenarioFinished();
        }
    }
}
