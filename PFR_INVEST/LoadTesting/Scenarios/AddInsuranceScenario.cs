﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddInsuranceScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Страхование - Добавить договор страхования"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();
            base.RaiseStepChanged(0.1, "Загрузка модели списка");
            var modelList = new InsuranceSIListViewModel();



            RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new InsuranceSIViewModel(ViewModelState.Create);

            RaiseStepChanged(0.3, "Заполнение формы");
            model.SelectedUK = model.UKList.RandomItem(rnd);
            model.SelectedContract = model.Contracts.RandomItem(rnd);
            model.Sum = 0.01M * rnd.Next(1000);
            model.Insurance = "Тестовой";
            model.ReqNum = "Тестовой " + rnd.Next(1000).ToString();
            model.ReqDate = DateTime.Now;
            model.CloseDate = DateTime.Now.AddDays(rnd.Next(1000));


            RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            RaiseStepChanged(0.9, "Обновление списка");
            modelList.RefreshList.Execute(null);


            base.RaiseFinishedPercentChanged(1.0);
        }
    }
}
