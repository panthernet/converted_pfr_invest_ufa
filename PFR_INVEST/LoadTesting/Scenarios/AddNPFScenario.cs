﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddNPFScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Справочники - Добавление НПФ"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            //RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            //RaiseStepChanged(new StepChangedEventArgs("Загрузка модели"));
            //RaiseScenarioFinished(new EventArgs());

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.1));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели списка"));
            var lmodel = new NPFListViewModel();

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.3));
            RaiseStepChanged(new StepChangedEventArgs("Загрузка модели формы"));
            var model = new NPFViewModel(ViewModelState.Create, 0);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.5));
            RaiseStepChanged(new StepChangedEventArgs("Заполнение формы"));
            model.LegalEntity.FullName = "Test model for NPF";
            model.LegalEntity.FormalizedName = "Test model";
            model.LegalEntity.ShortName = "Test";
            model.LegalAddress = "Test legal adress";
            model.RealAddress = "Test real adress";
            model.PostalAddress = "Test postal adress";
            model.Phone = "1234567";
            model.Fax = "1234567";
            model.EMail = "a@a.ru";
            model.LicNumber = "1234567890";
            model.LicRegistrator = "Test license registartor";
            model.HeadFirstName = "name";
            model.HeadLastName= "Test";
            model.HeadPosition = "Test position";
            model.HeadAccordingTo = "Order No25 on25/03/1993";
            model.HeadInaugurationDate = new DateTime(1993, 3, 25);
            model.CurrBankAccLegalEntityName = "Test name";
            model.INN = "1234567890";
            model.OKPP = "123456789";
            model.CurrBankAccAccountNumber = "12345678901234567890";
            model.CurrBankAccBankName = "Test bank name";
            model.CurrBankAccBankLocation = "Test location";
            model.CurrBankAccCorrespondentAccountNumber = "12345678901234567890";
            model.CurrBankAccBIK = "123456789";
            model.CurrBankAccINN = "1234567890";
            model.CurrBankAccKPP = "123456789";

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.7));
            RaiseStepChanged(new StepChangedEventArgs("Сохранение формы"));
            model.SaveCard.Execute(null);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(0.9));
            RaiseStepChanged(new StepChangedEventArgs("Обновление списка"));
            lmodel.RefreshList.Execute(null);

            //RaiseStepChanged(new StepChangedEventArgs("Удаление формы"));
            //model.ExecuteDeleteTest(DocOperation.Archive);

            RaiseFinishedPercentChanged(new FinishedPercentChangedEventArgs(1.0));
        }
    }
}
