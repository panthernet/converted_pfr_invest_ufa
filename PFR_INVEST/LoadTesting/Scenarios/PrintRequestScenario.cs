﻿using System;
using System.Linq;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using System.Windows.Documents;
using PFR_INVEST.BusinessLogic.ViewModelsList;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class PrintRequestScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Работа с СИ - Перечисление средств - Перечисления - Сформировать требование"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();
            base.RaiseFinishedPercentChanged(0.1);
            base.RaiseStepChanged("Загрузка модели списка");
            var model = new TransfersSIListViewModel();

            var list = model.List.Where(t => TransferDirectionIdentifier.IsFromUKToPFR(t.Direction)
                                                    && TransferStatusIdentifier.IsStatusInitialState(t.Status)).ToList();

            var item = list.RandomItem(rnd);


            base.RaiseStepChanged(0.5, "Сформировать требование");
            var recordID = item.ReqTransferID;
            var card = new ReqTransferViewModel(recordID, ViewModelState.Edit);
            PrintRequest PrintRequest = new PrintRequest(card.Operation, (new AddressType[]{ AddressType.Post,   AddressType.Fact}).ToList().RandomItem(rnd), false, 2016, 1);
            if (PrintRequest.Print(card.Transfer, card.Contract))
            {
                card.TransferStatus = TransferStatusIdentifier.sDemandGenerated;
                card.RequestCreationDate = DateTime.Now;
                DataContainerFacade.Save<ReqTransfer, long>(card.Transfer);
            }

            base.RaiseFinishedPercentChanged(1);

        }
    }
}
