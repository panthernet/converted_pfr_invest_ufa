﻿using System;
using PFR_INVEST.BusinessLogic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class AddTransferScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Бэк-офис - Счета - Добавить перечисление на счет"; }
        }

        public override void Run()
        {
            base.Login();
            var rnd = new Random();

			//base.RaiseStepChanged("Загрузка модели списка");
			//var modelList = new ACCListViewModel();

            base.RaiseStepChanged(0.2, "Загрузка модели формы");
            var model = new TransferToAccountViewModel() { State = ViewModelState.Create };

            base.RaiseStepChanged(0.5, "Заполнение формы");
            model.Account = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
			model.DestAccount = (new PortfoliosListViewModel()).PortfolioList.RandomItem(rnd);
            //model.SelectedCurrencyID = model.CurrencyList.RandomItem(rnd).ID;
            model.Summ = 0.01M * rnd.Next(10000);

            model.Comment = "Тестовой " + rnd.Next(1000);			

            base.RaiseStepChanged(0.7, "Сохранение формы");
            if (model.CanExecuteSave())
                model.SaveCard.Execute(null);
            else
                throw new Exception("Ошибка при проверке параметров для сохранения!");

            base.RaiseFinishedPercentChanged(1);
        }
    }
}
