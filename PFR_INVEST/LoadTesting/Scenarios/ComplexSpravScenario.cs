﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ComplexSpravScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Комплексный - Справочники"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            var scenarios = new List<ScenarioBase>();
            scenarios.Add(new AddBankScenario());
            scenarios.Add(new AddCBPaymentScenario());
            scenarios.Add(new AddCBScenario());
            scenarios.Add(new AddContractScenario());
            scenarios.Add(new AddDopAgreementScenario());
            scenarios.Add(new AddNPFScenario());
            scenarios.Add(new AddPFRBankAccountScenario());
            scenarios.Add(new AddSIScenario());

            RunInnerScenarioCycle(scenarios);
        }
    }
}
