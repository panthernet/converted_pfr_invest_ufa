﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.LoadTesting.Scenarios
{
    public class ComplexCBScenario : ScenarioBase
    {
        public override string ScenarioName
        {
            get { return "Комплексный - Работа с ЦБ"; }
        }

        public override void Run()
        {
            Login();
            var rnd = new Random();

            var scenarios = new List<ScenarioBase>();

            RunInnerScenarioCycle(scenarios);
        }
    }
}
