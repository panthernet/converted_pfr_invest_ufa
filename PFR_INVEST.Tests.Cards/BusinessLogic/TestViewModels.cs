﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.Cards.BusinessLogic
{
    [TestClass]
    public class TestViewModels : BaseTest
    {
        [TestMethod]
        public void TestBankViewModel()
        {
            //List<LegalEntity> lst = GetClient().Client.GetBanksListHib();
            var lst = GetClient().Client.GetBankListForDeposit();

            if (!lst.Any())
                Assert.Inconclusive();
            // PerformanceChecker chk = new PerformanceChecker();
            var firstID = lst.First(b => DataTools.IsEmail(b.EAddress) || string.IsNullOrEmpty(b.EAddress)).ID;
            var model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            model.TestFullLoad();
            //chk.CheckPerformance();

            if (!model.CanExecuteSave())
                Assert.Inconclusive();
            // Conatact Add
            // model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            Assert.IsFalse(model.CanExecuteSave());
            var contactName = $"Тестовой контакт {DateTime.Now.Ticks % 1000}";
            model.HeadsList.Add(new LegalEntityHead
            {
                FullName = contactName,
                Position = "Тестер",
                Phone = "123-456-789",
                Email = "www@www.www"
            });
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());
            // Contact Edit
            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.IsFalse(model.CanExecuteSave());
            var head = model.HeadsList.FirstOrDefault(h => h.FullName == contactName);
            Assert.IsTrue(model.HeadsList.Any());
            Assert.IsNotNull(head);
            Assert.AreEqual("Тестер", head.Position);
            Assert.AreEqual("123-456-789", head.Phone);
            Assert.AreEqual("www@www.www", head.Email);

            head.Position = "Тестер шаг 2";
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            Assert.IsTrue(model.EditHeadCommand.CanExecute(head));
            Assert.IsTrue(model.DeleteHeadCommand.CanExecute(head));

            // Contact delete
            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.IsFalse(model.CanExecuteSave());
            head = model.HeadsList.FirstOrDefault(h => h.Position == "Тестер шаг 2");
            Assert.IsNotNull(head);
            model.HeadsList.Remove(head);
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            // Rating Prepare
            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.IsFalse(model.CanExecuteSave());
            var savedRatings = model.RatingsList.ToArray();
            model.RatingsList.Clear();
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            //var Ratings = DataContainerFacade.GetList<Rating>();
            //if (Ratings.Count < 3)
            //    Assert.Inconclusive("Ratings");
            //var RatingAgencies = DataContainerFacade.GetList<RatingAgency>();
            //if (RatingAgencies.Count < 3)
            //    Assert.Inconclusive("RatingAgencies");
            //Ratings = Ratings.Take(3).ToList();
            //RatingAgencies = RatingAgencies.Take(3).ToList();
            // Rating Add
            //model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            var multRating = DataContainerFacade.GetList<MultiplierRating>();

            if (multRating.Count < 3)
                Assert.Inconclusive("MultiplierRating");

            multRating = multRating.Take(3).ToList();

            Assert.IsTrue(model.RatingsList.Count == 0);
            Assert.IsTrue(model.AddRatingCommand.CanExecute(null));
            foreach (var mr in multRating)
            {
                model.RatingsList.Add(new LegalEntityRating {MultiplierRatingID = mr.ID, Date = DateTime.Now});
            }

            //Assert.IsFalse(model.AddRatingCommand.CanExecute(null));
            //Assert.IsTrue(model.AddRatingCommand.CanExecute(null));
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            //// Rating Edit
            ////model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            //Assert.IsFalse(model.CanExecuteSave());
            //var rating = model.RatingsList.FirstOrDefault(h => h.AgencyID == RatingAgencies[0].ID);
            //Assert.IsNotNull(rating);
            //rating.Multiplier = 0.5M;
            //Assert.IsTrue(model.CanExecuteSave());
            //model.SaveCard.Execute(null);
            //Assert.IsFalse(model.CanExecuteSave());


            //// Rating delete
            ////model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            //Assert.IsFalse(model.CanExecuteSave());
            //rating = model.RatingsList.FirstOrDefault(h => h.AgencyID == RatingAgencies[2].ID);
            //Assert.IsNotNull(rating);
            //model.RatingsList.Remove(rating);
            //Assert.IsTrue(model.CanExecuteSave());
            //model.RatingsList.Clear();
            //model.SaveCard.Execute(null);
            //Assert.IsFalse(model.CanExecuteSave());




            //// Rating limit calculation
            ////model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            //model.RatingsList.Clear();
            //foreach (var ra in RatingAgencies)
            //{
            //    model.RatingsList.Add(new LegalEntityRating() { AgencyID = ra.ID, RatingID = Ratings[0].ID, Multiplier = 0.2M, Date = DateTime.Now });
            //}
            //Assert.AreEqual<decimal>(model.Limit4Money, Math.Round((model.Bank.Money ?? 0M) * 0.2M / 1000000M, 0));
            //model.RatingsList.First().Multiplier = 0.5M;
            //Assert.AreEqual<decimal>(model.Limit4Money, Math.Round((model.Bank.Money ?? 0M) * 0.5M / 1000000M, 0));

            //Assert.AreEqual<decimal>(model.Limit4Requests, (model.Limit4Money - model.DepositList.Sum(d => d.PendingVolume) / 1000000M) ?? 0M);

            //model.SaveCard.Execute(null);
            //Assert.IsFalse(model.CanExecuteSave());

            //Stock code field

            // model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            var savedCode = model.Bank.StockCode;
            Assert.IsFalse(model.CanExecuteSave());
            model.Bank.StockCode = "TST_12";
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.AreEqual("TST_12", model.Bank.StockCode);
            model.Bank.StockCode = savedCode;
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            var last = model.OwnCapitalDate;
            var capital = model.OwnCapital;

            var newDate =
                new DateTime?(last?.AddDays(1000) ?? DateTime.Today.AddDays(1000));
            decimal? newCapital = capital + 1 ?? 100;

            var latest = new OwnCapitalHistory
            {
                ImportDate = DateTime.Today,
                ReportDate = newDate.Value,
                IsImported = 0,
                LegalEntityID = model.Bank.ID,
                LegalEntityName = model.Bank.FormalizedName,
                Summ = newCapital.Value
            };

            var capId = DataContainerFacade.Save(latest);

            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.AreEqual(model.OwnCapital, newCapital);
            Assert.AreEqual(model.OwnCapitalDate, newDate);

            DataContainerFacade.Delete<OwnCapitalHistory>(capId);
            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.AreNotEqual(model.OwnCapital, newCapital);
            Assert.AreNotEqual(model.OwnCapitalDate, newDate);

            //License number
            //model = new BankViewModel(firstID) { State = ViewModelState.Edit };
            var LicNum = Random.Next(10000).ToString().PadRight(10, '0');
            while (
                !GetClient().Client.CheckLicenseUnique(model.Bank.ID, LicNum, model.Contragent.TypeName).IsSuccess)
            {
                LicNum = Random.Next(10000).ToString().PadRight(10, '0');
            }

            var LicDate = DateTime.Now.Date;

            model.RegistrationNum = Convert.ToInt64(LicNum);
            model.RegistrationDate = LicDate;

            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            model = new BankViewModel(firstID) {State = ViewModelState.Edit};
            Assert.AreEqual(LicNum, model.License.Number.Trim());
            Assert.AreEqual(LicNum, model.Bank.RegistrationNum.Trim());
            Assert.AreEqual(LicDate, model.License.RegistrationDate);
            Assert.AreEqual(LicDate, model.Bank.RegistrationDate);
        }

        [TestMethod]
        public void TestStockCodeLinkSave()
        {
            //List<LegalEntity> lst = GetClient().Client.GetBanksListHib();
            var lst = GetClient().Client.GetBankListForDeposit();

            if (!lst.Any())
                Assert.Inconclusive();
            // PerformanceChecker chk = new PerformanceChecker();
            var firstID = lst.First(b => DataTools.IsEmail(b.EAddress) || string.IsNullOrEmpty(b.EAddress)).ID;
            var model = new BankViewModel(firstID) {State = ViewModelState.Edit};

            if (model.StockList == null || !model.StockList.Any())
                Assert.Inconclusive();

            // delete existed links
            if (model.StockCodeList.Any())
            {
                model.StockCodeList.Clear();
            }

            var stockId = model.StockList.First().ID;

            const string stockCode = "Тестовый код биржи";

            var bankStockCode = new BankStockCode
            {
                BankID = firstID,
                StockID = stockId,
                StockCode = stockCode
            };

            model.StockCodeList.Add(bankStockCode);

            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());
            var linkedModel = new BankViewModel(firstID) {State = ViewModelState.Edit};

            Assert.IsTrue(linkedModel.StockCodeList != null && linkedModel.StockCodeList.Count == 1);
            Assert.AreEqual(linkedModel.StockCodeList.First().StockID, stockId);
            Assert.AreEqual(linkedModel.StockCodeList.First().BankID, firstID);
            Assert.AreEqual(linkedModel.StockCodeList.First().StockCode, stockCode);

            model.StockCodeList.Clear();

            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());
            linkedModel = new BankViewModel(firstID) {State = ViewModelState.Edit};

            Assert.IsTrue(linkedModel.StockCodeList != null && linkedModel.StockCodeList.Count == 0);
        }

        [TestMethod]
        public void TestBankLicenseUnique()
        {
            var lst = GetClient().Client.GetBankListForDeposit();
            if (!lst.Any() || lst.Count < 2)
                Assert.Inconclusive();
            var filtered = lst.Where(b => DataTools.IsEmail(b.EAddress) || string.IsNullOrEmpty(b.EAddress));
            var firstID = filtered.First().ID;
            var secondID = filtered.Skip(1).First().ID;

            var modelF = new BankViewModel(firstID) {State = ViewModelState.Edit};
            var modelS = new BankViewModel(secondID) {State = ViewModelState.Edit};

            if (!modelF.CanExecuteSave())
                Assert.Inconclusive();
            if (!modelS.CanExecuteSave())
                Assert.Inconclusive();


            modelF.TestFullLoad();
            modelS.TestFullLoad();

            // 999 => 999/9999
            var LicNum = "999";
            while (
                !GetClient().Client.CheckLicenseUnique(modelF.Bank.ID, LicNum, modelF.Contragent.TypeName).IsSuccess)
            {
                LicNum = Random.Next(100000000).ToString().PadRight(10, '0');
            }
            modelF.RegistrationNum = Convert.ToInt64(LicNum);
            Assert.IsTrue(modelF.CanExecuteSave());
            modelF.SaveCard.Execute(null);

            modelS.RegistrationNum = Convert.ToInt64(LicNum);
            Assert.IsFalse(modelS.CanExecuteSave());
            //modelS.RegistrationNum =  Convert.ToInt64(LicNum + "/" + Random.Next(10000).ToString());
            //Assert.IsFalse(modelS.CanExecuteSave());

            // 999/9999 => 999

            //LicNum = "999/8888";
            //while (!GetClient().Client.CheckLicenseUnique(modelF.Bank.ID, LicNum).IsSuccess)
            //{
            //    LicNum = Random.Next(100000000).ToString().PadRight(10, '0') + "/" + Random.Next(10000).ToString();
            //}
            //modelF.RegistrationNum = LicNum;
            //Assert.IsTrue(modelF.CanExecuteSave());
            //modelF.SaveCard.Execute(null);

            //modelS.RegistrationNum = LicNum;
            //Assert.IsFalse(modelS.CanExecuteSave());
            //modelS.RegistrationNum = LicNum.Split('/').First();
            //Assert.IsFalse(modelS.CanExecuteSave());

            //Restore normal number
            LicNum = "999";
            while (
                !GetClient().Client.CheckLicenseUnique(modelF.Bank.ID, LicNum, modelF.Contragent.TypeName).IsSuccess)
            {
                LicNum = Random.Next(100000000).ToString().PadRight(10, '0');
            }
            modelF.RegistrationNum = Convert.ToInt64(LicNum);
            Assert.IsTrue(modelF.CanExecuteSave());
            modelF.SaveCard.Execute(null);
        }


        [TestMethod]
        public void TestNPFViewModel()
        {
            var model = new NPFViewModel(ViewModelState.Create, 0);
            Assert.IsFalse(model.CanExecuteSave());

            model.FullName = "MegaBank";
            model.ShortName = "MB";
            model.FormalizedName = "MB Inc";
            model.LegalAddress = "Legal Address";
            model.PostalAddress = "Postal Address";
            model.RealAddress = "Real Address";
            model.Phone = "123456789";
            model.Fax = "123456789";
            model.EMail = "mb@mb.ru";
            model.GarantACBID = (long) LegalEntity.GarantACB.NotIncluded;

            var licNum = (DateTime.Now.Ticks%10000).ToString().PadRight(10, '0');
            var counter = 0;
            while (
                !GetClient().Client.CheckLicenseUnique(model.LegalEntity.ID, licNum, model.Contragent.TypeName)
                    .IsSuccess)
            {
                licNum = Random.Next(10000).ToString().PadRight(10, '0');
                counter++;
                if (counter == 100)
                    Assert.Fail("Unique License generation Fail");
            }
            model.LicNumber = licNum;
            model.LicRegDate = new DateTime(2003, 03, 03);
            model.LicRegistrator = "Register";

            model.INN = "1234567890";
            model.OKPP = "123456789";
            model.CurrBankAccLegalEntityName = "MegaBank";
            model.CurrBankAccAccountNumber = "12345678901234567890";
            model.CurrBankAccBankName = "MegaBank";
            model.CurrBankAccBankLocation = "somewhere";
            model.CurrBankAccCorrespondentAccountNumber = "12345678901234567890";
            model.CurrBankAccBIK = "123456789";
            model.CurrBankAccINN = "1234567890";
            model.CurrBankAccKPP = "123456789";


            //// Chief Add

            Assert.IsFalse(model.CanExecuteSave());

            model.HeadFirstName = "Иван";
            model.HeadLastName = "Иванов";
            model.HeadPosition = "Director";
            model.HeadAccordingTo = "Order No 192 from 2013/02/05";
            model.HeadInaugurationDate = new DateTime(2013, 2, 5);

            Assert.IsTrue(model.CanExecuteSave());

            model.SaveCard.Execute(null);
            var firstID = model.ID;

            Assert.IsFalse(model.CanExecuteSave());


            var xModel = model;

            // Chief Edit
            model = new NPFViewModel(ViewModelState.Edit, firstID);
            Assert.AreEqual(model.HeadFirstName, xModel.HeadFirstName);
            Assert.AreEqual(model.HeadLastName, xModel.HeadLastName);
            Assert.AreEqual(model.HeadPosition, xModel.HeadPosition);
            Assert.AreEqual(model.HeadAccordingTo, xModel.HeadAccordingTo);
            Assert.AreEqual(model.HeadInaugurationDate, xModel.HeadInaugurationDate);

            model.HeadPosition = "Тестер шаг 2";
            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());

            // Chief Retire
            model = new NPFViewModel(ViewModelState.Edit, firstID);

            model.DoRetireChief(new DateTime(2013, 5, 5));
            Assert.IsFalse(model.CanExecuteSave());

            model.HeadFirstName = "Петр";
            model.HeadLastName = "петров";
            model.HeadPosition = "Director";
            model.HeadAccordingTo = "Order No 192 from 2013/05/05";
            model.HeadInaugurationDate = new DateTime(2013, 5, 5);

            Assert.IsTrue(model.CanExecuteSave());
            model.SaveCard.Execute(null);

            model.DeleteFromDataBase();
        }


        /// <summary>
        /// Таск 28-1
        /// </summary>
        [TestMethod]
        public void TestDepClaimOfferViewModel_NotSigned()
        {
            var offers = DataContainerFacade.GetList<DepClaimOffer>();
            var filtered = offers.Where(o => o.Status == DepClaimOffer.Statuses.NotSigned).ToList();

            if (!filtered.Any())
                Assert.Inconclusive("В базе нет неподписаных оферт для теста");

            var model = new DepClaimOfferViewModel(filtered.First().ID, ViewModelState.Edit);
            Assert.AreEqual(2, model.AllowedStatuses.Count);
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.NotSigned));
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.Signed));

        }

        /// <summary>
        /// Таск 28-2
        /// </summary>
        [TestMethod]
        public void TestDepClaimOfferViewModel_Signed()
        {
            var offers = DataContainerFacade.GetList<DepClaimOffer>();
            var filtered = offers.Where(o => o.Status == DepClaimOffer.Statuses.Signed).ToList();

            if (!filtered.Any())
                Assert.Inconclusive("В базе нет подписаных оферт для теста");

            var model = new DepClaimOfferViewModel(filtered.First().ID, ViewModelState.Edit);
            Assert.AreEqual(3, model.AllowedStatuses.Count);
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.Signed));
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.Accepted));
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.NotAccepted));
        }

        /// <summary>
        /// Таск 28-3
        /// </summary>
        [TestMethod]
        public void TestDepClaimOfferViewModel_Accepted()
        {
            var offers = DataContainerFacade.GetList<DepClaimOffer>();
            var filtered = offers.Where(o => o.Status == DepClaimOffer.Statuses.Accepted).ToList();

            if (!filtered.Any())
                Assert.Inconclusive("В базе нет акцептированых оферт для теста");

            var model = new DepClaimOfferViewModel(filtered.First().ID, ViewModelState.Edit);
            //there may be more than 1 available status for offer
            Assert.IsTrue(model.AllowedStatuses.Count >= 1);
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.Accepted));
        }

        /// <summary>
        /// Таск 28-4
        /// </summary>
        [TestMethod]
        public void TestDepClaimOfferViewModel_NotAccepted()
        {
            var offers = DataContainerFacade.GetList<DepClaimOffer>();
            var filtered = offers.Where(o => o.Status == DepClaimOffer.Statuses.NotAccepted).ToList();

            if (!filtered.Any())
                Assert.Inconclusive("В базе нет неакцептированых оферт для теста");

            var model = new DepClaimOfferViewModel(filtered.First().ID, ViewModelState.Edit);
            Assert.IsTrue(model.AllowedStatuses.Count >= 1);
            Assert.IsTrue(model.AllowedStatuses.Contains(DepClaimOffer.Statuses.NotAccepted));
        }

        [TestMethod]
        public void TestLegalEntityCourierViewModel()
        {
            //StatusFilter sf = new StatusFilter() 
            //    { 
            //        new filterOP(){ bop = BOP.and, op = OP.notequal, IDENTIFIER = StatusIdentifier.s_Deleted}
            //    };

            var lst = GetClient().Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter).ToList();

            if (!lst.Any())
                Assert.Inconclusive();
            // PerformanceChecker chk = new PerformanceChecker();
            var firstID = lst.First().ID;

            var model = new NPFViewModel(ViewModelState.Edit, firstID);

            var c = new LegalEntityCourier
            {
                FirstName = "ИмяКурьера",
                LastName = "ФамилияКурьера",
                LetterOfAttorneyNumber = "12345",
                LetterOfAttorneyIssueDate = new DateTime(2013, 1, 1),
                LetterOfAttorneyExpireDate = new DateTime(2013, 1, 2),
                Type = LegalEntityCourier.Types.Courier
            };


            model.CourierList.Add(c);
            model.CourierListChanged.Add(c);

            model.SaveCard.Execute(null);


            model = new NPFViewModel(ViewModelState.Edit, firstID);


            var c2 = model.CourierList.FirstOrDefault(x => x.FirstName == "ИмяКурьера");
            Assert.IsNotNull(c2);

            if (c2.LetterOfAttorneyListChanged == null)
                c2.LetterOfAttorneyListChanged = new List<LegalEntityCourierLetterOfAttorney>();

            c2.LetterOfAttorneyListChanged.Add(new LegalEntityCourierLetterOfAttorney
            {
                Number = "12Zf",
                IssueDate = new DateTime(2012, 1, 1),
                ExpireDate = new DateTime(2012, 1, 1)
            });

            model.CourierListChanged.Add(c2);

            model.SaveCard.Execute(null);


            model = new NPFViewModel(ViewModelState.Edit, firstID);
            var c3 = model.CourierList.FirstOrDefault(x => x.FirstName == "ИмяКурьера");
            Assert.IsNotNull(c3);

            var mc = new LegalEntityCourierDlgViewModel(c3);
            var la = mc.LetterOfAttorneyList.FirstOrDefault(x => x.Number == "12Zf");
            Assert.IsNotNull(la);

            mc.DeleteFromDatabase();
        }


        [TestMethod]
        public void TestNPFContacts()
        {
            //StatusFilter sf = new StatusFilter() 
            //    { 
            //        new filterOP(){ bop = BOP.and, op = OP.notequal, IDENTIFIER = StatusIdentifier.s_Deleted}
            //    };

            var lst = GetClient().Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter).ToList();

            if (!lst.Any())
                Assert.Inconclusive();
            // PerformanceChecker chk = new PerformanceChecker();
            var firstID = lst.First().ID;

            var model = new NPFViewModel(ViewModelState.Edit, firstID);
            //Assert.Inconclusive("IsDataChanged: {0}",model.IsDataChanged);
            Assert.IsFalse(model.SaveCard.CanExecute(null));
            model.IsDataChanged = true;
            if (!model.SaveCard.CanExecute(null))
                Assert.Inconclusive("Not valid NPF ID={0}", model.ID);


            //Add contacts
            model.ContactList.Add(new LegalEntityCourier
            {
                FirstName = "Тестер 1",
                LastName = "ТестерТестерТест",
                Position = "ТестерТестерТест2"
            });

            Assert.IsTrue(model.SaveCard.CanExecute(null));
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.SaveCard.CanExecute(null));


            model = new NPFViewModel(ViewModelState.Edit, firstID);
            Assert.IsFalse(model.SaveCard.CanExecute(null));
            var contact = model.ContactList.FirstOrDefault(c => c.FirstName == "Тестер 1");
            Assert.IsNotNull(contact);
            //Edit
            contact.FirstName = "Тестер 2";

            Assert.IsTrue(model.SaveCard.CanExecute(null));
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.SaveCard.CanExecute(null));

            model = new NPFViewModel(ViewModelState.Edit, firstID);
            Assert.IsFalse(model.SaveCard.CanExecute(null));
            contact = model.ContactList.FirstOrDefault(c => c.FirstName == "Тестер 2");
            Assert.IsNotNull(contact);

            //Delete
            model.ContactList.Remove(contact);
            Assert.IsTrue(model.SaveCard.CanExecute(null));
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.SaveCard.CanExecute(null));

            model = new NPFViewModel(ViewModelState.Edit, firstID);
            Assert.IsFalse(model.SaveCard.CanExecute(null));
            contact = model.ContactList.FirstOrDefault(c => c.FirstName == "Tester 2");
            Assert.IsNull(contact);

        }

        [TestMethod]
        public void TestCreateKBK()
        {
            const string testKbkCode = "12345678901234567890";
            const string testKbkNewCode = "12345678900987654321";
            const string testKbkName = "Тестовое наименование доходов";
            const string testKbkNewName = "Тестовое наименование доходов 2";

            // cleanup
            var kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkCode).ToList();
            //KBKList.ForEach(DataContainerFacade.Delete);
            kbkList.ForEach(x => GetClient().Client.Delete(typeof (KBK).FullName, x));
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkNewCode).ToList();
            //KBKList.ForEach(DataContainerFacade.Delete);
            kbkList.ForEach(x => GetClient().Client.Delete(typeof (KBK).FullName, x));

            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkCode, true).ToList();
            Assert.IsFalse(kbkList.Any());
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkNewCode, true).ToList();
            Assert.IsFalse(kbkList.Any());

            //Create test
            var model = new KBKViewModel(0, ViewModelState.Create);
            Assert.IsFalse(model.CanExecuteSave());
            Assert.IsFalse(model.CanExecuteDelete());
            model.KBK.Code = testKbkCode;
            model.KBK.Name = testKbkName;
            Assert.IsTrue(model.CanExecuteSave());
            Assert.IsFalse(model.CanExecuteDelete());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());
            Assert.IsTrue(model.CanExecuteDelete());
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkCode, true).ToList();
            Assert.AreEqual(1, kbkList.Count);
            var createdKBK = kbkList.First();
            Assert.AreEqual(testKbkCode, createdKBK.Code);
            Assert.AreEqual(testKbkName, createdKBK.Name);
            Assert.AreEqual(model.KBK.ID, createdKBK.ID);

            //Update test
            Assert.IsFalse(model.KBKHistoryList.Any());
            model.KBK.Code = testKbkNewCode;
            model.KBK.Name = testKbkNewName;
            Assert.IsTrue(model.CanExecuteSave());
            Assert.IsTrue(model.CanExecuteDelete());
            model.SaveCard.Execute(null);
            Assert.IsFalse(model.CanExecuteSave());
            Assert.IsTrue(model.CanExecuteDelete());
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkNewCode, true).ToList();
            Assert.AreEqual(1, kbkList.Count);
            var updatedKBK = kbkList.First();
            Assert.AreEqual(testKbkNewCode, updatedKBK.Code);
            Assert.AreEqual(testKbkNewName, updatedKBK.Name);
            Assert.AreEqual(1, model.KBKHistoryList.Count);
            var historyKBK = model.KBKHistoryList.First();
            Assert.AreEqual(testKbkCode, historyKBK.Code);
            Assert.AreEqual(testKbkName, historyKBK.Name);
            Assert.AreEqual(createdKBK.ID, updatedKBK.ID);
            Assert.AreEqual(model.KBK.ID, updatedKBK.ID);
            Assert.AreEqual(historyKBK.KBKId, updatedKBK.ID);

            //Delete test
            model.DeleteCard.Execute(null);
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkCode, true).ToList();
            Assert.IsFalse(kbkList.Any());
            kbkList = DataContainerFacade.GetListByProperty<KBK>("Code", testKbkNewCode, true).ToList();
            Assert.IsFalse(kbkList.Any());
        }

        [TestMethod]
        public void TestBankStockCodeDlgViewModel()
        {
            const string TestStockCode = "Тестовый биржевой код";
            var lst = GetClient().Client.GetBankListForDeposit();

            if (!lst.Any())
                Assert.Inconclusive();
            // PerformanceChecker chk = new PerformanceChecker();
            var bankIDs =
                lst.Where(b => DataTools.IsEmail(b.EAddress) || string.IsNullOrEmpty(b.EAddress))
                    .Select(x => x.ID)
                    .ToList();
            if (bankIDs.Count < 2)
                Assert.Inconclusive();

            var stockList = DataContainerFacade.GetList<Stock>();
            if (stockList.Count < 2)
                Assert.Inconclusive();

            var links = DataContainerFacade.GetListByProperty<BankStockCode>("BankID", bankIDs.First()).ToList();
            links.ForEach(DataContainerFacade.Delete);

            var testStoredStockLink = new BankStockCode
            {
                BankID = bankIDs.First(),
                StockCode = TestStockCode,
                StockID = stockList.First().ID
            };

            testStoredStockLink.ID = DataContainerFacade.Save(testStoredStockLink);

            var testStockLink = new BankStockCode
            {
                BankID = bankIDs[1],
                StockCode = TestStockCode,
                StockID = stockList[1].ID
            };

            var model = new BankStockCodeDlgViewModel(testStockLink, false, new List<long> {stockList.First().ID});

            Assert.IsTrue(model.StockList.Any());
            Assert.IsNull(model.StockList.FirstOrDefault(x => x.ID == testStoredStockLink.StockID));

            Assert.IsTrue(string.IsNullOrWhiteSpace(model.ValidateStockCodeUnique()));

            model.Value.StockCode = TestStockCode + " 2";

            Assert.IsTrue(string.IsNullOrEmpty(model.ValidateStockCodeUnique()));

            DataContainerFacade.Delete(testStoredStockLink);
        }
        /* Тестирование классов блока контроля сроков ввода информации и уведомлений*/

        
    }

}
