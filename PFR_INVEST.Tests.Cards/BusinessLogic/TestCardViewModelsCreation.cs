﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.SI;
using PFR_INVEST.BusinessLogic.ViewModelsWizards.VR;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Tests.Shared;

namespace PFR_INVEST.Tests.Cards.BusinessLogic
{
    [TestClass]
    public class TestCardViewModelsCreation : BaseTest
    {

        #region Рейтинги и агенства

        [TestMethod]
        public void TestAgencyRatingViewModel()
        {
            var rating = DataContainerFacade.GetListLimit<MultiplierRating>(1).FirstOrDefault();

            var model = rating == null ? new AgencyRatingViewModel(-1, ViewModelState.Create) : new AgencyRatingViewModel(rating.ID, ViewModelState.Edit);
            var chk = new PerformanceChecker();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestRatingViewModel()
        {
            var rating = DataContainerFacade.GetListLimit<Rating>(1).FirstOrDefault();

            var model = rating == null ? new RatingViewModel(-1, ViewModelState.Create) : new RatingViewModel(rating.ID, ViewModelState.Edit);
            var chk = new PerformanceChecker();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestRatingAgencyModel()
        {
            var agency = DataContainerFacade.GetListLimit<RatingAgency>(1).FirstOrDefault();
            var model = agency == null ? new RatingAgencyViewModel(-1) : new RatingAgencyViewModel(agency.ID);

            var chk = new PerformanceChecker();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        #region  Авансовый платеж и уточнения к авансовому платежу

        [TestMethod]
        public void TestPrepaymentAccurateViewModel()
        {
            var aps = DataContainerFacade.GetListByPropertyConditions<Aps>(new List<ListPropertyCondition>
                                                                          {
                                                                             ListPropertyCondition.IsNotNull("DocDate"),
                                                                             new ListPropertyCondition {Name = "KindID", Value = 1L} 
                                                                          }
                ).FirstOrDefault();
            if (aps == null) Assert.Inconclusive();

            var chk = new PerformanceChecker();
            var modelPrepayment = new PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            modelPrepayment.Load(aps.ID);
            var model = new PrepaymentAccurateViewModel(modelPrepayment);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueDeadAccurateViewModel()
        {
            var aps = DataContainerFacade.GetListByProperty<Aps>("KindID", 2L).FirstOrDefault();
            if (aps == null) Assert.Inconclusive();

            var modelDueDead = new DueDeadViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            modelDueDead.Load(aps.ID);
            var chk = new PerformanceChecker();
            var model = new DueDeadAccurateViewModel(modelDueDead);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueExcessAccurateViewModel()
        {
            var aps = DataContainerFacade.GetListByProperty<Aps>("KindID", 3L).FirstOrDefault();
            if (aps == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var modelDueExcess = new DueExcessViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            modelDueExcess.Load(aps.ID);
            var model = new DueExcessAccurateViewModel(modelDueExcess);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueUndistributedAccurateViewModel()
        {
            var aps = DataContainerFacade.GetListByProperty<Aps>("KindID", 4L).FirstOrDefault();
            if (aps == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var modelDueUndistributed = new DueUndistributedViewModel(PortfolioIdentifier.PortfolioTypes.SPN);
            modelDueUndistributed.Load(aps.ID);
            var model = new DueUndistributedAccurateViewModel(modelDueUndistributed);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDueViewModel()
        {
            var spn = DataContainerFacade.GetListByPropertyConditions<AddSPN>(new List<ListPropertyCondition>
                                                                          {
                                                                             ListPropertyCondition.IsNotNull("PFRBankAccountID"),
                                                                             ListPropertyCondition.IsNotNull("PortfolioID"),
                                                                             ListPropertyCondition.IsNotNull("MonthID"),
                                                                             new ListPropertyCondition {Name = "Kind", Value = 1} 
                                                                          }
                ).FirstOrDefault();
            if (spn == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DueViewModel(spn.ID, PortfolioIdentifier.PortfolioTypes.SPN);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMonthAccurateViewModel()
        {
            var lst = DataContainerFacade.GetListLimit<DopSPN>(1);
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new MonthAccurateViewModel(ViewModelState.Edit, lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        #region Банки, счета, отделения и их статусы

        [TestMethod]
        public void TestNPFViewModel()
        {
            var lst = GetClient().Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter);
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new NPFViewModel(ViewModelState.Edit, lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestEnrollmentPercentsViewModel()
        {

            var acc = DataContainerFacade.GetListByPropertyConditions<AccOperation>(new List<ListPropertyCondition>
                                                                          {
                                                                              ListPropertyCondition.IsNotNull("PFRBankAccountID"),
                                                                              ListPropertyCondition.IsNotNull("PortfolioID"),
                                                                              ListPropertyCondition.Equal("KindID",1L)
                                                                          }
                ).FirstOrDefault();

            var chk = new PerformanceChecker();
            var model = new EnrollmentPercentsViewModel();
            if (acc == null) { Assert.Inconclusive(); }
            model.Load(acc.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestEnrollmentOtherViewModel()
        {
            var acc = DataContainerFacade.GetListByPropertyConditions<AccOperation>(new List<ListPropertyCondition>
                                                                          {
                                                                              ListPropertyCondition.IsNotNull("PFRBankAccountID"),
                                                                              ListPropertyCondition.IsNotNull("PortfolioID"),
                                                                              ListPropertyCondition.Equal("KindID",2L)
                                                                          }
            ).FirstOrDefault();

            var chk = new PerformanceChecker();
            var model = new EnrollmentOtherViewModel();
            if (acc == null) { Assert.Inconclusive(); }
            model.Load(acc.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferToAccount()
        {
            var acc = DataContainerFacade.GetListByPropertyConditions<AccOperation>(new List<ListPropertyCondition>
                                                                          {
                                                                              ListPropertyCondition.IsNotNull("PFRBankAccountID"),
                                                                              ListPropertyCondition.IsNotNull("PortfolioID"),
                                                                              ListPropertyCondition.Equal("KindID",3L)
                                                                          }
            ).FirstOrDefault();
            var chk = new PerformanceChecker();
            var model = new TransferToAccountViewModel();
            if (acc == null) { Assert.Inconclusive(); }
            model.Load(acc.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestWithdrawalViewModel()
        {
            var acc = DataContainerFacade.GetListByPropertyConditions<AccOperation>(new List<ListPropertyCondition>
                                                                          {
                                                                              ListPropertyCondition.IsNotNull("PFRBankAccountID"),
                                                                              ListPropertyCondition.IsNotNull("PortfolioID"),
                                                                              ListPropertyCondition.Equal("KindID",4L)
                                                                          }
                   ).FirstOrDefault();
            var chk = new PerformanceChecker();
            var model = new WithdrawalViewModel();
            if (acc == null) { Assert.Inconclusive(); }
            model.Load(acc.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
		      

       

        [TestMethod]
        public void TestPFRAccountViewModel()
        {
            var lst = DataContainerFacade.GetListLimit<PfrBankAccount>(1);
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new PFRAccountViewModel(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDepositViewModel()
        {
            var deposit = DataContainerFacade.GetListLimit<Deposit>(1).FirstOrDefault();
            if (deposit == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DepositViewModel(deposit.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankAccountCreateViewModel()
        {
            var legalEntity = DataContainerFacade.GetListLimit<LegalEntity>(1).FirstOrDefault();
            if (legalEntity == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BankAccountViewModel(legalEntity.ID, ViewModelState.Create);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankAccountEditViewModel()
        {
            var account = DataContainerFacade.GetListLimit<BankAccount>(1).FirstOrDefault();
            if (account == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BankAccountViewModel(account.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestBankAgentViewModel()
        {
            var lst = GetClient().Client.GetBankAgentListForDeposit();
            var model = !lst.Any() ? new BankAgentViewModel(-1) : new BankAgentViewModel(lst.First().ID);

            var chk = new PerformanceChecker();

            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBankConclusionViewModel()
        {
            var сonclusion = DataContainerFacade.GetListLimit<BankConclusion>(1).FirstOrDefault();
            var model = сonclusion == null ? new BankConclusionViewModel(-1) : new BankConclusionViewModel(сonclusion.ID);
            var chk = new PerformanceChecker();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();

        }

        [TestMethod]
        public void TestBankViewModel()
        {
            //Create
            var chk = new PerformanceChecker();
            var model = new BankViewModel(-1L);
            TestPublicProperty(model, "SelectedPfrAgreementStatus");//
            chk.CheckPerformance();
            //Edit
            var bank = GetClient().Client.GetBankItemListForDeposit().FirstOrDefault();
            if (bank == null) { Assert.Inconclusive(); }
            chk = new PerformanceChecker();
            model = new BankViewModel(bank.ID);
            TestPublicProperty(model, "SelectedPfrAgreementStatus");//
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPFRBranchViewModel()
        {
            var branch = DataContainerFacade.GetListLimit<PFRBranch>(1).FirstOrDefault();
            var chk = new PerformanceChecker();
            if (branch == null)
                Assert.Inconclusive();
            var model = new PFRBranchViewModel(branch.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestNPFChiefViewModel()
        {

            var chief = DataContainerFacade.GetListLimit<LegalEntityChief>(1).FirstOrDefault();
            if (chief == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new NPFChiefViewModel(ViewModelState.Edit, chief.ID);
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        #endregion

        #region Бюджет
        [TestMethod]
        public void TestBudgetViewModel()
        {
            var budget = DataContainerFacade.GetListLimit<Budget>(1).FirstOrDefault();
            if (budget == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BudgetViewModel();
            model.Load(budget.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBudgetCorrectionViewModel()
        {
            var budgetCorrection = DataContainerFacade.GetListLimit<BudgetCorrection>(1).FirstOrDefault();
            if (budgetCorrection == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BudgetCorrectionViewModel();
            model.Load(budgetCorrection.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
        #endregion

        #region СЧИЛС

        [TestMethod]
        public void TestReturnViewModel()
        {
            var lst = DataContainerFacade.GetList<Return>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ReturnViewModel();
            model.Load(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferSchilsViewModel()
        {
            var lst = DataContainerFacade.GetList<Transfer>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new TransferSchilsViewModel();
            model.Load(lst.First().ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDirectionViewModel()
        {
            var direction = DataContainerFacade.GetListLimit<Direction>(1).FirstOrDefault();
            if (direction == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DirectionViewModel();
            model.Load(direction.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
        #endregion

        #region ЦБ

        [TestMethod]
        public void TestSecurityInOrderViewModel1()
        {
            var lst = DataContainerFacade.GetList<CbOrder>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityInOrderViewModel(lst.First(), SecurityInOrderType.BuyCurrency, new List<CbInOrderListItem>());
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecurityInOrderViewModel2()
        {
            var lst = DataContainerFacade.GetList<CbOrder>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityInOrderViewModel(lst.First(), SecurityInOrderType.BuyRoublesOnAuction, new List<CbInOrderListItem>());
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecurityInOrderViewModel3()
        {
            var lst = DataContainerFacade.GetList<CbOrder>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityInOrderViewModel(lst.First(), SecurityInOrderType.BuyRublesOnSecondMarket, new List<CbInOrderListItem>());
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecurityInOrderViewModel4()
        {
            var lst = DataContainerFacade.GetList<CbOrder>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityInOrderViewModel(lst.First(), SecurityInOrderType.SaleCurrency, new List<CbInOrderListItem>());
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSecurityInOrderViewModel5()
        {
            var lst = DataContainerFacade.GetList<CbOrder>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SecurityInOrderViewModel(lst.First(), SecurityInOrderType.SaleRoubles, new List<CbInOrderListItem>());
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestIncomeSecurityViewModel()
        {
            var sec = DataContainerFacade.GetListLimit<Incomesec>(1).FirstOrDefault();
            if (sec == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new IncomeSecurityViewModel(ViewModelState.Edit, sec.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        #region Корреспонденция СИ
        [TestMethod]
        public void TestLinkedDocumentSIViewModel()
        {
            var lst = DataContainerFacade.GetListLimit<Attach>(1);
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new LinkedDocumentSIViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
        #endregion

        #region Страхование

        [TestMethod]
        public void TestInsuranceSIViewModel()
        {
            var insurance = DataContainerFacade.GetListLimit<InsuranceDoc>(1).FirstOrDefault();
            if (insurance == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new InsuranceSIViewModel(ViewModelState.Edit, insurance.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestInsuranceVRViewModel()
        {
            var insurance = DataContainerFacade.GetListLimit<InsuranceDoc>(1).FirstOrDefault();
            if (insurance == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new InsuranceVRViewModel(ViewModelState.Edit, insurance.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
        #endregion

        #region Перечисления СИ
        [TestMethod]
        public void TestYearPlanViewModel()
        {
            //some registers were partly migrated from IT lotus without full required information
            var lst = DataContainerFacade.GetList<SIRegister>().Where(r => r.OperationID != null && r.CompanyYearID != null).ToList();
            var chk = new PerformanceChecker();
            if (!lst.Any())
                Assert.Inconclusive();
            var model = new YearPlanViewModel(lst.First().ID, ViewModelState.Create);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestUKPaymentPlanViewModel()
        {
            var lst = DataContainerFacade.GetList<SITransfer>().Where(t => t.ContractID != null);
            var chk = new PerformanceChecker();
            var siTransfers = lst as SITransfer[] ?? lst.ToArray();
            if (!siTransfers.Any())
                Assert.Inconclusive();
            var model = new UKPaymentPlanViewModel(siTransfers.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIRegisterViewModel()
        {
            var lst = DataContainerFacade.GetList<SIRegister>();
            SIVRRegisterViewModel model = null;
            var chk = new PerformanceChecker();
            if (lst != null && lst.Count > 0)
                model = new SIVRRegisterViewModel(Document.Types.SI, lst.First().ID, ViewModelState.Edit);
            else
                Assert.Inconclusive();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferViewModel()
        {
            var lst = DataContainerFacade.GetList<SITransfer>().Where(t => t.ContractID != null).ToList();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new TransferViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestSIReqTransferViewModel()
        {
            var lst = DataContainerFacade.GetList<ReqTransfer>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new ReqTransferViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestMonthTransferCreationWizardModel()
        {
            var chk = new PerformanceChecker();
            var model = new SIMonthTransferCreationWizardModel(8);
            model.EnsureAvailableList();
            TestPublicProperty(model); chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPlanCorrectionWizardViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new SIPlanCorrectionWizardViewModel(8, 1);
            model.EnsureAvailableList();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestTransferSIWizardViewModelLoad()
        {
            var chk = new PerformanceChecker();
            TransferWizardViewModel model = new TransferSIWizardViewModel(false);
            model.EnsureAvailableList();
            TestPublicProperty(model);
            chk.CheckPerformance();
            model = new TransferSIWizardViewModel(true);
            model.EnsureAvailableList();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Перечисления ВР

        [TestMethod]
        public void TestTransferVRWizardViewModelLoad()
        {
            var chk = new PerformanceChecker();
            TransferWizardViewModel model = new TransferVRWizardViewModel(false);
            model.EnsureAvailableList();
            TestPublicProperty(model);
            chk.CheckPerformance();
            model = new TransferVRWizardViewModel(true);
            model.EnsureAvailableList();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }
        #endregion

        #region Умершие ЗЛ

        [TestMethod]
        public void TestDeadZLEditViewModel()
        {
            //var dead = DataContainerFacade.GetList<ERZLNotify>().FirstOrDefault();;
            var dead = GetClient().Client.GetDeadZLListHib().FirstOrDefault();
            if (dead == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DeadZLEditViewModel();
            model.Load(dead.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDeadZLAddViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new DeadZLAddViewModel();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        #region Выплаты, заявки

        [TestMethod]
        public void TestDelayedPaymentClaimViewModel()
        {
            using (PerformanceChecker.New())
            {
                var model = new DelayedPaymentClaimViewModel(-1, ViewModelState.Create);
                TestPublicProperty(model);
                model.CanExecuteSave();
            }

            using (PerformanceChecker.New())
            {
                var payment = DataContainerFacade.GetListLimit<DelayedPaymentClaim>(1).FirstOrDefault();
                if (payment == null) Assert.Inconclusive();
                var model = new DelayedPaymentClaimViewModel(payment.ID, ViewModelState.Edit);
                TestPublicProperty(model);
                model.CanExecuteSave();
            }
        }

        [TestMethod]
        public void TestDepClaimOfferViewModel()
        {
            var offer = DataContainerFacade.GetListLimit<DepClaimOffer>(1).FirstOrDefault();
            if (offer == null) Assert.Inconclusive();

            using (PerformanceChecker.New())
            {
                var model = new DepClaimOfferViewModel(offer.ID, ViewModelState.Edit);
                TestPublicProperty(model);
                model.CanExecuteSave();
            }
        }

        [TestMethod]
        public void TestDepClaimSelectParamsViewModel()
        {
            var claim = DataContainerFacade.GetListLimit<DepClaimSelectParams>(1).FirstOrDefault();
            if (claim == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DepClaimSelectParamsViewModel(claim.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }


        #endregion

        #region Документ

        [TestMethod]
        public void TestSIDocumentViewModel()
        {
            var document = GetClient().Client.GetSIDocumentsListHib(Document.Statuses.Active).FirstOrDefault();
            if (document == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DocumentSIViewModel(document.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void GetVRDocumentsListHib()
        {
            var document = GetClient().Client.GetVRDocumentsListHib(Document.Statuses.Active).FirstOrDefault();
            if (document == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DocumentVRViewModel(document.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }
        [TestMethod]
        public void TestNpfDocumentViewModel()
        {
            var document = GetClient().Client.GetNpfDocumentsListHib(Document.Statuses.Active).FirstOrDefault();
            if (document == null)
                Assert.Inconclusive();
            using (PerformanceChecker.New())
            {
                var model = new DocumentNpfViewModel(document.ID, ViewModelState.Edit);
                TestPublicProperty(model);
                model.CanExecuteSave();
            }
        }

        [TestMethod]
        public void TestLinkedDocumentNpfViewModel()
        {
            var lst = DataContainerFacade.GetListLimit<Attach>(1);
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new LinkedDocumentNpfViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

      

        #region Формы F10 - F140
        [TestMethod]
        public void TestF10ViewModel()
        {
            var f010 = DataContainerFacade.GetListLimit<EdoOdkF010>(1).FirstOrDefault();
            if (f010 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F10ViewModel(f010.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF12ViewModel()
        {
            var f012 = DataContainerFacade.GetListLimit<EdoOdkF012>(1).FirstOrDefault();
            if (f012 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F12ViewModel(f012.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF14ViewModel()
        {
            var f014 = DataContainerFacade.GetListLimit<EdoOdkF014>(1).FirstOrDefault();
            if (f014 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F14ViewModel(f014.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF15ViewModel()
        {
            var f015 = DataContainerFacade.GetListLimit<EdoOdkF015>(1).FirstOrDefault();
            if (f015 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F15ViewModel(f015.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF16ViewModel()
        {
            var f016 = DataContainerFacade.GetListLimit<EdoOdkF016>(1).FirstOrDefault();
            if (f016 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F16ViewModel(f016.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF20ViewModel()
        {
            var f020 = DataContainerFacade.GetListLimit<EdoOdkF020>(1).FirstOrDefault();
            if (f020 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F20ViewModel(f020.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF22ViewModel()
        {
            var f022 = DataContainerFacade.GetListLimit<EdoOdkF022>(1).FirstOrDefault();
            if (f022 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F22ViewModel(f022.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF25ViewModel()
        {
            var f025 = DataContainerFacade.GetListLimit<EdoOdkF025>(1).FirstOrDefault();
            if (f025 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F25ViewModel(f025.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF26ViewModel()
        {
            var f026 = DataContainerFacade.GetListLimit<EdoOdkF026>(1).FirstOrDefault();
            if (f026 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F26ViewModel(f026.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF40ViewModel()
        {
            var f040 = DataContainerFacade.GetListLimit<EdoOdkF040>(1).FirstOrDefault();
            if (f040 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F40ViewModel(f040.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF401402ViewModel()
        {
            var lst = DataContainerFacade.GetListByPropertyConditions<F401402UK>(new List<ListPropertyCondition>
                                                                          {
                                                                             ListPropertyCondition.IsNotNull("EdoID")
                                                                          });
            if (!lst.Any()) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var sample = lst.Last();

            if (sample == null)
                Assert.Inconclusive("No valid items exists.");

            var model = new F401402UKViewModel(sample.ID, false);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF50ViewModel()
        {
            var f050 = DataContainerFacade.GetListLimit<EdoOdkF050>(1).FirstOrDefault();
            if (f050 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F50ViewModel(f050.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF60ViewModel()
        {
            var f060 = DataContainerFacade.GetListLimit<EdoOdkF060>(1).FirstOrDefault();
            if (f060 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F60ViewModel(f060.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF70ViewModel()
        {
            var f070 = DataContainerFacade.GetListLimit<EdoOdkF070>(1).FirstOrDefault();
            if (f070 == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F70ViewModel(f070.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestF80ViewModel()
        {
            var f080 = DataContainerFacade.GetListLimit<EdoOdkF080>(1).FirstOrDefault();
            if (f080 == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new F80ViewModel(f080.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        #region Поручения и Отчеты

        [TestMethod]
        public void TestOrderViewModel()
        {
            var order = DataContainerFacade.GetListLimit<CbOrder>(1).FirstOrDefault();
            if (order == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new OrderViewModel(ViewModelState.Edit, order.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestBuyReportViewModel()
        {
            var report = GetClient().Client.GetReportByOrderType(OrderTypeIdentifier.BUYING, 1).FirstOrDefault();
            if (report == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new BuyReportViewModel(report.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }


        //[TestMethod]
        //public void TestBuyReportViewModel()
        //{
        //    List<OrdReport> lst = DataContainerFacade.GetList<OrdReport>();
        //    if (!lst.Any())
        //        Assert.Inconclusive();
        //    OrdReport report = lst.FirstOrDefault(rep => rep.CbInOrderID.HasValue && rep.GetCbInOrder().GetOrder().Type.Trim() == OrderTypeIdentifier.BUYING);
        //    if (report == null)
        //        Assert.Inconclusive();
        //    PerformanceChecker chk = new PerformanceChecker();
        //    BuyReportViewModel model = new BuyReportViewModel(report.ID, ViewModelState.Edit);
        //    TestPublicProperty(model);
        //    model.CanExecuteSave();
        //    chk.CheckPerformance();
        //}


        [TestMethod]
        public void TestSaleReportViewModel()
        {
            var report = GetClient().Client.GetReportByOrderType(OrderTypeIdentifier.SELLING, 1).FirstOrDefault();
            if (report == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new SaleReportViewModel(report.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestOrderRelayingViewModel()
        {
            var orderParent = DataContainerFacade.GetListByPropertyConditions<CbOrder>(new List<ListPropertyCondition>
                                                                          {
                                                                             ListPropertyCondition.IsNotNull("ParentId")
                                                                            
                                                                          }).FirstOrDefault();
            if (orderParent == null) Assert.Inconclusive();
            var order = DataContainerFacade.GetByID<CbOrder>(orderParent.ParentId);
            if (order == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();

            var model = new OrderRelayingViewModel(ViewModelState.Edit, order.ID);

            TestPublicProperty(model, "SelectedPortfolioBankAccount");
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestFastReportInputViewModel()
        {
            var cbInOrder = DataContainerFacade.GetListLimit<CbInOrder>(1).FirstOrDefault();
            if (cbInOrder == null) Assert.Inconclusive();
            var order = DataContainerFacade.GetByID<CbOrder>(cbInOrder.OrderID);
            if (order == null) Assert.Inconclusive();
            //List<CbOrder> lst = DataContainerFacade.GetList<CbOrder>();
            //if (!lst.Any())
            //    Assert.Inconclusive();


            //CbOrder order = lst.First(ord =>
            //{
            //    IEnumerable<CbInOrder> cbinorderlist = ord.GetCbInOrderList().Cast<CbInOrder>();
            //    return cbinorderlist != null && cbinorderlist.Count() > 0;
            //}); 
            var chk = new PerformanceChecker();
            var model = new FastReportInputViewModel(order.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }


        #endregion

        #region Фин.реестры

       

        [TestMethod]
        public void TestFinregisterPropsInputViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new FinregisterPropsInputViewModel();
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestFinRegisterViewModel()
        {
            var register = DataContainerFacade.GetListLimit<Finregister>(1).FirstOrDefault();
            if (register == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new FinRegisterViewModel(register.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestGivenFinRegisterViewModel()
        {
            var register = DataContainerFacade.GetListLimit<Finregister>(1).FirstOrDefault();
            if (register == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new GivenFinRegisterViewModel(register.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        #endregion

        [TestMethod]
        public void TestOwnFundsViewModel()
        {
            var lst = GetClient().Client.GetOwnedFoundsListHib();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new OwnFundsViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestERZLNotifyViewModel()
        {
            var erzl = DataContainerFacade.GetListLimit<ERZL>(1).FirstOrDefault();
            var chk = new PerformanceChecker();
            if (erzl == null)
                Assert.Inconclusive();
            var model = new ERZLNotifyViewModel();
            model.Init(erzl.ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance(100000);
        }

        [TestMethod]
        public void TestCostsViewModel()
        {
            var cost = DataContainerFacade.GetListLimit<Cost>(1).FirstOrDefault();
            if (cost == null) Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new CostsViewModel();
            model.Load(cost.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestDivisionViewModel()
        {
            var lst = DataContainerFacade.GetList<Division>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new DivisionViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestPersonViewModel()
        {
            var lst = DataContainerFacade.GetList<Person>();
            if (!lst.Any())
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new PersonViewModel(lst.First().ID, ViewModelState.Edit);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestExchangeRateViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ExchangeRateViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestKBKViewModel()
        {
            using (PerformanceChecker.New())
            {
                var kbk = DataContainerFacade.GetListLimit<KBK>(1).FirstOrDefault();
                if (kbk == null) Assert.Inconclusive();
                var model = new KBKViewModel(kbk.ID, ViewModelState.Edit);
                TestPublicProperty(model);
            }
        }

        [TestMethod]
        public void TestMarketPriceViewModel()
        {
            var price = DataContainerFacade.GetListLimit<MarketPrice>(1).FirstOrDefault();
            if (price == null)
                Assert.Inconclusive();
            var chk = new PerformanceChecker();
            var model = new MarketPriceViewModel();
            model.Load(price.ID);
            TestPublicProperty(model);
            model.CanExecuteSave();
            chk.CheckPerformance();
        }

        [TestMethod]
        public void TestClientThemesViewModel()
        {
            var chk = new PerformanceChecker();
            var model = new ClientThemesViewModel();
            TestPublicProperty(model);
            chk.CheckPerformance();
        }

       

        [TestMethod]
        public void TestNPFCodeViewModel()
        {
            var lst = DataContainerFacade.GetList<NPFErrorCode>();
            if (!lst.Any())Assert.Inconclusive();

            var chk = new PerformanceChecker();
            var model = new NPFCodeViewModel(lst.First().ID, ViewModelState.Create, false);
            TestPublicProperty(model);
            model.CanExecuteSave();
            model = new NPFCodeViewModel(-1, ViewModelState.Create, false);
            TestPublicProperty(model);
            model.CanExecuteSave();

            chk.CheckPerformance();
        }
        
    }
}
