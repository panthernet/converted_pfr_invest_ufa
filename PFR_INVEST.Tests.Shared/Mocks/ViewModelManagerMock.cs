﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.Tests.Shared.Mocks
{
	public class ViewModelManagerMock : IViewModelManager
	{
		public void RefreshCardViewModel(Type type, long cardID)
		{
			
		}

		public void RefreshViewModels(params Type[] modelTypes)
		{
			
		}


		public void UpdateDataInAllModels(IList<KeyValuePair<Type, long>> updatedData)
		{
			
		}


		public void UpdateDataInAllModels(Type type, long id)
		{
			
		}
	}
}
