﻿using System;
using System.Collections.Concurrent;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using db2connector.Contract;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;

namespace PFR_INVEST.Tests.Shared
{
    public class LightService: IDisposable
    {
        private static readonly ConcurrentDictionary<string, WCFClientLight> Clients = new ConcurrentDictionary<string, WCFClientLight>();
        private const string DEFAULT_USERNAME = "SPY10001HEX";

        public static WCFClientLight Get(string username = null)
        {
            var name = string.IsNullOrEmpty(username) ? DEFAULT_USERNAME : username;
            if (!Clients.ContainsKey(name) || Clients[name].IsFaulted())
            {
                Clients.Add(name, WCFClientLight.CreateLightClient(name));
            }
            return Clients[name];
        }

        public void Dispose()
        {
            Clients.ForEach(a=> a.Value?.Dispose());
            Clients.Clear();
        }

        public static object GetUser(string userName, string password = null)
        {
            return null;
           /* ApplicationUser user = null;
            using (var cli = WCFClient.CreateLightClient(userName, password))
            {
                var token = cli.Client.GetToken().ToString();
                var obj = cli.Client.GetUserInfo(new AuthRequest { Value1 = userName, AccessToken = token });
                if (obj == null) return null;
                var u = obj.Result as User;
                if (u != null)
                {
                    user = new ApplicationUser { UserName = userName, DisplayName = u.Name };
                    var roles = cli.Client.GetRoles(new AuthRequest { Value1 = userName, AccessToken = token }).Result as List<Role>;
                    roles?.ForEach(a => user.DokipRoles.Add(a.Name));
                }
            }
            return user;
            */
        }

        public static Task<bool> ValidateUser(string username, string password)
        {
            try
            {
                return Task.FromResult(true);
                //   var user = GetUser(username, password);
//                var userRole = UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.User);
                //              return Task.FromResult(user.DokipRoles.Any(a => a == userRole));
            }
            catch (InvalidLoginOrPasswordException)
            {
                return Task.FromResult(false);
            }
            catch (NotUserException)
            {
                return Task.FromResult(false);
            }
            catch (EndpointNotFoundException e)
            {
                return Task.FromResult(false);
            }
            catch (SecurityNegotiationException e)
            {
                return Task.FromResult(false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return Task.FromResult(false);
            }
        }
    }
}