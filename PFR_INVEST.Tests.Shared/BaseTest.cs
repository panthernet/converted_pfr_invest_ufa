﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Tests.Shared.Mocks;

namespace PFR_INVEST.Tests.Shared
{
    public class BaseTest
    {
        protected static Random Random = new Random();

        protected List<WCFClientLight> Clients { get; } = new List<WCFClientLight>();
        protected static List<WCFClientLight> StaticClients { get; } = new List<WCFClientLight>();

        private int _currentClient;
        private static int _currentClientStatic;

        static BaseTest()
        {
            ViewModelBase.DoNotThrowExceptionOnError = false;
            ViewModelBase.BusyHelper = new BusyHelperMock();
            ViewModelBase.ViewModelManager = new ViewModelManagerMock();
            ViewModelBase.Logger = new Log(null);
            App.ShowMessageBoxOnError = false;
            App.SetupEnvironment();

            var maxConnections = 3;
            var maxFails = 20;
            var fails = 0;
            for (var i = 1; i <= maxConnections; i++)
            {
                try
                {
                    StaticClients.Add(LightService.Get($"TestUserStatic{i}"));
                }
                catch
                {
                    i--;
                    fails++;
                    if (fails > maxFails) break;
                }
            }
            StaticClients.ForEach(a => a.Client.OpenDBConnection());

            BLServiceSystem.GetDataServiceDelegate = () => GetClientStatic()?.Client;
            ServiceSystem.GetDataServiceDelegate = () => GetClientStatic()?.Client;
            AuthServiceSystem.GetDataServiceDelegate = () => GetClientStatic()?.Client;
            AP.Provider = new AuthProvider("") { CurrentUserSecurity = new UserSecurity() };
            AP.Provider.CurrentUserSecurity.AddRole(DOKIP_ROLE_TYPE.Administrator);

        }

        public BaseTest()
        {
            var maxConnections = 3;
            var maxFails =20;
            var fails = 0;
            for (var i = 1; i <= maxConnections; i++)
            {
                try
                {
                    Clients.Add(LightService.Get($"TestUser{i}"));
                }
                catch
                {
                    i--;
                    fails++;
                    if (fails > maxFails) break;
                }
            }

            Clients.ForEach(a=> a.Client.OpenDBConnection());
        }

        protected WCFClientLight GetClient()
        {
            if (Clients.Count == 0) return null;
            _currentClient++;
            if (_currentClient >= Clients.Count) _currentClient = 0;
            return Clients[_currentClient];
        }

        protected static WCFClientLight GetClientStatic()
        {
            if (StaticClients.Count == 0) return null;
            _currentClientStatic++;
            if (_currentClientStatic >= StaticClients.Count) _currentClientStatic = 0;
            return StaticClients[_currentClientStatic];
        }


        protected T LoadFirstItem<T>() where T:BaseDataObject
        {
            var list = DataContainerFacade.GetList<T>();
            if (!list.Any())
                Assert.Inconclusive("List of '{0}' is empty", typeof(T).Name);
            return list.First();
        }

        protected void TestPublicProperty(ViewModelBase model, params string[] excludedProperties)
        {
            var card = model as ViewModelCard;
            if (card != null) 
            {
                if (card.State != ViewModelState.Create && !card.IsIDChanged && card.IsNotSavable == false) 
                {
                    //Assert.Fail("ID property not initialised");
                }
                    
            }


            var properties = model.GetType().GetProperties().Where(p => p.CanRead && p.GetIndexParameters().Length == 0);
            foreach (var pi in properties)
            {
                if (excludedProperties.Contains(pi.Name))
                    continue;
                var tmp = pi.GetValue(model, null);
                if (pi.CanWrite)
                    pi.SetValue(model, tmp, null);
            }
        }


        public XmlReader GetInputXMLFile(string filename, XmlReaderSettings settings)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return XmlReader.Create(thisAssembly.GetManifestResourceStream(path + "." + filename), settings);
        }

        public TextReader GetInputFile(string filename)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return new StreamReader(thisAssembly.GetManifestResourceStream(path + "." + filename));
        }

        public TextReader GetInputFile(string filename,string encoding)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            const string path = "PFR_INVEST.Tests.TestFiles";
            return new StreamReader(thisAssembly.GetManifestResourceStream(path + "." + filename), System.Text.Encoding.GetEncoding(encoding));
        }
    }
}
