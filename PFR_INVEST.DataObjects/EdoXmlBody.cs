﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class EdoXmlBody : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual byte[] Body { get; set; }
    }
}
