﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class DopSPN : BaseDataObject
	{
		public enum Kinds 
		{
			/// <summary>
			/// Уточнение за месяц
			/// </summary>
			DueMonth = 1,

			/// <summary>
			/// Уточнение пени и штрафов за месяц
			/// </summary>
			PennyMonth = 2,

			/// <summary>
			/// Уточнение за квартал
			/// </summary>
			DueQuarter = 3,

			/// <summary>
			/// Уточнение пени и штрафов за квартал
			/// </summary>
			PennyQuarter = 4
		}

		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		private long? _AddSpnID;
		public virtual long? AddSpnID
		{
			get { return _AddSpnID; }
			set { if (_AddSpnID != value) { _AddSpnID = value; OnPropertyChanged("AddSpnID"); } }
		}

		[DataMember]
		private long? _PortfolioID;
		public virtual long? PortfolioID
		{
			get { return _PortfolioID; }
			set { if (_PortfolioID != value) { _PortfolioID = value; OnPropertyChanged("PortfolioID"); } }
		}

		
		[DataMember]
		private long? _KDocID;
		public virtual long? KDocID
		{
			get { return _KDocID; }
			set { if (_KDocID != value) { _KDocID = value; OnPropertyChanged("KDocID"); } }
		}


		[DataMember]
		private long? _LegalEntityID;
		public virtual long? LegalEntityID
		{
			get { return _LegalEntityID; }
			set { if (_LegalEntityID != value) { _LegalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
		}

		[DataMember]
		private DateTime? _OperationDate;
		public virtual DateTime? OperationDate
		{
			get { return _OperationDate; }
			set { if (_OperationDate != value) { _OperationDate = value; OnPropertyChanged("OperationDate"); } }
		}

		[DataMember]
		private DateTime? _Date;
		public virtual DateTime? Date
		{
			get { return _Date; }
			set { if (_Date != value) { _Date = value; OnPropertyChanged("Date"); } }
		}

		[DataMember]
		private string _RegNum;
		public virtual string RegNum
		{
			get { return _RegNum; }
			set { if (_RegNum != value) { _RegNum = value; OnPropertyChanged("RegNum"); } }
		}

		[DataMember]
		private decimal? _MSumm;
		public virtual decimal? MSumm
		{
			get { return _MSumm; }
			set { if (_MSumm != value) { _MSumm = value; OnPropertyChanged("MSumm"); } }
		}

		[DataMember]
		private decimal? _MSummEmployer;
		public virtual decimal? MSummEmployer
		{
			get { return _MSummEmployer; }
			set { if (_MSummEmployer != value) { _MSummEmployer = value; OnPropertyChanged("MSummEmployer"); } }
		}

		[DataMember]
		private decimal? _MSummNakop;
		public virtual decimal? MSummNakop
		{
			get { return _MSummNakop; }
			set { if (_MSummNakop != value) { _MSummNakop = value; OnPropertyChanged("MSummNakop"); } }
		}

		[DataMember]
		private decimal? _MSummOther;
		public virtual decimal? MSummOther
		{
			get { return _MSummOther; }
			set { if (_MSummOther != value) { _MSummOther = value; OnPropertyChanged("MSummOther"); } }
		}

		[DataMember]
		private decimal? _MSummPerc;
		public virtual decimal? MSummPerc
		{
			get { return _MSummPerc; }
			set { if (_MSummPerc != value) { _MSummPerc = value; OnPropertyChanged("MSummPerc"); } }
		}

		[DataMember]
		private decimal? _ChSumm;
		public virtual decimal? ChSumm
		{
			get { return _ChSumm; }
			set { if (_ChSumm != value) { _ChSumm = value; OnPropertyChanged("ChSumm"); } }
		}

		[DataMember]
		private string _Comment;
		public virtual string Comment
		{
			get { return _Comment; }
			set { if (_Comment != value) { _Comment = value; OnPropertyChanged("Comment"); } }
		}

		[DataMember]
		private long? _PeriodID;
		public virtual long? PeriodID
		{
			get { return _PeriodID; }
			set { if (_PeriodID != value) { _PeriodID = value; OnPropertyChanged("PeriodID"); } }
		}

		[DataMember]
		private long? _DocKind;
		public virtual long? DocKind
		{
			get { return _DocKind; }
			set { if (_DocKind != value) { _DocKind = value; OnPropertyChanged("DocKind"); } }		
        }

        //Quarter        
        private int? _Quarter;
        [DataMember]
        public virtual int? Quarter
        {
            get { return _Quarter; }
            set { if (_Quarter != value) { _Quarter = value; OnPropertyChanged("Quarter"); } }
        }

        //Номер счёта для записей квартальных уточнений       
        private long? _QuarterPfrBankAccountID;
        [DataMember]
        public virtual long? QuarterPfrBankAccountID
        {
            get { return _QuarterPfrBankAccountID; }
            set { if (_QuarterPfrBankAccountID != value) { _QuarterPfrBankAccountID = value; OnPropertyChanged("QuarterPfrBankAccountID"); } }
        }

        /// <summary>
        /// Привязка к году для квартальных уточнений
        /// </summary>
        [DataMember]
        public virtual long? QuarterYear { get; set; }

        [DataMember]
        public virtual long? ClosingKDocID { get; set; }

		/// <summary>
		/// Год родительского взноса, только для чтения
		/// </summary>
		[DataMember]
		public virtual long? YearID { get;  set; }

		/// <summary>
		/// Год родительского ДК, только для чтения
		/// </summary>
		[DataMember]
		public virtual long? KDOCYearID { get; set; }

	}
}
