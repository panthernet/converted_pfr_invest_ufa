﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class BankConclusion : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long? _importId;
        public virtual long? ImportID
        {
            get { return _importId; }
            set
            {
                if (_importId == value) return;
                _importId = value;
                OnPropertyChanged("ImportID");
            }
        }

        [DataMember]
        private string _number;
        public virtual string Number
        {
            get { return _number; }
            set
            {
                if (_number == value) return;
                try
                {
                    _intNumber = string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
                }
                catch
                {
                    // ignored
                }
                _number = value;
                OnPropertyChanged("Number");
            }
        }

        [DataMember]
        private int _intNumber;
        public virtual int IntNumber
        {
            get { return _intNumber; }
            set
            {
                if (_intNumber == value) return;
                _intNumber = value;
                OnPropertyChanged("IntNumber");
            }
        }

        [DataMember]
        private string _code;
        public virtual string Code
        {
            get { return _code; }
            set
            {
                if (_code == value) return;
                _code = value;
                OnPropertyChanged("Сode");
            }
        }

        [DataMember]
        private string _name;
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (_name == value) return;
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        [DataMember]
        private string _city = "-";
        public virtual string City
        {
            get { return _city; }
            set
            {
                if (_city == value) return;
                _city = value;
                OnPropertyChanged("City");
            }
        }

        [DataMember]
        private DateTime _importDate;
        public virtual DateTime ImportDate
        {
            get { return _importDate; }
            set
            {
                if (_importDate == value) return;
                _importDate = value;
                OnPropertyChanged("ImportDate");
            }
        }

        [IgnoreDataMember]
        public virtual string BankStatus
        {
            get { return BankStatusBool ? "ДА" : "НЕТ"; }
            set
            {
                BankStatusBool = value.ToLower() == "да";
            }
        }

        [IgnoreDataMember]
        private bool _bankStatusBool;
        public virtual bool BankStatusBool
        {
            get { return _bankStatusBool; }
            set
            {
                if (_bankStatusBool == value) return;
                _bankStatusBool = value;
                OnPropertyChanged("BankStatus");
            }
        }

        [IgnoreDataMember]
        public virtual string ElementName { get; set; }

        [IgnoreDataMember]
        public virtual long ImportElementID { get; set; }
    }
}
