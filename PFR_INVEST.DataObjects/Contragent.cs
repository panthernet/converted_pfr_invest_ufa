﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Contragent : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        public enum Types
        {
            Bank = 1,
            BankAgent=2,
            GUK = 3,
            Npf = 4,
            PFR = 5,
            SD = 6,
            UK = 7
        }

        [DataMember]
        public virtual long ID { get; set; }
        
        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string TypeName { get; set; }
        
        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }
    }
}
