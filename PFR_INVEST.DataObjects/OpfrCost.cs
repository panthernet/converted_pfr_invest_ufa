﻿using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects
{
    [DataContract(IsReference = true)]
    public class OpfrCost: IKnownType
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }
    }
}
