﻿namespace PFR_INVEST.DataAccess.Server.UKReport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public interface IUKReportImportItem
    {
        string FileName
        {
            get;
            set;
        }

        string PathName
        {
            get;
            set;
        }

        string ReportOnDate
        {
            get;
            set;
        }

        DateTime? ReportOnDateNative
        {
            get;
            set;
        }
       
    }
}
