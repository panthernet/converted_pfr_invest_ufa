﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class EdoOdkF010 : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? ContractId { get; set; }

		/// <summary>
		/// Дата составления отчета:
		/// </summary>
		[DataMember]
		public virtual DateTime? ReportDate { get; set; }

		[IgnoreDataMember]
		public virtual decimal? NetWealthSum { get { return this.ItogoScha1; } set { this.ItogoScha1 = value; } }

		/// <summary>
		/// Регистрационный номер:
		/// </summary>
		[DataMember]
		public virtual string RegNum { get; set; }

		/// <summary>
		/// Дата регистрации:
		/// </summary>
		[DataMember]
		public virtual DateTime? RegDate { get; set; }

		/// <summary>
		/// Дата, данные на которую содержит отчет:
		/// </summary>
		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }

		/// <summary>
		/// Тип отчета:
		/// </summary>
		[DataMember]
		public virtual long? ReportType { get; set; }

		/// <summary>
		/// Подписавший отчет сотрудник УК:
		/// </summary>
		[DataMember]
		public virtual string PersonUK { get; set; }

		/// <summary>
		/// Подписавший отчет сотрудник спецдепа:
		/// </summary>
		[DataMember]
		public virtual string PersonSpecDep { get; set; }

		/// <summary>
		/// Исходящий номер документа:
		/// </summary>
		[DataMember]
		public virtual string RegNumberOut { get; set; }

		[DataMember]
		public virtual string Portfolio { get; set; }

		[DataMember]
		public virtual decimal? Funds1 { get; set; }
		[DataMember]
		public virtual decimal? Funds2 { get; set; }
		[DataMember]
		public virtual decimal? Deposit1 { get; set; }
		[DataMember]
		public virtual decimal? Deposit2 { get; set; }
		[DataMember]
		public virtual decimal? Cb1 { get; set; }
		[DataMember]
		public virtual decimal? Cb2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrf1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbrf2 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsubrf1 { get; set; }
		[DataMember]
		public virtual decimal? Gcbsubrf2 { get; set; }
		[DataMember]
		public virtual decimal? Obligaciimo1 { get; set; }
		[DataMember]
		public virtual decimal? Obligaciimo2 { get; set; }
		[DataMember]
		public virtual decimal? Obligaciirho1 { get; set; }
		[DataMember]
		public virtual decimal? Obligaciirho2 { get; set; }
		[DataMember]
		public virtual decimal? Akciire1 { get; set; }
		[DataMember]
		public virtual decimal? Akciire2 { get; set; }
		[DataMember]
		public virtual decimal? Pai1 { get; set; }
		[DataMember]
		public virtual decimal? Pai2 { get; set; }
		[DataMember]
		public virtual decimal? Ipotech2 { get; set; }
		[DataMember]
		public virtual decimal? Ipotech1 { get; set; }
		[DataMember]
		public virtual decimal? Debit1 { get; set; }
		[DataMember]
		public virtual decimal? Debit2 { get; set; }
		[DataMember]
		public virtual decimal? Prof1 { get; set; }
		[DataMember]
		public virtual decimal? Prof2 { get; set; }
		[DataMember]
		public virtual decimal? Proc1 { get; set; }
		[DataMember]
		public virtual decimal? Proc2 { get; set; }
		[DataMember]
		public virtual decimal? OtherDebit1 { get; set; }
		[DataMember]
		public virtual decimal? OtherDebit2 { get; set; }
		[DataMember]
		public virtual decimal? OtherActiv1 { get; set; }
		[DataMember]
		public virtual decimal? OtherActiv2 { get; set; }
		[DataMember]
		public virtual decimal? Itogo1 { get; set; }
		[DataMember]
		public virtual decimal? Itogo2 { get; set; }
		[DataMember]
		public virtual decimal? Credit1 { get; set; }
		[DataMember]
		public virtual decimal? Credit2 { get; set; }
		[DataMember]
		public virtual decimal? ProfObiaz1 { get; set; }
		[DataMember]
		public virtual decimal? ProfObiaz2 { get; set; }
		[DataMember]
		public virtual decimal? CreditReward1 { get; set; }
		[DataMember]
		public virtual decimal? CreditReward2 { get; set; }
		[DataMember]
		public virtual decimal? OtherCredit1 { get; set; }
		[DataMember]
		public virtual decimal? OtherCredit2 { get; set; }
		[DataMember]
		public virtual decimal? ItogoObiaz1 { get; set; }
		[DataMember]
		public virtual decimal? ItogoObiaz2 { get; set; }
		[DataMember]
		public virtual decimal? ItogoScha1 { get; set; }
		[DataMember]
		public virtual decimal? ItogoScha2 { get; set; }

        [DataMember]
        public virtual int IsTotalReport { get; set; }
	}
}
