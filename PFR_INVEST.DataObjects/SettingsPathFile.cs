﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class SettingsPathFile : BaseDataObject
	{
		public enum IdDefinition
		{
			OnesImport = 2,
			OnesExport = 1,
			PkipImport = 4,
			PkipExport = 3
		}

		public enum FileLocation
		{
			Client = 1,
			Server = 2
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime Date { get; set; }

		[DataMember]
		public virtual int ServClient { get; set; }

		[IgnoreDataMember]
		public virtual FileLocation ServClientValue
		{
			get { return (FileLocation)ServClient; }
			set { ServClient = (int)value; }
		}

		[DataMember]
		public virtual string Path { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

	}
}
