﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

    public class AccOperation : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? OperationDate { get; set; }

        [DataMember]
        public virtual DateTime? NewDate { get; set; }

        [DataMember]
        public virtual string Content { get; set; }

        [DataMember]
        public virtual decimal? Summ { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual long? KindID { get; set; }

        //можно ли добавить портфель без счетов, чтоб не было видно? Или добавить туда все счета. 
        //В портфеле комментарий должен быть что это портфель под импорт!!!
        //Счет подвязывается из нашей БД
        //В п/п тоже добавить этот портфель!!!


        /// <summary>
        /// Валюта
        /// </summary>
		[DataMember]
        public virtual long? CurrencyID { get; set; }

        /// <summary>
        /// Кастомный курс валюты
        /// </summary>
        [DataMember]
        public virtual long? CursID { get; set; }

        [DataMember]
        public virtual long? PFRBankAccountID { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? SourcePFRBankAccountID { get; set; }

        [DataMember]
        public virtual long? SourcePortfolioID { get; set; }
        [DataMember]
        public virtual int AddToCHFR { get; set; }
    }
}
