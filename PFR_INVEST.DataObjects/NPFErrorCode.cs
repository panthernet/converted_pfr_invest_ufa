﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class NPFErrorCode : BaseDataObject, IMarkedAsDeleted, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual int IsErrorCode { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }
    }
}
