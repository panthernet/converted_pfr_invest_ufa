﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class EdoOdkF401402 : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? ReportDate { get; set; }

        [DataMember]
        public virtual string Executor { get; set; }

        [DataMember]
        public virtual string AuthorizedPerson { get; set; }

        [DataMember]
        public virtual string RegNumberOut { get; set; }

        [DataMember]
        public virtual DateTime? FromDate { get; set; }

        [DataMember]
        public virtual DateTime? ToDate { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [IgnoreDataMember]
        public virtual int? Year
        {
            get
            {
                return FromDate.HasValue ? (int?)FromDate.Value.Year : null;
            }
        }

        [IgnoreDataMember]
        public virtual string Month
        {
            get
            {
                return FromDate.HasValue ? FromDate.Value.ToString("MMMM") : null;
            }
        }
    }
}
