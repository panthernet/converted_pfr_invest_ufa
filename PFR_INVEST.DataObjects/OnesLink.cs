﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesLink: BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ImportID { get; set; }

        [DataMember]
        public virtual long DocID { get; set; }
    }
}
