﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract(IsReference = true)]
    public class DeletedDataItem
    {
        [DataMember]
        public virtual List<DeletedData> DataList { get; set; } = new List<DeletedData>();
        [DataMember]
        public virtual long PreviousStatus { get; set; } = -10;
        [JsonIgnore]
        [IgnoreDataMember]
        public bool HasPrevoiusStatus => PreviousStatus != -10;
        [JsonIgnore]
        [IgnoreDataMember]
        public string DeleteLogCaption { get; set; }
    }

    [DataContract(IsReference = true)]
    public class DeletedData
    {
        [DataMember]
        public virtual long Id { get; set; }
        [DataMember]
        public virtual string StringType { get; set; }
        [DataMember]
        public virtual long PreviousStatus { get; set; } = -10;
        [JsonIgnore]
        [IgnoreDataMember]
        public bool HasPrevoiusStatus => PreviousStatus != -10;
        [JsonIgnore]
        [IgnoreDataMember]
        public long? OptionalEntryId { get; set; }

    }

}
