﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ServiceItems
{
    [DataContract(IsReference = true)]
    public class SaveSIItem
    {
        [DataMember]
        public Contragent Contragent;

        [DataMember]
        public LegalEntity LegalEntity;

        [DataMember]
        public List<License> Licenses = new List<License>();

        [DataMember]
        public List<Contract> Contracts = new List<Contract>();

        [DataMember]
        public List<Contact> Contacts = new List<Contact>();
    }
}
