﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PFR_INVEST.DataObjects
{
    public class LegalEntityRating : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        
        private long _LegalEntityID;
        [DataMember]
        public virtual long LegalEntityID
        {
            get { return _LegalEntityID; }
            set { if (_LegalEntityID != value) { _LegalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }


        private long _MultiplierRatingID;
        [DataMember]
        public virtual long MultiplierRatingID
        {
            get { return _MultiplierRatingID; }
            set { if (_MultiplierRatingID != value) { _MultiplierRatingID = value; OnPropertyChanged("MultiplierRatingID"); } }
        }

        //private long _AgencyID;
        //[DataMember]
        //public virtual long AgencyID
        //{
        //    get { return _AgencyID; }
        //    set { if (_AgencyID != value) { _AgencyID = value; OnPropertyChanged("AgencyID"); } }
        //}

        //private long _RatingID;
        //[DataMember]
        //public virtual long RatingID
        //{
        //    get { return _RatingID; }
        //    set { if (_RatingID != value) { _RatingID = value; OnPropertyChanged("RatingID"); } }
        //}

        
        private DateTime? _Date;
        [DataMember]
        public virtual DateTime? Date
        {
            get { return _Date; }
            set { if (_Date != value) { _Date = value; OnPropertyChanged("Date"); } }
        }

        [IgnoreDataMember]
        public virtual string ShortDate
        {
            get { return _Date.HasValue ? _Date.Value.ToShortDateString() : string.Empty; }
        }


        //private decimal? _Multiplier;
        //[DataMember]
        //[Description("Коеффициент")]
        //public virtual decimal? Multiplier
        //{
        //    get { return _Multiplier; }
        //    set { if (_Multiplier != value) { _Multiplier = value; OnPropertyChanged("Multiplier"); } }
        //}

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    //case "AgencyID":
                    //    if (this.AgencyID <= 0)
                    //        return "Поле обязательное для заполнения";
                    //    break;
                    //case "RatingID":
                    //    if (this.RatingID <= 0)
                    //        return "Поле обязательное для заполнения";
                    //    break;
                    case "MultiplierRatingID":
                        if (this.MultiplierRatingID <= 0)
                            return "Поле обязательное для заполнения";
                        break;
                    case "Date":
                        if (!Date.HasValue)
                            return "Поле обязательное для заполнения";
                        break;
                    //case "Multiplier":
                    //    if (!this.Multiplier.HasValue)// || this.Multiplier == 0M)
                    //        return "Поле обязательное для заполнения";
                    //    if (this.Multiplier < 0.0M)
                    //        return "Коэффициент должен быть положительным";
                    //    if (this.Multiplier > 1.0M)
                    //        return "Максимально допустимое значение коэффициента 1.0";
                    //    break;
                }
                return null;
            }
        }

       
    }
}