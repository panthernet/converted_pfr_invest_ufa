﻿using System.Runtime.Serialization;
using System.ComponentModel;
namespace PFR_INVEST.DataObjects
{
    public class MultiplierRating : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        //private long? _AgencyID;
        [DataMember]
        public virtual long? AgencyID { get; set; }
        //{
        //    get { return _AgencyID; }
        //    set { if (_AgencyID != value) { _AgencyID = value; OnPropertyChanged("AgencyID"); } }
        //}


        //private long? _RatingID;
        [DataMember]
        public virtual long? RatingID { get; set; }
        //{
        //    get { return _RatingID; }
        //    set { if (_RatingID != value) {  } }
        //}

        [DataMember]
        //private long? _number = 1;
        public virtual long? Number { get; set; }
        //{
        //    get { return _number; }
        //    set
        //    {
        //        if (_number != value)
        //        {
        //            _number = value;
        //            OnPropertyChanged("Number");
        //        }
        //    }
        //}

        //private decimal? _Multiplier;
        [DataMember]
        [Description("Коеффициент")]
        public virtual decimal? Multiplier { get; set; }
        //{
        //    get { return _Multiplier; }
        //    set { if (_Multiplier != value) { _Multiplier = value; OnPropertyChanged("Multiplier"); } }
        //}
    }
}
