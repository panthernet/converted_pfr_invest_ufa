﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	/// <summary>
	/// Платежное поручение по депозиту
	/// </summary>
	public class CBPaymentDetail : BaseDataObject, IMarkedAsDeleted
	{
		public CBPaymentDetail()
		{
			
		}

		/// <summary>
		/// Идентификатор
		/// </summary>
		[DataMember]
		public virtual long ID { get; set; }

	    private long _portfolioID;
        /// <summary>
        /// Ссылка на портфель (Portfolio)
        /// </summary>
        [DataMember]
		public virtual long PortfolioID {
            get { return _portfolioID; }
            set
            {
                _portfolioID = value;
                OnPropertyChanged("PortfolioID");
            }
		}

        private long _paymentDetailID;

	    /// <summary>
	    /// Ссылка на назначение платежа (PaymentDetail)
	    /// </summary>
	    [DataMember]
	    public virtual long PaymentDetailID
	    {
            get { return _paymentDetailID; }
            set
            {
                _paymentDetailID = value;
                OnPropertyChanged("PaymentDetailID");
            }
        }

	    private string _reportPaymentDetail;

	    /// <summary>
	    /// Назначение платежа для отчета ЦБ
	    /// </summary>
	    [DataMember]
	    public virtual string ReportPaymentDetail
	    {
	        get { return _reportPaymentDetail; }
	        set
	        {
	            _reportPaymentDetail = value;
	            OnPropertyChanged("ReportPaymentDetail");
	        }
	    }

        /// <summary>
		/// Статус записи
		/// </summary>
		[DataMember]
        public virtual long StatusID { get; set; }


        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "PortfolioID":
                        if (PortfolioID <= 0)
                            return "Поле обязательное для заполнения";
                        break;

                    case "PaymentDetailID":
                        if (PaymentDetailID <= 0)
                            return "Поле обязательное для заполнения";
                        break;

                    case "ReportPaymentDetail":
                        if (ReportPaymentDetail.TrimmedIsNullOrEmpty())
                            return "Поле обязательное для заполнения";
                        if (IsInvalidLength(ReportPaymentDetail, 500))
                            return "Максимальная длина 500 символов";
                        break;
                }

                return null;
            }
        }

        public virtual bool IsValid()
        {
            string[] fieldNames = { "PortfolioID", "PaymentDetailID", "ReportPaymentDetail" };
            return fieldNames.All(fieldName => string.IsNullOrEmpty(this[fieldName]));
        }

        private bool IsInvalidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }
    }
}
