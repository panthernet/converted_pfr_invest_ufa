﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract(IsReference = true)]
    public class OpfrTransferListItem: OpfrTransfer, IDataErrorInfo
    {
        [DataMember]
        public virtual string Kosgu { get; set; }

        [DataMember]
        public virtual string Opfr { get; set; }

        [DataMember]
        public virtual decimal PPSum { get; set; }

        [DataMember]
        public virtual string RegisterName { get; set; }

        public OpfrTransferListItem(OpfrTransfer tr, string kosgu, string opfr, string regnum, DateTime? regDate)
            : this(tr, kosgu, opfr)
        {
            if (!string.IsNullOrEmpty(regnum) || regDate.HasValue)
                RegisterName = $"{tr.OpfrRegisterID} \tдокумент:   \r\n\t№ {regnum} от: {regDate.Value.Date:dd.MM.yyyy}";
        }

        public OpfrTransferListItem(OpfrTransfer tr, string kosgu, string opfr)
        {
            ID = tr.ID;
            StatusID = tr.StatusID;
            Comment = tr.Comment;
            DIDate = tr.DIDate;
            OpfrCostID = tr.OpfrCostID;
            OpfrRegisterID = tr.OpfrRegisterID;
            PfrBranchID = tr.PfrBranchID;
            PortfolioID = tr.PortfolioID;
            Sum = tr.Sum;
            Kosgu = kosgu;
            Opfr = opfr;
            Status = tr.Status;

        }

        string IDataErrorInfo.Error => string.Empty;


        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Sum": return Sum.ValidateMaxFormat();
                    default: return null;
                }
            }
        }

        /// <summary>
        /// TODO 03.11.2016 Lord (Kuleshov M.V.)
        /// может конечно велосипед, но пока не нашел другого способа, как это сделать
        /// </summary>
        /// <returns></returns>
        public bool ValidateFields()
        {
            return ((IDataErrorInfo)this)["Sum"] == null;
        }
    }
}
