﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Threading;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class CommonRegisterListItem:BaseDataObject
	{
		public CommonRegisterListItem() 
		{
			Section = AsgFinTr.Sections.All;
		}

	    public CommonRegisterListItem(long ID, DateTime? date, string regnum) : 
            this(ID, date, regnum, null)
	    {
	        
	    }


        public CommonRegisterListItem(long ID, DateTime? date, string regnum, string approvedoc = null):this()
		{
		    var tmp = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
			this.ID = ID;
			Name = $"{ID} \tдокумент: {approvedoc}     \r\n\t№ {regnum} от: {date?.ToShortDateString() ?? string.Empty}";
		    Thread.CurrentThread.CurrentCulture = tmp;
		}

		[DataMember]
		public long ID { get; set; }

		[DataMember]
		public string Name { get; set; }

		[DataMember]
		public AsgFinTr.Sections Section { get; set; }
	}
}
