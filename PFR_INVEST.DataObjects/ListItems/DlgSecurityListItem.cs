﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DlgSecurityListItem : BaseDataObject
    {
        [DataMember]
        public Security Security { get; set; }
        [DataMember]
        public decimal Summ { get; set; }
    }
}
