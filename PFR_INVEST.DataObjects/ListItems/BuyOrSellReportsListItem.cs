﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class BuyOrSaleReportsListItem
    {
        /// <summary>
        /// ID OrdReport
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Портфель (год)
        /// </summary>
        [DataMember]
        public string PortfolioYear { get; set; }

        /// <summary>
        /// Номер поручения
        /// </summary>
        [DataMember]
        public string RegNum { get; set; }

        /// <summary>
        /// ID ЦБ
        /// </summary>
        [DataMember]
        public long SecurityID { get; set; }

        /// <summary>
        /// ЦБ
        /// </summary>
        [DataMember]
        private string securityName;
        public string SecurityName
        {
            get
            {
                return !string.IsNullOrEmpty(securityName) ? "Всего по " + securityName : "";
            }
            set
            {
                securityName = value;
            }
        }

        /// <summary>
        /// Дата поручения
        /// </summary>
        [DataMember]
        public DateTime? DateOrder { get; set; }

        /// <summary>
        /// Дата операции по счету ДЕПО
        /// </summary>
        [DataMember]
        public DateTime? DateDEPO { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public long Count { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [DataMember]
        public decimal Price { get; set; }

        /// <summary>
        /// SumNom
        /// </summary>
        [DataMember]
        public decimal SumNom { get; set; }

        /// <summary>
        /// Sum
        /// </summary>
        [DataMember]
        public decimal Sum { get; set; }

        /// <summary>
        /// Sum
        /// </summary>
        [DataMember]
        public decimal RubSum { get; set; }

        /// <summary>
        /// SumWithoutNKD
        /// </summary>
        [DataMember]
        public decimal SumWithoutNKD { get; set; }

        /// <summary>
        /// Yield
        /// </summary>
        [DataMember]
        public decimal Yield { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [DataMember]
        public string CurrencyName { get; set; }

        /// <summary>
        /// Тип (покупка/продажа)
        /// </summary>
        [DataMember]
        public string OrderType { get; set; }

        /// <summary>
        /// bool рублевая валюта или нет
        /// </summary>
        [DataMember]
        public bool CurrencyIsRubles { get; set; }

        /// <summary>
        /// Курс валюты на дату поручения/ДЕПО
        /// </summary>
        [DataMember]
        public decimal? Rate { get; set; }

		[DataMember]
		public decimal? RubNKD { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [DataMember]
        public string Comment { get; set; }
        /// <summary>
        /// Дата создания карточки
        /// </summary>
        [DataMember]
        public DateTime? DateDI { get; set; }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            var otherEntity = obj as BuyOrSaleReportsListItem;
            if (otherEntity != null)
            {
                if (otherEntity.Comment == Comment && otherEntity.Count == Count && otherEntity.CurrencyIsRubles == CurrencyIsRubles &&
                    otherEntity.CurrencyName == CurrencyName && otherEntity.ID == ID && otherEntity.Yield == Yield && otherEntity.SumWithoutNKD == SumWithoutNKD &&
                    otherEntity.SumNom == SumNom && otherEntity.Sum == Sum && otherEntity.SecurityName == SecurityName && otherEntity.SecurityID == SecurityID
                    && otherEntity.RegNum == RegNum && otherEntity.Price == Price && otherEntity.PortfolioYear == PortfolioYear && otherEntity.OrderType == OrderType
                    && otherEntity.DateDEPO.Equals(DateDEPO) && otherEntity.Rate.Equals(Rate) && otherEntity.DateDI.Equals(DateDI) && otherEntity.DateOrder.Equals(DateOrder))
                    return true;
                return false;
            }
            return false;
        }
    }
}
