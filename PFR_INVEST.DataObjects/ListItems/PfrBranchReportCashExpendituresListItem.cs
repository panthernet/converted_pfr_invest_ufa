﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportCashExpendituresListItem
    {
        public PfrBranchReportCashExpendituresListItem(PfrBranchReportCashExpenditures ce, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = ce.ID;
            ReportId = ce.BranchReportID;

            Sum_KBK_392_10_06_505_23_01_244_226 = ce.Sum_KBK_392_10_06_505_23_01_244_226;
            Sum_KBK_392_10_06_505_54_02_244_226_by400 = ce.Sum_KBK_392_10_06_505_54_02_244_226_by400;
            Sum_KBK_392_10_06_505_54_02_244_226_by472 = ce.Sum_KBK_392_10_06_505_54_02_244_226_by472;
            
            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }

            ReportFedoName = f.Name;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_23_01_244_226 { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_54_02_244_226_by400 { get; set; }

        [DataMember]
        public virtual decimal Sum_KBK_392_10_06_505_54_02_244_226_by472 { get; set; }

        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }
    }
}
