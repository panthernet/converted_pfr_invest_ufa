﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PFRAccountsListItem: BaseDataObject
    {        

        public PFRAccountsListItem(object[] a)
        {
            if (a == null) return;
            ID = Convert.ToInt64(a[0]);
            State = (string)a[1];
            AccountNumber = (string)a[2];
            Type = (string)a[3];
            BankName = (string)a[4];
            Currency = (string)a[5];
        }

        [DataMember]
        public long ID { get; set; }

		[IgnoreDataMember]
        public string DisplayName
        {
            get { return string.Format("{0} {1} ({2})", BankName, AccountNumber, Type); }
        }

		[DataMember]
        public string BankName { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

		[DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string State { get; set; }
    }
}
