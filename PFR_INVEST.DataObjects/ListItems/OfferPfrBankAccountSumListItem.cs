﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class OfferPfrBankAccountSumListItem : BaseDataObject
    {
        [IgnoreDataMember]
        public bool IsSelected => _selected || _selected2;
        [DataMember]
        private bool _selected;
        [DataMember]
        private bool _selected2;

        [IgnoreDataMember]
        public bool IsSelectedGroup
        {
            get { return _selected; }
            set
            {
                _selected = value;
                Group?.ForEach(a =>
                {
                    a._selected = value;
                    a._selected2 = value;
                    a.OnPropertyChanged(nameof(IsSelectedGroup));
                    a.OnPropertyChanged(nameof(IsSelectedGroup2));
                });
              //  OnPropertyChanged(nameof(IsSelectedGroup));
              //  OnPropertyChanged(nameof(IsSelectedGroup2));
            }
        }

        [IgnoreDataMember]
        public bool IsSelectedGroup2
        {
            get { return _selected2; }
            set
            {
                _selected2 = value;
                Group?.Where(a => a.OfferNumber == OfferNumber).ToList().ForEach(a =>
                {
                    a._selected2 = value;
                    a.OnPropertyChanged(nameof(IsSelectedGroup2));
                });

                //все вторичные в группе
                bool result = Group?.TrueForAll(a => a._selected2) ?? false;
                Group?.ForEach(a =>
                {
                    a._selected = result;
                    a.OnPropertyChanged(nameof(IsSelectedGroup));                    
                });
                //OnPropertyChanged(nameof(IsSelectedGroup));
                //OnPropertyChanged(nameof(IsSelectedGroup2));
            }
        }

        [IgnoreDataMember]
        public List<OfferPfrBankAccountSumListItem> Group { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public DateTime SettleDate { get; set; }

        [DataMember]
        public long OfferNumber { get; set; }

        [DataMember]
        public long AuctionID { get; set; }

        [DataMember]
        public OfferPfrBankAccountSum OfferPfrBankAccountSum { get; set; }

        public OfferPfrBankAccountSumListItem(OfferPfrBankAccountSum offerPfrBankAccountSum, string bankName, DateTime settleDate, long number, long? aucId = null)
        {
            OfferPfrBankAccountSum = offerPfrBankAccountSum;
            IsSelectedGroup = true;
            IsSelectedGroup2 = true;
            BankName = bankName;
            SettleDate = settleDate;
            OfferNumber = number;
            AuctionID = aucId ?? 0;
        }

    }
}
