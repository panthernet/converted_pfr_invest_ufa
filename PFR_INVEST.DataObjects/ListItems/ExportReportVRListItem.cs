﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ExportReportVRListItem
    {
       
        /// <summary>
        /// Формализованное наименомание УК
        /// </summary>
        [DataMember]
        public string FormalizedName { get; set; }

        /// <summary>
        /// Номер договора
        /// </summary>        
        [DataMember]
        public string ContractNumber { get; set; }

        /// <summary>
        /// Название Контракта
        /// </summary>
        [DataMember]
        public string ContractName { get; set; } 
        
         /// <summary>
        /// Сумма
        /// </summary>
        [DataMember]
        public decimal? Sum { get; set; } 
        
    
    }
}
