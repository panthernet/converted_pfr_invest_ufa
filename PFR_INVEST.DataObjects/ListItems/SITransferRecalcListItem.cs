﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SITransferRecalcListItem: BaseDataObject
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long? ZLCount { get; set; }

        [DataMember]
        public long? StartZLCount { get; set; }

        [DataMember]
        public string SPNDirectionName { get; set; }

        public SITransferRecalcListItem(object[] a)
        {
            if (a == null) return;
            ID = Convert.ToInt64(a[0]);
            ZLCount = Convert.ToInt64(a[1]);
            StartZLCount = Convert.ToInt64(a[2]);
            SPNDirectionName = a[3].ToString();
        }
    }
}
