﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class ReqTransferListItem : ReqTransfer
	{
		public ReqTransferListItem() { }
		public ReqTransferListItem(ReqTransfer transfer)
			: this()
		{
			transfer.CopyTo(this);
		}

	    public ReqTransferListItem(long id, string name)
	    {
	        ID = id;
	        UKName = name;
	    }

		public ReqTransferListItem(ReqTransfer transfer, string contractNumber, string ukName, long directionId = 0, long ppCount = 0)
			: this(transfer)
		{
			ContactNumber = contractNumber;
			UKName = ukName;
		    HasPP = ppCount > 0;

			_isFromUk = directionId == (long)SIRegister.Directions.FromUK || directionId == (long)SIRegister.Directions.FromGUK;
		}

        public ReqTransferListItem(ReqTransfer transfer, string contractNumber, string ukName, long directionId = 0)
            : this(transfer)
        {
            ContactNumber = contractNumber;
            UKName = ukName;

            _isFromUk = directionId == (long)SIRegister.Directions.FromUK || directionId == (long)SIRegister.Directions.FromGUK;
        }

        public ReqTransferListItem(ReqTransfer transfer, string ukName, string contractNumber, DateTime? contractDate)
            : this(transfer)
        {
            ContactNumber = contractNumber;
            UKName = ukName;
            ContractDate = contractDate;
        }

        [DataMember]
        public DateTime? ContractDate { get; private set; }


		[DataMember]
		public string ContactNumber { get; private set; }

		/// <summary>
		/// Наименование УК
		/// </summary>
		[DataMember]
		public string UKName { get; set; }

		[DataMember]
		private readonly bool _isFromUk;

        [DataMember]
	    public bool HasPP { get; set; }

        /// <summary>
        /// Поле для биндинга в списке выбора перечисления
        /// </summary>
        [IgnoreDataMember]
        public bool? HasPP2 { get { return HasPP; } }


	    /// <summary>
		/// Сумма СПН, зависит от направления СПН
		/// </summary>
		public decimal? SpnSum
		{
			get
			{
				if (!_isFromUk)
					return Sum;
			    // Если сумма инвестдохода отрицательная Сумма СПН должна быть равна значению Сумма.
			    return (InvestmentIncome ?? 0) < 0 ? Sum : (Sum ?? 0) - (InvestmentIncome ?? 0);
			}
		}
	}
}
