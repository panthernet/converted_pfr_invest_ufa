﻿using System;
using PFR_INVEST.Constants.Identifiers;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class RegisterListItem2
    {

        /// <summary>
        /// ID данного финреестра
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID реестра
        /// </summary>
        [DataMember]
        public long RegisterID { get; set; }
        private string _registerKind;
        /// <summary>
        /// Вид реестра
        /// </summary>
        [DataMember]
        public string RegisterKind 
        {
            get
            {
                return RegisterIdentifier.IsFromNPF(this._registerKind) ? RegisterIdentifier.NPFtoPFR :
                    RegisterIdentifier.IsToNPF(this._registerKind) ? RegisterIdentifier.PFRtoNPF :
                    RegisterIdentifier.IsTempReturn(this._registerKind) ? RegisterIdentifier.TempReturn :
                    RegisterIdentifier.IsTempAllocation(this._registerKind) ? RegisterIdentifier.TempAllocation :
                    null;
            }
            set { _registerKind = value; }
        }

        /// <summary>
        /// Содержание финреестра
        /// </summary>        
        [DataMember]
        public string Content { get; set; }

        /// <summary>
        /// Наименование контрагента
        /// </summary>
        [DataMember]
        public string ContragentName { get; set; }

        /// <summary>
        /// Сумма в финреестре
        /// </summary>
        [DataMember]
        public decimal Count { get; set; }

        /// <summary>
        /// Количество застрахованных лиц
        /// </summary>
        [DataMember]
        public long? ZLCount { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        [DataMember]
        public string FinregRegNum { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? FinregDate { get; set; }

        /// <summary>
        /// Номер
        /// </summary>
        [DataMember]
        public string RegNum { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? RegDate { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [DataMember]
        public string Status { get; set; }
    }
}
