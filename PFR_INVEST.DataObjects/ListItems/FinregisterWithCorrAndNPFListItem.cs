﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class FinregisterWithCorrAndNPFListItem : IDataErrorInfo
	{
		/// <summary>
		/// ID финреестра
		/// </summary>
		[DataMember]
		public long? ID { get; set; }

		/// <summary>
		/// сущность финреестра
		/// </summary>
		[DataMember]
		public Finregister Finregister { get; set; }

		/// <summary>
		/// наименование НПФ
		/// </summary>
		[DataMember]
		public string NPFName { get; set; }

		/// <summary>
		/// статус НПФ
		/// </summary>
		[DataMember]
		public string NPFStatus { get; set; }

		/// <summary>
		/// количество ЗЛ
		/// </summary>
		public long? zlcount = null;
		[DataMember]
		public long? ZLCount
		{
			get { return this.zlcount; }
			set
			{
				if (this.zlcount != value)
				{
					this.zlcount = value;
					if (this.Finregister != null)
					{
						this.Finregister.ZLCount = value;
						this.IsItemChanged = true;
						ItemChanged();
					}
				}
			}
		}


		/// <summary>
		/// сумма финреестра
		/// </summary>
		public decimal? count = null;
		[DataMember]
		public decimal? Count
		{
			get
			{
				return this.count;
			}

			set
			{
				this.count = value;
				if (this.Finregister != null)
				{
					this.Finregister.Count = value;
					this.IsItemChanged = true;
					ItemChanged();
				}
			}
		}

		/// <summary>
		/// сумма с учетом корректировки
		/// </summary>
		[DataMember]
		public decimal? CorrCount { get; set; }

		public decimal? cfrcount = null;
		[DataMember]
		public decimal? CFRCount
		{
			get
			{
				return this.cfrcount;
			}
			set
			{
				if (this.cfrcount != value)
				{
					this.cfrcount = value;
					if (this.Finregister != null)
					{
						this.Finregister.CFRCount = value;
						this.IsItemChanged = true;
						ItemChanged();
					}
				}
			}
		}

		public bool IsItemChanged { get; set; }

		public delegate void ItemChangedDelegate();
		public event ItemChangedDelegate OnItemChanged;
		public void ItemChanged()
		{
			if (OnItemChanged != null)
			{
				OnItemChanged();
			}
		}



		string IDataErrorInfo.Error
		{
			get { return string.Empty; }
		}

		string IDataErrorInfo.this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "Count": return Count.ValidateMaxFormat();
					case "CFRCount": return CFRCount.ValidateMaxFormat();
					case "CorrCount": return CorrCount.ValidateMaxFormat();
					default: return null;
				}
			}
		}
	}
}
