﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class CBRApprovalRequest: BaseDataObject
    {
        [DataMember]
        public virtual int Year { get; set; }
        [DataMember]
        public virtual int Quarter { get; set; }
        [DataMember]
        public virtual string Okud { get; set; }
        [DataMember]
        public virtual string Username1 { get; set; }
        [DataMember]
        public virtual string Login1 { get; set; }
        [DataMember]
        public virtual string Username2UK { get; set; }
        [DataMember]
        public virtual string Login2UK { get; set; }
        [DataMember]
        public virtual string Username2NPF { get; set; }
        [DataMember]
        public virtual string Login2NPF { get; set; }
        [DataMember]
        public virtual string Username3 { get; set; }
        [DataMember]
        public virtual string Login3 { get; set; }
        [DataMember]
        public virtual BOReportForm1.ReportTypeEnum ReportType { get; set; }
    }
}
