﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DopSPNListItem
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public decimal MSumm { get; set; }

        [DataMember]
        public string DocKind { get; set; }

        public bool IsKDoc
        {
            get
            {
                return string.Compare(DocKind, DueDocKindIdentifier.Treasurity) == 0;
            }
        }

        [DataMember]
        public decimal Diff { get; set; }
    }
}
