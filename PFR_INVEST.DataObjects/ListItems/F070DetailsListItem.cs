﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class F070DetailsListItem : BaseDataObject
	{
		public F070DetailsListItem() { }
		public F070DetailsListItem(EdoOdkF070 F070, Year year, Contract contract, LegalEntity le, long? lContractNumbers)
		{
			this.F070 = F070;
			this.year = year;
			this.le = le;
			this.contract = contract;
            this.lContractNumbers = lContractNumbers;
		}

        [DataMember]
        public virtual Contract contract { get; set; }
		[DataMember]
		public virtual Year year { get; set; }
		[DataMember]
		public virtual LegalEntity le { get; set; }

		[DataMember]
		public virtual EdoOdkF070 F070 { get; set; }
    
        [IgnoreDataMember]
        public virtual long ID { get { return this.F070 == null ? 0 : this.F070.ID; } }
        [IgnoreDataMember]
        public virtual string Year { get { return this.year == null ? null : this.year.Name; } }
        [IgnoreDataMember]
        public virtual string Quarter { get { return this.F070 == null ? null : string.Format("{0}\t({1})", this.F070.Quartal.HasValue ? this.F070.Quartal.Value.ToString() : string.Empty, this.lContractNumbers.HasValue ? this.lContractNumbers.Value.ToString() : string.Empty); } }
        [IgnoreDataMember]
        public virtual string Name { get { return this.le == null ? null : this.le.FormalizedName; } }
        [IgnoreDataMember]
        public virtual string RegNum { get { return this.F070 == null ? null : this.F070.RegNumber; } }
        [IgnoreDataMember]
        public virtual string ContractNumber { get { return this.contract == null ? null : this.contract.ContractNumber; } }
        [IgnoreDataMember]
        public virtual string Profit { get { return this.F070 == null ? null : this.F070.Profit1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string Retained { get { return this.F070 == null ? null : this.F070.Retained1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string UKReward { get { return this.F070 == null ? null : this.F070.UKReward1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual long? lContractNumbers { get; set; }
    }
}
