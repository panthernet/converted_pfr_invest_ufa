﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class BankListItem
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string ShortName { get; set; }

		[DataMember]
		public virtual string FormalizedName { get; set; }

        [DataMember]
        public virtual string LegalAddress { get; set; }

        [DataMember]
        public virtual string Phone { get; set; }

        [DataMember]
        public virtual string RegistrationNum { get; set; }

        [IgnoreDataMember]
        public virtual long? RegistrationNumInt
        {
            get
            {
                long v;
                if (long.TryParse(RegistrationNum, out v))
                    return v;
                return null;
            }
        }

        //данные, необходимые для вычисления активности банка

        [DataMember]
        public virtual string PFRAGRSTAT { get; set; }

        [DataMember]
        public virtual decimal? Money { get; set; }

        [IgnoreDataMember]
        public LegalEntity Entity { get { return ToLeForActiveCheck(); } }

        /// <summary>
        /// Создает LE с минимальным набором данных, необходимым на проверку активности банка
        /// </summary>
        private LegalEntity ToLeForActiveCheck()
        {
            return new LegalEntity
            {
                Money = Money,
                PFRAGRSTAT = PFRAGRSTAT
            };
        }
    }
}