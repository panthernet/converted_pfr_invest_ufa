﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class PPTransferListItem : ReqTransfer
	{
		public PPTransferListItem() { }

		//[DataMember]
		//public ReqTransfer ReqTransfer { get; set; }

		[DataMember]
		public string CommonPPNum { get; set; }

		[DataMember]
		public DateTime? CommonPPDate { get; set; }
	}
}
