﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects.Interfaces;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class CorrespondenceListItemNew : IIdentifiable
	{
		[DataMember]
		public long ID { get; set; }

		[DataMember]
		public string Sender { get; set; }

		[DataMember]
		public int? Year { get; set; }

		[DataMember]
		public string Month { get; set; }

		[DataMember]
		public DateTime? IncomingDate { get; set; }

		[DataMember]
		public string IncomingNumber { get; set; }

		[DataMember]
		public DateTime? OutgoingDate { get; set; }

		[DataMember]
		public string OutgoingNumber { get; set; }

		[DataMember]
		public string DocumentClass { get; set; }

		[DataMember]
		public string Executor { get; set; }

		[DataMember]
		public string AdditionalDocumentInfo { get; set; }

		[DataMember]
		public DateTime? ControlDate { get; set; }

		[DataMember]
		public DateTime? ExecutionDate { get; set; }

		[DataMember]
		public bool IsExecutionExpired { get; set; }

		[DataMember]
		public bool IsAttach { get; set; }
	}

	public class CorrespondenceListItemNewInfo : CorrespondenceListItemNew
	{
		public long? AttachId { get; set; }

		public long? AttachClassificID { get; set; }

		public string AttachAdditional { get; set; }

		public DateTime? AttachControlDate { get; set; }

		public DateTime? AttachExecutionDate { get; set; }

		public string AttachExecutorName { get; set; }

		public string AttachClassific { get; set; }
	}
}
