﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class NPFforERZLNotifyListItem : INotifyPropertyChanged
	{
        [DataMember]
	    public string ID { get; set; }

        [DataMember]
		public Contragent Contragent { get; set; }

		[DataMember]
		public LegalEntity LegalEntity { get; set; }

		[DataMember]
		public string NPFName
		{
			get
			{
			    if (LegalEntity != null)
					return LegalEntity.FormalizedNameFull;
			    if (Contragent != null)
			        return Contragent.Name;
			    return string.Empty;
			}

			set { }
		}

		private long? count;

        [DataMember]
        public long? DefaultCount { get; set; }

		[DataMember]
		public long? Count
		{
			get { return count; }

			set
			{
			    if (DefaultCount == null)
			        DefaultCount = value ?? 0;

				if (count != value)
				{
					count = value;
					OnPropertyChanged("Count");
					OnPropertyChanged("IsMarked");
				}
			}
		}

		[DataMember]
		public long? ContragentZLAccountID { get; set; }

		private ERZLNotify erzlNotify;

		[DataMember]
		public ERZLNotify ERZLNotify
		{
			get
			{
				return erzlNotify;
			}

			set
			{
				erzlNotify = value;
				OnPropertyChanged("ERZLNotify");
			}
		}

		[DataMember]
		public bool IsMarked
		{
			get
			{
				return (ExistERZLNotifyCount != null && Count != ExistERZLNotifyCount) ||
					(ExistERZLNotifyCount == null && Count != null);
			}

			set { }
		}

		private long? existERZLNotifyCount;

		[DataMember]
		public long? ExistERZLNotifyCount
		{
			get
			{
				return existERZLNotifyCount;
			}

			set
			{
				existERZLNotifyCount = value;
				OnPropertyChanged("ExistERZLNotifyCount");
			}
		}

		public NPFforERZLNotifyListItem Clone()
		{
			return new NPFforERZLNotifyListItem
			{
				Contragent = Contragent,
				LegalEntity = LegalEntity,
				Count = Count,
				ERZLNotify = ERZLNotify,
				ExistERZLNotifyCount = ExistERZLNotifyCount
			};
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
