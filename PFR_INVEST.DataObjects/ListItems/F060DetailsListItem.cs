﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class F060DetailsListItem : BaseDataObject
	{
		public F060DetailsListItem() { }
		public F060DetailsListItem(EdoOdkF060 F060, Year year, Contract contract, LegalEntity le, long? lContractNumbers)
		{
			this.F060 = F060;
			this.year = year;
			this.le = le;
			this.contract = contract;
            this.lContractNumbers = lContractNumbers;
		}

		[DataMember]
		public virtual Contract contract { get; set; }
		[DataMember]
		public virtual Year year { get; set; }
		[DataMember]
		public virtual LegalEntity le { get; set; }

		[DataMember]
		public virtual EdoOdkF060 F060 { get; set; }
    
        [IgnoreDataMember]
        public virtual long ID { get { return this.F060 == null ? 0 : this.F060.ID; } }
        [IgnoreDataMember]
        public virtual string Year { get { return this.year == null ? null : this.year.Name; } }
        [IgnoreDataMember]
        public virtual string Quarter { get { return this.F060 == null ? null : this.F060.Quartal.HasValue ? string.Format("{0}\t({1})", this.F060.Quartal.Value.ToString(), this.lContractNumbers.HasValue ? this.lContractNumbers.Value.ToString() : string.Empty) : string.Empty; } }
        [IgnoreDataMember]
        public virtual string Name { get { return this.le == null ? null : this.le.FormalizedName; } }
        [IgnoreDataMember]
        public virtual string RegNum { get { return this.F060 == null ? null : this.F060.RegNumber; } }
        [IgnoreDataMember]
        public virtual string ContractNumber { get { return this.contract == null ? null : this.contract.ContractNumber; } }
        [IgnoreDataMember]
        public virtual string SCHAStart1 { get { return this.F060 == null ? null : this.F060.SchaStart1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string PaymentReceived1 { get { return this.F060 == null ? null : this.F060.PaymentReceived1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string PaymentTransfer1 { get { return this.F060 == null ? null : this.F060.PaymentTransfer1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string Profit1 { get { return this.F060 == null ? null : this.F060.Profit1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string Detained1 { get { return this.F060 == null ? null : (this.F060.Detained1.Value + this.F060.DetainedUKReward1.Value).ToString("N2"); } }
        [IgnoreDataMember]
        public virtual string SCHAEnd1 { get { return this.F060 == null ? null : this.F060.SchaEnd1.Value.ToString("N2"); } }
        [IgnoreDataMember]
        public virtual long? lContractNumbers { get; set; }
    }
}
