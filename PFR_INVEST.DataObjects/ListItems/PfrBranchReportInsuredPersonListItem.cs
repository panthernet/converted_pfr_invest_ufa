﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportInsuredPersonListItem
    {
        const string NO_DATA = "Нет данных";

        public PfrBranchReportInsuredPersonListItem(PfrBranchReportInsuredPerson ip, PfrBranchReportInsuredPersonType t, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = ip.ID;
            ReportId = ip.ReportId;
            TypeId = ip.TypeId;

            Summary = ip.Summary;
            TerritoryPersonalAppealSum = ip.TerritoryPersonalAppealSum;
            TerritoryPersonalAppealGroup1 = ip.TerritoryPersonalAppealGroup1;
            TerritoryPersonalAppealGroup2 = ip.TerritoryPersonalAppealGroup2;
            TerritoryPersonalAppealGroup3 = ip.TerritoryPersonalAppealGroup3;
            TerritoryOtherSum = ip.TerritoryOtherSum;
            TerritoryOtherGroup1 = ip.TerritoryOtherGroup1;
            TerritoryOtherGroup2 = ip.TerritoryOtherGroup2;
            TerritoryOtherGroup3 = ip.TerritoryOtherGroup3;
            Gosuslugi = ip.Gosuslugi;
            Mfc = ip.Mfc;
            Agency = ip.Agency;

            // Обрабатываем все -1. Ставим null
            if (Summary < 0) Summary = null;
            if (TerritoryPersonalAppealSum < 0) TerritoryPersonalAppealSum = null;
            if (TerritoryPersonalAppealGroup1 < 0) TerritoryPersonalAppealGroup1 = null;
            if (TerritoryPersonalAppealGroup2 < 0) TerritoryPersonalAppealGroup2 = null;
            if (TerritoryPersonalAppealGroup3 < 0) TerritoryPersonalAppealGroup3 = null;
            if (TerritoryOtherSum < 0) TerritoryOtherSum = null;
            if (TerritoryOtherGroup1 < 0) TerritoryOtherGroup1 = null;
            if (TerritoryOtherGroup2 < 0) TerritoryOtherGroup2 = null;
            if (TerritoryOtherGroup3 < 0) TerritoryOtherGroup3 = null;
            if (Gosuslugi < 0) Gosuslugi = null;
            if (Mfc < 0) Mfc = null;
            if (Agency < 0) Agency = null;

            TypeName = t.Name;

            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }

            ReportFedoName = f.Name;
        }

        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }

        [DataMember]
        public long TypeId { get; set; }

        [DataMember]
        public long? Summary { get; set; }

        //public string SummaryText
        //{
        //    get { return Summary < 0 ? NO_DATA : Summary.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryPersonalAppealSum { get; set; }

        //public string TerritoryPersonalAppealSumText
        //{
        //    get { return TerritoryPersonalAppealSum < 0 ? NO_DATA : TerritoryPersonalAppealSum.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryPersonalAppealGroup1 { get; set; }

        //public string TerritoryPersonalAppealGroup1Text
        //{
        //    get { return TerritoryPersonalAppealGroup1 < 0 ? NO_DATA : TerritoryPersonalAppealGroup1.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryPersonalAppealGroup2 { get; set; }

        //public string TerritoryPersonalAppealGroup2Text
        //{
        //    get { return TerritoryPersonalAppealGroup2 < 0 ? NO_DATA : TerritoryPersonalAppealGroup2.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryPersonalAppealGroup3 { get; set; }

        //public string TerritoryPersonalAppealGroup3Text
        //{
        //    get { return TerritoryPersonalAppealGroup3 < 0 ? NO_DATA : TerritoryPersonalAppealGroup3.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryOtherSum { get; set; }

        //public string TerritoryOtherSumText
        //{
        //    get { return TerritoryOtherSum < 0 ? NO_DATA : TerritoryOtherSum.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryOtherGroup1 { get; set; }

        //public string TerritoryOtherGroup1Text
        //{
        //    get { return TerritoryOtherGroup1 < 0 ? NO_DATA : TerritoryOtherGroup1.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryOtherGroup2 { get; set; }

        //public string TerritoryOtherGroup2Text
        //{
        //    get { return TerritoryOtherGroup2 < 0 ? NO_DATA : TerritoryOtherGroup2.ToString("N0"); }
        //}

        [DataMember]
        public long? TerritoryOtherGroup3 { get; set; }

        //public string TerritoryOtherGroup3Text
        //{
        //    get { return TerritoryOtherGroup3 < 0 ? NO_DATA : TerritoryOtherGroup3.ToString("N0"); }
        //}

        [DataMember]
        public long? Gosuslugi { get; set; }

        //public string GosuslugiText
        //{
        //    get { return Gosuslugi < 0 ? NO_DATA : Gosuslugi.ToString("N0"); }
        //}

        [DataMember]
        public long? Mfc { get; set; }

        //public string MfcText
        //{
        //    get { return Mfc < 0 ? NO_DATA : Mfc.ToString("N0"); }
        //}

        [DataMember]
        public long? Agency { get; set; }
        //public string AgencyText
        //{
        //    get { return Agency < 0 ? NO_DATA : Agency.ToString("N0"); }
        //}

        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }

        [DataMember]
        public string TypeName { get; set; }
    }
}
