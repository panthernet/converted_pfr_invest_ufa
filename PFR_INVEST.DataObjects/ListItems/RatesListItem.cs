﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class RatesListItem
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public int Year { get; set; }

        [DataMember]
        public string Month { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public decimal? Rate { get; set; }
    }
}
