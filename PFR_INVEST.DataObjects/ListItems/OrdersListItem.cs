﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class OrdersListItem
    {
        /// <summary>
        /// ID CbInOrder 
        /// </summary>
        [DataMember]
        public long? ID { get; set; }

        /// <summary>
        /// ID поручения
        /// </summary>
        [DataMember]
        public long? OrderID { get; set; }

        /// <summary>
        /// Портфель (год)
        /// </summary>
        [DataMember]
        public string PortfolioYear { get; set; }

        /// <summary>
        /// Дата поручения
        /// </summary>
        [DataMember]
        public DateTime? RegDate { get; set; }

        /// <summary>
        /// Год и месяц (из RegDate)
        /// </summary>
        [DataMember]
        public DateTime? MonthYear { get; set; }

        /// <summary>
        /// Статус поручения
        /// </summary>
        [DataMember]
        public string Status { get; set; }

        /// <summary>
        /// Номер поручения
        /// </summary>
        [DataMember]
        private string regnum;
        public string RegNum 
        {
            get
            {
                return regnum + " (" + Status + ")"; 
            }

            set
            {
                regnum = value;
            }
        }

        /// <summary>
        /// Исполнить поручение по
        /// </summary>
        [DataMember]
        public DateTime? Term { get; set; }

        /// <summary>
        /// Сумма неконкурентной заявки поручения
        /// </summary>
        [DataMember]
        public decimal? NoncompReq { get; set; }

        /// <summary>
        /// Выпуск цб
        /// </summary>
        [DataMember]
        public string Issue { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        [DataMember]
        public long? Count { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        [DataMember]
        public decimal? Sum { get; set; }

        /// <summary>
        /// Общая стоимость
        /// </summary>
        [DataMember]
        public decimal? Value { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [DataMember]
        public decimal? Price { get; set; }

        /// <summary>
        /// Тип поручения
        /// </summary>
        [DataMember]
        public string Type { get; set; }

        /// <summary>
        /// Место покупки по поручению
        /// </summary>
        [DataMember]
        public string Place { get; set; }


        /// <summary>
        /// Признак что к order не привязана ни одна security
        /// </summary>
        [DataMember]
        public bool IsSecurityListEmpty { get; set; }


		[DataMember]
		public bool IsReadOnly { get; set; }

    }
}
