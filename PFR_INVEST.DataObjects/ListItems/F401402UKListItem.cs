﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    using System.Globalization;

    [DataContract]
    public class F401402UKListItem
    {
        /// <summary>
        /// ID данного F401402UK
        /// </summary>
        [DataMember]
        public virtual long ID { get; set; }

        /// <summary>
        /// ID данного EDO_ODKF401402
        /// </summary>
        [DataMember]
        public virtual long EdoID { get; set; }

		[DataMember]
		public virtual string DogSDNum { get; set; }

        [DataMember]
        public virtual string UKName { get; set; }

        [DataMember]
        public virtual string ContractNumber { get; set; }

        [DataMember]
        public virtual decimal? Itogo { get; set; }

        [DataMember]
        public virtual decimal? PayFromTransh { get; set; }

        [DataMember]
        public virtual DateTime? FromDate { get; set; }

        [DataMember]
        public virtual string StatusName { get; set; }

        [DataMember]
        public virtual long? StatusId { get; set; }

        [DataMember]
        public virtual string ControlRegnum { get; set; }

        [DataMember]
        public virtual DateTime? ControlGetDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlSetDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlFactDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlSendDate { get; set; }

        [IgnoreDataMember]
        public virtual decimal Diff
        {
            get
            {
                return (Itogo ?? 0) - (PayFromTransh ?? 0);
            }
        }

        [IgnoreDataMember]
        public virtual int? Year
        {
            get
            {
                return this.FromDate.HasValue ? FromDate.Value.Year : (int?)null;
            }
        }

        [IgnoreDataMember]
        public virtual string Month
        {
            get
            {
                return FromDate.HasValue ? FromDate.Value.ToString("MMMM", CultureInfo.CreateSpecificCulture("ru-RU")) : null;
            }
        }

        [IgnoreDataMember]
        public virtual string MonthWithSubitemsCount
        {
            get;
            set;
        }
    }
}
