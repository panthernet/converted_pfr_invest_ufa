﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class PfrBranchReportApplication741ListItem
    {
        public PfrBranchReportApplication741ListItem(PfrBranchReportApplication741 ap, PfrBranchReport r, PFRBranch b, FEDO f)
        {
            ID = ap.ID;
            ReportId = ap.ReportId;

            AppealDistribution = ap.AppealDistribution;
            AppealPaymentSummary = ap.AppealPaymentSummary;
            AppealPaymentCourt = ap.AppealPaymentCourt;
            AppealRefuse = ap.AppealRefuse;
            ResolutionPayment = ap.ResolutionPayment;
            ResolutionPaymentAdditional = ap.ResolutionPaymentAdditional;
            ResolutionPaymentRefuse = ap.ResolutionPaymentRefuse;

            ReportRegionName = b.Name;
            ReportYear = r.PeriodYear;
            ReportMonth = "";
            if (r.PeriodMonth != null)
            {
                ReportMonth = DateTools.GetMonthInWordsForDate(r.PeriodMonth);
            }

            ReportFedoName = f.Name;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public long ReportId { get; set; }

        [DataMember]
        public virtual long AppealDistribution { get; set; }

        [DataMember]
        public virtual long AppealPaymentSummary { get; set; }

        [DataMember]
        public virtual long AppealPaymentCourt { get; set; }

        [DataMember]
        public virtual long AppealRefuse { get; set; }

        [DataMember]
        public virtual long ResolutionPayment { get; set; }

        [DataMember]
        public virtual long ResolutionPaymentAdditional { get; set; }

        [DataMember]
        public virtual long ResolutionPaymentRefuse { get; set; }

        [DataMember]
        public string ReportMonth { get; set; }

        [DataMember]
        public int ReportYear { get; set; }

        [DataMember]
        public string ReportRegionName { get; set; }

        [DataMember]
        public string ReportFedoName { get; set; }
    }
}
