﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class TransferListItem
	{
		[DataContract]
		public enum ExecutionControlType : int
		{
			[EnumMember]
			ExecutionControlTypeNone = 0,
			[EnumMember]
			ExecutionControlTypeExecuted,
			[EnumMember]
			ExecutionControlTypeNotExecuted
		}

		/// <summary>
		/// ID реестра перечисления
		/// </summary>
		[DataMember]
		public long RegisterID { get; set; }

		[DataMember]
		public long? RegisterOperationID { get; set; }

        [DataMember]
        public long? RegisterOperationTypeID { get; set; }

		[DataMember]
		public long RegisterDirectionID { get; set; }

		[DataMember]
		public long RegisterCompanyMonthID { get; set; }

		[DataMember]
		public long RegisterCompanyYearID { get; set; }

		[DataMember]
		public long ContractStatusID { get; set; }
		/// <summary>
		/// Дата реестра перечисления
		/// </summary>
		[DataMember]
		public DateTime? RegisterDate { get; set; }

		/// <summary>
		/// ID списка/плана перечисления
		/// </summary>
		[DataMember]
		public long TransferID { get; set; }

		/// <summary>
		/// ID перечисления
		/// </summary>
		[DataMember]
		public long ReqTransferID { get; set; }

		/// <summary>
		/// Содержание операции
		/// </summary>
		[DataMember]
		public string Operation { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        [DataMember]
        public string OperationType { get; set; }


		/// <summary>
		/// Направление операции
		/// </summary>
		[DataMember]
		public string Direction { get; set; }

		/// <summary>
		/// Вид перечисления
		/// </summary>
		[DataMember]
		public string Kind { get; set; }

		/// <summary>
		/// Номер реестра перечислений
		/// </summary>
		[DataMember]
		public string RegisterNumber { get; set; }

		/// <summary>
		/// Статус перечисления
		/// </summary>
		[DataMember]
		public string Status { get; set; }

		/// <summary>
		/// Контроль исполнения
		/// </summary>
        [DataMember]
        public ExecutionControlType ExecutionControl { get; set; }

		/// <summary>
		/// Формализованное наименование управляющей компании
		/// </summary>
		[DataMember]
		public string UKFormalizedName { get; set; }

		/// <summary>
		/// Номер договора
		/// </summary>
		[DataMember]
		public string ContractNumber { get; set; }

		/// <summary>
		/// Идентификатор договора
		/// </summary>
		[DataMember]
		public long ContractId { get; set; }

		/// <summary>
		/// Дата договора
		/// </summary>
		[DataMember]
		public DateTime? ContractDate { get; set; }

		/// <summary>
		/// Количество застрахованных лиц
		/// </summary>
		[DataMember]
		public long ZLCount { get; set; }

		/// <summary>
		/// Сумма до операции
		/// </summary>
		[DataMember]
		public decimal SumBeforeOperation { get; set; }

		/// <summary>
		/// Сумма, перешедшая из ПФР в УК
		/// </summary>
		[DataMember]
		public decimal SumFromPFRToUK { get; set; }

		/// <summary>
		/// Сумма, перешедшая из УК в ПФР
		/// </summary>
		[DataMember]
		public decimal SumFromUKtoPFR { get; set; }

		/// <summary>
		/// Сумма после операции
		/// </summary>
		[DataMember]
		public decimal SumAfterOperation
		{
			get
			{
			    if (TransferDirectionIdentifier.IsFromUKToPFR(Direction))
					return SumBeforeOperation - SumFromUKtoPFR + InvestmentIncome;
			    return SumBeforeOperation + SumFromPFRToUK;
			}
			private set { }
		}

		/// <summary>
		/// Доход от инвестирования
		/// </summary>
		[DataMember]
		public decimal InvestmentIncome { get; set; }


		[IgnoreDataMember]
		public decimal SumFromUKSPN
		{
			get
			{
			    //1. Если значение поля Инвестдоход>=0, то
				//"СПН из УК"="Сумма из УК всего"-"Инвестдоход"
				//2. Если значение поля Инвестдоход<0, то
				//"СПН из УК"="Сумма из УК всего"
				if (InvestmentIncome >= 0)
					return SumFromUKtoPFR - InvestmentIncome;
			    return SumFromUKtoPFR;
			}
		}

		/// <summary>
		/// Компания по перечислениям
		/// </summary>
		[DataMember]
		public string Company { get; set; }

		/// <summary>
		/// Год перечисления
		/// </summary>
		[IgnoreDataMember]
		public string TransferDateYear => TransferDate?.Year.ToString() ?? ActDateYear;

	    /// <summary>
		/// Номер акта
		/// </summary>
		[DataMember]
		public string ActNum { get; set; }

		/// <summary>
		/// Дата акта
		/// </summary>
		[DataMember]
		public DateTime? ActDate { get; set; }

		/// <summary>
		/// Дата перечисления СПН
		/// </summary>
		[DataMember]
		public DateTime? TransferDate { get; set; }

		/// <summary>
		/// Год перечисления СПН
		/// </summary>
		[DataMember]
		public string ActDateYear { get; set; }

		/// <summary>
		/// Номер платежного поручения
		/// </summary>
		[DataMember]
		public string PaymentOrderNumber { get; set; }

		/// <summary>
		/// Дата платежного поручения
		/// </summary>
		[DataMember]
		public DateTime? PaymentOrderDate { get; set; }

		/// <summary>
		/// ID месяца в компании по перечислениям. Используется для сортировки в гриде.
		/// Валиден только для перечислений по выплатам правопреемников.
		/// </summary>
		[DataMember]
		public long MonthID { get; set; }

		/// <summary>
		/// Месяц в плане перечислений - валиден только для перечисления по выплатам правопреемников
		/// </summary>
		[DataMember]
		public string Month { get; set; }

		/// <summary>
		/// Плановая сумма перечисления - валидна только для перечисления по выплатам правопреемников
		/// </summary>
		[DataMember]
		public decimal PlanSum { get; set; }

		/// <summary>
		/// Фактическая сумма перечисления - валидна только для перечисления по выплатам правопреемников
		/// </summary>
		[DataMember]
		public decimal FactSum { get; set; }

		/// <summary>
		/// Отклонение - разница Плановая сумма- Фактическая сумма. Валидна только для перечисления по выплатам правопреемников 
		/// </summary>
		private decimal _mDiff;
		private bool _mDiffCalculated;
		[DataMember]
		public decimal Diff
		{
			get
			{
				if (!_mDiffCalculated)
				{
					if (FactSum != 0)
						_mDiff = PlanSum - FactSum;
					else
						_mDiff = 0;
					_mDiffCalculated = true;
				}
				return _mDiff;
			}
			private set { }
		}


		/// <summary>
		/// Временное решение для Мастер-Детейл грида
		/// </summary>
		[IgnoreDataMember]
		public virtual List<TransferListItem> Details { get; set; }

		/// <summary>
		/// Временное решение для Мастер-Детейл грида
		/// </summary>
		[DataMember]
		public virtual decimal? TotalToUK { get; set; }

		/// <summary>
		/// Временное решение для Мастер-Детейл грида
		/// </summary>
		[DataMember]
		public virtual decimal? TotalFromUK { get; set; }

		/// <summary>
		/// Временное решение для Мастер-Детейл грида
		/// </summary>
		[DataMember]
		public virtual decimal? TotalInvestdohod { get; set; }


		/// <summary>
		/// Временное решение для Мастер-Детейл грида
		/// </summary>
		[IgnoreDataMember]
		public virtual decimal? TotalFromUKSPN => TotalFromUK - TotalInvestdohod;

	    [DataMember]
        public long? UkPlanID { get; set; }
	}
}
