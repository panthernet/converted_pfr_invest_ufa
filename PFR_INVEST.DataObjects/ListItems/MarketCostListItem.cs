﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class MarketCostListItem : BaseDataObject
	{
		public MarketCostListItem() { }
		public MarketCostListItem(long ID, DateTime? ReportOnDate, decimal? MarketCost)
			: this(ID, ReportOnDate, MarketCost, null, null, null) { }
		public MarketCostListItem(long ID, DateTime? ReportOnDate, decimal? MarketCost, string ContractNumber, string UKName, string UKOldName)
		{
			this.ID = ID;
			this.ReportOnDate = ReportOnDate;
			this.MarketCost = MarketCost;
			this.ContractNumber = ContractNumber;
			this.UKName = LegalEntity.GetFormalizedNameFull(UKName, UKOldName);
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual DateTime? ReportOnDate { get; set; }

		[IgnoreDataMember]
		public virtual int? ReportOnDateYear { get { return this.ReportOnDate.HasValue ? (int?)this.ReportOnDate.Value.Year : null; } }

        [IgnoreDataMember]
        public virtual string ReportOnDateDate { get { return this.ReportOnDate.HasValue ? string.Format("{0}\t{1}", this.ReportOnDate.Value.Date.ToShortDateString(), this.lDocumentNumbers.HasValue ? string.Concat("(", this.lDocumentNumbers.Value.ToString(), ")") : string.Empty) : null; } }

        [IgnoreDataMember]
        public virtual long? lDocumentNumbers { get; set; }

       
		[DataMember]
		public virtual  MarketCostIdentifier.Forms FormType { get; set; }

		[DataMember]
		public virtual decimal? MarketCost { get; set; }

		[DataMember]
		public virtual string ContractNumber { get; set; }

		[DataMember]
		public virtual string UKName { get; set; }










	}
}
