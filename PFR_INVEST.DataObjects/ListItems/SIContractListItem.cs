﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class SIContractListItem
    {
        /// <summary>
        /// ID допсоглашения
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// ID договора
        /// </summary>
        [DataMember]
        public long ContractID { get; set; }

        /// <summary>
        /// ID of LegalEntity
        /// </summary>
        [DataMember]
        public long LegalEntityID { get; set; }

        /// <summary>
        /// Тип контрагента
        /// </summary>
        [DataMember]
        public string ContragentType { get; set; }

        /// <summary>
        /// Имя СИ
        /// </summary>
        [DataMember]
        public string SIName { get; set; }

        /// <summary>
        /// Номер договора-портфеля
        /// </summary>
        [DataMember]
        public string ContractPortfolioNumber { get; set; }

        /// <summary>
        /// Тип доп. договора
        /// </summary>
        [DataMember]
        public string SupAgreementKind { get; set; }

        /// <summary>
        /// Дата заключения доп. соглашения
        /// </summary>
        [DataMember]
        public DateTime? SupAgreementDate { get; set; }

        [DataMember]
        public DateTime? DissolutionDate { get; set; }

        /// <summary>
        /// Номер соглашения
        /// </summary>
        [DataMember]
        public string SupAgreementNumber { get; set; }

        ///// <summary>
        ///// Номер договора-портфеля
        ///// </summary>
        //[DataMember]
        //public SIContractListItemStatusType ContractStatus { get; set; }
    }
}
