﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class EdoOdkF140ListItem
    {
        [DataMember]
        public virtual long ID { get; set; }
        [DataMember]
        public virtual string UKName { get; set; }
        [DataMember]
        public virtual string ContractNumber { get; set; }
        [DataMember]
        public virtual string Status { get; set; }
        [DataMember]
        public virtual string DocNumber { get; set; }
        [DataMember]
        public virtual DateTime? ViolationDate { get; set; }
        [DataMember]
        public virtual string Infirm1 { get; set; }
        [DataMember]
        public virtual string Infirm2 { get; set; }
        [DataMember]
        public virtual DateTime? StopDate { get; set; }
        [DataMember]
        public virtual DateTime? RealStopDate { get; set; }
        [DataMember]
        public virtual string StopNumber { get; set; }
    }
}
