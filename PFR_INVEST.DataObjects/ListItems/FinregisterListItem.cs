﻿using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract]
	public class FinregisterListItem: Finregister
	{
		public FinregisterListItem() { }

		public FinregisterListItem(Finregister finregister) :this()
		{
			finregister.CopyTo(this);
		}

        [DataMember]
	    public bool? HasPP { get; set; }

        /// <summary>
        /// Поле для биндинга в списке выбора финреестра
        /// </summary>
        [IgnoreDataMember]
        public bool? HasPP2 { get { return HasPP; } }


		/// <summary>
		/// Конструктор специально для Hibernate
		/// </summary>
		public FinregisterListItem(Finregister finregister, Register register, string creditNPFName, string debitNPFName, long? ppCount = null) :this(finregister)
		{
			this.NPFName = RegisterIdentifier.IsFromNPF(register.Kind) ? creditNPFName : debitNPFName;
		    HasPP = ppCount > 0;
		}

        public FinregisterListItem(Finregister finregister, Register register, string creditNPFName, string debitNPFName)
            : this(finregister)
        {
            this.NPFName = RegisterIdentifier.IsFromNPF(register.Kind) ? creditNPFName : debitNPFName;
        }


		public FinregisterListItem(Finregister finregister, Register register, string creditNPFName, string debitNPFName, string creditNPFStatus, string debitNPFStatus)
			: this(finregister, register, creditNPFName, debitNPFName)
		{
			this.NPFStatus = RegisterIdentifier.IsFromNPF(register.Kind) ? creditNPFStatus : debitNPFStatus;
		}

		[DataMember]
		public string NPFName { get; set; }

		[DataMember]
		public string NPFStatus { get; set; }
	}
}
