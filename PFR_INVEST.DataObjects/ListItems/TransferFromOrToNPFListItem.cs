﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class TransferFromOrToNPFListItem
    {
        [DataMember]
        public long AsgFinTrID { get; set; }

        [DataMember]
        public long FinregisterID { get; set; }

        [DataMember]
        public string Portfolio { get; set; }

        [DataMember]
        public string RegisterKind { get; set; }

        [DataMember]
        public int? Year { get; set; }

        [DataMember]
        public string HalfYear { get; set; }

        [DataMember]
        public string QuarterYear { get; set; }

        [DataMember]
        public string Month { get; set; }

        [DataMember]
        public DateTime? PaymentOrderDate { get; set; }

        [DataMember]
        public DateTime? DraftPayDate { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public decimal? ToNPF { get; set; }

        [DataMember]
        public decimal? FromNPF { get; set; }

        [DataMember]
        public decimal? SPNCount { get; set; }

        [DataMember]
        public string NPFShortName { get; set; }

        [DataMember]
        public long? RegisterID { get; set; }

        [DataMember]
        public string ApproveDocName { get; set; }

        [DataMember]
        public string RegNum { get; set; }

		[DataMember]
		public decimal? CommonPPSum { get; set; }
    }
}
