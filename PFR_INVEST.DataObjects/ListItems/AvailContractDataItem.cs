﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    public class AvailContractDataItem
    {
        public AvailContractDataItem(SIUKPlan plan)
        {
            UKPlan = plan;
        }

        [DataMember]
        public SIUKPlan UKPlan { get; private set; }

        [IgnoreDataMember]
        public decimal? FactSum
        {
            get
            {
                if (!UKPlan.FactSum.HasValue)
                    UKPlan.FactSum = 0;
                return UKPlan.FactSum;
            }
        }

        [DataMember]
        public decimal InvestmentIncome { get; set; }

        [DataMember]
        public long QuantityZL { get; set; }

        [DataMember]
        public string LegalEntityFormalizedName { get; set; }

        [DataMember]
        public string ContractNumber { get; set; }

        [DataMember]
        public long ContractID { get; set; }

        [IgnoreDataMember]
        public decimal? PlanSum
        {
            get
            {
                return UKPlan.PlanSum;
            }
        }
    }
}
