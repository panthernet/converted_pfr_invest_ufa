﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DepClaimOfferListItem : BaseDataObject
    {
        [DataMember]
        public DepClaimOffer Offer { get; set; }

        [DataMember]
        public DepClaim2 DepClaim { get; set; }

        [DataMember]
        public DepClaimSelectParams Auction { get; set; }
    }
}
