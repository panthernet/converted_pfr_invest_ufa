﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    using System.Collections.Generic;

    public class AgencyRatingListItem : BaseDataObject
    {
        public AgencyRatingListItem()
            : base()
        {
        }

        public AgencyRatingListItem(MultiplierRating multiplier, Rating rating, RatingAgency agency)
        {
            MultiplierRating = multiplier;
            Rating = rating;
            RatingAgency = agency;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual MultiplierRating MultiplierRating { get; set; }

        [DataMember]
        public virtual Rating Rating { get; set; }

        [DataMember]
        public virtual RatingAgency RatingAgency{ get; set; }

        [IgnoreDataMember]
        public virtual long? Number { get { return MultiplierRating.Number; } }

        [IgnoreDataMember]
        public virtual string RatingName { get { return Rating.RatingName; } }

        [IgnoreDataMember]
        public virtual string AgencyName { get { return RatingAgency.Name; } }

        [IgnoreDataMember]
        public virtual decimal? Multiplier { get { return MultiplierRating.Multiplier; } }

        [IgnoreDataMember]
        public virtual long MultiplierID { get { return MultiplierRating.ID; } }
    }
}
