﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	[DataContract(IsReference = true)]
    public class LongDecimalTuple
    {
        [DataMember]
        public long? Key { get; set; }

        [DataMember]
        public decimal? Value { get; set; }

        public LongDecimalTuple() { }

        public LongDecimalTuple(long? key, decimal? value)
        {
            Key = key;
            Value = value;
        }
    }

	[DataContract(IsReference = true)]
    public class LongLongTuple
    {
        [DataMember]
        public long? Key { get; set; }

        [DataMember]
        public long? Value { get; set; }

        public LongLongTuple() { }

        public LongLongTuple(long? key, long? value)
        {
            Key = key;
            Value = value;
        }
    }

    [DataContract(IsReference = true)]
    public class LongLongLongTuple
    {
        [DataMember]
        public long? Key { get; set; }

        [DataMember]
        public long? Value { get; set; }

        [DataMember]
        public long? Value2 { get; set; }

        public LongLongLongTuple() { }

        public LongLongLongTuple(long? key, long? value, long? value2)
        {
            Key = key;
            Value = value;
            Value2 = value2;
        }
    }
}
