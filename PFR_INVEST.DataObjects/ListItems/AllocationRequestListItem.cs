﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class AllocationRequestListItem
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string DepKind { get; set; }

        [DataMember]
        public virtual decimal? MinRate { get; set; }

        [DataMember]
        public virtual int? Period { get; set; }

        [DataMember]
        public virtual DateTime? ADate { get; set; }

        [DataMember]
        public virtual string Status { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual decimal? LocationVolume { get; set; }

        [DataMember]
        public virtual string ReqNum { get; set; }

        [DataMember]
        public virtual string OrgTrade { get; set; }

        [DataMember]
        public virtual decimal? MinLocationVolume { get; set; }

        [DataMember]
        public virtual int? BankCount { get; set; }

        [DataMember]
        public virtual decimal? ClaimVolume { get; set; }

        [DataMember]
        public virtual int? ClaimCount { get; set; }

        [DataMember]
        public virtual decimal? DepMinRate { get; set; }

        [DataMember]
        public virtual decimal? DepMaxRate { get; set; }

        [DataMember]
        public virtual decimal? DepMinSum { get; set; }

        [DataMember]
        public virtual DateTime? PaymentDate { get; set; }

        [DataMember]
        public virtual string PaymentPeriodicity { get; set; }

        [DataMember]
        public virtual decimal? DepAmputation { get; set; }

        [DataMember]
        public virtual long? PfrBankAccountID { get; set; }
        private string _pfName;

        [DataMember]
        public virtual string PFName
        {
            get { return string.IsNullOrEmpty(_pfName) ? string.Empty : _pfName; }
            set { _pfName = value; }
        }
    }
}
