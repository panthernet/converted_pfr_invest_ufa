﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ERZLListItem
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        [DataMember]
        public string Content { get; set; }

        /// <summary>
        /// Дата FZ
        /// </summary>
        [DataMember]
        public DateTime? FZDate { get; set; }

        /// <summary>
        /// Название FZ
        /// </summary>
        [DataMember]
        public string FZName { get; set; }

        /// <summary>
        /// FZ_ID
        /// </summary>
        [DataMember]
        public long FZ_ID { get; set; }

        /// <summary>
        /// Company
        /// </summary>
        [DataMember]
        public string Company { get; set; }

        /// <summary>
        /// RegNum
        /// </summary>
        [DataMember]
        public string RegNum { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        [DataMember]
        public DateTime? Date { get; set; }

    }
}
