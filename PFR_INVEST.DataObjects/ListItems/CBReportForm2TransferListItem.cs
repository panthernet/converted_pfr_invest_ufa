﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class CBReportForm2TransferListItem
    {
        public int Fake { get; set; }
         
        [DataMember]
        public virtual string Count { get; set; }

        public virtual int CountInt => string.IsNullOrEmpty(Count) ? 0 : int.Parse(Count);

        [DataMember]
        public virtual string OpDescription { get; set; }

        [DataMember]
        public virtual string OpCode { get; set; }

        [DataMember]
        public virtual string OpActive { get; set; }

        [DataMember]
        public virtual string Date { get; set; }

        [DataMember]
        public virtual DateTime? DateValue { get; set; }

        [DataMember]
        public virtual string RcvName { get; set; }

        [DataMember]
        public virtual string INN { get; set; }
        
        [DataMember]
        public virtual string Assignment { get; set; }

        [DataMember]
        public virtual string ContractNum { get; set; }

        [DataMember]
        public virtual string Sum { get; set; }

        [DataMember]
        public virtual decimal SumValue { get; set; }

        public virtual long ID { get; set; }

        public virtual decimal ParentSum { get; set; }

        public virtual decimal SumValuePrec { get; set; }

        public virtual long PPID { get; set; }

        [IgnoreDataMember]
        public virtual bool IsUnlinked { get; set; }

        [DataMember]
        public string RegNum { get; set; }

        [DataMember]
        public DateTime? RegDate { get; set; }

        [DataMember]
        public string PPRegNum { get; set; }

        [DataMember]
        public DateTime? PPDraftDate { get; set; }
    }
}
