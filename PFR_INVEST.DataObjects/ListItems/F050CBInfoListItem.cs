﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
	public class F050CBInfoListItem : BaseDataObject
	{
		public F050CBInfoListItem() { }

        public F050CBInfoListItem(F050CBInfoListItemLite item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Num = item.Num;
            this.Price = item.Price;
            this.Type = item.Type;
            this.EdoID = item.EdoID;
            this.Count = item.Count;
            this.TypeID = item.TypeID;
            this.ReportDate = item.ReportDate;
            this.ContractNumber = item.ContractNumber;
            this.UKName = item.UKName;
            this.EDOCount = item.EDOCount;
        }

		public F050CBInfoListItem(F050CBInfo item)
		{
			this.ID = item.ID;
			this.Name = item.Name;
			this.Num = item.Num;
			this.Price = item.Price;
			this.Type = item.Type;
			this.EdoID = item.EdoID;
			this.Count = item.Count;
            this.TypeID= item.TypeID;
            
		}
		public F050CBInfoListItem(F050CBInfo item, DateTime? reportDate, string contractNumber, string UKName, string oldUKName, long? edo_cnt)
			: this(item)
		{
			this.ReportDate = reportDate;
			this.ContractNumber = contractNumber;
			this.UKName = LegalEntity.GetFormalizedNameFull(UKName, oldUKName);
            this.EDOCount = edo_cnt;
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? EdoID { get; set; }

		/// <summary>
		/// Тип ЦБ
		/// </summary>
		[DataMember]
		public virtual string Type { get; set; }
        /// <summary>
		/// IDТип ЦБ
		/// </summary>
		[DataMember]
		public virtual int? TypeID { get; set; }
		/// <summary>
		/// Наименование ЦБ
		/// </summary>
		[DataMember]
		public virtual string Name { get; set; }

		/// <summary>
		/// Номер выпуска
		/// </summary>
		[DataMember]
		public virtual string Num { get; set; }

		/// <summary>
		/// Количество ЦБ
		/// </summary>
		[DataMember]
		public virtual decimal? Count { get; set; }

		/// <summary>
		/// Рыночная стоимость ЦБ
		/// </summary>
		[DataMember]
		public virtual decimal? Price { get; set; }
		/// <summary>
		/// Дата отчёта из EDO_ODKF050
		/// </summary>
		[DataMember]
		public virtual DateTime? ReportDate { get; set; }

		/// <summary>
		/// № Договора из CONTRACT
		/// </summary>
		[DataMember]
		public virtual string ContractNumber { get; set; }

		/// <summary>
		/// Наименование УК из LEGALENTITY
		/// </summary>
		[DataMember]
		public virtual string UKName { get; set; }

        public virtual long? EDOCount { get; set; }

        public virtual string RDYear { get { return this.ReportDate.HasValue ? this.ReportDate.Value.Year.ToString() : string.Empty; } }

        public virtual string RDDate { 
            get 
            {
                string formatText = "{0} \t({1})";
            
                return this.ReportDate.HasValue ? string.Format(formatText, this.ReportDate.Value.Date.ToShortDateString(), this.EDOCount.ToString()) : string.Empty;
            } 
        }
        
	}

    [DataContract]
    public class F050ReportListItem : BaseDataObject
    {
        [DataMember]
        public long ID { get; set; }        
        
        /// <summary>
        /// Дата отчёта из EDO_ODKF050
        /// </summary>
        [DataMember]
        public DateTime? ReportDate { get; set; }

        /// <summary>
        /// № Договора из CONTRACT
        /// </summary>
        [DataMember]
        public string ContractNumber { get; set; }

        /// <summary>
        /// Наименование УК из LEGALENTITY
        /// </summary>
        [DataMember]
        public string UKName { get; set; }

        /// <summary>
        /// Наименование УК из LEGALENTITY старое
        /// </summary>
        [DataMember]
        public string UKOldName { get; set; }

        [IgnoreDataMember]
        public string UKNameFull { get { return LegalEntity.GetFormalizedNameFull(UKName, UKOldName); } }

        
        public long? EDOCount { get; set; }

        [DataMember]
        public long? CBCount { get; set; }

        [DataMember]
        public decimal? CBAmount { get; set; } 

        [IgnoreDataMember]
        public string RDYear { get { return this.ReportDate.HasValue ? this.ReportDate.Value.Year.ToString() : string.Empty; } }

        [IgnoreDataMember]
        public string RDDate
        {
            get
            {
                string formatText = "{0} \t({1})";

                return this.ReportDate.HasValue ? string.Format(formatText, this.ReportDate.Value.Date.ToShortDateString(), this.EDOCount.ToString()) : string.Empty;
            }
        }  
      
    }

    [DataContract]
    public class F050CBInfoListItemLite : IDataObjectLite
    {
        public F050CBInfoListItemLite() { }

        public F050CBInfoListItemLite(F050CBInfoLite item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Num = item.Num;
            this.Price = item.Price;
            this.Type = item.Type;
            this.EdoID = item.EdoID;
            this.Count = item.Count;
            this.TypeID = item.TypeID;

        }
        public F050CBInfoListItemLite(F050CBInfoLite item, DateTime? reportDate, string contractNumber, string UKName, string oldUKName, long? edo_cnt)
            : this(item)
        {
            this.ReportDate = reportDate;
            this.ContractNumber = contractNumber;
            this.UKName = LegalEntity.GetFormalizedNameFull(UKName, oldUKName);
            this.EDOCount = edo_cnt;
        }

        [DataMember(Name="A")]
        public long ID { get; set; }

        [DataMember(Name = "B")]
        public long? EdoID { get; set; }

        /// <summary>
        /// Тип ЦБ
        /// </summary>
        [DataMember(Name = "C")]
        public string Type { get; set; }
        /// <summary>
        /// IDТип ЦБ
        /// </summary>
        [DataMember(Name = "D")]
        public int? TypeID { get; set; }
        /// <summary>
        /// Наименование ЦБ
        /// </summary>
        [DataMember(Name = "E")]
        public string Name { get; set; }

        /// <summary>
        /// Номер выпуска
        /// </summary>
        [DataMember(Name = "F")]
        public string Num { get; set; }

        /// <summary>
        /// Количество ЦБ
        /// </summary>
        [DataMember(Name = "G")]
        public decimal? Count { get; set; }

        /// <summary>
        /// Рыночная стоимость ЦБ
        /// </summary>
        [DataMember(Name = "H")]
        public decimal? Price { get; set; }
        /// <summary>
        /// Дата отчёта из EDO_ODKF050
        /// </summary>
        [DataMember(Name = "I")]
        public DateTime? ReportDate { get; set; }

        /// <summary>
        /// № Договора из CONTRACT
        /// </summary>
        [DataMember(Name = "J")]
        public string ContractNumber { get; set; }

        /// <summary>
        /// Наименование УК из LEGALENTITY
        /// </summary>
        [DataMember(Name = "K")]
        public string UKName { get; set; }

        public long? EDOCount { get; set; }

        public string RDYear { get { return this.ReportDate.HasValue ? this.ReportDate.Value.Year.ToString() : string.Empty; } }

        public string RDDate
        {
            get
            {
                string formatText = "{0} \t({1})";

                return this.ReportDate.HasValue ? string.Format(formatText, this.ReportDate.Value.Date.ToShortDateString(), this.EDOCount.ToString()) : string.Empty;
            }
        }
    }

}
