﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ReqTransferDeleteListItem
    {
        [DataMember]
        public long ReqTransferId { get; set; }
        [DataMember]
        public long ContractId { get; set; }
        [DataMember]
        public bool IsActSigned { get; set; }
    }
}
