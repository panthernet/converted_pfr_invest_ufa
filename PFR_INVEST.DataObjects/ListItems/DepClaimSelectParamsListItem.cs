﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DepClaimSelectParamsListItem
    {
        [DataMember]
        public virtual long? ID { get; set; }

        [DataMember]
        public virtual int? AuctionNum { get; set; }

        [DataMember]
        public virtual int? MaxDepClaimAmount { get; set; }

        [DataMember]
        public virtual int? Period { get; set; }

        [DataMember]
        public virtual decimal? MaxInsuranceFee { get; set; }

        [DataMember]
        public virtual decimal? MinDepClaimVolume { get; set; }

        [DataMember]
        public virtual int? MinRate { get; set; }

        [DataMember]
        public virtual DateTime? PlacementDate { get; set; }

        [DataMember]
        public virtual DateTime? ReturnDate { get; set; }

        [DataMember]
        public virtual DateTime? SelectDate { get; set; }

        [DataMember]
        public virtual TimeSpan? DepClaimAcceptEnd { get; set; }

        [DataMember]
        public virtual TimeSpan? DepClaimAcceptStart { get; set; }

        [DataMember]
        public virtual TimeSpan? DepClaimListEnd { get; set; }

        [DataMember]
        public virtual TimeSpan? DepClaimListStart { get; set; }

        [DataMember]
        public virtual TimeSpan? FilterEnd { get; set; }

        [DataMember]
        public virtual TimeSpan? FilterStart { get; set; }

        [DataMember]
        public virtual string DepClaimSelectLocation { get; set; }

        [DataMember]
        public virtual TimeSpan? OfferReceiveEnd { get; set; }

        [DataMember]
        public virtual TimeSpan? OfferReceiveStart { get; set; }

        [DataMember]
        public virtual TimeSpan? OfferSendEnd { get; set; }

        [DataMember]
        public virtual TimeSpan? OfferSendStart { get; set; }

        public DepClaimSelectParamsListItem(int auctionNum)
        {
            AuctionNum = auctionNum;
        }
    }
}
