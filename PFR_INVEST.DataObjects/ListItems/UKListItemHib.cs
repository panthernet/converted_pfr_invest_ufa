﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class UKListItemHib
    {
		public UKListItemHib() { }

        [DataMember]
        public ReqTransfer ReqTransfer { get; set; }

        [DataMember]
        public string SPNDirectionName { get; set; }

        [DataMember]
        public string SPNOperationName { get; set; }

        [DataMember]
        public string FormalizedName { get; set; }

        [DataMember]
        public string ContractNumber { get; set; }

        [DataMember]
        public string Portfolio { get; set; }

        [DataMember]
        public string PfrBankAccount { get; set; }

        [DataMember]
        public bool? IsFromUKToPFR { get; set; }

        [DataMember]
        public long? SIRegisterID { get; set; }

        [DataMember]
        public string RegisterKind { get; set; }

        [DataMember]
        public string RegisterNumber { get; set; }

        [DataMember]
        public DateTime? RegisterDate { get; set; }

		[DataMember]
		public decimal? CommonPPSum { get; set; }

		[DataMember]
		public string CommonPPNumber { get; set; }

		[DataMember]
		public DateTime? CommonPPDate { get; set; }

		[DataMember]
		public long? CommonPPKBK { get; set; }
    }
}
