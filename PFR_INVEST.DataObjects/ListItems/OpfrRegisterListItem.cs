﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract(IsReference = true)]
    public class OpfrRegisterListItem
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? TransferID { get; set; }

        [DataMember]
        public virtual string TypeName { get; set; }

        [DataMember]
        public virtual string RegisterName { get; set; }

        [DataMember]
        public virtual string OpfrName { get; set; }

        [DataMember]
        public virtual long YearID { get; set; }

        [DataMember]
        public virtual string Year { get; set; }

        [DataMember]
        public virtual long MonthID { get; set; }

        [DataMember]
        public virtual string Month { get; set; }

        [DataMember]
        public virtual string Kosgu { get; set; }

        [DataMember]
        public virtual decimal Summ { get; set; }

        [DataMember]
        public virtual string TrancheNum { get; set; }

        [DataMember]
        public virtual DateTime? TrancheDate { get; set; }

        [DataMember]
        public virtual decimal PPSum { get; set; }

        public OpfrRegisterListItem(long id, long? trId, string typeName, string regNum, DateTime regDate,
            string bName, long yearId, long monthId, string kosgu, decimal sum, string tnum, DateTime? tdate)
        {
            ID = id;
            TransferID = trId;
            TypeName = typeName;
            RegisterName = $"{id} \tдокумент:   \r\n\t№ {regNum} от: {regDate.Date:dd.MM.yyyy}";
            OpfrName = bName;
            YearID = yearId;
            Year = (yearId + 2000).ToString();
            MonthID = monthId;
            Month = new CultureInfo("ru-RU").DateTimeFormat.GetMonthName((int) monthId).ToLower();
            Kosgu = kosgu;
            Summ = sum;
            TrancheDate = tdate;
            TrancheNum = tnum;
        }

        public OpfrRegisterListItem()
        {

        }
    }
}
