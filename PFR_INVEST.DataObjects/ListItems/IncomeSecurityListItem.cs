﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class IncomeSecurityListItem
    {
		public IncomeSecurityListItem() { }


        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public string Portfolio { get; set; }

        [DataMember]
        public string IncomeKind { get; set; }

        [DataMember]
        public string Year { get; set; }

        [DataMember]
        public string Quarter { get; set; }

        [DataMember]
        public string Month { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public DateTime? OrderDate { get; set; }

        [DataMember]
        public decimal? Sum { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public string Comment { get; set; }


        [DataMember]
        public long? PaymentHistoryID { get; set; }
    }
}