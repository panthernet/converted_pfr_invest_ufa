﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class ZLMovementListItem
    {
        /// <summary>
        /// ID данного списка перечисления
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Формализованное наименомание УК
        /// </summary>
        [DataMember]
        public string UKFormalizedName { get; set; }

        /// <summary>
        /// Номер договора
        /// </summary>        
        [DataMember]
        public string ContractNumber { get; set; }

        /// <summary>
        /// Содержание операции
        /// </summary>
        [DataMember]
        public string Operation { get; set; }

        /// <summary>
        /// Направление операции
        /// </summary>
        [DataMember]
        public string Direction { get; set; }

        /// <summary>
        /// Дата операции
        /// </summary>
        [DataMember]
        public DateTime? OperationDate { get; set; }

        /// <summary>
        /// Количество застрахованных лиц до операции
        /// </summary>
        [DataMember]
        public long ZLCountBeforeOpeartion { get; set; }

        /// <summary>
        /// Количество застрахованных лиц, перешедших из ПФР в УК
        /// </summary>
        [DataMember]
        public long ZLCountFromPFRToUK { get; set; }

        /// <summary>
        /// Количество застрахованных лиц, перешедших из УК в ПФР
        /// </summary>
        [DataMember]
        public long ZLCountFromUKtoPFR { get; set; }

        /// <summary>
        /// Количество застрахованных лиц по выплатам правопреемников
        /// Валиден только если содержание операции - выплаты правопреемникам, иначе - 0
        /// </summary>
        [DataMember]
        public long ZLCountToAssignPayments { get; set; }

        /// <summary>
        /// Количество застрахованных лиц после операции
        /// </summary>
        [DataMember]
        public long ZLCountAfterOperation
        {
            get
            {
                return ZLCountBeforeOpeartion + ZLCountFromPFRToUK - ZLCountFromUKtoPFR - ZLCountToAssignPayments;
            }
            private set { }
        }
    }
}
