﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.ListItems
{
    [DataContract]
    public class DirectionTransferListItem
    {
        [DataContract]
        public enum DirectionTransferListItemOperationType
        {
            [EnumMember]
            DirectionTransferListItemOperationTypeTransfer = 0,
            [EnumMember]
            DirectionTransferListItemOperationTypeReturn = 1
        }

        public DirectionTransferListItem(Transfer transfer)
        {
            ID = transfer.ID;
            Date = transfer.Date;
            Sum = transfer.Sum;
            OperationType = DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeTransfer;
        }

        public DirectionTransferListItem(Return _return)
        {
            ID = _return.ID;
            Date = _return.Date;
            Sum = -_return.Sum;
            OperationType = DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeReturn;
        }

        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Дата
        /// </summary>
        [DataMember]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Изменение
        /// </summary>
        [DataMember]
        public decimal? Sum { get; set; }

        /// <summary>
        /// Итог
        /// </summary>
        [DataMember]
        public decimal? Total { get; set; }

        /// <summary>
        /// Тип операции
        /// </summary>
        [DataMember]
        public DirectionTransferListItemOperationType OperationType { get; private set; }

        /// <summary>
        /// Содержание операции
        /// </summary>
        [DataMember]
        public string Operation
        {
            get
            {
                if (OperationType == DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeTransfer)
                    return "Перечисление";
                if (OperationType == DirectionTransferListItemOperationType.DirectionTransferListItemOperationTypeReturn)
                    return "Возврат";
                return null;
            }
            private set { }
        }
    }
}
