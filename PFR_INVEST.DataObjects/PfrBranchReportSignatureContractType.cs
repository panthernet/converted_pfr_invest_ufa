﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportSignatureContractType : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [IgnoreDataMember]
        public virtual SignatureContractType Type
        {
            get
            {
                switch (Name.ToUpper())
                {
                    case "NPF": return SignatureContractType.Npf;
                    case "CREDIT": return SignatureContractType.Credit;
                    case "COMMUNICATION": return SignatureContractType.Communication;
                }

                return SignatureContractType.Empty;
            }
        }

        public enum SignatureContractType
        {
            Empty,
            Npf,
            Credit,
            Communication
        }
    }
}
