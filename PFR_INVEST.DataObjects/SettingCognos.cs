﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SettingCognos : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string UrlName { get; set; }
       
        [DataMember]
        public virtual string FolderId { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }
    }
}
