﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Reflection;

namespace PFR_INVEST.DataObjects
{
	[DataContract]
	public class DepClaim2 : BaseDataObject
	{
		public enum Statuses
		{
			[Description("Новая")]
			New = 1,
			[Description("Подлежащая удовлетворению")]
			Confirm = 2,
			[Description("Не подлежащая удовлетворению")]
			NotConfirm = 3,
			[Description("Сгенерирована оферта")]
			ConfirmOfferCreated = 4,
			[Description("Сгенерирована справка об отсутствии акцепта")]
			ConfirmNotAccepted = 5

		}

		[DataMember]
		public virtual long ID { get; set; }


		[DataMember]
		public virtual string FirmID { get; set; }

		[DataMember]
		public virtual string FirmName { get; set; }

		[DataMember]
		public virtual long RecNum { get; set; }

		[DataMember]
		public virtual string SecurityID { get; set; }

		[DataMember]
		public virtual decimal Rate { get; set; }

		/// <summary>
		/// Сумма депозита
		/// </summary>
		[DataMember]
		public virtual decimal Amount { get; set; }


		/// <summary>
		/// Сумма процентов
		/// </summary>
		[DataMember]
		public virtual decimal OldPayment { get; set; }


		[DataMember]
		public virtual decimal OldPart2Sum { get; set; }

		/// <summary>
		/// Сумма депозита
		/// </summary>
		[DataMember]
		public virtual decimal OldAmount { get; set; }

		/// <summary>
		/// Ставка процентов(до удовлетворения)
		/// </summary>
		[DataMember]
		public virtual decimal OldRate { get; set; }


		/// <summary>
		/// Сумма процентов
		/// </summary>
		[DataMember]
		public virtual decimal Payment { get; set; }


		[DataMember]
		public virtual decimal Part2Sum { get; set; }


		[DataMember]
		public virtual DateTime SettleDate { get; set; }

		[DataMember]
		public virtual DateTime ReturnDate { get; set; }

		[IgnoreDataMember]
		public virtual Statuses Status
		{
			get { return (Statuses)this.StatusID; }
			set { this.StatusID = (int)value; }
		}

		[DataMember]
		public virtual int StatusID { get; set; }

		[DataMember]
		public virtual DateTime AuctionDate { get; set; }

		/// <summary>
		/// Биржа аукциона. Используется только для вычитки с базы
		/// </summary>
		[DataMember]
		public virtual string StockName { get; set; }

		/// <summary>
		/// Биржа аукциона. Используется только для вычитки с базы
		/// </summary>
		[DataMember]
		public virtual long StockID { get; set; }

		[DataMember]
		public virtual long AuctionID { get; set; }

		[DataMember]
		public virtual long BankID { get; set; }

		[DataMember]
		public virtual DateTime? DocumentDate { get; set; }

		[DataMember]
		public virtual TimeSpan? DocumentTime { get; set; }

		[DataMember]
		public virtual long Term { get; set; }




		/// <summary>
		/// Статус аукциона. Используется только для вычитки с базы
		/// </summary>
		[IgnoreDataMember]
		public virtual DepClaimSelectParams.Statuses AuctionStatus
		{
			get { return (DepClaimSelectParams.Statuses)this.AuctionStatusID; }
		}

		/// <summary>
		/// Статус аукциона. Используется только для вычитки с базы
		/// </summary>
		[DataMember]
		public virtual int AuctionStatusID { get; set; }

		/// <summary>
		///  Количество заявок c максимальной суммой. Поле только для вычитки из базы.
		/// </summary>
		[DataMember]
		public virtual int DepClaimMaxCount { get; set; }

		/// <summary>
		///  Количество сводных заявок. Поле только для вычитки из базы.
		/// </summary>
		[DataMember]
		public virtual int DepClaimCommonCount { get; set; }

		[IgnoreDataMember]
		public virtual string StatusText
		{
			get
			{
				FieldInfo fi = Status.GetType().GetField(Status.ToString());
				DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

				return attributes[0].Description;
			}
		}

		/// <summary>
		/// Ссылка на Выписку из реестра заявок, по котороый созавалась запись
		/// </summary>
		[DataMember]
		public virtual long? RepositoryID { get; set; }

		/// <summary>
		/// Ссылка на Выписку из реестра заявок, подлежащих удовлетворению, по которой создавалась запись
		/// </summary>
		[DataMember]
		public virtual long? ConfirmRepositoryID { get; set; }

		[IgnoreDataMember]
		public virtual string RepositoryHint { get { return RepositoryID.HasValue ? "Ссылка на оригинал" : string.Empty; } }

		[IgnoreDataMember]
		public virtual string ConfirmRepositoryHint { get { return ConfirmRepositoryID.HasValue ? "Ссылка на оригинал" : string.Empty; } }
	}
}
