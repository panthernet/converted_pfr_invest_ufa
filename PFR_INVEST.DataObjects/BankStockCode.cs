﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class BankStockCode : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        private long _bankID;
        [DataMember]
        public virtual long BankID
        {
            get { return _bankID; }
            set { if (_bankID != value) { _bankID = value; OnPropertyChanged("BankID"); } }
        }


        private string _stockCode;
        [DataMember]
        public virtual string StockCode
        {
            get { return _stockCode != null ? _stockCode.Trim() : null; }
            set { if (_stockCode != value) { _stockCode = value; OnPropertyChanged("StockCode"); } }
        }

        ///// <summary>
        ///// Используется только для вычитки из базы
        ///// </summary>
        //public virtual string StockName { get; set; }


        private long _stockID;
        [DataMember]
        public virtual long StockID
        {
            get { return _stockID; }
            set { if (_stockID != value) { _stockID = value; OnPropertyChanged("StockID"); } }
        }

		private const string DATA_TOO_LONG = "Размер указанного значения превышает допустимый";
        private const string DATA_REQUIRED = "Поле обязательное для заполнения";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToLower()) 
                {
                    case "stockid": return this.StockID.ValidateRequired();
                    case "bankid": return this.BankID.ValidateRequired();
                    case "stockcode":
						if (this.StockCode != null && this.StockCode.Length > 49)
							return DATA_TOO_LONG;
                        return string.IsNullOrEmpty(this.StockCode) || this.StockCode.Trim().Length == 0 ? DATA_REQUIRED : null;
                    default: return base[columnName];
                }
               
            }
        }

        public override bool Validate()
        {
            const string fieldNames =
                            "stockid|bankid|stockcode";

            return fieldNames.Split("|".ToCharArray()).All(fieldName => String.IsNullOrEmpty(this[fieldName]));
        }
    }
}
