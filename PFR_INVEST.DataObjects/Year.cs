﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class Year : BaseDataObject, IIdentifiable
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual string Name { get; set; }


		/// <summary>
		/// Конвертирует год в его ИД, на текущий момент year - 2000
		/// </summary>		
		public static long YearToID(int year)
		{
			return year - 2000;
		}

		/// <summary>
		/// Конвертирует ИД года в год, на текущий момент id + 2000
		/// </summary>	
		public static int IDToYear(long id)
		{
			return (int)(id + 2000);
		}
	}
}
