﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportCertificationAgreement : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long ReportId { get; set; }

        [DataMember]
        public virtual long? NpfId { get; set; }

        [DataMember]
        public virtual long TypeId { get; set; }

        [DataMember]
        public virtual long Summary { get; set; }

        [DataMember]
        public virtual long New { get; set; }

        [DataMember]
        public virtual long Existing { get; set; }

        [DataMember]
        public virtual long Terminated { get; set; }

        public enum enType
        {
            NpfSum = 1,
            Npf = 2,
            Credit = 3,
            Employer = 4,
            Communication = 5,
            Total = 6
        }

        [IgnoreDataMember]
        public virtual enType DataType
        {
            get { return (enType)TypeId; }
        }
    }
}
