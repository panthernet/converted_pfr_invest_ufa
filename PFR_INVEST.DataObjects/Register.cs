﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	
	public class Register :
        BaseDataObject, 
	    IMarkedAsDeleted
	{
		[DataMember]
		public virtual long ID { get; set; }

        [DataMember]
        public virtual string PayAssignment { get; set; }

		[DataMember]
		public virtual long? FZ_ID { get; set; }

		[DataMember]
		public virtual string Kind { get; set; }

		[DataMember]
		public virtual string RegNum { get; set; }

		//[Obsolete("Use AsgFinTr.PortfolioID instead", true)]
		[DataMember]
		public virtual long? PortfolioID { get; set; }

		[DataMember]
		public virtual long? ApproveDocID { get; set; }

		[DataMember]
		public virtual DateTime? RegDate { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

		[DataMember]
		public virtual string Company { get; set; }

		[DataMember]
		public virtual long? ERZL_ID { get; set; }

		[DataMember]
		public virtual string Content { get; set; }

        [DataMember]
        public virtual string TrancheNumber { get; set; }

		[DataMember]
		public virtual DateTime? TrancheDate { get; set; }

		[DataMember]
		public virtual long? ReturnID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }

        [DataMember]
        public virtual int? IsCustomArchive { get; set; }
	}
}
