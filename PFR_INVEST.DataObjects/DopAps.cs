﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class DopAps : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual DateTime? OperationDate { get; set; }
		[DataMember]
		public virtual DateTime? Date { get; set; }
		[DataMember]
		public virtual string RegNum { get; set; }
		[DataMember]
		public virtual decimal? Summ { get; set; }
		[DataMember]
		public virtual decimal? ChSumm { get; set; }
		[DataMember]
		public virtual string Comment { get; set; }
		[DataMember]
		public virtual long? DocKind { get; set; }
		[DataMember]
		public virtual long? ApsID { get; set; }
		[DataMember]
		public virtual long? PortfolioID { get; set; }
		[DataMember]
		public virtual long? SPortfolioID { get; set; }
		[DataMember]
		public virtual long? PFRBankAccountID { get; set; }
		[DataMember]
		public virtual long? SPFRBankAccountID { get; set; }

	}
}
