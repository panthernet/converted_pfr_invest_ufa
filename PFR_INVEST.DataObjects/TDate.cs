﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class TDate : BaseDataObject, ILoadedFromDBF
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? TradeDate { get; set; }

        [DataMember]
        public virtual TimeSpan? Time { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string SecurityID { get; set; }

        [DataMember]
        public virtual decimal? WAPrice { get; set; }

        [DataMember]
        public virtual decimal? TrendWAP { get; set; }

        [DataMember]
        public virtual decimal? NumberOfTrades { get; set; }

        [DataMember]
        public virtual decimal? Price { get; set; }

        [DataMember]
        public virtual decimal? TrendPrice { get; set; }

        [DataMember]
        public virtual decimal? Yield { get; set; }

        [DataMember]
        public virtual decimal? Value { get; set; }

        [DataMember]
        public virtual decimal? Quantity { get; set; }

        [DataMember]
        public virtual decimal? TotalValue { get; set; }

        [DataMember]
        public virtual decimal? TotalVolume { get; set; }

        [DataMember]
        public virtual decimal? AccInt { get; set; }

        [DataMember]
        public virtual decimal? SecNumberOfTrades { get; set; }
    }
}
