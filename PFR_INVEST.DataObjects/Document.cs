﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Document : BaseDataObject, IMarkedAsDeleted
    {
        /// <summary>
        /// Псевдо статусы. Используется для фильтрации выборки.
        /// </summary>
        public enum Statuses 
        {       
            /// <summary>
            /// Все
            /// </summary>
            All = 1,
            /// <summary>
            /// Активные(без даты выполнения)
            /// </summary>
            Active = 2,
            /// <summary>
            /// Выполненые(с датой выполнения)
            /// </summary>
            Executed = 3,
            /// <summary>
            /// Контроль(с датой контроля)
            /// </summary>
            Control = 4
        }

        /// <summary>
        /// Типы документации
        /// </summary>
        public enum Types
        {
            All = 0,
            /// <summary>
            /// SI = 1
            /// </summary>
			SI = 1,
			/// <summary>
			/// NPF = 2
			/// </summary>
            Npf = 2,
			/// <summary>
			/// VR = 3
			/// </summary>
            VR = 3,
            OPFR = 4,
            Other =  10
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string IncomingNumber { get; set; }

        [DataMember]
        public virtual DateTime? IncomingDate { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual string OutgoingNumber { get; set; }

        [DataMember]
        public virtual DateTime? OutgoingDate { get; set; }

        [DataMember]
        public virtual long? DocumentClassID { get; set; }

        [DataMember]
        public virtual long? AdditionalInfoID { get; set; }

        [DataMember]
        public virtual long? ExecutorID { get; set; }

        [DataMember]
        public virtual string ExecutorName { get; set; }

        [DataMember]
        public virtual DateTime? ExecutionDate { get; set; }

        [DataMember]
        public virtual string AdditionalExecutionInfo { get; set; }

        private string _AdditionalInfoText;
        [DataMember]
        public virtual string AdditionalInfoText
        {
            get { return _AdditionalInfoText; }
            set { if (_AdditionalInfoText != value) { _AdditionalInfoText = value; OnPropertyChanged("AdditionalInfoText"); } }
        }
                  
        [DataMember]
        public virtual DateTime? ControlDate { get; set; }

        [DataMember]
        public virtual string OriginalStoragePlace { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual int TypeID { get; set; }

        [IgnoreDataMember]
        public virtual Types Type
        {
            get { return (Types)this.TypeID; }
            set { this.TypeID = (int)value; }
        }


        public virtual long StatusID { get; set; }
    }

    public class AddDocument : Document
    {
        public string AdditionalInfo { get; set; }
    }

}
