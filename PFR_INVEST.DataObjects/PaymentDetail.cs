﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	/// <summary>
	/// Назначение платежа
	/// </summary>
	public class PaymentDetail : BaseDataObject, IMarkedAsDeleted, IIdentifiable
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual string PaymentDetails { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

		[DataMember]
		public virtual long? KBKID { get; set; }

		[DataMember]
		public virtual long StatusID { get; set; }
	}
}
