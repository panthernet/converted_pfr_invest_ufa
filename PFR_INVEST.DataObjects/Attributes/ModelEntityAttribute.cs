﻿using System;

namespace PFR_INVEST.DataObjects.Attributes
{
    /// <summary>
    /// Аттрибут указывающий с какой сущностью работает модель и какие особые параметры имеет
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class ModelEntityAttribute: Attribute
    {
        /// <summary>
        /// Описание для журналирования
        /// </summary>
        public string JournalDescription { get; set; }

        /// <summary>
        /// Тип сущности
        /// </summary>
        public Type Type { get; set; }
    }
}
