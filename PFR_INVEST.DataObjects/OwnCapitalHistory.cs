﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OwnCapitalHistory : BaseDataObject, ILoadedFromDBF
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private long _legalEntityID;
        public virtual long LegalEntityID
        {
            get { return _legalEntityID; }
            set { if (_legalEntityID != value) { _legalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
        }

        [IgnoreDataMember]
        private string _legalEntityName;
        public virtual string LegalEntityName
        {
            get { return _legalEntityName; }
            set { if (_legalEntityName != value) { _legalEntityName = value; OnPropertyChanged("LegalEntityName"); } }
        }

        [DataMember]
        private string _key;
        public virtual string Key
        {
            get { return _key; }
            set { if (_key != value) { _key = value; OnPropertyChanged("Key"); } }
        }
        
        [DataMember]
        private string _licenseNumber;
        public virtual string LicenseNumber
        {
            get { return _licenseNumber; }
            set { if (_licenseNumber != value) { _licenseNumber = value; OnPropertyChanged("LicenseNumber"); } }
        }

        [DataMember]
        private decimal _summ;
        public virtual decimal Summ
        {
            get { return _summ; }
            set { if (_summ != value) { _summ = value; OnPropertyChanged("Summ"); } }
        }

        [DataMember]
        private DateTime _reportDate;
        public virtual DateTime ReportDate 
        {
            get { return _reportDate; }
            set { if (_reportDate != value) { _reportDate = value; OnPropertyChanged("ReportDate"); } }
        }

        [DataMember]
        private DateTime _importDate;
        public virtual DateTime ImportDate
        {
            get { return _importDate; }
            set { if (_importDate != value) { _importDate = value; OnPropertyChanged("ImportDate"); } }
        }

        [DataMember]
        private byte _isImported;
        public virtual byte IsImported
        {
            get { return _isImported; }
            set { if (_isImported != value) { _isImported = value; OnPropertyChanged("IsImported"); } }
        }


        [IgnoreDataMember]
        public virtual string ImportType
        {
            get
            {
                return IsImported == 1 ? "Импортировано" : "Добавлено вручную";
            }
        }
    }
}
