﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBankAccount : BaseDataObject
    {
        [DataMember]
        private long id;
        public virtual long ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        [DataMember]
        private string accnum;
        public virtual string AccountNumber
        { 
            get { return accnum; }
            set { accnum = value; OnPropertyChanged("AccountNumber"); }
        }

        [DataMember]
        private DateTime? opendate;
        public virtual DateTime? OpenDate 
        {
            get { return opendate; }
            set { opendate = value; OnPropertyChanged("OpenDate"); } 
        }

        [DataMember]
        private DateTime? closedate;
        public virtual DateTime? CloseDate 
        {
            get { return closedate; }
            set { closedate = value; OnPropertyChanged("CloseDate"); }
        }

        [DataMember]
        private string status;
        public virtual string Status 
        {
            get { return status; }
            set { status = value; OnPropertyChanged("Status"); }
        }

        [DataMember]
        private long? curid;
        public virtual long? CurrencyID 
        {
            get { return curid; }
            set { curid = value; OnPropertyChanged("CurrencyID"); }
        }

        [DataMember]
        private long? leid;
        public virtual long? LegalEntityBankID         
        {
            get { return leid; }
            set { leid = value; OnPropertyChanged("LegalEntityBankID"); }
        }
        [DataMember]
        private long? accid;
        public virtual long? AccountTypeID
        {
            get { return accid; }
            set { accid = value; OnPropertyChanged("AccountTypeID"); }
        }

        [DataMember]
        private long? statusid;
        public virtual long? StatusID
        {
            get { return statusid; }
            set { statusid = value; OnPropertyChanged("StatusID"); }
        }
    }
}
