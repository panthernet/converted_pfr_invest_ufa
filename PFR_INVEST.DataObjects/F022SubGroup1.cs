﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using System;

    [Obsolete("Устаревший, использовать F025SubGroup1", true)]
	public class F022SubGroup1 : BaseDataObject, IF022Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string BrokerName { get; set; }
        [DataMember]
        public virtual string StateRegNum { get; set; }
        [DataMember]
		public virtual decimal? Amount { get; set; }
	}
}
