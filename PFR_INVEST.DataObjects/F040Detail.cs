﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class F040Detail : BaseDataObject
    {
        public F040Detail() { }

        public F040Detail(F040DetailLite item)
        {
            ID = item.ID;
            EdoID = item.EdoID;
            Broker = item.Broker;
            Exchange = item.Exchange;
            BuyCount = item.BuyCount;
            SellCount = item.SellCount;
            BuyAmount = item.BuyAmount;
            SellAmount = item.SellAmount;
            DetailID = item.DetailID;
            DetailText = item.DetailText;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? EdoID { get; set; }

        /// <summary>
        /// Наименование брокера
        /// </summary>
        [DataMember]
        public virtual string Broker { get; set; }

        /// <summary>
        /// Биржа
        /// </summary>
        [DataMember]
        public virtual string Exchange { get; set; }

        [DataMember]
        public virtual decimal? BuyCount { get; set; }

        [DataMember]
        public virtual decimal? SellCount { get; set; }

        [DataMember]
        public virtual decimal? BuyAmount { get; set; }

        [DataMember]
        public virtual decimal? SellAmount { get; set; }

        [DataMember]
        public virtual long? DetailID { get; set; }

        [DataMember]
        public virtual string DetailText { get; set; }

    }        

    [DataContract]
    public class F040DetailLite : IDataObjectLite
    {
        public F040DetailLite(F040Detail item)
        {
            ID = item.ID;
            EdoID = item.EdoID;
            Broker = item.Broker;
            Exchange = item.Exchange;
            BuyCount = item.BuyCount;
            SellCount = item.SellCount;
            BuyAmount = item.BuyAmount;
            SellAmount = item.SellAmount;
            DetailID = item.DetailID;
            DetailText = item.DetailText;
        }

        public F040DetailLite(long id, long? edoID, string broker, string exchange, decimal? buyCount, decimal? sellCount, decimal? buyAmount, decimal? sellAmount, long? detailID, string detailText)
        {
            ID = id;
            EdoID = edoID;
            Broker = broker;
            Exchange = exchange;
            BuyCount = buyCount;
            SellCount = sellCount;
            BuyAmount = buyAmount;
            SellAmount = sellAmount;
            DetailID = detailID;
            DetailText = detailText;
        }



        [DataMember(Name = "A")]
        public long ID { get; set; }

        [DataMember(Name = "B")]
        public long? EdoID { get; set; }

        /// <summary>
        /// Наименование брокера
        /// </summary>
        [DataMember(Name = "C")]
        public string Broker { get; set; }

        /// <summary>
        /// Биржа
        /// </summary>
        [DataMember(Name = "D")]
        public string Exchange { get; set; }

        [DataMember(Name = "E")]
        public decimal? BuyCount { get; set; }

        [DataMember(Name = "F")]
        public decimal? SellCount { get; set; }

        [DataMember(Name = "G")]
        public decimal? BuyAmount { get; set; }

        [DataMember(Name = "H")]
        public decimal? SellAmount { get; set; }

        [DataMember(Name = "I")]
        public long? DetailID { get; set; }

        [DataMember(Name = "J")]
        public string DetailText { get; set; }
    }

}
