﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportNpfNotice : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long BranchReportID { get; set; }

        /// <summary>
        /// Link to NPF
        /// </summary>
        [DataMember]
        public virtual long LegalEntityID { get; set; }

        [DataMember]
        public virtual long NoticeTotal { get; set; }
        [DataMember]
        public virtual long ContractTotal { get; set; }

        [DataMember]
        public virtual long NoticeNew { get; set; }
        [DataMember]
        public virtual long ContractNew { get; set; }

        [DataMember]
        public virtual long NoticeExisting { get; set; }
        [DataMember]
        public virtual long ContractExisting { get; set; }

        [DataMember]
        public virtual long NoticeToNpf { get; set; }
        [DataMember]
        public virtual long ContractToNpfReturned { get; set; }

        [DataMember]
        public virtual long ContractPtkSpu { get; set; }
    }
}
