﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class License : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private string _number;
		public virtual string Number
		{
			get { return _number; }
			set { if (_number != value) { _number = value; OnPropertyChanged("Number"); } }
		}


		[DataMember]
		private DateTime? _registrationDate;
		public virtual DateTime? RegistrationDate
		{
			get { return _registrationDate; }
			set { if (_registrationDate != value) { _registrationDate = value; OnPropertyChanged("RegistrationDate"); } }
		}


		[DataMember]
		private DateTime? _closeDate;
		public virtual DateTime? CloseDate
		{
			get { return _closeDate; }
			set { if (_closeDate != value) { _closeDate = value; OnPropertyChanged("CloseDate"); } }
		}


		[DataMember]
		private long? _legalEntityID;
		public virtual long? LegalEntityID
		{
			get { return _legalEntityID; }
			set { if (_legalEntityID != value) { _legalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
		}


		[DataMember]
		private DateTime? _licDate;
		public virtual DateTime? LicDate
		{
			get { return _licDate; }
			set { if (_licDate != value) { _licDate = value; OnPropertyChanged("LicDate"); } }
		}


		[DataMember]
		private string _registrator;
		public virtual string Registrator
		{
			get { return _registrator; }
			set { if (_registrator != value) { _registrator = value; OnPropertyChanged("Registrator"); } }
		}


		[DataMember]
		private long? _duration;
		public virtual long? Duration
		{
			get { return _duration; }
			set { if (_duration != value) { _duration = value; OnPropertyChanged("Duration"); } }
		}

	}
}
