﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportInsuredPersonType : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }
    }
}
