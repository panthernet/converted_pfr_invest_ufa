﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Branch : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Address { get; set; }

        [DataMember]
        public virtual string PostAddress { get; set; }

        [DataMember]
        public virtual string Phone { get; set; }
    }
}
