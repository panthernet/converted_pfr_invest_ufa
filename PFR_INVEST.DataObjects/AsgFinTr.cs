﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    /// <summary>
	/// Общее п/п (Ранее п/п для НПФ)
	/// </summary>
	public class AsgFinTr : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ContragentTypeID { get; set; } = (long) ContragentTypeEnum.None;

        public enum ContragentTypeEnum
	    {
	        None = 116001,
            UK = 116002,
            NPF = 116003,
            OPFR = 116004
        }

		// Element (Key = 101)
		public enum Directions
		{
			ToPFR = 101001, //Приход
			FromPFR = 101002 //Расход
		}

		//Element (Key = 100)
		public enum Links
		{
			NoLink = 100001,//'нет связи'    
			UK = 100002,//'Перечисления УК'    
			NPF = 100003,//'Передача СПН НПФ'    
			Deposit = 100004,//'Депозиты (размещение/возврат)'    
			Coupon = 100005,//'Купонный доход'    
			Budget = 100006,//'Средства Федерального Бюджета (МСК, Софинансирование)'    
			Region = 100007,//'Финансирование регионов (финансирование/возврат)',
            OPFR = 100008 // Перечисления ОПФР
		}

		//Element (Key = 102)
		public enum Sections
		{
			/// <summary>
			/// Работа с НПФ
			/// </summary>
			NPF = 102001,
			/// <summary>
			/// Работа с СИ
			/// </summary>
			SI = 102002,
			/// <summary>
			/// Работа с ВР
			/// </summary>
			VR = 102003,
			/// <summary>
			/// Работа с ЦБ и депозитами
			/// </summary>
			CB = 102004,
			/// <summary>
			/// Работа с регионами
			/// </summary>
			Region = 102005,
			/// <summary>
			/// Бэк-офис
			/// </summary>
			BackOffice = 102006,
			/// <summary>
			/// Все
			/// </summary>
			All = 102007
		}

		public AsgFinTr()
		{
			DIDate = DateTime.Today;
		}

        /// <summary>
        /// Очищает связь с перечислением и финреестром
        /// </summary>
	    public virtual void ClearLink()
	    {
            //аналогичная логика есть чистыми запросами к БД в DeleteSIVRRegister() и DeleteFinregisterInternal()
	        ReqTransferID = null;
	        FinregisterID = null;
            LinkedRegisterID = null;
            LinkElID = (long)Links.NoLink;
	    }

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		public virtual long? FinregisterID { get; set; }

		/// <summary>
		/// Сумма ПП
		/// </summary>
		[DataMember]
		public virtual decimal? DraftAmount { get; set; }

		/// <summary>
		/// Дата ПП
		/// </summary>
		[DataMember]
		public virtual DateTime? DraftDate { get; set; }


		[DataMember]
		public virtual DateTime? DIDate { get; set; }

		/// <summary>
		/// Номер ПП
		/// </summary>
		[DataMember]
		public virtual string DraftRegNum { get; set; }

		/// <summary>
		/// Дата перечисления СПН
		/// </summary>
		[DataMember]
		public virtual DateTime? DraftPayDate { get; set; }

		[DataMember]
		public virtual long? PFRBankAccountID { get; set; }

		[DataMember]
		public virtual string Comment { get; set; }

		[DataMember]
		public virtual long? PortfolioID { get; set; }

		//Промежуточные поля, мапинг старый - имена новые

		[DataMember]
		public virtual DateTime? PayDate { get { return DraftDate; } set { DraftDate = value; } }

		[DataMember]
		public virtual string PayNumber { get { return DraftRegNum; } set { DraftRegNum = value; } }

		[DataMember]
		public virtual decimal? PaySum { get { return DraftAmount; } set { DraftAmount = value; } }

		[DataMember]
		public virtual DateTime? SPNDate { get { return DraftPayDate; } set { DraftPayDate = value; } }


		//Связи


		/// <summary>
		/// Ссылка на направление передачи (ELEMENT)
		/// </summary>
		[DataMember]
		public virtual long? DirectionElID { get; set; }


		/// <summary>
		/// Ссылка на отдел ДОКИП (ELEMENT) 
		/// </summary>
		[DataMember]
		public virtual long? SectionElID { get; set; }


		/// <summary>
		/// Ссылка на связь (ELEMENT)
		/// </summary>
		[DataMember]
		public virtual long? LinkElID { get; set; }

		/// <summary>
		/// Ссылка на содерожание операции (PaymentDetail)
		/// </summary>
		[DataMember]
		public virtual long? PaymentDetailsID { get; set; }

		[DataMember]
		public virtual long? KBKID { get; set; }

		[DataMember]
		public virtual int Unlinked { get; set; }

		/// <summary>
		/// Таблица лотуса, откуда смигрирована запись, только для чтения
		/// </summary>
		[DataMember]
		public virtual string LotusTable { get; set; }

		/// <summary>
		/// Ссылка на связаный реестр
		/// </summary>
		[DataMember]
		public virtual long? LinkedRegisterID { get; set; }


		[DataMember]
		public virtual long? ReqTransferID { get; set; }

		[IgnoreDataMember]
		public virtual bool IsMigrated
		{
			get { return LotusTable != null; }
		}

		[IgnoreDataMember]
		public virtual bool IsMigratedFromSaldo
		{
			get { return IsMigrated && (LotusTable.Equals("TRANSFERUKFORM___ACCOUNTS", StringComparison.OrdinalIgnoreCase) 
										|| LotusTable.Equals("TRANSFERUKFORM___ACCOUNTS_50", StringComparison.OrdinalIgnoreCase)
										|| LotusTable.Equals("TRANSFERNPFFORM___ACCOUNTS", StringComparison.OrdinalIgnoreCase)
										|| LotusTable.Equals("TRANSFERNPFFORM___ACCOUNTS_50", StringComparison.OrdinalIgnoreCase));
			}
		}

		[DataMember]
		public virtual long StatusID { get; set; }

        [DataMember]
        public virtual long? InnContractID { get; set; }

        [DataMember]
        public virtual long? InnLegalEntityID { get; set; }

        [DataMember]
        public virtual DateTime? PayerIssueDate { get; set; }

        [DataMember]
        public virtual string CBPaymentDetail { get; set; }

        [IgnoreDataMember]
        public virtual bool IsSelected { get; set; }
		[IgnoreDataMember]
        public virtual bool IsSelectable { get { return !string.IsNullOrEmpty(ContragentName); } }
        [IgnoreDataMember]
        public virtual string ContragentName { get; set; }

	}
}
