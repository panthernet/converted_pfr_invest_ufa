﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DebuggerDisplay("Name = '{ShortName}', ID = {ID}")]
	public class LegalEntity
        : BaseDataObject
	{
        public const string INCLUDED = "Внесен в реестр участников";
        public const string NOT_INCLUDED = "Не внесен в реестр участников";

        //Element (Key = 103)
        public enum GarantACB
        {
            /// <summary>
			/// Внесен (103001)
            /// </summary>
            Included = 103001,
            /// <summary>
			/// Не внесен (103002)
            /// </summary>
            NotIncluded = 103002
        }

		[DataMember]
		public virtual long ID { get; set; }

        [DataMember]
        private int? _numberForDocuments;
        public virtual int? NumberForDocuments
        {
            get { return _numberForDocuments; }
            set { if (_numberForDocuments != value) { _numberForDocuments = value; OnPropertyChanged("NumberForDocuments"); } }            
        }

		[DataMember]
		private string _pdreason;
		public virtual string PDReason
		{
			get { return _pdreason; }
			set { if (_pdreason != value) { _pdreason = value; OnPropertyChanged("PDReason"); } }
		}

		[DataMember]
		private string _fullName;
		public virtual string FullName
		{
			get { return _fullName; }
			set { if (_fullName != value) { _fullName = value; OnPropertyChanged("FullName"); } }
		}


		[DataMember]
		private string _inn;
		public virtual string INN
		{
			get { return _inn; }
			set { if (_inn != value) { _inn = value; OnPropertyChanged("INN"); } }
		}


		[DataMember]
		private string _headPosition;
		public virtual string HeadPosition
		{
			get { return _headPosition; }
			set { if (_headPosition != value) { _headPosition = value; OnPropertyChanged("HeadPosition"); } }
		}


		[DataMember]
		private string _headFullName;
		public virtual string HeadFullName
		{
			get { return _headFullName; }
			set { if (_headFullName != value) { _headFullName = value; OnPropertyChanged("HeadFullName"); } }
		}


		[DataMember]
		private string _legalAddress;
		public virtual string LegalAddress
		{
			get { return _legalAddress; }
			set { if (_legalAddress != value) { _legalAddress = value; OnPropertyChanged("LegalAddress"); } }
		}


		[DataMember]
		private string _streetAddress;
		public virtual string StreetAddress
		{
			get { return _streetAddress; }
			set { if (_streetAddress != value) { _streetAddress = value; OnPropertyChanged("StreetAddress"); } }
		}


		[DataMember]
		private string _postAddress;
		public virtual string PostAddress
		{
			get { return _postAddress; }
			set { if (_postAddress != value) { _postAddress = value; OnPropertyChanged("PostAddress"); } }
		}


		[DataMember]
		private string _phone;
		public virtual string Phone
		{
			get { return _phone; }
			set { if (_phone != value) { _phone = value; OnPropertyChanged("Phone"); } }
		}


		[DataMember]
		private string _fax;
		public virtual string Fax
		{
			get { return _fax; }
			set { if (_fax != value) { _fax = value; OnPropertyChanged("Fax"); } }
		}


		[DataMember]
		private string _comment;
		public virtual string Comment
		{
			get { return _comment; }
			set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
		}


		[DataMember]
		private string _eAddress;
		public virtual string EAddress
		{
			get { return _eAddress; }
			set { if (_eAddress != value) { _eAddress = value; OnPropertyChanged("EAddress"); } }
		}


		[DataMember]
		private long? _contragentID;
		public virtual long? ContragentID
		{
			get { return _contragentID; }
			set { if (_contragentID != value) { _contragentID = value; OnPropertyChanged("ContragentID"); } }
		}


		[DataMember]
		private DateTime? _registrationDate;
		public virtual DateTime? RegistrationDate
		{
			get { return _registrationDate; }
			set { if (_registrationDate != value) { _registrationDate = value; OnPropertyChanged("RegistrationDate"); } }
		}

		[DataMember]
        private DateTime? _endDate;
        public virtual DateTime? EndDate
		{
            get { return _endDate; }
            set { if (_endDate != value) { _endDate = value; OnPropertyChanged("EndDate"); } }
		}
        

		[DataMember]
		private DateTime? _closeDate;
		public virtual DateTime? CloseDate
		{
			get { return _closeDate; }
			set { if (_closeDate != value) { _closeDate = value; OnPropertyChanged("CloseDate"); } }
		}


		[DataMember]
		private string _shortName;
		public virtual string ShortName
		{
			get { return _shortName; }
			set { if (_shortName != value) { _shortName = value; OnPropertyChanged("ShortName"); } }
		}


		[DataMember]
		private string _registrationNum;
		public virtual string RegistrationNum
		{
			get { return _registrationNum; }
			set { if (_registrationNum != value) { _registrationNum = value; OnPropertyChanged("RegistrationNum"); } }
		}


		[DataMember]
		private string _formalizedName;
		public virtual string FormalizedName
		{
			get { return _formalizedName; }
			set { if (_formalizedName != value) { _formalizedName = value; OnPropertyChanged("FormalizedName"); } }
		}


		[DataMember]
		private string _site;
		public virtual string Site
		{
			get { return _site; }
			set { if (_site != value) { _site = value; OnPropertyChanged("Site"); } }
		}


		[DataMember]
		private string _subsidiaries;
		public virtual string Subsidiaries
		{
			get { return _subsidiaries; }
			set { if (_subsidiaries != value) { _subsidiaries = value; OnPropertyChanged("Subsidiaries"); } }
		}


		[DataMember]
		private string _registrator;
		public virtual string Registrator
		{
			get { return _registrator; }
			set { if (_registrator != value) { _registrator = value; OnPropertyChanged("Registrator"); } }
		}


		[DataMember]
		private string _okpp;
		public virtual string OKPP
		{
			get { return _okpp; }
			set { if (_okpp != value) { _okpp = value; OnPropertyChanged("OKPP"); } }
		}


		[DataMember]
		private string _declName;
		public virtual string DeclName
		{
			get { return _declName; }
			set { if (_declName != value) { _declName = value; OnPropertyChanged("DeclName"); } }
		}


		[DataMember]
		private string _letterWho;
		public virtual string LetterWho
		{
			get { return _letterWho; }
			set { if (_letterWho != value) { _letterWho = value; OnPropertyChanged("LetterWho"); } }
		}


		[DataMember]
		private string _transfDocKind;
		public virtual string TransfDocKind
		{
			get { return _transfDocKind; }
			set { if (_transfDocKind != value) { _transfDocKind = value; OnPropertyChanged("TransfDocKind"); } }
		}


		[DataMember]
		private long? _legalStatusID;
		public virtual long? LegalStatusID
		{
			get { return _legalStatusID; }
			set { if (_legalStatusID != value) { _legalStatusID = value; OnPropertyChanged("LegalStatusID"); } }
		}


		[DataMember]
		private long? _pfbaID;
		public virtual long? PFBA_ID
		{
			get { return _pfbaID; }
			set { if (_pfbaID != value) { _pfbaID = value; OnPropertyChanged("PFBA_ID"); } }
		}


		[DataMember]
		private string _info;
		public virtual string Info
		{
			get { return _info; }
			set { if (_info != value) { _info = value; OnPropertyChanged("Info"); } }
		}


		[DataMember]
		private string _pfragr;
        /// <summary>
        /// № соглашения с ПФР
        /// </summary>
		public virtual string PFRAGR
		{
			get { return _pfragr; }
			set { if (_pfragr != value) { _pfragr = value; OnPropertyChanged("PFRAGR"); } }
		}


        [DataMember]
        private DateTime? _pfragrdate;
        /// <summary>
        /// Дата соглашения с ПФР
        /// </summary>
        public virtual DateTime? PFRAGRDATE
        {
            get { return _pfragrdate; }
            set { if (_pfragrdate != value) { _pfragrdate = value; OnPropertyChanged("PFRAGRDATE"); } }
        }


        [DataMember]
		private string _pfragrstat;
		public virtual string PFRAGRSTAT
		{
			get { return _pfragrstat; }
			set { if (_pfragrstat != value) { _pfragrstat = value; OnPropertyChanged("PFRAGRSTAT"); } }
		}


		[DataMember]
		private decimal? _money;
		public virtual decimal? Money
		{
			get { return _money; }
			set { if (_money != value) { _money = value; OnPropertyChanged("Money"); } }
		}


		[DataMember]
		private DateTime? _moneyDate;
		public virtual DateTime? MoneyDate
		{
			get { return _moneyDate; }
			set { if (_moneyDate != value) { _moneyDate = value; OnPropertyChanged("MoneyDate"); } }
		}


		[DataMember]
		private string _fitch;
		public virtual string Fitch
		{
			get { return _fitch; }
			set { if (_fitch != value) { _fitch = value; OnPropertyChanged("Fitch"); } }
		}


		[DataMember]
		private DateTime? _fitchDate;
		public virtual DateTime? FitchDate
		{
			get { return _fitchDate; }
			set { if (_fitchDate != value) { _fitchDate = value; OnPropertyChanged("FitchDate"); } }
		}


		[DataMember]
		private string _standard;
		public virtual string Standard
		{
			get { return _standard; }
			set { if (_standard != value) { _standard = value; OnPropertyChanged("Standard"); } }
		}


		[DataMember]
		private DateTime? _standardDate;
		public virtual DateTime? StandardDate
		{
			get { return _standardDate; }
			set { if (_standardDate != value) { _standardDate = value; OnPropertyChanged("StandardDate"); } }
		}


		[DataMember]
		private string _moody;
		public virtual string Moody
		{
			get { return _moody; }
			set { if (_moody != value) { _moody = value; OnPropertyChanged("Moody"); } }
		}


		[DataMember]
		private DateTime? _moodyDate;
		public virtual DateTime? MoodyDate
		{
			get { return _moodyDate; }
			set { if (_moodyDate != value) { _moodyDate = value; OnPropertyChanged("MoodyDate"); } }
		}


		[DataMember]
		private string _corrAcc;
		public virtual string CorrAcc
		{
			get { return _corrAcc; }
			set { if (_corrAcc != value) { _corrAcc = value; OnPropertyChanged("CorrAcc"); } }
		}

        [DataMember]
		private string _corrNote;
		public virtual string CorrNote
		{
			get { return _corrNote; }
			set { if (_corrNote != value) { _corrNote = value; OnPropertyChanged("CorrNote"); } }
		}
        

		[DataMember]
		private string _bik;
		public virtual string BIK
		{
			get { return _bik; }
			set { if (_bik != value) { _bik = value; OnPropertyChanged("BIK"); } }
		}


		[DataMember]
		private string _forLetter;
		public virtual string ForLetter
		{
			get { return _forLetter; }
			set { if (_forLetter != value) { _forLetter = value; OnPropertyChanged("ForLetter"); } }
		}


		[DataMember]
		private long? _sex;
		public virtual long? Sex
		{
			get { return _sex; }
			set { if (_sex != value) { _sex = value; OnPropertyChanged("Sex"); } }
		}

        [DataMember]
        private string _stockCode;
        public virtual string StockCode
        {
            get { return _stockCode; }
            set { if (_stockCode != value) { _stockCode = value; OnPropertyChanged("StockCode"); } }
        }

        [DataMember]
        private decimal? _limit4Money;
        public virtual decimal? Limit4Money
        {
            get { return _limit4Money; }
            set { _limit4Money = value; }
        }

        /// <summary>
        /// Признак внесения НПФ в реестр фондов участников (ELEMENT) 
        /// </summary>
        [DataMember]
        private long? _garantAcbid = (long)GarantACB.NotIncluded;
        public virtual long? GarantACBID
		{
            get { return _garantAcbid; }
            set { if (_garantAcbid != value) { _garantAcbid = value; OnPropertyChanged("GarantACBID"); } }
		}


        [DataMember]
        private DateTime? _fundDate;
        public virtual DateTime? FundDate
        {
            get { return _fundDate; }
            set { if (_fundDate != value) { _fundDate = value; OnPropertyChanged("FundDate"); } }
        }

        public virtual long? FundNum { get; set; }

        [DataMember]
        private DateTime? _notifDate;
        public virtual DateTime? NotifDate
        {
            get { return _notifDate; }
            set { if (_notifDate != value) { _notifDate = value; OnPropertyChanged("NotifDate"); } }
        }

        [DataMember]
        private string _notifNum;
        public virtual string NotifNum 
        {
            get { return _notifNum; }
            set { if (_notifNum != value) { _notifNum = value; OnPropertyChanged("NotifNum"); } }
        }


		//[DataMember]
		//public virtual long? BankAccountID { get; set; }

		[DataMember]
		public virtual string OldName { get; set; }

		//Костыль, что-бы забиндить колонку грида на обьект
		[IgnoreDataMember]
		public virtual LegalEntity self => this;

	    [IgnoreDataMember]
		public virtual string FormalizedNameFull => GetFormalizedNameFull(this.FormalizedName, this.OldName);

	    [IgnoreDataMember]
        public virtual string GarantName
        {
            get
            {
                if (_garantAcbid != null && _garantAcbid.Value == (long)GarantACB.Included)
                    return INCLUDED;
                return NOT_INCLUDED;
            }
        }
        
        public static string GetFormalizedNameFull(string formalizedName, string oldName)
        {
            return string.IsNullOrEmpty(oldName) ? formalizedName : $"{formalizedName} ({oldName})";
        }
	}
}
