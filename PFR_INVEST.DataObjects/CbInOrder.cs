﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

    public class CbInOrder : BaseDataObject, ICloneable
    {
        /// <summary>
        /// ID
        /// </summary>
        [DataMember]
        public virtual long ID { get; set; }

        /// <summary>
        /// ID ценной бумаги (Security)
        /// </summary>
        [DataMember]
        public virtual long? SecurityID { get; set; }

        /// <summary>
        /// ID поручения (CbOrder)
        /// </summary>
        [DataMember]
        public virtual long? OrderID { get; set; }

        /// <summary>
        /// Общая номинальная стоимость ЦБ
        /// </summary>
        [DataMember]
        public virtual decimal? NomValue { get; set; }

        /// <summary>
        /// Макс общая стоимость ЦБ (с НКД)
        /// </summary>
        [DataMember]
        public virtual decimal? MaxNomValue { get; set; }

        /// <summary>
        /// Макс цена (в %, без НКД)
        /// </summary>
        [DataMember]
        public virtual decimal? MaxPrice { get; set; }

        /// <summary>
        /// Мин общая стоимость ЦБ (с НКД)
        /// </summary>
        [DataMember]
        public virtual decimal? MinValueNKD { get; set; }

        /// <summary>
        /// Мин цена (в %, без НКД)
        /// </summary>
        [DataMember]
        public virtual decimal? MinPrice { get; set; }

        /// <summary>
        /// Количество штук
        /// </summary>
        [DataMember]
        public virtual long? Count { get; set; }
        /// <summary>
        /// Фикс. цена покупки
        /// </summary>
        [DataMember]
        public virtual decimal? FixPriceBay { get; set; }

        /// <summary>
        /// Макс. цена покупки
        /// </summary>
        [DataMember]
        public virtual decimal? MaxPriceBay { get; set; }

        /// <summary>
        /// Мин. цена продажи
        /// </summary>
        [DataMember]
        public virtual decimal? MinPriceSell { get; set; }



        /// <summary>
        /// Сумма денежных средств
        /// </summary>
        [DataMember]
        public virtual decimal? SumMoney { get; set; }

        public virtual object Clone()
        {
            CbInOrder ret = new CbInOrder()
            {
                ID = this.ID
            };
            if (this.ExtensionData != null)
                ret.ExtensionData = new Dictionary<string, object>(this.ExtensionData);
            return ret;
        }
        /// <summary>
        /// Дата создания карточки
        /// </summary>
        [DataMember]
        public virtual DateTime ? DateDI { get; set; }
    }
}
