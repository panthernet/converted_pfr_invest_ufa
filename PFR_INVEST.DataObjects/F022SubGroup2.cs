﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using System;

    [Obsolete("Устаревший, использовать F025SubGroup2", true)]
	public class F022SubGroup2 : BaseDataObject, IF022Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string IssuerName { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
		[DataMember]
		public virtual string StateRegNum { get; set; }

	}
}
