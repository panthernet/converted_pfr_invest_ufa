﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace PFR_INVEST.DataObjects
{

    public class ModelContactRecord : BaseDataObject
    {
        [DataMember]
        public virtual long ContactID {get;set;}

        [DataMember]
        public virtual string UkName {get;set;}
        
        [DataMember]
        public virtual string FIO {get;set;}
        
        [DataMember]
        public virtual string Email {get;set;}
        
    }

}
