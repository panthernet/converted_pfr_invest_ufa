﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DebuggerDisplay("[{ID}]-[{PortfolioID}] - {PortfolioName}")]
    public class Budget : BaseDataObject
    {
        public Budget()
        {
            DIDate = DateTime.Today;
        }

        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual DateTime? Date { get; set; }

        /// <summary>
        /// Дата проведения операции (создания карточки).
        /// </summary>
        [DataMember]
        public virtual DateTime? DIDate { get; set; }

        [DataMember]
        public virtual decimal? Sum { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        /// <summary>
        /// Используется только для вычитки из базы
        /// </summary>
        [DataMember]
        public virtual string PortfolioName { get; set; }
    }
}
