﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    using System;

    [Obsolete("Устаревший, использовать F025SubGroup3", true)]
	public class F022SubGroup3 : BaseDataObject, IF022Part
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? EdoID { get; set; }
		[DataMember]
		public virtual string DebName { get; set; }
		[DataMember]
		public virtual decimal? Amount { get; set; }
	}
}
