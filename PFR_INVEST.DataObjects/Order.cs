﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	// сущность CbOrder соответствует таблице "ORDER"
	// имя класса изменено из-за конфликта с NHibernate.Criterion.Order
	public class CbOrder : BaseDataObject
	{
		/// <summary>
		/// ID
		/// </summary>
		[DataMember]
		public virtual long ID { get; set; }

		/// <summary>
		/// ID портфеля
		/// </summary>
		[DataMember]
		public virtual long? PortfolioID { get; set; }

		/// <summary>
		/// ID торгового счета (PFRBankAccount) 
		/// </summary>
		[DataMember]
		public virtual long? TradeAccountID { get; set; }

		/// <summary>
		/// ID счета ДЕПО (PFRBankAccount) 
		/// </summary>
		[DataMember]
		public virtual long? DEPOAccountID { get; set; }

		/// <summary>
		/// ID счета Текущий (PFRBankAccount) 
		/// </summary>
		[DataMember]
		public virtual long? CurrentAccountID { get; set; }

		/// <summary>
		/// ID счета Транзитный (PFRBankAccount) 
		/// </summary>
		[DataMember]
		public virtual long? TransitAccountID { get; set; }

		/// <summary>
		/// ID счета банка-агента (LegalEntity) 
		/// </summary>
		[DataMember]
		public virtual long? BankAgentID { get; set; }

		/// <summary>
		/// Тип операции 
		/// </summary>
		[DataMember]
		public virtual string Type { get; set; }

		/// <summary>
		/// Уполномоченный от ПФР
		/// </summary>
		[DataMember]
		public virtual string Commissioner { get; set; }

		/// <summary>
		/// Комментарии
		/// </summary>
		[DataMember]
		public virtual string Comment { get; set; }

		/// <summary>
		/// Статус поручения
		/// </summary>
		[DataMember]
		public virtual string Status { get; set; }

		/// <summary>
		/// Номер поручения
		/// </summary>
		[DataMember]
		public virtual string RegNum { get; set; }

		/// <summary>
		/// Номер поручения уникальный (для перекладки)
		/// </summary>
		[DataMember]
		public virtual long RegNumLong { get; set; }

		/// <summary>
		/// Дата поручения
		/// </summary>
		[DataMember]
		public virtual DateTime? RegDate { get; set; }

		/// <summary>
		/// Исполнить по
		/// </summary>
		[DataMember]
		public virtual DateTime? Term { get; set; }

		/// <summary>
		/// Место покупки
		/// </summary>
		[DataMember]
		public virtual string Place { get; set; }

		/// <summary>
		/// Дествителен с
		/// </summary>
		[DataMember]
		public virtual DateTime? Start { get; set; }

		/// <summary>
		/// Дата создания поручения
		/// </summary>
		[DataMember]
		public virtual DateTime? DIDate { get; set; }

		/// <summary>
		/// Дествителен с
		/// </summary>
		[DataMember]
		public virtual TimeSpan? StartTime { get; set; }

		/// <summary>
		/// ID ФИО
		/// </summary>
		[DataMember]
		public virtual long? FIO_ID { get; set; }

		/// <summary>
		/// ID должности
		/// </summary>
		[DataMember]
		public virtual long? PostID { get; set; }

		/// <summary>
		/// Сумма неконкурентной заявки
		/// </summary>
		[DataMember]
		public virtual decimal? NoncompReq { get; set; }

		/// <summary>
		/// для операции "перекладка" может существовать список ордеров на покупку для одного ордера на продажу,
		/// которые связаны между собой через PARENT_ID
		/// </summary>
		[DataMember]
		public virtual long? ParentId { get; set; }


		[DataMember]
		public virtual string LotusTable { get; set; }

		[IgnoreDataMember]
		public virtual bool IsReadOnly
		{
			get { return !string.IsNullOrEmpty(LotusTable); }
		}
	}
}
