﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Incomesec : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? PFRBankAccountID { get; set; }

        [DataMember]
        public virtual DateTime? OperationDate { get; set; }

        [DataMember]
        public virtual string IncomeKind { get; set; }

        [DataMember]
        public virtual DateTime? OrderDate { get; set; }

        [DataMember]
        public virtual string OrderNumber { get; set; }

        [DataMember]
        public virtual decimal? OrderSum { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        [DataMember]
        public virtual long? PortfolioID { get; set; }

        [DataMember]
        public virtual long? CursID { get; set; }
    }
}
