﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SimpleObjectValue:BaseDataObject
    {
        private long _ID;
        [DataMember]
        public virtual long ID
        {
            get { return _ID; }
            set { if (_ID != value) { _ID = value; OnPropertyChanged("ID"); } }
        }


        private string _Name;
        [DataMember]
        public virtual string Name
        {
            get { return _Name; }
            set { if (_Name != value) { _Name = value; OnPropertyChanged("Name"); } }
        }


    }
}
