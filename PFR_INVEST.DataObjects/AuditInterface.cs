﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class AuditInterface : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }
		[DataMember]
		public virtual long? UIID { get; set; }
		[DataMember]
		public virtual long? EventID { get; set; }
	}
}
