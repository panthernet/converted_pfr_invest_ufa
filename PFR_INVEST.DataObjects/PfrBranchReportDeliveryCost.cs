﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class PfrBranchReportDeliveryCost : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long BranchReportID { get; set; }

        [DataMember]
        public virtual decimal Sum_221 { get; set; }

        [DataMember]
        public virtual decimal Sum_226 { get; set; }

        [DataMember]
        public virtual decimal Total { get; set; }
    }
}
