﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class SITransfer : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? InsuredPersonsCount { get; set; }

        [DataMember]
        public virtual long? InsuredPersonsCountBeforeOperation { get; set; }

        [DataMember]
        public virtual decimal? PlannedTransferSum { get; set; }

        [DataMember]
        public virtual long? TransferRegisterID { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

        [DataMember]
        public virtual long StatusID { get; set; }
    }
}
