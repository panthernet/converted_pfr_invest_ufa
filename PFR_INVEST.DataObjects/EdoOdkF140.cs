﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class EdoOdkF140 : BaseDataObject
    {
        public enum IDReportType
        {
            /// <summary>
            /// 1 - Уведомление о фактах нарушения законодательства, нормативно правовых актов
            /// </summary>
            Report = 1,

            /// <summary>
            /// 2 - Уведомление об устранении нарушения законодательства, нормативно правовых актов
            /// </summary>
            StopReport = 2,

            /// <summary>
            /// 3 - Уведомление о расхождении в результате сверки
            /// </summary>
            DivergenceReport = 3,

            /// <summary>
            /// 4 - Уведомление об устранении расхождений
            /// </summary>
            StopDivergenceReport = 4,

            /// <summary>
            /// Уведомление об  неустранении нарушения в срок
            /// </summary>
            NoStopReport = 5,
        }


        public static string GetReportType(int id)
        {

            switch (id)
            {
                case 1:
                    return "Уведомление о фактах нарушения законодательства, нормативно правовых актов";
                case 2:
                    return "Уведомление об устранении нарушения законодательства, нормативно правовых актов";
                case 3:
                    return "Уведомление о расхождении в результате сверки";
                case 4:
                    return "Уведомление об устранении расхождений";
                case 5:
                    return "Уведомление об  неустранении нарушения в срок";

                default:
                    return string.Empty;
            }
        }


        [DataMember]
        public virtual long ID { get; set; }
        [DataMember]
        public virtual string UKName { get; set; }
        [DataMember]
        public virtual string UKNameFull { get; set; }
        [DataMember]
        public virtual string UKINN { get; set; }


        [DataMember]
        private string _ukFilial;
        public virtual string UKFilial
        {
            get
            {
                return string.IsNullOrEmpty(_ukFilial) ? "00" : _ukFilial;
            }
            set
            {
                _ukFilial = value;
            }
        }

        [DataMember]
        public virtual DateTime? TheDate { get; set; }
        [DataMember]
        public virtual string DocNumber { get; set; }
        [DataMember]
        public virtual string BaseDocNumber { get; set; }
        [DataMember]
        public virtual string LinkReport { get; set; }
        [DataMember]
        public virtual string LinkStopReport { get; set; }
        [DataMember]
        public virtual string LinkNoStopReport { get; set; }
        [DataMember]
        public virtual DateTime? BaseDocDate { get; set; }
        [DataMember]
        public virtual string InvestcaseName { get; set; }
        [DataMember]
        public virtual DateTime? ViolationDate { get; set; }
        [DataMember]
        public virtual string ViolationType { get; set; }
        [DataMember]
        public virtual string ViolationNorma { get; set; }
        [DataMember]
        public virtual string ViolationFact { get; set; }
        [DataMember]
        public virtual string SpecDepTreat { get; set; }
        [DataMember]
        public virtual DateTime? StopDate { get; set; }
        [DataMember]
        public virtual DateTime? RealStopDate { get; set; }
        [DataMember]
        public virtual DateTime? DateOfReport { get; set; }
        [DataMember]
        public virtual string OperatorPost { get; set; }
        [DataMember]
        public virtual string OperatorFIO { get; set; }
        [DataMember]
        public virtual DateTime? StopDateDay { get; set; }
        [DataMember]
        public virtual string ContentReport { get; set; }
        [DataMember]
        public virtual string ContractNumber { get; set; }
        [DataMember]
        public virtual DateTime? ContractDate { get; set; }
        [DataMember]
        public virtual long? ReportTypeID { get; set; }
        [DataMember]
        public virtual string LicenseNfnpf { get; set; }
        [DataMember]
        public virtual string RegNumberOut { get; set; }
        [DataMember]
        public virtual long? CategoryId { get; set; }
        [DataMember]
        public virtual string Infirm1 { get; set; }
        [DataMember]
        public virtual string Infirm2 { get; set; }
        [DataMember]
        public virtual string RegNum { get; set; }
        [DataMember]
        public virtual DateTime? RegDate { get; set; }
        [DataMember]
        public virtual string Status { get; set; }
        [DataMember]
        public virtual string StopNumber { get; set; }
        [DataMember]
        public virtual string NoStopNumber { get; set; }
        [DataMember]
        public virtual string RealStopValue { get; set; }
        [DataMember]
        public virtual DateTime? StopReport { get; set; }
        [DataMember]
        public virtual DateTime? NoStopDate { get; set; }
        [DataMember]
        public virtual long ContractID { get; set; }

    }
}
