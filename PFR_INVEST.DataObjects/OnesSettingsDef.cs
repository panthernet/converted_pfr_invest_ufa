﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class OnesSettingsDef : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string ExportGroup { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Comment { get; set; }

        public class Groups
        {
            public const string UK = "UK";
            public const string VR = "VR";
            public const string NPF = "NPF";
            public const string DEPOSIT = "DEPOSIT";
        }

        public enum IntGroup
        {
            Npf = 0,
            Si,
            Vr,
            Depo,
            All,
            Opfr,
        }
    }
}
