﻿using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace PFR_INVEST.DataObjects
{
    public class KBKHistory : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long KBKId { get; set; }
       
        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual DateTime Date { get; set; }

        [IgnoreDataMember]
        public virtual string MaskedCode
        {
            get
            {
                if (Code == null || !Regex.IsMatch(Code, "\\d{20}")) return Code;
                var p1 = Convert.ToInt64(Code.Substring(0, 10));
                var p2 = Convert.ToInt64(Code.Substring(10, 10));

                return string.Format("{0}{1}", p1.ToString("000 0 00 0000"), p2.ToString("0 00 0000 000"));
            }
            set
            {
                string v = null;
                if (value != null)
                {
                    v = value.Replace(" ", "");
                }
                if (Code != v)
                {
                    Code = v;
                    OnPropertyChanged("Code");
                }
            }
        }
    }
}
