﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class KipLinks : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

		[DataMember]
		public virtual long? KipRegisterID { get; set; }

        [DataMember]
        public virtual long? LegalEntityID { get; set; }

        [DataMember]
        public virtual long? ContractID { get; set; }

		[DataMember]
		public virtual long? FinregisterID { get; set; }

		[DataMember]
		public virtual long? SITransferID { get; set; }

		[DataMember]
		public virtual string OrganizationCode { get; set; }

		[DataMember]
		public virtual string PortfolioCode { get; set; }

    }
}
