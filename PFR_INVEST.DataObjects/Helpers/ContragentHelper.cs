﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.DataObjects.Helpers
{
    public static class ContragentHelper
    {
        /// <summary>
        /// Обновление статуса контрагента, используется в карточках контракта и субъекта СИ/ВР
        /// </summary>
        /// <param name="contragent">Контрагент</param>
        /// <param name="contracts">Список договоров</param>
        public static void UpdateContragentStatus(Contragent contragent, List<Contract> contracts)
        {
            if (contracts.Any(c => c.DissolutionDate == null) || contracts.Any(c => c.DissolutionDate.HasValue && c.DissolutionDate.Value.Date > DateTime.Now.Date))
                contragent.StatusID = (long)StatusIdentifier.Identifier.Active;
            else
                contragent.StatusID = (long)StatusIdentifier.Identifier.Eliminated;
        }

        public static bool IsContractMarkedRed(DateTime? date)
        {
            return date != null && date != DateTime.MinValue && date.Value.Date < DateTime.Now.Date;
        }
    }
}
