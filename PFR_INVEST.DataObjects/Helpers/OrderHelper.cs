﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.DataObjects.Helpers
{
	public class OrderReportsHelper
	{
		public static Payment GetClosestPayment(DateTime date, List<Payment> payments)
		{
			var list = (from pay in payments
						orderby pay.PaymentDate ascending
						where pay.PaymentDate >= date
						select pay);
			if (list.Count() == 0) return null;
			else return list.First();
		}

		public static decimal CalcNKDForRubles(DateTime dateOrder, Payment closestPayment)
		{
			if (dateOrder == closestPayment.PaymentDate.Value.Date)
				return 0;

			var nkd = closestPayment.CouponSize.Value *
				   ((dateOrder - closestPayment.PaymentDate.Value).Days + closestPayment.CouponPeriod.Value)
				   / closestPayment.CouponPeriod.Value;

			//НКД на одну аблигацию принудительно округлять до 2х знаков после запятой.
			return Math.Round(nkd, 2, MidpointRounding.AwayFromZero);
		}

		public static decimal CalcNKDForDollars(DateTime dateDEPO, Payment closestPayment)
		{
			var depoDate = dateDEPO.Day == 31 ? dateDEPO.AddDays(-1) : dateDEPO;
			var paymentDate = closestPayment.PaymentDate.Value.Day == 31 ? closestPayment.PaymentDate.Value.AddDays(-1) : closestPayment.PaymentDate.Value;

			if (depoDate.Date == paymentDate.Date)
				return 0;

			var nkd = closestPayment.NotRepaymentNom.Value * closestPayment.Coupon.Value *
				   (
					   depoDate.Day - paymentDate.Day +
					   30 * (depoDate.Month - paymentDate.Month) +
					   360 * (depoDate.Year - paymentDate.Year) +
					   closestPayment.CouponPeriod.Value
				   )
				   / 36000m;

			//НКД на одну аблигацию принудительно округлять до 2х знаков после запятой.
			return Math.Round(nkd, 2, MidpointRounding.AwayFromZero);
		}
		/// <summary>
		/// Вычисление непогашенного остатка номинала
		/// </summary>
		/// <param name="Payments">Выплаты по сделке</param>
		/// <param name="date">Дата сделки</param>
		/// <param name="nomval">Номинал по ЦБ</param>
		/// <returns></returns>
		public static decimal CalcNFN(List<Payment> Payments, DateTime? date, decimal? nomval)
		{
			if (date == null) return 0;

			var payment = GetClosestPayment(date.Value, Payments);
			if (payment != null) return payment.NotRepaymentNom ?? 0;
			else return nomval ?? 0;

		}

		/// <summary>
		/// Возврщает список отчетов по покупкам с датой сделки или датой ДЕПО 
		/// (в зависимости от типа валюты), меньшей либо равной сегодняшней дате
		/// </summary>
		/// <param name="p_List">Исходный список</param>
		/// <param name="p_CurrencyIsRoubles">true, если валюта - рубли</param>
		/// <returns></returns>
		private static List<OrdReport> FilterBuyReports(OrdReport p_Report, IEnumerable<OrdReport> p_List, bool p_CurrencyIsRoubles)
		{
			return p_List.Where(
				report =>
				{
					if (p_CurrencyIsRoubles)
						return report.DateOrder.HasValue && report.DateOrder.Value.Date <= p_Report.DateOrder;
					else
						return report.DateDEPO.HasValue && report.DateDEPO.Value.Date <= p_Report.DateDEPO;
				}
			).OrderBy(
				report =>
				{
					if (p_CurrencyIsRoubles)
						return report.DateOrder.Value.Date;
					else
						return report.DateDEPO.Value.Date;
				}
			).ToList();
		}

		/// <summary>
		/// Возврщает список отчетов по продажам с датой сделки или датой ДЕПО 
		/// (в зависимости от типа валюты), меньшей либо сегодняшней дате.
		/// Сделки, совершенные в один день упорядочиваются по ID отчета - 
		/// отчеты, чей ID меньше рассматриваемого отчета считаются совершенными 
		/// раньше в этот день
		/// </summary>
		/// <param name="p_List">Исходный список</param>
		/// <param name="p_CurrencyIsRoubles">true, если валюта - рубли</param>
		/// <returns></returns>
		private static List<OrdReport> FilterSaleReports(OrdReport p_Report, IEnumerable<OrdReport> p_List, bool p_CurrencyIsRoubles)
		{
			long ID = p_Report.ID <= 0 ? Int64.MaxValue : p_Report.ID;

			return p_List.Where(
				report =>
				{
					if (p_CurrencyIsRoubles)
					{
						if (!report.DateOrder.HasValue) return false;
						if (report.DateOrder.Value.Date == p_Report.DateOrder.Value.Date)
							return report.ID < ID;
						else
							return report.DateOrder.Value.Date < p_Report.DateOrder.Value.Date;
					}
					else
					{
						if (!report.DateDEPO.HasValue) return false;
						if (report.DateDEPO.Value.Date == p_Report.DateDEPO.Value.Date)
							return report.ID < ID;
						else
							return report.DateDEPO.Value.Date < p_Report.DateDEPO.Value.Date;
					}
				}
			).OrderBy(
				report =>
				{
					if (p_CurrencyIsRoubles)
						return report.DateOrder.Value.Date;
					else
						return report.DateDEPO.Value.Date;
				}
			).ToList();
		}

		/// <summary>
		/// Расчет средневзвешенной цены покупки: сумма (цена*кол-во) / сумма (кол-во)
		/// </summary>
		/// <param name="group">отчеты за одну дату сделки/ДЕПО</param>
		/// <returns></returns>
		private static decimal CalcAverageWeightedPrice(IGrouping<DateTime, OrdReport> group)
		{
			return group.Sum(report => report.Price.Value * report.Count.Value) / group.Sum(report => report.Count.Value);
		}

		/// <summary>
		/// Расчет суммы сделки по цене покупки
		/// </summary>
		/// <param name="p_Report">Отчет, для которого проводится расчет</param>
		/// <param name="p_SecurityNomValue">Номинал ЦБ для отчета, для которого проводится расчет</param>
		/// <param name="p_BuyReports">Список отчетов по покупкам для ЦБ отчета и портфеля поручения, в котором находится отчет, для которого проводится расчет</param>
		/// <param name="p_SaleReports">Список отчетов по продажам для ЦБ отчета и портфеля поручения, в котором находится отчет, для которого проводится расчет</param>
		/// <returns></returns>
		public static decimal CalcTransactionAmountForPurchasePrice(OrdReport p_Report, PFR_INVEST.DataObjects.Security p_Security, List<OrdReport> p_BuyReports, List<OrdReport> p_SaleReports)
		{
			bool currencyIsRoubles = CurrencyIdentifier.IsRUB(p_Security.CurrencyName);

			//Отфильтровываем отчеты по покупкам и продажам, сделки по которым совершены
			//до даты сделки/ДЕПО обрабатываемого отчета
			var filteredBuyReports = FilterBuyReports(p_Report, p_BuyReports, currencyIsRoubles);
			var filteredSaleReports = FilterSaleReports(p_Report, p_SaleReports, currencyIsRoubles);

			//Количество проданных ЦБ до даты сделки/ДЕПО обрабатываемого отчета
			long soldCount = filteredSaleReports.Sum(rep => rep.Count).Value;

			//Находим отчет о покупке, в котором купили soldCount'ую ЦБ
			long tmpCount = 0;
			int indexer = 0;
			OrdReport firstBuyReport = null;
			if (soldCount == 0)
			{
				//Если бумаги еще не продавались, то начинаем продавать прямо из первго отчета о покупке
				firstBuyReport = filteredBuyReports.FirstOrDefault();
				if (firstBuyReport != null)
					tmpCount = firstBuyReport.Count.Value;
			}
			else
				//Если бумаги уже продавались, ищем отчет, с которого начать продавать ЦБ в этот раз
				while (tmpCount <= soldCount && indexer < filteredBuyReports.Count)
				{
					firstBuyReport = filteredBuyReports[indexer];
					tmpCount += firstBuyReport.Count.Value;
					indexer++;
				}
			if (firstBuyReport == null)
				return 0;
			//Выбираем все отчеты о покупке из filteredBuyReports, начиная с firstBuyReport включительно
			var finalBuyReports = filteredBuyReports.SkipWhile(report => filteredBuyReports.IndexOf(report) < filteredBuyReports.IndexOf(firstBuyReport)).ToList();

			//Количество ЦБ, продаваемых из firstBuyReport 
			long firstBuyReportCountOffset = tmpCount - soldCount;

			//Находим отчет о покупке, в котором продали p_Report.Count'ую ЦБ
			var lastBuyReport = firstBuyReport;
			tmpCount = firstBuyReportCountOffset;
			indexer = 1;
			while (tmpCount < p_Report.Count.Value && indexer < finalBuyReports.Count)
			{
				lastBuyReport = finalBuyReports[indexer];
				tmpCount += lastBuyReport.Count.Value;
				indexer++;
			}
			//выбираем все отчеты о покупке из finalBuyReports до lastBuyReport включительно
			finalBuyReports = finalBuyReports.TakeWhile(report => finalBuyReports.IndexOf(report) <= finalBuyReports.IndexOf(lastBuyReport)).ToList();

			//Количество ЦБ, уже проданных из firstBuyReport
			long soldInFirstCount = firstBuyReport.Count.Value - firstBuyReportCountOffset;
			//Количество ЦБ, остающихся в lastBuyReport после продажи
			long lastBuyReportCountOffset = finalBuyReports.Sum(report => report.Count.Value) - soldInFirstCount - p_Report.Count.Value;

			//Рассчитываем цену покупки на каждую дату из имеющихся дат сделок/ДЕПО 
			//на основе данных по отчетам по покупкам, начиная с firstBuyReport.
			//Если мы встречаемся с несколькими отчетами по покупке в 1 день, 
			//считаем для них всех средневзвешенную цену = сумма (цена*кол-во) / сумма (кол-во)

			//0. Цены покупки на каждую дату
			var pricesByDates = new Dictionary<DateTime, decimal>();

			//1. группируем filteredBuyReports по дате сделки/ДЕПО
			var pricesCalculationBuyReports = filteredBuyReports.GroupBy(
				report =>
				{
					if (currencyIsRoubles)
						return report.DateOrder.Value;
					else
						return report.DateDEPO.Value;
				}
			);

			//2. Заполняем pricesByDates
			foreach (var record in pricesCalculationBuyReports)
			{
				if (record.Count() == 1)
					pricesByDates[record.Key] = record.First().Price.Value;
				else
					pricesByDates[record.Key] = CalcAverageWeightedPrice(record);
			}

			//2. Рассчитываем transactionAmountForPurchasePrice
			decimal securityNomValue = p_Security.NomValue.Value;
			decimal transactionAmountForPurchasePrice = 0;
			foreach (var report in finalBuyReports)
			{
				var operationDate = currencyIsRoubles ? report.DateOrder.Value : report.DateDEPO.Value;
				decimal price = pricesByDates[((DateTime)operationDate)];
				long count;
				if (firstBuyReport == lastBuyReport)
					//Если продаются бумаги из одного отчета по покупке - 
					//цикл отработает один раз, count будет равно p_Report.Count.Value
					count = p_Report.Count.Value;
				else if (report == finalBuyReports.First())
					//Из первого отчета продаем firstBuyReportCountOffset
					count = firstBuyReportCountOffset;
				else if (report == finalBuyReports.Last())
					//Из последнего продаем разницу Count и остатка от продажи
					count = report.Count.Value - lastBuyReportCountOffset;
				else
					//Из промежутоного продаем все бумаги
					count = report.Count.Value;

				decimal nomSum = securityNomValue * price / 100m;
				decimal dateSum = nomSum * count;
				//Добавляем НКД
				dateSum += Math.Round((report.OneNKD ?? 0) * count, 2);

				transactionAmountForPurchasePrice += dateSum;
			}
			return transactionAmountForPurchasePrice;
		}

		public static long? GetAvailableBuyOrSaleSecurityCountByOrder(List<CbInOrder> p_CbInOrderList, List<BuyOrSaleReportsListItem> p_AlreadyAddedReports, Security p_Security, OrdReport p_Report)
		{
			//cписок CbInOrder для данной бумаги
			var cbinordersList = p_CbInOrderList.Where(cbinord => cbinord.SecurityID == p_Security.ID);
			//список уже добавленных отчетов по этой бумаге
			var reportsList = p_AlreadyAddedReports.Where(rep => rep.SecurityID == p_Security.ID && rep.ID != p_Report.ID);
			//количество уже добавленных бумаг
			long countByAreadyAddedreports = reportsList.Sum(rep => rep.Count);
			//количество  доступных бумаг считается ниже
			long availableByOrder;

			if (cbinordersList.Count() > 1)
			{
				//аукцион
				if ((cbinordersList.First().Count ?? 0) > 0)
					//если заполняли колонку количество - считаем сумму доступных по CbInOrder
					availableByOrder = cbinordersList.Sum(cbinord => cbinord.Count ?? 0);
				else
				{
					//если заполняли колонку "Сумма денежных средств" - считаем по формуле
					//Сумма по всем отчетам для поручения, ВКЛЮЧАЯ СОХРАНЯЕМЫЙ (кол-во бумаг * цена в % от номинала * номинал) =< "Сумма денежных средств" в поручении.
					decimal totalSum = cbinordersList.Sum(cbinord => cbinord.SumMoney ?? 0);
					decimal alreadyAddedSum = reportsList.Sum(report => report.Count * report.Price * 0.01M * (p_Security.NomValue ?? 0));
					decimal sum = ((p_Security.NomValue ?? 0) * (p_Report.Price * 0.01M ?? 0));
					if (sum > 0)
						availableByOrder = (long)Math.Floor((totalSum - alreadyAddedSum) / sum);
					else
						return null;
				}
			}
			else
			{
				//покупка-продажа не с аукцона
				//значит есть один CbInOrder для этой бумаги
				CbInOrder cbin = cbinordersList.First();
				if ((cbin.Count ?? 0) > 0)
				{
					//если заполняли колонку количество - просто сумма CbInOrder'а
					availableByOrder = cbin.Count ?? 0;
				}
				else
				{
					//если заполняли колонку "Сумма денежных средств" - считаем по формуле
					//Сумма по всем отчетам для поручения, ВКЛЮЧАЯ СОХРАНЯЕМЫЙ (кол-во бумаг * цена в % от номинала * номинал) =< "Сумма денежных средств" в поручении.
					decimal totalSum = cbin.SumMoney ?? 0;
					decimal alreadyAddedSum = reportsList.Sum(report => report.Count * report.Price * 0.01M * (p_Security.NomValue ?? 0));
					decimal sum = ((p_Security.NomValue ?? 0) * (p_Report.Price * 0.01M ?? 0));
					if (sum > 0)
						availableByOrder = (long)Math.Floor((totalSum - alreadyAddedSum) / sum);
					else
						return null;
				}
			}

			//результат - разница между доступным и добавленным
			return Math.Max(availableByOrder - countByAreadyAddedreports, 0);
		}

		public static long GetAvailableSaleSecurityCountByPortfolio(OrdReport p_Report, List<OrdReport> p_BuyReports, List<OrdReport> p_SaleReports, bool p_CurrencyIsRoubles)
		{
			long boughtCount = FilterBuyReports(p_Report, p_BuyReports, p_CurrencyIsRoubles).Sum(report => report.Count.Value);
			long soldCount = FilterSaleReports(p_Report, p_SaleReports, p_CurrencyIsRoubles).Sum(report => report.Count.Value);
			return Math.Max(boughtCount - soldCount, 0);
		}

		public static readonly List<string> Contragents = new List<string> 
        { 
            "Резидент", 
            "Нерезидент", 
            "Нет данных" 
        };

		public const string SELLING_REPORT = "Отчет о продаже";
		public const string BUYING_REPORT = "Отчет о покупке";
		public const string RELAYING_REPORT = "Отчет о перекладке ЦБ";

		public const string INVALID_PRICE = "Неверно указана цена ЦБ";
		public const string INVALID_YIELD = "Неверно указана доходность";

		public const string INVALID_СOUNT_LIMIT = "Количество ЦБ должно быть в интервале от 0 до {0}";
		public const string INVALID_MAX_СOUNT = "На данную дату нет доступных для продажи выбранных ЦБ или недостаточно данных для расчета";
		public const string INVALID_СOUNT = "Количество должно быть больше нуля";

		public const string INVALID_NKDTotal = "НКД, всего по сделке - недопустимое значение";
		public const string INVALID_COUNT_SECURITY = "Превышено допустимое количество ЦБ";

		public const string INVALID_COUNT_POSITIVE = "Значение не может быть меньше 0";

		public static string CheckPrice(decimal? price)
		{
			if (!price.HasValue)
				return INVALID_PRICE;
			return price <= 0 || price >= 1000 ? INVALID_PRICE : null;
		}

		public static string CheckCount(long? count, long maxCount)
		{
			if (maxCount <= 0)
				return INVALID_MAX_СOUNT;
			return !count.HasValue || count <= 0 || count > maxCount ? string.Format(INVALID_СOUNT_LIMIT, maxCount) : null;
		}

		public static string CheckCount(long count)
		{
			return count <= 0 ? INVALID_СOUNT : null;
		}

		public static string CheckNkdTotal(long count, decimal nkdOne, int pow)
		{
			return nkdOne < 0 ? INVALID_COUNT_POSITIVE : count * nkdOne <= (decimal)Math.Pow(10, pow) - 1 ? null : INVALID_NKDTotal;
		}

		public static string CheckCountSecurity(decimal sumNom, int pow)
		{
			return sumNom <= (decimal)Math.Pow(10, pow) - 1 ? null : INVALID_COUNT_SECURITY;
		}

		public static string CheckYield(decimal? yield)
		{
			return !yield.HasValue || yield <= 0 || yield >= 1000 ? INVALID_YIELD : null;
		}
	}
}
