﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Helpers
{
	public static class MarkedAsDeletedExtension
	{
		public static bool IsDeleted(this IMarkedAsDeleted item) 
		{
			return item.StatusID == -1;
		}
	}
}
