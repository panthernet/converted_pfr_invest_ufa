﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PFR_INVEST.DataObjects.Helpers
{
	public class BankHelper
	{
		public const string VEB = "вэб";
		public const string CB = "цб";
		public const string SBRF = "сбрф";

		public static bool IsVEB(LegalEntity bank)
		{
			return (bank != null) && (!string.IsNullOrEmpty(bank.FormalizedName))
				&& (bank.FormalizedName.Trim().Equals(VEB, StringComparison.OrdinalIgnoreCase));
		}

		public static bool IsCB(LegalEntity bank)
		{
			return (bank != null) && (!string.IsNullOrEmpty(bank.FormalizedName))
				&& (bank.FormalizedName.Trim().Equals(CB, StringComparison.OrdinalIgnoreCase));
		}

		public static bool IsSBRF(LegalEntity bank)
		{
			return (bank != null) && (!string.IsNullOrEmpty(bank.FormalizedName))
					&& (bank.FormalizedName.Trim().Equals(SBRF, StringComparison.OrdinalIgnoreCase));
		}


	    /// <summary>
	    /// Вычисление суммы процентов по вкладу
	    /// </summary>
	    /// <param name="sDate">Дата размещения</param>
	    /// <param name="summ">Сумма вклада</param>
	    /// <param name="rate">Процентная ставка, %</param>
	    /// <param name="rDate">Дата возврата</param>
	    public static decimal CalcPayment(DateTime rDate, DateTime sDate, decimal summ, decimal rate)
		{
            //добавлено разбитие расчета по годам и учет високосного года
            //http://jira.vs.it.ru/browse/DOKIPIV-967
		    var totalTerm = (rDate - sDate).Days;
            //если возврат в другом году, считаем отдельно проценты за оба года
		    if (rDate.Year != sDate.Year)
		    {
		        var term = (new DateTime(sDate.Year, 12, 31) - sDate).Days;
                var pay1 = (summ * rate * term) / ((365 + (DateTime.IsLeapYear(sDate.Year) ? 1 : 0)) * 100);
		        return pay1 + (summ*rate*(totalTerm - term))/((365 + (DateTime.IsLeapYear(rDate.Year) ? 1 : 0))*100);
		    }
		    return (summ * rate * totalTerm) / ((365 + (DateTime.IsLeapYear(sDate.Year) ? 1 : 0)) * 100);
		}


		/// <summary>
		/// Коэффициент (приказ Мин.Фина №24н от 17.02.2015)
		/// </summary>
		/// <param name="bank"></param>
		/// <returns></returns>
		public static decimal LimitMult(LegalEntity bank)
		{
			// от 25000 млн.рублей до 50000 млн.рублей включительно, значение коэффициента равно – 0,1;
			// от 50000 млн.рублей до 100000 млн.рублей включительно, значение коэффициента равно – 0,2;
			// более 100000 млн.рублей, значение коэффициента равно – 0,5;
			var money = (bank.Money ?? 0m) / 1000000.0m; //млн.рублей 
			if (money > 100000)
				return 0.5m;
			if (money > 50000)
				return 0.2m;
			if (money >= 25000)
				return 0.1m;
			return 0;
		}

		/// <summary>
		/// Лимит на средства, млн. руб. 
		/// </summary>        
		public static decimal Limit4Money(LegalEntity bank)
		{
			decimal m = (bank.Money ?? 0m) / 1000000.0m;
			return Math.Round(LimitMult(bank) * m, 0);
		}
	}
}
