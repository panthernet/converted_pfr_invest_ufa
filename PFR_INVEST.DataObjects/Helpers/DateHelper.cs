﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Helpers
{
    public class DateHelper
    {
        public static int GetAddSPNQuarter(int? month = null)
        {
            if (month == null)
                month = DateTime.Now.Month;
            if (month < 1 || month > 12)
                throw new ArgumentOutOfRangeException("month"); return (int)(((long)month) - 1) / 3 + 1;
        }
    }
}
