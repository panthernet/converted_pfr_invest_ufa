﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;

namespace PFR_INVEST.DataObjects.XMLModel.Auction
{
	/// <summary>
	/// Базовый документ для СПВБ биржи
	/// </summary>
	[XmlRoot("SPCEX_DOC")]
	public abstract class SPVB_BaseDocument
	{

		// <doc_props doc_ver="2.0" delivery_mode="P" delivery_confirmation="N" doc_type="selection_info" 
		// doc_date="2015-06-30" doc_time="16:15:30" doc_name="SelInfo_ID4_00100_0001_2015-06-30RT2015-06-30_161530.xml" sender_doc_id="SI_00100_0001#4"/>  				
		public class DocProperties
		{
			#region Fields

			[XmlAttribute("doc_ver")]
			public string Version { get; set; }

			[XmlAttribute("delivery_mode")]
			public string DeliveryMode { get; set; }

			[XmlAttribute("delivery_confirmation")]
			public string DeliveryConfirmation { get; set; }

			[XmlAttribute("doc_type")]
			public string DocType { get; set; }

			[XmlAttribute("doc_date")]
			public string Date
			{
				get { return DateValue.ToXmlString(); }
				set { DateValue = value.ParseAsDate(); }
			}

			[XmlIgnore]
			public DateTime DateValue { get; set; }

			[XmlAttribute("doc_time")]
			public string Time
			{
				get { return new DateTime(1, 1, 1, TimeValue.Hours, TimeValue.Minutes, TimeValue.Seconds).ToString(@"hh\:mm\:ss"); }
				set { TimeValue = DateTime.ParseExact(value, @"hh\:mm\:ss", CultureInfo.InvariantCulture).TimeOfDay; }
			}

			[XmlIgnore]
			public TimeSpan TimeValue { get; set; }

			[XmlAttribute("doc_name")]
			public string DocName { get; set; }

			[XmlAttribute("sender_doc_id")]
			public string SenderDocID { get; set; }

			#endregion

			public DocProperties()
			{
				Version = "2.0";
				DeliveryMode = "P";
				DeliveryConfirmation = "N";
				DocType = "selection_info";
				DateValue = DateTime.Now;
				TimeValue = DateTime.Now.TimeOfDay;

			}
		}



		public class AddreseInfoWraper
		{
			[XmlElement("addressee_info")]
			public AddreseInfo Addrese { get; set; }

			public AddreseInfoWraper()
			{
				Addrese = new AddreseInfo();
			}

			public static AddreseInfoWraper PFR { get { return new AddreseInfoWraper() { Addrese = AddreseInfo.PFR }; } }
			public static AddreseInfoWraper SPVB { get { return new AddreseInfoWraper() { Addrese = AddreseInfo.SPVB }; } }

			
		}

		public class AddreseInfo //: IXmlSerializable
		{
			#region Fields

			[XmlAttribute("id")]
			public string ID { get; set; }

			[XmlAttribute("name")]
			public string Name { get; set; }

			#endregion

			public static AddreseInfo PFR { get { return new AddreseInfo() { ID = "PFRF__0001", Name = "ПФР" }; } }
			public static AddreseInfo SPVB { get { return new AddreseInfo() { ID = "00100_0001", Name = "СПВБ" }; } }

			//public System.Xml.Schema.XmlSchema GetSchema()
			//{
			//    return null;
			//}

			//public void ReadXml(System.Xml.XmlReader reader)
			//{
			//    reader.ReadStartElement();
			//    this.ID = reader.GetAttribute("id");
			//    this.Name = reader.GetAttribute("name");
			//    if (!reader.IsEmptyElement)
			//        reader.ReadEndElement();
			//    reader.Read();
			//    reader.Read();


			//}

			//public void WriteXml(System.Xml.XmlWriter writer)
			//{
			//    writer.WriteStartElement("addressee_info");
			//    writer.WriteAttributeString("id", this.ID);
			//    writer.WriteAttributeString("name", this.Name);
			//    writer.WriteEndElement();
			//}
		}

		[XmlIgnore]
		public abstract string FileType { get; }

		[XmlIgnore]
		public abstract string DocType { get; }

		public SPVB_BaseDocument()
		{
			this.Properties = new DocProperties();
			this.Properties.DocType = this.DocType;
			this.Sender = new AddreseInfoWraper();
			this.Reciver = new AddreseInfoWraper();
		}

		public SPVB_BaseDocument(DepClaimSelectParams value)
			: this()
		{
			this.Properties.DocName = string.Format("{4}_ID{2}_{1:yyyy-MM-dd}_{0}RT{3:yyyy-MM-dd_HHmmss}.xml", value.AuctionNum, value.SelectDate, value.ID, DateTime.Now, this.FileType);
			this.Properties.SenderDocID = string.Format("{0}_{1:yyyy-MM-dd}#{2}", value.AuctionNum, value.SelectDate, value.ID);
		}

		[XmlAttribute("doc_date")]
		public string DocDate
		{
			get { return this.Properties.Date; }
			set { this.Properties.Date = value; }
		}

		[XmlElement("doc_props")]
		public DocProperties Properties { get; set; }

		[XmlElement("sender")]
		public AddreseInfoWraper Sender { get; set; }

		[XmlElement("receiver")]
		public AddreseInfoWraper Reciver { get; set; }


		public virtual bool IsValid 
		{
			get { return this.Properties.DocType == this.DocType;; }
		}
	}
}
