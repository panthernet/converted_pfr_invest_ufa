﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.DataObjects.XMLModel.Auction
{
	/// <summary>
	/// Модель для импорта сводного реестра заявок от СПВБ
	/// </summary>
	[XmlRoot("SPCEX_DOC")]
	public class SPVB_DepClaims : SPVB_BaseDocument
	{
		public class DepClaimItem 
		{
			[XmlAttribute("participant_code")]
			public string FIRMID { get; set; }

			[XmlAttribute("participant_name")]
			public string FIRMNAME { get; set; }

			//[XmlAttribute("RECNUM")]
			//public long RECNUM { get; set; }

			//[XmlAttribute("SECURITYID")]
			//public string SECURITYID { get; set; }

			[XmlAttribute("price")]
			public decimal RATE { get; set; }

			[XmlAttribute("quant")]
			public decimal AMOUNT { get; set; }

			[XmlAttribute("exec_date")]
			public DateTime SETTLEDATE { get; set; }

			//[XmlAttribute("PAYMENT")]
			//public decimal PAYMENT { get; set; }

			[XmlAttribute("repaym_date")]
			public DateTime RETURNDATE { get; set; }

			[XmlAttribute("last_date")]
			public DateTime UpdateDate { get; set; }

			[XmlAttribute("last_time")]
			public DateTime UpdateTime { get; set; }
		}

		public class DocBodyWraper 
		{
			[XmlElement("list_of_preliminary")]
			public DocBody Body { get; set; }

			public DocBodyWraper() 
			{
				this.Body = new DocBody();
			}
		}

		public class DocBody 
		{
			[XmlIgnore]
			public DateTime SelectDate { get; set; }
			[XmlAttribute("select_date")]
			public string SelectDateText 
			{
				get { return SelectDate.ToXmlString(); }
				set { SelectDate = value.ParseAsDate(); }
			}
			[XmlAttribute("executor_phone")]
			public string ExecutorPhone { get; set; }
			[XmlAttribute("executor_name")]
			public string ExecutorName { get; set; }
			[XmlAttribute("finstr")]
			public string Code { get; set; }
			[XmlAttribute("records")]
			public int Count { get; set; }

			[XmlElement("preliminary")]
			public List<DepClaimItem> DepClaims { get; set; }


			public DocBody()
			{
				this.DepClaims = new List<DepClaimItem>();
			}

		}

		public SPVB_DepClaims()
			: base()
		{


		}

		public override string FileType
		{
			get { return "DepClaim"; }
		}

		public override string DocType
		{
			get { return "list_of_preliminary"; }
		}

		[XmlElement("document")]
		public DocBodyWraper Document { get; set; }

		
	}
}
