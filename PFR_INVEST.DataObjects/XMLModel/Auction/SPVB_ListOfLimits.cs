﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Globalization;
using PFR_INVEST.DataObjects.ListItems;


namespace PFR_INVEST.DataObjects.XMLModel.Auction
{
	/// <summary>
	/// Класс для сериализации "Уведомления о параметрах отбора заявок" для биржи СПВБ
	/// </summary>	
	[XmlRoot("SPCEX_DOC")]
	public class SPVB_ListOfLimits : SPVB_BaseDocument
	{
		public class LimitItem 
		{
			[XmlAttribute("participant_code")]
			public string BankCode { get; set; }
			[XmlAttribute("participant_name")]
			public string BankName { get; set; }
			[XmlAttribute("limit")]
			public decimal Limit { get; set; }
			[XmlAttribute("range")]
			public string Range { get; set; }
		}

		public class DocBodyWraper
		{
			[XmlElement("list_of_limit")]
			public DocBody Body { get; set; }
		}

		public class DocBody 
		{
			[XmlIgnore]
			public DateTime SelectDate { get; set; }
			[XmlAttribute("limit_date")]
			public string SelectDateText
			{
				get { return SelectDate.ToXmlString(); }
				set { SelectDate = value.ParseAsDate(); }
			}
			[XmlIgnore]
			public DateTime LimitDate { get; set; }
			[XmlAttribute("select_date")]
			public string LimitDateText
			{
				get { return LimitDate.ToXmlString(); }
				set { LimitDate = value.ParseAsDate(); }
			}

			[XmlAttribute("executor_phone")]
			public string ExecutorPhone { get; set; }

			[XmlAttribute("executor_name")]
			public string ExecutorName { get; set; }			

			[XmlAttribute("records")]
			public int Count { get; set; }

			[XmlElement("participant_limit")]
			public List<LimitItem> Limits { get; set; }			
			
			public DocBody()
			{
				this.Limits = new List<LimitItem>();
			}
			
		}

		[Obsolete("Only for XML Serializer", true)]
		public SPVB_ListOfLimits() : this(new DepClaimSelectParams(), new List<BankLimitListItem>()) { }

		public SPVB_ListOfLimits(DepClaimSelectParams auction, IEnumerable<BankLimitListItem> limits)
			: base(auction)
		{
			this.Sender = AddreseInfoWraper.PFR;
			this.Reciver = AddreseInfoWraper.SPVB;
			this.Document = new DocBodyWraper() { Body = new DocBody() { ExecutorPhone = "+74959820604", ExecutorName = "Пенсионный Фонд Российской Федерации" } };
			this.Document.Body.SelectDate = auction.SelectDate;
			this.Document.Body.LimitDate = auction.SelectDate;
			foreach (var l in limits)
			{
				this.Document.Body.Limits.Add(new LimitItem() 
				{
					BankCode = l.StockCode,
					BankName = l.BankName,
					Range = "S",
					Limit = Math.Round(l.LimitTotal,0)
				});

				this.Document.Body.Limits.Add(new LimitItem()
				{
					BankCode = l.StockCode,
					BankName = l.BankName,
					Range = "O",
					Limit = Math.Round(l.LimitClaim, 0)
				});
			}
			this.Document.Body.Count = this.Document.Body.Limits.Count;
		}

		public override string FileType
		{
			get { return "Lim"; }
		}

		public override string DocType
		{
			get { return "list_of_limits"; }
		}

		[XmlElement("document")]
		public DocBodyWraper Document { get; set; }

	}
}
