﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel
{
    [Serializable, XmlRoot("row")]
    public class BondXmlRow
    {
        [XmlAttribute(AttributeName = "TRADEDATE")]
        [Browsable(false)]
        public DateTime TRADEDATE { get; set; }
        [XmlAttribute(AttributeName = "SECID")]
        [Browsable(false)]
        public string SECID { get; set; }
        [Browsable(false)]
        [DisplayName("Режим торгов")]
        [XmlAttribute(AttributeName = "BOARDNAME")]
        public string BOARDNAME { get; set; }
        [XmlAttribute(AttributeName = "BOARDID")]
        [DisplayName("Режим торгов")]
        public string BOARDID { get; set; }
        [XmlAttribute(AttributeName = "SHORTNAME")]
        public string SHORTNAME { get; set; }
        [XmlAttribute(AttributeName = "REGNUMBER")]
        public string REGNUMBER { get; set; }
        [XmlAttribute(AttributeName = "ISIN")]
        public string ISIN { get; set; }
        [XmlAttribute(AttributeName = "MATDATE")]
        public string MATDATE { get; set; }
        [XmlAttribute(AttributeName = "LISTNAME")]
        public string LISTNAME { get; set; }
        [XmlAttribute(AttributeName = "FACEVALUE")]
        public string FACEVALUE { get; set; }
        [XmlAttribute(AttributeName = "CURRENCYID")]
        public string CURRENCYID { get; set; }
        [XmlAttribute(AttributeName = "VOLUME")]
        public string VOLUME { get; set; }
        [XmlAttribute(AttributeName = "VALUE")]
        public string VALUE { get; set; }
        [XmlAttribute(AttributeName = "NUMTRADES")]
        public string NUMTRADES { get; set; }
        [XmlAttribute(AttributeName = "PREV")]
        public string PREV { get; set; }
        [XmlAttribute(AttributeName = "PREVLEGALCLOSEPRICE")]
        public string PREVLEGALCLOSEPRICE { get; set; }
        [XmlAttribute(AttributeName = "OPENPERIOD")]
        public string OPENPERIOD { get; set; }
        [XmlAttribute(AttributeName = "OPEN")]
        public string OPEN { get; set; }
        [XmlAttribute(AttributeName = "LEGALOPENPRICE")]
        public string LEGALOPENPRICE { get; set; }
        [XmlAttribute(AttributeName = "LOW")]
        public string LOW { get; set; }
        [XmlAttribute(AttributeName = "HIGH")]
        public string HIGH { get; set; }
        [XmlAttribute(AttributeName = "LEGALCLOSEPRICE")]
        public string LEGALCLOSEPRICE { get; set; }
        [XmlAttribute(AttributeName = "CLOSE")]
        public string CLOSE { get; set; }
        [XmlAttribute(AttributeName = "CLOSEPERIOD")]
        public string CLOSEPERIOD { get; set; }
        [XmlAttribute(AttributeName = "WAPRICE")]
        public string WAPRICE { get; set; }
        [XmlAttribute(AttributeName = "TRENDCLOSE")]
        public string TRENDCLOSE { get; set; }
        [XmlAttribute(AttributeName = "TRENDCLSPR")]
        public string TRENDCLSPR { get; set; }
        [XmlAttribute(AttributeName = "TRENDWAP")]
        public string TRENDWAP { get; set; }
        [XmlAttribute(AttributeName = "TRENDWAPPR")]
        public string TRENDWAPPR { get; set; }
        [XmlAttribute(AttributeName = "OPENVAL")]
        public string OPENVAL { get; set; }
        [XmlAttribute(AttributeName = "CLOSEVAL")]
        public string CLOSEVAL { get; set; }
        [XmlAttribute(AttributeName = "HIGHBID")]
        public string HIGHBID { get; set; }
        [XmlAttribute(AttributeName = "LOWOFFER")]
        public string LOWOFFER { get; set; }
        [XmlAttribute(AttributeName = "BID")]
        public string BID { get; set; }
        [XmlAttribute(AttributeName = "OFFER")]
        public string OFFER { get; set; }
        [XmlAttribute(AttributeName = "YIELDATWAP")]
        public string YIELDATWAP { get; set; }
        [XmlAttribute(AttributeName = "YIELDCLOSE")]
        public string YIELDCLOSE { get; set; }
        [XmlAttribute(AttributeName = "ACCINT")]
        public string ACCINT { get; set; }
        [XmlAttribute(AttributeName = "DURATION")]
        public string DURATION { get; set; }
        [XmlAttribute(AttributeName = "MARKETPRICE")]
        public string MARKETPRICE { get; set; }
        [XmlAttribute(AttributeName = "MARKETPRICE2")]
        public string MARKETPRICE2 { get; set; }
        [XmlAttribute(AttributeName = "ADMITTEDQUOTE")]
        public string ADMITTEDQUOTE { get; set; }
        [XmlAttribute(AttributeName = "ISSUESIZE")]
        public string ISSUESIZE { get; set; }
        [XmlAttribute(AttributeName = "MPVALTRD")]
        public string MPVALTRD { get; set; }
        [XmlAttribute(AttributeName = "MP2VALTRD")]
        public string MP2VALTRD { get; set; }
        [XmlAttribute(AttributeName = "ADMITTEDVALUE")]
        public string ADMITTEDVALUE { get; set; }
        [XmlAttribute(AttributeName = "MARKETPRICE3")]
        public string MARKETPRICE3 { get; set; }
        [XmlAttribute(AttributeName = "MARKETPRICE3TRADESVALUE")]
        public string MARKETPRICE3TRADESVALUE { get; set; }
        [XmlAttribute(AttributeName = "DECIMALS")]
        public string DECIMALS { get; set; }
        [XmlAttribute(AttributeName = "TYPE")]
        public string TYPE { get; set; }

        public BDate ConvertToBDate()
        {
            return new BDate()
            {
                BID = parseDoubleSafe(BID),
                BoardIdText = BOARDID,
                Close = parseDoubleSafe(CLOSE),
                Duration = int.Parse(string.IsNullOrEmpty(DURATION) ? "0" : DURATION),
                High = parseDoubleSafe(HIGH),
                HighBID = parseDoubleSafe(HIGHBID),
                Low = parseDoubleSafe(LOW),
                LowOffer = parseDoubleSafe(LOWOFFER),
                MarketPrice1 = parseDoubleSafe(MARKETPRICE),
                MarketPrice2 = parseDoubleSafe(MARKETPRICE2),
                NumberOfTrades = parseDoubleSafe(NUMTRADES),
                Offer = parseDoubleSafe(OFFER),
                Open = parseDoubleSafe(OPEN),
                SecurityIdText = SECID,
                TradeDate = TRADEDATE,
                TrendClose = parseDoubleSafe(TRENDCLOSE),
                TrendWAP = parseDoubleSafe(TRENDWAP),
                Value = parseDoubleSafe(VALUE),
                Volume = parseDoubleSafe(VOLUME),
                WAPrice = parseDoubleSafe(WAPRICE),
                YieldAtWAP = parseDoubleSafe(YIELDATWAP),
                YieldClose = parseDoubleSafe(YIELDCLOSE)
            };
        }
        private decimal parseDoubleSafe(string s)
        {
            return Decimal.Parse(string.Empty == s ? "0" : s, NumberStyles.Number,
                CultureInfo.InvariantCulture);
        }
    }
}
