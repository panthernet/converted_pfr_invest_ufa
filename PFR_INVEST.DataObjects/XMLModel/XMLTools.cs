﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace PFR_INVEST.DataObjects.XMLModel
{
	public static class XMLTools
	{
		public const string AUCTION_DATE_FORMAT = "yyyy-MM-dd";
		public const string KIP_DATE_FORMAT = "dd.MM.yyyy";

				

		public static string ToXmlString(this DateTime value)
		{
			return value.ToString(AUCTION_DATE_FORMAT);
		}

		public static string ToXmlString(this DateTime value, string format)
		{
			return value.ToString(format);
		}

		public static string ToXmlString(this TimeSpan value)
		{
			return new DateTime(1, 1, 1, value.Hours, value.Minutes, value.Seconds).ToString("HH:mm:ss");
		}

		public static string ToXmlString(this decimal value)
		{
			return (value).ToString("0.00", CultureInfo.InvariantCulture);
		}

		public static string ToXmlString(this int value)
		{
			return (value).ToString();
		}

		public static DateTime ParseAsDate(this string value)
		{
			return DateTime.ParseExact(value, AUCTION_DATE_FORMAT, CultureInfo.InvariantCulture);
		}

		public static DateTime ParseAsDate(this string value, string format)
		{
			return DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
		}

        public static string ToСBXmlString(this DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }


		public static string ToOnesXmlString(this DateTime value)
		{
			return value.ToString("dd.MM.yyyy");
		}

		public static string ToOnesFullXmlString(this DateTime value)
		{
			return value.ToString("dd.MM.yyyy hh:mm:ss");
		}

		public static string ToOnesXmlString(this decimal value)
		{
			return (value).ToString("0.00", CultureInfo.InvariantCulture);
		}

        public static string ToIntegerXmlString(this decimal value)
        {
            return (value).ToString("n0", CultureInfo.InvariantCulture);
        }
    }
}
