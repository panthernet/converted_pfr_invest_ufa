﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects.XMLModel.KIP
{
	[XmlRoot("ФайлПФР")]
	public class KIP_CommonDoc : KIP_DocumentBase
	{
		public class CommonDocumentInfo : DocumentInfo { }

		[XmlElement("ОПИСЬ")]
		public CommonDocumentInfo Info { get; set; }
	}
}
