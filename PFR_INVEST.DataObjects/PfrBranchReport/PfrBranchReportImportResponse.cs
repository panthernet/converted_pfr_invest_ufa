﻿using System;
using System.Runtime.Serialization;
using PFR_INVEST.DataObjects;
using System.Text;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportResponse : BaseDataObject
    {
        public enum enStatus
        {
            OK=0,
            Error = 1,
            Duplicate = 2
        }


        [DataMember]
        public virtual long StatusCode { get; set; }

        [DataMember]
        public virtual string ErrorMessage { get; set; }

        [DataMember]
        public virtual string StackTrace { get; set; }

        [IgnoreDataMember]
        public virtual bool IsError
        {
            get { return StatusCode != 0; }
        }

        [DataMember]
        public Guid[] DuplicateItemKeys { get; set; }

        //[DataMember]
        //public PfrBranchReportImportContentBase[] Items;

        [IgnoreDataMember]
        public enStatus Status
        {
            get { return (enStatus)StatusCode; }
            set { StatusCode = (long)value; }
        }
    }
}
