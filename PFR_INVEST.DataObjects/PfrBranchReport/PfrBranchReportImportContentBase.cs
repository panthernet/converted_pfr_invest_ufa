﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataObjects;
using System.Runtime.Serialization;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContentBase : IEquatable<PfrBranchReportImportContentBase>
    {
        public PfrBranchReportImportContentBase()
        {
            ReportKey = Guid.NewGuid();
        }


        public virtual int? RegionCode
        {
            get
            {
                return PfrBranchReport.Branch != null ? PfrBranchReport.Branch.RegionNumber : null;
            }
        }

        public virtual string RegionName
        {
            get { return PfrBranchReport.Branch.Name; }
        }

        public virtual int PeriodYear
        {
            get { return PfrBranchReport.PeriodYear; }
        }

        public virtual int? PeriodMonth
        {
            get { return PfrBranchReport.PeriodMonth; }
        }

        public virtual string PeriodMonthText
        {
            get
            {
                var month = PeriodMonth ?? 0;
                if (month > 0 && month <= 12)
                {
                    return DateTools.Months[month - 1];
                }

                return "";
            }
        }

        [DataMember]
        public PfrBranchReport PfrBranchReport { get; set; }


        /// <summary>
        /// Used to distinguish report items in response
        /// </summary>
        [DataMember]
        public Guid ReportKey { get; set; }

        public bool Equals(PfrBranchReportImportContentBase other)
        {
            if (other != null && this != null && this.RegionCode != null && other.RegionCode != null){
                return this.PeriodMonth == other.PeriodMonth && this.PeriodYear == other.PeriodYear && this.RegionCode == other.RegionCode && this.PfrBranchReport.ReportTypeID == other.PfrBranchReport.ReportTypeID;
            }
            else if (other != null && this != null && (this.RegionCode == null || other.RegionCode == null))
            {
                return this.PeriodMonth == other.PeriodMonth && this.PfrBranchReport.PeriodYear == other.PeriodYear && this.PfrBranchReport.BranchID == other.PfrBranchReport.BranchID
                        && this.PfrBranchReport.ReportTypeID == other.PfrBranchReport.ReportTypeID;
            }
            else
            {
                return other == null && this == null;
            }
        }
    }
}
