﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.BranchReport
{
    public class PfrBranchReportImportContent_NpfNotice : PfrBranchReportImportContentBase
    {
        [DataMember]
        public List<PfrBranchReportNpfNotice> Items;

        public void Add(PfrBranchReportNpfNotice reportRow)
        {
            Items.Add(reportRow);
        }
    }
}
