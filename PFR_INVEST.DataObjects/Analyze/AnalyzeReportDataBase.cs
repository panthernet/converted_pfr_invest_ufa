﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzeReportDataBase : BaseDataObject 
    {
        [DataMember]
        public virtual long Id { get; set; }
        [DataMember]
        public virtual long ReportId { get; set; }
        public virtual decimal? Total { get; set; }
        #region Поля только для чтения из БД
        public virtual int Year { get; set; }
        public virtual int Kvartal { get; set; }
        public virtual string QuarkText { get; set; }//используется только для вывода в PivotGrid
        public virtual bool HasError => !string.IsNullOrEmpty(ErrorMessage);
        public virtual string ErrorMessage { get; set; }
        #endregion

    }
}