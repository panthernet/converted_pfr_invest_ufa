﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzeYieldfundsData : AnalyzeReportDataBase
    {
        
        public virtual int SubjectId { get; set; }

        #region Поля только для чтения из БД
        public virtual string SubjectName { get; set; }
        public virtual SubjectSPN.FondType SubjectTypeFond { get; set; }
        public virtual short ReportAct { get; set; }
        public virtual int SubjOrderId { get; set; }
        public virtual string RepActTitle => _acts.FirstOrDefault(a => a.Key == ReportAct).Value;

        static KeyValuePair<int, string>[] _acts = new[] { new KeyValuePair<int, string>(140, "Приказ №140н"), new KeyValuePair<int, string>(107, "Приказ №107н") };

        #endregion

        public AnalyzeYieldfundsData(AnalyzeYieldfundsData d)
        {
            Id = d.Id;
            ReportId = d.ReportId;
            Total = d.Total;
            SubjectId = d.SubjectId;
            SubjectName = d.SubjectName;
            SubjOrderId = d.SubjOrderId;
            IsReadOnly = d.IsReadOnly;
        }

        public AnalyzeYieldfundsData()
        {
            
        }
    }


}
