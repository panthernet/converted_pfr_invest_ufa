﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects.Analyze
{

    public class AnalyzePensionplacementData : AnalyzeReportDataBase
    {
        public virtual int ActivesKindId { get; set; }
        public virtual int SubjectId { get; set; }
        //public virtual decimal? Total { get; set; }

        #region Поля только для чтения из БД
        public virtual string SubjectName { get; set; }
        public virtual string ActivesKindName { get; set; }
        public virtual int ActOrderId { get; set; }
        public virtual int SubjOrderId { get; set; }
        [IgnoreDataMember]
        public virtual string SubjectSubgroup => SubjectName.StartsWith("ГУК", StringComparison.CurrentCultureIgnoreCase) ? "ГУК" : SubjectName.StartsWith("ЧУК", StringComparison.CurrentCultureIgnoreCase) ? "ЧУК" : string.Empty;
        ///// <summary>
        ///// Вычисляемое на форме поле только для UI
        ///// </summary>
        //public virtual decimal Percent { get; set; }
        #endregion

        public virtual SubjectSPN.FondType SubjectTypeFond { get; set; }
        
        public AnalyzePensionplacementData(AnalyzePensionplacementData d)
        {
            Id = d.Id;
            ReportId = d.ReportId;
            Total = d.Total;

            ActivesKindId = d.ActivesKindId;
            SubjectId = d.SubjectId;
            SubjectName = d.SubjectName;
            ActivesKindName = d.ActivesKindName;
            ActOrderId = d.ActOrderId;
            SubjOrderId = d.SubjOrderId;
            IsReadOnly = d.IsReadOnly;
        }

        public AnalyzePensionplacementData()
        {

        }
    }


}
