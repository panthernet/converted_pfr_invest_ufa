﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class PaymentKind : BaseDataObject
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int UserRoleFlags { get; set; }
        [DataMember]
        public virtual int EditType { get; set; }
        [DataMember]
        public virtual int GroupId { get; set; }

        public static string GetGroupName(int GroupId)
        {
            return _Groups[GroupId];
        }

        private static string[] _Groups = new[]
        {
            @"Сумма выплат за счет СПН, млн. руб., нарастающим итогом",
            @"Общее количество решений о назначении выплат за счет СПН, нарастающим итогом, шт.",
            @"Средний размер выплат за счет СПН (накопленным итогом), руб.",
            @"Выплаты правоприемникам застрахованных лиц (с 2007 года)"
        };
    }
}
