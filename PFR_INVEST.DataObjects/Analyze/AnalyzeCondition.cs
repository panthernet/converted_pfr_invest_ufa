﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Analyze
{
    public class AnalyzeCondition : BaseDataObject
    {
        [DataMember]
        public virtual long Id { get; set; }
        [DataMember]
        public virtual string ReportName { get; set; }
        [DataMember]
        public virtual string MessageHeader { get; set; }
        [DataMember]
        public virtual string MessageText { get; set; }
        [DataMember]
        public virtual string RepeatInterval { get; set; }
        [DataMember]
        public virtual DateTime StartDate { get; set; }
        [DataMember]
        public virtual int AlertForDays { get; set; }
        [DataMember]
        public virtual string AlertSign { get; set; }
        [DataMember]
        public virtual int UserRoleFlags { get; set; }
        [DataMember]
        public virtual string ClassName { get; set; }
        [DataMember]
        public virtual short Status { get; set; }

        [IgnoreDataMember]
        public virtual bool IsActiveStatus
        {
            get { return Status == 1; }
            set { Status = value ? (short)1 : (short)0; }
        }

    }
}
