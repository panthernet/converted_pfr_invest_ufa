﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace PFR_INVEST.DataObjects.Analyze
{
    public class SubjectSPN : BaseDataObject
    {
        public enum FondType : short
        {
            ПФР = 0,
            НПФ = 1,
            ОАРРС = 2,
            ОФПР = 3
        }

        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [Required]
        [DataMember]
        public virtual short Fondtype { get; set; }
        [DataMember]
        public virtual int UserRoleFlags { get; set; }
        [DataMember]
        public virtual string GroupId { get; set; }
        [DataMember]
        public virtual string RibbonGroups { get; set; }
        [DataMember]
        public virtual int OrderId { get; set; }
        [IgnoreDataMember]
        public virtual string FondTypeName => ((SubjectSPN.FondType)this.Fondtype).ToString();

       
    }
}
