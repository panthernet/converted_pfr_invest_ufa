﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportOwnedFoundsListItem : OwnedFounds
	{
		public ReportOwnedFoundsListItem() { }

		public ReportOwnedFoundsListItem(OwnedFounds edo, LegalEntity uk)
			: this() 
		{
			edo.CopyToWithoutNull(this);
			UKName = uk.FormalizedName;
		}

		
		public string UKName { get; set; }
		
		
	}
}
