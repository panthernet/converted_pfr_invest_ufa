﻿using System;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportProfitabilityListItem : McProfitAbility
	{
		public ReportProfitabilityListItem() { }

		public ReportProfitabilityListItem(McProfitAbility edo, Contract contract)
			: this() 
		{
			edo.CopyToWithoutNull(this);			
			ContractNumber = contract.ContractNumber;
			ContractDate = contract.ContractDate;
			Portfolio = contract.PortfolioName;			
		}

		
		public string Portfolio { get; set; }
		public new string ContractNumber { get; set; }
		public DateTime? ContractDate { get; set; } 
		
	}
}
