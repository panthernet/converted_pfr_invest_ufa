﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
	public class ReportF060ListItem : EdoOdkF060
	{
		public ReportF060ListItem() { }

		public ReportF060ListItem(EdoOdkF060 edo, Contract contract, string ukName, string contragentType) : this() 
		{
			edo.CopyTo(this);
			UKName = ukName;
			ContractNumber = contract.ContractNumber;
			Portfolio = contract.PortfolioName;
			ContragentType = contragentType;
		}

		public string UKName { get; set; }
		public string Portfolio { get; set; }
		public string ContractNumber { get; set; }
		public string ContragentType { get; set; }
	}
}
