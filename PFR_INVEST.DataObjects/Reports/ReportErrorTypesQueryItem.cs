﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Reports
{
    [DataContract]
    public class ReportErrorTypesQueryItem
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ErrorCode { get; set; }
        [DataMember]
        public string ContractCode { get; set; }
        [DataMember]
        public string ContractName { get; set; }
        [DataMember]
        public int Sum { get; set; }

        public ReportErrorTypesQueryItem(object[] input)
        {
            Name = (string)input[0];
            ErrorCode = (string)input[1];
            ContractCode = (string)input[2];
            ContractName = (string)input[3];
            Sum = Convert.ToInt32(input[4]);
        }

        public ReportErrorTypesQueryItem(string name, string ecode, string ccode, string cname, int sum)
        {
            Name = name;
            ErrorCode = ecode;
            ContractCode = ccode;
            ContractName = cname;
            Sum = sum;
        }
    }

    [DataContract]
    public class ReportErrorSpreadQueryItem
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string CodeName { get; set; }
        [DataMember]
        public int Summ { get; set; }

        public ReportErrorSpreadQueryItem(object[] input)
        {
            Name = input[0]?.ToString() ?? "";
            Code = (string)input[1];
            CodeName = (string)input[2];
            Summ = Convert.ToInt32(input[3]);
        }
    }
}
