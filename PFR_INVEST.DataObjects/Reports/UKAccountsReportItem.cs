﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.DataObjects.Reports
{
    public class UKAccountsReportItem
    {
        public string SName { get; set; }
        public string RNum { get; set; }
        public string InnKppUpr { get; set; }
        public string FName { get; set; }
        public string ANum { get; set; }
        public string Bank { get; set; }
        public string CorAccNum { get; set; }
        public string BIK { get; set; }
        public string InnKppRec { get; set; }
    }
}
