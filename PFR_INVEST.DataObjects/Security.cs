﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class Security : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        private string _Name;
        public virtual string Name
        {
            get { return _Name; }
            set { if (_Name != value) { _Name = value; OnPropertyChanged("Name"); } }
        }

        [DataMember]
        private string _Issue;
        public virtual string Issue
        {
            get { return _Issue; }
            set { if (_Issue != value) { _Issue = value; OnPropertyChanged("Issue"); } }
        }

        [DataMember]
        private string _SecurityId;
        public virtual string SecurityId
        {
            get { return _SecurityId; }
            set { if (_SecurityId != value) { _SecurityId = value; OnPropertyChanged("SecurityId"); } }
        }

        [DataMember]
        private string _RegNum;
        public virtual string RegNum
        {
            get { return _RegNum; }
            set { if (_RegNum != value) { _RegNum = value; OnPropertyChanged("RegNum"); } }
        }

        [DataMember]
        private string _ISIN;
        public virtual string ISIN
        {
            get { return _ISIN; }
            set { if (_ISIN != value) { _ISIN = value; OnPropertyChanged("ISIN"); } }
        }

        [DataMember]
        private DateTime? _LocationDate;
        public virtual DateTime? LocationDate
        {
            get { return _LocationDate; }
            set { if (_LocationDate != value) { _LocationDate = value; OnPropertyChanged("LocationDate"); } }
        }

        [DataMember]
        private DateTime? _RepaymentDate;
        public virtual DateTime? RepaymentDate
        {
            get { return _RepaymentDate; }
            set { if (_RepaymentDate != value) { _RepaymentDate = value; OnPropertyChanged("RepaymentDate"); } }
        }

        [DataMember]
        private decimal? _IssueVolume;
        public virtual decimal? IssueVolume
        {
            get { return _IssueVolume; }
            set { if (_IssueVolume != value) { _IssueVolume = value; OnPropertyChanged("IssueVolume"); } }
        }

        [DataMember]
        private decimal? _PlacedVolume;
        public virtual decimal? PlacedVolume
        {
            get { return _PlacedVolume; }
            set { if (_PlacedVolume != value) { _PlacedVolume = value; OnPropertyChanged("PlacedVolume"); } }
        }

        [DataMember]
        private decimal? _NomValue;
        public virtual decimal? NomValue
        {
            get { return _NomValue; }
            set { if (_NomValue != value) { _NomValue = value; OnPropertyChanged("NomValue"); } }
        }

        [DataMember]
        private long? _PaymentPeriod;
        public virtual long? PaymentPeriod
        {
            get { return _PaymentPeriod; }
            set { if (_PaymentPeriod != value) { _PaymentPeriod = value; OnPropertyChanged("PaymentPeriod"); } }
        }

        [DataMember]
        private long? _KindID;
        public virtual long? KindID
        {
            get { return _KindID; }
            set { if (_KindID != value) { _KindID = value; OnPropertyChanged("KindID"); } }
        }

        [DataMember]
        private long? _CurrencyID;
        public virtual long? CurrencyID
        {
            get { return _CurrencyID; }
            set { if (_CurrencyID != value) { _CurrencyID = value; OnPropertyChanged("CurrencyID"); } }
        }

        [DataMember]
        private int? _status;
        public virtual int? Status
        {
            get { return _status; }
            set { if (_status != value) { _status = value; OnPropertyChanged("Status"); } }
        }

        [DataMember]
        public virtual string CurrencyName { get; set; }
        [DataMember]
        public virtual string KindName { get; set; }
    }
}
