﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class ROPSSum : BaseDataObject, IMarkedAsDeleted
    {
        [DataMember]
        public virtual long ID { get; set; }

		/// <summary>
		/// Дата распоряжения
		/// </summary>
		[DataMember]
		public virtual DateTime DirectionDate { get; set; }

		/// <summary>
		/// № распоряжения
		/// </summary>
        [DataMember]
        public virtual string DirectionNum { get; set; }

		/// <summary>
		/// Дата, по состоянию на которую рассчитан минимальный размер резерва
		/// </summary>
		[DataMember]
		public virtual DateTime Date { get; set; }

		/// <summary>
		/// Сумма минимального размера резерва
		/// </summary>
		[DataMember]
		public virtual decimal Sum { get; set; }

		/// <summary>
		/// Флаг удалёной записи (-1 = удалена)
		/// </summary>
		[DataMember]
		public virtual long StatusID { get; set; }
	}
}
