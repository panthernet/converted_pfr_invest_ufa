﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class F401402UKStatus : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() != this.GetType())
                return false;

            if (object.ReferenceEquals(this, obj))
                return true;

            var other = (F401402UKStatus)obj;
            if (ID == other.ID && Name == other.Name)
                return true;

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode() + (this.Name ?? string.Empty).GetHashCode();
        }
    }
}
