﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class F401402UK : BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual long? EdoID { get; set; }

        [DataMember]
        public virtual string DogSDNum { get; set; }

        [DataMember]
        public virtual DateTime? DogSDDate { get; set; }

        [DataMember]
        public virtual decimal? AverageScha { get; set; }

        [DataMember]
        public virtual decimal? NewTransh { get; set; }

        [DataMember]
        public virtual decimal? PayFromAverage { get; set; }

        [DataMember]
        public virtual decimal? PayFromTransh { get; set; }

        [DataMember]
        public virtual decimal? Itogo { get; set; }

        [DataMember]
        public virtual long? ContractId { get; set; }

        [DataMember]
        public virtual long? StatusID { get; set; }

        [DataMember]
        public virtual long? InitStatusID { get; set; }

        [DataMember]
        public virtual string ControlRegnum { get; set; }

        [DataMember]
        public virtual DateTime? ControlSendDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlGetDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlSetDate { get; set; }

        [DataMember]
        public virtual DateTime? ControlFactDate { get; set; }

        [DataMember]
        public virtual string StatusName { get; set; }

        [DataMember]
        public virtual string UKName { get; set; }

        [DataMember]
        public virtual string ContractNumber { get; set; }

        [DataMember]
        public virtual string OutgoingDocNumber { get; set; }

        [DataMember]
        public virtual DateTime? OutgoingDocDate { get; set; }

        [DataMember]
        public virtual DateTime? InitControlFactDate { get; set; }

        [DataMember]
        public virtual string InitOutgoingDocNumber { get; set; }

        [DataMember]
        public virtual DateTime? InitOutgoingDocDate { get; set; }
    }
}
