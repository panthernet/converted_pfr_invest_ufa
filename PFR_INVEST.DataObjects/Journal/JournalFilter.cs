﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects.Journal
{
	public class JournalFilter : BaseDataObject
	{
		[DataMember]
		public virtual DateTime? DateFrom { get; set; }

		[DataMember]
		public virtual DateTime? DateTo{ get; set; }

		[DataMember]
		public virtual string UserID { get; set; }

		[DataMember]
		public virtual string OsID { get; set; }

		[DataMember]
		public virtual string HostName { get; set; }

		[DataMember]
		public virtual string ObjectName { get; set; }

		[DataMember]
		public virtual string EventType { get; set; }

	}
}
