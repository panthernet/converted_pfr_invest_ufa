﻿using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    public class UKReportType : BaseDataObject, IIdentifiable
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string ReportType { get; set; }

        [DataMember]
        public virtual string ShortName { get; set; }

        [DataMember]
        public virtual string FullName { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

    }
}
