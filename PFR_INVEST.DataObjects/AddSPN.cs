﻿using System;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{

	public class AddSPN : BaseDataObject
	{
		public enum Kinds
		{
			/// <summary>
			/// 1 Поступление СПН
			/// </summary>
			Due = 1,

			/// <summary>
			/// 2 Поступление пенни и штрафов
			/// </summary>
			Penny = 2
		}

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private long? _portfolioID;
		public virtual long? PortfolioID
		{
			get { return _portfolioID; }
			set { if (_portfolioID != value) { _portfolioID = value; OnPropertyChanged("PortfolioID"); } }
		}


		[DataMember]
		private long? _legalEntityID;
		public virtual long? LegalEntityID
		{
			get { return _legalEntityID; }
			set { if (_legalEntityID != value) { _legalEntityID = value; OnPropertyChanged("LegalEntityID"); } }
		}


		[DataMember]
		private DateTime? _operationDate;
		public virtual DateTime? OperationDate
		{
			get { return _operationDate; }
			set { if (_operationDate != value) { _operationDate = value; OnPropertyChanged("OperationDate"); } }
		}


		[DataMember]
		private DateTime? _newDate;
		public virtual DateTime? NewDate
		{
			get { return _newDate; }
			set { if (_newDate != value) { _newDate = value; OnPropertyChanged("NewDate"); } }
		}


		[DataMember]
		private string _regNum;
		public virtual string RegNum
		{
			get { return _regNum; }
			set { if (_regNum != value) { _regNum = value; OnPropertyChanged("RegNum"); } }
		}


		[DataMember]
		private decimal? _summ;
		public virtual decimal? Summ
		{
			get { return _summ; }
			set { if (_summ != value) { _summ = value; OnPropertyChanged("Summ"); } }
		}


        [DataMember]
        private decimal? _summNakop;
        public virtual decimal? SummNakop
        {
            get { return _summNakop; }
            set {
                if (_summNakop == value) return;
                _summNakop = value;
                OnPropertyChanged("SummNakop");
                OnPropertyChanged("Summ");
            }
        }

        [DataMember]
        private decimal? _summOther;
        public virtual decimal? SummOther
        {
            get { return _summOther; }
            set
            {
                if (_summOther == value) return;
                _summOther = value;
                OnPropertyChanged("SummOther");
                OnPropertyChanged("Summ");

            }
        }

        [DataMember]
        private decimal? _summPerc;
        public virtual decimal? SummPerc
        {
            get { return _summPerc; }
            set
            {
                if (_summPerc == value) return;
                _summPerc = value;
                OnPropertyChanged("SummPerc");
                OnPropertyChanged("Summ");

            }
        }

        [DataMember]
        private decimal? _summEmployer;
        public virtual decimal? SummEmployer
        {
            get { return _summEmployer; }
            set
            {
                if (_summEmployer == value) return;
                _summEmployer = value;
                OnPropertyChanged("SummEmployer");
                OnPropertyChanged("Summ");

            }
        }

		[DataMember]
		private string _comment;
		public virtual string Comment
		{
			get { return _comment; }
			set { if (_comment != value) { _comment = value; OnPropertyChanged("Comment"); } }
		}


		[DataMember]
		private long? _monthID;
		public virtual long? MonthID
		{
			get { return _monthID; }
			set { if (_monthID != value) { _monthID = value; OnPropertyChanged("MonthID"); } }
		}

		[DataMember]
		private long? _yearID;
		public virtual long? YearID
		{
			get { return _yearID; }
			set { if (_yearID != value) { _yearID = value; OnPropertyChanged("YearID"); } }
		}


		[DataMember]
		private int? _kind;
		public virtual int? Kind
		{
			get { return _kind; }
			set { if (_kind != value) { _kind = value; OnPropertyChanged("Kind"); } }
		}


		[DataMember]
		private decimal? _add;
		public virtual decimal? Add
		{
			get { return _add; }
			set { if (_add != value) { _add = value; OnPropertyChanged("Add"); } }
		}


		[DataMember]
		private decimal? _addDSV;
		public virtual decimal? AddDSV
		{
			get { return _addDSV; }
			set { if (_addDSV != value) { _addDSV = value; OnPropertyChanged("AddDSV"); } }
		}


		[DataMember]
		private long? _pfrBankAccountID;
		public virtual long? PFRBankAccountID
		{
			get { return _pfrBankAccountID; }
			set { if (_pfrBankAccountID != value) { _pfrBankAccountID = value; OnPropertyChanged("PFRBankAccountID"); } }
		}


		[DataMember]
		public virtual long? KDocID { get; set; }

		[DataMember]
		public virtual long? ClosingKDocID { get; set; }
	}
}
