﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
	public class LegalEntityCourierLetterOfAttorney : BaseDataObject, ICloneable, IMarkedAsDeleted
	{

		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private long _LegalEntityCourierID;
		public virtual long LegalEntityCourierID
		{
			get { return _LegalEntityCourierID; }
			set { if (_LegalEntityCourierID != value) { _LegalEntityCourierID = value; OnPropertyChanged("LegalEntityCourierID"); } }
		}

		[DataMember]
		private string _Number;
		public virtual string Number
		{
			get { return _Number; }
			set { if (_Number != value) { _Number = value; OnPropertyChanged("Number"); } }
		}

		[DataMember]
		private DateTime? _IssueDate;
		public virtual DateTime? IssueDate
		{
			get { return _IssueDate; }
			set { if (_IssueDate != value) { _IssueDate = value; OnPropertyChanged("IssueDate"); } }
		}

		[DataMember]
		private DateTime? _ExpireDate;
		public virtual DateTime? ExpireDate
		{
			get { return _ExpireDate; }
			set { if (_ExpireDate != value) { _ExpireDate = value; OnPropertyChanged("ExpireDate"); } }
		}

		[DataMember]
		private DateTime? _RegisterDate = DateTime.Now;
		public virtual DateTime? RegisterDate
		{
			get { return _RegisterDate; }
			set { if (_RegisterDate != value) { _RegisterDate = value; OnPropertyChanged("RegisterDate"); } }
		}

		/// <summary>
		/// Ссылка на биржу (ELEMENT) 
		/// </summary>
		[DataMember]
		private long? _StockID;
		public virtual long? StockID
		{
			get { return _StockID; }
			set
			{
				if (_StockID != value)
				{
					_StockID = value;
					OnPropertyChanged("StockID");
				}
			}
		}

		[DataMember]
		private string _StockName;
		public virtual string StockName
		{
			get { return _StockName; }
			set { if (_StockName != value) { _StockName = value; OnPropertyChanged("StockName"); } }
		}

		[DataMember]
		private long _StatusID;
		public virtual long StatusID
		{
			get { return _StatusID; }
			set { if (_StatusID != value) { _StatusID = value; OnPropertyChanged("StatusID"); } }
		}

		public override string this[string columnName]
		{
			get
			{				
				switch (columnName)
				{
					case "Number": return Number.ValidateRequired() ?? Number.ValidateNonEmpty();
					case "IssueDate": return IssueDate.ValidateRequired();
					case "RegisterDate": return RegisterDate.ValidateRequired();
					case "ExpireDate": return ExpireDate.ValidateRequired();

				}
				return null;
			}
		}


		public virtual object Clone()
		{
			return new LegalEntityCourierLetterOfAttorney()
			{
				ID = this.ID,
				LegalEntityCourierID = this.LegalEntityCourierID,
				Number = this.Number,
				IssueDate = this.IssueDate,
				ExpireDate = this.ExpireDate,
				RegisterDate = this.RegisterDate,
				StockID = this.StockID,
				StockName = this.StockName
			};

		}
	}
}
