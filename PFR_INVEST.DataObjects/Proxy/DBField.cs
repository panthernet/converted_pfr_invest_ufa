﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PFR_INVEST.Proxy
{
    [DataContract]
    public enum FIELD_TYPE
    {
        [EnumMember]
        PrimaryKey = 0,
        [EnumMember]
        ForeignKey = 1,
        [EnumMember]
        Data = 2
    }

    [DataContract]
    public enum DATA_TYPE
    {
        [EnumMember]
        Long = 0,
        [EnumMember]
        String = 1,
        [EnumMember]
        Date = 2,
        [EnumMember]
        Double = 3
    }

    [DataContract]
    public class DBField : INotifyPropertyChanged, IDataErrorInfo
    {
        public string Error { get { return string.Empty; } }
        public virtual string this[string key]
        {
            get
            {
                return null;
            }
        }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string DbName { get; set; }
        [DataMember]
        public string Scheme { get; set; }
        [DataMember]
        public string DbTable { get; set; }
        [DataMember]
        public FIELD_TYPE FieldType { get; set; }
        [DataMember]
        public short Scale { get; set; }
        [DataMember]
        public DATA_TYPE DataType { get; set; }

        private object _value;
        [DataMember]
        public object Value
        {
            get { return _value; }
            set 
            {
                if (value == null || value is DBNull)
                {
                    _value = null;
                    OnPropertyChanged();
                }
                else
                {
                    try
                    {
                        if (DataType == DATA_TYPE.Long)
                        {
                            _value = Convert.ToInt64(value);
                        }
                        else if (DataType == DATA_TYPE.String)
                        {
                            _value = Convert.ToString(value);
                        }
                        else if (DataType == DATA_TYPE.Double)
                        {
                            _value = Convert.ToDecimal(value);
                        }
                        else if (DataType == DATA_TYPE.Date)
                        {
                            _value = Convert.ToDateTime(value);
                        }
                    }
                    catch
                    {
                        if (DataType == DATA_TYPE.Date)
                        {
                            _value = DateTime.Today;
                        }
                        else
                        {
                            _value = null;
                        }
                    }

                    if (((value is DateTime) && (DataType != DATA_TYPE.Date)))
                        throw new Exception("Несоответствие типов данных");

                    //this._value = value;
                    OnPropertyChanged();
                }
            }
        }
        public bool IsDataChanged { get; set; }


        public DBField(DBField fld)
        {
            if (fld == null)
                return;

            Name = fld.Name;
            DbName = fld.DbName;
            DbTable = fld.DbTable;
            FieldType = fld.FieldType;
            DataType = fld.DataType;
            Scheme = fld.Scheme;
            Value = fld.Value;
            Scale = fld.Scale;
        }

        public DBField(string name, string dbName, string scheme, string dbTable, FIELD_TYPE fieldType, DATA_TYPE dataType, short scale)
        {
            Name = name;
            DbName = dbName;
            DbTable = dbTable;

            if (((fieldType == FIELD_TYPE.ForeignKey) || (fieldType == FIELD_TYPE.PrimaryKey)) &&
                 (dataType != DATA_TYPE.Long))
                throw new Exception("Неверно задан тип ключевого поля");

            FieldType = fieldType;
            DataType = dataType;
            Scheme = scheme;
            Scale = scale;

            if (DataType == DATA_TYPE.Date)
                _value = DateTime.MinValue;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged()
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(string.Format("Fields", Name)));
                handler(this, new PropertyChangedEventArgs(string.Format("Value", Name)));
                IsDataChanged = true;
            }
        }

        #endregion
    }
}
