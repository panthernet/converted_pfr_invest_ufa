﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PFR_INVEST.DataObjects
{
	public class Payment : BaseDataObject
	{
		[DataMember]
		public virtual long ID { get; set; }

		[DataMember]
		private DateTime? _PaymentDate;
		public virtual DateTime? PaymentDate
		{
			get { return _PaymentDate; }
			set { if (_PaymentDate != value) { _PaymentDate = value; OnPropertyChanged("PaymentDate"); } }
		}

		[DataMember]
		private decimal? _CouponPeriod;
		public virtual decimal? CouponPeriod
		{
			get { return _CouponPeriod; }
			set { if (_CouponPeriod != value) { _CouponPeriod = value; OnPropertyChanged("CouponPeriod"); } }
		}

		[DataMember]
		private decimal? _Coupon;
		public virtual decimal? Coupon
		{
			get { return _Coupon; }
			set { if (_Coupon != value) { _Coupon = value; OnPropertyChanged("Coupon"); } }
		}

		[DataMember]
		private decimal? _RepaymentDebt;
		public virtual decimal? RepaymentDebt
		{
			get { return _RepaymentDebt; }
			set { if (_RepaymentDebt != value) { _RepaymentDebt = value; OnPropertyChanged("RepaymentDebt"); } }
		}

		[DataMember]
		private decimal? _PaymentBond;
		public virtual decimal? PaymentBond
		{
			get { return _PaymentBond; }
			set { if (_PaymentBond != value) { _PaymentBond = value; OnPropertyChanged("PaymentBond"); } }
		}

		[DataMember]
		private decimal? _Nominal;
		public virtual decimal? Nominal
		{
			get { return _Nominal; }
			set { if (_Nominal != value) { _Nominal = value; OnPropertyChanged("Nominal"); } }
		}

		[DataMember]
		private decimal? _NotRepaymentNom;
		public virtual decimal? NotRepaymentNom
		{
			get { return _NotRepaymentNom; }
			set { if (_NotRepaymentNom != value) { _NotRepaymentNom = value; OnPropertyChanged("NotRepaymentNom"); } }
		}

		[DataMember]
		private string _PaymentSize;
		public virtual string PaymentSize
		{
			get { return _PaymentSize; }
			set { if (_PaymentSize != value) { _PaymentSize = value; OnPropertyChanged("PaymentSize"); } }
		}
				
		[IgnoreDataMember]
		public virtual int? PaymentSizeInt
		{
			get
			{
				int n = 0;
				if (int.TryParse(this.PaymentSize, out n))
					return n;
				else
					return null;
			}
			set { this.PaymentSize = value.HasValue ? value.Value.ToString() : string.Empty; }
		}

		[DataMember]
		private decimal? _CouponSize;
		public virtual decimal? CouponSize
		{
			get { return _CouponSize; }
			set { if (_CouponSize != value) { _CouponSize = value; OnPropertyChanged("CouponSize"); } }
		}

		[DataMember]
		private decimal? _RepaymentNom;
		public virtual decimal? RepaymentNom
		{
			get { return _RepaymentNom; }
			set { if (_RepaymentNom != value) { _RepaymentNom = value; OnPropertyChanged("RepaymentNom"); } }
		}

		[DataMember]
		private long? _SecurityId;
		public virtual long? SecurityId
		{
			get { return _SecurityId; }
			set { if (_SecurityId != value) { _SecurityId = value; OnPropertyChanged("SecurityId"); } }
		}


		[DataMember]
		public virtual string SecurityName { get; set; }
	}
}
