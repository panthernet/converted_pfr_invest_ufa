﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PFR_INVEST.DataObjects
{
	[DataContract(IsReference = true)]
	public abstract class BaseDataObject : INotifyPropertyChanged, IDataErrorInfo 
	{
		[DataMember]
        [XmlIgnore]
		public virtual Dictionary<string, object> ExtensionData { get; set; }

        
	    protected BaseDataObject()
		{
            Error = string.Empty;
		}

		public static decimal? ConvertDecimalToNormal(decimal? pD, int pIScale)
		{
			if (pD == null)
				return null;
			var result = pD;
			for (var i = 0; i < pIScale; i++)
			{
				result = result * 0.1M;
			}
			return result;
		}

		#region INotifyPropertyChanged Members

		public virtual event PropertyChangedEventHandler PropertyChanged;

		public virtual void OnPropertyChanged(string propertyName)
		{
		    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

	    #endregion

		#region IDataErrorInfo Members

		public class ValidateEventArgs : EventArgs
		{
			public string PropertyName { get; set; }
			public string Error { get; set; }

			public ValidateEventArgs(string propertyName) { PropertyName = propertyName; }
		}

		public virtual event EventHandler<ValidateEventArgs> OnValidate;


        [IgnoreDataMember]
        [XmlIgnore]
        public virtual string Error { get; private set; }

		public virtual string this[string columnName]
		{
			get
			{
			    if (OnValidate == null) return null;
			    var e = new ValidateEventArgs(columnName);
			    OnValidate(this, e);
			    return e.Error;
			}
		}

        /// <summary>
        /// Универсальный метод валидации. Проверяет все поля сущности методом this[].
        /// </summary>
        /// <returns></returns>
        public virtual bool Validate() 
        {
            Error = string.Empty;

            var props = GetType().GetProperties().Where(p=>p.CanRead && p.CanWrite);
            foreach (var error in props.Select(p => this[p.Name]).Where(error => !string.IsNullOrEmpty(error))) {
                Error = error;
                return false;
            }
            return true;
        }

		#endregion
        /// <summary>
        /// ReadOnly flag for set/check acces for using when DataObjects will save without wrap on ViewModel
        /// </summary>
	    public virtual bool IsReadOnly { get; set; }
		
	}
}
