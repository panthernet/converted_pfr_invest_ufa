﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace PFR_INVEST.DataObjects
{
    [DataContract]
    public class RejectApplication: BaseDataObject
    {
        [DataMember]
        public virtual long ID { get; set; }

        [DataMember]
        public virtual string RegNum { get; set; }

        [DataMember]
        public virtual long? Region { get; set; }

        [DataMember]
        public virtual DateTime? RegDate { get; set; }

        [DataMember]
        public virtual string SelectionIdentifier { get; set; }

        [DataMember]
        public virtual string DocType { get; set; }

        [DataMember]
        public virtual string ErrorCode { get; set; }

        [DataMember]
        public virtual string FromIdentifier { get; set; }

        [DataMember]
        public virtual string Portfolio { get; set; }

        [DataMember]
        public virtual string ContractDockCode { get; set; }

        [DataMember]
        public virtual long? FileId { get; set; }

        [DataMember]
        public virtual string SelectionName { get; set; }

        [DataMember]
        public virtual string FromName { get; set; }

        public RejectApplication() { }

        public RejectApplication(object[] input)
        {
            ID = (long) input[0];
            RegNum = (string) input[1];
            Region = (long)input[2];
            RegDate = (DateTime?) input[3];
            SelectionIdentifier =(string) input[4];
            DocType = (string) input[5];
            ErrorCode = (string) input[6];
            FromIdentifier = (string) input[7];
            Portfolio = (string) input[8];
            ContractDockCode = (string) input[9];
            FileId = (long)input[10];
            FromName = (string)input[11];
            SelectionName = (string)input[12];
        }
    }
}
