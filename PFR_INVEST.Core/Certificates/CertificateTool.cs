﻿using System;

namespace PFR_INVEST.Core.Certificates
{
    public static class CertificateTool
    {
        public static string ExtractUserName(string p_Subject)
        {
            string[] names = p_Subject.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return names[0].Split(new char[] { '=' })[1].Trim();
        }

        public static string ExtractDomain(string p_Subject)
        {
            string[] names = p_Subject.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string result = "";
            foreach (string str in names)
            {
                if (str.Trim().ToLower().StartsWith("dc"))
                {
                    if (!string.IsNullOrEmpty(result)) result += ".";
                    result += str.Trim().Substring(3, str.Trim().Length - 3);
                }
            }
            return result;// string.Format("{0}.{1}.{2}", names[2].Split(new char[] { '=' })[1], names[3].Split(new char[] { '=' })[1], names[4].Split(new char[] { '=' })[1]);
        }
    }
}
