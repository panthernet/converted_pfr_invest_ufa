﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.Core.ListExtensions
{
    public static class LinqListExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if(source == null) return;
            foreach (T obj in source)
                action(obj);
        }
    }
}

namespace PFR_INVEST.Core.Tools
{

    public static class LinqExtensions
    {
        public static byte[] GetBytes(this string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(this byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char) + 1];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
