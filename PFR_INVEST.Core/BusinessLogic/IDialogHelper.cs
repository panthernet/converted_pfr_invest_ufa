﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;
using Document = PFR_INVEST.DataObjects.Document;

namespace PFR_INVEST.Core.BusinessLogic
{
	public interface IDialogHelper
	{
        Contract ShowSelectContract();

        string OpenFile(string filter);
	    string SaveFile(string filter);
		string[] OpenFiles(string filter);
		string OpenCatalog(string description = null, string selectedPatch = null, System.Environment.SpecialFolder rootFolder = System.Environment.SpecialFolder.MyComputer);
		Person ShowPersonLookup();

        void SelectReqTransferFromAssPay(long? ukPlanId);

		PortfolioFullListItem SelectPortfolio(params Portfolio.Types[] types);
		PortfolioFullListItem SelectPortfolio(bool rOnly, params Portfolio.Types[] types);
		PortfolioFullListItem SelectPortfolio(bool rOnly, bool excludeTypes, params Portfolio.Types[] types);
		/// <summary>
		/// Спец версия выбора счета в платежке по перечислениям НПФ
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		PortfolioFullListItem SelectPortfolio(PortfolioIdentifier.PortfolioPBAParams p);
		decimal? GetCourceManuallyOrFromCBRSite(long p_CurrencyID, DateTime p_CourceDate);

		List<NPFforERZLNotifyListItem> AddNPFToOther(List<NPFforERZLNotifyListItem> lst, long SelectedNPFCAID);
		List<NPFforERZLNotifyListItem> AddOther2NPF(List<NPFforERZLNotifyListItem> lst, long SelectedNPFCAID);

        bool AddNpfForFinregister(long registerId, long limitZl, decimal limitSpn, long parentId);

		bool AddContact(long leID);
		bool EditContact(long leID, long cId);
		void AddBranchSI(long leID);
		void AddBranchNPF(long leID);
		void EditBranchNPF(long leID);

		void AddDeposit();
		bool AddPayment(long SecurityID);

		bool AddNotify(long erzlID, List<ERZLNotifiesListItem> lst);
		bool EditNotify(long erzlID, List<ERZLNotifiesListItem> lst, ERZLNotifiesListItem focused);

		bool CheckTreasurityBaseDocument(int count, bool isDSV);

		bool ConfirmNPFNameChanged();

		bool ConfirmHistoryChanged();

        bool ConfirmOwnCapitalRewrite();

		bool ConfirmUpdateSIName();

		bool ConfirmSIRegisterDeleting();
		bool ConfirmSITransferListDeleting();

		bool ConfirmDeletingTemplates();

		bool RenameSI(long ID);
		void ViewOldSIName(long ID);

		bool SuspendActivity(long caID);
		DateTime? AnnulLicence();

		bool ViewPayment(long ID);

		bool CreatePFRContact(long BranchID);

		//bool ReturnDep(long depositID);

		bool AddUser();
		bool DeleteUser();

		/// <summary>
		/// Отображает модальное окно с выподающим списком счетов.
		/// </summary>
		/// <param name="excludeIDs">Идентификаторы счетов которые не попадут в выподающий список</param>
		/// <returns></returns>
		PortfolioPFRBankAccountListItem AddAccount(long[] excludeIDs);

		bool UploadTemplate();

		void AddMP(object model);

        /// <summary>
        /// Показать обычное диалоговое окно
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="p">Параметры форматирования сообщения</param>
		void ShowAlert(string msg, params object[] p);

        /// <summary>
        /// Показать диалоговое окно с ошибкой
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="p">Параметры форматирования сообщения</param>
		void ShowError(string msg, params object[] p);
        void RefreshUndeleteLists(string[] updateViewModels);

        /// <summary>
        /// Показать диалоговое окно с ошибкой
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок окна</param>
        void ShowError(string msg, string caption = null);

	    /// <summary>
	    /// Показать диалоговое окно с предупреждением
	    /// </summary>
	    /// <param name="msg">Сообщение</param>
	    /// <param name="caption">Заголовок окна</param>
	    /// <param name="yesNo">Использовать кнопки Да/Нет</param>
        /// <param name="okCancel">использовать кнопки Ок/Отмена, если не заданы Да/нет</param>
	    bool ShowWarning(string msg, string caption = null, bool yesNo = false, bool okCancel = false);

	    /// <summary>
	    /// Показать диалоговое окно с восклицательным знаком
	    /// </summary>
	    /// <param name="msg">Сообщение</param>
	    /// <param name="caption">Заголовок</param>
	    /// <param name="yesNo">Использовать кнопки Да/Нет</param>
	    bool ShowExclamation(string msg, string caption, bool yesNo = false);

        /// <summary>
        /// Диалог с информацией
        /// </summary>
        /// <param name="msg">Сообщение</param>
        /// <param name="caption">Заголовок</param>
	    void ShowInformation(string msg, string caption = null);

	    /// <summary>
	    /// Диалог, вопрос да/нет
	    /// </summary>
	    /// <param name="msg">Сообщение</param>
	    /// <param name="caption">Заголовок</param>
	    bool ShowQuestion(string msg, string caption);

	    /// <summary>
	    /// Диалог подтверждения
	    /// </summary>
	    /// <param name="msg">Сообщение</param>
	    /// <param name="caption">Заголовок</param>
	    bool ShowConfirmation(string msg, string caption = null);

		DlgSecurityListItem SelectSecurity();

	    void ShowOnesPkipPathSettings(int setting);

        void ShowOnesDefSettings();

		bool IgnorZLCountOwerflow(DateTime date, long zlCount);

        bool ViewOwnCapitalHistory(long bankId);
        bool EditOwnCapital(OwnCapitalHistory x, List<OwnCapitalHistory> items, bool isNew);

		bool EditLegalEntityHead(LegalEntityHead head, bool isNew);
		bool EditLegalEntityRating(LegalEntityRating rating, bool isNew, IList<long> excludedAgenceis);
		bool EditBankStockCode(BankStockCode rating, bool isNew, IList<long> excludedAgenceis);
		bool EditRating(Rating r, bool isNew);

		bool ConfirmImportDepClaim2Rewrite();

		bool ConfirmImportDepClaimMaxRewrite(string message);

		bool ConfirmImportKORewrite();

        bool ConfirmImportOwnCapitalRewrite(DateTime ReportDate);

		DateTime? RetireChief(DateTime? inaugurationDate);

		bool EditNpfCourier(LegalEntityCourier x, bool isNew);

		bool EditBankPerson(LegalEntityCourier x, bool isNew);

        bool EditCourierLetterOfAttorney(LegalEntityCourierLetterOfAttorney x, bool isNew);

        bool EditCourierCertificate(LegalEntityCourierCertificate x, bool isNew);

		void ShowNpfCourierDeletedList(List<LegalEntityCourier> CourierListDeleted);

		void ViewNpfCourier(LegalEntityCourier x);

        bool? ConfirmLegalEntityIdentifierSaveOnClose();

		bool? ConfirmLegalEntityCourierSaveOnClose();

        bool? ConfirmSaveOnClose();        

		bool EditNpfContact(LegalEntityCourier contact, bool isNew);

        bool EditNpfIdentifier(LegalEntityIdentifier identifier, bool isNew, List<LegalEntityIdentifier> existingLEIdentifiers);

        bool EditUKIdentifier(LegalEntityIdentifier identifier, bool isNew, List<LegalEntityIdentifier> existingLEIdentifiers);

        bool ShowAddSumForPortfolio(long offerId, decimal sum, out OfferPfrBankAccountSum offerPfrBankAccountSum, int valMaxDigits = 18, bool onlyInteger = false);

		bool ShowEditSumForPortfolio(OfferPfrBankAccountSum sum);

		void ShowDepClaim2OfferAcceptedList(IEnumerable<DepClaimOffer> listOffer, DateTime date);

		void AddViolationCategory();

		bool ShowDepositReturnList(IEnumerable<DepositReturnListItem> listOffer, DateTime date);

		bool ShowEditDepositReturnSum(ref DateTime paymentDate, ref decimal amountActual, decimal amountMax);

		void ShowEditDepositSetPercent(List<DepositReturnListItem> list);

        ReportIPUKPrompt ShowReportIPUKPrompt(bool isTotal = false);

		ReportDepositsPrompt ShowReportDepositsPrompt();

		ReportRSASCAPrompt ShowReportRSASCAPrompt(Document.Types type);

		ReportInvestRatioPrompt ShowReportInvestRatioPrompt(Document.Types type);

		ReportF060Prompt ShowReportF060Prompt(Document.Types type);

		ReportF070Prompt ShowReportF070Prompt(Document.Types type);

		ReportProfitabilityPrompt ShowReportProfitabilityPrompt(Document.Types type);

		ReportOwnedFoundsPrompt ShowReportOwnedFoundsPrompt(Document.Types type);
		
		

		ReportBalanceUKPrompt ShowReportBalanceUKPrompt();

        CommonRegisterListItem ShowSelectRegister(AsgFinTr.Sections section, AsgFinTr.Directions direction, string content = null);
        CommonRegisterListItem ShowSelectRegisterForPP(AsgFinTr.Sections section, long direction);

		FinregisterListItem SelectFinregister(long registerID);

		ReqTransferListItem SelectReqTransferSI(long registerID);

		ReqTransferListItem SelectReqTransferVR(long registerID);

        void ShowErrorReport(ErrorReportType type);

		AsgFinTr SelectUnlinkedCommonPP(long? trID, AsgFinTr.Sections section, AsgFinTr.Directions direction);

	    List<long> ShowOnesSelectExportDataDlg(OnesSettingsDef.IntGroup npf);

        bool CreateReportForCB(bool forceOne);

	    bool BOReportForm1CheckForAvailability(int quarter, int year);

        void RefreshBOReportForm1(BOReportForm1 selectedReport, bool showAfterUpdate = true);
        void RefreshCBReportManagerList(long id = 0);
        BOReportForm1Sum BOReportForm1AddSum(long reportId);
        void CBReportExportToWord(long reportId, bool skipGeneration = false);
	    void CBReportExportToXml(long reportId);

        bool EditCBPaymentDetail(CBPaymentDetail item, bool isNew);
	    void ImportOpfr(long id);
	    void ShowLoadingIndicator(string message = null);
	    void HideLoadingIndicator();
	    void CreateCBReportWizard();
	    void OpenCBReportApprovementStatus(long? id, bool isApproveMode = false);
	    bool? ShowYesNoCancelMessage(string message, string caption = null);
	    void OpenSpnDepositWizard(long id, int auctionStatusID);
	    void OpenChfrReportDialog();
        List<SIImportListItem> ShowImportSIWizardDialog(SIRegister.Directions dir);
	    void CreateCBMonthlyReport();
	    void CBMonthlyReportExport(long reportId);

        ReportFile ShowAggregateSPNReportDialog();
	    ReqTransferListItem SelectReqTransferOPFR(long registerID);
	    long[] ShowAddTransferDialog(long direction);
	    ReportFile ShowReportOpsFromNpfPrompt();
	}
}
