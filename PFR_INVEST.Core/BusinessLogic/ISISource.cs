﻿namespace PFR_INVEST.BusinessLogic
{
    public interface ISISource
    {
        long GetSIID();
    }
}
