﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public static class PersonHelper
    {
        public static List<Person> GetAvailablePersons(Document.Types dep)
        {   return DataContainerFacade.GetList<Person>().Where(p => !p.IsDisabled).OrderBy(a => a.FormattedFullName).ToList();

            /*string part = null;
            switch (dep)
            {
                case Document.Types.SI:
                    part = "ОВСИ";
                    break;
                case Document.Types.Npf:
                    part = "НПФ";
                    break;
                case Document.Types.VR:
                    part = "ВР";
                    break;
                case Document.Types.OPFR:
                    part = "БО";
                    break;
                default:
                    return new List<Person>();
            }
            var division = DataContainerFacade.GetList<Division>().FirstOrDefault(a=> a.ShortName.Equals(part, StringComparison.OrdinalIgnoreCase));
            return division == null
                ? new List<Person>()
                : DataContainerFacade.GetList<Person>().Where(p => !p.IsDisabled && p.DivisionId == division.ID).OrderBy(a => a.FormattedFullName).ToList();*/
        }
    }
}
