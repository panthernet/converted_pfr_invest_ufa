﻿using System.IO;
using System.Text;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.DataObjects.XMLModel;

namespace PFR_INVEST.BusinessLogic.Helper
{
    public static class DepClaimHelper
    {
        public static string SaveXML(XMLFileResult file, string folder = null)
        {
            if (!file.IsSuccesss)
                ViewModelBase.DialogHelper.ShowError(file.Error);
            else
            {
                folder = folder ?? Path.GetTempPath();
                var name = (!string.IsNullOrEmpty(folder) ? Path.Combine(folder, file.FileName) : file.FileName).AttachUniqueTimeStamp(true);

             /*   var index = 1;
                while (File.Exists(name))
                {
                    index++;
                    name = Path.Combine(folder, $"{fname} ({index}){ext}");
                }*/

                if (file.IsText)
                    File.WriteAllText(name, file.FileBody, Encoding.UTF8);
                if (file.IsBinary)
                    File.WriteAllBytes(name, file.FileBodyBinary);
                return name;
            }
            return null;
        }
    }
}
