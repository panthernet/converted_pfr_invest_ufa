﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Helper
{
    public sealed class QuarkDisplayHelper
    {
        private static Lazy<List<Quark>> _quarkSource;
        static QuarkDisplayHelper()
        {
            _quarkSource = new Lazy<List<Quark>>(() => DataContainerFacade.GetList<Quark>());
        }
        public static string GetQuarkDisplayValue(int? quarkNum)
        {
            if (!quarkNum.HasValue || quarkNum < 1 || quarkNum > 4 || _quarkSource.Value.FirstOrDefault(x => x.Index == quarkNum) == null)
                return "[не определено]";

            return _quarkSource.Value.First(x => x.Index == quarkNum).Name;

        }
        public static List<Quark> GetQuarks()
        {
            return _quarkSource.Value.ToList();
        }
    }
}
