﻿using db2connector.Contract;

namespace PFR_INVEST.BusinessLogic.Helper
{
    public static class AppSettingsHelper
    {
        private static readonly ServerSettings ServerSettings;

        static AppSettingsHelper()
        {
            ServerSettings = BLServiceSystem.Client.GetServerSettings(); 
        }
        public static bool IsOpfrTransfersEnabled => ServerSettings.IsOpfrTransfersEnabled;
        public static int PageSize => ServerSettings.PageSize;
    }
}
