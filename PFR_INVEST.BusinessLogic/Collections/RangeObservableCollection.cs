﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace PFR_INVEST.BusinessLogic.Collections
{
    public class RangeObservableCollection<T> : ObservableCollection<T>
    {
        private bool _suppressNotification;
        
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (!_suppressNotification)
                base.OnCollectionChanged(e);
        }

        public RangeObservableCollection(IEnumerable<T> items)
        {
            AddRange(items);
        }
        public RangeObservableCollection()
        {

        }

        public void RemoveWithoutNotification(T item)
        {
            _suppressNotification = true;
            Remove(item);
            _suppressNotification = false;
        }

        public void Fill(IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException(nameof(list));
            
            _suppressNotification = true;

            if (this.Any())
                Clear();

            foreach (T item in list)
            {
                Add(item);
            }
            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void AddRange(IEnumerable<T> list)
        {
            if (list == null)
                throw new ArgumentNullException(nameof(list));

            _suppressNotification = true;

            foreach (T item in list)
            {
                Add(item);
            }
            _suppressNotification = false;
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void ForEach(Action<T> executeItem)
        {
            foreach (var i in this)
            {
                executeItem(i);
            }
        }

    }
}
