﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class OwnFundsListViewModel : ViewModelList
    {
        private List<OwnFundsListItem> _mF50List;
        public List<OwnFundsListItem> F50List
        {
            get
            {
                RaiseDataRefreshing();
                return _mF50List;
            }
            set
            {
                _mF50List = value;
                OnPropertyChanged("F50List");
            }
        }

        public OwnFundsListViewModel()
        {
            DataObjectTypeForJournal = typeof(OwnedFounds);
        }

        protected override void ExecuteRefreshList(object param)
        {
            F50List = BLServiceSystem.Client.GetOwnedFoundsListHib();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
