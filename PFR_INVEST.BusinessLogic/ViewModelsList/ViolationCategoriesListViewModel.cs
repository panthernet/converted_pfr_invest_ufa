﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class ViolationCategoriesListViewModel : ViewModelList
    {
        private List<CategoryF140> _mCategoryF140List;
        public List<CategoryF140> CategoryF140List
        {
            get
            {
                RaiseDataRefreshing();
                return _mCategoryF140List;
            }
            set
            {
                _mCategoryF140List = value;
                OnPropertyChanged("CategoryF140List");
            }
        }

        public ViolationCategoriesListViewModel()
        {
            DataObjectTypeForJournal = typeof (CategoryF140);
        }

        protected override void ExecuteRefreshList(object param)
        {
            CategoryF140List = DataContainerFacade.GetList<CategoryF140>();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
