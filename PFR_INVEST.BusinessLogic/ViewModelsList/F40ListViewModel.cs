﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F40ListViewModel : PagedLoadListModelBase<F040DetailsListItem>
    {

        public F40ListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF040);
        }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetF040DetailsCount((int)Document.Types.All);
        }

        protected override List<F040DetailsListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF040DetailsByPage((int)Document.Types.All, (int)index).ToList();
        }

        protected override List<F040DetailsListItem> AfterListLoaded(List<F040DetailsListItem> list)
        {
            var lst = base.AfterListLoaded(list);

            lst = lst.GroupBy(d => d.EdoID).Select(d =>
            {
                var item = d.First();
                item.Details = d.Select(i => i.Detail).ToList();

                item.BuyCount = item.BuyCount ?? (long)item.Details.Sum(dt => dt.BuyCount ?? 0);
                item.BuyAmount = item.BuyAmount ?? item.Details.Sum(dt => dt.BuyAmount ?? 0);
                item.SellCount = item.SellCount ?? (long)item.Details.Sum(dt => dt.SellCount ?? 0);
                item.SellAmount = item.SellAmount ?? item.Details.Sum(dt => dt.SellAmount ?? 0);

                return item;
            }).ToList();

            return lst;

        }
    }
}
