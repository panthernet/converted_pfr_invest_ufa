﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class NetWealthsSIListViewModel : NetWealthsListViewModel
    { 
        public NetWealthsSIListViewModel() : base() { }

        protected override Document.Types DocumentType => Document.Types.SI;

        protected override void BeforeListRefresh()
        {
            this.dict = new Dictionary<string, List<NetWealthListItem>>();
            if (this.SettingsProvider == null)
                this.SettingsProvider = new OpenSettingsProvider("Чистые активы (СЧА) СИ", "Работа с СИ - Отчеты УК - Чистые активы (СЧА)", (s) => this.ExecuteRefreshList(null));
            if (!IsOnDataRefreshed)
            {
                OnDataRefreshed += (object sender, EventArgs e) => this.UpdateDocNumbers(this.TmpList);
            }
        }               
    }
}
