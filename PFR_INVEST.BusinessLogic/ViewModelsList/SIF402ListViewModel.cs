﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class SIF402ListViewModel : F401402UKListViewModel, ISettingOpenForm
    {

        override public bool ShowFilter => true;
        public OpenSettingsProvider SettingsProvider { get; set; }


        public SIF402ListViewModel()
            : base(Document.Types.SI)
        {
            // this.SettingsProvider = new Misc.OpenSettingsProvider("SIF402ViewModel", "Работа с СИ - Оплата услуг СД - Список оплат", (s) => this.ExecuteRefreshList(null));                
        }


        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetCountF401402Filtered((Document.Types)this.RefreshParameter, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<F401402UKListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF401402ListFilteredByPage((Document.Types)this.RefreshParameter, (int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override void BeforeListRefresh()
        {
            if (this.SettingsProvider == null)
                this.SettingsProvider = new OpenSettingsProvider("SIF402ViewModel", "Работа с СИ - Оплата услуг СД - Список оплат", (s) => this.ExecuteRefreshList(null));           
        }

		protected override List<F401402UKListItem> AfterListLoaded(List<F401402UKListItem> list)
		{
			var listRes = base.AfterListLoaded(list);
			Update(listRes);
			return listRes;
		}


        protected void Update(List<F401402UKListItem> part)
        {
            
            var groupedItems = part.GroupBy(x => x.EdoID).Select(x => x.GroupBy(y => y.DogSDNum)).ToList();
            var filteredItems = new List<F401402UKListItem>();

            // внутри группы по EdoId выбирается каждая первая запись во внутренней группе по DogSDNum
			filteredItems = groupedItems.SelectMany(x => x.Select(byDogSDNum => byDogSDNum.First())).ToList();
            //groupedItems.ForEach(x => filteredItems.AddRange(x.Select(byDogSDNum => byDogSDNum.First())));

            // заполнение счетчиков по годам и месяцам

            var groupedByDate = filteredItems.GroupBy(x => x.Month + x.Year).ToList();

            groupedByDate.ForEach(x => x.ToList().ForEach(y => y.MonthWithSubitemsCount = string.Format("{0}  ({1})", y.Month, x.Count())));						           
        }

    }
}
