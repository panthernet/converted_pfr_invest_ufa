﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
   public class ZLMovementsVRListViewModel : ZLMovementsListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            var movements = BLServiceSystem.Client.GetZLMovementsListByContractTypeHib((int)Document.Types.VR);
            var groupedMovements = GroupByContractNumber(movements);
            ZLMovements = Join(groupedMovements);
        }

    }
}
