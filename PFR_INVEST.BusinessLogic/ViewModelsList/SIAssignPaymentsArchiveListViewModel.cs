﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIAssignPaymentsArchiveListViewModel : AssignPaymentsArchiveListViewModel
    {
        //public readonly long OperationId = 8;
        private readonly long[] _availableOpIdList = { 8L, 10L, 11L };
        protected override List<TransferListItem> GetTransfers()
        {                  
            return BLServiceSystem.Client.GetTransfersListBYContractType(Document.Types.SI, true, true,false)
                 .Where(tr => (tr.RegisterOperationID == null || _availableOpIdList.Contains(tr.RegisterOperationID.Value)) && tr.MonthID > 0).ToList(); 
        }

        public SIAssignPaymentsArchiveListViewModel()
        {
            DataObjectTypeForJournal = typeof(SIRegister);
        }
    }
}
