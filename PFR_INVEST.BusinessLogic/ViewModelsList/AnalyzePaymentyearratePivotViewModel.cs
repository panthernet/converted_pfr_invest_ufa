﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzePaymentyearratePivotViewModel : ViewModelList<AnalyzePaymentyearrateReport>
    {
        public virtual string FormTitle => "Форма 7. Показатели назначения и выплаты пенсионных накоплений";
        public string FormTitleWithMeasure => $"{FormTitle}";

        public RangeObservableCollection<AnalyzePaymentyearrateData> ReportData { get; private set; }


        public AnalyzePaymentyearratePivotViewModel()
        {
            ReportData = new RangeObservableCollection<AnalyzePaymentyearrateData>();
            ExecuteRefreshList(null);
        }

        protected override void ExecuteRefreshList(object param)
        {
            if (ReportData != null)
            {
                var data = DataContainerFacade.GetClient().GetPaymentyearrateDataByYearKvartal();
                data.ForEach(d => d.QuarkText = QuarkDisplayHelper.GetQuarkDisplayValue(d.Kvartal));
                ReportData.Fill(data);
                RaiseDataRefreshing();
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}