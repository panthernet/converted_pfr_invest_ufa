﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class YieldsSIListViewModel : YieldsListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            YieldsList = BLServiceSystem.Client.GetMcProfitAbilityListHibByContractType((int)Document.Types.SI);
        }
    }
}
