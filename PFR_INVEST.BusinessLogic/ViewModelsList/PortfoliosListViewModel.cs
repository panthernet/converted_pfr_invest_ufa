﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class PortfoliosListViewModel : ViewModelList
	{
		public class PortfolioTypesFilter : PortfolioTypeFilter
		{
			public bool OnlyRubles;
		}

		public int FocusedRowHandle { get; set; }

		//public List<Element> PortfolioTypes { get; private set; }

		private List<PortfolioFullListItem> _mPortfolioList;
		public List<PortfolioFullListItem> PortfolioList
		{
			get
			{
				RaiseDataRefreshing();
				return _mPortfolioList;
			}
			set
			{
				_mPortfolioList = value;
				OnPropertyChanged("PortfolioList");
			}
		}

		private PortfolioFullListItem _mSelectedItem;
		public PortfolioFullListItem SelectedItem
		{
			get
			{
				return _mSelectedItem;
			}
			set
			{
				_mSelectedItem = value;
				OnPropertyChanged("SelectedItem");
			}
		}

		protected override void ExecuteRefreshList(object param)
		{
			if (param == null)
			{
				PortfolioList = BLServiceSystem.Client.GetPortfoliosByType().ToList();
			}
			else if (param is bool)
			{
				if ((bool)param)
					PortfolioList = BLServiceSystem.Client.GetFullPortfoliosListByType().ToList();
				else
					PortfolioList = BLServiceSystem.Client.GetPortfoliosByType().ToList();
			}
			else if (param is PortfolioTypesFilter)
			{
				var filter = (PortfolioTypesFilter)param;
				var list = new List<PortfolioFullListItem>();

				list.AddRange(BLServiceSystem.Client.GetPortfoliosByType(filter.Types, filter.Exclude));

				if (filter.OnlyRubles)
					list = list.FindAll(a => a.PfrBankAccount.CurrencyID == 1);

				var c = new PortfolioFullListComparer();
				ShowCurrency = true;
				PortfolioList = list.Distinct(c).ToList();
			}
			else if (param is PortfolioIdentifier.PortfolioPBAParams)
			{
				var p = (PortfolioIdentifier.PortfolioPBAParams)param;
				//скрываем валюту если есть фильтр по одной валюте
				//ShowCurrency = (p.CurrencyName == null || p.CurrencyName != null && p.CurrencyName.Count() > 1);
				var list = BLServiceSystem.Client.GetPortfoliosFiltered(p).ToList();
				PortfolioList = list;

				this.SelectedItem = p.SelectedPortfolioID.HasValue ? PortfolioList.FirstOrDefault(pl => pl.PortfolioID == p.SelectedPortfolioID.Value) : null;
			}
		}

		private readonly bool _roublesOnly;

		protected override bool CanExecuteRefreshList() { return true; }

		/// <summary>
		/// Все портфели, независимо от наличия счёта
		/// </summary>
		public PortfoliosListViewModel(bool allPortfolio)
			: base(allPortfolio)
		{
			FocusedRowHandle = -1;
			DataObjectTypeForJournal = typeof(Portfolio);
			Init();

		}

		public PortfoliosListViewModel(PortfolioIdentifier.PortfolioPBAParams p)
			: base(p)
		{
			FocusedRowHandle = -1;
			DataObjectTypeForJournal = typeof(Portfolio);
			Init();
		}

		public PortfoliosListViewModel(PortfolioTypesFilter filter)
			: base(filter)
		{
			FocusedRowHandle = -1;
			DataObjectTypeForJournal = typeof(Portfolio);
			_roublesOnly = filter.OnlyRubles;
			ExecuteRefreshList(filter);
			Init();
		}


		public PortfoliosListViewModel()
			: base(null) { Init(); }

		private void Init()
		{
			//PortfolioTypes = BLServiceSystem.Client.GetElementByType(Element.Types.PortfolioType);
		}

		private bool _mShowCurrency = true;
		public bool ShowCurrency
		{
			get
			{
				return _mShowCurrency;
			}
			set
			{
				if (_mShowCurrency == value) return;
				_mShowCurrency = value;
				OnPropertyChanged("ShowCurrency");
			}
		}
	}

	#region Comparer
	internal class PortfolioFullListComparer : IEqualityComparer<PortfolioFullListItem>
	{
		public bool Equals(PortfolioFullListItem x, PortfolioFullListItem y)
		{
			return ((x.PfrBankAccount == null || y.PfrBankAccount == null) || (x.PfrBankAccount.ID == y.PfrBankAccount.ID)) && x.PortfolioID == y.PortfolioID;
		}

		public int GetHashCode(PortfolioFullListItem o)
		{
			return (o.PortfolioID * (o.PfrBankAccount != null ? o.PfrBankAccount.ID : -1)).GetHashCode();
		}
	}
	#endregion
}
