﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BankAgentsListViewModel : ViewModelList
    {

        private IList<LegalEntity> m_BanksList;
        public IList<LegalEntity> BanksList
        {
            get
            {
                RaiseDataRefreshing();
                return m_BanksList;
            }
            set
            {
                m_BanksList = value;
                OnPropertyChanged("BanksList");
            }
        }

        public BankAgentsListViewModel()
        {
            DataObjectTypeForJournal = typeof (LegalEntity);
        }

        protected override void ExecuteRefreshList(object param)
        {
            BanksList = BLServiceSystem.Client.GetBankAgentListForDeposit();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
