﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRContractsListViewModel : ContractsListViewModel
    {
        public VRContractsListViewModel()
        {
            DataObjectTypeForJournal = typeof(Contract);
        }

        protected override List<SIContractListItem> GetContractItems()
        {
            return BLServiceSystem.Client.GetVRContractsListHib();
        }
    }
}
