﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepClaimMaxListViewModel : ViewModelList, IDepClaimMaxExportProvider
    {

        public DepClaimMaxListViewModel(DepClaimMaxListItem.Types type)
            : base(type)
        {
            DataObjectTypeForJournal = typeof (DepClaimMaxListItem);
            _depClaimType = type;
        }

        private readonly DepClaimMaxListItem.Types _depClaimType;
        public DepClaimMaxListItem.Types DepClaimType
        {
            get
            {
                RaiseDataRefreshing();
                return _depClaimType;
            }
        }


        private List<DepClaimMaxListItem> _depClaimMaxList;
        public List<DepClaimMaxListItem> DepClaimMaxList
        {
            get
            {
                RaiseDataRefreshing();
                return _depClaimMaxList;
            }
            set
            {
                _depClaimMaxList = value;
                OnPropertyChanged("DepClaimMaxList");
            }
        }


        public override bool CanExecuteDelete()
        {
            return CheckDeleteAccess(typeof(DepClaimMaxListViewModel));
        }

        protected override void ExecuteRefreshList(object param)
        {
            DepClaimMaxList = BLServiceSystem.Client.GetDepClaimMaxList((int)param);
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        public DepClaimMaxListItem ActiveItem { get; set; }
        public DepClaimMaxListItem SelectedDepClaimMax { get; set; }


        public long? AuctionID
        {
            get
            {
                if (ActiveItem == null)
                {                    
                    return SelectedDepClaimMax==null ? null:(long?)SelectedDepClaimMax.DepclaimselectparamsId ;
                }
                return ActiveItem.DepclaimselectparamsId;
            }
        }


        DepClaimSelectParams.Statuses? IDepClaimAuctionInfoProvider.AuctionStatus => (ActiveItem == null) ? SelectedDepClaimMax.AuctionStatus : ActiveItem.AuctionStatus;


        bool IDepClaimMaxExportProvider.IsMaxClaim => _depClaimType == DepClaimMaxListItem.Types.Max;

        bool IDepClaimMaxExportProvider.IsCommonClaim => _depClaimType == DepClaimMaxListItem.Types.Common;


        int? IDepClaimAuctionFullInfoProvider.DepClaimMaxCount
        {
            get
            {
                if (_depClaimType == DepClaimMaxListItem.Types.Max)
                {
                    return DepClaimMaxList.Count(dcm => dcm.DepclaimselectparamsId == AuctionID);
                }
                return null;
            }
        }

        int? IDepClaimAuctionFullInfoProvider.DepClaimCommonCount
        {
            get
            {
                if (_depClaimType == DepClaimMaxListItem.Types.Common)
                {
                    return DepClaimMaxList.Count(dcm => dcm.DepclaimselectparamsId == AuctionID);
                }
                return null;
            }
        }

        long? IDepClaimAuctionProvider.AuctionID => AuctionID;
    }
}
