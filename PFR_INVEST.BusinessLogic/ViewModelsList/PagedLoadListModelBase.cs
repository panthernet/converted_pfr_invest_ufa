﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Базовый интерфейс для моделей списков с постраничным обновлением
    /// </summary>
    public interface IPagedLoadListModel 
	{
        bool IsFaulted { get; set; }
        bool IsClosed { get; set; }

        event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Методы вызова обновления всего списка извне
        /// </summary>
        void RefreshListExternal();
	}

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	public abstract class PagedLoadListModelBase<T> : ViewModelList<T>, IPagedLoadListModel
    {
        public event EventHandler OnDataRefreshed;
        public bool IsOnDataRefreshed => OnDataRefreshed != null;

        /// <summary>
        /// Синхронное ожидание завершение подгрузки списка
        /// </summary>
        public List<T> WaitForList()
        {
            while (IsBusy) Thread.Sleep(10);
            return List;
        }

        /// <summary>
        /// Выводить время выполнения запросов в Output
        /// </summary>
        private const bool LOG_QUERY_TIMERS = false;

        private const bool LOG_THREADING = false;
        protected List<T> TmpList;
        protected List<T> MList;
        public List<T> List
        {
            get
            {
                RaiseDataRefreshing();
                return MList;
            }
            set
            {
                MList = value;
                OnPropertyChanged("List");
            }
        }


        protected virtual void UpdatePart(List<T> part) { }

        protected abstract long GetListCount();

        protected abstract List<T> GetListPart(long index);

        protected virtual void BeforeListRefresh() { }

        protected virtual bool BeforeListRefreshCheck() { return true; }

        protected override void ExecuteRefreshList(object param)
        {
            LogThreadMessage("BeforeListRefresh");
            BeforeListRefresh();
            
            if (!BeforeListRefreshCheck())
            {
                return;
            }

            List = new List<T>();
            LogThreadMessage("Set skipped");
            IsSkipped = true;
            while (IsBusy)
            {
                LogThreadMessage("Wait for isBusy drop");
                Thread.Sleep(100);
            }

            IsBusy = true;
            IsFaulted = false;
            IsSkipped = false;
            BusyMessage = "Загрузка данных";
            Task.Factory.StartNew(LoadListCount);
        }

        protected Stopwatch QueryTimer = new Stopwatch();

        protected void StartQueryTimer()
        {
            if (LOG_QUERY_TIMERS) QueryTimer.Restart();            
        }
        protected void StopQueryTimer(string msg)
        {
            if (LOG_QUERY_TIMERS)
            {
                Debug.WriteLine(msg, QueryTimer.Elapsed.Milliseconds);
                QueryTimer.Stop();
            }
        }

#if WEBCLIENT
        //убираем загрузку кол-ва записей
        private volatile int _webIndexCount = AppSettingsHelper.PageSize;
#endif

        protected void LoadListCount()
        {
            try
            {
                StartQueryTimer();
#if !WEBCLIENT
                Maximum = GetListCount();
#else
                Maximum = 1;
#endif
                StopQueryTimer("Count: {0}ms");
            }
            catch(Exception ex)
            {
                LogThreadMessage("Error on get count");
                Logger.WriteException(ex, "-----> PagedLoadListModelBase <-----");
                IsFaulted = true;
                Maximum = 0;
                IsBusy = false;
                throw ex;
            }

            if (Maximum < 0)
            {
                IsBusy = false; return;
            }
            BusyMessage = $"Загрузка записей 0 - {Maximum}";

            TmpList = new List<T>((int)Maximum);

            LogThreadMessage("Begin new Task");
            //Task.Factory.StartNew(LoadPart).ContinueWith(LoadNext);
            Task.Factory.StartNew(() => { }).ContinueWith(LoadNext);
        }

        protected void LoadPart()
        {
            LogThreadMessage("LoadPart invoked");
            StartQueryTimer();
            var index = TmpList.Count;
            try
            {
                var part = GetListPart(index);
                // обработка случая, когда невозможно выбрать новый элемент из списка, хотя запрошенное количество записей еще не выбрано (иначе загрузка зависает)
#if !WEBCLIENT
                if ((part == null || part.Count == 0) && Maximum > 0)
                {
                    throw new Exception("Получен пустой список при запросе страницы данных");
                }
#else
                _webIndexCount = part.Count;
#endif


                UpdatePart(part);
                TmpList.AddRange(part);
            }
            catch (Exception ex)
            {
                IsFaulted = true;
                Logger.WriteException(ex);
                throw;
            }

            BusyMessage = $"Загружено {TmpList.Count} из {Maximum} записей";
            LogThreadMessage("LoadPart finished");
            StopQueryTimer("Load part: {0}ms");
        }

        protected void LoadNext(Task task)
        {
            LogThreadMessage("LoadNext invoked, list count " + TmpList.Count, task);
            var condition = TmpList.Count < Maximum;
#if WEBCLIENT
            condition = _webIndexCount == AppSettingsHelper.PageSize;
#endif
            if (condition && !IsClosed && !IsFaulted && !IsSkipped)
            {
                LogThreadMessage("Load continue", task);
                Task.Factory.StartNew(LoadPart).ContinueWith(LoadNext);
            }
            else
            {
                if (IsSkipped && (TmpList.Count < Maximum && !IsClosed && !IsFaulted))
                {
                    LogThreadMessage("Load skipped", task);
                }
                else
                    LogThreadMessage("Load completed", task);

                BusyMessage = string.Format("Подождите. Идет обработка данных.");
                TmpList = AfterListLoaded(TmpList);
                ListLoadedInternal(TmpList);
                OnDataRefreshed?.Invoke(this, EventArgs.Empty);
                List = TmpList;
                IsBusy = false;
                AfterListRefreshFinished();
                StopQueryTimer("Load complete {0}");
                LogThreadMessage("LoadNext finished", task);
            }

        }

        /// <summary>
        /// Выполняется после завершения асинхронной загрузки списка
        /// </summary>
        protected virtual void AfterListRefreshFinished()
        {
        }

		protected virtual void ListLoadedInternal(List<T> list)
		{
		}

        /// <summary>
        /// Выполянется после завершения получения нового списка при обновлении
        /// </summary>
        /// <param name="list">Список</param>
        protected virtual List<T> AfterListLoaded(List<T> list)
        {
            return list;
        }

        private bool _mIsBusy;
        public bool IsBusy
        {
            get { return _mIsBusy; }
            protected set
            {
                if (_mIsBusy == value)
                    return;
                _mIsBusy = value;
                OnPropertyChanged("IsBusy");
            }
        }

        private string _mBusyMessage = string.Empty;
        public string BusyMessage
        {
            get { return _mBusyMessage; }
            protected set
            {
                if (_mBusyMessage == value)
                    return;
                _mBusyMessage = value;
                OnPropertyChanged("BusyMessage");
            }
        }

        protected bool IsOnDataRefreshedNoSet()
        {
            return OnDataRefreshed == null;
        }
        protected override bool CanExecuteRefreshList()
        {
            return !IsBusy;
        }

        protected PagedLoadListModelBase(object param = null)
            : base(param)
        {
            OnPropertyChanged("Maximum");
            OnPropertyChanged("Current");
        }

        public long Maximum
        {
            get;
            set;
        }

        public bool IsClosed { get; set; }

        public bool IsSkipped { get; set; }

        private bool _mIsFaulted;
        public bool IsFaulted
        {
            get { return _mIsFaulted; }
            set
            {
                if (_mIsFaulted == value)
                    return;
                _mIsFaulted = value;
                OnPropertyChanged("IsFaulted");
            }
        }

        private void LogThreadMessage(string message, Task task = null)
        {
            if (!LOG_THREADING) return;

            Logger.WriteLine(task != null
                ? $"Thread # {Thread.CurrentThread.ManagedThreadId} Task #{task.Id} - {message}"
                : $"Thread #{Thread.CurrentThread.ManagedThreadId} - {message}");
        }

        public void RefreshListExternal()
        {
            ExecuteRefreshList(null);
        }
    }
}
