﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzeInsuredpersonPivotViewModel : ViewModelList<AnalyzeInsuredpersonReport>
    {
        public virtual string FormTitle
            =>
                "Форма 4. Количество застрахованных лиц в системе обязательного пенсионного страхования, нарастающим итогом"
            ;

        public string FormTitleWithMeasure => $"{FormTitle}, (человек)";

        public RangeObservableCollection<AnalyzeInsuredpersonData> ReportData { get; private set; }

        public AnalyzeInsuredpersonPivotViewModel()
        {
            ReportData = new RangeObservableCollection<AnalyzeInsuredpersonData>();
            ExecuteRefreshList(null);
        }


        protected override void ExecuteRefreshList(object param)
        {
            if (ReportData != null)
            {
                var data = DataContainerFacade.GetClient().GetInsuredpersonDataByYearKvartal();
                data.ForEach(d => d.QuarkText = QuarkDisplayHelper.GetQuarkDisplayValue(d.Kvartal));
                ReportData.Fill(data);
                RaiseDataRefreshing();
            }
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}