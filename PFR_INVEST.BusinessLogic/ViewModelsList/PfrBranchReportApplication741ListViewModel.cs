﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_directory_editor)]
    public class PfrBranchReportApplication741ListViewModel : ViewModelList
    {
        private List<PfrBranchReportApplication741ListItem> _items;
        public List<PfrBranchReportApplication741ListItem> Items
        {
            get
            {
                RaiseDataRefreshing();
                return _items;
            }
            set
            {
                _items = value;
                OnPropertyChanged("Items");
            }
        }

        public PfrBranchReportApplication741ListViewModel()
        {
            DataObjectTypeForJournal = typeof(PfrBranchReportApplication741);
        }

        protected override void ExecuteRefreshList(object param)
        {
            Items = BLServiceSystem.Client.GetPfrReportApplication741List();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
