﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepositsArchiveListViewModel : ViewModelList
    {
        private List<DepositListItem> _depositsList = new List<DepositListItem>();
        public List<DepositListItem> DepositsList
        {
            get
            {
                return _depositsList;
            }
            set
            {
                _depositsList = value;
                OnPropertyChanged("DepositsList");
            }
        }

        public DepositsArchiveListViewModel(object param)
            : base(param)
        {
            DataObjectTypeForJournal = typeof(Deposit);
        }

        protected override void ExecuteRefreshList(object param)
        {
            var xL = BLServiceSystem.Client.GetDepositsListByPropertyHib2("Status", (long)2);//Возвращен
            xL.AddRange(BLServiceSystem.Client.GetDepositsListByPropertyHib2("Status", (long)4));//Возвращен с нарушениями

            DepositsList = xL;

            OnPropertyChanged("DepositsList");
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

    }
}
