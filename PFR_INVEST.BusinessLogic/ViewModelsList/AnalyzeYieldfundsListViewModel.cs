﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker,
        DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OFPR_worker,
        DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzeYieldfundsListViewModel : ViewModelListObservable<AnalyzeYieldfundsReport>
    {
        public virtual string FormTitle
            =>
                _actId == 107
                    ? "Форма 6.1 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 22.08.2005 № 107н «Об утверждении стандартов раскрытия информации об инвестировании средств пенсионных накоплений»"
                    : _actId == 140
                        ? "Форма 6.2 Доходность инвестирования средств пенсионных накоплений, рассчитанная в соответствии с приказом Минфина России от 18.11.2005 № 140н «Об утверждении порядка расчета результатов инвестирования средств пенсионных накоплений для их отражения в специальной части индивидуальных лицевых счетов»"
                        : string.Empty;

        public bool IsQuarkReport => _actId == 107;
        public bool IsArchiveMode { get; set; }
        public int? Year { get; set; }
        public int? Quark { get; set; }

        private int _actId = 107;

        private bool isInit;

        public AnalyzeYieldfundsListViewModel(int actId)
        {
            _actId = actId;
            isInit = true;
            RefreshList.Execute(null);
        }

        //public virtual bool WithSubtract => false;

        protected override void ExecuteRefreshList(object param)
        {
            if (isInit)
            {
                var data = DataContainerFacade.GetClient()
                    .GetYieldfundsReportsByYearKvartal(Year, Quark, _actId, IsArchiveMode);
                data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));

                List.Fill(data);
                //.Where(r => IsArchiveMode ? r.Status == -1 : r.Status == 0));
            }
        }
    }
}