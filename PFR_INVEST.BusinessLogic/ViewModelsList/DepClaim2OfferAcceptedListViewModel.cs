﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public class DepClaim2OfferAcceptedListViewModel : ViewModelList
	{
		private readonly IEnumerable<DepClaimOffer> _offerList;

		private ObservableCollection<OfferPfrBankAccountSumListItem> _list;

		public DepClaim2OfferAcceptedListViewModel(IEnumerable<DepClaimOffer> offerList)
		{
			DataObjectTypeForJournal = typeof(DepClaimOffer);

			_offerList = offerList;

			var a = BLServiceSystem.Client.GetOfferPfrBankAccountSumForOffers(_offerList.Select(item => item.ID).Distinct().ToArray());
			List = new ObservableCollection<OfferPfrBankAccountSumListItem>(a);

			var gL = from x in List group x by new { x.OfferPfrBankAccountSum.OfferId } into p select new { Items = p.ToList() };

			foreach (var g in gL)
			{
				if (g.Items.Count <= 1)// Группа нужна если больше 1 элемента
					continue;
				foreach (var x in g.Items)
					x.Group = g.Items;
			}
			OkCommand = new DelegateCommand(o => OkCommand_CanExecute(), OkCommand_Execute);
		}

		public DelegateCommand Cancel { get; private set; }

		public DelegateCommand OkCommand { get; private set; }

		public bool OkCommand_CanExecute()
		{
			return AllowGenerateDeposits;
		}

	    public static bool ExecuteAction(List<OfferPfrBankAccountSumListItem> list)
	    {
            var xL = list.Where(item => item.IsSelected).Select(item => item.OfferPfrBankAccountSum).ToList();
            //Проверяем, что за время редактирования формы никто ничего в базе не поменял
            if (xL.Select(d => d.OfferId).Distinct().Select(offerID => DataContainerFacade.GetByID<DepClaimOffer>(offerID)).Any(dbOffer => dbOffer == null || dbOffer.Status != DepClaimOffer.Statuses.Accepted))
            {
                DialogHelper.ShowAlert("Невозможно разместить депозиты, так как одна из оферт была изменена во время операции. Повторите операцию размещения депозита.");
                return false;
            }

            foreach (var d in xL)
            {
                var sumPF = DataContainerFacade.GetByID<OfferPfrBankAccountSum>(d.ID);
                if (sumPF == null || d.Sum != sumPF.Sum)
                {
                    DialogHelper.ShowAlert("Невозможно разместить депозиты, так как суммы по портфелю одной из оферт были изменены во время операции. Повторите операцию размещения депозита.");
                    return false;
                }
            }


            if (BLServiceSystem.Client.GenerateDeposits(xL).IsSuccess)
            {
                ViewModelManager.RefreshViewModels(
                    typeof(DepositsListViewModel),
                    typeof(DepositsListByPortfolioViewModel),
                    typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel),
                    typeof(TempAllocationListViewModel),
                    typeof(IncomeSecurityListViewModel)//, typeof(BalanceListViewModel)
                    );
                DialogHelper.ShowAlert("Депозиты успешно сформированы!");
            }
	        return true;
	    }

		private void OkCommand_Execute(object o)
		{
		    ExecuteAction(List.ToList());
		}

		public bool AllowGenerateDeposits
		{
			get
			{
				return List != null && List.Any(item => item.IsSelected);
			}
		}



		public ObservableCollection<OfferPfrBankAccountSumListItem> List
		{
			get { return _list; }
			set
			{
				_list = value;
				OnPropertyChanged("List");
			}
		}

		public OfferPfrBankAccountSumListItem SelectedItem { get; set; }

		protected override void ExecuteRefreshList(object param)
		{

		}

		protected override bool CanExecuteRefreshList()
		{
			return _offerList != null;
		}
	}
}
