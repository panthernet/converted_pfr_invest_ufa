﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepositsListViewModel : ViewModelList
    {
        private List<DepositListItem> m_DepositsList = new List<DepositListItem>();
        public List<DepositListItem> DepositsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_DepositsList;
            }
            set
            {
                m_DepositsList = value;
                OnPropertyChanged("DepositsList");
            }
        }

        public DepositsListViewModel(object param)
            : base(param)
        {
            DataObjectTypeForJournal = typeof(Deposit);
        }

        protected override void ExecuteRefreshList(object param)
        {

            var xL = BLServiceSystem.Client.GetDepositsListByPropertyHib2("Status", param);
            if ((long)param == 1)
            {
                xL.AddRange(BLServiceSystem.Client.GetDepositsListByPropertyHib2("Status", (long)3));
            }

            DepositsList = xL;
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
