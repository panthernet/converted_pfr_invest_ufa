﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.User)]
    public class AnalyzeIncomingstatementsPivotViewModel : ViewModelList<AnalyzeIncomingstatementsReport>
    {
        public virtual string FormTitle
            =>
                "Форма 6. Принятые заявления застрахованных лиц по формированию средств пенсионных накоплений с разбивкой по способу подачи"
            ;

        public string FormTitleWithMeasure
            =>
                $"{FormTitle} за {QuarkDisplayHelper.GetQuarkDisplayValue(Facade.SelectedKvartal)}, {Facade.SelectedYear}г., (заявок, шт.)"
            ; //$"{FormTitle}, (заявок, шт.)";


        public AnalyzeReportPivotFacade<AnalyzeIncomingstatementsData> Facade => _facade;
        private AnalyzeReportPivotFacade<AnalyzeIncomingstatementsData> _facade;

        public AnalyzeIncomingstatementsPivotViewModel()
        {
            _facade = new AnalyzeReportPivotFacade<AnalyzeIncomingstatementsData>(() => RaiseDataRefreshing(),
                () => DataContainerFacade.GetClient().GetIncomingstatementsDataByYearKvartal());
        }

        protected override void ExecuteRefreshList(object param)
        {
            _facade?.RefreshDBData();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}