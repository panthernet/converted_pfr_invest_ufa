﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class InsuranceVRListViewModel : InsuranceListViewModel
    {
        private const bool ARCHIVE = false;

        protected override void ExecuteRefreshList(object param)
        {
            InsuranceList = BLServiceSystem.Client.GetInsuranceListByType(ARCHIVE, (int)Document.Types.VR);
        }

        public InsuranceVRListViewModel()
            : base(ARCHIVE)
        {
            DataObjectTypeForJournal = typeof(InsuranceDoc);
        }



    }
}
