﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	public class MarketCostScopeListViewModel : ViewModelList, ISettingOpenForm
    {
        private IList<MarketCostListItem> m_MarketCostScopeList;
        public IList<MarketCostListItem> MarketCostScopeList
        {
            get
            {
                return m_MarketCostScopeList;
            }
            set
            {
                m_MarketCostScopeList = value;
                OnPropertyChanged("MarketCostScopeList");
            }
        }

        public MarketCostScopeListViewModel()
        {
            //
            DataObjectTypeForJournal = typeof(object);
            DataObjectDescForJournal = "Композитный список из данных таблиц EDO_ODK";
        }

		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		public OpenSettingsProvider SettingsProvider { get; set; }

        protected override void ExecuteRefreshList(object param)
        {
            // В лотусе частично 20,22,25 полностью 24, 26
            // 22 поглощено 25, 
            // Итого частично 20, 25 и полностью 24, 26

			if (this.SettingsProvider == null)
				this.SettingsProvider = new OpenSettingsProvider("MarketCostScopeListViewModel", "Работа с СИ - Отчеты УК - РСА совокупно", (s) => this.ExecuteRefreshList(null));

			var list = new List<MarketCostListItem>();
			if (SettingsProvider != null && SettingsProvider.IsPeriodChecked)
			{
				list.AddRange(BLServiceSystem.Client.GetMarketCostF020ListNullContract(SettingsProvider.PeriodStart, SettingsProvider.PeriodEnd)); //20
				list.AddRange(BLServiceSystem.Client.GetMarketCostF025ListNullContract(SettingsProvider.PeriodStart, SettingsProvider.PeriodEnd)); //25
				list.AddRange(BLServiceSystem.Client.GetMarketCostF024List(SettingsProvider.PeriodStart, SettingsProvider.PeriodEnd)); //24
				list.AddRange(BLServiceSystem.Client.GetMarketCostF026List(SettingsProvider.PeriodStart, SettingsProvider.PeriodEnd)); //26
			}
			else
			{
				list.AddRange(BLServiceSystem.Client.GetMarketCostF020ListNullContract()); //20
				list.AddRange(BLServiceSystem.Client.GetMarketCostF025ListNullContract()); //25
				list.AddRange(BLServiceSystem.Client.GetMarketCostF024List()); //24
				list.AddRange(BLServiceSystem.Client.GetMarketCostF026List()); //26
				//list.AddRange(BLServiceSystem.Client.GetMarketCostF022List());
				//list.AddRange(BLServiceSystem.Client.GetMarketCostF024List());// устарел, используем GetMarketCostF026List
			}

			
            MarketCostScopeList = list;
        }
    }
}
