﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class TransfersSIArchListViewModel : TransfersArchListViewModel
    {
        public override Document.Types DocumentType => Document.Types.SI;
    }
}
