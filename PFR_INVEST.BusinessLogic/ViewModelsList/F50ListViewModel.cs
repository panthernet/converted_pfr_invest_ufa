﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F50ListViewModel : PagedLoadListModelBase<F050ReportListItem>
    {

        public class F50ReportItemWrapper : F050ReportListItem
        {
            public F50ReportItemWrapper(F050ReportListItem value)
                : base()
            {
                value.CopyTo(this);
            }

            public virtual IList<F050CBInfo> Details => this.GetDetails();
        }

        #region YearsList


        private Dictionary<string, int> m_YearsList;
        public Dictionary<string, int> YearsList
        {
            get
            {
                return m_YearsList;
            }
            set
            {
                m_YearsList = value;
                OnPropertyChanged("YearsList");
            }
        }



        private KeyValuePair<string, int> m_SelectedYear;
        public KeyValuePair<string, int> SelectedYear
        {
            get
            {
                return m_SelectedYear;
            }
            set
            {

                if (value.Key == null || value.Key == m_SelectedYear.Key) return;

                if (m_YearsList.ContainsKey(value.Key) && m_YearsList.First().Value == value.Value)
                {
                    if (
                        !DialogHelper.ShowConfirmation(
                            string.Format(
                                "Загрузка данных за \"{0}\" может занять продолжительное время!\nНачать загрузку?",
                                value.Key)))
                    {

                        return;
                    }

                }

                m_SelectedYear = value;

                this.RefreshList.Execute(null);
            }
        }

        protected void SetOptionYearsList()
        {
            m_YearsList = DateTools.GetYearsDictionaryPlusTwoYears();
            m_SelectedYear = m_YearsList.FirstOrDefault(y => y.Value == DateTime.Now.Year);
        }
        #endregion


        public F50ListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF050);

            if (m_SelectedYear.Key == null)
            {
                SetOptionYearsList();
            }
        }

        protected override List<F050ReportListItem> AfterListLoaded(List<F050ReportListItem> list)
        {
            var lst = base.AfterListLoaded(list);
            lst = lst.Select(n => new F50ReportItemWrapper(n)).Cast<F050ReportListItem>().ToList();
            UpdateDocNumbers(lst);
            
            // счет количества записей по дате
            var g = lst.GroupBy(x => x.ReportDate);
            g.ToList().ForEach(x => x.ToList().ForEach(y => y.EDOCount = x.Count()));

            return lst;
        }

        protected override void BeforeListRefresh()
        {
            base.BeforeListRefresh();
            this.Dict = new Dictionary<string, List<F050ReportListItem>>();
        }

        protected Dictionary<string, List<F050ReportListItem>> Dict;

        protected void UpdateDocNumbers(List<F050ReportListItem> newList)
        {
            //string sFormat = "{0}-{1}";
            //newList.ForEach(i =>
            //{
            //    string key = i.ReportDate.HasValue ? string.Format(sFormat, i.ReportDate.Value.Year.ToString(), i.ReportDate.Value.Date.ToShortDateString()) : string.Format(sFormat, string.Empty, string.Empty);
            //    if (!dict.ContainsKey(key))
            //        dict[key] = new List<F050ReportListItem>();
            //    dict[key].Add(i);
            //});
            //foreach (var l in dict)
            //{
            //    int cnt = l.Value.GroupBy(j => j.ContractNumber).Count();
            //    l.Value.ForEach(i =>
            //    {
            //        i.EDOCount = cnt;
            //    });
            //}
            //this.List = newList;


            //OnPropertyChanged("Current");
        }

        public virtual Document.Types DocumentType => Document.Types.All;


        protected override long GetListCount()
        {
            if (m_SelectedYear.Key == null)
            {
                SetOptionYearsList();
            }
            return BLServiceSystem.Client.GetF050ReportCount((int)this.DocumentType, this.SelectedYear.Value);
        }

        protected override List<F050ReportListItem> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetF050ReportByPage((int)this.DocumentType, (int)index, this.SelectedYear.Value).ToList();
        }


    }
}
