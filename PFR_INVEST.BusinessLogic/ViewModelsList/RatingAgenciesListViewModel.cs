﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]

    public class RatingAgenciesListViewModel :  ViewModelList

{
        private List<RatingAgency> m_RatingAgencies;
        public List<RatingAgency> RatingAgencies
        {
            get
            {
                RaiseDataRefreshing();
                return m_RatingAgencies;
            }
            set
            {
                m_RatingAgencies = value;
                OnPropertyChanged("RatingAgencies");
            }
        }

        public RatingAgenciesListViewModel()
        {
            DataObjectTypeForJournal = typeof(RatingAgency);
        }

        public override bool CanExecuteDelete()
        {
            return CheckDeleteAccess(typeof(RatingAgencyViewModel));
        }

        protected override void ExecuteRefreshList(object param)
        {
           RatingAgencies = BLServiceSystem.Client.GetRatingAgenciesList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
}
}
