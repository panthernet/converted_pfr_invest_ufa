﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class OpfrListViewModel : PagedLoadObservableListModelBase<OpfrRegisterListItem>, ISettingOpenForm, IUpdateListenerModel
    {
        private OpfrRegisterListItem _selectedItem;
        public override Type DataObjectTypeForJournal => typeof(OpfrRegisterListItem);
        public OpenSettingsProvider SettingsProvider { get; set; }

        protected override long GetListCount()
        {
            return BLServiceSystem.Client.GetOpfrRegisterListItemCount(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<OpfrRegisterListItem> GetListPart(long index)
        {
            var x =  BLServiceSystem.Client.GetOpfrRegisterListItemPage((int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
            return x;
        }

        protected override void BeforeListRefresh()
        {
            if (SettingsProvider == null)
                SettingsProvider = new OpenSettingsProvider("OpfrListViewModel", "Бэк-офис - ОПФР - Распоряжения Правления", (s) => ExecuteRefreshList(null));
        }
        

        public Type[] UpdateListenTypes => new[] { typeof(OpfrRegisterListItem), typeof(OpfrRegister), typeof(OpfrTransfer) };

        public void OnDataUpdate(Type type, long id)
        {
            if (type == typeof(OpfrTransfer))
            {
                var item = BLServiceSystem.Client.GetOpfrRegisterListItemByTransferID(id);
                var items = new List<OpfrRegisterListItem>();
                if (item != null) items.Add(item);
                var olds = List.Where(l => l.TransferID == id).ToList();

                if (!olds.Any() && item != null)
                {
                    var e = List.FirstOrDefault(a => a.ID == item.ID);
                    if (e != null && e.TransferID == null)
                        olds.Add(e);
                }

                var sItem = olds.FirstOrDefault();
                if (!items.Any() && olds.Any() && List.Count(a => a.ID == sItem.ID) == 1)
                {
                    items.Add(new OpfrRegisterListItem
                    {
                        ID = sItem.ID,
                        YearID = sItem.YearID,
                        Year = sItem.Year,
                        MonthID = sItem.MonthID,
                        Month = sItem.Month,
                        RegisterName = sItem.RegisterName,
                        TypeName = sItem.TypeName,
                    });
                }
                UpdateRecord(olds, items);
            }
            else
            {
               
                var items = BLServiceSystem.Client.GetOpfrRegisterListItems(id);
                var olds = List.Where(l => l.ID == id).ToList();
                UpdateRecord(olds, items);
               
            }

           
        }

    }
}
