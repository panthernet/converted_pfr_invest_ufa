﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class YieldsListViewModel : ViewModelList
    {
        private IList<McProfitAbility> m_YieldsList;
        public IList<McProfitAbility> YieldsList
        {
            get
            {
                RaiseDataRefreshing();
                return m_YieldsList;
            }
            set
            {
                m_YieldsList = value;
                OnPropertyChanged("YieldsList");
            }
        }

        public YieldsListViewModel()
        {
            DataObjectTypeForJournal = typeof(McProfitAbility);
        }

        protected override void ExecuteRefreshList(object param)
        {
            YieldsList = BLServiceSystem.Client.GetMcProfitAbilityListHib();	
			
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
