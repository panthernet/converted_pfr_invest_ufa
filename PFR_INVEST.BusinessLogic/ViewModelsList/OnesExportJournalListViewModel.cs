﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class OnesExportJournalListViewModel : PagedLoadObservableListModelBase<OnesJournal>, ISettingOpenForm
    {
        public override Type DataObjectTypeForJournal => typeof(OnesJournal);
        public OpenSettingsProvider SettingsProvider { get; set; }
        public bool IsEventsRegisteredOnDataRefreshed;


        protected override long GetListCount()
        {
            /*var cList = new List<ListPropertyCondition>
            {
                ListPropertyCondition.GreaterEqThan("LogDate", SettingsProvider.Filter.PeriodStart),
                ListPropertyCondition.LessEqThan("LogDate", SettingsProvider.Filter.PeriodEnd),
                ListPropertyCondition.In("Action",new object[] { (int)OnesJournal.Actions.ExportComplete, (int)OnesJournal.Actions.Export }),
            };
            return DataContainerFacade.GetListByPropertyConditionsCount<OnesJournal>(cList);*/
            return BLServiceSystem.Client.GetOnesExportJournalListCount(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override List<OnesJournal> GetListPart(long index)
        {
            return BLServiceSystem.Client.GetOnesExportJournal((int)index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
        }

        protected override void BeforeListRefresh(){
            if (!IsEventsRegisteredOnDataRefreshed)
                IsEventsRegisteredOnDataRefreshed = true;

            if (SettingsProvider == null)
            {
                SettingsProvider = new OpenSettingsProvider("OnesExportJournalListViewModel", "Экспорт 1С - Журнал", (s) => this.ExecuteRefreshList(null))
                {
                    PeriodEnd = DateTime.Today,
                    PeriodStart = DateTime.Today//.AddYears(-1).Date
                };
            }
        }
    }
}
