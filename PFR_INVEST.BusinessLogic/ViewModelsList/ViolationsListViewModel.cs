﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class ViolationsListViewModel : ViewModelList
    {
        private IList<EdoOdkF140ListItem> _mList;
        public IList<EdoOdkF140ListItem> List
        {
            get
            {
                return _mList;
            }
            set
            {
                _mList = value;
                OnPropertyChanged("List");
            }
        }

        public ViolationsListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF140);
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetEdoOdkF140ListItems();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
