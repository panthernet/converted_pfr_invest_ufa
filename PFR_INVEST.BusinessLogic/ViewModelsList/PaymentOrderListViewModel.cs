﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class PaymentOrderListViewModel : PagedLoadObservableListModelBase<PaymentOrderListItem>, ISettingOpenForm, IUpdateListenerModel
	{
		public List<Element> DirectionsList { get; private set; }
		public List<Element> DocumentTypeList { get; private set; }
		public IList<BankListItem> BankList { get; private set; }

		public PaymentOrderListViewModel(object param = null)
			: base(param)
		{
			ID = -1;
		    DataObjectTypeForJournal = typeof (PaymentOrder);
			DirectionsList = BLServiceSystem.Client.GetElementByType(Element.Types.PPDirection);
			DocumentTypeList = BLServiceSystem.Client.GetElementByType(Element.Types.PODocumentType);
			BankList = BLServiceSystem.Client.GetCommonBankListItems();
		}


		protected override void BeforeListRefresh()
		{
			if (SettingsProvider == null)
			{
				SettingsProvider = new OpenSettingsProvider("PaymentOrderListViewModel", "Бэк-офис - Временное размещение - Загруженные п/п по депозитам", (s) => ExecuteRefreshList(null)) { IsPeriodVisible = true };
			}
        }

		protected override long GetListCount()
		{
			return BLServiceSystem.Client.GetPaymentOrderCount(SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
		}


		protected override List<PaymentOrderListItem> GetListPart(long index)
		{
			var part = BLServiceSystem.Client.GetPaymentOrderByPage(index, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);

			return part;
		}


		public OpenSettingsProvider SettingsProvider { get; set; }


		public Type[] UpdateListenTypes => new[] { typeof(AsgFinTr) };

	    public void OnDataUpdate(Type type, long id)
		{			
			var item = BLServiceSystem.Client.GetPaymentOrderByID(id, SettingsProvider.Filter.PeriodStart, SettingsProvider.Filter.PeriodEnd);
			var old = List.FirstOrDefault(l => l.ID == id);
			
			UpdateRecord(old, item);
		}
	}
}
