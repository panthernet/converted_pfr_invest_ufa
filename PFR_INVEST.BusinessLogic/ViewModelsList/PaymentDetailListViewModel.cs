﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class PaymentDetailListViewModel : ViewModelListObservable<PaymentDetail>, IUpdateListenerModel
	{
		public List<KBK> KBKList { get; private set; }

		public PaymentDetailListViewModel()
		{
            DataObjectTypeForJournal = typeof(PaymentDetail);
			LoadKBKList();
		}

		protected override void ExecuteRefreshList(object param)
		{
			List.Fill(DataContainerFacade.GetList<PaymentDetail>());
		}

		private void LoadKBKList() 
		{
			KBKList = DataContainerFacade.GetList<KBK>(false);
			OnPropertyChanged("KBKList");
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(PaymentDetail),
		    typeof(KBK)};

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(PaymentDetail))
			{
				var item = DataContainerFacade.GetByID<PaymentDetail>(id);
				var old = List.FirstOrDefault(p => p.ID == id);

				base.UpdateRecord(old, item);

				//foreach (var p in List.Where(p => p.ID == id).ToList())
				//    List.Remove(p);

				//var pd = DataContainerFacade.GetByID<PaymentDetail>(id);

				//if (pd != null && !pd.IsDeleted())
				//    List.Add(pd);
				//Util.ReplaceElementByID(List, id);

			}
			else if (type == typeof(KBK)) 
			{
				LoadKBKList();				
			}
		}
	}
}
