﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class PensionNotificationListViewModel : ViewModelList
    {
        private List<PensionNotification> _list;
        public List<PensionNotification> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private PensionNotification _selectedPensionNotification;

        public PensionNotification SelectedPensionNotification
        {
            get { return _selectedPensionNotification; }
            set
            {
                _selectedPensionNotification = value;
                OnPropertyChanged("SelectedPensionNotification");
            }
        }

        public PensionNotificationListViewModel()
        {
            DataObjectTypeForJournal = typeof(PensionNotification);
        }

        protected override void ExecuteRefreshList(object param)
        {
            List = DataContainerFacade.GetList<PensionNotification>().ToList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

    }
}
