﻿

using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class LoadedODKLogListVRViewModel : LoadedODKLogListViewModel
    {
        public LoadedODKLogListVRViewModel() : base(true) { }

        protected override void ExecuteRefreshList(object param)
        {
            List = BLServiceSystem.Client.GetLoadedSuccessODKLogListByTypeContract(this.DateFrom, this.DateTo, this.SelectedForm == ALL_FORMS ? null : this.SelectedForm, (int)Document.Types.VR);
            if (List != null)
                List.ToList().ForEach(a => { if (string.IsNullOrEmpty(a.Filename)) a.Filename = "xml"; });
            //JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT);
        }
    }
}
