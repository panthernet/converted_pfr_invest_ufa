﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class AnalyzePaymentyearrateListViewModel : ViewModelListObservable<AnalyzePaymentyearrateReport>
    {
        public bool IsArchiveMode { get; set; }
        public int? Year { get; set; }
        public int? Quark { get; set; }
        public virtual string FormTitle => $"Форма 7. Показатели назначения и выплаты пенсионных накоплений";

        protected override void ExecuteRefreshList(object param)
        {
            var data = DataContainerFacade.GetClient().GetPaymentyearrateReportByYearKvartal(Year, Quark, IsArchiveMode);
            data.ForEach(l => l.QuarkDisplay = QuarkDisplayHelper.GetQuarkDisplayValue(l.Kvartal));
            List.Fill(data);
        }
    }
}