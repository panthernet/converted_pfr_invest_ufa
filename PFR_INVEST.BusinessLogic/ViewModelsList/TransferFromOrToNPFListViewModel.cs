﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class TransferFromOrToNPFListViewModel : ViewModelList, IUpdateListenerModel
	{
	    private ObservableCollection<TransferFromOrToNPFListItem> _mTransfersList;
		public ObservableCollection<TransferFromOrToNPFListItem> TransfersList
		{
			get
			{
				RaiseDataRefreshing();
				return _mTransfersList;
			}
			set
			{
				_mTransfersList = value;
				OnPropertyChanged("TransfersList");
			}
		}

		public TransferFromOrToNPFListViewModel(bool pfrtoNPF)
			: base(pfrtoNPF)
		{
            DataObjectTypeForJournal = typeof(Register);
		}

		protected override void ExecuteRefreshList(object param)
		{
			var list = BLServiceSystem.Client.GetRegistersListPPNotEntered((bool)param);
			TransfersList = new ObservableCollection<TransferFromOrToNPFListItem>(list);
		}

		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Finregister) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long objectId)
		{
			if (type == typeof(Finregister))
			{
				var old = TransfersList.Where(t => t.FinregisterID == objectId).ToList();
				var newFr = BLServiceSystem.Client.GetRegistersListPPNotEntered((bool)RefreshParameter, objectId);

				newFr.ForEach(fr => TransfersList.Add(fr));
				old.ForEach(fr => TransfersList.Remove(fr));
			}
		}
	}
}
