﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class F80ListViewModel : ViewModelList
    {
        private List<F080DetailsListItem> m_F80List = new List<F080DetailsListItem>();
        public List<F080DetailsListItem> F80List
        {
            get
            {
                return m_F80List;
            }
            set
            {
                m_F80List = value;
                OnPropertyChanged("F80List");
            }
        }

        public F80ListViewModel()
        {
            DataObjectTypeForJournal = typeof(EdoOdkF080);
        }

        protected override void ExecuteRefreshList(object param)
        {
            CalculateDocNumbers(BLServiceSystem.Client.GetF80ListHib());
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
        protected void CalculateDocNumbers(List<F080DetailsListItem> newList)
        {
            this.F80List=newList;

            const string sFormat = "{0}-{1}";
            var dict = new Dictionary<string, List<F080DetailsListItem>>();

            this.F80List.ForEach(i =>
            {
                string key = i.reportOnDate.HasValue ? string.Format(sFormat, i.reportOnDate.Value.Year.ToString(), i.reportOnDate.Value.Date.ToShortDateString()) : string.Empty;
                if (!dict.ContainsKey(key))
                    dict[key] = new List<F080DetailsListItem>();
                dict[key].Add(i);
            });
            foreach (var l in dict)
            {
                int cnt = l.Value.GroupBy(i => i.ContractNumber).Count();
                l.Value.ForEach(i =>
                {
                    i.lContractNumbers = cnt;
                });
            }
            OnPropertyChanged("F80List");
        }
    }
}
