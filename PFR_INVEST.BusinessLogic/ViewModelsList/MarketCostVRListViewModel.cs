﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class MarketCostVRListViewModel : MarketCostListViewModel
    {
		protected override Document.Types DocType => Document.Types.VR;

        //protected override long GetListCount()
		//{
		//    F020ListCount = BLServiceSystem.Client.GetCountMarketCostF020((int)Document.Types.VR);
		//    F025ListCount = BLServiceSystem.Client.GetCountMarketCostF025((int)Document.Types.VR);
		//    return F020ListCount + F025ListCount;

		//}

		//protected override List<MarketCostListItem> GetListPart(long index)
		//{
		//    return index < F020ListCount ?
		//        BLServiceSystem.Client.GetMarketCostF020ListByPage((int)Document.Types.VR, (int)index).ToList() :
		//        BLServiceSystem.Client.GetMarketCostF025ListByPage((int)Document.Types.VR, (int)(index - F020ListCount)).ToList();

		//}



    }
}
