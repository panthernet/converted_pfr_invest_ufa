﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
	public abstract class CorrespondenceBaseListViewModel : ViewModelListObservable<CorrespondenceListItemNew>, IUpdateListenerModel
	{
		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		private object Param { get; set; }
				
		protected abstract List<CorrespondenceListItemNew> GetDocuments(Document.Statuses status, long? documentID = null, long? attachID = null);

		protected override void ExecuteRefreshList(object param)
		{
			Param = param;

			var t1 = DateTime.Now;
			var tmpList = GetDocuments((param as Document.Statuses?) ?? Document.Statuses.All);
			Debug.WriteLine(t1 - DateTime.Now);
			t1 = DateTime.Now;
			Debug.WriteLine(t1 - DateTime.Now);
			t1 = DateTime.Now;
			RaiseDataRefreshing();
			Debug.WriteLine(t1 - DateTime.Now);
			List.Fill(tmpList);
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new Type[] { typeof(Document) ,typeof(Attach)};

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(Document))
			{
				var oldItems = List.Where(d => (d.ID == id && !d.IsAttach) ).ToList();
				//Обновляем только документ, атачи пропускаем
				var newItems = GetDocuments((Param as Document.Statuses?) ?? Document.Statuses.All, id, null).Where(s=>!s.IsAttach).ToList() ;
				base.UpdateRecords(oldItems, newItems);
			}
			if (type == typeof(Attach))
			{
				var oldItems = List.Where(d => d.ID == id && d.IsAttach).ToList();
				//Обновляем атачи , документ пропускаем
				var newItems = GetDocuments((Param as Document.Statuses?) ?? Document.Statuses.All, null, id).Where(s => s.IsAttach).ToList(); 
				base.UpdateRecords(oldItems, newItems);
			}
		}
	}
}