﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class CorrespondenceSIArchiveListViewModel : CorrespondenceSIBaseListViewModel
    {
        protected override void ExecuteRefreshList(object param)
        {
            base.ExecuteRefreshList(Document.Statuses.Executed);
        }
    }
}
