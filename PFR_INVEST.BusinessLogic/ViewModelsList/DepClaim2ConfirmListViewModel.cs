﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class DepClaim2ConfirmListViewModel : ViewModelList<DepClaim2>,
																			IDepClaimOfferCreateProvider,
																			IDepClaim2ConfirmExportProvider
	{

		public DepClaim2ConfirmListViewModel()
		{
			DataObjectTypeForJournal = typeof(DepClaim2);
		}

		public override bool CanExecuteDelete()
		{
			return CheckDeleteAccess(typeof(DepClaim2ConfirmListViewModel));
		}

		protected override void ExecuteRefreshList(object param)
		{
			var list = DataContainerFacade.GetList<DepClaim2>().Where(c =>
											c.Status == DepClaim2.Statuses.Confirm
										  || c.Status == DepClaim2.Statuses.ConfirmOfferCreated
										  || c.Status == DepClaim2.Statuses.ConfirmNotAccepted).ToList();

			foreach (var x in list)
			{
				if (x.Term != 0 && x.Part2Sum != 0) continue;
				x.Part2Sum = x.Amount + x.Payment;
				x.Term = (x.ReturnDate - x.SettleDate).Days;
			}

			LoadAuctionDate(list);
			//LoadRepositoryLinks(list);

			List = list;
		}

		private void LoadAuctionDate(List<DepClaim2> xL)
		{
			var pL = new List<ListPropertyCondition> { new ListPropertyCondition() { Name = "ID", Values = (from x in xL select x.AuctionID).Cast<object>().ToArray(), Operation = "in" } };
			var auctionList = DataContainerFacade.GetListByPropertyConditions<DepClaimSelectParams>(pL).ToList();

			foreach (var x in xL)
			{
				var a = auctionList.SingleOrDefault(p => p.ID == x.AuctionID);
				if (a != null)
				{
					x.AuctionDate = a.SelectDate;
					x.StockName = a.StockName;
					x.StockID = a.StockId;
				}
			}
		}

		//private void LoadRepositoryLinks(List<DepClaim2> xL)
		//{
		//    var rL = BLServiceSystem.Client.GetLastRepositoryLinks().Where(x => x.RepositoryKey == 4);//выписка из реестра, подлеж. удовл.
		//    foreach (var x in xL)
		//    {
		//        var r = rL.FirstOrDefault(y => y.AuctionID == x.AuctionID);
		//        if (r != null) x.RepositoryID = r.RepositoryID;
		//    }
		//}

		protected override bool CanExecuteRefreshList()
		{
			return true;
		}

		private List<DepClaimOfferListItem> _activeAuctionOfferts;
		private List<DepClaimOfferListItem> ActiveAuctionOfferts => _activeAuctionOfferts ?? new List<DepClaimOfferListItem>();

	    private DepClaim2 _activeDepClaim;
		public DepClaim2 ActiveDepClaim
		{
			get { return _activeDepClaim; }
			set
			{
				_activeDepClaim = value;
				_activeAuctionOfferts = AuctionID.HasValue ? BLServiceSystem.Client.GetDepClaimOfferListByAuctionId(AuctionID.Value) : new List<DepClaimOfferListItem>();
			}
		}

		public long? SelectedAuctionID { get; set; }

		public DepClaimSelectParams.Statuses? SelectedAuctionStatus { get; set; }

		public long? AuctionID => ActiveDepClaim != null ? ActiveDepClaim.AuctionID : SelectedAuctionID;

	    DepClaimSelectParams.Statuses? IDepClaimAuctionInfoProvider.AuctionStatus => SelectedAuctionStatus;

	    public bool CanGenerateOfferts
		{
			get
			{
				if (SelectedAuctionStatus == DepClaimSelectParams.Statuses.DepClaimConfirmImported)
					return true;

				if (SelectedAuctionStatus == DepClaimSelectParams.Statuses.OfferCreated && ActiveAuctionOfferts.All(item => item.Offer.Status == DepClaimOffer.Statuses.NotSigned))
					return true;

				return false;
			}
		}

	}
}
