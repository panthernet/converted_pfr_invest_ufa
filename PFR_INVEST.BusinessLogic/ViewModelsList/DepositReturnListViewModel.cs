﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	public sealed class DepositReturnListViewModel : ViewModelList, IUpdateRaisingModel, IRequestCustomAction
	{
		private ObservableCollection<DepositReturnListItem> _list;

		public ObservableCollection<DepositReturnListItem> List
		{
			get { return _list; }
			set
			{
				_list = value;
				OnPropertyChanged("List");
			}
		}

		private readonly IList<DepositReturnListItem> _allItems;

		public ICommand SetPercentCommand { get; private set; }

		public DepositReturnListViewModel(IEnumerable<DepositReturnListItem> items)
		{
			SetPercentCommand = new DelegateCommand<long?>(SetPercent);

			DataObjectTypeForJournal = typeof(Deposit);
			//Исключаем бессмысленные записи с 0 суммой
			_allItems = items.Where(d => d.Amount != 0).ToList();

			foreach (var x in _allItems)
			{
				x.IsSelected = (x.IsPercent == false) || (x.IsPercent && x.Amount > 0);
				x.GroupItems = null;
			}

			List = new ObservableCollection<DepositReturnListItem>(_allItems.Where(p => p.IsSelected));


			var gL = from x in List group x by new { x.DepositID, x.IsPercent } into p select new { Items = p.ToList() };

			foreach (var g in gL)
			{
				if (g.Items.Count <= 1)// Группа нужна если больше 1 элемента
					continue;

				foreach (var x in g.Items)
					x.GroupItems = g.Items;
			}

			OkCommand = new DelegateCommand(o => GetCanOkCommand(), o => ExecuteOkCommand(null));
		}

		public DelegateCommand OkCommand { get; private set; }

		public bool GetCanOkCommand()
		{
			return CanOkCommand;
		}

		public bool CanOkCommand
		{
			get
			{
				return List != null && List.Any(p => p.IsSelected);
			}
		}


	    public static void ExecuteAction(IList<DepositReturnListItem> allItems)
	    {
            var xL = allItems.ToList();
            if (BLServiceSystem.Client.DepositReturn(xL))//Нам нужны пары записей: для суммы и для процентов, так что шлем все записи
            {
                ViewModelManager.RefreshViewModels(
                    typeof(DepositsListViewModel),
                    typeof(DepositsListByPortfolioViewModel),
                    typeof(DepClaimSelectParamsViewModel),
                    typeof(DepositsArchiveListViewModel),
                    typeof(TempAllocationListViewModel),
                    typeof(IncomeSecurityListViewModel)//, typeof(BalanceListViewModel)
                    );
                var idList = (from x in xL select x.DepositID).ToList().Distinct();
                foreach (var id in idList)
                    ViewModelManager.RefreshCardViewModel(typeof(DepositViewModel), id);
            }
        }

		private void ExecuteOkCommand(object o)
		{
			if (!CanOkCommand)
				return;
            ExecuteAction(_allItems);
            ViewModelManager.UpdateDataInAllModels(GetUpdatedList());
            DialogHelper.ShowAlert("Депозиты возвращены успешно!");
        }


		public DepositReturnListItem SelectedItem { get; set; }

		protected override void ExecuteRefreshList(object param)
		{
		}

		protected override bool CanExecuteRefreshList()
		{
			return false;
		}

		private void SetPercent(long? offerNumber)
		{
			var list = List.Where(p => p.OfferNumber == offerNumber && p.IsPercent).ToList();
			if (list.Count == 0) return;

			DialogHelper.ShowEditDepositSetPercent(list);
            list.ForEach(d => { d.AmountActual = d.Amount; d.OnPropertyChanged("AmountActual"); });
            OnRequestCustomAction();
		}

		public void EditSum(DepositReturnListItem x)
		{
			EditSum(x.OfferNumber, x.IsPercent);
		}

		public void EditSum(long offerNumber, bool isPercent)
		{
			//Get list of records by depositID, percent/notPercent
			var xL = List.Where(p => p.OfferNumber == offerNumber && p.IsPercent == isPercent).ToList();

			if (xL.Count == 0)//Item not found, Just for any case
				return;


			var paymentDate = xL[0].PaymentDate;
			decimal amount = 0;
			decimal amountMax = 0;
			foreach (var x in xL)
			{
				amount += x.AmountActual;
				amountMax += x.Amount;
			}

			if (DialogHelper.ShowEditDepositReturnSum(ref paymentDate, ref amount, amountMax))
			{
				//Set values in items
				DistributeSum(paymentDate, amount, amountMax, xL);
			    OnRequestCustomAction();
			}
		}

		private static void DistributeSum(DateTime paymentDate, decimal amount, decimal amountMax, List<DepositReturnListItem> xL)
		{
			if (amount > amountMax)
				throw new ArgumentOutOfRangeException("amount",@"Сумма платежа первышает необходимую сумму");

			if (amountMax == 0 || amount == 0)
			{
				foreach (var x in xL)
				{
					x.SetPayment(paymentDate, 0);
				}
				return;
			}

			var sum = amount;
			decimal changeSum = 0;

			foreach (var x in xL)
			{
				var n = Math.Round((amount / amountMax) * x.Amount, 2);
				changeSum += x.SetPayment(paymentDate, n);
				sum -= n;
			}
			sum += changeSum;

			// Distributed too much or not distributed some money, set payment to first possible account
			if (sum != 0)
			{
				foreach (var x in xL)
				{
					var n = sum + x.AmountActual;//<0
					sum = x.SetPayment(paymentDate, n);
				}

				//if (sum != 0) //Something is wrong in calc. Just for any case
				//	throw new InvalidOperationException("Ошибка распределения суммы по портфелям.");
			}
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>>();

			//list.Add(new KeyValuePair<Type, long>(typeof(DepositReturnListItem), ID));

			_allItems.ToList().ForEach(dop =>
			{
				list.Add(new KeyValuePair<Type, long>(typeof(Deposit), dop.DepositID));
			});

			return list;
		}

	    public event EventHandler RequestCustomAction;

	    private void OnRequestCustomAction()
	    {
	        RequestCustomAction?.Invoke(null, null);
	    }
	}
}
