﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator)]
	public sealed class DKListViewModel : ViewModelList<KDoc>
	{
		public KDoc.Types Type { get; protected set; }

		public DKListViewModel(KDoc.Types type)
			: base(type)
		{
            DataObjectTypeForJournal = typeof(KDoc);
			Type = type;
		}

		protected override void ExecuteRefreshList(object param)
		{
			var type = (KDoc.Types)param;
		    List = BLServiceSystem.Client.GetKDocsForPeriod((long) type); //DataContainerFacade.GetList<KDoc>().Where(d => d.ReportPeriodID == (int)type).ToList();
		}
	}
}
