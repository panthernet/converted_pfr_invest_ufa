﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsList
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepClaim2ListViewModel : ViewModelList<DepClaim2>, 
                                                IDepClaim2ConfirmImportProvider,
                                                IDepClaim2ExportProvider,
												IDepClaimStockProvider
    {

        public DepClaim2ListViewModel()
        {
            DataObjectTypeForJournal = typeof(DepClaim2);
        }

        public override bool CanExecuteDelete()
        {
            return CheckDeleteAccess(typeof(DepClaim2ListViewModel));
        }

        protected override void ExecuteRefreshList(object param)
        {
            var xL = DataContainerFacade.GetList<DepClaim2>();
            LoadAuctionDate(xL);
			//LoadRepositoryLinks(xL);
            List = xL;
        }

        private void LoadAuctionDate(List<DepClaim2> xL)
        {
            var pL = new List<ListPropertyCondition> {new ListPropertyCondition() {Name = "ID", Values = (from x in xL select x.AuctionID).Cast<object>().ToArray(), Operation = "in"}};
            var auctionList = DataContainerFacade.GetListByPropertyConditions<DepClaimSelectParams>(pL).ToList();

            foreach (var x in xL)
            {
                var a = auctionList.SingleOrDefault(p => p.ID == x.AuctionID);
				if (a != null)
				{
					x.AuctionDate = a.SelectDate;
					x.StockName = a.StockName;
					x.StockID = a.StockId;
				}
            }
        }

		//private void LoadRepositoryLinks(List<DepClaim2> xL)
		//{
		//    var rL = BLServiceSystem.Client.GetLastRepositoryLinks().Where(x=>x.RepositoryKey == 3);//выписка из реестра
		//    foreach (var x in xL)
		//    {
		//        var r = rL.FirstOrDefault(y => y.AuctionID == x.AuctionID);
		//        if (r != null) x.RepositoryID = r.RepositoryID;
		//    }
		//}

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }


        public DepClaim2 ActiveDepClaim { get; set; }

        public long? SelectedAuctionID { get; set; }

        public DepClaim2 SelectedAuctionAnyClaim { get; set; }

        public long? AuctionID => ActiveDepClaim != null ? ActiveDepClaim.AuctionID : SelectedAuctionID;


        int? IDepClaimAuctionFullInfoProvider.DepClaimMaxCount => SelectedAuctionAnyClaim == null ? (int?)null : SelectedAuctionAnyClaim.DepClaimMaxCount;

        int? IDepClaimAuctionFullInfoProvider.DepClaimCommonCount => SelectedAuctionAnyClaim == null ? (int?)null : SelectedAuctionAnyClaim.DepClaimCommonCount;

        DepClaimSelectParams.Statuses? IDepClaimAuctionInfoProvider.AuctionStatus => SelectedAuctionAnyClaim == null ? (DepClaimSelectParams.Statuses?)null : SelectedAuctionAnyClaim.AuctionStatus;

        bool IDepClaimStockProvider.IsMoscowStockSelected => (ActiveDepClaim != null && ActiveDepClaim.StockID == (long)Stock.StockID.MoscowStock);

        bool IDepClaimStockProvider.IsSPVBStockSelected => (ActiveDepClaim != null && ActiveDepClaim.StockID == (long)Stock.StockID.SPVB);

        long? IDepClaimAuctionProvider.AuctionID => AuctionID;
    }
}
