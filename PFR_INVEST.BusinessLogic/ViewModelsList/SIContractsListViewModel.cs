﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIContractsListViewModel : ContractsListViewModel
    {

        public SIContractsListViewModel()
        {
            DataObjectTypeForJournal = typeof(Contract);
        }

        protected override List<SIContractListItem> GetContractItems()
        {
            return BLServiceSystem.Client.GetSIContractsListHib();
        }
    }
}
