﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public sealed class DepositsListByPortfolioViewModel : ViewModelList
    {
        private List<DepositListItem> _mDepositsList = new List<DepositListItem>();
        public List<DepositListItem> DepositsList
        {
            get
            {
                RaiseDataRefreshing();
                return _mDepositsList;
            }
            set
            {
                _mDepositsList = value;
                OnPropertyChanged("DepositsList");
            }
        }

        public DepositsListByPortfolioViewModel()
        {
            DataObjectTypeForJournal = typeof(Deposit);
        }

        protected override void ExecuteRefreshList(object param)
        {
            DepositsList = BLServiceSystem.Client.GetDepositsListByPortfolioHib();
            //var x = DepositsList.Where(a => a.IsExtra && a.OperationDate != null && a.OperationDate.Value.Day == 5 && a.OperationDate.Value.Month == 12).ToList();
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

    }
}
