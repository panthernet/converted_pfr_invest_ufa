﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public sealed class MonthAccurateViewModel : ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel, IRequestCloseViewModel
    {
        public DopSPN DopSPN { get; private set; }
        public Month Month { get; private set; }
        public AddSPN AddSPN { get; private set; }
        public Portfolio.Types PType { get; private set; }
	    public string DocKind { get; private set; }

	    private decimal _mLastSum;
		//public decimal MSumm
		//{
		//	get
		//	{
		//		return DopSPN.MSumm ?? 0;
		//	}
		//	set
		//	{
		//		DopSPN.MSumm = value;
		//		RefreshChSum();
		//		OnPropertyChanged("MSumm");
		//	}
		//}

        public decimal ChSumm
        {
            get
            {
                return DopSPN.ChSumm ?? 0;
            }

            set
            {
                if (DopSPN.ChSumm.GetValueOrDefault() != value)
                {
                    DopSPN.ChSumm = value;
                    OnPropertyChanged("ChSumm");
                }
            }
        }

        public DateTime? OperationDate
        {
            get
            {
                return DopSPN == null ? null : DopSPN.OperationDate;
            }
            set
            {
                DopSPN.OperationDate = value;
                OnPropertyChanged("OperationDate");
            }
        }

        public DateTime? DocDate
        {
            get
            {
                return DopSPN.Date;
            }
            set
            {
                DopSPN.Date = value;
                RecalcAddSPNSumm();
                RefreshChSum();
                OnPropertyChanged("DocDate");
            }
        }

        public string RegNum
        {
            get
            {
                return DopSPN.RegNum;
            }
            set{
                DopSPN.RegNum = value;
                OnPropertyChanged("RegNum");
            }
        }

        /// <summary>
        /// Отображаемое поле месяца для записи Период
        /// </summary>
        public string MonthName => Month.Name;

        /// <summary>
        /// Отображаемое поле года для записи Период
        /// </summary>
        public long? MonthYear => DopSPN.YearID + 2000;

        public string Comment
        {
            get
            {
                return DopSPN.Comment;
            }
            set
            {
                DopSPN.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

	    /// <summary>
	    /// Это режим ДСВ?
	    /// </summary>
	    public bool IsDSV => DocKind == DueDocKindIdentifier.DueDSVAccurate;

        //// TODO Нужно разобраться, как определять СВ или ДСВ
		//	// Топорно конечно, но пока ничего другого не удалось придумать
		//	get { return (LogCaption + "").ToLower().Contains("дсв"); }
		//}

		public string MSummLabel => IsDSV ? "Месячные отклонения, итого (руб.):" : "Месячные отклонения (руб.):";

        /// <summary>
		/// Режим чтения для поля "Отклонения, итого"
		/// </summary>
		public bool IsMSummTotalReadOnly => IsDSV || IsMSummReadOnly;

        public GridLength MSummPartHeight => IsDSV ? new GridLength(38) : new GridLength(0);

        public bool IsMSummReadOnly => ReadAccessOnly;

        public string DocDateError { get; private set; }
        public DateTime? MaxDate { get; private set; }
        public DateTime? MinDate { get; private set; }

        private PortfolioFullListItem _mAccount;
        public PortfolioFullListItem Account
        {
            get
            {
                return _mAccount;
            }
            set
            {
                _mAccount = value;
                if (value != null)
                {
                    DopSPN.PortfolioID = value.Portfolio.ID;
                    DopSPN.LegalEntityID = value.BankLE.ID;
                    OnPropertyChanged("AccountNumber");
                    OnPropertyChanged("Bank");
                    OnPropertyChanged("PFName");
                    OnPropertyChanged("Account");
                }
            }
        }

		private readonly bool _isReadOnly;
		public override bool IsReadOnly => _isReadOnly;

        public decimal MSumm
		{
			get
			{
				if (IsDSV)
				{
					DopSPN.MSumm = MSummNakop + MSummEmployer + MSummOther + MSummPerc;
					// http://jira.dob.datateh.ru/browse/DOKIPIV-1097?focusedCommentId=282320&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-282320
					if (_mLastSum != 0)
					{
						ChSumm = (DopSPN.MSumm ?? 0) - _mLastSum;
					}
				}
				
				return DopSPN.MSumm ?? 0;
			}
			set
			{	
				DopSPN.MSumm = value;
				RefreshChSum();
				OnPropertyChanged("MSumm");
			}
		}


		public decimal MSummNakop
		{
			get { return DopSPN.MSummNakop ?? 0; }
			set
			{
				DopSPN.MSummNakop = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummNakop");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummOther
		{
			get { return DopSPN.MSummOther ?? 0; }
			set
			{
				DopSPN.MSummOther = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummOther");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummPerc
		{
			get { return DopSPN.MSummPerc ?? 0; }
			set
			{
				DopSPN.MSummPerc = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummPerc");
					OnPropertyChanged("MSumm");
				}
			}
		}

		public decimal MSummEmployer
		{
			get { return DopSPN.MSummEmployer ?? 0; }
			set
			{
				DopSPN.MSummEmployer = value;

				if (IsDSV)
				{
					OnPropertyChanged("MSummEmployer");
					OnPropertyChanged("MSumm");
				}
			}
		}

        public bool ReadAccessOnly => IsReadOnly || State != ViewModelState.Create && (IsBlockedByQuarterDop || State == ViewModelState.Read ||
                                                                                       !(AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.OFPR_manager) ||
                                                                                         AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator)));

        /// <summary>
        /// Уточнение на месяц заблокировано квартальным уточнением
        /// В т.ч. доступность полей и удаление
        /// </summary>
        public bool IsBlockedByQuarterDop { get; private set; }

        public override bool CanExecuteDelete()
        {
            return !IsReadOnly && State == ViewModelState.Edit && !IsBlockedByQuarterDop;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(DopSPN);
            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    ID = DopSPN.ID = DataContainerFacade.Save(DopSPN);
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return !IsReadOnly && Validate() && IsDataChanged;
        }

        #region Validation
        private const string ERROR_DOCDATE = "Введите дату уточнения";
        private const string ERROR_OPDATE = "Введите дату проведения операции ДИ";
        private const string ERROR_REGNUM = "Введите регистрационный номер";
        //private const string ERROR_SUMM = "Сумма должна быть больше нуля";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "OperationDate": return OperationDate == null ? ERROR_OPDATE : null;
                    case "DocDate": return DocDate == null ? ERROR_DOCDATE : null;
                    case "RegNum": return string.IsNullOrWhiteSpace(RegNum) ? ERROR_REGNUM : RegNum.ValidateMaxLength(32);
                    case "MSumm": return MSumm.ValidateMaxFormat();
                    case "ChSumm": return ChSumm.ValidateMaxFormat();
                    case "Comment": return Comment.ValidateMaxLength(512);
                    default: return null;
                }
            }
        }

        private bool CheckDocDate()
        {
            if (!DocDate.HasValue) return false;
            if (!MinDate.HasValue) return true;
            if (MaxDate.HasValue)
            {
                //Минимальной обе даты есть - проверяем диапазон
                return MinDate.Value.Date <= DocDate.Value.Date && DocDate.Value.Date <= MaxDate.Value.Date;
            }
            //только минимальная дата - проверяем ее
            return MinDate.Value.Date <= DocDate.Value.Date;
            //Минимальной даты нет - нечего проверять
        }

        private bool Validate()
        {
            return string.IsNullOrEmpty(this["OperationDate"]) &&
                   string.IsNullOrEmpty(this["RegNum"]) &&
                   string.IsNullOrEmpty(this["MSumm"]) &&
                   string.IsNullOrEmpty(this["ChSumm"]) &&
                   string.IsNullOrEmpty(this["Comment"]) &&
                   CheckDocDate();
        }
        #endregion

        private void RefreshChSum()
        {
            if (_mLastSum != 0)
            {
                ChSumm = MSumm - _mLastSum;
            }
        }

        private const string M_DOC_DATE_ERROR_FORMAT_GREATER = "Значение должно быть не меньше {0}";
        private const string M_DOC_DATE_ERROR_FORMAT_RANGE = "Значение должно быть в диапазоне от {0} до {1}";
        private void ReInitMaxMinDatesAndLastSum()
        {
            var lst = BLServiceSystem.Client.GetDopSPNListForDue(AddSPN.ID);
            int cnt = lst.Count;

            if (State == ViewModelState.Create)
            {
                //Дата уточнения - либо дата создания ADDSPN либо дата последнего уточнения
                DocDate = cnt > 0 ? lst.Last().Date : AddSPN.NewDate;

                //Минимальная дата при создании нового уточнения - такая же, максимальной нет
                MinDate = DocDate;
                DocDateError = string.Format(M_DOC_DATE_ERROR_FORMAT_GREATER, MinDate.Value.ToString("dd.MM.yyyy"));
            }
            else
            {
                //тут cnt больше нуля, потому что мы открыли уже сохраненную карточку уточнения
                if (cnt > 1)
                {
                    int index = lst.IndexOf(lst.First(dop => dop.ID == ID));
                    if (index == 0)
                    {
                        //первое уточнение из нескольких - минимальная - дата создания ADDSPN
                        MinDate = AddSPN.NewDate;
                        //...максимальная - дата второго уточнения
                        MaxDate = lst[1].Date;
                        DocDateError = string.Format(M_DOC_DATE_ERROR_FORMAT_RANGE, MinDate.Value.ToString("dd.MM.yyyy"), MaxDate.Value.ToString("dd.MM.yyyy"));
                        //сумма самого СВ/ДСВ
                        //m_LastSum = AddSPN.Summ ?? 0;
                    }
                    else if (index == cnt - 1)
                    {
                        //последнее уточнение из нескольких - минимальная дата - предпоследняя, максимальной нет
                        MinDate = lst[cnt - 2].Date;
                        DocDateError = string.Format(M_DOC_DATE_ERROR_FORMAT_GREATER, MinDate.Value.ToString("dd.MM.yyyy"));
                        //сумма предпоследнего уточнения
                        //m_LastSum = lst[cnt - 2].MSumm;
                    }
                    else
                    {
                        //если уточнение посередине - выбираем границы по рядом стоящим утонениям
                        MinDate = lst[index - 1].Date;
                        MaxDate = lst[index + 1].Date;
                        DocDateError = string.Format(M_DOC_DATE_ERROR_FORMAT_RANGE, MinDate.Value.ToString("dd.MM.yyyy"), MaxDate.Value.ToString("dd.MM.yyyy"));
                        //сумма предыдущего уточнения
                        //m_LastSum = lst[index - 1].MSumm;
                    }
                }
                else
                {
                    //если единственное уточнение для СВ/ДСВ - минимальная дата - дата создания ADDSPN, максимальной нет
                    MinDate = AddSPN.NewDate;
                    DocDateError = string.Format(M_DOC_DATE_ERROR_FORMAT_GREATER, MinDate.Value.ToString("dd.MM.yyyy"));
                    //сумма самого СВ/ДСВ
                    //m_LastSum = AddSPN.Summ ?? 0;
                }
            }
            RecalcAddSPNSumm();
            //RefreshChSum();
        }

        private void RecalcAddSPNSumm()
        {
            // http://jira.vs.it.ru/browse/DOKIPIV-84

            if (AddSPN.MonthID.HasValue && AddSPN.YearID.HasValue)
            {
                var dt = new DateTime((int)AddSPN.YearID.Value + 2000, (int)AddSPN.MonthID.Value, 1);
				
                var type = PType == Portfolio.Types.DSV ? PortfolioTypeFilter.DSV : PortfolioTypeFilter.NotDSV;
                _mLastSum = BLServiceSystem.Client.GetAddSPNSumByPeriod(type, dt);
            }
        }

        private void RefreshDueViewModel()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(DueViewModel)) as DueViewModel;
            if (vm != null)
                vm.RefreshDopSPNList();
        }

        /// <summary>
        /// Вычисляем квартал по месяцу и храним в привате
        /// </summary>
        private readonly int _quarter;

        public MonthAccurateViewModel(ViewModelState action, long id, string docKind = "")
            //: base(typeof(DueListViewModel))
        {
            DataObjectTypeForJournal = typeof(DopSPN);
            State = action;
	        DocKind = docKind;

            if (State == ViewModelState.Create)
            {
                ID = id;
                AddSPN = DataContainerFacade.GetByID<AddSPN>(id);
                var p = DataContainerFacade.GetByID<Portfolio>(AddSPN.PortfolioID);
                if (p != null) { PType = p.TypeEl; }
                DopSPN = new DopSPN
                {
                    AddSpnID = id,
                    PeriodID = AddSPN.MonthID,
                    OperationDate = DateTime.Today,
                    DocKind = 1,
                    YearID = AddSPN.YearID //для нового уточнения поставим год, чтобы отображался в поле
                };
            }
            else
            {
                ID = id;
                DopSPN = DataContainerFacade.GetByID<DopSPN>(ID);
                AddSPN = DataContainerFacade.GetByID<AddSPN>(DopSPN.AddSpnID);
                var p = DataContainerFacade.GetByID<Portfolio>(AddSPN.PortfolioID);
				if (p != null) { PType = p.TypeEl; }
            }

            _quarter = DueHelper.GetAddSPNQuarter(AddSPN.MonthID);
            UpdateIsBlockedByQuarterDop();
            Month = DopSPN.GetMonth();
            //Счёт уточнения, если есть
            if (DopSPN.PortfolioID.HasValue && DopSPN.QuarterPfrBankAccountID.HasValue)
                Account = BLServiceSystem.Client.GetPortfolioFull(DopSPN.QuarterPfrBankAccountID.Value, DopSPN.PortfolioID.Value);
            //Если нет, то берём счёт основного поступления
            else if (AddSPN.PortfolioID.HasValue && AddSPN.LegalEntityID.HasValue && AddSPN.PFRBankAccountID.HasValue)
                Account = BLServiceSystem.Client.GetPortfolioFullForBank(AddSPN.PortfolioID.Value, AddSPN.LegalEntityID.Value, AddSPN.PFRBankAccountID.Value);

            ReInitMaxMinDatesAndLastSum();
            RefreshConnectedCardsViewModels = RefreshDueViewModel;
			_isReadOnly = UpdateReadOnly();
        }

		private bool UpdateReadOnly()
		{
		    //Создание - можно редактировать
			//if (this.DopSPN.ID == 0)
			//	return false;


			//Есть ДК - значит редактировать уже нельзя
			var hasKDoc = DueViewModel.IsClosedByKDoc(AddSPN.ID);
		    return hasKDoc;
		}

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (DopSPN), DopSPN.ID)};
            return list;
        }
        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose != null)
            {
                RequestClose(this, EventArgs.Empty);
            }
        }/// <summary>
        /// Проверяем не блокируются ли поля из-за наличия уочнения за квартал совпадающее по портфелю и временному диапазону
        /// </summary>
        private void UpdateIsBlockedByQuarterDop()
        {
            var isDataChanged = IsDataChanged;
            IsBlockedByQuarterDop = DueHelper.GetIsBlockedByQuarterDop(DopSPN, _quarter);
            OnPropertyChanged("IsBlockedByQuarterDop");
            OnPropertyChanged("ReadAccessOnly");
            if (!isDataChanged) 
                IsDataChanged = false;
        }

        Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(DopSPN), typeof(AddSPN) };

        void IUpdateListenerModel.OnDataUpdate(Type type, long id)
        {
            if (type == typeof(DopSPN))
            {
                if (DopSPN != null && id == DopSPN.ID)
                {
                    var dspn = DataContainerFacade.GetByID<DopSPN>(id);
                    DopSPN = dspn;


                    if (dspn == null)
                    {                        
                        CloseWindow();
                    }else
                        UpdateIsBlockedByQuarterDop();
                }
            }

            // обработка закрытия окна, если сущность не сохранена в БД и удаляется родительская сущность
            if (type == typeof(AddSPN))
            {
                if (State == ViewModelState.Create || id == ID)
                {
                    CloseWindow();
                }
            }
        }
    }
}
