﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
	public class UKPaymentPlanViewModel : ViewModelCard, IRequestCloseViewModel
	{
		public delegate void NeedReloadTransferDataDelegate();
		public event NeedReloadTransferDataDelegate OnNeedReloadTransferDataDelegate;
		private void NeedReloadTransferData()
		{
		    OnNeedReloadTransferDataDelegate?.Invoke();
		}

		public SITransfer Transfer { get; set; }
		public int ConractTypeId;
		public bool IsReadOnlyField => ID > 0 || EditAccessDenied;

	    public bool IsEnabledField => ID <= 0;

	    #region Constants
		public string Register => Transfer?.GetTransferRegister()?.RegisterNumber;

	    public string Company => Transfer?.GetTransferRegister()?.GetCompanyYear().Name;

	    #endregion

		#region УК
		private List<LegalEntity> _mUKList = new List<LegalEntity>();
		public List<LegalEntity> UKList
		{
			get
			{
				return _mUKList;
			}

			set
			{
				_mUKList = value;
				OnPropertyChanged("UKList");
			}
		}

		private LegalEntity _selectedUK;
		public LegalEntity SelectedUK
		{
			get { return _selectedUK; }
			set
			{
				_selectedUK = value;
				UpdateContractsSelection();
			}
		}

		#endregion

		#region Договора
		private List<Contract> _mContractsList;
		public List<Contract> ContractsList
		{
			get
			{
				return _mContractsList;
			}
			set
			{
				_mContractsList = value;
				if (value != null)
					SelectedContract = ContractsList.FirstOrDefault();
				OnPropertyChanged("ContractsList");
			}
		}

		private Contract _mSelectedContract;
		public Contract SelectedContract
		{
			get
			{
				return _mSelectedContract;
			}
			set
			{
				_mSelectedContract = value;
				if (_mSelectedContract != null)
					Transfer.ContractID = value.ID;
				OnPropertyChanged("SelectedContract");
			}
		}

		public DateTime? ContractDate => SelectedContract?.ContractDate;

	    #endregion

		#region Вычисляемые поля
		public decimal PlanSum
		{
			get
			{
				return Transfer.PlannedTransferSum.GetValueOrDefault();
			}
			set
			{
				Transfer.PlannedTransferSum = value;
				ReDivideSums();
				OnPropertyChanged("PlanSum");
			}
		}

		public long ZLCount
		{
			get
			{
				return Transfer.InsuredPersonsCount.GetValueOrDefault();
			}
			set
			{
				Transfer.InsuredPersonsCount = value;
				OnPropertyChanged("ZLCount");
			}
		}

		private decimal _mInvestIncome;
		public decimal InvestIncome
		{
			get
			{
				return _mInvestIncome;
			}
			set
			{
				_mInvestIncome = value;
				OnPropertyChanged("InvestIncome");
			}
		}

		private decimal _mFactSum;
		public decimal FactSum
		{
			get
			{
				return _mFactSum;
			}
			set
			{
				_mFactSum = value;
				OnPropertyChanged("FactSum");
			}
		}
		#endregion

		private List<UKPaymentListItem> _mTransferRequests;
		public List<UKPaymentListItem> TransferRequests
		{
			get
			{
				return _mTransferRequests;
			}
			set
			{
				_mTransferRequests = value;
				OnPropertyChanged("TransferRequests");
			}
		}

		#region Execute implementation



		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create &&
				!TransferRequests.Any(x => x.ReqTransferID > 0);
		}

		protected override void ExecuteDelete(int delType)
		{
			BLServiceSystem.Client.DeleteUKPaymentsPlan(ID);
			base.ExecuteDelete(DocOperation.Archive);
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					var list = new List<LongDecimalTuple>();
					if (State == ViewModelState.Edit)
						TransferRequests.ForEach(a => list.Add(new LongDecimalTuple(a.ID, a.PlanSum)));
					else TransferRequests.ForEach(a => list.Add(new LongDecimalTuple(a.MonthID, a.PlanSum)));
					var newId = BLServiceSystem.Client.SaveUKPaymentPlan(Transfer, list);
					if (State == ViewModelState.Create)
						ID = Transfer.ID = newId;
					RefreshTransfers();
					break;
			}
		}
		#endregion

		#region Validation
		const string INVALID_ZL_COUNT = "Неверное значение количества застрахованных лиц";
		const string INVALID_PLAN_SUM = "Неверное значение плановой суммы";
		public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToUpper())
				{
					case "ZLCOUNT":
						return ZLCount <= 0 ? INVALID_ZL_COUNT : null;
					case "PLANSUM":
						return PlanSum <= 0 ? INVALID_PLAN_SUM : null;
					default:
						return null;
				}
			}
		}

		private bool Validate()
		{
			return string.IsNullOrEmpty(this["ZLCOUNT"]) &&
				   string.IsNullOrEmpty(this["PLANSUM"]);
		}
		#endregion

		public UKPaymentPlanViewModel(long entityId, ViewModelState state)
			: base(typeof(SIAssignPaymentsListViewModel), typeof(VRAssignPaymentsListViewModel), typeof(SIAssignPaymentsArchiveListViewModel), typeof(VRAssignPaymentsArchiveListViewModel))
		{
			DataObjectTypeForJournal = typeof(SITransfer);
			State = state;

			if (entityId > 0 && State != ViewModelState.Create)
			{
				//Edit
				ID = entityId;
				Transfer = DataContainerFacade.GetByID<SITransfer, long>(ID);
				var contract = Transfer.GetContract();
				ConractTypeId = contract.TypeID;
				long legalEntityID = contract.GetLegalEntity().ID;
				UpdateContractsList(legalEntityID);

				//оставляем только неиспользованные УК
				UKList = BLServiceSystem.Client.GetAvailableUkListForYearPlan(legalEntityID, ConractTypeId, false);

				SelectedUK = UKList.Find(le => le.ID == legalEntityID);
				SelectedContract = ContractsList.Find(c => c.ID == contract.ID);
			}
			else
			{
				//get contract type
				UpdateContractsList(entityId);
				var contract = _usedContracts.FirstOrDefault();
				ConractTypeId = contract?.TypeID ?? (int)Document.Types.SI;

				//оставляем только неиспользованные УК
				UKList = BLServiceSystem.Client.GetAvailableUkListForYearPlan(entityId, ConractTypeId, true);
				Transfer = new SITransfer { TransferRegisterID = entityId };
				if (!UKList.Any())// || (UKList.Count == 1 && UKList[0].GetContracts().Count == _usedContracts.Count(a => a.LegalEntityID == UKList[0].ID)))
				{
					DialogHelper.ShowError("У выбранного плана нет доступных УК для создания плана выплат!");

				    RequestClose?.Invoke(this, null);
				    return;
				}
				SelectedUK = UKList.First();
			}

			RefreshTransfers();
			RecalcSums();

			RefreshConnectedCardsViewModels = RefreshParentVM;
		}

		private void UpdateContractsList(long legalEntityId)
		{
			var exTransfers = DataContainerFacade.GetListByPropertyConditions<SITransfer>(
					new List<ListPropertyCondition>
                    {
                        ListPropertyCondition.Equal("TransferRegisterID", legalEntityId),
                        ListPropertyCondition.NotEqual("StatusID", (long)-1)
                    }).ToList();
			exTransfers.ForEach(a => _usedContracts.Add(a.GetContract()));
		}

		readonly List<Contract> _usedContracts = new List<Contract>();

		private void UpdateContractsSelection()
		{
			//get contract
			var contracts = SelectedUK.GetContracts();
			ContractsList = contracts.Where(c => c.TypeID == ConractTypeId && _usedContracts.Count(a => a.ID == c.ID && a.LegalEntityID == c.LegalEntityID) == 0).ToList();
			if (ID > 0 && ContractsList.Count == 0)
				ContractsList = new List<Contract> { Transfer.GetContract() };
			SelectedContract = SelectedUK == null ? null : ContractsList.FirstOrDefault();

		}

		private void RefreshParentVM()
		{
		    var vm4 = GetViewModel?.Invoke(typeof(YearPlanViewModel)) as YearPlanViewModel;
		    vm4?.RefreshUKPaymentsPlans();
		}

		public void RefreshTransfers()
		{
			bool alreadyChanged = IsDataChanged;

			if (ID > 0)
			{
				TransferRequests = BLServiceSystem.Client.GetUKPaymentsForPlan(ID);
			}
			else
			{
				var newList = new List<UKPaymentListItem>();
				for (var i = 1; i <= 12; i++)
				{
					newList.Add(
						new UKPaymentListItem
						{
							Month = DataContainerFacade.GetByID<Month, long>(i).Name,
							MonthID = i,
							PlanSum = 0,
							TransferListID = ID
						}
					);
				}
				TransferRequests = newList;
			}

			IsDataChanged = alreadyChanged;
		}

		private void ReDivideSums()
		{
			if (PlanSum <= 0)
				return;

			var sums = CurrencyTools.DivideSum(PlanSum, 12);
			for (int i = 0; i < 12; i++)
				TransferRequests[i].PlanSum = sums[i];
			NeedReloadTransferData();
		}

		public void RecalcSums()
		{
			Transfer.PlannedTransferSum = TransferRequests.Sum(req => req.PlanSum);
			OnPropertyChanged("PlanSum");

			FactSum = TransferRequests.Sum(req => req.FactSum);
			InvestIncome = TransferRequests.Sum(req => req.InvestDohod);

			Transfer.InsuredPersonsCount = TransferRequests.Sum(req => req.ZLCount);
			OnPropertyChanged("ZLCount");
		}

		public event EventHandler RequestClose;
	}
}
