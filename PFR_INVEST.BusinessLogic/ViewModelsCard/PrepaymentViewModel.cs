﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PrepaymentViewModel : ApsModelBase
	{
        public PrepaymentViewModel(PortfolioIdentifier.PortfolioTypes t)
			: base(t)
		{
            APS.KindID = 1; // потому что авансовый платёж
		}
	}
}
