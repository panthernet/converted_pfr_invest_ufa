﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ServiceItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OUFV_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    [UseRestore(typeof(Contragent))]
    public sealed class SIViewModel : ViewModelCard, ISISource, IUpdateListenerModel
    {
        #region Fields

        public override Type DataObjectTypeForJournal => typeof(LegalEntity);

        private List<LegalStatus> _lStatusesList = new List<LegalStatus>();
        private List<OldSIName> _oldSINames = new List<OldSIName>();
        private List<Contact> _contactsList = new List<Contact>();
        private List<Contract> _contractsList = new List<Contract>();
        private List<Branch> _branchesList = new List<Branch>();
        private List<LegalEntityIdentifier> _identifiersList = new List<LegalEntityIdentifier>();

        private Contact _selectedContact;
        private string _licUKNumber;
        private DateTime? _licUKRegistrationDate;
        private long? _licUKDuration;
        private string _licDdNumber;
        private DateTime? _licDdRegistrationDate;
        private string _licSdNumber;
        private DateTime? _licSdRegistrationDate;
        private long? _licSdDuration;
        private SIType _selectedType = SIType.УК;
        private LegalStatus _mSelectedLStatus;
        private Branch _selectedItem;
        private LegalEntityIdentifier _selectedIdentifier;

        private static readonly SIType[] _typesList = SIType.AllTypes.ToArray();
        private static readonly string[] _sexList = { "Мужской", "Женский" };
        private static readonly string[] _mTransfDocKinds = { "по почте", "курьером", "электронной почтой" };
        private readonly Contragent _contragent;

        //для отслеживания изменения названий
        private string _mOldFullName;
        private string _mOldShortName;
        private string _mOldFormalizedName;
        private string _mOldInn;

        #endregion

        #region Properties

        public LegalEntity LegalEntity { get; set; }

        public Contact SelectedContact
        {
            get { return _selectedContact; }
            set
            {
                _selectedContact = value;
                bool oldIsDataChanged = IsDataChanged;
                OnPropertyChanged("SelectedContact");
                IsDataChanged = oldIsDataChanged;
            }
        }

        public bool StateIsCreate => State == ViewModelState.Create;

        public string PDReason
        {
            get
            {
                return LegalEntity.PDReason;
            }
            set
            {
                LegalEntity.PDReason = value;
                OnPropertyChanged("PDReason");
            }
        }

        public DateTime? CloseDate
        {
            get
            {
                return LegalEntity.CloseDate;
            }
            set
            {
                LegalEntity.CloseDate = value;
                OnPropertyChanged("CloseDate");
            }
        }

        public string FullName
        {
            get
            {
                return LegalEntity.FullName;
            }
            set
            {
                LegalEntity.FullName = value;
                OnPropertyChanged("FullName");
            }
        }

        public string ShortName
        {
            get
            {
                return LegalEntity.ShortName;
            }
            set
            {
                LegalEntity.ShortName = value;
                OnPropertyChanged("ShortName");
            }
        }

        public string FormalizedName
        {
            get
            {
                return LegalEntity.FormalizedName;
            }
            set
            {
                LegalEntity.FormalizedName = value;
                OnPropertyChanged("FormalizedName");
				UpdateHeader();
            }
        }

        public string DeclarationName
        {
            get
            {
                return LegalEntity.DeclName;
            }
            set
            {
                LegalEntity.DeclName = value;
                OnPropertyChanged("DeclarationName");
            }
        }

        public string RegistrationNum
        {
            get
            {
                return LegalEntity.RegistrationNum;
            }
            set
            {
                LegalEntity.RegistrationNum = value;
                OnPropertyChanged("RegistrationNum");
            }
        }

        public DateTime? RegistrationDate
        {
            get
            {
                return LegalEntity.RegistrationDate;
            }
            set
            {
                LegalEntity.RegistrationDate = value;
                OnPropertyChanged("RegistrationDate");
            }
        }

        public string Registrator
        {
            get
            {
                return LegalEntity.Registrator;
            }
            set
            {
                LegalEntity.Registrator = value;
                OnPropertyChanged("Registrator");
            }
        }

        public string INN
        {
            get
            {
                return LegalEntity.INN;
            }
            set
            {
                LegalEntity.INN = value;
                OnPropertyChanged("INN");
            }
        }

        public string OKPP
        {
            get
            {
                return LegalEntity.OKPP;
            }
            set
            {
                LegalEntity.OKPP = value;
                OnPropertyChanged("OKPP");
            }
        }

        public string LegalAddress
        {
            get
            {
                return LegalEntity.LegalAddress;
            }
            set
            {
                LegalEntity.LegalAddress = value;
                OnPropertyChanged("LegalAddress");
            }
        }

        public string StreetAddress
        {
            get
            {
                return LegalEntity.StreetAddress;
            }
            set
            {
                LegalEntity.StreetAddress = value;
                OnPropertyChanged("StreetAddress");
            }
        }

        public string PostAddress
        {
            get
            {
                return LegalEntity.PostAddress;
            }
            set
            {
                LegalEntity.PostAddress = value;
                OnPropertyChanged("PostAddress");
            }
        }

        public string Phone
        {
            get
            {
                return LegalEntity.Phone;
            }
            set
            {
                LegalEntity.Phone = value;
                OnPropertyChanged("Phone");
            }
        }

        public string Fax
        {
            get
            {
                return LegalEntity.Fax;
            }
            set
            {
                LegalEntity.Fax = value;
                OnPropertyChanged("Phone");
            }
        }

        public string Email
        {
            get
            {
                return LegalEntity.EAddress;
            }
            set
            {
                LegalEntity.EAddress = value;
                OnPropertyChanged("Email");
            }
        }

        public string Site
        {
            get
            {
                return LegalEntity.Site;
            }
            set
            {
                LegalEntity.Site = value;
                OnPropertyChanged("Site");
            }
        }

        public string HeadFullName
        {
            get
            {
                return LegalEntity.HeadFullName;
            }
            set
            {
                LegalEntity.HeadFullName = value;
                OnPropertyChanged("HeadFullName");
            }
        }

        public string HeadPosition
        {
            get
            {
                return LegalEntity.HeadPosition;
            }
            set
            {
                LegalEntity.HeadPosition = value;
                OnPropertyChanged("HeadPosition");
            }
        }

        public string LetterWho
        {
            get
            {
                return LegalEntity.LetterWho;
            }
            set
            {
                LegalEntity.LetterWho = value;
                OnPropertyChanged("LetterWho");
            }
        }

        public object TransfDocKinds => _mTransfDocKinds;

        public string TransfDocKind
        {
            get
            {
                return LegalEntity.TransfDocKind;
            }
            set
            {
                LegalEntity.TransfDocKind = value;
                OnPropertyChanged("TransfDocKind");
            }
        }

        public string LicUKNumber
        {
            get
            {
                return _licUKNumber;
            }
            set
            {
                _licUKNumber = value;
                OnPropertyChanged("LicUKNumber");
            }
        }

        public DateTime? LicUKRegistrationDate
        {
            get
            {
                return _licUKRegistrationDate;
            }
            set
            {
                _licUKRegistrationDate = value;
                OnPropertyChanged("LicUKRegistrationDate");
            }
        }

        public long? LicUKDuration
        {
            get
            {
                return _licUKDuration;
            }
            set
            {
                _licUKDuration = value;
                OnPropertyChanged("LicUKDuration");
            }
        }

        public string LicDDNumber
        {
            get
            {
                return _licDdNumber;
            }
            set
            {
                _licDdNumber = value;
                OnPropertyChanged("LicDDNumber");
            }
        }

        public DateTime? LicDDRegistrationDate
        {
            get
            {
                return _licDdRegistrationDate;
            }
            set
            {
                _licDdRegistrationDate = value;
                OnPropertyChanged("LicDDRegistrationDate");
            }
        }

        public string LicSDNumber
        {
            get
            {
                return _licSdNumber;
            }
            set
            {
                _licSdNumber = value;
                OnPropertyChanged("LicSDNumber");
            }
        }

        public DateTime? LicSDRegistrationDate
        {
            get
            {
                return _licSdRegistrationDate;
            }
            set
            {
                _licSdRegistrationDate = value;
                OnPropertyChanged("LicSDRegistrationDate");
            }
        }

        public long? LicSDDuration
        {
            get
            {
                return _licSdDuration;
            }
            set
            {
                _licSdDuration = value;
                OnPropertyChanged("LicSDDuration");
            }
        }

        public License LicenseUK
        {
            get
            {
                if ((SelectedType == SIType.УК || SelectedType == SIType.ГУК)
                    && LicensiesList != null && LicensiesList.Count > 0)
                {
                    return (from lic in LicensiesList
                            where lic.Duration != null
                            select lic).FirstOrDefault();
                }
                return null;
            }
        }

        public License LicenseDD
        {
            get
            {
                if (SelectedType == _typesList[2]
                    && LicensiesList != null && LicensiesList.Count > 0)
                {
                    return (from lic in LicensiesList
                            where lic.Duration == null
                            select lic).FirstOrDefault();
                }
                return null;
            }
        }

        public License LicenseSD
        {
            get
            {
                if (SelectedType == _typesList[2]
                    && LicensiesList != null && LicensiesList.Count > 0)
                {
                    return (from lic in LicensiesList
                            where lic.Duration != null
                            select lic).FirstOrDefault();
                }
                return null;
            }

        }

        public List<License> LicensiesList
        {
            get
            {
                try
                {
                    return new List<License>(LegalEntity.GetLicensiesList().Cast<License>());
                }
                catch
                {
                    return null;
                }
            }
        }

        public SIType[] TypesList => _typesList;

        public string[] SexList => _sexList;

        public long? Sex
        {
            get
            {
                return LegalEntity.Sex;
            }
            set
            {
                LegalEntity.Sex = value;
                OnPropertyChanged("Sex");
            }
        }

        public SIType SelectedType
        {
            get
            {
                return _selectedType;
            }
            set
            {
                _selectedType = value;
                if (_contragent != null) _contragent.TypeName = value.ShortTitle;
                OnPropertyChanged("SelectedType");

                OnPropertyChanged("LicUKNumber");
                OnPropertyChanged("LicUKRegistrationDate");
                OnPropertyChanged("LicUKDuration");

                OnPropertyChanged("LicDDNumber");
                OnPropertyChanged("LicDDRegistrationDate");

                OnPropertyChanged("LicSDNumber");
                OnPropertyChanged("LicSDRegistrationDate");
                OnPropertyChanged("LicSDDuration");
            }
        }

        public List<LegalStatus> LStatusesList
        {
            get
            {
                return _lStatusesList;
            }
            set
            {
                _lStatusesList = value;
                OnPropertyChanged("LStatusesList");
            }
        }

        public LegalStatus SelectedLStatus
        {
            get
            {
                return _mSelectedLStatus;
            }

            set
            {
                _mSelectedLStatus = value;
                if (value != null)
                    LegalEntity.LegalStatusID = value.ID;
                OnPropertyChanged("SelectedLStatus");
            }
        }

        public List<OldSIName> OldSINames
        {
            get
            {
                return _oldSINames;
            }
            set
            {
                _oldSINames = value;
                OnPropertyChanged("OldSINames");
            }
        }

        public List<Contact> ContactsList
        {
            get
            {
                return _contactsList;
            }
            set
            {
                _contactsList = value;
                OnPropertyChanged("ContactsList");
            }
        }

        public List<Contract> ContractsList
        {
            get
            {
                return _contractsList;
            }
            set
            {
                _contractsList = value;
                OnPropertyChanged("ContractsList");
            }
        }

        public List<LegalEntityIdentifier> IdentifiersList
        {
            get
            {
                return _identifiersList;
            }
            set
            {
                _identifiersList = value;
                OnPropertyChanged("IdentifiersList");
            }
        }
          
        public List<Branch> BranchesList
        {
            get
            {
                return _branchesList;
            }
            set
            {
                _branchesList = value;
                OnPropertyChanged("BranchesList");
            }
        }

        public Branch SelectedItem
        {
            get
            {
                return _selectedItem;
            }

            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
                IsDataChanged = false;
            }
        }

        public LegalEntityIdentifier SelectedIdentifier
        {
            get
            {
                return _selectedIdentifier;
            }

            set
            {
                _selectedIdentifier = value;
                OnPropertyChanged("SelectedIdentifier");
                IsDataChanged = false;
            }
        }
        
        public Contract SelectedContract { get; set; }
        #endregion

        #region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && LegalEntity.GetContragent().StatusID != -1;
        }

        protected override void ExecuteDelete(int delType)
        {
            var contracts = BLServiceSystem.Client.GetContractsListForLegalEntityHib(LegalEntity.ID);
            var data = contracts.Where(a => a.StatusID >= 0).Select(item => new DeletedData {Id = item.ID, StringType = typeof(Contract).Name, PreviousStatus = item.StatusID}).ToList();

            DeleteEntity(_contragent, data, ()=> BLServiceSystem.Client.DeleteSI(_contragent.ID, LegalEntity.ID));
        }

        /// <summary>
        /// для обновления формы
        /// </summary>
        public void ExecuteResumeLicense()
        {
            bool wasChnaged = IsDataChanged;
            var le = DataContainerFacade.GetByID<LegalEntity, long>(InternalID);
            CloseDate = le.CloseDate;
            PDReason = le.PDReason;
            IsDataChanged = wasChnaged;
            JournalLogger.LogChangeStateEvent(this, "Деятельность возобновлена");
        }

        /// <summary>
        /// для обновления формы
        /// </summary>
        public void ExecuteStopLicense()
        {
            bool wasChnaged = IsDataChanged;
            var le = DataContainerFacade.GetByID<LegalEntity, long>(InternalID);
            CloseDate = le.CloseDate;
            PDReason = le.PDReason;
            IsDataChanged = wasChnaged;
            JournalLogger.LogChangeStateEvent(this, "Деятельность прекращена");
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    var sData = new SaveSIItem();
                    
                    foreach (var contact in ContactsList.Where(contact => contact.FIO != null && contact.FIO.Trim() != "" && contact.Phone != null && (contact.Phone.Trim() != "" || contact.Email != null && contact.Email.Trim() != "")))
                        sData.Contacts.Add(contact);

                    if (State == ViewModelState.Edit)
                    {
                        #region OLD

                        /*
                    if (SelectedType == _typesList[2])
                    {
                        if (LicenseSD != null)
                        {
                            LicenseSD.Number = _licSdNumber;
                            LicenseSD.RegistrationDate = LicSDRegistrationDate;
                            LicenseSD.Duration = LicSDDuration;
                            DataContainerFacade.Save<License, long>(LicenseSD);
                            //contragent.StatusID = isLicenseDurationNoFinish(LicenseSD) ? 1 : 6;
                        }
                        else
                        {
                            License lic = new License
                            {
                                LegalEntityID = LegalEntity.ID,
                                Number = _licSdNumber,
                                RegistrationDate = _licSdRegistrationDate,
                                Duration = _licSdDuration
                            };
                            DataContainerFacade.Save<License, long>(lic);
                            //contragent.StatusID = isLicenseDurationNoFinish(lic) ? 1 : 6;
                        }

                        if (LicenseDD != null)
                        {
                            LicenseDD.Number = _licDdNumber;
                            LicenseDD.RegistrationDate = LicDDRegistrationDate;
                            DataContainerFacade.Save<License, long>(LicenseDD);
                        }

                        else
                        {
                            var lic = new License
                            {
                                LegalEntityID = LegalEntity.ID,
                                Number = _licDdNumber,
                                RegistrationDate = _licDdRegistrationDate,
                                Duration = null
                            };
                            DataContainerFacade.Save<License, long>(lic);
                        }
                    }
                    else
                    {
                        if (LicenseUK != null)
                        {
                            LicenseUK.Number = _licUKNumber;
                            LicenseUK.RegistrationDate = LicUKRegistrationDate;
                            LicenseUK.Duration = LicUKDuration;
                            DataContainerFacade.Save<License, long>(LicenseUK);
                        }

                        else
                        {
                            var lic = new License
                            {
                                LegalEntityID = LegalEntity.ID,
                                Number = _licUKNumber,
                                RegistrationDate = _licUKRegistrationDate,
                                Duration = _licUKDuration
                            };
                            DataContainerFacade.Save<License, long>(lic);
                        }
                    }
                    if (_mOldFullName != FullName ||
                        _mOldFormalizedName != FormalizedName ||
                        _mOldShortName != ShortName ||
                        _mOldInn != INN)
                    {
                        if (!DialogHelper.ConfirmUpdateSIName())
                        {
                            FullName = _mOldFullName;
                            FormalizedName = _mOldFormalizedName;
                            ShortName = _mOldShortName;
                            INN = _mOldInn;
                        }
                        else
                        {
                            _contragent.Name = FormalizedName;
                            foreach (var contract in BLServiceSystem.Client.GetContractsListForLegalEntityHib(LegalEntity.ID))
                            {
                                contract.FormalizedName = FormalizedName;
                                contract.FullName = FullName;
                                DataContainerFacade.Save<Contract, long>(contract);
                            }
                        }
                    }
                    DataContainerFacade.Save<Contragent, long>(_contragent);
                    DataContainerFacade.Save<LegalEntity, long>(LegalEntity);
                    */

                        #endregion

                        if (SelectedType == _typesList[2])
                        {
                            if (LicenseSD != null)
                            {
                                LicenseSD.Number = _licSdNumber;
                                LicenseSD.RegistrationDate = LicSDRegistrationDate;
                                LicenseSD.Duration = LicSDDuration;
                                sData.Licenses.Add(LicenseSD);
                            }
                            else
                            {
                                var lic = new License
                                {
                                    LegalEntityID = LegalEntity.ID,
                                    Number = _licSdNumber,
                                    RegistrationDate = _licSdRegistrationDate,
                                    Duration = _licSdDuration
                                };
                                sData.Licenses.Add(lic);
                            }

                            if (LicenseDD != null)
                            {
                                LicenseDD.Number = _licDdNumber;
                                LicenseDD.RegistrationDate = LicDDRegistrationDate;
                                sData.Licenses.Add(LicenseDD);
                            }
                            else
                            {
                                var lic = new License
                                {
                                    LegalEntityID = LegalEntity.ID,
                                    Number = _licDdNumber,
                                    RegistrationDate = _licDdRegistrationDate,
                                    Duration = null
                                };
                                sData.Licenses.Add(lic);
                            }
                        }
                        else
                        {
                            if (LicenseUK != null)
                            {
                                LicenseUK.Number = _licUKNumber;
                                LicenseUK.RegistrationDate = LicUKRegistrationDate;
                                LicenseUK.Duration = LicUKDuration;
                                sData.Licenses.Add(LicenseUK);
                            }
                            else
                            {
                                var lic = new License
                                {
                                    LegalEntityID = LegalEntity.ID,
                                    Number = _licUKNumber,
                                    RegistrationDate = _licUKRegistrationDate,
                                    Duration = _licUKDuration
                                };
                                sData.Licenses.Add(lic);
                            }
                        }
                        if (_mOldFullName != FullName ||
                            _mOldFormalizedName != FormalizedName ||
                            _mOldShortName != ShortName ||
                            _mOldInn != INN)
                        {
                            if (!DialogHelper.ConfirmUpdateSIName())
                            {
                                FullName = _mOldFullName;
                                FormalizedName = _mOldFormalizedName;
                                ShortName = _mOldShortName;
                                INN = _mOldInn;
                            }
                            else
                            {
                                _contragent.Name = FormalizedName;
                                foreach (var contract in BLServiceSystem.Client.GetContractsListForLegalEntityHib(LegalEntity.ID))
                                {
                                    contract.FormalizedName = FormalizedName;
                                    contract.FullName = FullName;
                                    sData.Contracts.Add(contract);
                                }
                            }
                        }
                        sData.Contragent = _contragent;
                        sData.LegalEntity = LegalEntity;
                        BLServiceSystem.Client.SaveSI(sData);
                    }
                    else
                    {
                        #region OLD

                        /*_contragent.Name = LegalEntity.FormalizedName;
                    _contragent.TypeName = SelectedType.ShortTitle;
                    _contragent.StatusID = 6;
                    LegalEntity.ContragentID = DataContainerFacade.Save<Contragent, long>(_contragent);
                    LegalEntity.ID = DataContainerFacade.Save<LegalEntity, long>(LegalEntity);
                    ID = LegalEntity.ID;
                    _mOldFullName = FullName;
                    _mOldFormalizedName = FormalizedName;
                    _mOldShortName = ShortName;
                    _mOldInn = INN;

                    if (SelectedType == _typesList[2])
                    {
                        var licSD = new License {LegalEntityID = LegalEntity.ID, Number = _licSdNumber, RegistrationDate = _licSdRegistrationDate, Duration = _licSdDuration};
                        licSD.ID = DataContainerFacade.Save<License, long>(licSD);
                        var licDD = new License {LegalEntityID = LegalEntity.ID, Number = _licDdNumber, RegistrationDate = _licDdRegistrationDate, Duration = null};
                        licDD.ID = DataContainerFacade.Save<License, long>(licDD);
                    }
                    else
                    {
                        var lic = new License {LegalEntityID = LegalEntity.ID, Number = _licUKNumber, RegistrationDate = _licUKRegistrationDate, Duration = _licUKDuration};
                        lic.ID = DataContainerFacade.Save<License, long>(lic);
                    }*/

                        #endregion

                        _contragent.Name = LegalEntity.FormalizedName;
                        _contragent.TypeName = SelectedType.ShortTitle;
                        _contragent.StatusID = StatusIdentifier.Identifier.Eliminated.ToLong();
                        sData.Contragent = _contragent;
                        sData.LegalEntity = LegalEntity;
                        if (SelectedType == _typesList[2])
                        {
                            sData.Licenses.Add(new License { Number = _licSdNumber, RegistrationDate = _licSdRegistrationDate, Duration = _licSdDuration });
                            sData.Licenses.Add(new License { Number = _licDdNumber, RegistrationDate = _licDdRegistrationDate, Duration = null });
                        }
                        else sData.Licenses.Add(new License { Number = _licUKNumber, RegistrationDate = _licUKRegistrationDate, Duration = _licUKDuration });

                        var result = BLServiceSystem.Client.SaveSI(sData);
                        LegalEntity.ContragentID = _contragent.ID = result.ContragentId;
                        ID = LegalEntity.ID = result.LegalEntityId;
                        if (SelectedType == _typesList[2])
                        {
                            if (LicenseSD != null) LicenseSD.ID = result.LicSdId ?? 0;
                            if (LicenseDD != null) LicenseDD.ID = result.LicDdId ?? 0;
                        }
                        else if (LicenseUK != null) LicenseUK.ID = result.LicUkId ?? 0;

                        _mOldFullName = FullName;
                        _mOldFormalizedName = FormalizedName;
                        _mOldShortName = ShortName;
                        _mOldInn = INN;
                        //Fields = BLServiceSystem.Client.GetSIExt(ID);
                    }
                    break;
            }

            //Contacts
            foreach (var contact in ContactsList.Where(contact => contact.FIO != null && contact.FIO.Trim() != "" && contact.Phone != null && (contact.Phone.Trim() != "" || contact.Email != null && contact.Email.Trim() != "")))
            {
                contact.LegalEntityID = LegalEntity.ID;
                DataContainerFacade.Save<Contact, long>(contact);
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        private void ExecuteAddBranch()
        {
            bool isDataChanged = IsDataChanged;
            try
            {
                DialogHelper.AddBranchSI(ID);
                RefreshBranchesList();
            }
            catch
            {
                RaiseGetDataError();
            }
            IsDataChanged = isDataChanged;
        }

        public bool CanExecuteAddBranch()
        {
            return LegalEntity.ID > 0 && !EditAccessDenied;
        }

        public void ExecuteDeleteBranch()
        {
            bool isDataChanged = IsDataChanged;
            if (SelectedItem != null)
            {
                DataContainerFacade.Delete(SelectedItem);
                //BLServiceSystem.Client.DeleteBranch(SelectedItem.ID);
                JournalLogger.LogEvent("Отделение", JournalEventType.DELETE, SelectedItem.ID, "From: " + GetType().Name, null, typeof (Branch));
            }
            SelectedItem = null;
            RefreshBranchesList();
            IsDataChanged = isDataChanged;
        }

        public bool CanExecuteDeleteBranch()
        {
            return (SelectedItem != null) && CheckDeleteAccess(typeof(PFRBranchViewModel));
        }

        public void ExecuteDeleteIdentifier()
        {
            var x = SelectedIdentifier;
            //проверка на наличие идентификатора в БД
            var count = DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("SelectionIdentifier",x.Identifier)
            }) + DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("FromIdentifier",x.Identifier)
            });
            if (count != 0)
            {
                DialogHelper.ShowError("Выбранный идентификатор невозможно удалить, т.к. он используется в системе!");
                return;
            }

            bool isDataChanged = IsDataChanged;
            if (SelectedIdentifier != null)
            {
                SelectedIdentifier.StatusID = -1;
                BLServiceSystem.Client.SaveDeleteEntryLog(SelectedIdentifier.GetType().Name, "Идентификатор СИ - " + SelectedIdentifier.Identifier, SelectedIdentifier.ID, 1);
                DataContainerFacade.Save(SelectedIdentifier);
                JournalLogger.LogEvent("Идентификатор СИ", JournalEventType.DELETE, SelectedIdentifier.ID, "From: " + GetType().Name, null, typeof(LegalEntityIdentifier));

            }
            SelectedItem = null;
            RefreshIdentifiersList();
            IsDataChanged = isDataChanged;
        }

        public bool CanExecuteDeleteIdentifier()
        {
            return EditAccessAllowed && SelectedIdentifier != null;
        }

        public void ExecuteAddIdentifier()
        {
            bool isDataChanged = IsDataChanged;
            try
            {
                var x = new LegalEntityIdentifier();

                //var idenList = DataContainerFacade.GetList<LegalEntityIdentifier>();
                var idenList = BLServiceSystem.Client.GetExistingIdentifiers().ToList();
                bool res = DialogHelper.EditUKIdentifier(x, true, idenList);
                if (res)
                {
                    x.LegalEntityID = ID;
                    var cid = DataContainerFacade.Save(x);
                    JournalLogger.LogEvent("Идентификатор СИ", JournalEventType.INSERT, cid, "From: " + GetType().Name, null, typeof(LegalEntityIdentifier));

                    x.ID = cid;

                    RefreshIdentifiersList();                    
                }
            }
            catch
            {
                RaiseGetDataError();
            }
            IsDataChanged = isDataChanged;
        }

        public bool CanExecuteAddIdentifier()
        {
            return LegalEntity.ID > 0 && EditAccessAllowed;
        }

        private void ExecuteEditIdentifier(LegalEntityIdentifier x)
        {
            //var x = DataContainerFacade.GetByID<LegalEntityIdentifier>(SelectedIdentifier.ID);
            //var idenList = DataContainerFacade.GetList<LegalEntityIdentifier>();
            var idenList = BLServiceSystem.Client.GetExistingIdentifiers().ToList();
            idenList.RemoveAll(i => i.Identifier.Equals(x.Identifier, StringComparison.CurrentCultureIgnoreCase));
            if (DialogHelper.EditUKIdentifier(x, false, idenList))
            {
                DataContainerFacade.Save(x);

                IdentifiersList = BLServiceSystem.Client.GetUKIdentifiers(LegalEntity.ID);
            }
        }

        public bool CanExecuteEditIdentifier(LegalEntityIdentifier x)
        {
            return SelectedIdentifier != null && (EditAccessAllowed || IsUnderViewerAccess);
        }

        private void ExecuteDeleteContract()
        {
            bool isDataChanged = IsDataChanged;
            if (SelectedContract != null)
            {
                if (DialogHelper.ShowQuestion("Удалить выбранный документ?", $"Удаление документа {SelectedContract.FullName}"))
                {
                    DeleteEntity(SelectedContract);
                    RefreshContractsList();
                    UpdateContragent();
                    RefreshConnectedViewModels();
                }
            }
            IsDataChanged = isDataChanged;
        }

        public bool CanExecuteDeleteContract()
        {
            return (SelectedContract != null && SelectedContract.Status >= 0) && CheckDeleteAccess(typeof(SIContractViewModel));
        }

        public bool CanExecuteEditContact()
        {
            return SelectedContact != null && !EditAccessDenied;
        }

        private void ExecuteEditContact()
        {
            if (DialogHelper.EditContact(LegalEntity.ID, SelectedContact.ID))
            {
                RefreshContacts();
            }
        }

        private void ExecuteAddContact()
        {
            if (DialogHelper.AddContact(LegalEntity.ID))
            {
                RefreshContacts();
            }
        }

        
        public bool CanExecuteAddContact()
        {
            return LegalEntity.ID > 0 && !EditAccessDenied;
        }

        private void ExecuteDeleteContact()
        {
            DataContainerFacade.Delete(SelectedContact);
            JournalLogger.LogEvent("Контакт", JournalEventType.DELETE, SelectedContact.ID, "From: " + GetType().Name, null, typeof(Contact));
            SelectedContact = null;
            RefreshContacts();
        }

        public bool CanExecuteDeleteContact()
        {
            return SelectedContact != null && DeleteAccessAllowed;
        }
        #endregion

        #region Commands
        private readonly ICommand _addBranch;
        public ICommand AddBranch => _addBranch;

        private readonly ICommand _deleteBranch;
        public ICommand DeleteBranch => _deleteBranch;

        private readonly ICommand _addIdentifier;
        public ICommand AddIdentifier => _addIdentifier;

        private readonly ICommand _editIdentifier;
        public ICommand EditIdentifier => _editIdentifier;

        private readonly ICommand _deleteIdentifier;
        public ICommand DeleteIdentifier => _deleteIdentifier;

        //private readonly ICommand addContract;
        //public ICommand AddContract
        //{
        //    get
        //    {
        //        return addContract;
        //    }
        //}

        private readonly ICommand _deleteContract;
        public ICommand DeleteContract => _deleteContract;

        private readonly ICommand _addContact;
        public ICommand AddContact => _addContact;

        private readonly ICommand _editContact;
        public ICommand EditContact => _editContact;

        private readonly ICommand _deleteContact;
        public ICommand DeleteContact => _deleteContact;

        #endregion

        #region Refreshes

        private void RefreshIdentifiersList()
        {
            IdentifiersList = BLServiceSystem.Client.GetUKIdentifiers(LegalEntity.ID);
        }

        public void RefreshContractsList()
        {
            bool isDataChanged = IsDataChanged;
            ContractsList = BLServiceSystem.Client.GetContractsListForLegalEntityHib(LegalEntity.ID) ?? new List<Contract>();
            IsDataChanged = isDataChanged;
        }

        public void RefreshContacts()
        {
            if (LegalEntity == null) return;
            DataContainerFacade.ResetExtensionData(LegalEntity, "Contacts");
            var contacts = LegalEntity.GetContacts();
            ContactsList = contacts == null ? new List<Contact>() :
                new List<Contact>(contacts.Cast<Contact>());
            OnPropertyChanged("IsDeleteContactBtnEnabled");
        }

        /// <summary>
        /// Общий метод обертка для обновления списков
        /// </summary>
        /// <param name="method">Метод</param>
        private void RefreshList(Action method)
        {
            try
            {
                method();
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                if (IsInitialized)
                    RaiseGetDataError();
                else
                    throw new ViewModelInitializeException();
            }
        }

        private void UpdateContragent()
        {
            if (_contragent == null || string.Equals(_contragent.TypeName, "НПФ", StringComparison.OrdinalIgnoreCase))
                return;
       
            var contracts = ContractsList;

            if (contracts == null ) return;
            ContragentHelper.UpdateContragentStatus(_contragent, contracts.ToList());

            DataContainerFacade.Save(_contragent);
        }

        private void RefreshOldSINamesList()
        {
            OldSINames = BLServiceSystem.Client.GetOldSINamesSortedByDate(LegalEntity.ID);
        }

        private void RefreshBranchesList()
        {
            BranchesList = BLServiceSystem.Client.GetListByProperty(typeof(Branch).FullName, "LegalEntityID", LegalEntity.ID).Cast<Branch>().ToList();
        }

        #endregion

        #region ЗАГРУЗКА ДАННЫХ ПО ТАБАМ
        private int _tabIndex;
        public int TabIndex
        {
            get { return _tabIndex; }
            set
            {
                _tabIndex = value;
                OnTabIndexChanged(value);
            }
        }

        private readonly int[] _visitedTabs = new int[7];

        private void OnTabIndexChanged(int value)
        {
            if (_visitedTabs[value] != 0) return;
            var isChanged = IsDataChanged;
            switch (value)
            {
                case 0: //основные
                    if (LegalEntity.ID > 0)
                        RefreshList(RefreshOldSINamesList);
                    break;
                case 1: //адрес
                case 2: //лицензии
                case 3: //доп. информация
                    RefreshList(() =>
                    {
                        LStatusesList = DataContainerFacade.GetList<LegalStatus>();
                        SelectedLStatus = LStatusesList.Find(status => status.ID == LegalEntity.LegalStatusID);
                        OnPropertyChanged("SelectedLStatus");
                        RefreshContacts();
                    });
                    break;
                case 4: //договоры
                    if (LegalEntity.ID > 0)
                       RefreshList(RefreshContractsList);
                    break;
                case 5: //филиалы
                    if (LegalEntity.ID > 0)
                        RefreshList(RefreshBranchesList);
                    break;
                case 6: //идентификатор
                    if (LegalEntity.ID > 0)
                        RefreshList(RefreshIdentifiersList);
                    break;
            }

            _visitedTabs[value] = 1;
            if (!isChanged) IsDataChanged = false;
        }

        /// <summary>
        /// Загружаем все данные, для успешного прохождения тестов
        /// </summary>
        public void TestFullLoad()
        {
            var count = 0;
            _visitedTabs.ForEach(a =>
            {
                OnTabIndexChanged(count++);
            });
        }
        #endregion

        public SIViewModel(long id, ViewModelState action)
            : base(typeof(SIListViewModel), typeof(SIContractsListViewModel), typeof(VRContractsListViewModel), typeof(InsuranceSIListViewModel), typeof(InsuranceVRListViewModel), typeof(InsuranceArchiveSIListViewModel), typeof(InsuranceArchiveVRListViewModel))
        {
            State = action;

            UpdateListenTypes = new[] { typeof(Branch) };

            #region Command bindings
            _addBranch = new DelegateCommand(o => CanExecuteAddBranch(),
                o => ExecuteAddBranch());

            _deleteBranch = new DelegateCommand(o => CanExecuteDeleteBranch(),
                o => ExecuteDeleteBranch());

            _addIdentifier = new DelegateCommand(o => CanExecuteAddIdentifier(),
                o => ExecuteAddIdentifier());

            _editIdentifier = new DelegateCommand<LegalEntityIdentifier>(CanExecuteEditIdentifier,
                ExecuteEditIdentifier);

            _deleteIdentifier = new DelegateCommand(o => CanExecuteDeleteIdentifier(),
                o => ExecuteDeleteIdentifier());

            _deleteContract = new DelegateCommand(o => CanExecuteDeleteContract(),
                o => ExecuteDeleteContract());

            _addContact = new DelegateCommand(o => CanExecuteAddContact(),
                o => ExecuteAddContact());
            _editContact = new DelegateCommand(o => CanExecuteEditContact(),
                o => ExecuteEditContact());

            _deleteContact = new DelegateCommand(o => CanExecuteDeleteContact(),
                o => ExecuteDeleteContact());
            #endregion

            if (action == ViewModelState.Create)
            {
                //Fields = BLServiceSystem.Client.GetSIStructureExt();
                LegalEntity = new LegalEntity { RegistrationDate = DateTime.Now };
                _contragent = new Contragent();
                _licUKRegistrationDate = DateTime.Now;
                _licDdRegistrationDate = DateTime.Now;
                _licSdRegistrationDate = DateTime.Now;
                CloseDate = null;
            }
            else
            {
                ID = id;
                //Fields = BLServiceSystem.Client.GetSIExt(id);
                LegalEntity = DataContainerFacade.GetByID<LegalEntity, long>(id);

                _mOldFullName = LegalEntity.FullName;
                _mOldFormalizedName = LegalEntity.FormalizedName;
                _mOldShortName = LegalEntity.ShortName;
                _mOldInn = INN;

                _contragent = LegalEntity.GetContragent();
                _selectedType = _contragent.TypeName;

                if (_selectedType == _typesList[2])
                {
                    if (LicenseDD != null)
                    {
                        _licDdNumber = LicenseDD.Number;
                        LicDDRegistrationDate = LicenseDD.RegistrationDate;
                    }

                    if (LicenseSD != null)
                    {
                        _licSdNumber = LicenseSD.Number;
                        LicSDRegistrationDate = LicenseSD.RegistrationDate;
                        LicSDDuration = LicenseSD.Duration;
                    }
                }
                else
                {
                    if (LicenseUK != null)
                    {
                        _licUKNumber = LicenseUK.Number;
                        LicUKRegistrationDate = LicenseUK.RegistrationDate;
                        LicUKDuration = LicenseUK.Duration;
                    }
                }
            }

            OnTabIndexChanged(0);
            OnTabIndexChanged(1);
            OnTabIndexChanged(2);
			UpdateHeader();
            //RefreshLists();
            //RefreshContacts();

            IsInitialized = true;
        }

		private void UpdateHeader()
		{
			Header = "Субъект " + FormalizedName;
		}

        #region Validation

        private string Validate()
        {
            var data = "fullname|shortname|formalizedname|declarationname|selectedtype|registrationnum|registrationdate|registrator|inn|okpp|legaladdress|streetaddress|postaddress|phone";
            data += "|fax|email|headfullname|headposition|letterwho";
            if (_selectedType == _typesList[2])
                data += "|licddnumber|licddregistrationdate|licsdnumber|licsdregistrationdate|licsdduration";
            else
                data += "|licuknumber|licukregistrationdate|licukduration";
            foreach (var res in data.ToLower().Split('|').Select(key => this[key]).Where(res => !string.IsNullOrEmpty(res)))
                return res;
            return string.Empty;
        }

        private const string DURATION_HUNDRED = "Значение не более 100";

        public override string this[string key]
        {
            get
            {
                try
                {
                    switch (key.ToLower())
                    {
                        case "fullname": return string.IsNullOrEmpty(LegalEntity.FullName) || LegalEntity.FullName.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "shortname": return string.IsNullOrEmpty(LegalEntity.ShortName) || LegalEntity.ShortName.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "formalizedname": return string.IsNullOrEmpty(LegalEntity.FormalizedName) || LegalEntity.FormalizedName.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "declarationname": return string.IsNullOrEmpty(LegalEntity.DeclName) || LegalEntity.DeclName.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "registrationnum": return string.IsNullOrEmpty(LegalEntity.RegistrationNum) || LegalEntity.RegistrationNum.Length > 50 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "registrationdate": return LegalEntity.RegistrationDate == null || LegalEntity.RegistrationDate == DateTime.MinValue ? DataTools.DATAERROR_NEED_DATA : null;
                        case "registrator": return string.IsNullOrEmpty(LegalEntity.Registrator) || LegalEntity.Registrator.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;

                        case "inn": return LegalEntity.INN == null || !(LegalEntity.INN.Length == 10 && DataTools.IsNumericString(LegalEntity.INN)) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "okpp": return LegalEntity.OKPP == null || !(LegalEntity.OKPP != null && LegalEntity.OKPP.Trim().Length == 9 && DataTools.IsNumericString(LegalEntity.OKPP.Trim())) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "legaladdress": return string.IsNullOrEmpty(LegalEntity.LegalAddress) || LegalEntity.LegalAddress.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "streetaddress": return string.IsNullOrEmpty(LegalEntity.StreetAddress) || LegalEntity.StreetAddress.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "postaddress": return string.IsNullOrEmpty(LegalEntity.PostAddress) || LegalEntity.PostAddress.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "phone": return string.IsNullOrEmpty(LegalEntity.Phone) || LegalEntity.Phone.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "fax": return string.IsNullOrEmpty(LegalEntity.Fax) || LegalEntity.Fax.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "email": return LegalEntity.EAddress == null || LegalEntity.EAddress.Length > 128 || !DataTools.IsEmailList(LegalEntity.EAddress, " ,;\t".ToArray()) ? DataTools.DATAERROR_NEED_DATA : null;

                        case "licuknumber": return (_selectedType == _typesList[0] || _selectedType == _typesList[1]) &&
                                                   (string.IsNullOrEmpty(_licUKNumber) || _licUKNumber.Length > 50) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "licukregistrationdate": return (_selectedType == _typesList[0] || _selectedType == _typesList[1]) &&
                                                    (_licUKRegistrationDate == null || _licUKRegistrationDate == DateTime.MinValue) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "licukduration": return (_selectedType == _typesList[0] || _selectedType == _typesList[1]) &&
                                                     (_licUKDuration == null || _licUKDuration.ToString().Length == 0 || _licUKDuration <= 0) ? DataTools.DATAERROR_NEED_DATA : _licUKDuration > 100 ? DURATION_HUNDRED  : null;

                        case "licddnumber": return (_selectedType == _typesList[2]) &&
                                                   (string.IsNullOrEmpty(_licDdNumber) || _licDdNumber.Length > 50) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "licddregistrationdate": return (_selectedType == _typesList[2]) &&
                                                    (_licDdRegistrationDate == null || _licDdRegistrationDate == DateTime.MinValue) ? DataTools.DATAERROR_NEED_DATA : null;

                        case "licsdnumber": return (_selectedType == _typesList[2]) &&
                                                   (string.IsNullOrEmpty(_licSdNumber) || _licSdNumber.Length > 50) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "licsdregistrationdate": return (_selectedType == _typesList[2]) &&
                                                    (_licSdRegistrationDate == null || _licSdRegistrationDate == DateTime.MinValue) ? DataTools.DATAERROR_NEED_DATA : null;
                        case "licsdduration": return (_selectedType == _typesList[2]) &&
                            (_licSdDuration == null || _licSdDuration.ToString().Length == 0 || _licSdDuration <= 0) ? DataTools.DATAERROR_NEED_DATA : _licSdDuration > 100 ? DURATION_HUNDRED : null;

                        case "headfullname": return string.IsNullOrEmpty(LegalEntity.HeadFullName) || LegalEntity.HeadFullName.Length > 305 ? DataTools.DATAERROR_NEED_DATA : null;
                        case "headposition": return string.IsNullOrEmpty(LegalEntity.HeadPosition) || LegalEntity.HeadPosition.Length > 128 ? DataTools.DATAERROR_NEED_DATA : null;

                        case "letterwho": return string.IsNullOrEmpty(LegalEntity.LetterWho) || LegalEntity.LetterWho.Length > 512 ? DataTools.DATAERROR_NEED_DATA : null;

                        default: return null;
                    }
                }
                catch { return DataTools.DATAERROR_NEED_DATA; }
            }
        }
        #endregion

        #region ISISource
        long ISISource.GetSIID()
        {
            return LegalEntity != null ? LegalEntity.ID : -1;
        }
        #endregion

        public void Rename()
        {
            if (DialogHelper.RenameSI(LegalEntity.ID))
            {
                bool wasChanged = IsDataChanged;
                LegalEntity = DataContainerFacade.GetByID<LegalEntity>(LegalEntity.ID);
                _mOldFullName = LegalEntity.FullName;
                _mOldFormalizedName = LegalEntity.FormalizedName;
                _mOldShortName = LegalEntity.ShortName;
                _mOldInn = LegalEntity.INN;

                //RefreshLists();
                RefreshOldSINamesList();

                OnPropertyChanged("FullName");
                OnPropertyChanged("ShortName");
                OnPropertyChanged("FormalizedName");
                OnPropertyChanged("INN");

                //если не было ни каких изменений кроме переименования,
                // то помечаем как без изменений
                if (!wasChanged) IsDataChanged = false;

                if (GetViewModel != null)
                {
                    var silVM = GetViewModel(typeof(SIListViewModel)) as SIListViewModel;
                    if (silVM != null)
                        silVM.RefreshList.Execute(null);
                }
            }
        }


       /* private bool isLicenseDurationNoFinish(License license)
        {
            if (license == null) return false;
            if (license.RegistrationDate.HasValue && license.Duration.HasValue)
            {
                return DateTime.Now < license.RegistrationDate.Value.AddYears((int)license.Duration.Value);
            }
            return false;
        }*/

        public Type[] UpdateListenTypes { get; private set; }
        public void OnDataUpdate(Type type, long id)
        {
            if(type == typeof(Branch))
                RefreshBranchesList();
        }
    }

}
