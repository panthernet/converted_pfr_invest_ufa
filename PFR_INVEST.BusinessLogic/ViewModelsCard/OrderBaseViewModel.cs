﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class OrderBaseViewModel : ViewModelCard
    {
        public CbOrder Order { get; protected set; }

        #region Statuses
        protected void RefreshStatuses(string status)
        {
            Statuses = OrderStatusIdentifier.GetAvailableStatuses(status);
        }
        protected void RefreshStatuses(string status, string deleteStatus)
        {
            var st = OrderStatusIdentifier.GetAvailableStatuses(status);
            st.Remove(deleteStatus);
            Statuses = st;
        }
        private List<string> m_Statuses;
        public List<string> Statuses
        {
            get
            {
                return m_Statuses;
            }
            set
            {
                m_Statuses = value;
                SelectedStatus = value.FirstOrDefault();
                OnPropertyChanged("Statuses");
            }
        }

        protected void RefreshCanEditStatus()
        {
            //Статус можно редактировать только, если он "На подписи" или "На исполении"
			CanEditStatus = !Order.IsReadOnly && (OrderStatusIdentifier.IsStatusOnSigning(InitialStatus) || OrderStatusIdentifier.IsStatusOnExecution(InitialStatus));
        }

        private bool m_CanEditStatus;
        public bool CanEditStatus
        {
            get
            {
                return m_CanEditStatus;
            }
            set
            {
                m_CanEditStatus = value;
                OnPropertyChanged("CanEditStatus");
            }
        }


        public string InitialStatus;
        public string SelectedStatus
        {
            get
            {
                return Order.Status;
            }
            set
            {
                Order.Status = value;
                OnPropertyChanged("SelectedStatus");
            }
        }
        #endregion

        #region Comment
        public string Comment
        {
            get
            {
                return Order.Comment;
            }
            set
            {
                Order.Comment = value;
                OnPropertyChanged("Comment");
            }
        }
        #endregion

        #region RegDate & Term
        public DateTime? RegDate
        {
            get
            {
                if (Order != null && Order.RegDate.HasValue)
                    return Order.RegDate.Value.Date;
                return DateTime.Today;
            }
            set
            {
                if (value != null)
                {
                    Order.RegDate = value;
                    Order.Start = value;
                    if (Term < value)
                        Term = value;
                    OnPropertyChanged("RegDate");
                    OnPropertyChanged("Start");
                }
            }
        }

        public DateTime? DIDate
        {
            get
            {
                if (Order != null && Order.DIDate.HasValue)
                    return Order.DIDate.Value.Date;
                return DateTime.Today;
            }
            set
            {
                if (value != null)
                {
                    Order.DIDate = value;
                }
            }
        }

        public virtual DateTime? RegDateMinValue
        {
            get
            {
                if (DIDate.HasValue)
                    return DIDate.Value.AddDays(-31);
                return DateTime.Today.AddDays(-31);
            }
        }

        public DateTime? Term
        {
            get
            {
                if (Order != null && Order.Term.HasValue)
                    return Order.Term.Value;
                return DateTime.Today;
            }
            set
            {
                if (value != null)
                {
                    if (value < RegDate)
                        Order.Term = RegDate;
                    else
                        Order.Term = value;
                    OnPropertyChanged("Term");
                    OnPropertyChanged("RegDate");
                }
            }
        }
        #endregion

        #region RegNum
        //счетчик поручений в портфеле
        //long m_Counter = 0;
        //long m_OriginalCounter = 0;
        //long m_OriginalPortfolioYearID = 0;

        public long GenerateCounter(long? yearID, string orderType)
        {
            return BLServiceSystem.Client.GetOrderCounterForPortfolioYear(yearID, orderType) + 1;
        }

        public string GenerateOrderRegNum(Portfolio portfolio, LegalEntity bankAgent, string oType, long regNum)
        {
            //if (SelectedPortfolio == null || !SelectedPortfolio.YearID.HasValue || SelectedBankAgent == null)
            //    return;
            if (portfolio == null || bankAgent == null)
                return string.Empty;

            string year = portfolio.YearID.HasValue ? DataContainerFacade.GetByID<Year>(portfolio.YearID).Name : "____";

            string bank = bankAgent.FormalizedName;
            if (bank.Contains("ЦБ"))
                bank = "ЦБР";

            //string opType = (OrderTypeIdentifier.IsBuyOrder(SelectedType)) ? "К" : "П";

            return string.Format("{0}-{1}-{2}/{3}", year, bank, oType, regNum);//Order.RegNumLong);
        }

        public string RegNum
        {
            get
            {
                return Order.RegNum;
            }
            set
            {
                Order.RegNum = value;
                OnPropertyChanged("RegNum");
            }
        }
        #endregion

        public OrderBaseViewModel()
            : base(typeof(OrdersListViewModel))
        {
            DataObjectTypeForJournal = typeof(CbOrder);
        }

        protected override void ExecuteSave()
        {
        }

        public override bool CanExecuteSave()
        {
            return Validate();
        }

        protected virtual bool Validate()
        {
            return string.IsNullOrEmpty(this["Term"])
                && string.IsNullOrEmpty(this["RegDate"])
                && string.IsNullOrEmpty(this["Comment"]);
        }

        const string NULL_ERROR = "Необходимо указать данные";
        const string DATE_VALUE_ERROR = "Неверное значение даты";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Term": return Term == null ? NULL_ERROR :
                        Term < RegDate ? DATE_VALUE_ERROR : null;
                    //case "RegNum": return RegNum == null ? NULL_ERROR : null;
                    case "RegDate": return RegDate == null ? NULL_ERROR :
                        RegDate < RegDateMinValue ? DATE_VALUE_ERROR : null;
                    case "Comment": return Comment != null ? Comment.ValidateMaxLength(512) : null;
                    default: return null;
                }
            }
        }
    }
}
