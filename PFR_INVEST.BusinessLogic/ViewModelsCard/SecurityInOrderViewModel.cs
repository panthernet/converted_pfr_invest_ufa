﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)][EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class SecurityInOrderViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public CbInOrder CbInOrder { get; private set; }
        public CbOrder Order { get; private set; }
        public BindingList<CbInOrderListItem> CreatedItems { get; private set; }

        public SecurityInOrderType SecurityInOrderType { get; private set; }

        public readonly IEnumerable<CbInOrderListItem> AlreadyAddedSecurities;

        public event EventHandler OnCbInOrderListItemCreated;
        protected void RaiseCbInOrderListItemCreated()
        {
            if (OnCbInOrderListItemCreated != null)
                OnCbInOrderListItemCreated(this, null);
        }

        public event EventHandler OnNeedUpdateControlsValidationState;
        protected void RaiseNeedUpdateControlsValidationState()
        {
            if (OnNeedUpdateControlsValidationState != null)
                OnNeedUpdateControlsValidationState(this, null);
        }

        public bool NoAvailableSecuritiesFound
        {
            get
            {
                if (SecuritiesList == null || SecuritiesList.Count == 0) return true;
                return SecuritiesList.All(sec => sec.AvailableCount <= 0);
            }
        }

        #region TotalNomValue
        /// <summary>
        /// Видимость поля "Общая номинальная стоимость ЦБ"
        /// </summary>
        public bool TotalNomValueVisible => SecurityInOrderType == SecurityInOrderType.BuyCurrency || SecurityInOrderType == SecurityInOrderType.SaleCurrency;

        public decimal TotalNomValue
        {
            get
            {
                return CbInOrder.NomValue ?? 0;
            }
            set
            {
                CbInOrder.NomValue = value;
                RaiseNeedUpdateControlsValidationState();
                OnPropertyChanged("TotalNomValue");
            }
        }
        #endregion

        #region Count
        /// <summary>
        /// Видимость поля "Количество, штук"
        /// </summary>
        public bool CountVisible => SecurityInOrderType == SecurityInOrderType.BuyRublesOnSecondMarket || SecurityInOrderType == SecurityInOrderType.SaleRoubles;

        public long Count
        {
            get
            {
                return CbInOrder.Count ?? 0;
            }

            set
            {
                CbInOrder.Count = value;
                RaiseNeedUpdateControlsValidationState();
                OnPropertyChanged("Count");
            }
        }
        #endregion

        #region MaxCostValue
        /// <summary>
        /// Видимость поля "Макс. общая стоимость ЦБ (с НКД)"
        /// </summary>
        public bool MaxCostVisible => SecurityInOrderType == SecurityInOrderType.BuyCurrency;

        public decimal MaxCostValue
        {
            get
            {
                return CbInOrder.MaxNomValue ?? 0;
            }
            set
            {
                CbInOrder.MaxNomValue = value;
                RaiseNeedUpdateControlsValidationState();
                OnPropertyChanged("MaxCostValue");
            }
        }
        #endregion

        #region MinCostValue
        /// <summary>
        /// Видимость поля "Мин. общая стоимость ЦБ (с НКД)"
        /// </summary>
        public bool MinCostVisible => SecurityInOrderType == SecurityInOrderType.SaleCurrency;

        public decimal MinCostValue
        {
            get
            {
                return CbInOrder.MinValueNKD ?? 0;
            }

            set
            {
                CbInOrder.MinValueNKD = value;
                RaiseNeedUpdateControlsValidationState();
                OnPropertyChanged("MinCostValue");
            }
        }
        #endregion

        #region SumMoney
        /// <summary>
        /// Видимость поля "Сумма денежных средств, руб."
        /// </summary>
        public bool SumMoneyVisible => SecurityInOrderType == SecurityInOrderType.BuyRublesOnSecondMarket;

        public decimal SumMoney
        {
            get
            {
                return CbInOrder.SumMoney ?? 0;
            }

            set
            {
                CbInOrder.SumMoney = value;
                RaiseNeedUpdateControlsValidationState();
                OnPropertyChanged("SumMoney");
            }
        }
        #endregion

        #region MinSellPrice
        /// <summary>
        /// Видимость поля "Мин. цена продажи (в %)"
        /// </summary>
        public bool MinSellPriceVisible => SecurityInOrderType == SecurityInOrderType.SaleRoubles;

        public decimal MinSellPrice
        {
            get
            {
                return CbInOrder.MinPriceSell ?? 0;
            }
            set
            {
                CbInOrder.MinPriceSell = value;
                OnPropertyChanged("MinSellPrice");
            }
        }
        #endregion

        #region MaxPrice
        /// <summary>
        /// Видимость поля "Макс. цена (в %, без НКД)"
        /// </summary>
        public bool MaxPriceVisible => SecurityInOrderType == SecurityInOrderType.BuyCurrency;

        public decimal MaxPrice
        {
            get
            {
                return CbInOrder.MaxPrice ?? 0;
            }
            set
            {
                CbInOrder.MaxPrice = value;
                OnPropertyChanged("MaxPrice");
            }
        }
        #endregion

        #region MinPrice
        /// <summary>
        /// Видимость поля "Мин. цена (в %, без НКД)"
        /// </summary>
        public bool MinPriceVisible => SecurityInOrderType == SecurityInOrderType.SaleCurrency;

        public decimal MinPrice
        {
            get
            {
                return CbInOrder.MinPrice ?? 0;
            }

            set
            {
                CbInOrder.MinPrice = value;
                OnPropertyChanged("MinPrice");
            }
        }
        #endregion

        #region AuctionSecurities
        /// <summary>
        /// Видимость грида ЦБ при покупке на аукционе
        /// </summary>
        public Visibility AuctionSecuritiesVisible
        {
            get
            {
                if (SecurityInOrderType == SecurityInOrderType.BuyRoublesOnAuction)
                    return Visibility.Visible;
                return Visibility.Collapsed;
            }
        }
        #endregion

        #region MaxPriceBay
        /// <summary>
        /// Видимость поля "Макс. цена покупки (в %)"
        /// </summary>
        public bool MaxBuyPriceVisible => SecurityInOrderType == SecurityInOrderType.BuyRublesOnSecondMarket;

        public decimal MaxBuyPrice
        {
            get
            {
                return CbInOrder.MaxPriceBay ?? 0;
            }
            set
            {
                CbInOrder.MaxPriceBay = value;
                OnPropertyChanged("MaxBuyPrice");
            }
        }
        #endregion

        #region SecuritiesList
        private List<Payment> m_PaymentsList;
        private void RefreshPayments()
        {
            m_PaymentsList = BLServiceSystem.Client.GetPaymentsListHib(SelectedSecurity.SecurityID);
        }

        private List<SecurityAvailableForOrderListItem> m_SecuritiesList;
        public List<SecurityAvailableForOrderListItem> SecuritiesList
        {
            get
            {
                return m_SecuritiesList;
            }
            set
            {
                m_SecuritiesList = value;
                OnPropertyChanged("SecuritiesList");
            }
        }

        SecurityAvailableForOrderListItem m_SelectedSecurity;
        public SecurityAvailableForOrderListItem SelectedSecurity
        {
            get
            {
                return m_SelectedSecurity;
            }
            set
            {
                if (value != null && CbInOrder.SecurityID != value.SecurityID)
                {
                    CbInOrder.SecurityID = value.SecurityID;
                    m_SelectedSecurity = value;
                    RefreshPayments();
                    RaiseNeedUpdateControlsValidationState();
                }
                OnPropertyChanged("SelectedSecurity");
                OnPropertyChanged("SelectedSecurityCurrencyName");
                OnPropertyChanged("SelectedSecurityKind");
                OnPropertyChanged("SelectedSecurityNominal");
            }
        }

        public string SelectedSecurityCurrencyName
        {
            get
            {
                if (SelectedSecurity == null)
                    return null;
                return SelectedSecurity.Security.CurrencyName;
            }
        }
        public string SelectedSecurityKind
        {
            get
            {
                if (SelectedSecurity == null)
                    return null;
                return SelectedSecurity.Security.KindName;
            }
        }
        public decimal SelectedSecurityNominal
        {
            //В добавлении ц/б в поручение выводить не номинал, а непогашенный остаток номинала (брать из выплат)
            get
            {
                if (m_PaymentsList == null)
                    return 0;
                var list = m_PaymentsList.Where(paym => paym.PaymentDate <= DateTime.Today);
                if (list == null || list.Count() == 0) return 0;
                return list.First().NotRepaymentNom ?? 0m;
            }
        }
        public long SelectedSecurityAvailableCount
        {
            get
            {
                if (SelectedSecurity == null)
                    return 0;
                return SelectedSecurity.AvailableCount;
            }
        }
        #endregion

       /* public override string GetDocumentHeader()
        {
            return "Добавление ЦБ к поручению";
        }*/

        public SecurityInOrderViewModel(CbOrder p_Order, SecurityInOrderType p_OrderType, IEnumerable<CbInOrderListItem> p_AlreadyAddedSecurities)
        {
            //DataObjectTypeForJournal = typeof(CbInOrder);
            State = ViewModelState.Create;
            Order = p_Order;
            SecurityInOrderType = p_OrderType;

            CbInOrder = new CbInOrder();
            CbInOrder.DateDI = DateTime.Now.Date;
            AlreadyAddedSecurities = p_AlreadyAddedSecurities;
            RefreshLists();

            if (!NoAvailableSecuritiesFound)
                SelectedSecurity = SecuritiesList.FirstOrDefault();

            CreatedItems = new BindingList<CbInOrderListItem>();

            AddSecurity = new DelegateCommand(o => true, o => EcecuteAddSecurity());
            RecalcCanSelectOrRemoveSecurities();
        }

        public void RefreshLists()
        {
            DataContainerFacade.ResetExtensionData(Order, "DEPOAccount");
            PfrBankAccount depoAccount = Order.GetDEPOAccount();
            if (depoAccount == null)
            {
                DataContainerFacade.ResetExtensionData(Order, "TradeAccount");
                DataContainerFacade.ResetExtensionData(Order, "CurrentAccount");
                DataContainerFacade.ResetExtensionData(Order, "TransitAccount");
                depoAccount = Order.GetPfrBankAccount();
            }
                
               
            if (depoAccount == null || !depoAccount.CurrencyID.HasValue)
                return;

            if (OrderTypeIdentifier.IsBuyOrder(Order.Type))
            {
//                if (SecurityInOrderType == SecurityInOrderType.BuyRoublesOnAuction && AlreadyAddedSecurities.Any())
//                    SecuritiesList = new List<SecurityAvailableForOrderListItem>(new[] { 
//                        BLServiceSystem.Client.GetSecurityAvailableForAuctionOrder(AlreadyAddedSecurities.First().Security.ID)
//                    });
//                else
                    if (Order.Term.HasValue)
                        SecuritiesList = BLServiceSystem.Client.GetSecuritiesAvailableForBuyOrder(depoAccount.CurrencyID.Value, Order.Term.Value);
                    else
                        SecuritiesList = null;
            }
            else if (OrderTypeIdentifier.IsSellOrder(Order.Type))
                if (Order.PortfolioID.HasValue)
                    SecuritiesList = BLServiceSystem.Client.GetSecuritiesAvailableForSaleOrder(depoAccount.CurrencyID.Value, Order.PortfolioID.Value);
                else
                    SecuritiesList = null;

            if (/*SecurityInOrderType != SecurityInOrderType.BuyRoublesOnAuction &&*/ SecuritiesList != null)
                SecuritiesList.RemoveAll(
                    sec => AlreadyAddedSecurities.Any(item => item.Security.ID == sec.SecurityID) || sec.Security.Status != 0
                );
        }

        private void RecalcCanSelectOrRemoveSecurities()
        {
            CanRemoveSecurity = CreatedItems.Count > 0;

            if (SecurityInOrderType != SecurityInOrderType.BuyRoublesOnAuction) return;
            if (AlreadyAddedSecurities.Any())
            {
                //CanSelectSecurity = false;
                CanSelectSecurity = many_CanSelectSecurity;
                return;
            }
            if (CreatedItems.Count > 0)
            {
                CanSelectSecurity = false;
                return;
            }
            CanSelectSecurity = true;
        }

        private bool m_CanSelectSecurity = true;
        private bool many_CanSelectSecurity = true;
        public bool CanSelectSecurity
        {
            get
            {
                return m_CanSelectSecurity;
            }
            set
            {
                m_CanSelectSecurity = value;
                OnPropertyChanged("CanSelectSecurity");
            }
        }

        private bool m_CanRemoveSecurity;
        public bool CanRemoveSecurity
        {
            get
            {
                return m_CanRemoveSecurity;
            }
            set
            {
                m_CanRemoveSecurity = value;
                OnPropertyChanged("CanRemoveSecurity");
            }
        }

        public ICommand AddSecurity { get; private set; }
        private void EcecuteAddSecurity()
        {
            many_CanSelectSecurity = false;
            CreatedItems.Add(new CbInOrderListItem
                {
                    CbInOrder = CbInOrder.Clone() as CbInOrder,
                    Security = SelectedSecurity.Security,
                    Currency = SelectedSecurity.Security.GetCurrency()
                }
            );
            RecalcCanSelectOrRemoveSecurities();
        }

        public void RemoveSecurity(CbInOrderListItem item)
        {
            CreatedItems.Remove(item);
            RecalcCanSelectOrRemoveSecurities();
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Create:
                    if (SecurityInOrderType != SecurityInOrderType.BuyRoublesOnAuction)
                        CreatedItems.Add(new CbInOrderListItem
                            {
                                CbInOrder = CbInOrder,
                                Security = SelectedSecurity.Security,
                                Currency = SelectedSecurity.Security.GetCurrency()
                            }
                        );
                    RaiseCbInOrderListItemCreated();
                    break;
                case ViewModelState.Read:
                case ViewModelState.Edit:
                default:
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate();
        }

        private const string INVALID_COUNT = "Количество должно быть больше нуля и меньше 100000000.";
        private const string INVALID_COUNT_AVAILABLE = "Количество должно быть не больше общего количества бумаг в портфеле.";
        private const string INVALID_VALUE = "Значение должно быть больше нуля.";
        private const string INVALID_SELECTED = "Значение должно быть выбрано.";
        private const string INVALID_PERCENT = "Значение должно быть больше нуля и меньше 1000.";

        private bool CheckCreatedItemsForAuction()
        {
            if (CreatedItems.Count == 0)
                return false;

            foreach (var item in CreatedItems)
            {
                if ((item.Count ?? 0) <= 0 && (item.SumMoney ?? 0) <= 0)
                    return false;
                if ((item.FixPriceBay ?? 0) <= 0)
                    return false;
            }
            return true;
        }

        public bool Validate()
        {
            if (SecurityInOrderType == SecurityInOrderType.BuyRoublesOnAuction)
                return CheckCreatedItemsForAuction();

            return string.IsNullOrEmpty(this["TotalNomValue"]) &&
                   string.IsNullOrEmpty(this["Count"]) &&
                   string.IsNullOrEmpty(this["MaxCostValue"]) &&
                   string.IsNullOrEmpty(this["MinCostValue"]) &&
                   string.IsNullOrEmpty(this["SumMoney"]) &&
                   string.IsNullOrEmpty(this["MinSellPrice"]) &&
                   string.IsNullOrEmpty(this["MaxPrice"]) &&
                   string.IsNullOrEmpty(this["MinPrice"]) &&
                   string.IsNullOrEmpty(this["MaxBuyPrice"]) &&
                   string.IsNullOrEmpty(this["SelectedSecurity"]);

        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedSecurity":
                        return SelectedSecurity != null && SelectedSecurity.SecurityID > 0 ? null : INVALID_SELECTED;

                    case "TotalNomValue":
                        if (TotalNomValueVisible)
                        {
                            if (TotalNomValue > 0)
                                return null;
                            if (SecurityInOrderType == SecurityInOrderType.BuyCurrency)
                            {
                                if (MaxCostValue > 0)
                                    return null;
                            }
                            else if (SecurityInOrderType == SecurityInOrderType.SaleCurrency)
                            {
                                if (MinCostValue > 0)
                                    return null;
                            }
                            return INVALID_VALUE;
                        }
                        return null;
                    case "Count":
                        if (CountVisible)
                        {
                            if (SecurityInOrderType == SecurityInOrderType.SaleRoubles && Count > SelectedSecurityAvailableCount)
                                return INVALID_COUNT_AVAILABLE;
                            if (Count > 0 && Count < 100000000)
                                return null;
                            if (SecurityInOrderType == SecurityInOrderType.BuyRublesOnSecondMarket)
                            {
                                if (SumMoney > 0)
                                    return null;
                            }

                            return INVALID_COUNT;
                        }
                        return null;
                    case "MaxCostValue":
                        if (MaxCostVisible)
                        {
                            if (MaxCostValue > 0)
                                return null;
                            if (TotalNomValue > 0)
                                return null;

                            return INVALID_VALUE;
                        }
                        return null;
                    case "MinCostValue":
                        if (MinCostVisible)
                        {
                            if (MinCostValue > 0)
                                return null;
                            if (TotalNomValue > 0)
                                return null;

                            return INVALID_VALUE;
                        }
                        return null;
                    case "SumMoney":
                        if (SumMoneyVisible)
                        {
                            if (SumMoney > 0)
                                return null;
                            if (Count > 0)
                                return null;

                            return INVALID_VALUE;
                        }
                        return null;
                    case "MinSellPrice":
                        if (MinSellPriceVisible)
                        {
                            return MinSellPrice > 0 && MinSellPrice < 1000 ? null : INVALID_PERCENT;
                        }
                        return null;
                    case "MaxPrice":
                        if (MaxPriceVisible)
                        {
                            //Проверяется всегда, 5 значащих цифр
                            return MaxPrice > 0 && MaxPrice < 1000 ? null : INVALID_PERCENT;
                        }
                        return null;
                    case "MinPrice":
                        if (MinPriceVisible)
                        {
                            //Проверяется всегда, 5 значащих цифр
                            return MinPrice > 0 && MinPrice < 1000 ? null : INVALID_PERCENT;
                        }
                        return null;
                    case "MaxBuyPrice":
                        if (MaxBuyPriceVisible)
                        {
                            //Проверяется всегда
                            return MaxBuyPrice > 0 ? null : INVALID_VALUE;
                        }
                        return null;
                    default:
                        return null;
                }
            }
        }
    }
}
