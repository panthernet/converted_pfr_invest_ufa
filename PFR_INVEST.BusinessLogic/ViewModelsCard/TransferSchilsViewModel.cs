﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public sealed class TransferSchilsViewModel : ViewModelCard, IUpdateRaisingModel
	{
		public Transfer Transfer { get; private set; }

		private readonly ICommand _selectAccount;
		public ICommand SelectAccount => _selectAccount;

	    private PortfolioFullListItem _mAccount;
		public PortfolioFullListItem Account
		{
			get
			{
				return _mAccount;
			}
			set
			{
				_mAccount = value;
				if (value != null)
				{
					Transfer.PFRBankAccountID = value.PfrBankAccount.ID;
					Transfer.PortfolioID = value.Portfolio.ID;
				}
				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("AccountPortfolio");
			}
		}

		public string AccountNumber => Account == null ? "" : Account.PfrBankAccount.AccountNumber;

	    public string AccountPortfolio => Account == null ? "" : Account.Portfolio.Year;

	    private List<Direction> _mDirectionsList;
		public List<Direction> DirectionsList
		{
			get
			{
				return _mDirectionsList;
			}
			set
			{
				_mDirectionsList = value;
				OnPropertyChanged("DirectionsList");
			}
		}

		private Direction _mSelectedDirection;
		public Direction SelectedDirection
		{
			get
			{
				return _mSelectedDirection;
			}
			set
			{
				_mSelectedDirection = value;
				if (value != null)
				{
					Transfer.DirectionID = value.ID;
					DirectionDate = value.Date;
				}
			}
		}

		private readonly List<string> _mRecipientsList = new List<string> { "ОПФР", "ИЦПУ", "Не определен" };
		public List<string> RecipientsList => _mRecipientsList;

	    public string SelectedRecipient
		{
			get
			{
				return (Transfer.Recipient ?? string.Empty).Trim();
			}
			set
			{
				Transfer.Recipient = value;
				OnPropertyChanged("SelectedRecipient");
			}
		}

		public DateTime? Date
		{
			get
			{
				return Transfer.Date;
			}
			set
			{
				Transfer.Date = value;
				OnPropertyChanged("Date");
			}
		}

		public DateTime? DIDate
		{
			get
			{
				return Transfer.DIDate;
			}
			set
			{
				Transfer.DIDate = value;
				OnPropertyChanged("DIDate");
			}
		}

		private DateTime? _mDirectionDate;
		public DateTime? DirectionDate
		{
			get
			{
				return _mDirectionDate;
			}
			set
			{
				_mDirectionDate = value;
				OnPropertyChanged("DirectionDate");
			}
		}

		public string Number
		{
			get
			{
				return Transfer.Number;
			}
			set
			{
				Transfer.Number = value;
				OnPropertyChanged("Number");
			}
		}

		public decimal? Sum
		{
			get
			{
				return Transfer.Sum.HasValue ? Transfer.Sum : 0;
			}
			set
			{
				Transfer.Sum = value;
				OnPropertyChanged("Sum");
			}
		}

		public string Comment
		{
			get
			{
				return Transfer.Comment;
			}
			set
			{
				Transfer.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		private bool CanExecuteSelectAccount()
		{
			return !IsReadOnly;
		}

		private void ExecuteSelectAccount()
		{
			var filter = new PortfolioIdentifier.PortfolioPBAParams { CurrencyName = new[] { "рубли" } };
			//filter.PortfolioType = new string[] { PortfolioIdentifier.SPN.ToLower() };
			var selected = DialogHelper.SelectPortfolio(filter);

			if (selected != null)
			{
				Account = selected;
			}
		}


		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			DataContainerFacade.Delete(Transfer);

			base.ExecuteDelete(DocOperation.Delete);
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
					DataContainerFacade.Save<Transfer, long>(Transfer);
					break;
				case ViewModelState.Create:
					ID = DataContainerFacade.Save<Transfer, long>(Transfer);
					Transfer.ID = ID;
					break;
			}
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}
				
		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "Date": return Date.ValidateRequired();
					case "DIDate": return DIDate.ValidateRequired();
					case "Number": return Number.ValidateRequired() ?? Number.ValidateMaxLength(32);
					case "Comment": return Comment.ValidateMaxLength(512);
					case "Sum": return Sum.ValidateRequired() ?? Sum.ValidateMaxFormat();
					default: return null;
				}
			}
		}

		private bool Validate()
		{
			return ValidateFields() &&
				   Account != null;
		}

		public void LoadDirection(long lID)
		{
			SelectedDirection = DirectionsList.Find(dir => dir.ID == lID);
		}

		public void Load(long lID)
		{
			try
			{
				ID = lID;
				Transfer = DataContainerFacade.GetByID<Transfer, long>(lID);

				if (Transfer.DirectionID.HasValue)
					LoadDirection(Transfer.DirectionID.Value);

				if (Transfer.PFRBankAccountID.HasValue && Transfer.PortfolioID.HasValue)
					Account = BLServiceSystem.Client.GetPortfolioFull(Transfer.PFRBankAccountID.Value, Transfer.PortfolioID.Value);
			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				throw new ViewModelInitializeException();
			}
		}

		public TransferSchilsViewModel()
			: base(typeof(SchilsCostsListViewModel))
		{
			DataObjectTypeForJournal = typeof(Transfer);
			DirectionsList = DataContainerFacade.GetList<Direction>();

			_selectAccount = new DelegateCommand(o => CanExecuteSelectAccount(),
				o => ExecuteSelectAccount());

			Transfer = new Transfer();

			SelectedDirection = DirectionsList.FirstOrDefault();
			SelectedRecipient = RecipientsList.FirstOrDefault();

			DIDate = DateTime.Now;
			Date = DateTime.Now;

			RefreshConnectedCardsViewModels = RefreshParentViewModel;
		}

		private static void RefreshParentViewModel()
		{
			if (GetViewModel == null) return;
			var vm = GetViewModel(typeof(DirectionViewModel)) as DirectionViewModel;
			if (vm != null)
				vm.RefreshTransfers();
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Transfer), Transfer.ID) };
		}
	}
}
