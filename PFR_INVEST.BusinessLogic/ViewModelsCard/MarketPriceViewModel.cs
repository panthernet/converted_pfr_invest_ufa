﻿using System;
using System.Collections.Generic;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Client.ObjectsExtensions;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class MarketPriceViewModel : ViewModelCard, IUpdateRaisingModel
    {
        #region Properties

        SecurityViewModel parentModel;
        
        private MarketPrice _MarketPrice;
        //public MarketPrice MarketPrice
        //{
        //    get { return _MarketPrice; }
        //    set
        //    {
        //        _MarketPrice = value;
        //        OnPropertyChanged("MarketPrice");
        //    }
        //}

        public DateTime? MarketPriceDate
        {
            get { return _MarketPrice.Date; }
            set
            {
                _MarketPrice.Date = value;
                OnPropertyChanged("MarketPriceDate");
            }
        }

        public decimal? WeightedAveragePrice
        {
            get { return _MarketPrice.WeightedAveragePrice ?? 0; }
            set
            {
                _MarketPrice.WeightedAveragePrice = value;
                OnPropertyChanged("WeightedAveragePrice");
            }
        }

        public decimal? Price
        {
            get { return _MarketPrice.Price ?? 0; }
            set
            {
                _MarketPrice.Price = value;
                OnPropertyChanged("Price");
            }
        }

        public decimal? RepaymentProfit
        {
            get { return _MarketPrice.RepaymentProfit ?? 0; }
            set
            {
                _MarketPrice.RepaymentProfit = value;
                OnPropertyChanged("RepaymentProfit");
            }
        }


        public decimal? CloseAsk
        {
            get { return _MarketPrice.CloseAsk ?? 0; }
            set
            {
                _MarketPrice.CloseAsk = value;
                OnPropertyChanged("CloseAsk");
            }
        }

        public decimal? CloseBid
        {
            get { return _MarketPrice.CloseBid ?? 0; }
            set
            {
                _MarketPrice.CloseBid = value;
                OnPropertyChanged("CloseBid");
            }
        }

        public decimal? YtmAsk
        {
            get { return _MarketPrice.YtmAsk ?? 0; }
            set
            {
                _MarketPrice.YtmAsk = value;
                OnPropertyChanged("YtmAsk");
            }
        }

        public decimal? YtmBid
        {
            get { return _MarketPrice.YtmBid ?? 0; }
            set
            {
                _MarketPrice.YtmBid = value;
                OnPropertyChanged("YtmBid");
            }
        }
        public decimal? Profit
        {
            get { return _MarketPrice.Profit ?? 0; }
            set
            {
                _MarketPrice.Profit = value;
                OnPropertyChanged("Profit");
            }
        }
        public decimal? CouponProfit
        {
            get { return _MarketPrice.CouponProfit ?? 0; }
            set
            {
                _MarketPrice.CouponProfit = value;
                OnPropertyChanged("CouponProfit");
            }
        }


        private Security _CB;
        public Security CB
        {
            get { return _CB; }
            set
            {
                _CB = value;
                OnPropertyChanged("CB");
            }
        }

        public Visibility RubVisible => CB != null && CurrencyIdentifier.IsRUB(CB.CurrencyName) ? Visibility.Visible : Visibility.Collapsed;

        public Visibility CurrencyVisible => CB != null && !CurrencyIdentifier.IsRUB(CB.CurrencyName) ? Visibility.Visible : Visibility.Collapsed;

        #endregion

        #region Execute implementation
        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    ID = _MarketPrice.ID = DataContainerFacade.Save<MarketPrice, long>(_MarketPrice);
                    if (parentModel != null)
                        parentModel.RefreshLists();
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && DeleteAccessAllowed;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(_MarketPrice);
            base.ExecuteDelete(DocOperation.Delete);
            if (parentModel!=null)
            {
                parentModel.SelectedMP = null;
                parentModel.RefreshLists();
            }
        }
        #endregion

        #region Validation

        string s_error = "Неправильный формат данных либо значение вне пределов диапазона!";

        public override string this[string columnName]
        {
            get
            {
                bool fail = false;
                switch (columnName)
                {
                    case "MarketPriceDate": fail = !_MarketPrice.Date.HasValue; break;
                    case "WeightedAveragePrice": fail = !_MarketPrice.WeightedAveragePrice.HasValue || _MarketPrice.WeightedAveragePrice.Value <= 0; break;
                    case "Price": fail = !_MarketPrice.Price.HasValue || _MarketPrice.Price.Value <= 0; break;
                    case "RepaymentProfit": fail = !_MarketPrice.RepaymentProfit.HasValue || _MarketPrice.RepaymentProfit.Value <= 0 ; break;
                    case "CloseAsk": fail = !_MarketPrice.CloseAsk.HasValue || _MarketPrice.CloseAsk.Value <= 0; break;
                    case "CloseBid": fail = !_MarketPrice.CloseBid.HasValue || _MarketPrice.CloseBid.Value <= 0; break;
                    case "YtmAsk": fail = !_MarketPrice.YtmAsk.HasValue || _MarketPrice.YtmAsk.Value <= 0; break;
                    case "YtmBid": fail = !_MarketPrice.YtmBid.HasValue || _MarketPrice.YtmBid.Value <= 0; break;                   
                    case "CouponProfit": fail = !_MarketPrice.CouponProfit.HasValue || _MarketPrice.CouponProfit.Value <= 0; break;

                    default: break;
                }

                return fail ? s_error : string.Empty;
            }
        }

        private readonly static string[] notNullFields = {
        "MarketPriceDate|WeightedAveragePrice|Price|RepaymentProfit|CouponProfit",
        "MarketPriceDate|CloseAsk|CloseBid|YtmAsk|YtmBid|RepaymentProfit|CouponProfit"
                                                         };
        private string Validate()
        {
            int idx = CurrencyIdentifier.IsRUB(CB.CurrencyName) ? 0 : 1;
            bool b_valid = true;
            foreach (string key in notNullFields[idx].Split('|'))
            {
                b_valid = b_valid & string.IsNullOrEmpty(this[key]);
            }
            return b_valid ? string.Empty : s_error;
        }
        #endregion

        #region Load
        /// <summary>
        /// Загрузка данных об страховых взносах умерших
        /// </summary>
        /// <param name="lId">ID страховых взносов умерших в базе</param>
        public void Load(long lId)
        {
            try
            {
                ID = lId;
                _MarketPrice = DataContainerFacade.GetByID<MarketPrice, long>(lId);
                CB = _MarketPrice.GetSecurity();
                Register();

                OnPropertyChanged("RubVisible");
                OnPropertyChanged("CurrencyVisible");
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        public MarketPriceViewModel(long secID)
            : base(typeof(MarketPricesListViewModel))
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
            ID = secID;
            CB = DataContainerFacade.GetByID<Security, long>(secID);
            _MarketPrice = new MarketPrice { SecurityId = CB.ID, Date = DateTime.Now };
            Register();
        }

        public MarketPriceViewModel()
            : base(typeof(MarketPricesListViewModel))
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
            _MarketPrice = new MarketPrice();
            Register();
        }

        void Register()
        {
            _MarketPrice.PropertyChanged += (sender, e) => { IsDataChanged = true; };
        }

        public MarketPriceViewModel(Type p_ListViewModel)
            : base(p_ListViewModel)
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
            _MarketPrice = new MarketPrice();
            Register();
        }
        public MarketPriceViewModel(IEnumerable<Type> p_ListViewModelCollection)
            : base(p_ListViewModelCollection)
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
            _MarketPrice = new MarketPrice();
            Register();
        }
        public MarketPriceViewModel(SecurityViewModel model, long identity)
        {
            DataObjectTypeForJournal = typeof(MarketPrice);
            ID = model.ID;
            CB = model.CB;
            parentModel = model;if (identity > 0)
                _MarketPrice = DataContainerFacade.GetByID<MarketPrice>(identity);
            else
                _MarketPrice = new MarketPrice { SecurityId = CB.ID, Date = DateTime.Now };
            Register();
        }



		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>>
			{ 
				new KeyValuePair<Type, long>(typeof(MarketPrice), _MarketPrice.ID)
			};
		}
	}
}