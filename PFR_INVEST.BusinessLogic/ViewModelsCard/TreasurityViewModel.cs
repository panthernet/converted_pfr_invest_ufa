﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.User)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public sealed class TreasurityViewModel : ViewModelCard, IUpdateRaisingModel
	{
		private readonly KDoc _treasurity;

		private readonly string _mPFName;

		// в суммах участвуют до 15 слагаемых, то есть -2 цифры от максимума в БД DECIMAL(19,4), то есть 13 цифр
		private const decimal MAX_ADD = 9999999999999;

		public bool ReadAccessOnly => IsReadOnly;

	    public bool EnabledState => !ReadAccessOnly;


	    #region MonthsList

		public long? SelectedPeriodID
		{
			get
			{
				return _treasurity.ReportPeriodID;
			}
			set
			{
				if (_treasurity.ReportPeriodID != value)
				{
					_treasurity.ReportPeriodID = value;
					CheckDate = CheckDate;//Для обновления формата
					OnPropertyChanged("SelectedMonth");
				}
			}
		}


		public List<Element> PeriodList { get; protected set; }
		#endregion

		#region Portfolios
		private List<Portfolio> _mSPNPortfolios;
		public List<Portfolio> Portfolios
		{
			get
			{
				return _mSPNPortfolios;
			}
			set
			{
				_mSPNPortfolios = value;
				OnPropertyChanged("Portfolios");
			}
		}


		public long? SelectedPortfolioID
		{
			get
			{
				return _treasurity.PortfolioID;
			}
			set
			{
				_treasurity.PortfolioID = value;
				OnPropertyChanged("SelectedPortfolioID");
			}
		}

		#endregion

		#region Dates & Regnum
		public string Regnum
		{
			get
			{
				return _treasurity.RegNum;
			}
			set
			{
				if (_treasurity.RegNum != value)
				{
					_treasurity.RegNum = value;
					OnPropertyChanged("Regnum");
				}
			}
		}

		public DateTime? OperationDate
		{
			get
			{
				return _treasurity.OperationDate;
			}
			set
			{
				_treasurity.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		public DateTime? DocDate
		{
			get
			{
				return _treasurity.DocDate;
			}
			set
			{
				_treasurity.DocDate = value;
				OnPropertyChanged("DocDate");
			}
		}

		public DateTime? CheckDate
		{
			get
			{
				return _treasurity.CheckDate;
			}
			set
			{

				_treasurity.CheckDate = FormatCheckDate(value);
				OnPropertyChanged("CheckDate");
			}
		}

		private DateTime? FormatCheckDate(DateTime? date)
		{
			if (!date.HasValue) return null;

			if (_treasurity.IsMonthDocument)
				return new DateTime(date.Value.Year, date.Value.Month, 1);
		    return _treasurity.IsYearDocument ? new DateTime(date.Value.Year, 1, 1) : date;
		}

		#endregion

		#region Total
		private void RefreshTotalPenny()
		{
            TotalPenny = Penni1 + Penni2 + Penni3 + Penni4 + Penni5 + Penni2Proc + Penni3Proc + Penni4Proc + Penni1Proc + 
						+ Proc1 + Proc2 + Proc3 + Proc4 + Proc5
						+ Pay1 + Pay2 + Pay3 + Pay4 + Pay5;
		}

		private void RefreshTotalPayment()
		{
			TotalPayment = Summ1 + Summ2 + Summ3 + Summ4 + Summ5
							+ Other1 + Other2 + Other3 + Other4 + Other5
							+ Fix;
		}


		public decimal TotalPenny
		{
			get
			{
				return _treasurity.TotalPenny;
			}
			set
			{
				if (_treasurity.TotalPenny != value)
				{
					_treasurity.TotalPenny = value;
					OnPropertyChanged("TotalPenny");
				}
			}
		}


		public decimal TotalPayment
		{
			get
			{
				return _treasurity.TotalSumm;
			}
			set
			{
			    if (_treasurity.TotalSumm == value) return;
			    _treasurity.TotalSumm = value;
			    OnPropertyChanged("TotalPayment");
			}
		}
		#endregion

		#region 1
		private void RefreshKBK1()
		{
            KBK1 = Summ1 + Other1 + Penni1 + Pay1 + Proc1 + Penni1Proc;
		}

		private decimal _mKBK1;
		public decimal KBK1
		{
			get
			{
				return _mKBK1;
			}
			set
			{
				if (_mKBK1 != value)
				{
					_mKBK1 = value;
					OnPropertyChanged("KBK1");
				}
			}
		}

		public decimal Summ1
		{
			get
			{
				return _treasurity.Summ1;
			}
			set
			{
			    if (_treasurity.Summ1 == value) return;
			    _treasurity.Summ1 = value;
			    RefreshKBK1();
			    RefreshTotalPayment();
			    OnPropertyChanged("Summ1");
			}
		}

	    public decimal Penni1Proc
	    {
	        get
	        {
                return _treasurity.Penni1Proc;
	        }
	        set
	        {
                if (_treasurity.Penni1Proc != value)
                {
                    _treasurity.Penni1Proc = value;
                    RefreshKBK1();
                    RefreshTotalPenny();
                    OnPropertyChanged("Penni1Proc");
                }  
	        }
	    }

		public decimal Penni1
		{
			get
			{
				return _treasurity.Penni1;
			}
			set
			{
				if (_treasurity.Penni1 != value)
				{
					_treasurity.Penni1 = value;
					RefreshKBK1();
					RefreshTotalPenny();
					OnPropertyChanged("Penni1");
				}
			}
		}

		public decimal Pay1
		{
			get
			{
				return _treasurity.Pay1;
			}
			set
			{
			    if (_treasurity.Pay1 == value) return;
			    _treasurity.Pay1 = value;
			    RefreshKBK1();
			    RefreshTotalPenny();
			    OnPropertyChanged("Pay1");
			}
		}

		public decimal Other1
		{
			get
			{
				return _treasurity.Other1;
			}
			set
			{
			    if (_treasurity.Other1 == value) return;
			    _treasurity.Other1 = value;
			    RefreshKBK1();
			    RefreshTotalPayment();
			    OnPropertyChanged("Other1");
			}
		}

		public decimal Proc1
		{
			get
			{
				return _treasurity.Proc1;
			}
			set
			{
			    if (_treasurity.Proc1 == value) return;
			    _treasurity.Proc1 = value;
			    RefreshKBK1();
			    RefreshTotalPenny();
			    OnPropertyChanged("Proc1");
			}
		}
		#endregion

		#region 2
		private void RefreshKBK2()
		{
            KBK2 = Summ2 + Other2 + Proc2 + Penni2 + Pay2 + Penni2Proc;
		}

        public decimal Penni2Proc
        {
            get
            {
                return _treasurity.Penni2Proc;
            }
            set
            {
                if (_treasurity.Penni2Proc != value)
                {
                    _treasurity.Penni2Proc = value;
                    RefreshKBK2();
                    RefreshTotalPenny();
                    OnPropertyChanged("Penni2Proc");
                }
            }
        }

		private decimal _mKBK2;
		public decimal KBK2
		{
			get
			{
				return _mKBK2;
			}
			set
			{
			    if (_mKBK2 == value) return;
			    _mKBK2 = value;
			    OnPropertyChanged("KBK2");
			}
		}

		public decimal Summ2
		{
			get
			{
				return _treasurity.Summ2;
			}
			set
			{
			    if (_treasurity.Summ2 == value) return;
			    _treasurity.Summ2 = value;
			    RefreshKBK2();
			    RefreshTotalPayment();
			    OnPropertyChanged("Summ2");
			}
		}

		public decimal Penni2
		{
			get
			{
				return _treasurity.Penni2;
			}
			set
			{
			    if (_treasurity.Penni2 == value) return;
			    _treasurity.Penni2 = value;
			    RefreshKBK2();
			    RefreshTotalPenny();
			    OnPropertyChanged("Penni2");
			}
		}

		public decimal Pay2
		{
			get
			{
				return _treasurity.Pay2;
			}
			set
			{
			    if (_treasurity.Pay2 == value) return;
			    _treasurity.Pay2 = value;
			    RefreshKBK2();
			    RefreshTotalPenny();
			    OnPropertyChanged("Pay2");
			}
		}

		public decimal Other2
		{
			get
			{
				return _treasurity.Other2;
			}
			set
			{
			    if (_treasurity.Other2 == value) return;
			    _treasurity.Other2 = value;
			    RefreshKBK2();
			    RefreshTotalPayment();
			    OnPropertyChanged("Other2");
			}
		}

		public decimal Proc2
		{
			get
			{
				return _treasurity.Proc2;
			}
			set
			{
			    if (_treasurity.Proc2 == value) return;
			    _treasurity.Proc2 = value;
			    RefreshKBK2();
			    RefreshTotalPenny();
			    OnPropertyChanged("Proc2");
			}
		}
		#endregion

		#region Fix
		public decimal Fix
		{
			get
			{
				return _treasurity.Fix;
			}
			set
			{
			    if (_treasurity.Fix == value) return;
			    _treasurity.Fix = value;
			    RefreshTotalPayment();
			    OnPropertyChanged("Fix");
			}
		}
		#endregion

		#region 3
		private void RefreshBudgetPFR()
		{
            BudgetPFR = Summ3 + Other3 + Penni3 + Pay3 + Proc3 + Penni3Proc;
		}

        public decimal Penni3Proc
        {
            get
            {
                return _treasurity.Penni3Proc;
            }
            set
            {
                if (_treasurity.Penni3Proc != value)
                {
                    _treasurity.Penni3Proc = value;
                    RefreshBudgetPFR();
                    RefreshTotalPenny();
                    OnPropertyChanged("Penni3Proc");
                }
            }
        }

		private decimal _mBudgetPFR;
		public decimal BudgetPFR
		{
			get
			{
				return _mBudgetPFR;
			}
			set
			{
			    if (_mBudgetPFR == value) return;
			    _mBudgetPFR = value;
			    OnPropertyChanged("BudgetPFR");
			}
		}

		public decimal Summ3
		{
			get
			{
				return _treasurity.Summ3;
			}
			set
			{
			    if (_treasurity.Summ3 == value) return;
			    _treasurity.Summ3 = value;
			    RefreshBudgetPFR();
			    RefreshTotalPayment();
			    OnPropertyChanged("Summ3");
			}
		}

		public decimal Penni3
		{
			get
			{
				return _treasurity.Penni3;
			}
			set
			{
			    if (_treasurity.Penni3 == value) return;
			    _treasurity.Penni3 = value;
			    RefreshBudgetPFR();
			    RefreshTotalPenny();
			    OnPropertyChanged("Penni3");
			}
		}

		public decimal Pay3
		{
			get
			{
				return _treasurity.Pay3;
			}
			set
			{
			    if (_treasurity.Pay3 == value) return;
			    _treasurity.Pay3 = value;
			    RefreshBudgetPFR();
			    RefreshTotalPenny();
			    OnPropertyChanged("Pay3");
			}
		}

		public decimal Other3
		{
			get
			{
				return _treasurity.Other3;
			}
			set
			{
			    if (_treasurity.Other3 == value) return;
			    _treasurity.Other3 = value;
			    RefreshBudgetPFR();
			    RefreshTotalPayment();
			    OnPropertyChanged("Other3");
			}
		}

		public decimal Proc3
		{
			get
			{
				return _treasurity.Proc3;
			}
			set
			{
			    if (_treasurity.Proc3 == value) return;
			    _treasurity.Proc3 = value;
			    RefreshBudgetPFR();
			    RefreshTotalPenny();
			    OnPropertyChanged("Proc3");
			}
		}
		#endregion

		#region 4
		private void RefreshWholeYear()
		{
            WholeYear = Summ4 + Other4 + Penni4 + Pay4 + Proc4 + Penni4Proc;
		}

        public decimal Penni4Proc
        {
            get
            {
                return _treasurity.Penni4Proc;
            }
            set
            {
                if (_treasurity.Penni4Proc == value) return;
                _treasurity.Penni4Proc = value;
                RefreshWholeYear();
                RefreshTotalPenny();
                OnPropertyChanged("Penni4Proc");
            }
        }

		private decimal _mWholeYear;
		public decimal WholeYear
		{
			get
			{
				return _mWholeYear;
			}
			set
			{
			    if (_mWholeYear == value) return;
			    _mWholeYear = value;
			    OnPropertyChanged("WholeYear");
			}
		}

		public decimal Summ4
		{
			get
			{
				return _treasurity.Summ4;
			}
			set
			{
			    if (_treasurity.Summ4 == value) return;
			    _treasurity.Summ4 = value;
			    RefreshWholeYear();
			    RefreshTotalPayment();
			    OnPropertyChanged("Summ4");
			}
		}

		public decimal Penni4
		{
			get
			{
				return _treasurity.Penni4;
			}
			set
			{
			    if (_treasurity.Penni4 == value) return;
			    _treasurity.Penni4 = value;
			    RefreshWholeYear();
			    RefreshTotalPenny();
			    OnPropertyChanged("Penni4");
			}
		}

		public decimal Pay4
		{
			get
			{
				return _treasurity.Pay4;
			}
			set
			{
			    if (_treasurity.Pay4 == value) return;
			    _treasurity.Pay4 = value;
			    RefreshWholeYear();
			    RefreshTotalPenny();
			    OnPropertyChanged("Pay4");
			}
		}

		public decimal Other4
		{
			get
			{
				return _treasurity.Other4;
			}
			set
			{
			    if (_treasurity.Other4 == value) return;
			    _treasurity.Other4 = value;
			    RefreshWholeYear();
			    RefreshTotalPayment();
			    OnPropertyChanged("Other4");
			}
		}

		public decimal Proc4
		{
			get
			{
				return _treasurity.Proc4;
			}
			set
			{
			    if (_treasurity.Proc4 == value) return;
			    _treasurity.Proc4 = value;
			    RefreshWholeYear();
			    RefreshTotalPenny();
			    OnPropertyChanged("Proc4");
			}
		}
		#endregion

		#region 5
		private void RefreshTotal5()
		{
			Total5 = Summ5 + Other5 + Penni5 + Pay5 + Proc5;
		}

		private decimal _mTotal5;
		public decimal Total5
		{
			get
			{
				return _mTotal5;
			}
			set
			{
			    if (_mTotal5 == value) return;
			    _mTotal5 = value;
			    OnPropertyChanged("Total5");
			}
		}

		public decimal Summ5
		{
			get
			{
				return _treasurity.Summ5;
			}
			set
			{
			    if (_treasurity.Summ5 == value) return;
			    _treasurity.Summ5 = value;
			    RefreshTotal5();
			    RefreshTotalPayment();
			    OnPropertyChanged("Summ5");
			}
		}

		public decimal Penni5
		{
			get
			{
				return _treasurity.Penni5;
			}
			set
			{
			    if (_treasurity.Penni5 == value) return;
			    _treasurity.Penni5 = value;
			    RefreshTotal5();
			    RefreshTotalPenny();
			    OnPropertyChanged("Penni5");
			}
		}

		public decimal Pay5
		{
			get
			{
				return _treasurity.Pay5;
			}
			set
			{
			    if (_treasurity.Pay5 == value) return;
			    _treasurity.Pay5 = value;
			    RefreshTotal5();
			    RefreshTotalPenny();
			    OnPropertyChanged("Pay5");
			}
		}

		public decimal Other5
		{
			get
			{
				return _treasurity.Other5;
			}
			set
			{
			    if (_treasurity.Other5 == value) return;
			    _treasurity.Other5 = value;
			    RefreshTotal5();
			    RefreshTotalPayment();
			    OnPropertyChanged("Other5");
			}
		}

		public decimal Proc5
		{
			get
			{
				return _treasurity.Proc5;
			}
			set
			{
			    if (_treasurity.Proc5 == value) return;
			    _treasurity.Proc5 = value;
			    RefreshTotal5();
			    RefreshTotalPenny();
			    OnPropertyChanged("Proc5");
			}
		}
		#endregion


		public override bool IsReadOnly => State != ViewModelState.Create;

	    public bool IsDeleteEnable { get; private set; }

		#region PortfolioAcc

		public string PfAccount => SelectedPortfolioBankAccount != null ? SelectedPortfolioBankAccount.AccountNumber : null;

	    public string PfPortfolio => SelectedPortfolioBankAccount != null ? SelectedPortfolioBankAccount.PFName : _mPFName;

	    private PortfolioFullListItem _selectedPortfolioBankAccount;
		public PortfolioFullListItem SelectedPortfolioBankAccount
		{
			get { return _selectedPortfolioBankAccount; }
			set
			{
				_selectedPortfolioBankAccount = value;
				if (value != null)
				{
					_treasurity.PortfolioPFRBankAccountID = value.ID;
					SelectedPortfolioID = value.PortfolioID;
				}
				else
				{
					_treasurity.PortfolioPFRBankAccountID = null;
					SelectedPortfolioID = null;
				}

				OnPropertyChanged("SelectedPortfolioBankAccount");
				OnPropertyChanged("PfAccount");
				OnPropertyChanged("PfPortfolio");

			}
		}

		public ICommand SelectedPortfolioBankAccountCommand { get; set; }

		private void SelectedPortfolioBankAccountAction(object o)
		{
			PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink param = null;

			if (SelectedPortfolioBankAccount != null && SelectedPortfolioBankAccount.PortfolioPFRBankAccount != null)
			{
				param = new PortfolioIdentifier.PortfolioPBAParams.PortfolioPBAParamsPortfolioAccountLink
				{
					PfrBankAccountID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.PFRBankAccountID.Value,
					PortfolioID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.PortfolioID.Value
				};
			}

			PortfolioIdentifier.PortfolioPBAParams p = new PortfolioIdentifier.PortfolioPBAParams();
			p.AccountType = new[] { "текущий" };
			p.ContragentContains = new[] { "цб" };
			p.SelectedPortfolioID = SelectedPortfolioID;
			//Все кроме ДСВ
			p.PortfolioTypeFilter = new PortfolioIdentifier.PortfolioPBAParams.PortfolioFilter(true, (long)Portfolio.Types.DSV);

			if (param != null)
			{
				p.HiddenPortfolioAccountLinks = new[] { param };

			}

			var portfolioAccount = DialogHelper.SelectPortfolio(p);

			if (portfolioAccount != null)
			{
				SelectedPortfolioBankAccount = portfolioAccount;				
				//this.Report.TotalAccountPfBaccID = SelectedPortfolioBankAccount.PortfolioPFRBankAccount.ID;
			}

		}

		#endregion

		public TreasurityViewModel(ViewModelState action, long id, Type itemType)
			: base(
				//typeof(DueListViewModel), 
			typeof(DKListViewModel))
		{
            DataObjectTypeForJournal = typeof(KDoc);
            State = action;

			SelectedPortfolioBankAccountCommand = new DelegateCommand(o => !IsReadOnly, SelectedPortfolioBankAccountAction);

			PeriodList = BLServiceSystem.Client.GetElementByType(Element.Types.KDocPeriod);


			if (State == ViewModelState.Create)
			{
				//В id может быть передан идентификатор одного из типов: AddSPN, DopSPN, Aps, DopAps, из которого надо попытаться выбрать портфель и счет
				_treasurity = new KDoc();

                long? accountID = null, portfolioID = null;
                DateTime? chDate = null;

                if (itemType == typeof(AddSPN))
                {
                    var spn = DataContainerFacade.GetByID<AddSPN>(id);

                    if (spn != null)
                    {
                        accountID = spn.PFRBankAccountID;
                        portfolioID = spn.PortfolioID;
                    }

                    //Выставляем отчётную дату периода, для периода из СВ

                    if (spn.YearID.HasValue && spn.MonthID.HasValue)
                    {
                        chDate = new DateTime(2000 + (int)spn.YearID, (int)spn.MonthID, 1).AddMonths(1);
                    }
                    else if (spn.MonthID.HasValue && spn.NewDate.HasValue)
                    {
                        chDate = new DateTime(spn.NewDate.Value.Year, (int)spn.MonthID, 1).AddMonths(1);
                    }
                }
                else if (itemType == typeof(DopSPN))
                {
                    var dspn = DataContainerFacade.GetByID<DopSPN>(id);

                    AddSPN rel = null;
                    if (dspn != null && dspn.AddSpnID.HasValue)
                    {
                        rel = DataContainerFacade.GetByID<AddSPN>(dspn.AddSpnID.Value);
                    }

                    if (dspn != null)
                    {
                        portfolioID = dspn.PortfolioID;
                    }

                    accountID = rel != null ? rel.PFRBankAccountID : dspn.QuarterPfrBankAccountID;

                    //chDate = new DateTime(2000 + (int)dspn.YearID, (int)dspn.MonthID, 1).AddMonths(1);
                }
                else if (itemType == typeof(Aps))
                {
                    var aps = DataContainerFacade.GetByID<Aps>(id);
                    if (aps != null)
                    {
                        accountID = aps.PFRBankAccountID;
                        portfolioID = aps.PortfolioID;
                    }

                    //chDate = new DateTime(2000 + (int)aps.YearID, (int)aps.MonthID, 1).AddMonths(1);
                }
                else if (itemType == typeof(DopAps))
                {
                    var daps = DataContainerFacade.GetByID<DopAps>(id);
                    if (daps != null)
                    {
                        accountID = daps.PFRBankAccountID;
                        portfolioID = daps.PortfolioID;
                    }

                    //chDate = new DateTime(2000 + (int)daps.YearID, (int)daps.MonthID, 1).AddMonths(1);
                }
                else if (itemType == typeof(KDoc))
                {
                    var dk = DataContainerFacade.GetByID<KDoc>(id);
                    if (dk != null && dk.PortfolioPFRBankAccountID.HasValue)
                    {
                        //accountID = dk.PortfolioPFRBankAccountID;
                        //portfolioID = dk.PortfolioID;
                        SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFullByPfBaccID(dk.PortfolioPFRBankAccountID.Value);
                    }

                    chDate = dk.CheckDate;
                }

                if (accountID.HasValue && portfolioID.HasValue)
                    SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFull(accountID.Value, portfolioID.Value);
                //else if (portfolioID.HasValue)
                //{
                //    //Для новых записей показываем имя портфеля, если не найден счёт-портфель
                //    SelectedPortfolioID = portfolioID;
                //    m_pfName = DataContainerFacade.GetByID<Portfolio>(portfolioID).Year;
                //}

                CheckDate = chDate;


			}
			else
			{
				ID = id;
				_treasurity = DataContainerFacade.GetByID<KDoc>(ID);
				RefreshKBK1();
				RefreshKBK2();
				RefreshBudgetPFR();
				RefreshWholeYear();
                RefreshTotal5();
				RefreshTotalPenny();
				RefreshTotalPayment();

				if (_treasurity.PortfolioPFRBankAccountID.HasValue)
					SelectedPortfolioBankAccount = BLServiceSystem.Client.GetPortfolioFullByPfBaccID(_treasurity.PortfolioPFRBankAccountID.Value);
				else
				{
					//Для импортированых записей у которых есть только портфель
					var p = DataContainerFacade.GetByID<Portfolio>(_treasurity.PortfolioID);
					if (p != null)
						_mPFName = p.Year;
				}
				UpdateReadOnly();
			}

			_mAllDecimalProperties = GetType().GetProperties().Where(prop => prop.PropertyType == typeof(decimal));
		}

	    private void UpdateReadOnly()
		{
			IsDeleteEnable = !BLServiceSystem.Client.IsKDocClosed(_treasurity.ID);
			OnPropertyChanged("IsReadOnly");
			OnPropertyChanged("ReadAccessOnly");

		}

		protected override bool BeforeExecuteSaveCheck()
		{
			var res = BLServiceSystem.Client.CheckNewKDoc(_treasurity);

			if (!res.IsSuccess)
			{
				DialogHelper.ShowAlert(res.ErrorMessage);
				return false;
			}

			return true;
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Create:
				case ViewModelState.Edit:
					ID = BLServiceSystem.Client.SaveKDoc(_treasurity);
					//ID = DataContainerFacade.Save<KDoc>(Treasurity);
					_treasurity.ID = ID;
					break;
			}
		}

		protected override void OnCardSaved()
		{
			base.OnCardSaved();
			UpdateReadOnly();
			IsDataChanged = false;
		}
		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit && IsDeleteEnable;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			BLServiceSystem.Client.DeleteKDoc(_treasurity);
			//DataContainerFacade.Delete<KDoc>(Treasurity);
			base.ExecuteDelete(DocOperation.Delete);
		}

		private readonly IEnumerable<PropertyInfo> _mAllDecimalProperties;
		private bool Validate()
		{
			if (_mAllDecimalProperties.Any(pi => !string.IsNullOrEmpty(this[pi.Name])))
			{
			    return false;
			}

		    return
		        ("Regnum|SelectedPortfolioID|SelectedPeriodID|CheckDate|DocDate|SelectedPortfolioBankAccount|PfAccount" +
                 "Penni1Proc|Penni2Proc|Penni3Proc|Penni4Proc|Penni1|Penni2|Penni3|Penni4|Penni5|Proc1|Proc2|Proc3|Proc4|Proc5|Pay1|Pay2|Pay3|Pay4|Pay5|Summ1|Summ2|Summ3|Summ4|Summ5|Other1|Other2|Other3|Other4|Other5|Fix").Split('|')
		            .All(f => string.IsNullOrEmpty(this[f]));
		}

		private const string ERROR_INVALID_VALUE = "Введите значение";
		private const string MAX_VALUE_EXCEED = "Превышено допустимое значение";

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "Regnum": return string.IsNullOrWhiteSpace(Regnum) ? ERROR_INVALID_VALUE : null;
					case "SelectedPortfolioID": return SelectedPortfolioID.ValidateRequired();
					case "SelectedPeriodID": return SelectedPeriodID.ValidateRequired();
					case "CheckDate": return CheckDate.ValidateRequired() ?? ((CheckDate != FormatCheckDate(CheckDate)) ? "Неверная отчётная дата" : null);
					case "DocDate": return DocDate.ValidateRequired();
					case "SelectedPortfolioBankAccount": return SelectedPortfolioBankAccount.ValidateRequired();
					case "PfAccount": return SelectedPortfolioBankAccount.ValidateRequired();
					case "Penni1": return CheckAddSumm(Penni1);
					case "Penni2": return CheckAddSumm(Penni2);
					case "Penni3": return CheckAddSumm(Penni3);
					case "Penni4": return CheckAddSumm(Penni4);
					case "Penni5": return CheckAddSumm(Penni5);
					case "Proc1": return CheckAddSumm(Proc1);
					case "Proc2": return CheckAddSumm(Proc2);
					case "Proc3": return CheckAddSumm(Proc3);
					case "Proc4": return CheckAddSumm(Proc4);
					case "Proc5": return CheckAddSumm(Proc5);
					case "Pay1": return CheckAddSumm(Pay1);
					case "Pay2": return CheckAddSumm(Pay2);
					case "Pay3": return CheckAddSumm(Pay3);
					case "Pay4": return CheckAddSumm(Pay4);
					case "Pay5": return CheckAddSumm(Pay5);
					case "Summ1": return CheckAddSumm(Summ1);
					case "Summ2": return CheckAddSumm(Summ2);
					case "Summ3": return CheckAddSumm(Summ3);
					case "Summ4": return CheckAddSumm(Summ4);
					case "Summ5": return CheckAddSumm(Summ5);
					case "Other1": return CheckAddSumm(Other1);
					case "Other2": return CheckAddSumm(Other2);
					case "Other3": return CheckAddSumm(Other3);
					case "Other4": return CheckAddSumm(Other4);
					case "Other5": return CheckAddSumm(Other5);
				}

			    if ((KDoc.DocVersion) _treasurity.DocType >= KDoc.DocVersion.Version3)
			    {
			        switch (columnName)
			        {
                        case "Penni1Proc": return CheckAddSumm(Penni1Proc);
                        case "Penni2Proc": return CheckAddSumm(Penni2Proc);
                        case "Penni3Proc": return CheckAddSumm(Penni3Proc);
                        case "Penni4Proc": return CheckAddSumm(Penni4Proc);
			        }
			    }

				//Проверка всех денежных полей на не минусовое значение
				//PropertyInfo pi = m_AllDecimalProperties.FirstOrDefault(prop => prop.Name == columnName);
				//if (pi != null)
				//{
				//    return ((decimal)pi.GetValue(this, null)).ValidateNonNegative();
				//}
				return null;
			}
		}

		private string CheckAddSumm(decimal v)
		{
			return Math.Abs(v) > MAX_ADD ? MAX_VALUE_EXCEED : null;
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			// метод SaveKDoc вместе с KDoc сохраняет набор DopSPN, который тоже требуется обновить в гридах
			var dspn = DataContainerFacade.GetListByProperty<DopSPN>("KDocID", _treasurity.ID).ToList();
			var aspn = DataContainerFacade.GetListByProperty<AddSPN>("KDocID", _treasurity.ID).ToList();

		    var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (KDoc), _treasurity.ID)};


		    dspn.ForEach(dop =>
			{
				list.Add(new KeyValuePair<Type, long>(typeof(DopSPN), dop.ID));
			});

			aspn.ForEach(a =>
			{
				list.Add(new KeyValuePair<Type, long>(typeof(AddSPN), a.ID));
			});

			//так же обновляем все СВ за месяц. Нужно для корректной работы блокировок добавления Уточнений в СВ

			var sv = DataContainerFacade.GetListByProperty<AddSPN>("ClosingKDocID", _treasurity.ID).ToList();
			sv.ForEach(a =>
			{
				list.Add(new KeyValuePair<Type, long>(typeof(AddSPN), a.ID));
			});

			var svDop = DataContainerFacade.GetListByProperty<DopSPN>("ClosingKDocID", _treasurity.ID).ToList();
			svDop.ForEach(a =>
			{
				list.Add(new KeyValuePair<Type, long>(typeof(DopSPN), a.ID));
			});

			return list;
		}
	}
}
