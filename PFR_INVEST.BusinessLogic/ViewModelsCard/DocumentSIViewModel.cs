﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class DocumentSIViewModel : DocumentBaseViewModel
    {
        public DocumentSIViewModel() : this(0, ViewModelState.Create) { }
        public DocumentSIViewModel(long id, ViewModelState state)
            : base(id, state) { }


        public override Document.Types Type => Document.Types.SI;

        protected override IList<string> GetAdditionalExecutionInfo()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.SIDocumentAdditionalExecutionInfo).Where(x => x.Visible).Select(e => e.Name).ToList();  
        }

        protected override IList<string> GetOriginalStoragePlaces()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.SIDocumentStoragePlaces).Where(x=>x.Visible).Select(e => e.Name).ToList();  
        }

        protected override IList<LegalEntity> GetLegalEntityList()
        {
            return BLServiceSystem.Client.GetActiveUKListHib();
        }

        protected override IList<DocumentClass> GetDocumentClassList()
        {
            return DataContainerFacade.GetList<DocumentClass>();
        }
		public override bool HasExecutorList => true;
        //protected override IList<Person> GetPersonList()
		//{
		//    return DataContainerFacade.GetList<Person>();
		//}

        protected override IList<AdditionalDocumentInfo> GetAdditionalInfoList()
        {
            return new List<AdditionalDocumentInfo>(); 
            //return DataContainerFacade.GetList<AdditionalDocumentInfo>();
        }

        public override bool HasOutgoingNumber => true;
    }
}