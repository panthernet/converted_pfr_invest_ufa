﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	public class ApsAccurateModelBase : ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel, IRequestCloseViewModel
	{
		#region Fields
		/// <summary>
		/// Родительская view-модель (неразнесенные страховые взносы)
		/// </summary>
		private ApsModelBase _parentVM;

		public DopAps Dopaps;

		public decimal? Summ
		{
			get
			{
				return Dopaps == null ? null : Dopaps.Summ;
			}

			set
			{
				if (Dopaps != null)
					Dopaps.Summ = value;
				OnPropertyChanged("ChSumm");
				OnPropertyChanged("Summ");
			}
		}

		public decimal? ChSumm
		{
			get
			{
			    if (_parentVM == null || Dopaps == null) return 0;
			    decimal? basesum = _parentVM.Summ;

			    if (_parentVM.DopAPSs != null && _parentVM.DopAPSs.Count > 0)
			    {
			        //DopSPNListItem prevEnt = null;
			        DopAps prevEnt = null;
			        foreach (var ent in _parentVM.DopAPSs)
			        {
			            if (ent.ID == Dopaps.ID)
			            {
			                basesum = prevEnt == null ? basesum : prevEnt.Summ;
			                break;
			            }
			            prevEnt = ent;
			            basesum = ent == null ? basesum : prevEnt.Summ;
			        }
			    }
			    Dopaps.ChSumm = Summ - basesum;
			    return Dopaps.ChSumm;
			}

			set
			{
				if (Dopaps != null)
					Dopaps.ChSumm = value;
				OnPropertyChanged("ChSumm");
			}
		}

		public DateTime? OperationDate
		{
			get { return Dopaps == null ? null : Dopaps.OperationDate; }
			set
			{
				if (Dopaps != null)
					Dopaps.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		public DateTime? DocDate
		{
			get { return Dopaps == null ? null : Dopaps.Date; }
			set
			{
				if (Dopaps != null)
					Dopaps.Date = value;
				OnPropertyChanged("DocDate");
			}
		}

		public DateTime MaxDate { get; set; }

		public DateTime MinDate
		{
			get
			{
				MaxDate = DateTime.MaxValue;
				int cnt = _parentVM.DopAPSs.Count;

				switch (State)
				{
					case ViewModelState.Create:

						if (cnt == 0)
							return _parentVM.DocDate.HasValue ? _parentVM.DocDate.Value.Date : DateTime.Now.Date;

						cnt--;

						return GetMaxDate(cnt);
					default:
					case ViewModelState.Edit:
						for (int idx = 0; idx < cnt; idx++)
						{
						    if (Dopaps.ID != _parentVM.DopAPSs[idx].ID) continue;
						    if (idx + 1 == cnt)
						        return GetMinDate(idx);
						    MaxDate = GetMaxDate(idx + 1);
						    return GetMinDate(idx);
						}
						break;
				}
				return DocDate ?? DocDate.Value.Date;
			}
		}

		private PortfolioFullListItem _srcAccount;
		/// <summary>
		/// Инвестиционный портфель (счёт)
		/// </summary>
		public PortfolioFullListItem SrcAccount
		{
			get
			{
				return _srcAccount;
			}
			set
			{
				_srcAccount = value;

				if (value != null && Dopaps != null)
				{
					Dopaps.SPortfolioID = value.Portfolio.ID;
					Dopaps.SPFRBankAccountID = value.PfrBankAccount.ID;
				}
				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("Account");
				OnPropertyChanged("SrcAccount");
			}
		}

		private PortfolioFullListItem _dstAccount;
		/// <summary>
		/// Целевой портфель (счёт)
		/// </summary>
		public PortfolioFullListItem DstAccount
		{
			get
			{
				return _dstAccount;
			}
			set
			{
				_dstAccount = value;
				if (value != null && Dopaps != null)
				{
					Dopaps.PortfolioID = value.Portfolio.ID;
					Dopaps.PFRBankAccountID = value.PfrBankAccount.ID;
				}
				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
				OnPropertyChanged("DstAccount");
			}
		}
		public bool UserRoleType => State == ViewModelState.Read;

	    public string RegNum
		{
			get { return Dopaps == null ? null : Dopaps.RegNum; }
			set
			{
				if (Dopaps != null)
					Dopaps.RegNum = value;
				OnPropertyChanged("RegNum");
			}
		}

		public string Comment
		{
			get { return Dopaps == null ? null : Dopaps.Comment; }
			set
			{
				if (Dopaps != null)
					Dopaps.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		#endregion

		#region Execute implementation

		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
			BLServiceSystem.Client.DeleteDopAPSList(new List<long> { ID });
			_parentVM.Load(_parentVM.ID);
			RefreshCards();
			base.ExecuteDelete(DocOperation.Delete);
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
					DataContainerFacade.Save(Dopaps);
					_parentVM.Load(_parentVM.ID);
					break;
				case ViewModelState.Create:
					Dopaps.ApsID = _parentVM.ID;
					Dopaps.ID = DataContainerFacade.Save(Dopaps);
					_parentVM.Load(_parentVM.ID);
					break;
			}
		}
		#endregion

		#region Validation
		public override string this[string columnName]
		{
			get
			{
				const string errorMessage = "Неверный формат данных";
				const string errorDate = "Недопустимое значение";
				if (Dopaps == null || _parentVM == null)
					return errorMessage;

				switch (columnName.ToUpper())
				{
					case "OPERATIONDATE": return !Dopaps.OperationDate.HasValue || Dopaps.OperationDate.Value.Year < 1900 ? errorMessage : null;
					case "DATE": return !Dopaps.Date.HasValue || Dopaps.Date.Value.Year < 1900 ? errorMessage : null;
					case "REGNUM": return string.IsNullOrWhiteSpace(Dopaps.RegNum) ? errorMessage : null;
					case "SUMM": return !Dopaps.Summ.HasValue ? errorMessage : null;
					case "CHSUMM": return !Dopaps.ChSumm.HasValue ? errorMessage : null;
					case "DOCDATE": return !(Dopaps.Date >= MinDate && Dopaps.Date <= MaxDate) ? errorDate : null;
					default: return null;
				}
			}
		}

		private string Validate()
		{
			const string errorMessage = "Неверный формат данных";
			const string validate = "OPERATIONDATE|DATE|REGNUM|SUMM|DOCDATE";//|CHSUMM";

			if ((SrcAccount == null) || (DstAccount == null))
				return errorMessage;

			return validate.Split('|').Any(key => !string.IsNullOrEmpty(this[key])) ? errorMessage : null;
		}
		#endregion

		private DateTime? GetDateByIndex(int idx)
		{
		    return idx >= _parentVM.DopAPSs.Count ? DateTime.Now : _parentVM.DopAPSs[idx].Date;
		}

	    private DateTime GetMinDate(int idx)
		{
	        var comparedate = idx > 0 ? GetDateByIndex(idx - 1) : _parentVM.DocDate;

			var mindate = GetDateByIndex(idx);

			int compare = DateTime.Compare(comparedate ?? DateTime.Now, mindate ?? DateTime.MinValue);
			if (compare <= 0)
				return comparedate.HasValue ? comparedate.Value.Date : DateTime.Now;
			return mindate.HasValue ? mindate.Value.Date : DateTime.MinValue;
		}

		protected DateTime GetMaxDate(int idx)
		{
			var maxdate = GetDateByIndex(idx);

			int compare = DateTime.Compare(_parentVM.DocDate ?? DateTime.Now, maxdate ?? DateTime.MaxValue);
			if (compare > 0)
				return _parentVM.DocDate.HasValue ? _parentVM.DocDate.Value.Date : DateTime.Now;
			return maxdate.HasValue ? maxdate.Value.Date : DateTime.MaxValue;
		}

		/// <summary>
		/// Загрузка данных об уточнении страховых взносов умерших
		/// </summary>
		/// <param name="lId">ID уточнения страховых взносов умерших в базе</param>
		public void Load(long lId)
		{
			ID = lId;
			try
			{
				Dopaps = DataContainerFacade.GetByID<DopAps>(lId);
                if (Dopaps == null)
                {
                    // произведено удаление, надо закрыть карточку
                    CloseWindow();
                    return;
                }

				long foreignKey = Dopaps.ApsID ?? Dopaps.ApsID.Value;
				if (foreignKey != 0)
				{
					if (_parentVM.ID != foreignKey)
					{
						var newViewModel = new ApsModelBase(//typeof(DueListViewModel), 
                            PortfolioIdentifier.PortfolioTypes.SPN);
						newViewModel.Load(foreignKey);
						_parentVM = newViewModel;
					}
				}
				if (Dopaps.SPFRBankAccountID.HasValue)
				{
					long srcAccID = Dopaps.SPFRBankAccountID.Value;
					long srcPFID = Dopaps.SPortfolioID.Value;
					SrcAccount = BLServiceSystem.Client.GetPortfolioFull(srcAccID, srcPFID);
				}
				if (Dopaps.PFRBankAccountID.HasValue)
				{
					long dstAccID = Dopaps.PFRBankAccountID.Value;
					long dstPFID = Dopaps.PortfolioID.Value;
					DstAccount = BLServiceSystem.Client.GetPortfolioFull(dstAccID, dstPFID);
				}
				IsDataChanged = false;
			}
			catch (Exception e)
			{
				if (DoNotThrowExceptionOnError == false)
					throw;

				Logger.WriteException(e);
				throw new ViewModelInitializeException();
			}
		}

		public ApsAccurateModelBase(ApsModelBase parentVM)
		{
			_parentVM = parentVM;
			ID = parentVM.ID;
            DataObjectTypeForJournal = typeof(DopAps);

			Dopaps = new DopAps { OperationDate = DateTime.Now };
			if (parentVM.State == ViewModelState.Read && GetViewModel != null)
			{
				var parentvm = GetViewModel(typeof(ApsModelBase)) as ApsModelBase;
				if (parentvm != null)
					parentVM = parentvm;
			}
			int cnt = parentVM.DopAPSs == null ? 0 : parentVM.DopAPSs.Count;
			DocDate = cnt > 0 ? GetMaxDate(cnt - 1).Date : parentVM.DocDate.Value.Date;
			SrcAccount = parentVM.SrcAccount;
			DstAccount = parentVM.DstAccount;
		}

		protected void RefreshCards()
		{
		    if (GetViewModel == null) return;
		    var vm = GetViewModel(typeof(PrepaymentViewModel)) as PrepaymentViewModel;
		    if (vm != null)
		    {
		        vm.RefreshDops(vm.ID);
		    }
		}


		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
            return new List<KeyValuePair<Type, long>>
            { 
				new KeyValuePair<Type, long>(typeof(DopAps), Dopaps.ID)
			};
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(DopAps), typeof(Aps) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(DopAps))
			{
				if (Dopaps!=null && id == Dopaps.ID)
					Load(Dopaps.ID);
			}

            // обработка закрытия окна, если сущность не сохранена в БД и удаляется родительская сущность
            if (type == typeof(Aps))
            {
                if (_parentVM == null || id == _parentVM.ID)
                {
                    CloseWindow();
                }
            }
		}

        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose == null) return;
            // необходимо заблокировать запрос о сохранении при закрытии окна
            Deleting = true;
            RequestClose(this, EventArgs.Empty);
        }
    }
}
