﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public sealed class IncomeSecurityViewModel : ViewModelCard, IUpdateRaisingModel, ICloseViewModelAfterSave
	{
		//private const decimal MAX_DECIMAL_19_4 = 1000000000000000;
		private readonly Incomesec _incomesec;

		private Currency _selCurrency;
		public Currency SelectedCurrency
		{
			get { return _selCurrency; }
			set
			{
				_selCurrency = value;
				if (_incomesec == null)
					return;
				SelectedCurs = GetCursForSelectedCurrency();
				OnPropertyChanged("SelectedCurrency");
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		private Curs _selCurs;
		public Curs SelectedCurs
		{
			get { return _selCurs; }
			set
			{
				if (_selCurs != value)
				{
					_selCurs = value;
					RefreshTotal();
					if (_incomesec == null)
						return;
					_incomesec.CursID = value == null ? null : (long?)value.ID;
				}
				OnPropertyChanged("Rate");
				OnPropertyChanged("Total");
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		public decimal Rate
		{
			get
			{
			    if (SelectedCurrency != null)
				{
					var curr = SelectedCurrency;
					var curs = SelectedCurs;
					if (CurrencyHelper.IsRUB(curr))
						return 1;
				    if (curs != null)
				        return curs.Value ?? 0;
				    return 0;
				}
			    return 0;
			}
		}

		public decimal? Summ
		{
			get
			{
				return _incomesec == null ? null : _incomesec.OrderSum;
			}
			set
			{
				if (_incomesec == null)
					return;
				_incomesec.OrderSum = value;
				OnPropertyChanged("Summ");
				RefreshTotal();
			}
		}

		private void RefreshTotal()
		{
			Total = Rate * Summ;
		}

		private decimal? _mTotal;
		public decimal? Total
		{
			get
			{
				return _mTotal;
			}
			set
			{
				if (value.HasValue)
					_mTotal = decimal.Round(value.Value, 2);
				OnPropertyChanged("Total");
			}
		}

		public string Comment
		{
			get
			{
				return _incomesec.Comment;
			}
			set
			{
				_incomesec.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		public string OrderNumber
		{
			get {
			    return _incomesec.OrderNumber != null ? _incomesec.OrderNumber.Trim() : null;
			}
		    set
			{
				_incomesec.OrderNumber = value;
				OnPropertyChanged("OrderNumber");
			}
		}

		public DateTime? OrderDate
		{
			get
			{
				return _incomesec == null ? null : _incomesec.OrderDate;
			}
			set
			{
				if (_incomesec == null)
					return;
				_incomesec.OrderDate = value;
				SelectedCurs = GetCursForSelectedCurrency();
				OnPropertyChanged("OrderDate");
				OnPropertyChanged("IsSelectCourceButtonEnabled");
			}
		}

		public DateTime? OperationDate
		{
			get
			{
				return _incomesec.OperationDate;
			}
			set
			{
				_incomesec.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		private static readonly List<string> MIncomeKinds = new List<string>
		{
				IncomeSecIdentifier.s_CouponYield, 
				IncomeSecIdentifier.s_SecurityRedemption, 
				IncomeSecIdentifier.s_DepositsPercents
		};
		public List<string> IncomeKinds => MIncomeKinds;

	    public string SelectedIncomeKind
		{
			get
			{
				return _incomesec.IncomeKind;
			}
			set
			{
				_incomesec.IncomeKind = value;
				OnPropertyChanged("SelectedIncomeKind");
			}
		}

		private PortfolioFullListItem _mAccount;
		public PortfolioFullListItem Account
		{
			get
			{
				return _mAccount;
			}
			set
			{
				_mAccount = value;
				if (_incomesec == null)
					return;
				if (value != null)
				{
					_incomesec.PFRBankAccountID = value.PfrBankAccount.ID;
					_incomesec.PortfolioID = value.Portfolio.ID;
					SelectedCurrency = value.PfrBankAccount.GetCurrency();
				}
				OnPropertyChanged("Account");
				OnPropertyChanged("AccountNumber");
				OnPropertyChanged("Bank");
				OnPropertyChanged("PFName");
			}
		}

		public bool IsSelectCourceButtonEnabled => (SelectedCurs == null || SelectedCurs.Date == null || !SelectedCurs.Date.Value.Equals(OrderDate)) && Rate == 0;

	    private readonly ICommand _selectAccount;
		public ICommand SelectAccount => _selectAccount;

	    #region Execute
		private bool Validate()
		{
			return !string.IsNullOrEmpty(_incomesec.IncomeKind) &&
				   !string.IsNullOrEmpty(_incomesec.OrderNumber) &&
				   _incomesec.PortfolioID != 0 && Rate > 0 &&
				   _incomesec.PFRBankAccountID != 0 &&				   
				   ValidateFields();
		}

		public override bool CanExecuteDelete()
		{
			return State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType)
		{
			DataContainerFacade.Delete(_incomesec);
			base.ExecuteDelete(DocOperation.Delete);
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					{
						ID = DataContainerFacade.Save(_incomesec);
						_incomesec.ID = ID;
					}
					break;
			}
		}

		private static bool CanExecuteSelectAccount()
		{
			return true;
		}

		private void ExecuteSelectAccount()
		{
			var p = new PortfolioIdentifier.PortfolioPBAParams
			{
														 AccountType = new[] { "текущий", "торговый" }														 
													 };

			var selected = DialogHelper.SelectPortfolio(p);

			if (selected != null)
			{
				Account = selected;
			}
		}
		#endregion

		public override string this[string columnName]
		{
			get
			{
				switch (columnName.ToUpper())
				{
					case "SUMM": return Summ.ValidateMaxFormat();
					case "RATE": return Rate > 0 ? null : "Неверное значение курса";
					case "ORDERNUMBER": return OrderNumber.ValidateRequired();
					case "ORDERDATE": return OrderDate.ValidateRequired();
					case "OPERATIONDATE": return OperationDate.ValidateRequired();
					case "ACCOUNTNUMBER": return string.IsNullOrWhiteSpace(Account.AccountNumber) ? DataTools.DATAERROR_NEED_DATA : null;
					default:
						return null;
				}
			}
		}

		Curs GetCursForSelectedCurrency()
		{
			if (SelectedCurrency == null || _incomesec == null || !_incomesec.OrderDate.HasValue)
				return null;

			var curses = BLServiceSystem.Client.GetOnDateCurrentCursesHib(_incomesec.OrderDate ?? DateTime.Now).ToList();
			return curses.FirstOrDefault(c => c.CurrencyID == SelectedCurrency.ID && c.Date.Value.Equals(_incomesec.OrderDate.Value.Date));
		}

		public void GetCourceManuallyOrFromCBRSite()
		{
			if (SelectedCurrency == null || _incomesec == null || !_incomesec.OrderDate.HasValue)
				return;

			var requestedRate = DialogHelper.GetCourceManuallyOrFromCBRSite(SelectedCurrency.ID, _incomesec.OrderDate.Value);
		    if (!requestedRate.HasValue) return;
		    var rate = new Curs
		    {
		        Value = requestedRate.Value,
		        Date = _incomesec.OrderDate,
		        CurrencyID = SelectedCurrency.ID
		    };
		    rate.ID = DataContainerFacade.Save<Curs, long>(rate);
		    SelectedCurs = rate;
		}

		public IncomeSecurityViewModel(ViewModelState action, long id)
			: base(new List<Type>())
		{
            DataObjectTypeForJournal = typeof(Incomesec);
            State = action;
			ID = id;

			_selectAccount = new DelegateCommand(o => CanExecuteSelectAccount(),
				o => ExecuteSelectAccount());

			if (State == ViewModelState.Create)
			{
				_incomesec = new Incomesec();
				OperationDate = DateTime.Today;
				OrderDate = DateTime.Today;
				SelectedIncomeKind = IncomeKinds.FirstOrDefault();
				Summ = 0;
			}
			else
			{
				_incomesec = DataContainerFacade.GetByID<Incomesec>(ID);
				Account = BLServiceSystem.Client.GetPortfolioFull(_incomesec.PFRBankAccountID ?? 0, _incomesec.PortfolioID ?? 0);
				SelectedIncomeKind = IncomeKinds.FirstOrDefault(kind => kind == _incomesec.IncomeKind);
				//огород - вместо хранения ID валюты, хранится ID курса этой валюты оО
				if (_incomesec.CursID.HasValue)
				{
					SelectedCurs = _incomesec.GetCurs();
					if (SelectedCurs != null)
						SelectedCurrency = SelectedCurs.GetCurrency();
				}
			}
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
		    return new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (Incomesec), _incomesec.ID)};
		}
	}
}
