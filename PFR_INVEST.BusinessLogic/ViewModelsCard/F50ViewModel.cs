﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class F50ViewModel : ViewModelCard
    {
        /// <summary>
        /// Поле хранения данных для старой ODBC системы
        /// </summary>
        public Dictionary<string, DBField> Fields { get; set; }

        #region Fields

        //private List<F50CBInfoListItem> cbInfoList = new List<F50CBInfoListItem>();
        //public List<F50CBInfoListItem> CBInfoList
        //{
        //    get { return cbInfoList; }
        //    set { cbInfoList = value; OnPropertyChanged("CBInfoList"); }
        //}

        private IList<F050CBInfo> cbInfoList = new List<F050CBInfo>();
        public IList<F050CBInfo> CBInfoList
        {
            get { return cbInfoList; }
            set { cbInfoList = value; OnPropertyChanged("CBInfoList"); }
        }

        private List<F50InfoListItem> infoList = new List<F50InfoListItem>();
        public List<F50InfoListItem> InfoList
        {
            get { return infoList; }
            set { infoList = value; OnPropertyChanged("InfoList"); }
        }

        public string Portfolio
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(Fields["EDO_ODKF050.PORTFOLIO"].Value as string))
                    return Fields["EDO_ODKF050.PORTFOLIO"].Value as string;
                return Fields["CONTRACT.PORTFNAME"].Value as string;
            }
        }

        #endregion

        public F50ViewModel(long id)
        {
            //DataObjectTypeForJournal = typeof(EdoOdkF020);
            ID = id;
            Fields = BLServiceSystem.Client.GetF50(id);
            foreach (string key in Fields.Keys)
            {
                if (Fields[key].Value is DateTime)
                    Fields[key].Value = DBFieldHelper.GetDate(key, Fields);
            }

            //DBEntity[] list = SaveProxyConvert.ConvertList(BLServiceSystem.Client.GetCBInfosListForF50(id));
            //this.cbInfoList = list.Select(dbEntity => new F50CBInfoListItem(dbEntity)).ToList();

            CBInfoList = BLServiceSystem.Client.GetCBInfosListForF50(id);

            try
            {
                infoList.Add(new F50InfoListItem("Государственные ценные бумаги Российской Федерации",
                                        Fields["EDO_ODKF050.GCB1"].Value as decimal?,
                                        Fields["EDO_ODKF050.GCB2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Государственные ценные бумаги субъектов Российской Федерации",
                                        Fields["EDO_ODKF050.GCBSUBRF1"].Value as decimal?,
                                        Fields["EDO_ODKF050.GCBSUBRF2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Облигации, выпущенные от имени муниципальных образований",
                                        Fields["EDO_ODKF050.OBLIGACIIMO1"].Value as decimal?,
                                        Fields["EDO_ODKF050.OBLIGACIIMO2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Облигации российских хозяйственных обществ",
                                        Fields["EDO_ODKF050.OBLIGACIIRHO1"].Value as decimal?,
                                        Fields["EDO_ODKF050.OBLIGACIIRHO2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Акции российских эмитентов, созданных в форме открытых акционерных обществ",
                                        Fields["EDO_ODKF050.AKCIIRAO1"].Value as decimal?,
                                        Fields["EDO_ODKF050.AKCIIRAO2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Паи (акции, доли) индексных инвестиционных фондов",
                                        Fields["EDO_ODKF050.PAI1"].Value as decimal?,
                                        Fields["EDO_ODKF050.PAI2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Ипотечные ценные бумаги",
                                        Fields["EDO_ODKF050.IPOTECH1"].Value as decimal?,
                                        Fields["EDO_ODKF050.IPOTECH2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("Ценные бумаги международных финансовых организаций",
                                        Fields["EDO_ODKF050.OBLIGMFO1"].Value as decimal?,
                                        Fields["EDO_ODKF050.OBLIGMFO2"].Value as decimal?));
                infoList.Add(new F50InfoListItem("ИТОГО ЦЕННЫХ БУМАГ",
                                        Fields["EDO_ODKF050.ITOGOCB1"].Value as decimal?,
                                        Fields["EDO_ODKF050.ITOGOCB2"].Value as decimal?));
                //this.infoList = this.infoList.Where(item => item.Count > 0 || item.Price > 0).ToList();
            }
            catch { }
        }

        public override bool CanExecuteSave() { return false; }

        protected override void ExecuteSave() { }

        public void RefreshLists() { }

        public override string this[string columnName] => "";

        private string Validate()
        {
            return "";
        }
    }
}
