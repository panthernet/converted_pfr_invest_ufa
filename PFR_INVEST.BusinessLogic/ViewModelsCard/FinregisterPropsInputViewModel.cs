﻿using System;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    [Obsolete("Похоже класс не используется")]
    public class FinregisterPropsInputViewModel : ViewModelCardDialog
    {

        //private Finregister finregister;

        private string _npfName = "";
        public string NPFName
        {
            get
            {
                return _npfName;
            }

            set
            {
                _npfName = value;
                OnPropertyChanged("NPFName");
            }
        }

        private string _regnum = "";
        public string RegNum
        {
            get
            {
                return _regnum;
            }

            set
            {
                _regnum = value;
                OnPropertyChanged("RegNum");
            }
        }

        private DateTime? _date;
        public DateTime? Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        private decimal? _count;
        public decimal? Count
        {
            get
            {
                return _count;

            }

            set
            {
                _count = value;
                OnPropertyChanged("Count");
            }
        }

        public FinregisterPropsInputViewModel()
            : base(typeof(RegistersListViewModel), true)
        {
            ID = -1;
        }

        #region Execute implementations

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate());
        }
        #endregion

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToLower())
                {
                    case "regnum": return string.IsNullOrEmpty(RegNum) ? errorMessage : null;
                    case "npfname": return string.IsNullOrEmpty(NPFName) ? errorMessage : null;
                    case "count": return Count == null || Count == 0 || !DataTools.IsNumericString(Count.ToString()) ? errorMessage : null;
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            const string fieldNames =
                "regnum|npfName|count";
            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !String.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }
    }
}
