﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class PaymentHistoryViewModel : ViewModelCard, ICloseViewModelAfterSave
    {

        public PaymentHistoryItem DataItem { get; set; }

        #region Execute implementations
        private bool Validate()
        {
            return
                !String.IsNullOrEmpty(DataItem.Payment.OrderNumber) &&
                   DataItem.Payment.OrderDate != null && DataItem.Payment.OrderDate != DateTime.MinValue;
        }

        public override bool CanExecuteDelete()
        {
            return State == ViewModelState.Edit;
        }
				
        protected override void ExecuteDelete(int delType)
        {
            if (!DialogHelper.ShowConfirmation(@"Удаление записи приведёт к обновлению статуса депозита. Продолжить?")) return;
            BLServiceSystem.Client.DeleteDepositPayment(DataItem.Payment.ID);
            base.ExecuteDelete(DocOperation.Delete);
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    {
                        ID = DataContainerFacade.Save(DataItem.Payment);
                        DataItem.Payment.ID = ID;
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        public override string this[string columnName]
        {
            get
            {
                switch (columnName.ToLower())
                {
                    case "ordernumber": return string.IsNullOrWhiteSpace(OrderNumber) ? DataTools.DATAERROR_NEED_DATA : null;
                    case "orderdate": return (OrderDate == null) || (((DateTime)OrderDate).Year < 1900) ? DataTools.DATAERROR_NEED_DATA : null;
                    default:
                        return null;
                }
            }
        }

        public PaymentHistoryViewModel(ViewModelState action, long id)
            : base(new List<Type>
            { typeof(IncomeSecurityListViewModel)//, typeof(BalanceListViewModel)
                , typeof(DepositsListViewModel), typeof(DepositsListByPortfolioViewModel)})
        {
            DataObjectTypeForJournal = typeof(PaymentHistory);
            State = action;
            ID = id;

            if (State == ViewModelState.Create)
            {
                throw new NotImplementedException();
            }
            DataItem = BLServiceSystem.Client.GetPaymentHistoryItem(ID);
            //OnPropertyChanged("DataItem");
        }

        public decimal Rate => DataItem == null ? 1 : DataItem.Rate;

        public DateTime? OrderDate
        {
            get { return DataItem.Payment.OrderDate; }
            set
            {
                DataItem.Payment.OrderDate = value;
                OnPropertyChanged("OrderDate");
            }
        }

        public string OrderNumber
        {
            get { return DataItem.Payment.OrderNumber; }
            set
            {
                DataItem.Payment.OrderNumber = value;
                OnPropertyChanged("OrderNumber");
            }
        }

        public string Comment
        {
            get { return DataItem.Payment.Comment; }
            set
            {
                DataItem.Payment.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public DateTime Date
        {
            get { return DataItem.Payment.Date; }
            set
            {
                DataItem.Payment.Date = value;
                OnPropertyChanged("Date");
            }
        }

        public string Currency
        {
            get { return DataItem.Currency; }
            set
            {
                DataItem.Payment.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public decimal Sum
        {
            get { return DataItem.Payment.Sum; }
            set
            {
                DataItem.Payment.Sum = value;
                OnPropertyChanged("Sum");
            }
        }
    }
}
