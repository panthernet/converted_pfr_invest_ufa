﻿using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class WithdrawalViewModel : AccModelBase, ICloseViewModelAfterSave
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public WithdrawalViewModel()
        {
            ID = 0;
            acc.KindID = 4;// потому что списание со счета
        }
				
    }
}
