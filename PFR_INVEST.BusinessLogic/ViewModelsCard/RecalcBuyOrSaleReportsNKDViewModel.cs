﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class RecalcBuyOrSaleReportsNKDViewModel : ViewModelCard
    {
        private long[] m_Reports;
        public int ReportsCount;
        public int CurrentReportIndex;
        public int ErrorsCount;
        public bool Canceled;

        public RecalcBuyOrSaleReportsNKDViewModel()
        {
            DataObjectTypeForJournal = typeof(object);
        }

        protected override void ExecuteSave() { }
        public override bool CanExecuteSave()
        {
            return false;
        }

        public void LoadReports()
        {
            m_Reports = BLServiceSystem.Client.GetOrdReportsIDsForRecalc();
            ReportsCount = m_Reports.Length;
            CurrentReportIndex = 0;
            ErrorsCount = 0;
            Canceled = false;
           // BLServiceSystem.Client.RecalcNKDForReport(4345);
        }

        public void RecalcNextReport()
        {
            if (IsRecalcuationFinished())
                return;

            try
            {
                BLServiceSystem.Client.RecalcNKDForReport(m_Reports[CurrentReportIndex]);
            }
            catch (Exception ex)
            {
                HandleException(ex);
                ErrorsCount++;
            }

            CurrentReportIndex++;
        }

        public bool IsRecalcuationFinished()
        {
            return ReportsCount <= CurrentReportIndex;
        }

        public override string this[string columnName] => null;
    }
}