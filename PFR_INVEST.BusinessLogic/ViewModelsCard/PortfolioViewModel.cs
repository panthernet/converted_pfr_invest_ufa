﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
	public class PortfolioViewModel : ViewModelCard
	{
		readonly Portfolio _portfolio;
		readonly bool _mNeedSaveAccountsOnPortfolioSave;
		readonly List<PortfolioPFRBankAccountListItem> _mNeedSaveAccounts;

		#region PortfolioTypes

		public List<Element> PortfolioTypes { get; private set; }

		public long SelectedPortfolioType
		{
			get
			{
				return _portfolio.TypeID;
			}
			set
			{
				_portfolio.TypeID = value;
				OnPropertyChanged("SelectedPortfolioType");
			}
		}
		#endregion

		#region YearsList
		public List<Year> YearsList { get; set; }

		private Year m_SelectedYear;
		public Year SelectedYear
		{
			get
			{
				return m_SelectedYear;
			}

			set
			{
				if (value != null && _portfolio.YearID != value.ID)
				{
					m_SelectedYear = value;
					if (value.ID > 0)
					{
						_portfolio.YearID = value.ID;
					}
					OnPropertyChanged("SelectedYear");
				}
			}
		}
		#endregion

		public string Year
		{
			get
			{
				return _portfolio.Year;
			}
			set
			{
				_portfolio.Year = value;
				OnPropertyChanged("Year");
			}
		}

		public PortfolioPFRBankAccountListItem SelectedAccount { get; set; }
		private List<PortfolioPFRBankAccountListItem> m_PFRAccountsList;
		public List<PortfolioPFRBankAccountListItem> PFRAccountsList
		{
			get
			{
				return m_PFRAccountsList;
			}
			set
			{
				m_PFRAccountsList = value;
				OnPropertyChanged("PFRAccountsList");
			}
		}

		private List<PFRAccountsListItem> m_FullAccountsList;
		public List<PFRAccountsListItem> FullAccountsList
		{
			get
			{
				return m_FullAccountsList;
			}
			set
			{
				m_FullAccountsList = value;
			}
		}

		public ICommand DeleteAccount { get; private set; }
		public ICommand AddAccount { get; private set; }

		private List<PFRAccountsListItem> GetFullAccountsList()
		{
			var list = BLServiceSystem.Client.GetPFRAccountsList();
			return list;
		}

		private void ExecuteAddAccount()
		{
			FullAccountsList = GetFullAccountsList();
			if (!CanExecuteAddAccount())
			{
				DialogHelper.ShowAlert("Все возможные счета уже добавлены");
				return;
			}

			long? dialogID = null;
			long[] selectedIDs = null; //уже выбранные счета

			if (_mNeedSaveAccountsOnPortfolioSave)
			{
				selectedIDs = (from a in _mNeedSaveAccounts select a.ID).ToArray();
				if (PFRAccountsList != null && PFRAccountsList.Count > 0)
					selectedIDs = (from a in PFRAccountsList select a.ID).Union(selectedIDs).ToArray();
			}
			else
				selectedIDs = (from a in PFRAccountsList select a.ID).ToArray();
			var link = DialogHelper.AddAccount(selectedIDs);

			if (link != null)
			{
				dialogID = link.ID;

				if (_mNeedSaveAccountsOnPortfolioSave)
				{
					if (_mNeedSaveAccounts.Any(x => x.ID == dialogID.Value))
						return;

					_mNeedSaveAccounts.Add(link);
				}
				else
				{
					if (PFRAccountsList != null && PFRAccountsList.Any(acc => acc.ID == dialogID.Value))
						return;

					//ищем, есть ли скрытая связка выбранного счета с портфелем, если есть - открываем связь, если нет - добавляем новую
					bool Found = false;
					var list = DataContainerFacade.GetListByProperty<PortfolioPFRBankAccount>("PortfolioID", ID);
					foreach (var item in list)
					{
						if (item.StatusID == -1 && item.PFRBankAccountID == dialogID.Value)
						{
							item.StatusID = 0;
							item.AssignKindID = link.AssignKindID;
							DataContainerFacade.Save(item);
							JournalLogger.LogEvent("Портфель ПФР", JournalEventType.INSERT, ID, "Добавление счета", String.Format("Счет ID:{0} ", item.PFRBankAccountID));
							Found = true; break;
						}
					}

					if (!Found)
					{
						var newAcc = new PortfolioPFRBankAccount
						{
							PortfolioID = ID,
							PFRBankAccountID = dialogID.Value,
							AssignKindID = link.AssignKindID,
							StatusID = 0
						};

						DataContainerFacade.Save(newAcc);
						JournalLogger.LogEvent("Портфель ПФР", JournalEventType.INSERT, ID, "Добавление счета", String.Format("Счет ID:{0} ", newAcc.PFRBankAccountID));
					}
					RefreshListViewModels(ConnectedListViewModels);
				}

				RefreshAccounts();
			}
		}

		private bool CanExecuteAddAccount()
		{
			long[] selectedIDs = null; //уже выбранные счета

			if (_mNeedSaveAccountsOnPortfolioSave)
			{
				selectedIDs = (from a in _mNeedSaveAccounts select a.ID).ToArray();
				if (PFRAccountsList != null && PFRAccountsList.Count > 0)
					selectedIDs = (from a in PFRAccountsList select a.ID).Union(selectedIDs).ToArray();
			}
			else
				selectedIDs = (from a in PFRAccountsList select a.ID).ToArray();
			return !EditAccessDenied && FullAccountsList.Any(x => !selectedIDs.Contains(x.ID));
		}

		private void ExecuteDeleteAccount()
		{
			if (ID > 0) //портфель уже существует
			{
				////создаем моедль, и удаляем объект, для соблюдения всех процедур (логирование и удаление в архив)
				//PFRAccountViewModel model = new PFRAccountViewModel();
				//model.Load(SelectedAccount.ID);
				//if (model.BA != null) model.ExecuteDelete();

				//исключаем счет из портфеля (счет будет отображаться в списке счетов)
				//по факту выставляем статус связки портфеля и счета в -1 (счет не отображается дял текущего портфеля)
				if (!DialogHelper.ShowWarning("Внимание! В целях безопасности, убедитесь, что исключаемый счет не участвует в операциях связанных с текущим портфелем! Продолжить?", "Внимание", true))
					return;
				var pflist = DataContainerFacade.GetListByProperty<PortfolioPFRBankAccount>("PortfolioID", ID);
				foreach (var item in pflist)
				{
					if (item.PFRBankAccountID == SelectedAccount.ID)
					{
						item.StatusID = -1;
						DataContainerFacade.Save(item);

						JournalLogger.LogEvent("Портфель ПФР", JournalEventType.DELETE, ID, "Исключение/Удаление счета", String.Format("Счет ID:{0} ", item.PFRBankAccountID));
						//DataContainerFacade.Delete<PortfolioPFRBankAccount>(item.ID); 
						break;
					}
				}
				//acc.PortfolioID = null; 
				//DataContainerFacade.Save<PortfolioPFRBankAccount>(acc);
			}
			//else //созданный, еще не сохраненный  портфель
			//{
			if (_mNeedSaveAccounts != null) //удаляем из временной коллекции
			{
				_mNeedSaveAccounts.RemoveAll(x => x.ID == SelectedAccount.ID);
			}
			//}

			RefreshAccounts();
			RefreshListViewModels(ConnectedListViewModels);
		}

		private bool CanExecuteDeleteAccount()
		{
			return !EditAccessDenied
				&& SelectedAccount != null;
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			var e = DataContainerFacade.GetListByPropertyConditions<Portfolio>(new List<ListPropertyCondition>
                                                                          {
                                                                              ListPropertyCondition.Equal("Year", _portfolio.Year),
                                                                              ListPropertyCondition.Equal("TypeID", _portfolio.TypeID),
                                                                              _portfolio.YearID.HasValue? ListPropertyCondition.Equal("YearID", _portfolio.YearID):ListPropertyCondition.IsNull("YearID"),
                                                                              ListPropertyCondition.NotEqual("ID", _portfolio.ID)
                                                                          });
			if (e.Any())
			{
				if (e.Single().StatusID == -1)// удален
				{
					DialogHelper.ShowAlert(@"Введенный Портфель ранее уже был добавлен в систему, но сейчас находится в статусе ""Удален"".
Необходимо обратиться к Администратору для восстановления Портфеля!
Повторно Портфель с такими же параметрами (""Наименование портфеля"", ""Тип портфеля"" и ""Год"") создать нельзя!");
				}
				else
				{
					//DialogHelper.Alert("Портфель с выбранным типом, годом и наименованием уже существует");
					DialogHelper.ShowAlert("Введенный Портфель уже есть в системе!");
				}
				return false;
			}

			
			var list = PFRAccountsList.ToList();
			list.AddRange(_mNeedSaveAccounts);
			var main = list.Where(x => x.AssignKindName.ToLower().Trim().Equals("основной", StringComparison.InvariantCultureIgnoreCase)).Select(x => x.ID).Distinct();

			if (main.Count() > 1)
			{
				DialogHelper.ShowAlert("Портфель может содержать только один основной счет");
				return false;
			}

			return base.BeforeExecuteSaveCheck();
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:

                    _portfolio.TrimAllPropsAndFields();

					ID = DataContainerFacade.Save(_portfolio);
					_portfolio.ID = ID;

					if (_mNeedSaveAccountsOnPortfolioSave)
					{
						var existsLinks = DataContainerFacade.GetListByProperty<PortfolioPFRBankAccount>("PortfolioID", ID);

						foreach (PortfolioPFRBankAccountListItem item in _mNeedSaveAccounts)
						{
							// if (existsLinks.Any(x => x.PFRBankAccountID == item.ID)) continue; // у связи появился тип, поэтому его надо сравнивать и корректировать при необходимости
							var ex = existsLinks.FirstOrDefault(x => x.PFRBankAccountID == item.ID);
							if (ex != null)
							{
								if (ex.AssignKindID != item.AssignKindID || ex.StatusID == -1)
								{
									ex.AssignKindID = item.AssignKindID;
									ex.StatusID = 0;
									DataContainerFacade.Save(ex);
								}
							}
							else
							{
								PortfolioPFRBankAccount newAcc = new PortfolioPFRBankAccount
								{
									PortfolioID = ID,
									PFRBankAccountID = item.ID,
									StatusID = 0,
									AssignKindID = item.AssignKindID
								};
								DataContainerFacade.Save(newAcc);
							}
						}

						//m_NeedSaveAccountsOnPortfolioSave = false;
					}

					break;
				default:
					break;
			}
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create && !PFRAccountsList.Any();
		}

		public override bool BeforeExecuteDeleteCheck()
		{
			// потеряло смысл после уточнения http://jira.vs.it.ru/browse/PFRPTK-1507?focusedCommentId=220505&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-220505
			//if (DXMessageBox.Show("Внимание! Удаление портфеля приведет к отвязке всех привязанных к нему счетов. Продолжить?", "Внимание", System.Windows.MessageBoxButton.YesNo, System.Windows.MessageBoxImage.Warning) == System.Windows.MessageBoxResult.No)
			//    return;

		    if (_portfolio.StatusID == 2)
		    {
                DialogHelper.ShowError("Невозможно удалить портфель, так как он является системным портфелем!");
                return false;
            }

			var pflist = DataContainerFacade.GetListByProperty<PortfolioPFRBankAccount>("PortfolioID", ID);
			if (pflist.Any(x => x.StatusID != -1))
			{
				DialogHelper.ShowError("Невозможно удалить портфель, так как он имеет привязанные счета.");
				return false;
			}
			return true;
		}

		protected override void ExecuteDelete(int delType)
		{

			_portfolio.StatusID = -1;
			DataContainerFacade.Save<Portfolio, long>(_portfolio);

			////удаляем привязки для счетов к этому портфелю
			//var pflist = DataContainerFacade.GetListByProperty<PortfolioPFRBankAccount>("PortfolioID", ID);
			//foreach (var item in pflist)
			//{
			//    item.StatusID = -1;
			//    DataContainerFacade.Save<PortfolioPFRBankAccount>(item);
			//}

			/*foreach (var lID in Portfolio.GetPortfolioPFRBankAccountList().Cast<PortfolioPFRBankAccount>().Select(acc => acc.PFRBankAccountID))
			{
				var acc = DataContainerFacade.GetByID<PfrBankAccount>(lID);
				acc.StatusID = -1;
				DataContainerFacade.Save<PfrBankAccount, long>(acc);
			}*/
			base.ExecuteDelete(DocOperation.Archive);
		}

		private void RefreshAccounts()
		{
			bool alreadyChanged = IsDataChanged;

			//var obj = DataContainerFacade.GetByID<PortfolioPFRBankAccount>(1190);

			if (_mNeedSaveAccountsOnPortfolioSave)
			{
				List<PortfolioPFRBankAccountListItem> t;
				if (_mNeedSaveAccounts != null && _mNeedSaveAccounts.Count > 0)
				{
					//выставление полей BankName и Type
					t = BLServiceSystem.Client.GetAccountsForPorPortfolioByIDs(_mNeedSaveAccounts.Select(x => x.ID).ToArray());
					// выставление AssignKind
					t.ForEach(x =>
					{
						var src = _mNeedSaveAccounts.FirstOrDefault(y => y.ID == x.ID);
						x.AssignKindID = src.AssignKindID;
						x.AssignKindName = src.AssignKindName;
					});
					alreadyChanged = true;
				}
				else
					t = new List<PortfolioPFRBankAccountListItem>();

				var ex = BLServiceSystem.Client.GetAccountsForPorPortfolio(_portfolio.ID);

				ex.ForEach(x => { if (t.All(y => y.ID != x.ID)) t.Add(x); });

				PFRAccountsList = t;
			}
			else
				PFRAccountsList = BLServiceSystem.Client.GetAccountsForPorPortfolio(_portfolio.ID);

			IsDataChanged = alreadyChanged;
		}

		public PortfolioViewModel(long id, ViewModelState action)
			: base(typeof(PortfoliosListViewModel))
		{
			DataObjectTypeForJournal = typeof(Portfolio);
			PortfolioTypes = BLServiceSystem.Client.GetElementByType(Element.Types.PortfolioType);

			YearsList = new List<Year> { new Year { ID = -1, Name = "Год не указан" } };
			YearsList.AddRange(DataContainerFacade.GetList<Year>());
			AddAccount = new DelegateCommand(o => CanExecuteAddAccount(),
				o => ExecuteAddAccount());

			DeleteAccount = new DelegateCommand(o => CanExecuteDeleteAccount(),
				o => ExecuteDeleteAccount());

			State = action;
			_mNeedSaveAccountsOnPortfolioSave = true;
			_mNeedSaveAccounts = new List<PortfolioPFRBankAccountListItem>();

			if (State == ViewModelState.Create)
			{
				_portfolio = new Portfolio();
				SelectedPortfolioType = PortfolioTypes.First().ID;
				PFRAccountsList = new List<PortfolioPFRBankAccountListItem>();
			}
			else
			{
				ID = id;
				_portfolio = DataContainerFacade.GetByID<Portfolio>(ID);
				m_SelectedYear = YearsList.FirstOrDefault(year => year.ID == _portfolio.YearID);
				RefreshAccounts();
			}

			FullAccountsList = GetFullAccountsList();
		}

		private const string ERROR_NAME = "Не указано наименование";
		private const string ERROR_SELECTED_TYPE = "Не выбран тип";
		private const string ERROR_SELECTED_YEAR = "Не выбран год";
		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "Year": return Year.ValidateNonEmpty();
					case "SelectedPortfolioType": return SelectedPortfolioType.ValidateRequired();
					case "SelectedYear": return null;
					default: return null;
				}
			}
		}

		private bool Validate()
		{
			return ValidateFields();
		}
	}
}
