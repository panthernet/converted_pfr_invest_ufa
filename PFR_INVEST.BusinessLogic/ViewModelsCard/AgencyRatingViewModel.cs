﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class AgencyRatingViewModel : ViewModelCard, IRefreshableCardViewModel
    {
        public MultiplierRating MultiplierRating { get; private set; }

        public IList<RatingAgency> Agencies { get; private set; }
        public IList<Rating> Ratings { get; private set; }
        public IList<long?> ReservedAgencyNumbers { get; private set; }

        public long? AgencyID
        {
            get
            {
                return MultiplierRating.AgencyID;
            }
            set
            {
                if (MultiplierRating.AgencyID == value) return;
                MultiplierRating.AgencyID = value;
                OnAgencyChanged();
                OnPropertyChanged("AgencyID");
                OnPropertyChanged("IsNumberEditReadOnly");
                OnPropertyChanged("Ratings");
            }
        }

        private void OnAgencyChanged(bool presetNumber = true)
        {
            var existsMultipliers = DataContainerFacade.GetListByProperty<MultiplierRating>("AgencyID", AgencyID).Where(x => x.ID != MultiplierRating.ID).ToList();
            ReservedAgencyNumbers = existsMultipliers.Select(x => x.Number).ToList();

            if (presetNumber)
            {
                if (existsMultipliers.Any())
                {
                    Number = existsMultipliers.Max(x => x.Number) + 1;
                }
                else
                {
                    Number = 1;
                }
            }

            Ratings = DataContainerFacade.GetList<Rating>().Where(x => existsMultipliers.All(m => m.RatingID != x.ID)).ToList();
        }

        public long? RatingID
        {
            get
            {
                return MultiplierRating.RatingID;
            }
            set
            {
                if (MultiplierRating.RatingID == value) return;
                MultiplierRating.RatingID = value;
                OnPropertyChanged("RatingID");
            }
        }

        public long? Number
        {
            get
            {
                return MultiplierRating.Number;
            }
            set
            {
                if (MultiplierRating.Number == value) return;
                MultiplierRating.Number = value;
                OnPropertyChanged("Number");
            }
        }

        public decimal? Multiplier
        {
            get
            {
                return MultiplierRating.Multiplier;
            }
            set
            {
                if (MultiplierRating.Multiplier == value) return;
                MultiplierRating.Multiplier = value;
                OnPropertyChanged("Multiplier");
            }
        }
        public AgencyRatingViewModel(long multiplierRatingID, ViewModelState state)
            : base(typeof(AgencyRatingsListViewModel))
        {
            DataObjectTypeForJournal = typeof (MultiplierRating);
            State = state;
            if (multiplierRatingID > 0)
            {
                var r = DataContainerFacade.GetByID<MultiplierRating>(multiplierRatingID);
                if (r == null)
                {
                    throw new Exception("Не удалось получить коэффициент рейтинга с идентификатором " + multiplierRatingID);
                }

                MultiplierRating = r;
                var number = Number;
                OnAgencyChanged();
                Number = number;
                ID = multiplierRatingID;
                CardID = ID;
            }
            else
            {
                MultiplierRating = new MultiplierRating();
            }

            Agencies = DataContainerFacade.GetList<RatingAgency>();
            Ratings = DataContainerFacade.GetList<Rating>();

            //this.IsAgencyReadOnly = isAgencyReadOnly;
            //this.Agencies = DataContainerFacade.GetList<RatingAgency>().Where(a => !excludedAgenceis.Contains(a.ID)).ToList();
            //this.Ratings = DataContainerFacade.GetList<Rating>();
            //this.Rating = rating;
            //this.SaveCard = new DelegateCommand<object>(o =>
            //{
            //    return string.IsNullOrWhiteSpace(this.Rating["AgencyID"])
            //        && string.IsNullOrWhiteSpace(this.Rating["RatingID"])
            //        && string.IsNullOrWhiteSpace(this.Rating["Date"])
            //        && string.IsNullOrWhiteSpace(this.Rating["Multiplier"]);

            //}, o => { });

            //if (this.Rating.AgencyID == 0 && this.Agencies.Count == 1)
            //    this.Rating.AgencyID = this.Agencies.First().ID;
            //if (!this.Rating.Multiplier.HasValue)
            //    this.Rating.Multiplier = 0.0M;
        }

        public bool IsNumberEditReadOnly => MultiplierRating.AgencyID == null || EditAccessDenied;

        protected override bool BeforeExecuteSaveCheck()
        {
            OnAgencyChanged(false);
            if (Ratings.All(x => x.ID != RatingID))
            {
                OnPropertyChanged("Ratings");
                DialogHelper.ShowError("Заданный рейтинг уже зарегистрирован для выбранного агентства.");
                return false;
            }

            if (ReservedAgencyNumbers.Any(x => x == Number))
            {
                OnPropertyChanged("Number");
                DialogHelper.ShowError("Заданный номер уже используется в выбранном агентстве.");
                return false;
            }

            return base.BeforeExecuteSaveCheck();
        }

        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                    case ViewModelState.Create:
                        ID = MultiplierRating.ID = DataContainerFacade.Save<MultiplierRating, long>(MultiplierRating);
                        CardID = ID;
                        break;
                }
            }
            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && MultiplierRating.ID > 0;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            var linkedBanks = DataContainerFacade.GetListByProperty<LegalEntityRating>("MultiplierRatingID", MultiplierRating.ID);

            if (!linkedBanks.Any()) return true;

            var bankList = DataContainerFacade.GetList<LegalEntity>();
            DialogHelper.ShowError(string.Format("Удаляемый рейтинг имеет привязку к банкам\r\n{0}", bankList.Where(x => linkedBanks.Any(lb => lb.LegalEntityID == x.ID)).Select(x => x.FormalizedName).Aggregate((x, y) => x + ", " + y)));
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(MultiplierRating);
            base.ExecuteDelete(delType);
        }

        public string Validate()
        {
            return "AgencyID|RatingID|Number|Multiplier".Split('|')
                .Select(key => this[key])
                .Any(error => !string.IsNullOrEmpty(error)) ? "Не заполнены все обязательные поля!" : string.Empty;
        }

        public override string this[string columnName]
        {
            get
            {
                const string error = "Поле обязательное для заполнения";
                const string numberReservedError = "Неуникальный номер";
                switch (columnName)
                {
                    case "AgencyID":
                        return MultiplierRating.AgencyID == null ? error : null;
                    case "RatingID":
                        return MultiplierRating.RatingID == null ? error : null;
                    case "Number":
                        return MultiplierRating.Number == null ? error :
                            (ReservedAgencyNumbers != null && ReservedAgencyNumbers.Any(x => MultiplierRating.Number == x)) ? numberReservedError :
                            null;
                    case "Multiplier":
                        return MultiplierRating.Multiplier == null ? error : (MultiplierRating.Multiplier <= 1 && MultiplierRating.Multiplier >= 0) ? null : "Значение от 0 до 1";
                    default:
                        return null;
                }
            }
        }

        public void RefreshModel()
        {
            if (State != ViewModelState.Create )
            {
                 MultiplierRating = DataContainerFacade.GetByID<MultiplierRating>(CardID);
            }

            var number = Number;
            OnAgencyChanged();
            Number = number;
            Agencies = DataContainerFacade.GetList<RatingAgency>();
            Ratings = DataContainerFacade.GetList<Rating>();
            OnPropertyChanged("Number");
            OnPropertyChanged("AgencyID");
            OnPropertyChanged("Multiplier");
            OnPropertyChanged("Ratings");
        }

        public long? CardID { get; private set; }
    }
}
