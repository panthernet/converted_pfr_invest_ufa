﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
	public class FastReportInputViewModel : ViewModelCard, IUpdateRaisingModel
	{
		#region Статические строки

		private const string ORDER_COUNT_OWERFLOW_MSG = "Превышен лимит количества ЦБ \"{0}\", указанный в поручении. Сохранение отменено!";
		private const string ORDER_SUM_OWERFLOW_MSG = "Превышен лимит суммы сделки по ЦБ \"{0}\", указанный в поручении. Сохранение отменено!";

		#endregion

		public EventHandler<NotAllItemsHaveCoursesEventArgs> OnNotAllItemsHaveCourses;
		protected void RaiseNotAllItemsHaveCourses(List<DateTime> dates)
		{
			if (OnNotAllItemsHaveCourses != null)
				OnNotAllItemsHaveCourses(this, new NotAllItemsHaveCoursesEventArgs(dates));
		}

		public EventHandler<StringEventArgs> OnSellAbort;
		protected void RaiseSellAbort(string text)
		{
			if (OnSellAbort != null)
				OnSellAbort(this, new StringEventArgs(text));
		}


		public bool CurrensyIsRubles { get; private set; }
		public long CurrensyID { get; private set; }
		public bool IsSalerColumnVisible => !CurrensyIsRubles;

	    public bool IsBuyOrder => OrderTypeIdentifier.IsBuyOrder(Type);

	    public readonly CbOrder Order;
		public string Type => Order != null && Order.Type != null ? Order.Type.Trim() : "";

	    public string RegNum => Order != null && Order.RegNum != null ? Order.RegNum.Trim() : "";
	    public DateTime? RegDate => Order != null ? Order.RegDate : null;

	    private readonly PfrBankAccount m_DepoAccount;
		public string AccountNumber => m_DepoAccount != null ? m_DepoAccount.AccountNumber : null;

	    private readonly Portfolio m_Portfolio;
		public string PortfolioYear => m_Portfolio != null ? m_Portfolio.Year : null;

	    private readonly LegalEntity m_LegalEntity;
		public String Bank => m_LegalEntity != null ? m_LegalEntity.FormalizedName : null;

	    BindingList<FastReportItem> m_FastReportItems = new BindingList<FastReportItem>();
		public BindingList<FastReportItem> FastReportItems
		{
			get
			{
				return m_FastReportItems;
			}
			set
			{
				m_FastReportItems = value;
				OnPropertyChanged("FastReportItems");
			}
		}

		public FastReportInputViewModel(long p_ID)
			: base(new List<Type> { typeof(SaleReportsListViewModel), typeof(BuyReportsListViewModel), typeof(OrdersListViewModel) })
		{
            DataObjectTypeForJournal = typeof(OrdReport);
            ID = p_ID;

			Order = DataContainerFacade.GetByID<CbOrder, long>(ID);
			m_Portfolio = Order.GetPortfolio();
			m_LegalEntity = Order.GetBankAgent();
			m_DepoAccount = Order.GetDEPOAccount();

			FastReportItems.ListChanged += FastReportItems_ListChanged;

			RefreshIssues();
			RefreshConnectedCardsViewModels = RefreshOrderViewModel;
		}

		protected void RefreshOrderViewModel()
		{
			if (GetViewModel != null)
			{
				OrderViewModel vm = GetViewModel(typeof(OrderViewModel)) as OrderViewModel;
				if (vm != null && vm.ID == Order.ID)
					vm.RefreshReports();
			}
		}

		void FastReportItems_ListChanged(object sender, ListChangedEventArgs e)
		{
			OnPropertyChanged("FastReportItems");
		}

		public static List<Issue> Issues { get; set; }
		public void RefreshIssues()
		{
			IEnumerable<CbInOrder> cbinorders = Order.GetCbInOrderList().Cast<CbInOrder>();

			if (cbinorders == null || cbinorders.Count() < 1)
			{
				Issues = new List<Issue>();
				return;
			}

			CurrensyIsRubles = CurrencyIdentifier.IsRUB(cbinorders.First().GetSecurity().CurrencyName);
			CurrensyID = cbinorders.First().GetSecurity().CurrencyID.Value;

			Issues = BLServiceSystem.Client.GetSecuritiesListForOrderHib(ID).ConvertAll(
				security => new Issue
				{
					CbInOrderID = cbinorders.First(
						cbinorder => cbinorder.SecurityID == security.ID
					).ID,
					SecurityID = security.ID,
					Name = security.Name.Trim(),
					NomValue = security.NomValue ?? 0
				}
			);
		}

		public void AddNewRow()
		{
			DateTime defaultDate = DateTime.Now;
			DateTime defaultOpDate = DateTime.Now;
			if (FastReportItems.Count > 0)
			{
				defaultDate = FastReportItems[0].Date;
				defaultOpDate = FastReportItems[0].Date;
			}

			FastReportItem item = new FastReportItem(CurrensyIsRubles)
			{
				Issue = Issues.FirstOrDefault(),
				Count = 0,
				Date = defaultDate,
				ReportDate = defaultDate,
				DEPODate = defaultOpDate,
				Price = 0,
				Contragent = OrderReportsHelper.Contragents.Last(),
				Yield = 0
			};
			FastReportItems.Add(item);
			item.PropertyChanged += item_PropertyChanged;
		}

		void item_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			FastReportItems_ListChanged(FastReportItems, new ListChangedEventArgs(ListChangedType.ItemChanged, FastReportItems.IndexOf(sender as FastReportItem)));
		}


		protected override bool BeforeExecuteSaveCheck()
		{
			if (!CurrensyIsRubles)
			{
				List<DateTime> notPresentCoursesDates = new List<DateTime>();
				FastReportItems.ToList().ForEach(
					item =>
					{
						DateTime date;
						if (CurrensyIsRubles)
							date = item.Date;
						else
							date = item.DEPODate;

						if (BLServiceSystem.Client.GetRateForDateHib(CurrensyID, date) == null)
							notPresentCoursesDates.Add(date);
					}
				);

				if (notPresentCoursesDates.Count > 0)
				{
					RaiseNotAllItemsHaveCourses(notPresentCoursesDates);
					return false;
				}
			}

			//перед сохранением нужно убедиться что отчеты не превышают количесва ЦБ в поручении
			//Проверяем все кроме валютных покупок
			if (CurrensyIsRubles || !OrderTypeIdentifier.IsBuyOrder(Order.Type))
			{
				List<CbInOrder> m_CbInOrderList = Order.GetCbInOrderList().Cast<CbInOrder>().ToList();

				int cbCount = m_CbInOrderList.GroupBy(cb => cb.SecurityID).Count();
				List<BuyOrSaleReportsListItem> m_AlreadyAddedReports = BLServiceSystem.Client.GetBuyOrSaleReportsListHib(OrderTypeIdentifier.IsSellOrder(Order.Type), Order.ID);

				//если в поручении несколько одинаковых ценных бумаг, то это аукцион
				if (m_CbInOrderList.Count() > 1 && cbCount == m_CbInOrderList.Count())
				{
					foreach (var item in m_CbInOrderList)
					{
						Security security = DataContainerFacade.GetByID<Security>(item.SecurityID);
						//если заполняли колонку количество - считаем сумму доступных по CbInOrder
						if ((item.Count.HasValue && item.Count > 0))
						{
							long limit = (long)item.Count;
							long addeCount = m_AlreadyAddedReports.Where(a => a.SecurityID == item.SecurityID).Sum(r => r.Count);
							long newCount = FastReportItems.Where(a => a.Issue.SecurityID == item.SecurityID).Sum(r => r.Count);
							if (limit - addeCount < newCount)
							{
								RaiseSellAbort(string.Format(ORDER_COUNT_OWERFLOW_MSG, security.Name));
								//throw new ViewModelSaveUserCancelledException();
								return false;
							}
						}
						else
						{
							decimal limit = m_CbInOrderList.Sum(cbinord => cbinord.SumMoney ?? 0);
							decimal addedSum = m_AlreadyAddedReports.Sum(r => r.Count * r.Price * 0.01M * (security.NomValue ?? 0));
							decimal newSum = FastReportItems.Sum(r => r.Count * r.Price * 0.01M * (security.NomValue ?? 0));
							if (limit - addedSum < newSum)
							{
								RaiseSellAbort(string.Format(ORDER_SUM_OWERFLOW_MSG, security.Name));
								//throw new ViewModelSaveUserCancelledException();
								return false;
							}
						}
					}
				}
				else
				{
					OrdReport emptyReport = new OrdReport();
					foreach (CbInOrder cbInOrder in m_CbInOrderList)
					{

						Security security = DataContainerFacade.GetByID<Security>(cbInOrder.SecurityID);
						//если ограницение по количеству бумаг
						if (cbInOrder.Count.HasValue && cbInOrder.Count > 0)
						{
							//количество свободных ценных бумаг в распоряжении   
							long? rest = OrderReportsHelper.GetAvailableBuyOrSaleSecurityCountByOrder(m_CbInOrderList, m_AlreadyAddedReports, security, emptyReport);

							//если заполняли колонку количество - просто сумма CbInOrder'а
							long fastReportSecurityCount = FastReportItems.Where(r => r.Issue.SecurityID == cbInOrder.SecurityID).Select(r => r.Count).Sum();
							if (rest - fastReportSecurityCount < 0)
							{
								RaiseSellAbort(string.Format(ORDER_COUNT_OWERFLOW_MSG, security.Name));
								//throw new ViewModelSaveUserCancelledException();
								return false;
							}
						}
						else if (cbInOrder.SumMoney.HasValue) //ограничение по сумме сделки
						{
							//сумма новых отчетов
							decimal fastReportSecuritySumMoney = FastReportItems.Where(r => r.Issue.SecurityID == cbInOrder.SecurityID).Sum(r => r.Count * r.Price * 0.01M * (security.NomValue ?? 0));
							//сумма уже существующих отчетов
							decimal alreadyAddedSum = m_AlreadyAddedReports.Where(r => r.SecurityID == cbInOrder.SecurityID).Sum(report => report.Count * report.Price * 0.01M * (security.NomValue ?? 0));

							if (cbInOrder.SumMoney - alreadyAddedSum - fastReportSecuritySumMoney < 0)
							{
								RaiseSellAbort(string.Format(ORDER_SUM_OWERFLOW_MSG, security.Name));
								//throw new ViewModelSaveUserCancelledException();
								return false;
							}
						}
					}
				}
			}

			return base.BeforeExecuteSaveCheck();
		}

		protected override void ExecuteSave()
		{
			if (State == ViewModelState.Edit)
				foreach (FastReportItem item in FastReportItems)
				{
					if (item.Issue.CbInOrderID < 0) continue;

					OrdReport rep = new OrdReport
					{
						ReportDate = item.ReportDate,
						DateOrder = item.Date,
						DateDEPO = item.DEPODate,
						Count = item.Count,
						Price = item.Price,						
						OneNKD = item.OneNKD,
						Yield = item.Yield,
						OneNomValue = item.Issue.NomValue,
						ContragentStatus = item.Contragent,
						CbInOrderID = item.Issue.CbInOrderID,
						Sum = item.Summ
					};

					if (item.ID > 0)
					{
						//Если быстрый ввод уже сохраняли, то восстанавливаем ID из FastReportItem
						rep.ID = item.ID;
						DataContainerFacade.Save<OrdReport, long>(rep);
					}
					else
					{
						//Если быстрый ввод еще сохраняли, FastReportItem.ID устанавливается поле сохранения отчета
						rep.ID = DataContainerFacade.Save<OrdReport, long>(rep);
						item.ID = rep.ID;
					}
				}
			IsDataChanged = false;
		}

		private bool Validate()
		{
			foreach (FastReportItem item in FastReportItems)
			{
				if (item.Price <= 0 || item.Count <= 0)
					return false;
				if (IsBuyOrder && item.Yield <= 0)
					return false;
			}

			return true;
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

		public override string this[string columnName] => null;

	    public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
			var list = new List<KeyValuePair<Type, long>>();

			foreach (var item in FastReportItems)
			{
				list.Add(new KeyValuePair<Type, long>(typeof(OrdReport), item.ID));
			}

			return list;
		}
	}

	public class Issue
	{
		public long CbInOrderID { get; set; }
		public long SecurityID { get; set; }
		public string Name { get; set; }
		public decimal NomValue { get; set; }
	}

	public class FastReportItem : INotifyPropertyChanged
	{
		private readonly bool m_CurrencyIsRoubles;

		public FastReportItem(bool p_CurrencyIsRoubles)
		{
			m_CurrencyIsRoubles = p_CurrencyIsRoubles;
		}

		public long ID { get; set; }

		private Issue m_Issue;
		public Issue Issue
		{
			get
			{
				return m_Issue;
			}
			set
			{
				if (m_Issue != value)
				{
					m_Issue = value;
					RecalcNKD();
					OnPropertyChanged("Issue");
				}
			}
		}

		private DateTime m_Date;
		public DateTime Date
		{
			get
			{
				return m_Date;
			}
			set
			{
				if (m_Date != value)
				{
					m_Date = value;
					if (m_CurrencyIsRoubles)
						RecalcNKD();
					OnPropertyChanged("Date");
				}
			}
		}

		private DateTime m_ReportDate;
		public DateTime ReportDate
		{
			get
			{
				return m_ReportDate;
			}
			set
			{
				if (m_ReportDate != value)
				{
					m_ReportDate = value;
					OnPropertyChanged("ReportDate");
				}
			}
		}

		private DateTime m_DEPODate;
		public DateTime DEPODate
		{
			get
			{
				return m_DEPODate;
			}
			set
			{
				if (m_DEPODate != value)
				{
					m_DEPODate = value;
					if (!m_CurrencyIsRoubles)
						RecalcNKD();
					OnPropertyChanged("DEPODate");
				}
			}
		}

		private long m_Count;
		public long Count
		{
			get
			{
				return m_Count;
			}
			set
			{
				if (m_Count != value)
				{
					m_Count = value;
					RecalcNKD();
					OnPropertyChanged("Count");
					RecalcSum();
				}
			}
		}

		private decimal m_Price;
		public decimal Price
		{
			get
			{
				return m_Price;
			}
			set
			{
				if (m_Price != value)
				{
					m_Price = value;
					RecalcSum();
					OnPropertyChanged("Price");
				}
			}
		}

		private decimal? m_OneNKD;
		private decimal m_NKD_Calculated;
		public decimal? OneNKD
		{
			get { return m_OneNKD; }
			set
			{
				if (m_OneNKD != value)
				{
					m_OneNKD = value;
					OnPropertyChanged("OneNKD");
					RecalcSum();
				}
			}
		}

		private decimal m_Yield;
		public decimal Yield
		{
			get
			{
				return m_Yield;
			}
			set
			{
				if (m_Yield != value)
				{
					m_Yield = value;
					OnPropertyChanged("Yield");
				}
			}
		}

		private string m_Contragent;
		public string Contragent
		{
			get
			{
				return m_Contragent;
			}
			set
			{
				if (m_Contragent != value)
				{
					m_Contragent = value;
					OnPropertyChanged("Contragent");
				}
			}
		}
		public decimal Summ { get; private set; }

		public void RecalcSum()
		{
			if (Issue != null)
			{
				Summ = Issue.NomValue * Count * Price / 100 + (OneNKD ?? 0) * Count;
			}
		}

		public void RecalcNKD()
		{
			//Ближайшая выплата
			DateTime dt;
			if (m_CurrencyIsRoubles)
				dt = Date;
			else
				dt = DEPODate;

			List<Payment> m_PaymentsList = BLServiceSystem.Client.GetPaymentsListHib(Issue.SecurityID);
			Payment m_ClosestPayment;
			if (m_PaymentsList != null)
				m_ClosestPayment = OrderReportsHelper.GetClosestPayment(dt, m_PaymentsList);
			else
				m_ClosestPayment = null;

			//НКД на одну облигацию
			decimal perOne = 0;
			if (m_ClosestPayment != null)
			{
				decimal total;
				if (m_CurrencyIsRoubles)
				{
					total = OrderReportsHelper.CalcNKDForRubles(Date, m_ClosestPayment);
				}
				else
				{
					total = OrderReportsHelper.CalcNKDForDollars(DEPODate, m_ClosestPayment);
				}
				if (total < 0)
					perOne = 0;
				else
					perOne = Math.Round(total, 16);//Точность НКД 16
			}
			else
				perOne = 0;

			//НКД всего
			m_NKD_Calculated = perOne;
			OnPropertyChanged("NKD");
			RecalcSum();
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	public class NotAllItemsHaveCoursesEventArgs : EventArgs
	{
		public readonly List<DateTime> NotPresentCoursesDates;

		public NotAllItemsHaveCoursesEventArgs(List<DateTime> dates)
		{
			NotPresentCoursesDates = dates;
		}

		public string GetNotPresentCoursesDatesString()
		{
			return string.Join(", ", NotPresentCoursesDates.Select(date => date.ToShortDateString()));
		}
	}

	public class StringEventArgs : EventArgs
	{
		public readonly string Text;

		public StringEventArgs(string text)
		{
			Text = text;
		}
	}

}

