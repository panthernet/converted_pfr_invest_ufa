﻿
using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class BankConclusionViewModel : ViewModelCard
    {
        #region Properties
        private BankConclusion _BankConclusion;
        public BankConclusion BankConclusion
        {
            get { return _BankConclusion; }
            set
            {
                _BankConclusion = value;
                OnPropertyChanged("BankConclusion");
            }
        }

        public string Name
        {
            get { return _BankConclusion.Name; }
            set
            {
                _BankConclusion.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Code
        {
            get { return _BankConclusion.Code; }
            set
            {
                _BankConclusion.Code = value;
                OnPropertyChanged("Code");
            }
        }
        
        public string City
        {
            get { return _BankConclusion.City; }
            set
            {
                _BankConclusion.City = value;
                OnPropertyChanged("City");
            }
        }

        public DateTime ImportDate
        {
            get { return _BankConclusion.ImportDate; }
            set
            {
                _BankConclusion.ImportDate = value;
                OnPropertyChanged("ImportDate");
            }
        }
        #endregion

        #region конструкторы
        public BankConclusionViewModel()
            : base(typeof(BankConclusionListViewModel))
        {
            _BankConclusion = new BankConclusion();
            DataObjectTypeForJournal = typeof(BankConclusion);    
        }

        public BankConclusionViewModel(Type p_ListViewModel)
            : base(p_ListViewModel)
        {
            _BankConclusion = new BankConclusion();
            DataObjectTypeForJournal = typeof(BankConclusion);
        }

        public BankConclusionViewModel(IEnumerable<Type> p_ListViewModelCollection)
            : base(p_ListViewModelCollection)
        {
            _BankConclusion = new BankConclusion();
            DataObjectTypeForJournal = typeof(BankConclusion);
        }

        public BankConclusionViewModel(long identity)
            : base(typeof(RatingAgenciesListViewModel))
        {
            _BankConclusion = identity > 0 ? DataContainerFacade.GetByID<BankConclusion>(identity) : new BankConclusion();
            DataObjectTypeForJournal = typeof(BankConclusion);
        }

        #endregion


        #region Execute implementation

        protected override void ExecuteSave()
        {
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override bool CanExecuteDelete()
        {
            return false;
        }
        
        public override string this[string columnName] => "";

        #endregion


    }
}
