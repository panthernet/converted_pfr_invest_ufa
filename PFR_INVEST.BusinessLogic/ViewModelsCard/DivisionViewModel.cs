﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
	public class DivisionViewModel : ViewModelCard, IUpdateListenerModel
	{
		public Division Division { get; set; }
		private Person _selectedPerson;
		public Person SelectedPerson {
	        get { return _selectedPerson; }
			set
			{
				_selectedPerson = value;
				OnPropertyChanged("CanExecuteDeleteContact");
			}
        }

		public string Name
		{
			get { return Division.Name; }
			set { Division.Name = value; OnPropertyChanged("Name"); }
		}

		public string ShortName
		{
			get { return Division.ShortName; }
			set { Division.ShortName = value; OnPropertyChanged("ShortName"); }
		}


		private List<Person> _persons = new List<Person>();
		public List<Person> Persons
		{
			get { return _persons; }
			set { _persons = value; OnPropertyChanged("Persons"); }
		}

		#region Commands
		private readonly ICommand _deletePerson;
		public ICommand DeletePerson => _deletePerson;

	    #endregion

		public DivisionViewModel(long id, ViewModelState action)
			: base(typeof(DivisionListViewModel))
		{
			IgnorePropertyChanged("CanExecuteDeleteContact");

			DataObjectTypeForJournal = typeof(Division);
			if (action == ViewModelState.Create)
			{
				Division = new Division();
			}
			else
			{
				ID = id;
				Division = DataContainerFacade.GetByID<Division, long>(ID);
				State = ViewModelState.Edit;
			}

			_deletePerson = new DelegateCommand(o => CanExecuteDeletePerson(),
				o => ExecuteDeletePerson());
			RefreshLists();
		}
		public DivisionViewModel(Type pListViewModel)
			: base(pListViewModel)
		{

		}
		public DivisionViewModel(IEnumerable<Type> pListViewModelCollection)
			: base(pListViewModelCollection)
		{

		}

		#region Execute implementation

        public bool CanExecuteAddContact => (ID > 0);

	    /// <summary>
		/// Показывает активность кнопки "Удалить"
		/// </summary>
		public bool CanExecuteDeleteContact => HasContacts;

	    public Person SavePerson(Person person)
        {
            if (person.ID != 0) return person;
            person.DivisionId = Division.ID;
            person.ID = DataContainerFacade.Save<Person, long>(person);
            return person;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            if (BLServiceSystem.Client.GetPersonListHib(Division.ID).Count == 0) return true;
            DialogHelper.ShowError("Для удаления подразделения необходимо сначала удалить всех сотрудников!");
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            var division = DataContainerFacade.GetByID<Division, long>(ID);
            DataContainerFacade.Delete(division);
            base.ExecuteDelete(DocOperation.Delete);
        }

		public bool CanExecuteDeletePerson()
		{
			return SelectedPerson != null && !EditAccessDenied;
		}

		public void ExecuteDeletePerson()
		{
			if (!DialogHelper.ShowQuestion("Удалить выбранный элемент?", "Удаление")) return;
			if (SelectedPerson.ID != 0)
			{
				var contact = BLServiceSystem.Client.GetPersonHib(SelectedPerson.ID);
				if (contact != null)
				{
					var m = new PersonViewModel(SelectedPerson.ID, ViewModelState.Read);
					if (!m.BeforeExecuteDeleteCheck())
						return;
					m.ExecuteDelete();
				}
			}
			else
			{
				Persons.Remove(SelectedPerson);
			}
			RefreshLists(true);

			if (!Persons.Any())
			{
				DialogHelper.ShowAlert("Все сотрудники удалены.");
			}
		}

		public override bool CanExecuteSave()
		{
			return Validate() && IsDataChanged;
		}

	    protected override bool BeforeExecuteSaveCheck()
	    {
            var divisions = DataContainerFacade.GetList<Division>();
	        if (divisions.FirstOrDefault(a => a.Name == Division.Name && Division.ID != a.ID) != null)
	        {
	            DialogHelper.ShowError("Выбранное значение полного наименования подразделения уже присутствует в системе!\nУкажите другое значение полного наименования подразделения и повторите попытку сохранения.");
	            return false;
	        }
	        if (divisions.FirstOrDefault(a => a.ShortName == Division.ShortName && Division.ID != a.ID) != null)
            {
                DialogHelper.ShowError("Выбранное значение краткого наименования подразделения уже присутствует в системе!\nУкажите другое значение краткого наименования подразделения и повторите попытку сохранения.");
                return false;
            }

	        return true;
        }

	    protected override void ExecuteSave()
		{
		    if (!CanExecuteSave()) return;
		    switch (State)
		    {
		        case ViewModelState.Read:
		            break;
		        case ViewModelState.Edit:
		        case ViewModelState.Create:

		            if (State == ViewModelState.Create)
		            {
		                Division.ID = ID;
		                ID = DataContainerFacade.Save<Division, long>(Division);
		                Division.ID = ID;
		            }
		            else DataContainerFacade.Save<Division, long>(Division);

		            foreach (var item in Persons)
		                SavePerson(item);
		            break;
		    }
		}

	    #endregion

	    public void RefreshLists(bool onlyContacts = false)
		{
			bool wasChanged = IsDataChanged;
			if (Division.ID != 0)
				Persons = BLServiceSystem.Client.GetPersonListHib(Division.ID);
			IsDataChanged = wasChanged;

			OnPropertyChanged("CanExecuteDeleteContact");
		}

		public override string this[string columnName] => "";

	    private bool Validate()
		{
			return !string.IsNullOrWhiteSpace(Division.Name) && !string.IsNullOrWhiteSpace(Division.ShortName);
		}

		public bool HasContacts => _persons.Count > 0 && CanExecuteDeletePerson();

	    public void CreatePerson()
		{
		    if (!DialogHelper.CreatePFRContact(ID)) return;
		    RefreshLists(true);
		    OnPropertyChanged("PFRContactsList");
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(Person) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(Person)) 
			{
				RefreshLists();
			}
		}
	}
}
