﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class SupAgreementViewModel : ViewModelCard, IRequestCloseViewModel
    {
        public SupAgreement SupAgreement { get; private set; }
        public Contract Contract { get; private set; }
        private DateTime? _suspendDate;

        private List<string> _mSupAgreementKinds;

        public List<string> SupAgreementKinds => _mSupAgreementKinds ?? (_mSupAgreementKinds = State != ViewModelState.Create ?
                                                     new List<string> {SupAgreementIdentifier.PROLONGATION, SupAgreementIdentifier.CHANGE_NAME, SupAgreementIdentifier.OTHER} :
                                                     new List<string> {SupAgreementIdentifier.PROLONGATION, SupAgreementIdentifier.OTHER});

        public string SelectedSupAgreementKind
        {
            get
            {
                return SupAgreement.Kind;
            }
            set
            {
                SupAgreement.Kind = value;
                OnPropertyChanged(nameof(ExtentionSAVisible));
                if (IsExtentionSA)
                {
                    SupAgreementExtention = null;
                    SupAgreementSuspendDate = null;
                }
                OnPropertyChanged(nameof(SelectedSupAgreementKind));
            }
        }

        public bool IsExtentionSA => SupAgreementIdentifier.IsProlongation(SelectedSupAgreementKind);

        public Visibility ExtentionSAVisible => IsExtentionSA ? Visibility.Visible : Visibility.Collapsed;

        public string ContractNumber => Contract.ContractNumber;

        public string ContractPortfolioName => Contract.PortfolioName;

        public string UKFullName => Contract.GetLegalEntity().FullName;

        public string UKFormalName => Contract.GetLegalEntity().FormalizedName;

        public string Number
        {
            get
            {
                return SupAgreement.Number;
            }
            set
            {
                SupAgreement.Number = value;
                OnPropertyChanged(nameof(Number));
            }
        }

        public DateTime? SupAgreementDate
        {
            get
            {
                return SupAgreement.RegDate;
            }
            set
            {
                SupAgreement.RegDate = value;
                RecalcSuspendDate();
                OnPropertyChanged(nameof(SupAgreementDate));
            }
        }

        public long? SupAgreementExtention
        {
            get
            {
                return SupAgreement.Extention;
            }
            set
            {
                SupAgreement.Extention = value;
                RecalcSuspendDate();
                OnPropertyChanged(nameof(SupAgreementExtention));
            }
        }

        private void RecalcSuspendDate()
        {
            if (!IsExtentionSA) return;
            if (!SupAgreementDate.HasValue || !SupAgreementExtention.HasValue)
                SupAgreementSuspendDate = null;
            else
                SupAgreementSuspendDate = SupAgreementDate.Value.AddYears((int)SupAgreementExtention.Value).AddDays(1);
        }

        public DateTime? SupAgreementSuspendDate
        {
            get
            {
                return SupAgreement.SupendDate;
            }
            set
            {
                SupAgreement.SupendDate = value;
                OnPropertyChanged(nameof(SupAgreementSuspendDate));
            }
        }

        public SupAgreementViewModel(long id, ViewModelState action)
            : base(typeof(SIContractsListViewModel), typeof(VRContractsListViewModel))
        {
            DataObjectTypeForJournal = typeof (SupAgreement);
            if (action == ViewModelState.Create)
            {
                Contract = DataContainerFacade.GetByID<Contract, long>(id);
                //ищем следующий номер при помощи парисинга, так как номер 
                //может содержать буквы (таково требование)
                int nextNumber = 0;
                foreach (var supArg in Contract.GetSupAgreements())
                {
                    int lastNumber;
                    if (supArg.Number == null)
                        continue;
                    if (Int32.TryParse(Regex.Match(supArg.Number.Trim(), "^\\d+").Value , out lastNumber) && lastNumber > nextNumber) 
                        nextNumber = lastNumber;
                }
                nextNumber++;
                SupAgreement = new SupAgreement
                {
                    ContractID = id,
                    Number = nextNumber.ToString()
                };
            }
            else
            {
                ID = id;
                SupAgreement = DataContainerFacade.GetByID<SupAgreement, long>(ID);
                Contract = SupAgreement.GetContract();
            }

            RefreshConnectedCardsViewModels = RefreshParentViewModels;
        }

        private void RefreshParentViewModels()
        {
            if (GetViewModel == null) return;
            var vm = GetViewModel(typeof(SIContractViewModel)) as SIContractViewModel;
            if (vm != null)
            {
                vm.RefreshSupAgreements();
                vm.ContractSupAgreementEndDate = _suspendDate;
            }
        }

        private void SetContractSupAgreementSuspendDate()
        {
           // if (IsExtentionSA)
           // {
           //     SuspendDate = SupAgreement.SupendDate;
           // }
           // else
            //{
                if (Contract.ExtensionData != null)
                    Contract.ExtensionData.Remove("SupAgreements");
                /*IEnumerable<SupAgreement> sups = Contract.GetSupAgreements().ToList();
                if (sups.Any(sa => SupAgreementIdentifier.IsProlongation(sa.Kind)))
                {
                    _suspendDate = (from sup in sups
                                   where SupAgreementIdentifier.IsProlongation(sup.Kind)
                                   orderby sup.SupendDate ascending
                                   select sup.SupendDate).Last();
                }
                else _suspendDate = null;*/
                _suspendDate = ContractHelper.GetContractSupAgrEndDate(Contract.GetSupAgreements());

                //вычисляем корректную дату окончания договора с учетом доп. соглашений согласно http://jira.vs.it.ru/browse/DOKIPIV-385
                Contract.ContractSupAgreementEndDate = _suspendDate;
           // }
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    DataContainerFacade.Save<SupAgreement, long>(SupAgreement);
                    SetContractSupAgreementSuspendDate();
                    DataContainerFacade.Save<Contract, long>(Contract);
                    break;
                case ViewModelState.Create:
                    ID = DataContainerFacade.Save<SupAgreement, long>(SupAgreement);
                    SupAgreement.ID = ID;
                    SetContractSupAgreementSuspendDate();
                    DataContainerFacade.Save<Contract, long>(Contract);
                    _mSupAgreementKinds = null;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return CheckDeleteAccess(typeof(SupAgreementViewModel));
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(SupAgreement);
            base.ExecuteDelete(DocOperation.Delete);
        }

        private const string INVALID_SELECTED_SUPAGREEMENT_KIND = "Не указан вид соглашения";
        private const string INVALID_SUPAGREEMENT_DATE = "Не указана дата регистрации соглашения";
        private const string INVALID_SUPAGREEMENT_EXTENTION = "Не верно указан срок продления";
        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedSupAgreementKind": return string.IsNullOrEmpty(SelectedSupAgreementKind) ? INVALID_SELECTED_SUPAGREEMENT_KIND : null;
                    case "SupAgreementDate": return SupAgreementDate == null ? INVALID_SUPAGREEMENT_DATE : null;
                    case "Number": return Number.ValidateMaxLength(128);
                    case "SupAgreementExtention":
                        if (IsExtentionSA)
                            return !SupAgreementExtention.HasValue || SupAgreementExtention.Value <= 0 ? INVALID_SUPAGREEMENT_EXTENTION : null;
                        return null;
                    default:
                        return null;
                }
            }
        }

        private bool Validate()
        {
            return string.IsNullOrEmpty(this["SelectedSupAgreementKind"]) &&
                   string.IsNullOrEmpty(this["SupAgreementDate"]) &&
                   string.IsNullOrEmpty(this["SupAgreementExtention"]) &&
                   string.IsNullOrEmpty(this["Number"]) &&
                   string.IsNullOrEmpty(this["SupAgreementSuspendDate"]);
        }

        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose != null)
            {
                RequestClose(this, EventArgs.Empty);
            }
        }
    }
}
