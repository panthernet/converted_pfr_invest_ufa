﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class RatingViewModel : ViewModelCard
    {
        #region Fields
        //SecurityViewModel parentModel = null;
        public Rating Rating { get; private set; }
        private readonly IList<LegalEntity> _banks = BLServiceSystem.Client.GetBankListForDeposit();
        private readonly Dictionary<long, List<MultiplierRating>> _ratingBankMapping = new Dictionary<long, List<MultiplierRating>>();
        #endregion

        #region Properties

        public string RatingName
        {
            get { return Rating.RatingName; }
            set
            {
                if (Rating.RatingName != value)
                {
                    Rating.RatingName = value;
                    OnPropertyChanged("RatingName");
                }
            }
        }

        //public long Number
        //{
        //    get { return Rating.Number; }
        //    set
        //    {
        //        if (Rating.Number != value)
        //        {
        //            Rating.Number = value;
        //            OnPropertyChanged("Number");
        //        }
        //    }
        //}

        #endregion

        #region Execute Implementations
        protected override void ExecuteSave()
        {
            if (CanExecuteSave())
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Create:
                    case ViewModelState.Edit:
                        Rating.RatingName = Rating.RatingName.Trim();
                        if (Rating.RatingName.Equals(string.Empty))
                        {
                            DialogHelper.ShowWarning(INVALID_RATING_NAME, CAPTION);
                            return;
                        }

                        var list = BLServiceSystem.Client.GetRatingsListHib();

                        if (State == ViewModelState.Create
                           && list.Any(l => l.RatingName.ToLower().Equals(Rating.RatingName.ToLower())))
                        {
                            DialogHelper.ShowWarning(ALREADY_EXISTS_RATING_NAME, CAPTION);
                            return;
                        }
                        if (State == ViewModelState.Edit
                            && list.Any(l => l.RatingName.ToLower().Equals(Rating.RatingName.ToLower()) && l.ID != Rating.ID))
                        {
                            DialogHelper.ShowWarning(ALREADY_EXISTS_RATING_NAME, CAPTION);
                            return;
                        }

                        ID = Rating.ID = DataContainerFacade.Save<Rating, long>(Rating);
                        IsDataChanged = false;
                        //BLServiceSystem.Client.UpdateRating(this.Fields);
                        //this.ID = BLServiceSystem.Client.AddRating(this.Fields);
                        break;
                    default:
                        break;
                }
            }
        }

        public override bool CanExecuteSave()
        {
            return Validate() && IsDataChanged;
        }

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && DeleteAccessAllowed & !IsUsed(_banks);
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(Rating);
            base.ExecuteDelete(DocOperation.Delete);
        }

        #endregion

        private bool IsUsed(IList<LegalEntity> le)
        {
            return le.Any(legalEntity => _ratingBankMapping[legalEntity.ID].Any(r => r.RatingID == Rating.ID));
        }

        public RatingViewModel(long _id, ViewModelState state)
            : base(typeof(RatingsListViewModel))
        {
            DataObjectTypeForJournal = typeof (Rating);
            State = state;

            foreach (var bank in _banks)
            {
                _ratingBankMapping.Add(bank.ID, BLServiceSystem.Client.GetMultiplierRatingByLegalEntityID(bank.ID).ToList());
            }

            if (State == ViewModelState.Create)
            {
                ID = 0;
                Rating = new Rating();
                Rating.ID = 0;
                
            }
            else
            {
                ID = _id;
                if (InternalID > 0)
                {
                    Rating = DataContainerFacade.GetByID<Rating, long>(InternalID);
                }
            }

            Register();
        }

        void Register()
        {
            Rating.PropertyChanged += (sender, e) => { IsDataChanged = true; };
        }

        private const string INVALID_RATING_NAME = "Не введено наименование рейтинга";
        private const string RequiredFieldWarning = "Поле обязательно для заполнения";
        private const string ALREADY_EXISTS_RATING_NAME = "Рейтинг с таким именем уже существует";
        private const string ALREADY_EXISTS_RATING_NUMBER = "Рейтинг с таким номером уже существует";
        private const string CAPTION = "Отказ в выполнении операции";

        public override string this[string columnName]
        {
            get
            {
                //const string errorMessage = "Неверный формат данных";
                switch (columnName)
                {
                    case "RatingName":
                        return string.IsNullOrEmpty(RatingName) ? INVALID_RATING_NAME : null;
                    
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return
                string.IsNullOrWhiteSpace(this["RatingName"]) &&
                string.IsNullOrWhiteSpace(this["Number"]);
        }
    }
}
