﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsCard;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using License = PFR_INVEST.DataObjects.License;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
	public sealed class BankViewModel : ViewModelCard
	{


		private int _tabIndex;
		public int TabIndex
		{
			get { return _tabIndex; }
			set
			{
				_tabIndex = value;
				OnTabIndexChanged(value);
			}
		}

		private LegalEntity _bank;
		public LegalEntity Bank
		{
			get { return _bank; }
			set
			{
				_bank = value;
				OnPropertyChanged("Bank");
			}
		}

		private License _license;
		public License License
		{
			get { return _license; }
			set
			{
				_license = value;
				OnPropertyChanged("License");
			}
		}

		private Contragent _mContragent;
		public Contragent Contragent
		{
			get { return _mContragent; }
			set
			{
				if (_mContragent != value)
				{
					_mContragent = value;
					OnPropertyChanged("Contragent");
				}
			}
		}

		#region Ratings

		public List<string> FitchRatings => BankCalculator.FitchRatings.ToList();

	    public string SelectedFitchRating
		{
			get { return Bank.Fitch; }
			set
			{
				Bank.Fitch = value;
				OnPropertyChanged("SelectedFitchRating");
				OnPropertyChanged("Limit4Money");
				OnPropertyChanged("Limit4Requests");
			}
		}

		public List<string> SPRatings => BankCalculator.SPRatings.ToList();

	    public string SelectedSPRating
		{
			get { return Bank.Standard; }
			set
			{
				Bank.Standard = value;
				OnPropertyChanged("SelectedSPRating");
				OnPropertyChanged("Limit4Money");
				OnPropertyChanged("Limit4Requests");
			}
		}

		public List<string> MoodyRatings => BankCalculator.MoodyRatings.ToList();

	    public string SelectedMoodyRating
		{
			get { return Bank.Moody; }
			set
			{
				Bank.Moody = value;
				OnPropertyChanged("SelectedMoodyRating");
				OnPropertyChanged("Limit4Money");
				OnPropertyChanged("Limit4Requests");
			}
		}
		#endregion

		#region OwnCapital

		public decimal? OwnCapital
		{
			get { return Bank.Money; }
			set
			{
				Bank.Money = value;
				OnPropertyChanged("OwnCapital");
				OnPropertyChanged("LimitMult");
				OnPropertyChanged("Limit4Money");
				OnPropertyChanged("Limit4Requests");
			}
		}

		public DateTime? OwnCapitalDate
		{
			get { return Bank.MoneyDate; }
			set
			{
				Bank.MoneyDate = value;
				OnPropertyChanged("OwnCapitalDate");
			}
		}

		#endregion

		#region TotalVolume


		public decimal TotalDeposit
		{
			get
			{
				var summ = DepositList.Sum(d => d.PendingVolume);

				return summ ?? 0;
			}
		}

		public string TotalDepositText => Util.format(TotalDeposit);

	    #endregion

		public long? RegistrationNum
		{
			get
			{
				long v;
				if (long.TryParse(License.Number, out v))
				{
					return v;
				}

				return null;
			}
			set
			{
				License.Number = value?.ToString();
				Bank.RegistrationNum = value?.ToString();
				LicenseValidator.Reset();
				OnPropertyChanged("RegistrationNum");
			}
		}

		public DateTime? RegistrationDate
		{
			get { return License.RegistrationDate; }
			set
			{
				License.RegistrationDate = value;
				Bank.RegistrationDate = value;
				OnPropertyChanged("RegistrationDate");
			}
		}


		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create && Bank.GetContragent().StatusID != -1;
		}

		protected override void ExecuteDelete(int delType)
		{
			var ca = Bank.GetContragent(); ca.StatusID = -1;
			DataContainerFacade.Save<Contragent, long>(ca);
			base.ExecuteDelete(DocOperation.Archive);
		}

		/// <summary>
		/// Лимит на средства, млн. руб.
		/// </summary>
		public decimal Limit4Money => BankCalculator.Limit4Money(Bank);

	    /// <summary>
		/// Коэффициент (приказ Мин.Фина №24н от 17.02.2015)
		/// </summary>
		public decimal LimitMult => BankCalculator.LimitMult(Bank);

	    /// <summary>
		/// Лимит на средства с учетом размещенных депозитов, млн. руб.
		/// </summary>
		public decimal Limit4Requests
		{
			get
			{
				return Limit4Money - Math.Round((TotalDeposit / 1000000M), MidpointRounding.AwayFromZero);
				//return BankCalculator.Limit4Requests(this.Bank); 
			}
			set { OnPropertyChanged("Limit4Requests"); }
		}

		public bool NotAllRatingFilled
		{
			get
			{
				return MultiplierRatingsList.Select(r => r.AgencyID).Distinct().Count() < RatingAgencies.Count;
			}
		}

		public bool NotAllStockFilled
		{
			get
			{
				return StockCodeList.Select(r => r.StockID).Distinct().Count() < StockList.Count;
			}
		}

		#region PfrAgreementStatuses

		private List<string> _pfrAgreementStatuses = new List<string>();
		public List<string> PfrAgreementStatuses
		{
			get { return _pfrAgreementStatuses; }
			set { _pfrAgreementStatuses = value; OnPropertyChanged("PfrAgreementStatuses"); }
		}

		public string SelectedPfrAgreementStatus
		{
			get { return Bank.PFRAGRSTAT; }
			set
			{
				if (value == null)
					throw new Exception("Статус генерального соглашения с ПФР невозможно сбросить в NULL");

				if (Bank.PFRAGRSTAT == null)
				{
					var msg = "Статус генерального соглашения с ПФР: " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}
				else
				{
					if (Bank.PFRAGRSTAT.Equals(value))
						return;

					var msg = "Статус генерального соглашения с ПФР: " + Bank.PFRAGRSTAT + " -> " + value;
					JournalLogger.LogChangeStateEvent(this, msg);
				}

				Bank.PFRAGRSTAT = value;
				OnPropertyChanged("SelectedPfrAgreementStatus");
			}
		}
		#endregion

		private IList<BankDepositInfoItem> _depositList = new List<BankDepositInfoItem>();
		public IList<BankDepositInfoItem> DepositList
		{
			get { return _depositList; }
			set
			{
				_depositList = value;
				OnPropertyChanged("DepositList");
			}
		}

		private IList<BankConclusion> _statusList = new List<BankConclusion>();
		public IList<BankConclusion> StatusList
		{
			get { return _statusList; }
			set
			{
				_statusList = value;
				OnPropertyChanged("StatusList");
			}
		}

		private IList<LegalEntityHead> OriginalHeadList { get; set; }
		private ObservableList<LegalEntityHead> _headsList = new ObservableList<LegalEntityHead>();
		public ObservableList<LegalEntityHead> HeadsList
		{
			get { return _headsList; }
			private set { _headsList = value; OnPropertyChanged("HeadsList"); }
		}

		private IList<LegalEntityRating> OriginalRatingList { get; set; }
		private ObservableList<LegalEntityRating> _ratingsList = new ObservableList<LegalEntityRating>();

		public ObservableList<LegalEntityRating> RatingsList
		{
			get { return _ratingsList; }
			private set
			{
				_ratingsList = value;
				RefreshMultiplierRatings();
				OnPropertyChanged("RatingAgencies");
				OnPropertyChanged("Ratings");
				OnPropertyChanged("RatingsList");
				OnPropertyChanged("MultiplierHelperRatingsList");
			}
		}

		private ObservableList<MultiplierRating> _multiplierRatingsList = new ObservableList<MultiplierRating>();
		public ObservableList<MultiplierRating> MultiplierRatingsList
		{
			get { return _multiplierRatingsList; }
			private set
			{
				_multiplierRatingsList = value;
				OnPropertyChanged("MultiplierRatingsList");
			}
		}

		private ObservableList<MultiplierRatingHelper> _multiplierHelperRatingsList = new ObservableList<MultiplierRatingHelper>();
		public ObservableList<MultiplierRatingHelper> MultiplierHelperRatingsList
		{
			get { return _multiplierHelperRatingsList; }
			private set
			{
				_multiplierHelperRatingsList = value;
				OnPropertyChanged("MultiplierHelperRatingsList");
			}
		}


		private void RefreshMultiplierRatings()
		{
			MultiplierRatingsList = new ObservableList<MultiplierRating>(BLServiceSystem.Client.GetMultiplierRatingByLegalEntityID(ID));
			RefreshMultiplierRatingsHelper();

		}

		private void RefreshMultiplierRatingsHelper()
		{
			var res = from mrl in MultiplierRatingsList
					  from ra in RatingAgencies
					  from r in Ratings
					  from rl in RatingsList
					  where mrl.AgencyID == ra.ID && r.ID == mrl.RatingID && rl.MultiplierRatingID == mrl.ID
					  select new MultiplierRatingHelper
					  {
								 ID = mrl.ID,
								 AgencyID = mrl.AgencyID,
								 RatingID = mrl.RatingID,
								 Number = mrl.Number,
								 Multiplier = mrl.Multiplier,
								 Name = ra.Name,
								 RatingName = r.RatingName,
								 ShortDate = rl.ShortDate

							 };

			var temp = new ObservableList<MultiplierRatingHelper>();
			res.ForEach(r => temp.Add(r));
			MultiplierHelperRatingsList = temp;
		}


		public IList<Stock> StockList { get; protected set; }

		private IList<BankStockCode> OriginalStockCodeList { get; set; }
		private ObservableList<BankStockCode> _stockCodeList = new ObservableList<BankStockCode>();
		public ObservableList<BankStockCode> StockCodeList
		{
			get { return _stockCodeList; }
			private set
			{
				_stockCodeList = value;
				OnPropertyChanged("StockCodeList");
			}
		}

		public ICommand AddPerson { get; private set; }
		public ICommand DeletePerson { get; private set; }
		public ICommand ShowDeletedPersonList { get; private set; }

		private List<LegalEntityCourier> _personList = new List<LegalEntityCourier>();
		public List<LegalEntityCourier> PersonList
		{
			get
			{
				return _personList;
			}
			set
			{
				_personList = value;
				OnPropertyChanged("PersonList");
			}
		}

		/*
		private IList<OwnCapitalHistory> _ownCapitalList = new ObservableList<OwnCapitalHistory>();
		public IList<OwnCapitalHistory> OwnCapitalList
		{
			get { return this._ownCapitalList; }
			private set
			{
				this._ownCapitalList = value;
				this.OnPropertyChanged("OwnCapitalList");
			}
		}
		*/


		private List<LegalEntityCourier> _personListDeleted = new List<LegalEntityCourier>();
		public List<LegalEntityCourier> PersonListDeleted
		{
			get
			{
				return _personListDeleted;
			}
			set
			{
				_personListDeleted = value;
				OnPropertyChanged("CourierListDeleted");
			}
		}

		private List<LegalEntityCourier> _personListChanged = new List<LegalEntityCourier>();
		public List<LegalEntityCourier> PersonListChanged
		{
			get
			{
				return _personListChanged;
			}
			set
			{
				_personListChanged = value;
				OnPropertyChanged("CourierListChanged");
			}
		}

		private IList<Rating> _ratings = new List<Rating>();
		public IList<Rating> Ratings
		{
			get { return _ratings; }
			private set { _ratings = value; OnPropertyChanged("Ratings"); }
		}

		private IList<RatingAgency> _ratingAgencies = new List<RatingAgency>();
		public IList<RatingAgency> RatingAgencies
		{
			get { return _ratingAgencies; }
			private set { _ratingAgencies = value; OnPropertyChanged("RatingAgencies"); }
		}


		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;

			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Edit:
				case ViewModelState.Create:
					/*Contragent.Name = Bank.FormalizedName;
					Contragent.ID = DataContainerFacade.Save<Contragent, long>(Contragent);
					Bank.ContragentID = Contragent.ID;
					Bank.ID = DataContainerFacade.Save<LegalEntity, long>(Bank);
					License.LegalEntityID = Bank.ID;
					License.ID = DataContainerFacade.Save<License, long>(License);

					var deletedHeads = OriginalHeadList.Where(h => !HeadsList.Contains(h));
					foreach (var h in deletedHeads) DataContainerFacade.Delete(h);
					foreach (var h in HeadsList)
					{
						h.LegalEntityID = Bank.ID;
						h.ID = DataContainerFacade.Save(h);
					}

					//Ratings
					var deletedRatings = OriginalRatingList.Where(h => !RatingsList.Select(r => r.ID).Contains(h.ID)).ToList();
					foreach (var h in deletedRatings)
					{
						DataContainerFacade.Delete(h);
					}
					foreach (var h in RatingsList)
					{
						h.LegalEntityID = Bank.ID;
						h.ID = DataContainerFacade.Save(h);

					}
					OriginalRatingList = Bank.GetRatings();

					//StockCodes

					var deletedStockCodes = OriginalStockCodeList.Where(h => !StockCodeList.Select(r => r.ID).Contains(h.ID)).ToList();
					foreach (var h in deletedStockCodes) DataContainerFacade.Delete(h);
					foreach (var h in StockCodeList)
					{
						h.BankID = Bank.ID;
						h.ID = DataContainerFacade.Save(h);

					}
					OriginalStockCodeList = Bank.GetStockCodes();

					ID = Bank.ID;

					//Persons
					SavePersonList();
					*/

					var result = BLServiceSystem.Client.SaveBank(Contragent,
						Bank,
						License,
						HeadsList.ToList(),
						OriginalHeadList.Where(h => !HeadsList.Contains(h)).Select(a => a.ID).ToList(),
						RatingsList.ToList(),
						OriginalRatingList.Where(h => !RatingsList.Select(r => r.ID).Contains(h.ID)).Select(a => a.ID).ToList(),
						StockCodeList.ToList(),
						OriginalStockCodeList.Where(h => !StockCodeList.Select(r => r.ID).Contains(h.ID)).Select(a => a.ID).ToList(),
						PersonListChanged);
					ID = Bank.ID = result.Key.Value;
					License.ID = result.Value2.Value;
					License.LegalEntityID = Bank.ID;
					Bank.ContragentID = Contragent.ID = result.Value.Value;

					//Не забываем очистить всякие списки изменений
					PersonListChanged.Clear();

					//Не забываем обновить все нужные данные из базы после сохранения
					ReloadVisitedTabs();
					break;
			}
		}

		public string ValidateStockCodeUnique(BankStockCode bsc)
		{
			var existsStock = DataContainerFacade.GetListByProperty<BankStockCode>("StockCode", bsc.StockCode).Where(x => x.StockID == bsc.StockID && x.BankID != bsc.BankID).ToList();
			if (existsStock.Any())
			{
				var stock = DataContainerFacade.GetByID<Stock>(existsStock.First().StockID);
				var bank = DataContainerFacade.GetByID<LegalEntity>(existsStock.First().BankID);
				return $"Биржевой код \"{bsc.StockCode}\" уже зарегистрирован для биржи \"{stock.Name}\" в банке \"{bank.ShortName}\"";
			}
			return null;
		}

		public override bool CanExecuteSave()
		{
			return string.IsNullOrEmpty(Validate()) && IsDataChanged;
		}

		private void Register()
		{
			License.PropertyChanged += (sender, e) => { IsDataChanged = true; };
			Bank.PropertyChanged += (sender, e) => { IsDataChanged = true; };
			Bank.OnValidate += Bank_Validate;
			License.OnValidate += License_Validate;

			LicenseValidator = new AsyncValidator(() =>
			{
				var result = BLServiceSystem.Client.CheckLicenseUnique(Bank.ID, License.Number, Contragent.TypeName);
				return result.IsSuccess ? null : result.ErrorMessage;
			},
			() => { OnPropertyChanged("RegistrationNum"); });


			RefreshConnectedCardsViewModels = RefreshRatingAgencyCards;
		}

		private void RefreshRatingAgencyCards()
		{
		    var list = GetViewModelsList?.Invoke(typeof(RatingAgencyViewModel));
		    list?.Cast<RatingAgencyViewModel>().ToList().ForEach(raVM => raVM.RefreshRatingBankMapping());
		}

		#region Execute Person

		private bool CanExecuteAddPerson()
		{
			return EditAccessAllowed;
		}

		private void ExecuteAddPerson()
		{
			var x = new LegalEntityCourier { Type = LegalEntityCourier.Types.Person };
			var res = DialogHelper.EditBankPerson(x, true);
			if (res)
			{
				PersonList.Add(x);
				if (!PersonListChanged.Contains(x))
					PersonListChanged.Add(x);

				PersonList = PersonList.ToList();//Refresh display table
				OnPropertyChanged("PersonList");
			}
		}

		private bool CanExecuteDeletePerson()
		{
			//If have selected item in grid
			return DeleteAccessAllowed && FocusedPerson != null;
		}

		private void ExecuteDeletePerson()
		{
			if (FocusedPerson == null)
				return;

			var x = FocusedPerson;

			PersonList.Remove(x);

			x.MarkDeleted();

			PersonListDeleted.Add(x);
			if (!PersonListChanged.Contains(x))
				PersonListChanged.Add(x);

			PersonList = PersonList.ToList();
			OnPropertyChanged("PersonList");
		}

		private bool CanExecuteShowDeletedPersonList()
		{
			return PersonListDeleted.Count > 0;
		}

		private void ExecuteShowDeletedPersonList()
		{
			DialogHelper.ShowNpfCourierDeletedList(PersonListDeleted);
		}

		public void EditPerson(LegalEntityCourier x)
		{
			if (x == null)
				return;

			if (x.LetterOfAttorneyList == null || !IsDataChanged)
				x.LetterOfAttorneyList = DataContainerFacade.GetListByProperty<LegalEntityCourierLetterOfAttorney>("LegalEntityCourierID", x.ID).Where(l => l.StatusID != -1).ToList();
			if (x.CertificatesList == null || !IsDataChanged)
				x.CertificatesList = DataContainerFacade.GetListByProperty<LegalEntityCourierCertificate>("LegalEntityCourierID", x.ID).Where(c => c.StatusID != -1).ToList();

			var res = DialogHelper.EditBankPerson(x, false);
			if (res)
			{
				if (!PersonListChanged.Contains(x))
					PersonListChanged.Add(x);

				PersonList = PersonList.ToList();//Refresh display table
				OnPropertyChanged("PersonList");
			}
		}

		public LegalEntityCourier FocusedPerson { get; set; }

		private void LoadPersonList(LegalEntity legalEntity)
		{
			if (legalEntity == null || legalEntity.ID == 0)
			{
				PersonList = new List<LegalEntityCourier>();
				PersonListDeleted = new List<LegalEntityCourier>();
				return;
			}

			var xL = DataContainerFacade.GetListByProperty<LegalEntityCourier>("LegalEntityID", legalEntity.ID);

			var persons = xL.Where(x => x.Type == LegalEntityCourier.Types.Person).ToList();
			PersonList = persons.Where(x => !x.IsDeleted).ToList();
			PersonListDeleted = persons.Where(x => x.IsDeleted).ToList();
			OnPropertyChanged("PersonList");
			OnPropertyChanged("PersonListDeleted");
		}

		/*private void SavePersonList()
		{
			foreach (var x in PersonListChanged)
			{
				x.LegalEntityID = ID;
				x.LetterOfAttorneyExpireAlert = x.SignatureExpireAlert = (int)LegalEntityCourier.AlertTypes.NotAlerted;
				var cid = DataContainerFacade.Save(x);
				x.ID = cid;

				if (x.LetterOfAttorneyListChanged != null)
				{
					foreach (var y in x.LetterOfAttorneyListChanged)
					{
						y.LegalEntityCourierID = cid;
						y.ID = DataContainerFacade.Save(y);
					}

					x.LetterOfAttorneyListChanged = null;
				}
				if (x.LetterOfAttorneyListDeleted != null)
				{
					x.LetterOfAttorneyListDeleted.ForEach(l =>
					{
						if (l.ID > 0)
						{
							DataContainerFacade.Delete<LegalEntityCourierLetterOfAttorney>(l.ID);
							BLServiceSystem.Client.SaveDeleteEntryLog(typeof(LegalEntityCourierLetterOfAttorney).Name, string.Format("Доверенность '{0}'", l.Number), l.ID, 1);
						}
					}
					);
					x.LetterOfAttorneyListDeleted = null;
				}

				if (x.CertificatesListChanged != null)
				{
					foreach (var y in x.CertificatesListChanged)
					{
						y.LegalEntityCourierID = cid;
						y.ID = DataContainerFacade.Save(y);
					}

					x.CertificatesListChanged = null;
				}
				if (x.CertificatesListDeleted != null)
				{
					x.CertificatesListDeleted.ForEach(l =>
					{
						if (l.ID > 0)
						{
							DataContainerFacade.Delete<LegalEntityCourierCertificate>(l.ID);
							BLServiceSystem.Client.SaveDeleteEntryLog(typeof(LegalEntityCourierCertificate).Name, string.Format("Сертификат '{0}'", l.Signature), l.ID, 1);
						}
					});
                    
					x.CertificatesListDeleted = null;
				}
			}
		}*/
		#endregion

	    private void RatingsList_Changed(object sender, EventArgs e)
		{
			IsDataChanged = true;
			//OnPropertyChanged("NotAllRatingFilled");
			OnPropertyChanged("Limit4Money");
			OnPropertyChanged("LimitMult");

			OnPropertyChanged("Limit4Requests");
		}

		#region Validate

		private AsyncValidator LicenseValidator { get; set; }


		private void License_Validate(object sender, BaseDataObject.ValidateEventArgs e)
		{
			var license = (sender as License);
			if (license != null)
			{
				var error = "Поле обязательное для заполнения";
				switch (e.PropertyName)
				{
					case "Number": e.Error = string.IsNullOrWhiteSpace(license.Number) ? error : null;
						break;
				}
			}
		}

		private void Bank_Validate(object sender, BaseDataObject.ValidateEventArgs e)
		{
			var bank = (sender as LegalEntity);
			if (bank != null)
			{
				var error = "Поле обязательное для заполнения";
				switch (e.PropertyName)
				{
					case "FullName": e.Error = string.IsNullOrWhiteSpace(bank.FullName) ? error : null;
						break;
					case "RegistrationNum": e.Error = string.IsNullOrWhiteSpace(bank.RegistrationNum) ? error : null;
						break;
					//case "Registrator": e.Error = string.IsNullOrWhiteSpace(bank.Registrator) ? error : null;
					//    break;
					case "ShortName": e.Error = string.IsNullOrWhiteSpace(bank.ShortName) ? error : null;
						break;
					case "FormalizedName": e.Error = string.IsNullOrWhiteSpace(bank.FormalizedName) ? error : null;
						break;
					case "LegalAddress": e.Error = string.IsNullOrWhiteSpace(bank.LegalAddress) ? error : null;
						break;
					case "PostAddress": e.Error = string.IsNullOrWhiteSpace(bank.PostAddress) ? error : null;
						break;
					case "Phone": e.Error = string.IsNullOrWhiteSpace(bank.Phone) ? error : null;
						break;

					case "CorrAcc": e.Error = string.IsNullOrWhiteSpace(bank.CorrAcc) ? error : null;
						break;
					case "BIK": e.Error = string.IsNullOrWhiteSpace(bank.BIK) ? error : null;
						break;
					case "INN": e.Error = (string.IsNullOrWhiteSpace(bank.INN) || (Bank.INN.Length != 10)) ? "Длина поля должна равняться 10" : null;
						break;
					case "OKPP": e.Error = string.IsNullOrWhiteSpace(bank.OKPP) ? error : null;
						break;

					case "PFRAGR": e.Error = string.IsNullOrWhiteSpace(bank.PFRAGR) ? error : null;
						break;
					case "PFRAGRDATE": e.Error = bank.PFRAGRDATE == null ? error : null;
						break;
				}
				if (string.IsNullOrEmpty(e.Error))
				{
					//проверка максимальной допустимой длины поля
					switch (e.PropertyName)
					{
						case "FullName": e.Error = ValidateLength(bank.FullName, 512); break;
						case "RegistrationNum": e.Error = ValidateLength(bank.RegistrationNum, 40); break;
						case "Registrator": e.Error = ValidateLength(bank.Registrator, 512); break;
						case "ShortName": e.Error = ValidateLength(bank.ShortName, 128); break;
						case "FormalizedName": e.Error = ValidateLength(bank.FormalizedName, 512); break;
						case "LegalAddress": e.Error = ValidateLength(bank.LegalAddress, 512); break;
						case "StreetAddress": e.Error = ValidateLength(bank.StreetAddress, 512); break;
						case "PostAddress": e.Error = ValidateLength(bank.PostAddress, 512); break;
						case "Phone": e.Error = ValidateLength(bank.Phone, 128); break;
						case "Fax": e.Error = ValidateLength(bank.Fax, 128); break;
						case "EAddress": e.Error = ValidateEmail(bank.EAddress) ?? ValidateLength(bank.EAddress, 128); break;
						case "Site": e.Error = ValidateLength(bank.Site, 128); break;

						case "CorrAcc": e.Error = ValidateLengthExact(bank.CorrAcc, 20); break;
						case "CorrNote": e.Error = ValidateLength(bank.CorrNote, 256); break;

						case "BIK": e.Error = ValidateLengthExact(bank.BIK, 9); break;
						case "INN": e.Error = ValidateLengthExact(bank.INN, 10); break;
						case "OKPP": e.Error = ValidateLengthExact(bank.OKPP, 9); break;

						case "PFRAGR": e.Error = ValidateLength(bank.PFRAGR, 20); break;
					}
				}
			}
		}

		private string ValidateEmail(string value)
		{
			value = (value ?? string.Empty).Trim();

			if (string.IsNullOrEmpty(value) || DataTools.IsEmail(value)) return null;
		    return "Неверный формат";

		    //string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
			//      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
			//      @".)+))([a-zA-Z]{2,5}|[0-9]{1,5})(\]?)$";
			//Regex re = new Regex(strRegex);
			//if (string.IsNullOrEmpty(value) || re.IsMatch(value))
			//    return null;
			//else
			//    return "Неверный формат";
		}

		private string ValidateLengthExact(string value, int length)
		{
			var error = $"Длина поля должна равняться {length}";
			return (value ?? string.Empty).Trim().Length != length ? error : null;
		}

		private string ValidateLength(string value, int length)
		{
			const string error = "слишком длинное значение поля";
			return (value ?? string.Empty).Length > length ? error : null;
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			if (!LicenseValidator.Validate())
			{
				DialogHelper.ShowError(LicenseValidator.Error);
				return false;
			}

			string error = null;
			if (StockCodeList.Any(x => (error = ValidateStockCodeUnique(x)) != null))
			{
				DialogHelper.ShowError(error);
				return false;
			}
			return true;
		}
		private string Validate()
		{
			const string bankFields = "FullName|ShortName|FormalizedName|RegistrationNum|Registrator|LegalAddress|StreetAddress|PostAddress|Phone|CorrAcc|CorrNote|BIK|INN|OKPP|EAddress|PFRAGR|PFRAGRDATE";
			const string licenseFields = "Number";
			const string selfFiels = "RegistrationNum|RegistrationDate|OwnCapital";

			foreach (var error in selfFiels.Split('|').Select(key => this[key]).Where(error => !string.IsNullOrWhiteSpace(error)))
				return error;
			foreach (var e in bankFields.Split('|').Select(key => new BaseDataObject.ValidateEventArgs(key)))
			{
				Bank_Validate(Bank, e);
				if (!string.IsNullOrEmpty(e.Error))
					return e.Error;
			}
			foreach (var e in licenseFields.Split('|').Select(key => new BaseDataObject.ValidateEventArgs(key)))
			{
				License_Validate(Bank, e);
				if (!string.IsNullOrEmpty(e.Error))
					return e.Error;
			}
			if (HeadsList.Any(h => !string.IsNullOrEmpty(h["FullName"])
								   || !string.IsNullOrEmpty(h["Position"])))
				return "Неверно заполнен контакт";

			return !LicenseValidator.ValidateIfNot() ? LicenseValidator.Error : "";
		}

		#endregion Validate

		public DelegateCommand<LegalEntityHead> DeleteHeadCommand { get; private set; }
		public DelegateCommand<LegalEntityHead> EditHeadCommand { get; private set; }
		public DelegateCommand<object> AddHeadCommand { get; private set; }

		public DelegateCommand<MultiplierRating> DeleteRatingCommand { get; private set; }
		public DelegateCommand<MultiplierRating> EditRatingCommand { get; private set; }
		public DelegateCommand<object> AddRatingCommand { get; private set; }

		public DelegateCommand<object> ViewOwnCapitalHistoryCommand { get; private set; }

		public DelegateCommand<BankStockCode> DeleteStockCodeCommand { get; private set; }
		public DelegateCommand<BankStockCode> EditStockCodeCommand { get; private set; }
		public DelegateCommand<object> AddStockCodeCommand { get; private set; }

		public void RegisterHeadCommands()
		{
			DeleteHeadCommand = new DelegateCommand<LegalEntityHead>(h => HeadsList.Count > 0 && h != null && EditAccessAllowed,
				h =>
				{
					if (DialogHelper.ShowExclamation("Удалить контакт?", $"Удаление контакта {h.FullName}", true))
						HeadsList.Remove(h);
				});
			EditHeadCommand = new DelegateCommand<LegalEntityHead>(h => HeadsList.Count > 0 && h != null && EditAccessAllowed,
				h =>
				{
					if (DialogHelper.EditLegalEntityHead(h, false))
					{
						IsDataChanged = true;
					}
				});
			AddHeadCommand = new DelegateCommand<object>(o => EditAccessAllowed, o =>
				{
					var h = new LegalEntityHead { LegalEntityID = Bank.ID };
					if (DialogHelper.EditLegalEntityHead(h, true))
					{
						HeadsList.Add(h);
					}
				});
			OnPropertyChanged("AddHeadCommand");
			OnPropertyChanged("EditHeadCommand");
			OnPropertyChanged("DeleteHeadCommand");
		}

		public void RegisterOwnCapitalCommands()
		{
			ViewOwnCapitalHistoryCommand = new DelegateCommand<object>(o =>
			{
				if (Bank == null || Bank.ID == 0)
				{
					DialogHelper.ShowExclamation("Данная функция будет доступна после сохранения формы Банка!", "Внимание");
					return;
				}
				DialogHelper.ViewOwnCapitalHistory(Bank.ID);

				FillInOwnCapital();
			});
			OnPropertyChanged("ViewOwnCapitalHistoryCommand");

		}

		public void RegisterRatingCommands()
		{
			DeleteRatingCommand = new DelegateCommand<MultiplierRating>(r => RatingsList.Count > 0 && r != null && DeleteAccessAllowed,
				r =>
				{
					var msg = $"Удалить рейтинг '{RatingAgencies.First(a => a.ID == r.AgencyID).Name}' ?";
					if (!DialogHelper.ShowExclamation(msg, "Удаление рейтинга", true)) return;
					var leR = RatingsList.First(x => x.MultiplierRatingID == r.ID);
					RatingsList.Remove(leR);
					MultiplierRatingsList.Remove(MultiplierRatingsList.FirstOrDefault(mrl => mrl.ID == r.ID));
					RefreshMultiplierRatingsHelper();
				});
			EditRatingCommand = new DelegateCommand<MultiplierRating>(r => RatingsList.Count > 0 && r != null && EditAccessAllowed,
				r =>
				{
					var leR = RatingsList.First(x => x.MultiplierRatingID == r.ID); if (DialogHelper.EditLegalEntityRating(leR, false, null))
					{
						IsDataChanged = true;
						RatingAgencies = DataContainerFacade.GetList<RatingAgency>();
						Ratings = DataContainerFacade.GetList<Rating>();
						MultiplierRatingsList.Remove(MultiplierRatingsList.FirstOrDefault(mrl => mrl.ID == r.ID));
						MultiplierRatingsList.Add(DataContainerFacade.GetByID<MultiplierRating>(leR.MultiplierRatingID));
						RefreshMultiplierRatingsHelper();
					}
				});
			AddRatingCommand = new DelegateCommand<object>(o => NotAllRatingFilled && EditAccessAllowed,
				o =>
				{
					var h = new LegalEntityRating { LegalEntityID = Bank.ID, Date = DateTime.Now };

					var excludedAgencies = MultiplierRatingsList.Where(ra => ra.AgencyID.HasValue).Select(ra => ra.AgencyID.Value).ToList();
					if (DialogHelper.EditLegalEntityRating(h, true, excludedAgencies))
					{
						RatingAgencies = DataContainerFacade.GetList<RatingAgency>();
						Ratings = DataContainerFacade.GetList<Rating>();
						RatingsList.Add(h);
						MultiplierRatingsList.Add(DataContainerFacade.GetByID<MultiplierRating>(h.MultiplierRatingID));
						RefreshMultiplierRatingsHelper();
					}
				});
			OnPropertyChanged("AddRatingCommand");
			OnPropertyChanged("EditRatingCommand");
			OnPropertyChanged("DeleteRatingCommand");
		}

		public void RegisterStockCodeCommands()
		{
			DeleteStockCodeCommand = new DelegateCommand<BankStockCode>(r => StockCodeList.Count > 0 && r != null && DeleteAccessAllowed,
					r =>
					{
						var msg = $"Удалить биржевой код для биржи '{StockList.First(a => a.ID == r.StockID).Name}' ?";
						if (DialogHelper.ShowExclamation(msg, "Удаление биржевого кода", true))
						{
							StockCodeList.Remove(r);
							IsDataChanged = true;
						}
					});
			EditStockCodeCommand = new DelegateCommand<BankStockCode>(r => StockCodeList.Count > 0 && r != null && EditAccessAllowed,
				r =>
				{
					var excludedAgencies = StockCodeList.Select(ra => ra.StockID).ToList();
					excludedAgencies.Remove(r.StockID);
					if (DialogHelper.EditBankStockCode(r, false, excludedAgencies))
					{
						IsDataChanged = true;
					}
				});
			AddStockCodeCommand = new DelegateCommand<object>(o => NotAllStockFilled && EditAccessAllowed,
				o =>
				{
					if (State == ViewModelState.Create)
					{
						DialogHelper.ShowAlert("Для добавления Биржевого кода необходимо сохранить текущий банк!\n (Для сохранения банка необходимо заполнить все обязательные поля и нажать кнопку \"Сохранить\")");
						return;
					}

					var h = new BankStockCode { BankID = Bank.ID };

					var excludedAgencies = StockCodeList.Select(ra => ra.StockID).ToList();
					if (DialogHelper.EditBankStockCode(h, true, excludedAgencies))
					{
						StockCodeList.Add(h);
						IsDataChanged = true;
					}
				});
			OnPropertyChanged("AddStockCodeCommand");
			OnPropertyChanged("EditStockCodeCommand");
			OnPropertyChanged("DeleteStockCodeCommand");
		}

		private void RegisterPersonCommands()
		{
			AddPerson = new DelegateCommand(o => CanExecuteAddPerson(), o => ExecuteAddPerson());
			DeletePerson = new DelegateCommand(o => CanExecuteDeletePerson(), o => ExecuteDeletePerson());
			ShowDeletedPersonList = new DelegateCommand(o => CanExecuteShowDeletedPersonList(), o => ExecuteShowDeletedPersonList());
			OnPropertyChanged("AddPerson");
			OnPropertyChanged("DeletePerson");
			OnPropertyChanged("ShowDeletedPersonList");
		}

		#region ЗАГРУЗКА ДАННЫХ ПО ТАБАМ
		private readonly int[] _visitedTabs = new int[9];
		private void OnTabIndexChanged(int value)
		{
			if (_visitedTabs[value] != 0) return;
			var isChanged = IsDataChanged;
			switch (value)
			{
				case 0: //Основная информация
				case 1: //адрес
				case 2: //лицензии
					break;
				case 3: //контакты
					RegisterHeadCommands();
					if (ID > 0)
					{
						OriginalHeadList = Bank.GetHeads();
						HeadsList = new ObservableList<LegalEntityHead>(OriginalHeadList);
						HeadsList.CollectionChanged += (sender, e) => { IsDataChanged = true; };
						HeadsList.ItemChanged += (sender, e) => { IsDataChanged = true; };
						OnPropertyChanged("OriginalHeadList");
						OnPropertyChanged("HeadsList");
					}
					break;
				case 4: //банковские реквизиты
					StockList = DataContainerFacade.GetList<Stock>();
					OnPropertyChanged("StockList");
					RegisterStockCodeCommands();
					if (ID > 0)
					{
						OriginalStockCodeList = Bank.GetStockCodes();
						StockCodeList = new ObservableList<BankStockCode>(OriginalStockCodeList);
						StockCodeList.CollectionChanged += (sender, e) => { IsDataChanged = true; };
						StockCodeList.ItemChanged += (sender, e) => { IsDataChanged = true; };
						OnPropertyChanged("OriginalStockCodeList");
						OnPropertyChanged("StockCodeList");
					}
					break;
				case 5: //рейтинги и лимиты
			        FillDepositList();
                    RegisterRatingCommands();
					RatingAgencies = DataContainerFacade.GetList<RatingAgency>();
					Ratings = DataContainerFacade.GetList<Rating>();
					if (ID > 0)
					{
						OriginalRatingList = Bank.GetRatings();
						RatingsList = new ObservableList<LegalEntityRating>(OriginalRatingList);
						MultiplierRatingsList = new ObservableList<MultiplierRating>(BLServiceSystem.Client.GetMultiplierRatingByLegalEntityID(ID));
						OnPropertyChanged("OriginalRatingList");
						OnPropertyChanged("MultiplierRatingsList");
					}
					RatingsList.CollectionChanged += RatingsList_Changed;
					RatingsList.ItemChanged += RatingsList_Changed;
					OnPropertyChanged("RatingAgencies");
					OnPropertyChanged("Ratings");
					OnPropertyChanged("RatingsList");
					break;
				case 6: //депозиты
                    FillDepositList();
					break;
				case 7: //история изменения статуса
					if (ID > 0)
					{
						StatusList = Bank.GetBankStatuses();
						OnPropertyChanged("StatusList");
					}
					break;
				case 8: //уполномоченные лица
					RegisterPersonCommands();
					LoadPersonList(Bank);
					break;
			}

			OnPropertyChanged("LimitMult");
			OnPropertyChanged("Limit4Money");
			OnPropertyChanged("Limit4Requests");
			OnPropertyChanged("OwnCapitalDate");
			OnPropertyChanged("SelectedSPRating");
			OnPropertyChanged("SelectedFitchRating");
			OnPropertyChanged("SelectedMoodyRating");

			_visitedTabs[value] = 1;

			if (!isChanged) IsDataChanged = false;
		}

	    private void FillDepositList()
	    {
            if (ID > 0 && !DepositList.Any())
            {
                DepositList = BLServiceSystem.Client.GetBankDepositInfoList(Bank.ID);
            }
        }

	    /// <summary>
		/// Загружаем все данные, для успешного прохождения тестов
		/// </summary>
		public void TestFullLoad()
		{
			var count = 0;
			_visitedTabs.ForEach(a =>
			{
				OnTabIndexChanged(count++);
			});
		}
		#endregion

		public override Type DataObjectTypeForJournal => typeof(LegalEntity);

	    public BankViewModel(long id)
			: base(typeof(BanksListViewModel))
		{
			_visitedTabs[0] = _visitedTabs[1] = _visitedTabs[2] = 1;

			RegisterOwnCapitalCommands();
			PfrAgreementStatuses = new List<string>("Расторгнуто/Не заключено|Заключено|На этапе расторжения".Split('|'));

			OriginalHeadList = new List<LegalEntityHead>();
			OriginalRatingList = new List<LegalEntityRating>();
			OriginalStockCodeList = new List<BankStockCode>();
			StockList = new List<Stock>();

			if (id > 0)
			{
				ID = id;
				Bank = DataContainerFacade.GetByID<LegalEntity, long>(id);
				Contragent = Bank.GetContragent();
				License = Bank.GetLicensiesList().Cast<License>().FirstOrDefault() ?? new License { LegalEntityID = Bank.ID };
				FillInOwnCapital();
				
			}
			else
			{
				Bank = new LegalEntity();
                Contragent = new Contragent { TypeName = "Банк", StatusID = StatusIdentifier.Identifier.Active.ToLong() };
				License = new License();
				//null = "Нет рейтинга"
				SelectedFitchRating = null;
				SelectedSPRating = null;
				SelectedMoodyRating = null;
				OwnCapital = 0;

				var now = DateTime.Now;
				OwnCapitalDate = now;
				Bank.StandardDate = now;
				Bank.FitchDate = now;
				Bank.MoodyDate = now;
			}

			Bank.PropertyChanged +=Bank_PropertyChanged;
			UpdateHeader();

			//DEBUG
			/*var count = 0;
			_visitedTabs.ForEach(a =>
			{
				OnTabIndexChanged(count++);
			});*/


			Register();

			OnPropertyChanged("LimitMult");
			OnPropertyChanged("Limit4Money");
			OnPropertyChanged("Limit4Requests");
			OnPropertyChanged("OwnCapitalDate");
			OnPropertyChanged("SelectedSPRating");
			OnPropertyChanged("SelectedFitchRating");
			OnPropertyChanged("SelectedMoodyRating");

			IsDataChanged = false;
		}

	    private void Bank_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "FormalizedName")
				UpdateHeader();
		}

		private void UpdateHeader()
		{
			Header = "Банк " + Bank.FormalizedName;
		}


		private void ReloadVisitedTabs()
		{
			for (int i = 0; i < _visitedTabs.Length; i++)
			{
				if (_visitedTabs[i] == 1)
				{
					_visitedTabs[i] = 0;
					OnTabIndexChanged(i);
				}
			}
		}

		private void FillInOwnCapital()
		{
			var isAlreadyChanged = IsDataChanged;
		    if (Bank.ID != 0)
		    {
		        var result = BLServiceSystem.Client.FillInOwnCapital(Bank.ID);
		        OwnCapital = (decimal?)result[0];
		        OwnCapitalDate = (DateTime?)result[1];
		    }
            if (!isAlreadyChanged) IsDataChanged = false;
		}


		public override string this[string columnName]
		{
			get
			{
				//Debug.WriteLine("Validate " + columnName);
				var required = "Поле обязательное для заполнения";
				switch (columnName)
				{
					case "RegistrationNum":
						return !RegistrationNum.HasValue ? required : LicenseValidator.Error;

					case "RegistrationDate":
						return RegistrationDate.HasValue ? string.Empty : required;
					//могут, еще как могут
					/*
					case "OwnCapital":
						if (this.OwnCapital < 0.0M)
							return "Собственные средства не могут быть отрицательными";
						break;
					*/
				}
				return string.Empty;
			}
		}

		public class MultiplierRatingHelper : MultiplierRating
		{
			public string Name { get; set; }
			public string RatingName { get; set; }
			public string ShortDate { get; set; }
		}

	}
}