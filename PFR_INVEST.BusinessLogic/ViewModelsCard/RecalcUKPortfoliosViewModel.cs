﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class RecalcUKPortfoliosViewModel : ViewModelCard
    {
        protected List<Contract> m_contracts;
        public int ContractsCount;
        public int CurrentContractIndex;
        public int ErrorsCount;
        public bool Canceled;

        protected override void ExecuteSave() { }
        public override bool CanExecuteSave()
        {
            return false;
        }

        public RecalcUKPortfoliosViewModel()
        {
            DataObjectTypeForJournal = typeof(object);
            ID = 0;
        }


        public virtual void LoadContracts()
        {
            m_contracts = DataContainerFacade.GetList<Contract>();

            ContractsCount = m_contracts.Count;
            CurrentContractIndex = 0;
            ErrorsCount = 0;
            Canceled = false;
        }

        public void RecalcNextContract()
        {
            if (IsRecalcuationFinished())
                return;

            Contract contract = m_contracts[CurrentContractIndex];
            try
            {
                //loop on contracts
                BLServiceSystem.Client.RecalcUKPortfoliosForContract(contract.ID);
            }
            catch (Exception ex)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(ex);
                ErrorsCount++;
            }

            CurrentContractIndex++;
        }

        public bool IsRecalcuationFinished()
        {
            return ContractsCount <= CurrentContractIndex;
        }

        public override string this[string columnName] => null;
    }
}