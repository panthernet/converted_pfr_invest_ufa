﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Attributes;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    [ModelEntity(Type = typeof(OpfrTransfer))]
    public class OpfrTransferViewModel : ViewModelCard, IUpdateRaisingModel
    {
        //сумма, тотал сумма - расчет, косгу - комбо
        private readonly OpfrTransfer _transfer;
        private readonly OpfrRegister _register;
        private readonly Element _status;
        private readonly Element _regType;
        private readonly Element _kind;
        private bool _isNotEditable;
        private readonly decimal _totalSumExcludeSelf;

        public List<PFRBranch> OpfrList { get; set; }
        public List<OpfrCost> KosguList { get; set; }

        public List<AsgFinTr> PPList { get; set; } = new List<AsgFinTr>();

        public bool IsPPGridVisible => PPList.Any() && AppSettingsHelper.IsOpfrTransfersEnabled;

        public long KosguID
        {
            get { return _transfer.OpfrCostID; }
            set { _transfer.OpfrCostID = value; OnPropertyChanged("KosguID"); }
        }

        public decimal TotalSum => _totalSumExcludeSelf + Summ;


        public bool IsSumNotChangable => _transfer.Status != (long) Element.SpecialDictionaryItems.OpfrTransferStatusInitial;

        public bool IsNotEditable
        {
            get { return _isNotEditable; }
            set { _isNotEditable = value; OnPropertyChangedNoData("IsNotEditable"); }
        }

        public string PortfolioName { get; }


        public string Status => _status.Name;

        public string KindName => _kind.Name;

        public decimal Summ
        {
            get { return _transfer.Sum; }
            set { _transfer.Sum = value; OnPropertyChanged("Summ"); OnPropertyChangedNoData("TotalSum"); }
        }

        public DateTime DIDate
        {
            get { return _transfer.DIDate; }
            set { _transfer.DIDate = value; OnPropertyChanged("DIDate"); }
        }

        public long OpfrID
        {
            get { return _transfer.PfrBranchID; }
            set { _transfer.PfrBranchID = value; OnPropertyChanged("OpfrID"); GetTotalSum(); }
        }


        public string RegType => _regType.Name;

        public string Comment
        {
            get { return _transfer.Comment; }
            set { _transfer.Comment = value; OnPropertyChanged("Comment"); }
        }

        public OpfrTransferViewModel(long registerId, decimal totalSumExcludeSelf, long? id = null)
        {
            _totalSumExcludeSelf = totalSumExcludeSelf;
            OpfrList = DataContainerFacade.GetList<PFRBranch>(id == null).OrderBy(a => a.Name).ToList();

            _transfer = id == null ? new OpfrTransfer() : DataContainerFacade.GetByID<OpfrTransfer>(id);
            _register = DataContainerFacade.GetByID<OpfrRegister>(registerId);
            KosguList = DataContainerFacade.GetList<OpfrCost>(id == null).OrderBy(a=> a.Name).ToList();

            ID = _transfer.ID;
            if (id == null)
            {
                _transfer.OpfrRegisterID = _register.ID;
                IsDataChanged = true;
                KosguID = KosguList.Count > 0 ? KosguList.First().ID : 0;
                OpfrID = OpfrList.Count > 0 ? OpfrList.First().ID : 0;
                _status =
                    DataContainerFacade.GetByID<Element>((long) Element.SpecialDictionaryItems.OpfrTransferStatusInitial);
            }
            else
            {
                IsNotEditable = true;
                if (_transfer.PortfolioID.HasValue)
                {
                    var pf = DataContainerFacade.GetByID<Portfolio>(_transfer.PortfolioID.Value);
                    if(pf != null)
                        PortfolioName = pf.Name;
                }
                _status = DataContainerFacade.GetByID<Element>(_transfer.Status);

                var ppIdList = DataContainerFacade.GetListByProperty<LinkPPOPFRTransfer>("TransferID", id, true).Select(a => a.PPID);
                PPList = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(ListPropertyCondition.In("ID", ppIdList.Cast<object>().ToArray())).Where(a=> a.StatusID != -1).ToList();                
            }
            _regType = DataContainerFacade.GetByID<Element>(_register.RegTypeID);
            _kind = DataContainerFacade.GetByID<Element>(_register.KindID);
            GetTotalSum();
        }


        private void GetTotalSum()
        {
           /* _totalSumExcludeSelf = DataContainerFacade.GetListByPropertyConditions<OpfrTransfer>(
                ListPropertyCondition.Equal("StatusID", 1L),
                ListPropertyCondition.Equal("OpfrRegisterID", _register.ID),
                ListPropertyCondition.Equal("PfrBranchID", _transfer.PfrBranchID),
                ListPropertyCondition.NotEqual("ID", ID)).Sum(a => a.Sum);

            OnPropertyChangedNoData("TotalSum");*/
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Comment":
                        return _transfer.Comment.ValidateMaxLength(2500);
                    case "Summ":
                        return Summ.ValidateMaxFormat();
                }

                return null;
            }
        }

        private string Validate()
        {
            const string values = "Comment|Summ";
            if (_transfer == null) return "Ошибка";
            return values.Split('|').Any(fld => !string.IsNullOrEmpty(this[fld])) ? "Неверный формат данных" : null;
        }

        /// <summary>
        /// Проверка перед сохранением. Место для всяких подтверждений и т.д.
        /// True - для продолжения процеса сохранения 
        /// </summary>
        protected override bool BeforeExecuteSaveCheck()
        {
            if (State != ViewModelState.Create) return true;

            if (DataContainerFacade.GetListByPropertyConditionsCount<OpfrTransfer>(
                ListPropertyCondition.Equal("StatusID", 1L),
                ListPropertyCondition.Equal("OpfrRegisterID", _register.ID),
                ListPropertyCondition.Equal("PfrBranchID", _transfer.PfrBranchID),
                ListPropertyCondition.Equal("OpfrCostID", _transfer.OpfrCostID)) > 0)
            {
                DialogHelper.ShowError("У реестра уже созданы перечисления с указанными параметрами! Сохранение перечисления невозможно.");
                return false;
            }
            return true;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Create:
                case ViewModelState.Edit:
                    ID = _transfer.ID = DataContainerFacade.Save(_transfer);
                    IsNotEditable = true;
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && (State == ViewModelState.Create || IsDataChanged);
        }

        public override bool CanExecuteDelete()
        {
            return CanDeleteTransfer(_transfer);
        }

        public static bool CanDeleteTransfer(OpfrTransfer transfer)
        {
            return transfer.Status == (long)Element.SpecialDictionaryItems.OpfrTransferStatusInitial;
        }

        protected override void ExecuteDelete(int delType = 1)
        {
            DataContainerFacade.Delete(_transfer);
            base.ExecuteDelete(DocOperation.Archive);
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(OpfrTransfer), _transfer.ID) };
        }
    }
}
