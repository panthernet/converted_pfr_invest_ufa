﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class DeadZLEditViewModel : ViewModelCard
    {
        private string _npfDisplayName = string.Empty;
        [DisplayName("Наименование НПФ")]
        public string NPFDisplayName
        {
            get { return _npfDisplayName; }
            set
            {
                _npfDisplayName = value;
                OnPropertyChanged("NPFDisplayName");
            }
        }

        private DateTime? _date;
        public DateTime? Date
        {
            get { return _date; }

            set
            {
                if (_date == value) return;
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        private ERZLNotify _currErzlNotify;
        [DisplayName("Количество")]
        [Required]
        public long? NotifyCount
        {
            get
            {
                return _currErzlNotify?.Count;
            }

            set
            {
                if (_currErzlNotify == null) return;
                _currErzlNotify.Count = value;
                OnPropertyChanged("NotifyCount");
            }
        }

        [DisplayName("Дата уведомления")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [Required]
        public DateTime? NotifyDate
        {
            get
            {
                return _currErzlNotify?.Date;
            }

            set
            {
                if (_currErzlNotify == null) return;
                _currErzlNotify.Date = value;
                OnPropertyChanged("NotifyDate");
            }
        }


        #region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(_currErzlNotify);
            base.ExecuteDelete(DocOperation.Delete);
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    if (State == ViewModelState.Edit)
                    {
                        if (_currErzlNotify != null)
                            DataContainerFacade.Save<ERZLNotify, long>(_currErzlNotify);
                    }
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) && IsDataChanged;
        }
        #endregion

        public void Load(long id)
        {
            try
            {
                ID = id;
                _currErzlNotify = DataContainerFacade.GetByID<ERZLNotify, long>(id);

                if (_currErzlNotify == null) return;
                try
                {
                    _npfDisplayName = _currErzlNotify.GetCreditAccount().GetContragent().GetLegalEntity().FormalizedNameFull;
                }
                catch
                {
                    _npfDisplayName = "";
                }
            }
            catch { }
        }

        public DeadZLEditViewModel()
#if !WEBCLIENT
            : base(typeof(DeadZLListViewModel))
#endif
        {
            DataObjectTypeForJournal = typeof(ERZLNotify);
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToLower())
                {
                    case "notifydate": return NotifyDate == null || NotifyDate == DateTime.MinValue ? errorMessage : null;
                    case "notifycount": return NotifyCount == null || NotifyCount == 0 ? errorMessage : null;
                    default: return null;
                }
            }
        }

        private string Validate()
        {
            const string fieldNames = "notifydate|notifycount";
            return fieldNames.Split("|".ToCharArray()).Any(fieldName => !string.IsNullOrEmpty(this[fieldName])) ? "Неверный формат данных" : null;
        }
    }
}
