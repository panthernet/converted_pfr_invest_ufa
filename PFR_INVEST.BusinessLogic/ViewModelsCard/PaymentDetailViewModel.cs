﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
	public class PaymentDetailViewModel : ViewModelCard, IUpdateRaisingModel
	{

		public List<KBK> KBKList { get; private set; }

		public PaymentDetail PaymentDetail { get; private set; }

		public long? KBKID
		{
			get { return PaymentDetail.KBKID; }
			set { PaymentDetail.KBKID = value; OnPropertyChanged("KBKID"); }
		}

		public string Details
		{
			get { return PaymentDetail.PaymentDetails; }
			set { PaymentDetail.PaymentDetails = value; OnPropertyChanged("Details"); }
		}

		public string Comment
		{
			get { return PaymentDetail.Comment; }
			set { PaymentDetail.Comment = value; OnPropertyChanged("Comment"); }
		}


		public override bool CanExecuteDelete()
		{
			return State != ViewModelState.Create;
		}

		protected override void ExecuteDelete(int delType)
		{
			DataContainerFacade.Delete(PaymentDetail);
			base.ExecuteDelete(DocOperation.Delete);
		}

		protected override void ExecuteSave()
		{
			if (!CanExecuteSave()) return;
			{
				switch (State)
				{
					case ViewModelState.Read:
						break;
					case ViewModelState.Edit:
					case ViewModelState.Create:
						ID = PaymentDetail.ID = DataContainerFacade.Save(PaymentDetail);
						break;

					default:
						break;
				}
			}
		}

		public override bool CanExecuteSave()
		{
			return IsValid() && IsDataChanged;
		}


		public PaymentDetailViewModel(long id, ViewModelState action)
		{
            DataObjectTypeForJournal = typeof(PaymentDetail);
            State = action;
			KBKList = DataContainerFacade.GetList<KBK>();

			if (action == ViewModelState.Create)
			{
				PaymentDetail = new PaymentDetail();
				ID = -1;
			}
			else
			{
				PaymentDetail = DataContainerFacade.GetByID<PaymentDetail>(id);
				//Показываем "удалённый" КБК
				if (KBKList.All(k => k.ID != PaymentDetail.KBKID)) 
				{
					var kbk = DataContainerFacade.GetByID<KBK>(PaymentDetail.KBKID);
					if (kbk != null)
						KBKList.Insert(0, kbk);
				}
				ID = id;
			}

		}


		private bool IsValid()
		{
			const string data = "KBKID|Details|Comment";
			foreach (string key in data.Split('|'))
			{				
				if (!string.IsNullOrEmpty(this[key]))
					return false;
			}
			return true;
		}


		public override string this[string key]
		{
			get
			{
				try
				{
					switch (key)
					{
						case "KBKID": return KBKID.ValidateRequired();
                        case "Details": return string.IsNullOrWhiteSpace(Details) ? "Поле обязательное для заполнения" : Details.ValidateMaxLength(150);
                        case "Comment": return Comment != null ? Comment.ValidateMaxLength(150) : null;

						default: return null;
					}
				}
				catch { return DataTools.DATAERROR_NEED_DATA; }
			}
		}


		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
			return new List<KeyValuePair<Type, long>>
			{
				new KeyValuePair<Type, long>(typeof(PaymentDetail), PaymentDetail.ID)
			};
		}
	}
}
