﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator,DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_directory_editor)]
    public class NPFCodeViewModel : ViewModelCard, IValidatableViewModel //, IUpdateRaisingModel
    {
        #region Fields

        public NPFErrorCode NPFCode { get; private set; }

        [Display(Name = "Код")]
        [Required]
        [StringLength(10, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Code
        {
            get { return NPFCode.Code; }
            set
            {
                if (NPFCode.Code == value) return;
                NPFCode.Code = value;
                OnPropertyChanged("Code");
            }
        }

        [Display(Name = "Обоснование")]
        [Required]
        [StringLength(256, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Description
        {
            get { return NPFCode.Description; }
            set
            {
                if (NPFCode.Description == value) return;
                NPFCode.Description = value;
                OnPropertyChanged("Description");
            }
        }

        [Display(Name = "Комментарий")]
        [StringLength(1024, ErrorMessage = WebModelHelper.VALIDATION_DEFAULT_MSG_STRING)]
        public string Comment
        {
            get { return NPFCode.Comment; }
            set
            {
                if (NPFCode.Comment == value) return;
                NPFCode.Comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public string CodeLabel => NPFCode.IsErrorCode == 1 ? "Код ошибки*:" : "Код стыковки с договором*:";

        #endregion

        #region Execute Implementations

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create;
        }

        public override bool BeforeExecuteDeleteCheck()
        {
            var isError = NPFCode.IsErrorCode == 1;
            if (DataContainerFacade.GetListByPropertyConditionsCount<RejectApplication>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal(isError ? "ErrorCode" : "ContractDockCode", NPFCode.Code)
            }) <= 0) return true;
            DialogHelper.ShowError(string.Format("Выбранный {0} удалить нельзя, т.к. он используется в системе!", isError ? "код ошибки" : " код стыковки с договором"));
            return false;
        }

        protected override void ExecuteDelete(int delType)
        {
            DataContainerFacade.Delete(NPFCode);
            base.ExecuteDelete(DocOperation.Archive);
        }

        protected override bool BeforeExecuteSaveCheck()
        {
            var existList = DataContainerFacade.GetListByPropertyConditions<NPFErrorCode>(
                new List<ListPropertyCondition>
                {
                    ListPropertyCondition.Equal("Code", NPFCode.Code),
                    ListPropertyCondition.Equal("IsErrorCode", NPFCode.IsErrorCode)
                }).Where(x => x.ID != NPFCode.ID).ToList();
            if (!existList.Any()) return true;
            var error = existList.First().StatusID == -1
                ? "Указанный код ранее уже был добавлен в систему, но сейчас находится в статусе \"Удален\"."
                : "Указанный код уже используется в системе.";
            DialogHelper.ShowError(error);
            return false;
        }

        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;
            {
                switch (State)
                {
                    case ViewModelState.Read:
                        break;
                    case ViewModelState.Edit:
                        NPFCode.Code = Code.Trim();
                        DataContainerFacade.Save(NPFCode);
                        break;
                    case ViewModelState.Create:
                        NPFCode.Code = Code.Trim();
                        ID = NPFCode.ID = DataContainerFacade.Save(NPFCode);
                        break;
                    default:
                        break;
                }
            }
        }


        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate()) &&
                   IsDataChanged;
        }

#endregion



        public NPFCodeViewModel(long id, ViewModelState action, bool isErrorCode)
            : base(typeof (NPFCodeListViewModel))
        {
            DataObjectTypeForJournal = typeof(NPFErrorCode);
            State = action;

            if (action == ViewModelState.Create)
            {
                NPFCode = new NPFErrorCode {IsErrorCode = isErrorCode ? 1 : 0};
                ID = 0;
            }
            else
            {
                NPFCode = DataContainerFacade.GetByID<NPFErrorCode>(id);
                ID = id;
            }
        }


        public string Validate()
        {
            const string fields = "code|description|comment";
            foreach (var e in fields.ToLower().Split('|').Select(key => this[key]).Where(e => !string.IsNullOrEmpty(e)))
            {
                return e;
            }
            return string.Empty;
        }

#region Validation

        public override string this[string key]
        {
            get
            {
                try
                {
                    switch (key.ToLower())
                    {
						case "code": return NPFCode.Code.TrimmedIsNullOrEmpty() ? DataTools.DATAERROR_NEED_DATA : NPFCode.Code.ValidateMaxLength(10);
                        case "description":
							return NPFCode.Description.TrimmedIsNullOrEmpty()
									   ? DataTools.DATAERROR_NEED_DATA
									   : NPFCode.Description.ValidateMaxLength(256);
                        case "comment":
							return NPFCode.Comment != null ? NPFCode.Comment.ValidateMaxLength(1024)
									   : null;


                        default:
                            return null;
                    }
                }
                catch
                {
                    return DataTools.DATAERROR_NEED_DATA;
                }
            }
        }

#endregion

        public bool IsEdit => State == ViewModelState.Edit;
    }
}
