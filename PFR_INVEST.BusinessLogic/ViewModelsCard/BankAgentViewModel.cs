﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class BankAgentViewModel : ViewModelCard
    {
        private readonly string[] rates = { "AAA", "AA+", "AA", "AA-", "A+", "A", "A-", "BBB+", "BBB", "BBB-", "BB+", "BB", "BB-", "B+", "B", "B-", "CCC", "CC", "C", "RD", "D", "Нет рейтинга", "Отозван" };
        private readonly string[] rates2 = { "Aaa", "Aa1", "Aa2", "Aa3", "A1", "A2", "A3", "Baa1", "Baa2", "Baa3", "Ba1", "Ba2", "Ba3", "B1", "B2", "B3", "Caa1", "Caa2", "Caa3", "Ca", "C", "Нет рейтинга", "Отозван" };

        private LegalEntity _Bank;
        public LegalEntity Bank
        {
            get { return _Bank; }
            set
            {
                _Bank = value;
                OnPropertyChanged("Bank");
            }
        }

        private License _License;
        public License License
        {
            get { return _License; }
            set
            {
                _License = value;
                OnPropertyChanged("License");
            }
        }

        private Contragent m_Contragent;
        public Contragent Contragent
        {
            get { return m_Contragent; }
            set
            {
                if (m_Contragent != value)
                {
                    m_Contragent = value;
                    OnPropertyChanged("Contragent");
                }
            }
        }

        #region Ratings

        public List<String> FitchRatings => rates.ToList();

        public String SelectedFitchRating
        {
            get { return Bank.Fitch; }
            set
            {
                Bank.Fitch = value;
                OnPropertyChanged("SelectedFitchRating");
                OnPropertyChanged("Limit4Money");
                OnPropertyChanged("Limit4Requests");
            }
        }

        public List<String> SPRatings => rates.ToList();

        public String SelectedSPRating
        {
            get { return Bank.Standard; }
            set
            {
                Bank.Standard = value;
                OnPropertyChanged("SelectedSPRating");
                OnPropertyChanged("Limit4Money");
                OnPropertyChanged("Limit4Requests");
            }
        }

        public List<String> MoodyRatings => rates2.ToList();

        public String SelectedMoodyRating
        {
            get { return Bank.Moody; }
            set
            {
                Bank.Moody = value;
                OnPropertyChanged("SelectedMoodyRating");
                OnPropertyChanged("Limit4Money");
                OnPropertyChanged("Limit4Requests");
            }
        }
        #endregion

        #region OwnCapital


        public decimal? OwnCapital
        {
            get { return Bank.Money; }
            set
            {
                Bank.Money = value;
                OnPropertyChanged("OwnCapital");
                OnPropertyChanged("Limit4Money");
                OnPropertyChanged("Limit4Requests");
            }
        }

        public DateTime? OwnCapitalDate
        {
            get { return Bank.MoneyDate; }
            set
            {
                Bank.MoneyDate = value;
                OnPropertyChanged("OwnCapitalDate");
            }
        }

        #endregion


        #region Limit4Money

        public double Limit4Money
        {
            get
            {
                decimal m = (OwnCapital ?? 0m) / 1000000.0m;
                double r = calculateR();
                double res = r * (double)m;
                return Math.Round(res, 0);
            }
            set
            {
                OnPropertyChanged("Limit4Money");
            }
        }

        #endregion

        #region Limit4Requests


        public double Limit4Requests
        {
            get
            {
                return Math.Round(Limit4Money - calculateD() + calculateV());
            }
            set
            {
                OnPropertyChanged("Limit4Requests");
            }
        }

        #endregion

        #region PfrAgreementStatuses

        private List<String> pfrAgreementStatuses;
        public List<String> PfrAgreementStatuses
        {
            get { return pfrAgreementStatuses; }
            set { pfrAgreementStatuses = value; OnPropertyChanged("PfrAgreementStatuses"); }
        }


        public String SelectedPfrAgreementStatus
        {
            get { return Bank.PFRAGRSTAT; }
            set
            {
                Bank.PFRAGRSTAT = value;
                OnPropertyChanged("SelectedPfrAgreementStatus");
            }
        }
        #endregion

        public override bool CanExecuteDelete()
        {
            return State != ViewModelState.Create && Bank.GetContragent().StatusID != -1;
        }

        protected override void ExecuteDelete(int delType)
        {
            var ca = Bank.GetContragent(); ca.StatusID = -1;
            DataContainerFacade.Save<Contragent, long>(ca);
            base.ExecuteDelete(DocOperation.Archive);
        }


        protected override void ExecuteSave()
        {
            if (!CanExecuteSave()) return;

            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                case ViewModelState.Create:
                    Contragent.Name = Bank.FormalizedName;
                    Contragent.ID = DataContainerFacade.Save<Contragent, long>(Contragent);
                    Bank.ContragentID = Contragent.ID;
                    Bank.ID = DataContainerFacade.Save<LegalEntity, long>(Bank);
                    License.LegalEntityID = Bank.ID;
                    License.ID = DataContainerFacade.Save<License, long>(License);

                    ID = Bank.ID;
                    break;
                default:
                    break;
            }
        }

        public override bool CanExecuteSave()
        {
            return String.IsNullOrEmpty(Validate()) && IsDataChanged;
        }

        private static bool IsRating(IList<string> values, string rating, string compareRating)
        {
            if (string.IsNullOrWhiteSpace(rating))
                rating = values.LastOrDefault() ?? string.Empty;
            rating = rating.TrimEnd();
            return values.IndexOf(rating) <= values.IndexOf(compareRating);
        }

        private double calculateR()
        {
            if ((IsRating(FitchRatings, SelectedFitchRating, "BBB+")
                    || IsRating(SPRatings, SelectedSPRating, "BBB+")
                    || IsRating(MoodyRatings, SelectedMoodyRating, "Baa1"))
                && (OwnCapital >= 90000000000))
                return 1;
            if (IsRating(FitchRatings, SelectedFitchRating, "BBB")
                || IsRating(SPRatings, SelectedSPRating, "BBB")
                || IsRating(MoodyRatings, SelectedMoodyRating, "Baa2"))
                return 0.75;
            if (IsRating(FitchRatings, SelectedFitchRating, "BB+")
                || IsRating(SPRatings, SelectedSPRating, "BB+")
                || IsRating(MoodyRatings, SelectedMoodyRating, "Ba1"))
                return 0.5;
            if (IsRating(FitchRatings, SelectedFitchRating, "BB-")
                || IsRating(SPRatings, SelectedSPRating, "BB-")
                || IsRating(MoodyRatings, SelectedMoodyRating, "Ba3"))
                return 0.25;
            return 0;
        }

        private static double calculateD()
        {
            //d=(d1+d2+d3)/1000000 (в млн руб)"
            //d1 - сумма СПН, размещенных в банке на начало предшествующего рабочего дня
            //d2 - сумма СПН, подлежащиз размещению в предшествующий рабочий день
            //d3 - сумма СПН, подлежащих размещению в депозиты в день проведения Отбора заявок

            return 0;


        }

        private static double calculateV()
        {
            //v=(v1+v2+v3) /1000000
            //v1 - сумма СПН, подлежащих вохврату из банка на начало предшествующего Отбору заявок рабочего дня
            //v2 - сумма СПН, подлежащих вохврату из банка в день Отбора заявок 
            //v3 - сумма СПН, подлежащих вохврату из банка в день перечисления средств по итогам аукциона

            return 0;
        }

        public void Load(long lid)
        {
            try
            {

            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }

        private void Register()
        {
            License.PropertyChanged += (sender, e) =>
                                            {
                                                IsDataChanged = true;
                                            };
            Bank.PropertyChanged += (sender, e) =>
                                         {
                                             IsDataChanged = true;
                                         };
            Bank.OnValidate += Bank_Validate;
            License.OnValidate += License_Validate;
        }

        #region Validate

        private void License_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            var license = (sender as License);
            const string error = "Поле обязательное для заполнения";
            if (license == null)
                e.Error = error;
            else
            {
                switch (e.PropertyName)
                {
                    case "Number": e.Error = string.IsNullOrWhiteSpace(license.Number) ? error : null;
                        break;
                    case "RegistrationDate": e.Error = string.IsNullOrWhiteSpace(license.RegistrationDate.ToString()) ? error : null;
                        break;
                }
            }
        }

        private void Bank_Validate(object sender, BaseDataObject.ValidateEventArgs e)
        {
            var bank = (sender as LegalEntity);
            if (bank != null)
            {
                const string error = "Поле обязательное для заполнения";
                switch (e.PropertyName)
                {
                    case "FullName": e.Error = string.IsNullOrWhiteSpace(bank.FullName) ? error : null;
                        break;
                    case "RegistrationNum": e.Error = string.IsNullOrWhiteSpace(bank.RegistrationNum) ? error : null;
                        break;
                    case "Registrator": e.Error = string.IsNullOrWhiteSpace(bank.Registrator) ? error : null;
                        break;
                    case "ShortName": e.Error = string.IsNullOrWhiteSpace(bank.ShortName) ? error : null;
                        break;
                    case "FormalizedName": e.Error = string.IsNullOrWhiteSpace(bank.FormalizedName) ? error : null;
                        break;


                    case "LegalAddress": e.Error = string.IsNullOrWhiteSpace(bank.LegalAddress) ? error : null;
                        break;
                    case "PostAddress": e.Error = string.IsNullOrWhiteSpace(bank.PostAddress) ? error : null;
                        break;
                    case "Phone": e.Error = string.IsNullOrWhiteSpace(bank.Phone) ? error : null;
                        break;

                    case "CorrAcc": e.Error = string.IsNullOrWhiteSpace(bank.CorrAcc) ? error : null;
                        break;
                    case "BIK": e.Error = string.IsNullOrWhiteSpace(bank.BIK) ? error : null;
                        break;
                    case "INN": e.Error = (string.IsNullOrWhiteSpace(bank.INN) || (Bank.INN.Length != 10)) ? "Длина поля должна равняться 10" : null;
                        break;
                    case "OKPP": e.Error = string.IsNullOrWhiteSpace(bank.OKPP) ? error : null;
                        break;
                }
                if (string.IsNullOrEmpty(e.Error))
                {
                    //проверка максимальной допустимой длины поля
                    switch (e.PropertyName)
                    {
                        case "FullName": e.Error = ValidateLength(bank.FullName, 512); break;
                        case "RegistrationNum": e.Error = ValidateLength(bank.RegistrationNum, 50); break;
                        case "Registrator": e.Error = ValidateLength(bank.Registrator, 512); break;
                        case "ShortName": e.Error = ValidateLength(bank.ShortName, 128); break;
                        case "FormalizedName": e.Error = ValidateLength(bank.FormalizedName, 512); break;
                        case "LegalAddress": e.Error = ValidateLength(bank.LegalAddress, 512); break;
                        case "PostAddress": e.Error = ValidateLength(bank.PostAddress, 512); break;
                        case "Phone": e.Error = ValidateLength(bank.Phone, 128); break;
                        case "EAddress": e.Error = string.IsNullOrEmpty(bank.EAddress) ? "Значение не введено" : null; break;

                        case "CorrAcc": e.Error = ValidateLengthExact(bank.CorrAcc, 20); break;
                        case "BIK": e.Error = ValidateLengthExact(bank.BIK, 9); break;
                        case "INN": e.Error = ValidateLengthExact(bank.INN, 10); break;
                        case "OKPP": e.Error = ValidateLengthExact(bank.OKPP, 9); break;

                    }
                }
            }
        }

        private static string ValidateLengthExact(string value, int length)
        {
            string error = string.Format("Длина поля должна равняться {0}", length);
            return (value ?? string.Empty).Trim().Length != length ? error : null;
        }

        private static string ValidateLength(string value, int length)
        {
            const string error = "слишком длинное значение поля";
            return (value ?? string.Empty).Length > length ? error : null;
        }

        private string Validate()
        {
            const string bankFields = "FullName|ShortName|FormalizedName|RegistrationNum|Registrator|LegalAddress|PostAddress|Phone|CorrAcc|BIK|INN|OKPP|EAddress";
            const string licenseFields = "Number|RegistrationDate";

            foreach (string key in bankFields.Split('|'))
            {
                var e = new BaseDataObject.ValidateEventArgs(key);
                Bank_Validate(Bank, e);
                if (!string.IsNullOrEmpty(e.Error))
                    return e.Error;
            }
            foreach (string key in licenseFields.Split('|'))
            {
                var e = new BaseDataObject.ValidateEventArgs(key);
                License_Validate(License, e);
                if (!string.IsNullOrEmpty(e.Error))
                    return e.Error;
            }
            return "";
        }

        #endregion Validate

        public BankAgentViewModel(long id)
            : base(typeof(BankAgentsListViewModel))
        {
            PfrAgreementStatuses = new List<string>("Расторгнуто/Не заключено|Заключено|На этапе расторжения".Split('|'));
            DataObjectTypeForJournal = typeof(LegalEntity);

            if (id > 0)
            {
                ID = id;
                Bank = DataContainerFacade.GetByID<LegalEntity, long>(id);
                Contragent = Bank.GetContragent();
                License = Bank.GetLicensiesList().Cast<License>().FirstOrDefault() ?? new License { LegalEntityID = Bank.ID };
                
            }
            else
            {
                Bank = new LegalEntity();
                Contragent = new Contragent { TypeName = "Банк-агент", StatusID = StatusIdentifier.Identifier.Active.ToLong() };
                License = new License();
                //null = "Нет рейтинга"
                SelectedFitchRating = null;
                SelectedSPRating = null;
                SelectedMoodyRating = null;
                //OwnCapital = 70000000000;

                var now = DateTime.Now;
                OwnCapitalDate = now;
                Bank.StandardDate = now;
                Bank.FitchDate = now;
                Bank.MoodyDate = now;
            }

            Register();

            OnPropertyChanged("Limit4Money");
            OnPropertyChanged("Limit4Requests");
            OnPropertyChanged("OwnCapitalDate");
            OnPropertyChanged("SelectedSPRating");
            OnPropertyChanged("SelectedFitchRating");
            OnPropertyChanged("SelectedMoodyRating");
            IsDataChanged = false;
        }



        public override string this[string columnName] => string.Empty;
    }

}