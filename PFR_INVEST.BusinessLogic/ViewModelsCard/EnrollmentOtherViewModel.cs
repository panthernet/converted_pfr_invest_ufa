﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class EnrollmentOtherViewModel : AccModelBase, ICloseViewModelAfterSave
    {
        private readonly List<string> _contentTypes = new List<string> { "Прочие доходы", "Корректировочные операции" };
        public List<string> ContentTypes => _contentTypes;

        private bool _AddToCHFR;
        public bool AddToCHFR
        {
            get { return _AddToCHFR = acc.AddToCHFR != 0; }
            set
            {
                if (SetPropertyValue(nameof(AddToCHFR), ref _AddToCHFR, value))
                    acc.AddToCHFR = value ? 1 : 0;
            }
        }

        public EnrollmentOtherViewModel()
        {
            ID = 0;
            acc.KindID = 2;// потому что зачисление прочих поступлений
            acc.Content = ContentTypes[0];
        }
    }
}
