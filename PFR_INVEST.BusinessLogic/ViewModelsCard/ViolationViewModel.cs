﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

//using PFR_INVEST.DataAccess.Client.ObjectsExtensions;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class ViolationViewModel : ViewModelCard
    {

        #region Property

        private EdoOdkF140 _Document;
        public EdoOdkF140 Document
        {
            get { return _Document; }
            set
            {
                _Document = value;
                OnPropertyChanged("Document");
            }
        }

        private Contract _Contract;
        public Contract Contract
        {
            get { return _Contract; }
            set
            {
                _Contract = value;
                OnPropertyChanged("Contract");
            }
        }

        private string _LinkReport;
        //public string LinkReport
        //{
        //    get { return this.Document.LinkReport; }
        //    set
        //    {
        //        this._LinkReport = value;
        //        this.Document.LinkReport = this._LinkReport;
        //        this.OnPropertyChanged("LinkReport");
        //    }
        //}
        public string LinkReport => _LinkReport;

        private long? _OriginalXmlIDLinkReport;
        public long? OriginalXmlIDLinkReport => _OriginalXmlIDLinkReport;

        private bool _IsOriginalXmlIDLinkReport;
        public bool IsOriginalXmlIDLinkReport => _IsOriginalXmlIDLinkReport;


        private DateTime? _StopDate;
        public DateTime? StopDate
        {
            get { return Document.StopDate; }
            set
            {
                _StopDate = value;
                Document.StopDate = _StopDate;
                OnPropertyChanged("StopDate");
                SetStatus();
            }
        }
        private DateTime? _RealStopDate;
        public DateTime? RealStopDate
        {
            get { return Document.RealStopDate; }
            set
            {
                _RealStopDate = value;
                Document.RealStopDate = _RealStopDate;
                OnPropertyChanged("RealStopDate");
                SetStatus();
            }
        }

        private string _StopNumber;
        public string StopNumber
        {
            get { return Document.StopNumber; }
            set
            {
                _StopNumber = value;
                Document.StopNumber = _StopNumber;
                OnPropertyChanged("StopNumber");
            }
        }

        private DateTime? _StopReport;
        public DateTime? StopReport
        {
            get { return Document.StopReport; }
            set
            {
                _StopReport = value;
                Document.StopReport = _StopReport;
                OnPropertyChanged("StopReport");
            }
        }

        private string _LinkStopReport;
        //public string LinkStopReport
        //{
        //    get { return this.Document.LinkStopReport; }
        //    set
        //    {
        //        this._LinkStopReport = value;
        //        this.Document.LinkStopReport = this._LinkStopReport;
        //        this.OnPropertyChanged("LinkStopReport");
        //    }
        //}
        public string LinkStopReport => _LinkStopReport;


        private long? _OriginalXmlIDLinkStopReport;
        public long? OriginalXmlIDLinkStopReport => _OriginalXmlIDLinkStopReport;

        private bool _IsOriginalXmlIDLinkStopReport;
        public bool IsOriginalXmlIDLinkStopReport => _IsOriginalXmlIDLinkStopReport;


        private string _NoStopNumber;
        public string NoStopNumber
        {
            get { return Document.NoStopNumber; }
            set
            {
                _NoStopNumber = value;
                Document.NoStopNumber = _NoStopNumber;
                OnPropertyChanged("NoStopNumber");
            }
        }

        private DateTime? _NoStopDate;
        public DateTime? NoStopDate
        {
            get { return Document.NoStopDate; }
            set
            {
                _NoStopDate = value;
                Document.NoStopDate = _NoStopDate;
                OnPropertyChanged("NoStopDate");
            }
        }

        private string _LinkNoStopReport;
        //public string LinkNoStopReport
        //{
        //    get { return this.Document.LinkNoStopReport; }
        //    set
        //    {
        //        this._LinkNoStopReport = value;
        //        this.Document.LinkNoStopReport = this._LinkNoStopReport;
        //        this.OnPropertyChanged("LinkNoStopReport");
        //    }
        //}
        public string LinkNoStopReport => _LinkNoStopReport;

        private long? _OriginalXmlIDLinkNoStopReport;
        public long? OriginalXmlIDLinkNoStopReport => _OriginalXmlIDLinkNoStopReport;

        private bool _IsOriginalXmlIDLinkNoStopReport;
        public bool IsOriginalXmlIDLinkNoStopReport => _IsOriginalXmlIDLinkNoStopReport;


        private string _RealStopValue;
        public string RealStopValue
        {
            get { return Document.RealStopValue; }
            set
            {
                _RealStopValue = value;
                Document.RealStopValue = _RealStopValue;
                OnPropertyChanged("RealStopValue");
            }
        }






        #endregion

        private readonly List<Element> _statusF140;

        public ViolationViewModel(long id)
            : base(typeof(ViolationsSIListViewModel), typeof(ViolationsVRListViewModel))
        {
            DataObjectTypeForJournal = typeof(EdoOdkF140);
            ID = id;
            Document = id > 0 ? DataContainerFacade.GetByID<EdoOdkF140, long>(id) : new EdoOdkF140();

            _statusF140 = BLServiceSystem.Client.GetElementByType(Element.Types.StatusF140);

            SetLink();
        }

        public override bool CanExecuteSave() { return IsDataChanged; }

        protected override void ExecuteSave()
        {
            switch (State)
            {
                case ViewModelState.Read:
                    break;
                case ViewModelState.Edit:
                    ID = DataContainerFacade.Save(Document);
                    break;
                case ViewModelState.Create:
                    break;
                default:
                    break;
            }



        }

        public void RefreshLists() { }

        public override string this[string columnName] => string.Empty;

        private string Validate() { return string.Empty; }

        private void SetStatus()
        {
            if (RealStopDate == null)
            {
                var status = _statusF140.FirstOrDefault(el => el.Order == 1).Name;
                if (status != null)
                {
                    Document.Status = status;
                }
            }
            else if (StopDate < RealStopDate)
            {
                var status = _statusF140.FirstOrDefault(el => el.Order == 2).Name;
                if (status != null)
                {
                    Document.Status = status;
                }
            }
            else if (StopDate >= RealStopDate)
            {
                var status = _statusF140.FirstOrDefault(el => el.Order == 3).Name;
                if (status != null)
                {
                    Document.Status = status;
                }
            }

        }

        private void SetLink()
        {
            long id;
            if (long.TryParse(Document.LinkReport, out id))
            {
                var log = DataContainerFacade.GetByID<EDOLog>(id);
                _LinkReport = log.Filename;
                _OriginalXmlIDLinkReport = log.DocBodyId;
                _IsOriginalXmlIDLinkReport = true;
            }
            else
            {
                _LinkReport = null;
                _OriginalXmlIDLinkReport = null;
                _IsOriginalXmlIDLinkReport = false;
            }

            if (long.TryParse(Document.LinkNoStopReport, out id))
            {
                var log = DataContainerFacade.GetByID<EDOLog>(id);
                _LinkNoStopReport = log.Filename;
                _OriginalXmlIDLinkNoStopReport = log.DocBodyId;
                _IsOriginalXmlIDLinkNoStopReport = true;
            }
            else
            {
                _LinkNoStopReport = null;
                _OriginalXmlIDLinkNoStopReport = null;
                _IsOriginalXmlIDLinkNoStopReport = false;
            }

            if (long.TryParse(Document.LinkStopReport, out id))
            {
                var log = DataContainerFacade.GetByID<EDOLog>(id);
                _LinkStopReport = log.Filename;
                _OriginalXmlIDLinkStopReport = log.DocBodyId;
                _IsOriginalXmlIDLinkStopReport = true;
            }
            else
            {
                _LinkStopReport = null;
                _OriginalXmlIDLinkStopReport = null;
                _IsOriginalXmlIDLinkStopReport = false;
            }
        }
    }
}
