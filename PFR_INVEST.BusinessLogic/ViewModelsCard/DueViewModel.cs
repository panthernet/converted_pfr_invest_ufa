﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Helper;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
	public class DueViewModel : ViewModelCard, IUpdateRaisingModel, IUpdateListenerModel
	{
		public static bool IsClosedByKDoc(long addSPNID)
		{
			return BLServiceSystem.Client.IsAddSPNClosedByKDoc(addSPNID);
		}

		//private const decimal MAX_DECIMAL_19_4 = 1000000000000000;
		public AddSPN AddSPN { get; private set; }
		public PortfolioIdentifier.PortfolioTypes Type { get; private set; }

		#region Данные о поступлении СПН
		public DateTime? OperationDate
		{
			get
			{
				return AddSPN.OperationDate;
			}
			set
			{
				AddSPN.OperationDate = value;
				OnPropertyChanged("OperationDate");
			}
		}

		private PortfolioFullListItem _mAccount;
		private bool _mPortfolioChanged;
		public PortfolioFullListItem Account
		{
			get
			{
				return _mAccount;
			}

			set
			{
				if (value != null)
				{
					_mAccount = value;
					//http://jira.vs.it.ru/browse/PFRPTK-1660?focusedCommentId=206765&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-206765
					//if (AddSPN.PortfolioID.GetValueOrDefault() != value.Portfolio.ID)
					//{
					//    m_PortfolioChanged = true;
					//    AddSPN.PortfolioID = value.Portfolio.ID;
					//    MonthsList = BLServiceSystem.Client.GetValidMothsListForAddPSNWithPortfolioIDAndMonth(AddSPN.PortfolioID, AddSPN.MonthID);
					//}
					AddSPN.PFRBankAccountID = value.PfrBankAccount.ID;
					AddSPN.LegalEntityID = value.BankLE.ID;
					AddSPN.PortfolioID = value.PortfolioID;
					OnPropertyChanged("AccountNumber");
					OnPropertyChanged("BankName");
					OnPropertyChanged("Portfolio");
					OnPropertyChanged("Account");
				}
			}
		}
		#endregion

		#region Первичное поступление
		private const string M_MAX_NEW_DATE_ERROR_FORMAT = "Значение должно быть не больше {0}";
		public string MaxNewDateError { get; private set; }
		public DateTime? MaxNewDate { get; private set; }
		public DateTime? NewDate
		{
			get
			{
				return AddSPN.NewDate;
			}
			set
			{
				AddSPN.NewDate = value;
				OnPropertyChanged("NewDate");
			}
		}

		public string RegNum
		{
			get
			{
				return AddSPN.RegNum;
			}
			set
			{
				AddSPN.RegNum = value;
				OnPropertyChanged("RegNum");
			}
		}


		public long? SelectedYear
		{
			get { return AddSPN.YearID; }
			set { AddSPN.YearID = value; OnPropertyChanged("SelectedYear"); }
		}

		public List<Year> YearsList { get; private set; }


		private bool _mMonthChanged;
		public long? SelectedMonth
		{
			get { return AddSPN.MonthID; }
			set
			{
				if (AddSPN.MonthID != value)
				{
					AddSPN.MonthID = value;
					_mMonthChanged = true;
				}
				OnPropertyChanged("SelectedMonth");
			}
		}

		public List<Month> MonthsList { get; private set; }


	    public decimal Summ
	    {
	        get
	        {
	            AddSPN.Summ = SummNakop + SummEmployer + SummOther + SummPerc;
	            return AddSPN.Summ ?? 0;
	        }
	    }


	    public decimal SummNakop
        {
            get { return AddSPN.SummNakop ?? 0; }
            set
            {
                AddSPN.SummNakop = value;
                OnPropertyChanged("SummNakop");
                OnPropertyChanged("Summ");
            }
        }

        public decimal SummOther
        {
            get { return AddSPN.SummOther ?? 0; }
            set
            {
                AddSPN.SummOther = value;
                OnPropertyChanged("SummOther");
                OnPropertyChanged("Summ");
            }
        }

        public decimal SummPerc
        {
            get { return AddSPN.SummPerc ?? 0; }
            set
            {
                AddSPN.SummPerc = value;
                OnPropertyChanged("SummPerc");
                OnPropertyChanged("Summ");
            }
        }

        public decimal SummEmployer
        {
            get { return AddSPN.SummEmployer ?? 0; }
            set
            {
                AddSPN.SummEmployer = value;
                OnPropertyChanged("SummEmployer");
                OnPropertyChanged("Summ");
            }
        }

		//private void RecalcSumm()
		//{
		//    Summ = PaymentDSV + PaymentBoss;
		//}
		#endregion

		private bool _mIsReadOnly;

        private bool isReadOnly
        {
            get
            {
                return _mIsReadOnly;
            }
            set
            {
                _mIsReadOnly = value;
				var dc = IsDataChanged;
                OnPropertyChanged("IsReadOnly");
                OnPropertyChanged("ReadAccessOnly");
                OnPropertyChanged("IsSummReadOnly");
				IsDataChanged = dc;
            }
        }

		public override bool IsReadOnly => isReadOnly;

	    public string Comment
		{
			get
			{
				return AddSPN.Comment;
			}
			set
			{
				AddSPN.Comment = value;
				OnPropertyChanged("Comment");
			}
		}

		public bool IsSummReadOnly => ReadAccessOnly;

	    public Visibility IsDSVDueVisibility => Type == PortfolioIdentifier.PortfolioTypes.DSV || Type == PortfolioIdentifier.PortfolioTypes.ALL ? Visibility.Visible : Visibility.Collapsed;

	    public bool ReadAccessOnly => IsReadOnly
	                                  || State != ViewModelState.Create && (State == ViewModelState.Read ||
	                                                                        !(IsUserHasAccessRole));

	    public bool IsUserHasAccessRole => AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.OFPR_manager) ||
		                                   AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator);

	    private bool? _isClosedByKDoc;
		public bool IsItemClosedByKDoc
		{
			get
			{
				if (_isClosedByKDoc.HasValue)
				{
					return _isClosedByKDoc.Value;
				}
			    if (AddSPN != null)
			    {
			        _isClosedByKDoc = IsClosedByKDoc(AddSPN.ID);
			        return _isClosedByKDoc.Value;
			    }
			    return false;
			}
		}

		public DopSPNListItem SelectedDop { get; set; }
		private List<DopSPNListItem> _mDopSPNList;
		public List<DopSPNListItem> DopSPNList
		{
			get
			{
				return _mDopSPNList;
			}
			set
			{
				_mDopSPNList = value;
				OnPropertyChanged("DopSPNList");
			}
		}

		#region Execute implementation
		public override bool CanExecuteDelete()
		{
			return !IsReadOnly && State == ViewModelState.Edit;
		}

		protected override void ExecuteDelete(int delType = 1)
		{
            BLServiceSystem.Client.DeleteDue(AddSPN.ID);
			/*foreach (var dop in _mDopSPNList)
			{
				DataContainerFacade.Delete<DopSPN>(dop.ID);
			}
			DataContainerFacade.Delete(AddSPN);*/
			base.ExecuteDelete(DocOperation.Delete);
		}

		private void ExecuteDeleteDop()
		{
		    if (!SelectedDop.IsKDoc)
		    {
                var dopSpn = DataContainerFacade.GetByID<DopSPN>(SelectedDop.ID);
                if (dopSpn != null && DueHelper.GetIsBlockedByQuarterDop(dopSpn))
                {
                    DialogHelper.ShowError("Невозможно удалить уточнение за месяц, т.к. создано уточнение за квартал!");
                    return;
                }
            }

		    BLServiceSystem.Client.DeleteDopSPN(SelectedDop.ID, SelectedDop.IsKDoc);

           /* if (SelectedDop.IsKDoc)
                DataContainerFacade.Delete<KDoc>(SelectedDop.ID);
            else
            {
                DataContainerFacade.Delete<DopSPN>(SelectedDop.ID);
            }*/

			//RefreshConnectedViewModels();
			ViewModelManager.UpdateDataInAllModels(GetUpdatedList());
			RefreshDopSPNList();

			SelectedDop = null;
		}

		private bool CanExecuteDeleteDop()
		{
			return SelectedDop != null && DeleteAccessAllowed && IsUserHasAccessRole && !IsItemClosedByKDoc && !(AddSPN.KDocID > 0) && State != ViewModelState.Read;
		}

		private void ExecuteSelectAccount()
		{
			PortfolioFullListItem selected = null;
			switch (Type) 
			{
				case PortfolioIdentifier.PortfolioTypes.SPN:
				case PortfolioIdentifier.PortfolioTypes.NOT_DSV:
					selected = DialogHelper.SelectPortfolio(true, true, Portfolio.Types.DSV);
					break;
				case PortfolioIdentifier.PortfolioTypes.DSV:
				case PortfolioIdentifier.PortfolioTypes.NOT_SPN:
					selected = DialogHelper.SelectPortfolio(true, true, Portfolio.Types.SPN);
					break;
				default:
					selected = DialogHelper.SelectPortfolio(true);
					break;

			}		   
			Account = selected;
		}

		public override bool CanExecuteSave()
		{
			return !IsReadOnly && Validate() && IsDataChanged;
		}

		protected override void ExecuteSave()
		{
			switch (State)
			{
				case ViewModelState.Read:
					break;
				case ViewModelState.Create:
				case ViewModelState.Edit:
					//Сохранение после редактирования с апдейтом портфеля и месяца для всех уточнений
			        ID = AddSPN.ID = BLServiceSystem.Client.SaveDue(AddSPN, (_mPortfolioChanged || _mMonthChanged) ? DopSPNList : null);
				    _isClosedByKDoc = IsClosedByKDoc(AddSPN.ID);
					State = ViewModelState.Edit;
					isReadOnly = UpdateReadOnly();
					break;
			}
		}

		protected override void OnCardSaved()
		{
			base.OnCardSaved();
			RefreshDopSPNList();
		}
		#endregion

		#region Validation
		private const string ERROR_NEWDATE = "Введите дату первичного поступления";
		private const string ERROR_REGNUM = "Введите регистрационный номер";
		/*private const string ERROR_SUMM = "Введите сумму СПН";
		private const string ERROR_PAYMENT_DSV = "Введите корректное значение взносов ДСВ";
		private const string ERROR_PAYMENT_BOSS = "Введите корректное значение взносов работодателя";
		private const string ERROR_SUMM_OVERFLOW = "Введите корректные значения взносов ДСВ и работодателя";*/
		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "NewDate": return (NewDate == null) ? ERROR_NEWDATE : null;
					case "RegNum": return string.IsNullOrWhiteSpace(RegNum) ? ERROR_REGNUM : null;
					case "Summ": return Summ.ValidateMaxFormat(); // значение 0 разрешено
					
					default: return null;
				}
			}
		}

		private bool CheckNewDate()
		{
		    if (NewDate.HasValue)
			{
				if (MaxNewDate.HasValue)
				{
					return NewDate.Value.Date <= MaxNewDate.Value.Date;
				}
				return true;
			}
		    return false;
		}

		private bool Validate()
		{
			return string.IsNullOrWhiteSpace(this["RegNum"]) &&
                   string.IsNullOrWhiteSpace(this["Summ"]) &&
				   string.IsNullOrEmpty(this["PaymentDSV"]) &&
				   string.IsNullOrEmpty(this["PaymentBoss"]) &&
				   AddSPN.MonthID.HasValue &&
				   Account != null &&
				   CheckNewDate();
		}
		#endregion

		public ICommand SelectAccount { get; private set; }
		public ICommand DeleteDop { get; private set; }
		private void InitCommands()
		{
			SelectAccount = new DelegateCommand(o => !ReadAccessOnly, o => ExecuteSelectAccount());
			DeleteDop = new DelegateCommand(o => CanExecuteDeleteDop(), o => ExecuteDeleteDop());
		}

		public void RefreshDopSPNList()
		{
			try
			{
				bool alreadyChanged = IsDataChanged;
				DopSPNList = BLServiceSystem.Client.GetDopSPNListForDue(AddSPN.ID);
				if (DopSPNList.Count > 0)
				{
					MaxNewDate = DopSPNList.First().Date;
					if (MaxNewDate.HasValue)
						MaxNewDateError = string.Format(M_MAX_NEW_DATE_ERROR_FORMAT, MaxNewDate.Value.ToString("dd.MM.yyyy"));
				}
				IsDataChanged = alreadyChanged;
				isReadOnly = UpdateReadOnly();
			}
			catch (Exception e)
			{
				HandleException(e);
			}
		}


		public DueViewModel(long id, PortfolioIdentifier.PortfolioTypes pType)
		//: base(typeof(DueListViewModel))
		{
            DataObjectTypeForJournal = typeof(AddSPN);
            Type = pType;
			InitCommands();
			MonthsList = DataContainerFacade.GetList<Month>();
			YearsList = DataContainerFacade.GetList<Year>();

			if (id <= 0)
			{
				State = ViewModelState.Create;

				AddSPN = new AddSPN
				{
					Kind = (int) AddSPN.Kinds.Due
				};
				OperationDate = DateTime.Today;
				NewDate = DateTime.Today;
				//m_MonthsList = DataContainerFacade.GetList<Month>();//http://jira.vs.it.ru/browse/PFRPTK-1660?focusedCommentId=206765&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-206765
				SelectedMonth = DateTime.Today.Month;
				SelectedYear = DateTime.Today.Year % 100;
				DopSPNList = new List<DopSPNListItem>();

				isReadOnly = UpdateReadOnly();
			}
			else
			{
				State = ViewModelState.Edit;
				Load(id);
            }
            OnPropertyChanged("Summ");
		}

		private void Load(long id)
		{
			State = ViewModelState.Edit;
			ID = id;

			var data = DataContainerFacade.GetByID<AddSPN, long>(ID);

			if (data == null)
				return;

			AddSPN = data;

			if (AddSPN.PFRBankAccountID.HasValue && AddSPN.PortfolioID.HasValue)
				Account = BLServiceSystem.Client.GetPortfolioFull(AddSPN.PFRBankAccountID.Value, AddSPN.PortfolioID.Value);

			RefreshDopSPNList();
			isReadOnly = UpdateReadOnly();
			IsDataChanged = false;
		}


		private bool UpdateReadOnly()
		{
			//Создание - можно редактировать
			if (AddSPN.ID == 0)
				return false;

			// могли быть изменены свойства, влияющие на определение наличия ДК: год и период
			_isClosedByKDoc = IsClosedByKDoc(AddSPN.ID);

			//Создан из ДК - нельзя редактировать
			if (AddSPN.KDocID > 0)
				return true;

			//Есть Уточнения - редактировать нельзя
			if (DopSPNList.Count > 0)
				return true;

            //если нет уточнений за месяц, но есть блокирующее уточенние за квартал
		    if (DueHelper.GetIsBlockedByQuarterDop(AddSPN))
		        return true;

			//Есть ДК - значит редактировать уже нельзя
			return _isClosedByKDoc ?? false;
		}

		public IList<KeyValuePair<Type, long>> GetUpdatedList()
		{
		    var res = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (AddSPN), AddSPN.ID)};
		    res.AddRange(DopSPNList.Select(item => item.IsKDoc ? new KeyValuePair<Type, long>(typeof (KDoc), item.ID) : new KeyValuePair<Type, long>(typeof (DopSPN), item.ID)));
		    return res;
		}

		Type[] IUpdateListenerModel.UpdateListenTypes => new[] { typeof(AddSPN) };

	    void IUpdateListenerModel.OnDataUpdate(Type type, long id)
		{
			if (type == typeof(AddSPN) && id == ID)
			{
				Load(id);
			}
		}
	}
}
