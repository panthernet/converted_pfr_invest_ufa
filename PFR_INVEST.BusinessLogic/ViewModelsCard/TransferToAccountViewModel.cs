﻿using System;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public class TransferType
    {
        public TransferType(string s_tt)
        {
            Title = s_tt;
        }
        public string Title { get; set; }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class TransferToAccountViewModel : AccModelBase, ICloseViewModelAfterSave
    {
        #region Fields
        //private IList<Currency> currList;

        private TransferType[] _contents = { new TransferType("Перечисление"), new TransferType("Конвертация") };

        public TransferType[] ContentList
        {
            get { return _contents; }
            set
            {
                _contents = value;
            }
        }

        public override object Content
        {
            get
            {
                foreach (var t in _contents.Where(t => t.Title == acc.Content))
                    return t;
                return _contents[0];
            }
            set { acc.Content = ((TransferType)value).Title; OnPropertyChanged("Content"); }
        }

        public string RegNum
        {
            get { return acc.RegNum; }
            set
            {
                if (acc.RegNum != value)
                {
                    acc.RegNum = value;
                    OnPropertyChanged("RegNum");
                }
            }
        }

        private PortfolioFullListItem _destaccount;
        /// <summary>
        /// Портфель (счёт)
        /// </summary>
        public PortfolioFullListItem DestAccount
        {
            get { return _destaccount; }

            set
            {
                _destaccount = value;
                if (value != null)
                    acc.PortfolioID = value.Portfolio.ID;

                if (value != null)
                    acc.PFRBankAccountID = value.PfrBankAccount.ID;

                OnPropertyChanged("AccountNumber");
                OnPropertyChanged("Bank");
                OnPropertyChanged("PFName");
                OnPropertyChanged("DestAccount");
            }
        }


        /// <summary>
        /// Портфель (счёт) источника
        /// </summary>
        public override PortfolioFullListItem Account
        {
            get { return account; }

            set
            {
                account = value;
                if (value != null)
                    acc.SourcePortfolioID = value.Portfolio.ID;

                if (value != null)
                {
                    acc.SourcePFRBankAccountID = value.PfrBankAccount.ID;
                    SelectedCurrency = value.PfrBankAccount.GetCurrency();
                }

                OnPropertyChanged("Account");
            }
        }

        #endregion

        #region Execute implementation

        /// <summary>
        /// Открытие выбора счета (портфеля)
        /// </summary>
        private void ExecuteSelectDestAccount()
        {
            var selected = DialogHelper.SelectPortfolio();

            if (selected == null) return;
            if (ComparePortfolios(Account, selected))
                DialogHelper.ShowError("Выбранный портфель указан в качестве источника");
            else if (CompareAccounts(Account, selected))
                DialogHelper.ShowError("Выбранный счет указан в качестве источника");
            else 
                DestAccount = selected;
        }

        protected override void ExecuteSelectAccount()
        {
            var selected = DialogHelper.SelectPortfolio();

            if (selected == null) return;
            if (ComparePortfolios(DestAccount, selected))
                DialogHelper.ShowError("Выбранный портфель указан в качестве источника");
            else if (CompareAccounts(DestAccount, selected))
                DialogHelper.ShowError("Выбранный счет указан в качестве получателя");
            else
                Account = selected;
        }

        private static bool ComparePortfolios(PortfolioFullListItem x, PortfolioFullListItem y)
        {
            return x != null && y != null && x.PortfolioID == y.PortfolioID;
        }

        private static bool CompareAccounts(PortfolioFullListItem x, PortfolioFullListItem y)
        {
            return x != null && y != null && x.PfrBankAccount.ID == y.PfrBankAccount.ID && x.PortfolioID == y.PortfolioID;
        }
        #endregion

        #region Validation
        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Неверный формат данных";
                switch (columnName.ToUpper())
                {
                    case "REGNUM": return ((acc.RegNum ?? "").Length > 32) ? errorMessage : null;
                    default: return base[columnName];
                }
            }
        }

        protected override string Validate()
        {
            const string errorMessage = "Неверный формат данных";

            return DestAccount == null ? errorMessage : base.Validate();

            //return (acc == null || acc.CurrencyID == null || acc.PFRBankAccountID == null || acc.Summ == null || acc.PortfolioID == null || this.Rate == 0) //|| string.IsNullOrEmpty(acc.Content) 
            //    ? errorMessage : null;
        }
        #endregion

        public ICommand SelectDestAccount { get; private set; }

        public override void Load(long lId)
        {
            try
            {
                ID = lId;
                acc = DataContainerFacade.GetByID<AccOperation, long>(lId);

                if (acc.PFRBankAccountID.HasValue && acc.PortfolioID.HasValue)
                {
                    DestAccount = BLServiceSystem.Client.GetPortfolioFull((long)acc.PFRBankAccountID, (long)acc.PortfolioID);
                }

                if (acc.SourcePFRBankAccountID.HasValue && acc.SourcePortfolioID.HasValue)
                {
                    Account = BLServiceSystem.Client.GetPortfolioFull((long)acc.SourcePFRBankAccountID, (long)acc.SourcePortfolioID);
                }

                if (acc.Content == null)
                    acc.Content = "Перечисление";
            }
            catch (Exception e)
            {
                if (DoNotThrowExceptionOnError == false)
                    throw;

                Logger.WriteException(e);
                throw new ViewModelInitializeException();
            }
        }
        private bool _AddToCHFR;
        public bool AddToCHFR
        {
            get { return _AddToCHFR = acc.AddToCHFR != 0; }
            set
            {
                if (SetPropertyValue(nameof(AddToCHFR), ref _AddToCHFR, value))
                    acc.AddToCHFR = value ? 1 : 0;
            }
        }
        public TransferToAccountViewModel()
        {
            SelectDestAccount = new DelegateCommand(o => !IsReadOnly, o => ExecuteSelectDestAccount());

            acc.Content = "Перечисление";
            acc.KindID = 3;// потому что перечисление на счет			
        }
    }
}
