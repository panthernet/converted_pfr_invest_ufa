﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;

namespace PFR_INVEST.BusinessLogic.ViewModelsCard
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class CommonPPSplitViewModel : ViewModelCard, IUpdateRaisingModel, IRequestCustomAction, ICloseViewModelAfterSave
    {
        #region Properties

        public string PFName => (Account ?? PortfolioFullListItem.Empty).PFName;

        public string AccountNumber => (Account ?? PortfolioFullListItem.Empty).AccountNumber;

        private long? _directionElID;
        public long? DirectionElID
        {
            get { return _directionElID; }
            set
            {
                if (_directionElID != value)
                {
                    _directionElID = value; OnPropertyChanged("DirectionElID");
                    //При смене напрaвления сбрасываем связи
                    ResetTransfer();
                }
            }
        }

        private long? _sectionElID;
        public long? SectionElID
        {
            get { return _sectionElID; }
            set
            {
                if (_sectionElID != value)
                {
                    if (value == -1)
                        value = null;
                    _sectionElID = value;
                    OnPropertyChanged("SectionElID");
                    //TODO не нужно? IsRegisterAllowed = value.HasValue;
                    //Сброс выбраного реестра
                    LinkedRegisterID = null;
                    LinkedRegisterText = null;
                    //При смене реестра сбрасываем связи
                    ResetTransfer();
                }

            }
        }



        /// <summary>
        /// Видимость полей формы с реестром
        /// </summary>
        public bool IsRegisterAllowed => true;

        private long? _paymentDetailsID;
        public long? PaymentDetailsID
        {
            get { return _paymentDetailsID; }
            set
            {
                if (_paymentDetailsID != value)
                {
                    _paymentDetailsID = value;
                    OnPropertyChanged("PaymentDetailsID");
                }
            }
        }

        private DateTime? _spnDate;
        public DateTime? SPNDate => _spnDate;

        private DateTime? _payerIssueDate;
        public DateTime? PayerIssueDate
        {
            get { return _payerIssueDate; }
            set { _payerIssueDate = value; OnPropertyChanged("PayerIssueDate"); }
        }

        public List<Element> DirectionList { get; private set; }
        public List<Element> SectionList { get; private set; }
        public List<PaymentDetail> PaymentDetailList { get; private set; }
        public List<KBK> KBKList { get; private set; }

        private PaymentDetail _paymentDetail;
        public PaymentDetail PaymentDetail
        {
            get { return _paymentDetail; }
            set
            {
                _paymentDetail = value;
                KBKID = value != null ? value.KBKID : null;
                OnPropertyChanged("PaymentDetail");
            }
        }

        private long? _kbkId;
        public long? KBKID
        {
            get { return _kbkId; }
            set { if (_kbkId != value) { _kbkId = value; OnPropertyChanged("KBKID"); } }
        }

        private long? _linkedRegisterID;
        public long? LinkedRegisterID
        {
            get { return _linkedRegisterID; }
            set { if (_linkedRegisterID != value) { _linkedRegisterID = value; OnPropertyChanged("LinkedRegisterID"); } }
        }

        private PortfolioFullListItem _mAccount;
        public PortfolioFullListItem Account
        {
            get
            {
                return _mAccount;
            }
            set
            {
                _mAccount = value;
                if (_mAccount != null)
                {
                    //TODO on save
                    //PP.PFRBankAccountID = value.AccountID;
                    //PP.PortfolioID = value.PortfolioID;

                    OnPropertyChanged("Account");
                    OnPropertyChanged("AccountNumber");
                    OnPropertyChanged("PFName");
                }
            }
        }

        private string _linkedRegisterText;
        public string LinkedRegisterText
        {
            get { return _linkedRegisterText; }
            set { _linkedRegisterText = value; OnPropertyChanged("LinkedRegisterText"); }
        }

        /// <summary>
        /// Является ли Направление доступным только для чтения
        /// </summary>
        public bool IsDirectionReadOnly => LinkedRegisterID.HasValue;

        /// <summary>
        /// Являются ли поля формы (для п/п) доступными только для чтения
        /// </summary>
        public override bool IsReadOnly => false;


        public List<AsgFinTr> PPList { get; set; }

        public ICommand FindContragentsCommand { get; private set; }
        public ICommand SelectAccountCommand { get; private set; }
        public ICommand SelectRegisterCommand { get; private set; }
        public ICommand ClearRegisterCommand { get; private set; }

        public readonly List<long> AllowedSections = new List<long> { (long)AsgFinTr.Sections.NPF, (long)AsgFinTr.Sections.SI, (long)AsgFinTr.Sections.VR };

        #endregion

        public CommonPPSplitViewModel()
        {
            DataObjectTypeForJournal = typeof(AsgFinTr);

            //списки
            DirectionList = BLServiceSystem.Client.GetElementByType(Element.Types.PPDirection);
            var sl = BLServiceSystem.Client.GetElementByType(Element.Types.PPSection).Where(s => AllowedSections.Contains(s.ID)).ToList();
            sl.Insert(0, new Element { ID = -1, Name = "(не указан)" });
            SectionList = sl;
            PaymentDetailList = DataContainerFacade.GetList<PaymentDetail>();
            KBKList = DataContainerFacade.GetList<KBK>(false);

            //команды
            FindContragentsCommand = new DelegateCommand(a=> ValidateFields(), a=> FindContragents());
            SelectAccountCommand = new DelegateCommand(o => !IsReadOnly, o => { SelectAccountExecute(); });
            SelectRegisterCommand = new DelegateCommand(o => DirectionElID.HasValue && SectionElID.HasValue, o => { SelectRegisterExecute(); });
            ClearRegisterCommand = new DelegateCommand(o => DirectionElID.HasValue && SectionElID.HasValue && LinkedRegisterID.HasValue,
                                                        o => { SetRegister(null); });
        }

        private readonly DateTime _ppDate;

        private readonly Portfolio _pf;

        public CommonPPSplitViewModel(DateTime date)
            : this()
        {
            _ppDate = date;
            _pf = DataContainerFacade.GetListByProperty<Portfolio>("StatusID", 2L).FirstOrDefault();
            if (_pf == null)
                return;
            var pp = DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("DraftPayDate", _ppDate),
                ListPropertyCondition.Equal("PortfolioID", _pf.ID)
            }).FirstOrDefault();        

            if (pp == null) return;
            _spnDate = pp.SPNDate;

            if (pp.PFRBankAccountID.HasValue && pp.PortfolioID.HasValue)
                Account = BLServiceSystem.Client.GetPortfolioFull(pp.PFRBankAccountID.Value, pp.PortfolioID.Value);
        }

        private void SelectRegisterExecute()
        {
               var selected = DialogHelper.ShowSelectRegisterForPP((AsgFinTr.Sections)SectionElID, DirectionElID ?? 0);
            if (selected != null)
            {
                SetRegister(selected);
                //При смене реестра сбрасываем связи
                ResetTransfer();
            }
        }

        /// <summary>
        /// Установка регистра
        /// </summary>
        /// <param name="register">Регистр</param>
        /// <param name="forLoad">Флаг, указывающий, что регистр загружается при открытии формы (пропуск сброса данных трансфера)</param>
        private void SetRegister(CommonRegisterListItem register, bool forLoad = false)
        {
            if (register != null)
            {
                LinkedRegisterID = register.ID;
                LinkedRegisterText = register.Name;
            }
            else
            {
                LinkedRegisterID = null;
                LinkedRegisterText = null;
            }
            OnPropertyChanged("IsDirectionReadOnly");
        }

        private void SelectAccountExecute()
        {
            var selected = DialogHelper.SelectPortfolio();
            if (selected != null)
                Account = selected;
        }

        private void FindContragents()
        {

            PPList =
                DataContainerFacade.GetListByPropertyConditions<AsgFinTr>(new List<ListPropertyCondition>
                {
                    ListPropertyCondition.Equal("DraftPayDate", _ppDate), 
                    //если раздел Работа с ВР - выбираем также и раздел Работа с СИ
                    ListPropertyCondition.In("SectionElID", new object[]{ _sectionElID, _sectionElID == (long)AsgFinTr.Sections.VR ? (long)AsgFinTr.Sections.SI : _sectionElID}.Distinct().ToArray()), 
                    ListPropertyCondition.Equal("DirectionElID", _directionElID), 
                    ListPropertyCondition.Equal("PortfolioID", _pf.ID), 
                    ListPropertyCondition.IsNull("ReqTransferID"), 
                    ListPropertyCondition.IsNull("FinregisterID"), 
                    ListPropertyCondition.NotEqual("StatusID", -1L)
                }).ToList();

            if (_sectionElID == (long) AsgFinTr.Sections.NPF)
            {
                //var register = DataContainerFacade.GetByID<Register>(_linkedRegisterID);
                var isFromNPF = (long) AsgFinTr.Directions.ToPFR == _directionElID;
                var list = BLServiceSystem.Client.GetFinregListForCommonPPSplit(_linkedRegisterID.Value, isFromNPF);
                    //DataContainerFacade.GetByID<Register>(_linkedRegisterID).GetFinregistersList();
                PPList.Where(a=> a.InnLegalEntityID.HasValue).ForEach(pp =>
                {
                    var found = list.FirstOrDefault(a => a.Count == pp.PaySum && pp.InnLegalEntityID == (isFromNPF ? a.GetCreditAccount() : a.GetDebitAccount()).GetContragent().GetLegalEntity().ID);
                    pp.FinregisterID = found?.ID;
                    if (found != null)
                    {
                        pp.ContragentName = (isFromNPF ? found.GetCreditAccount() : found.GetDebitAccount()).GetContragent().Name;
                        list.Remove(found);
                    }
                });
            }
            else
            {
                //var register = DataContainerFacade.GetByID<SIRegister>(_linkedRegisterID);
                var list = new List<ReqTransfer>();
                //var isTwoPP = (long)AsgFinTr.Directions.ToPFR == _directionElID;
                list.AddRange(BLServiceSystem.Client.GetReqTransferListForCommonPPSplit(_linkedRegisterID.Value)); //register.GetListSITransfer().ForEach(a=> list.AddRange(a.GetReqTransfers()));
                if ((long)AsgFinTr.Directions.FromPFR == _directionElID)
                {
                    //по суммам
                    PPList.Where(a => a.InnLegalEntityID.HasValue).ForEach(pp =>
                    {
                        //Не проверяем InnContractID
                        //Виталий Ярхамов: в платежках, выяснилось, что всегда присутствуют ИНН и КПП - но они всегда принадлежат именно самому контрагенту - т.е. УК или НПФ. А у Договора УК если и есть свой ИНН - то даже у нас в ПТК ДОКИП он не содержится.
                        //а тот, что есть на форме договора - это не ИНН самого договора, а банка плательщика - как то так.
                        var found = list.FirstOrDefault(a => a.Sum == pp.PaySum && pp.InnLegalEntityID == a.GetTransferList().GetContract().GetLegalEntity().ID);
                        pp.ReqTransferID = found?.ID;
                        if (found != null)
                        {
                            pp.ContragentName = found.GetTransferList().GetContract().GetLegalEntity().FormalizedName;
                            list.Remove(found);
                        }
                    });
                }
                else
                {
                    //тут всегда две платежки, ищем сумму по двум
                    var tmpPPList = PPList.ToList();
                    list.ToList().ForEach(tr =>
                    {
                        var c = tr.GetTransferList().GetContract();
                        var le = c.GetLegalEntity();
                        var fPPList = tmpPPList.Where(a => (a.InnLegalEntityID.HasValue && a.InnLegalEntityID == le.ID)).ToList();          // || (a.InnContractID.HasValue && a.InnContractID == c.ID)              
                        for (int i = 0; i < fPPList.Count; i++)
                            for (int j = i+1; j < fPPList.Count; j++)
                            {
                                var sum = fPPList[i].PaySum + fPPList[j].PaySum;
                                //нашли соответствие
                                if (sum == tr.Sum)
                                {
                                    fPPList[i].ReqTransferID = tr.ID;
                                    fPPList[j].ReqTransferID = tr.ID;
                                    fPPList[i].ContragentName = le.FormalizedName;
                                    fPPList[j].ContragentName = le.FormalizedName;
                                    tmpPPList.Remove(fPPList[i]);
                                    tmpPPList.Remove(fPPList[j]);
                                    list.Remove(tr);
                                    return;
                                }
                            }                        
                    });

                    _singleIdList.Clear();
                    if (tmpPPList.Count > 0)
                    {
                        var oneList = BLServiceSystem.Client.GetReqTransferPartialListForCommonPPSplit(_linkedRegisterID);
                        oneList.ForEach(tr =>
                        {
                            var fPPList = tmpPPList.Where(a => (a.InnLegalEntityID.HasValue && a.InnLegalEntityID == tr.LegalEntityID)).ToList();                        
                            for (int i = 0; i < fPPList.Count; i++)
                                if (tr.SumDiff == fPPList[i].PaySum)
                                {
                                    fPPList[i].ReqTransferID = tr.ReqTransferID;
                                    fPPList[i].ContragentName = tr.LegalEntityName;
                                    _singleIdList.Add(tr.ReqTransferID);
                                    tmpPPList.Remove(fPPList[i]);
                                    return;
                                }
                        });
                    }
                }
            }
            PPList.Where(a=> !string.IsNullOrEmpty(a.ContragentName)).ForEach(a=> a.IsSelected = true);
            _singleIdList = _singleIdList.Distinct().ToList();
            OnPropertyChanged("PPList");
        }
        /// <summary>
        /// Временный список ID перечислений, для СИ/ВР направления приход, когда найдено 2ое п/п
        /// </summary>
        private List<long> _singleIdList = new List<long>();

        public override bool CanExecuteSave()
        {
            return State == ViewModelState.Create && ValidateFields() && PPList != null && PPList.Count > 0 && PPList.Count(a=> a.IsSelected) > 0;
        }

        protected override void ExecuteSave()
        {
            State = ViewModelState.Edit;
            var isTwoPP = _sectionElID != (long) AsgFinTr.Sections.NPF && (long)AsgFinTr.Directions.ToPFR == _directionElID;
            var list = PPList.Where(a=> a.IsSelected).ToList();
            list.ForEach(pp =>
            {
                pp.LinkedRegisterID = _linkedRegisterID;
                pp.KBKID = _kbkId;
                pp.PFRBankAccountID = Account.AccountID;
                pp.PortfolioID = Account.PortfolioID;
                pp.LinkElID = pp.FinregisterID.HasValue ? (long) AsgFinTr.Links.NPF : (long) AsgFinTr.Links.UK;
                pp.PaymentDetailsID = _paymentDetailsID;
                pp.PayerIssueDate = _payerIssueDate;
            });

            BLServiceSystem.Client.BatchUpdatePP(list, isTwoPP, _singleIdList);
            IsDataChanged = false;
        }

        #region Validation

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "PaymentDetailsID": return PaymentDetailsID.ValidateRequired();
                    case "DirectionElID": return DirectionElID.ValidateRequired();
                    case "AccountNumber": return AccountNumber.ValidateRequired();
                    case "LinkedRegisterID": return LinkedRegisterID.ValidateRequired();
                    case "SectionElID": return SectionElID.ValidateRequired();
                    default:
                        return null;
                }
            }
        }


        #endregion

        private void ResetTransfer()
        {
            OnPropertyChanged("IsDirectionReadOnly");
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            var list = new List<KeyValuePair<Type, long>>();
            PPList.Where(a => a.IsSelected).ForEach(a => list.Add(new KeyValuePair<Type, long>(typeof(AsgFinTr), a.ID)));
            return list;
        }

        public event EventHandler RequestCustomAction;

        private void OnRequestCustomAction()
        {
            if (RequestCustomAction != null)
                RequestCustomAction(null, null);
        }

    }


}
