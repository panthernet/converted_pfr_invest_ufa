﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class AllocationNPFViewModel : FinRegisterViewModel, ICloseViewModelAfterSave
	{
		private readonly SPNReturnViewModel _parentModel;

		public override bool IsCFRCountVisible => true;

	    public override Visibility TempAllocationVisibility => Visibility.Visible;
	    public override Visibility CommonFinregisterVisibility => Visibility.Collapsed;

	    // recordID - идентификатор основной записи реестра, поэтому никаких дополнительных запросов при проверке привязанных НПФ не требуется
		public AllocationNPFViewModel(long recordId, ViewModelState action)
			: base(recordId, action)
		{
			//RegConnectedCard();
			FilterNPFList();
		}

		/*private void RegConnectedCard()
		{
		    if (ConnectedListViewModels.Contains(typeof (NPFTempAllocationListViewModel))) return;
		    var t = ConnectedListViewModels.ToList();
		    t.Add(typeof(NPFTempAllocationListViewModel));
		    ConnectedListViewModels = t;
		}*/

		public override void RefreshConnectedCards()
		{
		    if (GetViewModel == null) return;
		    var rVM = GetViewModel(typeof(SPNAllocationViewModel)) as SPNAllocationViewModel;
		    if (rVM != null)
		        rVM.LoadRegister();
		    var rrVM = GetViewModel(typeof(SPNReturnViewModel)) as SPNReturnViewModel;
		    if (rrVM != null)
		        rrVM.LoadRegister();
		    //var frVM = GetViewModel(typeof(AllocationNPFViewModel)) as AllocationNPFViewModel;
		    //if (frVM != null)
		    //    frVM.RefreshStatus();
		}

        /// <summary>
        /// конструктор для редактирования НПФ в памяти (Реестр изъятия еще не сохранен в БД)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="parentModel"></param>
        public AllocationNPFViewModel(Finregister source, SPNReturnViewModel parentModel)
		{
			State = ViewModelState.Edit;
			RefreshConnectedCardsViewModels = RefreshConnectedCards;

			Finregister = source;
			Register = parentModel.register;			
			_parentModel = parentModel;

			FilterNPFList();

			StatusNPFList = DataContainerFacade.GetList<Status>().Where(x => x.ID > 0).ToList();
			NPFGarantList = BLServiceSystem.Client.GetElementByType(Element.Types.GarantACB).Where(x => x.Visible).ToList();

			FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;
			FinStatuses = RegisterIdentifier.FinregisterStatuses.ToList;

			SelectedNPFGarant = NPFGarantList.FirstOrDefault();
			SelectedStatusNPF = StatusNPFList.FirstOrDefault();

			Status = FinStatuses.First(x => RegisterIdentifier.FinregisterStatuses.IsCreated(x));
			FinMoveType = FinMoveTypes.First();

			if (NPFList != null && Finregister != null)
			{

				var caId = GetCurrentFinregisterContragent();

				SelectedNPF = NPFList.FirstOrDefault(ca => ca.ContragentID == caId);

				if (SelectedNPF != null)
				{
					var contragent = SelectedNPF.GetContragent();
					SelectedStatusNPF = StatusNPFList.FirstOrDefault(x => x.ID == contragent.StatusID);
					SelectedNPFGarant = NPFGarantList.FirstOrDefault(x => x.ID == SelectedNPF.GarantACBID);					
				}

				SelectedNPF = NPFList.FirstOrDefault(ca => 	ca.ContragentID == caId);
			}

			//RegConnectedCard();
		}

		/// <summary>
		/// конструктор для добавления НПФ в реестр изъятия, находящийся в памяти
		/// </summary>
		/// <param name="parentModel"></param>
		public AllocationNPFViewModel(SPNReturnViewModel parentModel)
		{
			_parentModel = parentModel;
			State = ViewModelState.Create;
			RefreshConnectedCardsViewModels = RefreshConnectedCards;

		    Finregister = new Finregister {ZLCount = 0, CFRCount = 0};
		    Register = parentModel.register;			

			FilterNPFList();

			StatusNPFList = DataContainerFacade.GetList<Status>().Where(x => x.ID > 0).ToList();
			NPFGarantList = BLServiceSystem.Client.GetElementByType(Element.Types.GarantACB).Where(x => x.Visible).ToList();

			FinMoveTypes = RegisterIdentifier.FinregisterMoveTypes.ToList;
			FinStatuses = RegisterIdentifier.FinregisterStatuses.ToList;

			SelectedNPFGarant = NPFGarantList.FirstOrDefault();
			SelectedStatusNPF = StatusNPFList.FirstOrDefault();

			Status = FinStatuses.First(RegisterIdentifier.FinregisterStatuses.IsCreated);

			FinMoveType = FinMoveTypes.First();
			//RegConnectedCard();
		}

		protected override void ExecuteSave()
		{
			if (State == ViewModelState.Create && _parentModel != null)
			{
				BindNPFAccounts();
				var targetFr = _parentModel.FinregWithNPFList.FirstOrDefault(x => x.Finregister.CrAccID == Finregister.CrAccID && x.Finregister.DbtAccID == Finregister.DbtAccID);
				if (targetFr == null)
				{
					var caId = GetCurrentFinregisterContragent();
					string npfName = SelectedNPF.FormalizedNameFull;
					if (caId != null)
					{
						npfName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caId, "Name");
					}

					var newList = _parentModel.FinregWithNPFList;
					//ParentModel.FinregWithNPFList
					newList.Add(new FinregisterWithCorrAndNPFListItem
					{
						CFRCount = Finregister.CFRCount,
						Count = Finregister.Count,
						Finregister = Finregister,
						ID = 0,
						IsItemChanged = true,
						NPFName = npfName,
						//NPFStatus = SelectedNPF.GetLegalStatus().Name,
						ZLCount = Finregister.ZLCount
					});

					_parentModel.FinregWithNPFList = newList;
				}
				else
				{
					targetFr.CFRCount = Finregister.CFRCount;
					targetFr.Count = Finregister.Count;
					targetFr.ZLCount = Finregister.ZLCount;
					targetFr.IsItemChanged = true;
					//targetFR.NPFName = SelectedNPF.FormalizedNameFull;
					//targetFR.NPFStatus = SelectedNPF.GetLegalStatus().Name;
				}

				_parentModel.RaiseFinregisterChanged();
				
				return;
			}

			base.ExecuteSave();
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			// валидация корректности сумм после сохранения
		    if (_parentModel != null || Register == null || Register.ID <= 0 || SelectedNPF == null) return base.BeforeExecuteSaveCheck();
		    if (RegisterIdentifier.IsToNPF(Register.Kind))
		    {// если редактирование не кешированного списка (добавление\редактирование НПФ в изъятие)
		        var allowedFR = GetFinregWithAllowedSums();
		        if (allowedFR == null) return false;
		        // для изъятия суммы не должны превосходить рассчетные
		        if (Count > allowedFR.Count)
		        {
		            DialogHelper.ShowAlert("Недостаточно средств в размещении. Максимально допустимая сумма {0}", allowedFR.Count);
		            return false;
		        }

		        if (ZLCount > allowedFR.ZLCount)
		        {
		            DialogHelper.ShowAlert("Неверно указано количество ЗЛ. Максимально допустимое количество {0}", allowedFR.ZLCount);
		            return false;
		        }
		    }
		    else
		    {// редактирование НПФ размещения
		        // при редактировании исчерпанного размещения будет недостаток средств, который должен быть компенсирован (не даем сохранить размещение, если сумма изъяти окажется в итоге больше суммы размещения)
		        var allowedFR = GetFinregWithAllowedSums();
		        if (allowedFR == null) return true;

		        if (-allowedFR.Count > Count)
		        {
		            DialogHelper.ShowAlert("Сумма в размещении недостаточна для существующих в системе изъятий. Минимально допустимая сумма {0}", -allowedFR.Count);
		            return false;
		        }

		        if (-allowedFR.ZLCount > ZLCount)
		        {
		            DialogHelper.ShowAlert("Неверно указано количество ЗЛ. Минимально допустимое количество {0}", -allowedFR.ZLCount);
		            return false;
		        }

		        // также надо проверить остаток суммы при смене выбранного НПФ
		        if (Finregister.ID > 0)
		        {
		            var dbFR = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
		            (new List<Finregister> { dbFR }).PopulateBankAccounts();
		            var accountDebit = dbFR.GetDebitAccount();
		            var accountCredit = dbFR.GetCreditAccount();

		            if (accountDebit != null && accountCredit != null)
		            {
		                var caId = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID;

		                if (caId != SelectedNPF.ContragentID)
		                {
		                    var caName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caId, "Name");
		                    var portfID = Register.PortfolioID.Value;
		                    //var selName = SelectedNPF.GetContragent().Name;

		                    var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);
		                    var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);

		                    if (allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count) < 0)
		                    {
		                        DialogHelper.ShowAlert("Невозможно сменить НПФ, так как сумма изъятия превышает остаток размещения");
		                        return false;
		                    }

		                    if (allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount) < 0)
		                    {
		                        DialogHelper.ShowAlert("Невозможно удалить НПФ, так как количество ЗЛ изъятия превышает количество ЗЛ размещения");
		                        return false;
		                    }
		                }
		            }

		            //return true;
		        }
		    }

		    return base.BeforeExecuteSaveCheck();
		}

		public override bool BeforeExecuteDeleteCheck()
		{
		    if (Register == null || Register.ID <= 0 || !RegisterIdentifier.IsTempAllocation(Register.Kind)) return base.BeforeExecuteDeleteCheck();
		    var dbFR = DataContainerFacade.GetByID<Finregister>(Finregister.ID);
		    (new List<Finregister> { dbFR }).PopulateBankAccounts();
		    var accountDebit = dbFR.GetDebitAccount();
		    var accountCredit = dbFR.GetCreditAccount();

		    if (accountDebit != null && accountCredit != null)
		    {
		        var caID = RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID;

		        var caName = DataContainerFacade.GetObjectProperty<Contragent, long, string>(caID, "Name");
		        var portfID = Register.PortfolioID.Value;
		        //var selName = SelectedNPF.GetContragent().Name;

		        var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);
		        var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(portfID).Where(x => x.NPFName == caName && x.ID != Finregister.ID);

		        if (allListSPN.Sum(x => x.Count) - retListSPN.Sum(x => x.Count) < 0)
		        {
		            DialogHelper.ShowAlert("Невозможно удалить НПФ, так как сумма изъятия превышает остаток размещения");
		            return false;
		        }

		        if (allListSPN.Sum(x => x.ZLCount) - retListSPN.Sum(x => x.ZLCount) < 0)
		        {
		            DialogHelper.ShowAlert("Невозможно удалить НПФ, так как количество ЗЛ изъятия превышает количество ЗЛ размещения");
		            return false;
		        }
		    }

		    return base.BeforeExecuteDeleteCheck();
		}

		//public override string this[string columnName]
		//{
		//    get
		//    {
		//        const string errorMessage = "Неверный формат данных";
		//        switch (columnName.ToLower())
		//        {
		//            case "cfrcount": return  this.CFRCount == null || this.CFRCount < 0  ? errorMessage : null;
		//            default: return base[columnName];
		//        }
		//    }
		//}

		//protected override string Validate()
		//{
		//    const string fieldNames =
		//        "cfrcount";

		//    foreach (string fieldName in fieldNames.Split("|".ToCharArray()))
		//    {
		//        if (!String.IsNullOrEmpty(this[fieldName]))
		//        {
		//            return "Неверный формат данных";
		//        }
		//    }

		//    return base.Validate();
		//}

		protected override void FilterNPFList()
		{
			if (_parentModel != null)
			{
				//var avList = ParentModel.GetAvailableNPFList();
				//var currentNPF = avList.FirstOrDefault(x=>x.CrAccID == this.finregister.CrAccID && x.DbtAccID == this.finregister.DbtAccID);
				//avList.RemoveAll(x => ParentModel.FinregWithNPFList.Any(y => y.NPFName == x.NPFName && y.NPFName != currentNPF.NPFName));
				var allNPF = BLServiceSystem.Client.GetNPFListLEHib(StatusFilter.NotDeletedFilter).ToList();

				// исключить НПФ, содержащиеся в реестре и НПФ, у которых недостаточно средств в размещении
				// добавить НПФ, если этот нпф текущий при редактировании
				var avList = _parentModel.GetAvailableNPFList(true);

				var caID = GetCurrentFinregisterContragent();

				if (caID != null)
				{
					NPFList = allNPF.Where(x => avList.Any(y => y.NPFName == x.GetContragent().Name)
						|| x.ContragentID == caID).ToList();
				}
				else
				{
					NPFList = allNPF.Where(x => avList.Any(y => y.NPFName == x.GetContragent().Name)).ToList();
				}
			}
			else
			{
				// базовый фильтр не учитывает средства размещения

				//base.filterNPFList();
				if (Register != null && Register.PortfolioID.HasValue)
				{
					// загрузка всех доступных НПФ
					base.FilterNPFList();

					// используется основной реестр, который создается при созранении изъятия
					if (RegisterIdentifier.IsToNPF(Register.Kind))
					{
						// выбор только тех НПФ, для которых есть остаток средств размещения
						var portfID = Register.PortfolioID.Value;

						var allListSPN = BLServiceSystem.Client.GetSPNAllocatedFinregisters(portfID);
						var retListSPN = BLServiceSystem.Client.GetSPNReturnedFinregisters(portfID);

						var allSPN = allListSPN.GroupBy(x => x.NPFName);
						var retSPN = retListSPN.GroupBy(x => x.NPFName);

						var avList = new List<LegalEntity>();

						foreach (var gr in allSPN)
						{
							var ret = retSPN.FirstOrDefault(x => x.Key == gr.Key);
							var sumCount = gr.Sum(x => x.Count);
							var ZLCount = gr.Sum(x => x.ZLCount);
							if (ret != null)
							{
								sumCount -= ret.Sum(x => x.Count);
								ZLCount -= ret.Sum(x => x.ZLCount);
							}

							if (sumCount > 0 || ZLCount > 0)
							{
								var avNPF = NPFList.FirstOrDefault(x => x.GetContragent().Name == gr.Key);
								if (avNPF != null) avList.Add(avNPF);
							}
						}

						// восстановление текущего НПФ (может быть исключен из-за недостатка сумм)
						var caID = GetCurrentFinregisterContragent();
						if (caID != null)
						{
							var curNPF = NPFList.FirstOrDefault(x => x.ContragentID == caID);
							if (curNPF != null && avList.All(x => x.ContragentID != caID))
							{
								avList.Add(curNPF);
							}
						}

						NPFList = avList;

						SelectedNPF = caID != null ? NPFList.FirstOrDefault(x => x.ContragentID == caID) : NPFList.FirstOrDefault();
					}
				}
			}
		}

		protected long? GetCurrentFinregisterContragent()
		{
		    if (Finregister == null) return null;
		    (new List<Finregister> { Finregister }).PopulateBankAccounts();
		    var accountDebit = Finregister.GetDebitAccount();
		    var accountCredit = Finregister.GetCreditAccount();

		    if (accountDebit != null && accountCredit != null)
		    {
		        return (RegisterIdentifier.IsFromNPF(Register.Kind.ToLower()) ? accountCredit.ContragentID : accountDebit.ContragentID);
		    }
		    return null;
		}

        public override IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(Finregister), ID) };
        }
	}
}
