﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using System.IO;
using PFR_INVEST.DataAccess.Client;
using System.Xml;

namespace PFR_INVEST.BusinessLogic.XMLModels
{
    [Serializable]
    public class BanksVerificationDataContainer
    {
        public string LegalEntityRatingsXML { get; set; }
        public string MultiplierRatingsXML { get; set; }
        public string RatingsXML { get; set; }
        public string BanksXML { get; set; }
        public string RatingAgenciesXML { get; set; }
        public string DepclaimsXML { get; set; }
        public string BankDepositInfoQuery { get; set; }
        public string BankDepositInfoXML { get; set; }

        [XmlIgnore]
        public IList<LegalEntityRating> LegalEntityRatings { get; set; }
        [XmlIgnore]
        public IList<MultiplierRating> MultiplierRatings { get; set; }
        [XmlIgnore]
        public IList<Rating> Ratings { get; set; }
        [XmlIgnore]
        public IList<LegalEntity> Banks { get; set; }
        [XmlIgnore]
        public IList<RatingAgency> RatingAgencies { get; set; }
        [XmlIgnore]
        public IList<DepClaim2> Depclaims { get; set; }
        [XmlIgnore]
        public IList<BankDepositInfoItem> BankDepositInfo { get; set; }
		[XmlIgnore]
		public bool IsArchiveData { get; set; }


        /// <summary>
        /// Получение данных из БД, заполнение полей сущности и сериализованных данных
        /// </summary>
        public BanksVerificationDataContainer(bool loadDBData = true, bool skipInactiveBanks = false, long? stockId = null)
        {
			if (loadDBData)
			{
                Banks = BLServiceSystem.Client.GetBankListForDeposit(stockId).Distinct().ToList();
			    if (skipInactiveBanks)
			        Banks = Banks.Where(BankCalculator.IsActive).ToList();

				BanksXML = DataContainerFacade.GetXMLByList(Banks.ToList());
				//var ratingAgencies = BLServiceSystem.Client.GetRatingAgenciesList();
				RatingAgencies = DataContainerFacade.GetListByProperty<RatingAgency>("StatusID", (long)1);
				RatingAgenciesXML = DataContainerFacade.GetXMLByPropertyConditions<RatingAgency>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           ListPropertyCondition.Equal("StatusID", (long)1)
                                                                                       });


				LegalEntityRatings = DataContainerFacade.GetListByPropertyConditions<LegalEntityRating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("LegalEntityID", "in", Banks.Select(b => b.ID).Cast<object>().ToArray())
                                                                                       });
				LegalEntityRatingsXML = DataContainerFacade.GetXMLByPropertyConditions<LegalEntityRating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("LegalEntityID", "in", Banks.Select(b => b.ID).Cast<object>().ToArray())
                                                                                       });

				MultiplierRatings = DataContainerFacade.GetListByPropertyConditions<MultiplierRating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("ID","in", LegalEntityRatings.Select(x=>x.MultiplierRatingID).Cast<object>().ToArray())
                                                                                       });
				MultiplierRatingsXML = DataContainerFacade.GetXMLByPropertyConditions<MultiplierRating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("ID","in", LegalEntityRatings.Select(x=>x.MultiplierRatingID).Cast<object>().ToArray())
                                                                                       });

				Ratings = DataContainerFacade.GetListByPropertyConditions<Rating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("ID","in", MultiplierRatings//.Where(p => banks.Select(b=>b.ID).Contains(p.LegalEntityID))
                                                                                                   .Select(p=>p.RatingID).Distinct().Cast<object>().ToArray())
                                                                                       });
				RatingsXML = DataContainerFacade.GetXMLByPropertyConditions<Rating>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("ID","in", MultiplierRatings//.Where(p => banks.Select(b=>b.ID).Contains(p.LegalEntityID))
                                                                                                   .Select(p=>p.RatingID).Distinct().Cast<object>().ToArray())
                                                                                       });
				IsArchiveData = false;
			}
        }

        public void LoadBankLimitData()
        {
            Depclaims = DataContainerFacade.GetListByPropertyConditions<DepClaim2>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("BankID", "in",
                                                                                                   Banks.Select(b => b.ID).Cast<object>().ToArray())
                                                                                       });
            DepclaimsXML = DataContainerFacade.GetXMLByPropertyConditions<DepClaim2>(new List<ListPropertyCondition>
                                                                                       {
                                                                                           new ListPropertyCondition("BankID", "in",
                                                                                                   Banks.Select(b => b.ID).Cast<object>().ToArray())
                                                                                       });

            BankDepositInfo = BLServiceSystem.Client.GetBankDepositInfoListByBankIds(Banks.Select(x => x.ID).ToArray());
            BankDepositInfoXML = Util.GetXmlByEntityList(BankDepositInfo.ToList());
            BankDepositInfoQuery = string.Join("\r\n", BLServiceSystem.Client.GetBankDepositInfoQuery(Banks.Select(x => x.ID).ToArray()));
        }

        public static BanksVerificationDataContainer FromXML(string xml)
        {
			var sr = new MemoryStream(Encoding.UTF8.GetBytes(xml));
			var xReader = XmlReader.Create(sr);

			var res = new BanksVerificationDataContainer(false);

			var sProp = typeof(BanksVerificationDataContainer).GetProperties().Where(x => x.PropertyType == typeof(string)).ToArray();

			// десериализация восстанавливает только xml записи списков
			while (xReader.Read())
			{
				//if (xReader.NodeType == XmlNodeType.Element&&xReader.Name=="BanksXML")
				//{
				//    res.BanksXML = xReader.ReadInnerXml();
				//}
				foreach (var sp in sProp)
				{
					if (xReader.NodeType == XmlNodeType.Element && xReader.Name == sp.Name)
					{
						sp.SetValue(res, xReader.ReadInnerXml(), null);
					}
				}
			}

			//var xmlDoc = new XmlDocument();
			//xmlDoc.Load(sr);

			//var nd = xmlDoc.SelectSingleNode("//Sources/BanksXML");

			//// десериализация восстанавливает только xml записи списков
			//var res = (BanksVerificationDataContainer)ser.Deserialize(sr);

			// восстановление типизированных списков
			res.Banks = DataContainerFacade.GetListByXML<LegalEntity>(res.BanksXML);
			res.RatingAgencies = DataContainerFacade.GetListByXML<RatingAgency>(res.RatingAgenciesXML);
			res.LegalEntityRatings = DataContainerFacade.GetListByXML<LegalEntityRating>(res.LegalEntityRatingsXML);
			res.MultiplierRatings = DataContainerFacade.GetListByXML<MultiplierRating>(res.MultiplierRatingsXML);
			res.Ratings = DataContainerFacade.GetListByXML<Rating>(res.RatingsXML);
			res.Depclaims = DataContainerFacade.GetListByXML<DepClaim2>(res.DepclaimsXML);
			res.BankDepositInfo = Util.GetEntityListByXml<BankDepositInfoItem>(res.BankDepositInfoXML);
			res.IsArchiveData = true;

            return res;
        }

        public string GetXML()
        {
            //var ser = new XmlSerializer(this.GetType());
            //var sw = new StringWriter();
            //ser.Serialize(sw, this);

            //return sw.ToString();

            // получение всех строковых полей для сохранения в xml без экранирования
            var sProp = GetType().GetProperties().Where(x => x.PropertyType == typeof(string));

            using (var ms = new MemoryStream())
            using (var xWriter = new XmlTextWriter(ms, Encoding.UTF8))
            {
                xWriter.Formatting = Formatting.Indented;
                xWriter.Indentation = 4;
                xWriter.IndentChar = ' ';

                xWriter.WriteStartDocument();
				xWriter.WriteStartElement("BanksVerificationDataContainer");

                foreach (var sp in sProp)
                {
                    var val = ((string)sp.GetValue(this, null));
                    xWriter.WriteStartElement(sp.Name);
                    xWriter.WriteRaw("\r\n" + (val ?? string.Empty) + "\r\n");
                    xWriter.WriteEndElement();
                }

                xWriter.WriteEndElement();
                xWriter.WriteEndDocument();
                xWriter.Flush();
				var buffer = ms.ToArray();
                var result = Encoding.UTF8.GetString(buffer);
                return result;
            }

        }
    }
}
