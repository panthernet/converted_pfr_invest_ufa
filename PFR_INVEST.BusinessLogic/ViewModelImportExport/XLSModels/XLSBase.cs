﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Excel;
using System.IO;

namespace PFR_INVEST.BusinessLogic.XLSModels
{
    public abstract class XLSBase
    {
        private Application _excelApp;
        private readonly Workbook _workbook;
        private readonly Worksheet _worksheet;

        public virtual int ColumnsCount { get; set; } = 0;
        public IList<XLSRow<int>> Rows { get; private set; }

        protected XLSBase(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException($"Файл {fileName} не был найден");

            _excelApp = new Application { Visible = false };
            _workbook = _excelApp.Workbooks.Open(fileName);
            _worksheet = (Worksheet)_workbook.Sheets[1];
            if (ColumnsCount == 0)
                throw new Exception("Необходимо задать кол-во колонок!");
            LoadData();
        }

        private void LoadData()
        {
            try
            {
                PopulateRows();
                ValidateRows();
            }
            finally
            {
                if (_excelApp != null)
                {
                    ReleaseComObject(_worksheet);
                    ReleaseComObject(_workbook);
                    _excelApp.Quit();
                    ReleaseComObject(_excelApp);
                    _excelApp = null;
                }
            }
        }

        private static void ReleaseComObject(object comObject)
        {
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(comObject);
        }

        private void PopulateRows()
        {
            Rows = new List<XLSRow<int>>();

            for (int i = 2; i <= _worksheet.UsedRange.Rows.Count; i++)
            {
                var row = new XLSRow<int>
                {
                    Items = new Dictionary<int, object>()
                };

                if(_worksheet.UsedRange.Columns.Count != ColumnsCount)
                    throw new Exception("Не совпадает кол-во столбцов!");

                for (int j = 1; j <= _worksheet.UsedRange.Columns.Count; j++)
                {
                    row.Items[j] = ((Range)_worksheet.UsedRange.Cells[i, j]).Value;
                }

                Rows.Add(row);
            }
        }

        private void ValidateRows()
        {
            if (Rows == null || Rows.Count == 0)
                throw new Exception("Пустой документ.");

            foreach (var row in Rows)
            {
                if(row.Items == null)
                    throw new Exception("Документ не может содержать пустые строки.");
            }
        }
    }

}
