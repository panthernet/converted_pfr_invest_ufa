﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.ViewModelImportExport
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class CBReportExportToXMLViewModel : ViewModelCardDialog
    {
        private const string XML_SCHEME  = "PURCB_OKUD0418001.xsd";
        private const string XML2_SCHEME = "PURCB_OKUD0418002.xsd";
        private const string XML3_SCHEME = "PURCB_OKUD0418003.xsd";
        private const string NS_URI = "http://www.it.ru/Schemas/Avior/ПУРЦБ";
        private const string NS_PREFIX = "av";


        private static void SaveToRepo(byte[] content, long reportId, string comment)
        {
            BLServiceSystem.Client.SaveCBReportToRepository(GZipCompressor.Compress(content), reportId, comment);
        }

        public bool GenerateXML(BOReportForm1 report, Person selectedPerson, string fileName)
        {
            switch (report.RType)
            {
                case BOReportForm1.ReportTypeEnum.One:
                    return GenerateReport1(report, selectedPerson, fileName, XML_SCHEME);
                case BOReportForm1.ReportTypeEnum.Two:
                    return GenerateReport2(report, selectedPerson, fileName, XML2_SCHEME);
                case BOReportForm1.ReportTypeEnum.Three:
                    return GenerateReport3(report, selectedPerson, fileName, XML3_SCHEME);
                default:
                    return false;
            }
        }

        private bool GenerateReport3(BOReportForm1 report, Person selectedPerson, string fileName, string schemeName)
        {
            try
            {
                var depList = BLServiceSystem.Client.GetDepositsListForCBReportForm3(report.Year, report.Quartal);

                var schema = Util.GetXmlSchemaSet(schemeName, typeof(DocumentBase));
                var doc = XML.CreateXmlDocumentWithRoot("ОКУД0418003", NS_URI, NS_PREFIX);

                #region Body
                doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                doc.DocumentElement.SetAttribute("appVersion", "2.16.3");
                var qNode = XML.CreateAddXmlNode(doc.FirstChild, "ОтчетныйКвартал", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Код", report.Quartal.ToString("000"), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Описание", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Активно", "true", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОтчетныйГод", report.Year.ToString(), NS_URI, NS_PREFIX);

                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ИНН1", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОГРН1", report.OGRN, NS_URI, NS_PREFIX);

                qNode = XML.CreateAddXmlNode(doc.FirstChild, "Отчетность", NS_URI, NS_PREFIX);
                qNode = XML.CreateAddXmlNode(qNode, "Таблица_СведенияОбАктивах", NS_URI, NS_PREFIX);
                var iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел3Заголовок", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр1", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр2", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр3", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр4", "4", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр5", "5", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр6", "6", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр7", "7", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр8", "8", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр9", "9", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр10", "10", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр11", "11", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Стр1Гр12", "12", NS_URI, NS_PREFIX);

                var count = 1;
                depList.ForEach(a =>
                {
                    var node = XML.CreateAddXmlNode(qNode, "Таб_СведенияОбАктивахСтроки_1", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол1_НомерСтроки", count++.ToString(), NS_URI, NS_PREFIX);
                    var mNode = XML.CreateAddXmlNode(node, "Кол2_ВидАктива", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Код", "4", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Описание", "Депозит", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Активно", "true", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол3_ИдентАктива", a.DepositContract, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол4_НаименЭмитента", a.BankFullName, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол5_ИдентБрокера", a.BankINN, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол6_КоличЦБ", a.SecurityCount.ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол7_РыночнСтоим", Convert.ToInt64(Math.Round(a.Sum)).ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол8_НакоплКупонДоход", Convert.ToInt64(Math.Round(a.CouponIncome)).ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол9_ТекущПроцентСтавка", a.Percent.ToOnesXmlString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол10_ДатаРазмещения", a.SettleDate.ToСBXmlString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(node, "Кол11_ДатаОкончания", a.ReturnDate.ToСBXmlString(), NS_URI, NS_PREFIX);
                    mNode = XML.CreateAddXmlNode(node, "Кол12_Периодичность", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Код", "4", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Описание", "в конце срока", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(mNode, "Активно", "true", NS_URI, NS_PREFIX);
                });

                //формирование сопр. письма
                var pLetter = XML.CreateAddXmlNode(doc.FirstChild, "СопроводительноеПисьмо", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОКАТО", report.OKATO, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ИНН", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОГРН", report.OGRN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ПолноеНаименование", report.FullName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "КраткоеНаименование", report.ShortName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ДатаРегистрации", report.RegDate.Value.ToСBXmlString(), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "НомерРегистрации", report.RegNum, NS_URI, NS_PREFIX);
                var pLetterSign = XML.CreateAddXmlNode(pLetter, "ПодписьЕдиноличногоИсполнительногоОргана", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ФамилияПодписанта", selectedPerson.LastName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ИмяПодписанта", selectedPerson.FirstName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ОтчествоПодписанта", selectedPerson.MiddleName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ДолжностьПодписанта", selectedPerson.Position, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ТелефонПодписанта", selectedPerson.Phone, NS_URI, NS_PREFIX);
                #endregion

                SaveReport(doc.OuterXml, fileName,schema, report.ID, $"Отчеты в Банк России - {report.QuartalText} {report.Year} ({fileName})");

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "При экспорте XML файла отчета для БО №3");
                return false;
            } 
        }

        private void SaveReport(string content, string filename, XmlSchemaSet schema, long id, string comment)
        {
            #region Save & Validate
            var bytes = Encoding.Default.GetBytes(content);

            if(!string.IsNullOrEmpty(filename))
                using (var writer = File.CreateText(filename))
                {
                    writer.Write(Encoding.Default.GetChars(bytes));
                }

            SaveToRepo(bytes, id, comment);

            var valResult = XML.ValidateXmlFile(bytes, schema);
            if (!string.IsNullOrEmpty(valResult))
            {
                var errorsList = valResult.Split('|').ToList();

                Logger.WriteLine("При валидации XML файла произошли следующие ошибки:");
                errorsList.ForEach(a => Logger.WriteLine(a));
            }
            #endregion
        }

        private bool GenerateReport2(BOReportForm1 report, Person selectedPerson, string fileName, string schemeName)
        {
            try
            {
                var transfersList = BLServiceSystem.Client.GetTransfersListForCBReportForm2(report.Year, report.Quartal);

                var schema = Util.GetXmlSchemaSet(schemeName, typeof(DocumentBase));
                var doc = XML.CreateXmlDocumentWithRoot("ОКУД0418002", NS_URI, NS_PREFIX);

                #region Body
                doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                doc.DocumentElement.SetAttribute("appVersion", "2.16.3");
                var qNode = XML.CreateAddXmlNode(doc.FirstChild, "ОтчетныйКвартал", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Код", report.Quartal.ToString("000"), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Описание", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Активно", "true", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОтчетныйГод", report.Year.ToString(), NS_URI, NS_PREFIX);

                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ИНН1", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОГРН1", report.OGRN, NS_URI, NS_PREFIX);

                qNode = XML.CreateAddXmlNode(doc.FirstChild, "Раздел1_ОперацииПриобретЦБ", NS_URI, NS_PREFIX);
                qNode = XML.CreateAddXmlNode(qNode, "Таблица_ОперацииПриобретЦБ", NS_URI, NS_PREFIX);
                var iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1Заголовок", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр1", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр2", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр3", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр4", "4", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр5", "5", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр6", "6", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр7", "7", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр8", "8", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр9", "9", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр10", "10", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1Стр1Гр11", "11", NS_URI, NS_PREFIX);
                //пропускаем строки с данными Таб_Раздел1Строки_1
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1Итог", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог1", "Итого:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог2", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог3", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог4", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог5", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог6", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог7", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог8", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог9", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог10", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтог11", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1ИтогПриобрет", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено", "Приобретено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретено9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПриобретеноСумма", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1ИтогПродано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано", "продано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПродано9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПроданоСумма", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1ИтогПереведено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено", "переведено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведено9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПереведеноСумма", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел1ИтогПогашено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено", "погашено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашено9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел1СтрИтогПогашеноСумма", "0", NS_URI, NS_PREFIX);

                qNode = XML.CreateAddXmlNode(doc.FirstChild, "Раздел2_ОперПередачаЦБ", NS_URI, NS_PREFIX);
                qNode = XML.CreateAddXmlNode(qNode, "Таблица_ОперацииПередачаЦБ", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел2Заголовок", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр1Заголовок", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр2Заголовок", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр3Заголовок", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр4Заголовок", "4", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр5Заголовок", "5", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр6Заголовок", "6", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр7Заголовок", "7", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр8Заголовок", "8", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр9Заголовок", "9", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр10Заголовок", "10", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр11Заголовок", "11", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2Гр12Заголовок", "12", NS_URI, NS_PREFIX);
                //пропускаем строки с данными Таб_Раздел2Строки_1
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел2Итог", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог1", "Итого:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог2", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог3", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог4", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог5", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог6", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог7", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог8", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог9", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог10", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог11", "0", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтог12", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел2ИтогПередано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано", "передано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПередано10", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПереданоСумма", "0", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел2ИтогПолучено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено", "получено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено1", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено8", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено9", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолучено10", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел2СтрИтогПолученоСумма", "0", NS_URI, NS_PREFIX);

                qNode = XML.CreateAddXmlNode(doc.FirstChild, "Раздел3_ОперацииПередачаДенСр", NS_URI, NS_PREFIX);
                qNode = XML.CreateAddXmlNode(qNode, "Таблица_ОперацииПередачаДенСр", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел3Заголовок", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_56", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_57", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_58", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_59", "4", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_60", "5", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_61", "6", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_62", "7", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Cell_63", "8", NS_URI, NS_PREFIX);

                var sumSend = 0m;
                var sumRcv = 0m;

                transfersList.ForEach(a =>
                {
                    iNode = XML.CreateAddXmlNode(qNode, "Таб_Раздел3Строки_1", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол1_Раздел3НомерСтроки", a.Count, NS_URI, NS_PREFIX);
                    var iNode2 = XML.CreateAddXmlNode(iNode, "Кол2_Раздел3ТипОперации", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode2, "Код", a.OpCode, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode2, "Описание", a.OpDescription, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode2, "Активно", a.OpActive, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол3_Раздел3ДатаОперации", a.DateValue.Value.ToСBXmlString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол4_Раздел3НаименованиеПолуч", a.RcvName, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол5_Раздел3ИНН", a.INN, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол6_Раздел3Назначение", a.Assignment, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол7_Раздел3НомерДатаДог", a.ContractNum, NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(iNode, "Кол8_Раздел3Сумма", a.Sum, NS_URI, NS_PREFIX);
                    if (a.OpCode == "1") sumSend += a.SumValue;
                    else sumRcv += a.SumValue;
                });


                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел3Итог", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог1", "Итого", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог2", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог3", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог4", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог5", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог6", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог7", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтог8", "", NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел3ИтогПередано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано1", "передано", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано2", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано3", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано4", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано5", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано6", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано7", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Раздел3ГрИтогоПередано8", ((long)sumSend).ToString(), NS_URI, NS_PREFIX);
                iNode = XML.CreateAddXmlNode(qNode, "Строка_Раздел3ИтогПолучено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол1_Раздел3ИтогПолучено", "получено", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол2_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол3_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол4_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол5_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол6_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол7_Раздел3ИтогПолучено", "x", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(iNode, "Кол8_Раздел3ИтогПолучено", ((long)sumRcv).ToString(), NS_URI, NS_PREFIX);

                //формирование сопр. письма
                var pLetter = XML.CreateAddXmlNode(doc.FirstChild, "СопроводительноеПисьмо", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОКАТО", report.OKATO, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ИНН", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОГРН", report.OGRN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ПолноеНаименование", report.FullName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "КраткоеНаименование", report.ShortName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ДатаРегистрации", report.RegDate.Value.ToСBXmlString(), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "НомерРегистрации", report.RegNum, NS_URI, NS_PREFIX);
                var pLetterSign = XML.CreateAddXmlNode(pLetter, "ПодписьЕдиноличногоИсполнительногоОргана", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ФамилияПодписанта", selectedPerson.LastName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ИмяПодписанта", selectedPerson.FirstName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ОтчествоПодписанта", selectedPerson.MiddleName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ДолжностьПодписанта", selectedPerson.Position, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ТелефонПодписанта", selectedPerson.Phone, NS_URI, NS_PREFIX);
                #endregion

                SaveReport(doc.OuterXml, fileName, schema, report.ID, $"Отчеты в Банк России - {report.QuartalText} {report.Year} ({fileName})");


                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "При экспорте XML файла отчета для БО №2");
                return false;
            } 
        }


        private bool GenerateReport1(BOReportForm1 report, Person selectedPerson, string fileName, string schemeName)
        {
            try
            {
                var schema = Util.GetXmlSchemaSet(schemeName, typeof(DocumentBase));
                var pf = report.PortfolioID.HasValue ? DataContainerFacade.GetByID<Portfolio>(report.PortfolioID) : null;

                #region Body
                var doc = XML.CreateXmlDocumentWithRoot("ОКУД0418001", NS_URI, NS_PREFIX);
                doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                doc.DocumentElement.SetAttribute("appVersion", "2.16.3");

                var qNode = XML.CreateAddXmlNode(doc.FirstChild, "ОтчетныйКвартал", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Код", report.Quartal.ToString("000"), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Описание", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(qNode, "Активно", "true", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОтчетныйГод", report.Year.ToString(), NS_URI, NS_PREFIX);

                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ИНН1", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(doc.FirstChild, "ОГРН1", report.OGRN, NS_URI, NS_PREFIX);

                //формирование данных по отчету
                var pCore = XML.CreateAddXmlNode(doc.FirstChild, "Отчетность", NS_URI, NS_PREFIX);
                var pCore2 = XML.CreateAddXmlNode(pCore, "Таблица_ОтчетПенсНак", NS_URI, NS_PREFIX);
                var pCoreEl = XML.CreateAddXmlNode(pCore2, "Строка_ПоказателиНомерГрафы", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "НомерГрафы1", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "НомерГрафы2", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "НомерГрафы3", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "НомерГрафы4", "4", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_26", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр1Гр1", "Средства пенсионных накоплений, поступившие в течение финансового года (стр.2+стр.3+стр.4)", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр1Гр2", "1", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_29", GetVal(report.Data.Total1), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_30", GetVal(report.Data.Quartal1), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_31", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч1", "в том числе:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч2", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч3", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч4", "", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_36", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр2Гр1", "страховые взносы на финансирование накопительной пенсии", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр2Гр2", "2", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_39", GetVal(report.Data.Total2), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_40", GetVal(report.Data.Quartal2), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_41", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр3Гр1", "дополнительные страховые взносы на накопительную пенсию", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_43", "3", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_44", GetVal(report.Data.Total3), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_45", GetVal(report.Data.Quartal3), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_46", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр4Гр1", "взносы работодателя в пользу застрахованного лица, уплаченные в соответствии с Федеральным законом &quot;О дополнительных страховых взносах на накопительную пенсию и государственной поддержке формирования пенсионных накоплений&quot;", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр4Гр2", "4", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_49", GetVal(report.Data.Total4), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_50", GetVal(report.Data.Quartal4), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_51", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр5Гр1", "Средства пенсионных накоплений, инвестированные в установленном порядке", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр5Гр2", "5", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_54", GetVal(report.Data.Total5), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_55", GetVal(report.Data.Quartal5), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_56", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр6Гр1", "Доходы от инвестирования средств пенсионных накоплений, поступивших в течение финансового года (стр.7+стр.10+стр.11)", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр6Гр2", "6", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_59", GetVal(report.Data.Total6), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_60", GetVal(report.Data.Quartal6), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_61", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч5", "в том числе:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч6", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч7", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч8", "", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_66", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр7Гр1", "доход от инвестирования средств пенсионных накоплений в государственные ценные бумаги Российской Федерации (стр.8+стр.9)", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр7Гр2", "7", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_69", GetVal(report.Data.Total7), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_70", GetVal(report.Data.Quartal7), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_71", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч9", "в том числе:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч10", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч11", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч12", "", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_76", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр8Гр1", "реализованный", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр8Гр2", "8", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_79", GetVal(report.Data.Total8), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_80", GetVal(report.Data.Quartal8), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_81", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр9Гр1", "нереализованный", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Ст9Гр2", "9", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_84", GetVal(report.Data.Total9), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_85", GetVal(report.Data.Quartal9), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_86", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр10Гр1", "проценты по депозитам в кредитных организациях", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр10Гр2", "10", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_89", GetVal(report.Data.Total10), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_90", GetVal(report.Data.Quartal10), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_91", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр11Гр1", "прочие доходы", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр11Гр2", "11", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_94", GetVal(report.Data.Total11), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_95", GetVal(report.Data.Quartal11), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_96", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр12Гр1", "Расходы, связанные с инвестированием средств пенсионных накоплений, поступивших в течение финансового года (стр.13+стр.14+стр.15+стр.16)", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр12Гр2", "12", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_99", GetVal(report.Data.Total12), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_100", GetVal(report.Data.Quartal12), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_101", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч13", "в том числе:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч14", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч15", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрВтч16", "", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_106", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр13Гр1", "оплата услуг брокера", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр13Гр2", "13", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_109", GetVal(report.Data.Total13), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_110", GetVal(report.Data.Quartal13), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_111", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр14Гр1", "оплата услуг биржи", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_113", "14", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_114", GetVal(report.Data.Total14), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_115", GetVal(report.Data.Quartal14), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_116", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр15Гр1", "оплата услуг депозитария", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр15Гр2", "15", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_119", GetVal(report.Data.Total15), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_120", GetVal(report.Data.Quartal15), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_121", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр16Гр1", "прочие расходы", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр16Гр2", "16", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_124", GetVal(report.Data.Total16), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_125", GetVal(report.Data.Quartal16), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_126", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр17Гр1", "Финансовый результат от инвестирования средств пенсионных накоплений (стр.6-стр.12)", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр17Гр2", "17", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_129", GetVal(report.Data.Total17), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_130", GetVal(report.Data.Quartal17), NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_136", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр18Гр1", "Чистый финансовый результат от временного размещения средств пенсионных накоплений", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр18Гр2", "18", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_139", GetVal(report.Data.Total18), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_140", "", NS_URI, NS_PREFIX);
                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_141", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрИзНего1", "из него:", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрИзНего2", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрИзНего3", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "СтрИзНего4", "", NS_URI, NS_PREFIX);

                pCoreEl = XML.CreateAddXmlNode(pCore2, "Row_172", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр19Гр1", "по инвестиционному портфелю "+ (pf == null ? "отчетного квартала отчетного года" : pf.Name), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Стр19Гр2", "19", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_175", "", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pCoreEl, "Cell_176", GetVal(report.Data.Quartal19), NS_URI, NS_PREFIX);

                int counter = 20;
                report.SumList.ForEach(sum =>
                {
                    pCoreEl = XML.CreateAddXmlNode(pCore2, "Таб_Справочно_1", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(pCoreEl, "Кол_СправочноНаименование", $@"по инвестиционному портфелю {sum.PortfolioName}", NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(pCoreEl, "Кол_СправочноКодСтроки", counter++.ToString(), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(pCoreEl, "Кол_СправочноВсего", GetVal(sum.Sum), NS_URI, NS_PREFIX);
                    XML.CreateAddXmlNodeWithValue(pCoreEl, "Кол_СправочноЗаОтчетКвартал", GetVal(sum.Sum), NS_URI, NS_PREFIX);

                });

                //формирование сопр. письма
                var pLetter = XML.CreateAddXmlNode(doc.FirstChild, "СопроводительноеПисьмо", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОКАТО", report.OKATO, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ИНН", report.INN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ОГРН", report.OGRN, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ПолноеНаименование", report.FullName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "КраткоеНаименование", report.ShortName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "ДатаРегистрации", report.RegDate.Value.ToСBXmlString(), NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetter, "НомерРегистрации", report.RegNum, NS_URI, NS_PREFIX);
                var pLetterSign = XML.CreateAddXmlNode(pLetter, "ПодписьЕдиноличногоИсполнительногоОргана", NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ФамилияПодписанта", selectedPerson.LastName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ИмяПодписанта", selectedPerson.FirstName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ОтчествоПодписанта", selectedPerson.MiddleName, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ДолжностьПодписанта", selectedPerson.Position, NS_URI, NS_PREFIX);
                XML.CreateAddXmlNodeWithValue(pLetterSign, "ТелефонПодписанта", selectedPerson.Phone, NS_URI, NS_PREFIX);
                #endregion

                SaveReport(doc.OuterXml, fileName, schema, report.ID, $"Отчеты в Банк России - {report.QuartalText} {report.Year} ({fileName})");

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteException(ex, "При экспорте XML файла отчета для БО №1");
                return false;
            } 
        }

        private string GetVal(decimal? value)
        {
            return value == null ? "0" : Convert.ToInt64(Math.Round(value.Value)).ToString();
        }
    }
}
