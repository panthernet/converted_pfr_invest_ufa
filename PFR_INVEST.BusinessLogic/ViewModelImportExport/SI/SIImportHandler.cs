﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.SIReport;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelImportExport.SI
{
    public class SIImportHandler: NPFImportHandlerBase
    {
        public override string OpenFileMask => "Данные по УК(.xls, .xlsx)|*.xls;*.xlsx";

	    public List<SIImportListItem> Items { get; set; } = new List<SIImportListItem>();

        private bool _canExecuteImport;

        private readonly bool _isFromUk;

        public SIImportHandler(SIRegister.Directions dir)
        {
            _isFromUk = dir == SIRegister.Directions.FromUK || dir == SIRegister.Directions.FromGUK;
        }

        public override bool CanExecuteImport()
        {
            return _canExecuteImport;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool ExecuteImport(out string message)
        {
            _canExecuteImport = false;
            message = null;
            return true;
        }


        public override bool IsValidateDoc(Worksheet worksheet, out string message)
        {
            message = null;
            _canExecuteImport = false;
            //импорт данных в класс
            if (!SetDataImport(worksheet, out message))
                return false;

            _canExecuteImport = true;
            return true;
        }


        private bool SetDataImport(Worksheet w, out string message)
        {
            message = null;
            var ukList = BLServiceSystem.Client.GetUKListHib();
            var cList = BLServiceSystem.Client.GetContractsForUKs();

            var list = new List<SIImportListItem>();
            int counter = 2;
            bool cond = true;
            while (cond)
            {
                cond = w.Range["A" + counter].Value2 != null;
                if (cond)
                {
                    var item = new SIImportListItem
                    {
                        UK = (string) w.Range["A" + counter].Value2,
                        RegNum = (string) w.Range["B" + counter].Value2
                    };

                    if (!_isFromUk)
                    {
                        try
                        {
                            item.SumToUK = ConvToDecimal(w.Range["C" + counter].Value2);
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.Error("SI import", ex);
                            message = $"Не удалось прочитать значение \"Сумма в УК\" <{w.Range["C" + counter].Value2}> для УК <{item.UK}> с договором <{item.RegNum}>! Импорт будет прерван!";
                            return false;
                        }
                    }
                    else
                    {
                        //from UK
                        try
                        {
                            item.SumFromUKTotal = ConvToDecimal(w.Range["C" + counter].Value2);
                        }
                        catch(Exception ex)
                        {
                            Logger.Instance.Error("SI import", ex);
                            message = $"Не удалось прочитать значение \"Сумма из УК\" <{w.Range["C" + counter].Value2}> для УК <{item.UK}> с договором <{item.RegNum}>! Импорт будет прерван!";
                            return false;
                        }

                        try
                        {
                            item.SpnFromUK = ConvToDecimal(w.Range["D" + counter].Value2);
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.Error("SI import", ex);
                            message = $"Не удалось прочитать значение \"СПН из УК\" <{w.Range["D" + counter].Value2}> для УК <{item.UK}> с договором <{item.RegNum}>! Импорт будет прерван!";
                            return false;
                        }

                        try
                        {
                            item.InvestIncome = ConvToDecimal(w.Range["E" + counter].Value2);
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.Error("SI import", ex);
                            message = $"Не удалось прочитать значение \"Инвестдоход\" <{w.Range["E" + counter].Value2}> для УК <{item.UK}> с договором <{item.RegNum}>! Импорт будет прерван!";
                            return false;
                        }
                    }

                    try
                    {
                        item.ZLCount = Convert.ToInt32(w.Range[(_isFromUk ? "F" : "D") + counter].Value2);
                    }
                    catch (Exception ex)
                    {
                            Logger.Instance.Error("SI import", ex);
                        message = $"Не удалось прочитать значение \"Кол-во ЗЛ\" <{w.Range[(_isFromUk ? "F" : "D") + counter].Value2}> для УК <{item.UK}> с договором <{item.RegNum}>! Импорт будет прерван!";
                        return false;
                    }

                    var name = item.UK.Trim().ToLower();
                    var cName = item.RegNum.Trim().ToLower();
                    if (ukList.FirstOrDefault(a => a.FormalizedName.ToLower() == name) == null || cList.FirstOrDefault(a => a.ContractNumber.ToLower() == cName) == null)
                    {
                        item.IsMismatched = true;
                        message =$"Импортируемый файл содержит не найденные в системе пары \"УК\" и \"Дата договора\".\nТакие строки в таблице импорта будут выделены красным цветом и импортированы в реестр не будут!";
                    }
                    list.Add(item);
                }
                counter++;
            }
            Items = list;
            return true;
        }

        private decimal? ConvToDecimal(object obj)
        {
            if(obj == null) return null;
            return Convert.ToDecimal(obj);
        }
    }
}
