﻿
namespace PFR_INVEST.BusinessLogic.SIReport
{
    using Microsoft.Office.Interop.Excel;

    public class NPFImportHandlerEmpty : NPFImportHandlerBase
    {
        public override string OpenFileMask => string.Empty;

        public override bool CanExecuteImport()
        {
            return false;
        }



        public override bool ExecuteImport(out string message)
        {
            message = null;
            return false;
        }

        public override bool IsValidateDoc(Worksheet worksheet, out string message)
        {
            throw new System.NotImplementedException();
        }

        public override void RefreshViewModels()
        {
            throw new System.NotImplementedException();
        }
    }
}
