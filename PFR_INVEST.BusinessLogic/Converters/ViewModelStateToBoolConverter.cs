﻿using System;
using System.Windows.Data;

namespace PFR_INVEST.BusinessLogic.Converters
{
    /// <summary>
    /// Returns true if state is readonly and false if not
    /// </summary>
    public class ViewModelStateToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool result = true;
            result = !(value is ViewModelState
                     && ((ViewModelState)value == ViewModelState.Create
                        || (ViewModelState)value == ViewModelState.Edit));

            return result;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ViewModelState result;
            if (value is bool && (bool)value)
                result = ViewModelState.Edit;
            else
                result = ViewModelState.Read;

            return result;
        }
    }
}
