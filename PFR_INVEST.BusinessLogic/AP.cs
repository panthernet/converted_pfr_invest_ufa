﻿using System;
using PFR_INVEST.Auth.ClientData;

namespace PFR_INVEST.BusinessLogic
{
    /// <summary>
    /// Провайдер методов работы с авторизацией и проверкой доступа
    /// </summary>
    public static class AP
    {
#if !WEBCLIENT
        public static AuthProvider Provider;
#else
        public static AuthProvider Provider => GetUserAP();

        public static Func<AuthProvider> GetUserAP;
#endif
    }
}
