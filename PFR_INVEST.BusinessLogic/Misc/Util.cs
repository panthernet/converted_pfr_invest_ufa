﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Proxy;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Schema;

namespace PFR_INVEST.BusinessLogic
{
    internal static class Util
    {
        public static XmlReader GetShema(string shemaName, Type type)
        {
            var thisAssembly = Assembly.GetAssembly(type);
            string path = "PFR_INVEST.XmlSchemas.Schemas";
            return new XmlTextReader(thisAssembly.GetManifestResourceStream(string.Format("{0}.{1}", path, shemaName)));
        }

        public static XmlSchemaSet GetXmlSchemaSet(string shemaName, Type type, bool compiled = true)
        {
            using (var reader = GetShema(shemaName, type))
            {
                var xmlSchema = XmlSchema.Read(reader, null);
                var xmlSchemaSet = new XmlSchemaSet();
                xmlSchemaSet.Add(xmlSchema);
                if(compiled)
                    xmlSchemaSet.Compile();
                return xmlSchemaSet;
            }
        }

        public static string format(decimal money)
        {
            if (money >= 0)
                return money.ToString("### ### ### ### ##0.00");
            var defFormat = money.ToString("### ### ### ### ##0.00");
            var ret = defFormat;

            if (defFormat[0] == '-')
                ret = "-" + defFormat.Substring(1).Trim();

            return ret;
        }

        public static string format(object money)
        {
            if (Convert.ToString(money) == "")
                money = 0;

            return format(Convert.ToDecimal(money));
        }

        public static bool isFieldExists(Dictionary<string, DBField> fields, string key)
        {
            return fields != null && fields.ContainsKey(key);// && fields[key].Value != null;
        }

        public static bool isFieldExists(DBEntity entity, string key)
        {
            return entity != null && isFieldExists(entity.Fields, key);
        }

        public static void setDBEntityField(ref Dictionary<string, DBField> dstFields, string dstKey, Dictionary<string, DBField> srcFields, string srcKey)
        {
            if (srcFields != null && dstFields != null && isFieldExists(srcFields, srcKey) && dstFields.Keys.Contains(dstKey))
            {
                dstFields[dstKey].Value = srcFields[srcKey].Value;
            }
        }

        public static string GetXmlByEntityList<T>(List<T> eList)
        {
            if (eList == null || eList.Count == 0) return string.Empty;
            var s = new XmlSerializer(eList.GetType());
            
            var sw = new StringWriter();
            s.Serialize(sw, eList);

            var r = sw.ToString();
            // удаление объявления xml
            return r.Substring(r.IndexOf("<ArrayOf", StringComparison.Ordinal));
        }

        public static List<T> GetEntityListByXml<T>(string xml)
        {
            if (string.IsNullOrEmpty(xml.Trim()))
                return new List<T>();

            var s = new XmlSerializer(typeof(List<T>));
            var tr = new StringReader(xml);
            var res = (List<T>)s.Deserialize(tr);

            return res;
        }

    }
}
