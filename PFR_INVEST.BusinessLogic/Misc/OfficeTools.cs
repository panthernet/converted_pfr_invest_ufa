﻿namespace PFR_INVEST.BusinessLogic
{
    public static class OfficeTools
    {
        public static void FindAndReplace(Microsoft.Office.Interop.Word._Application app, object findText, object replaceWithText)
        {
            var sX = replaceWithText as string;
            //Slice text to portions per 255 chars
            if (sX != null && sX.Length > 255)
            {
                var index = 0;
                var pieceLength = 255 - findText.ToString().Length;
                var sxl = sX.Length;

                do
                {
                    string s;
                    if (index + pieceLength < sxl)
                        s = sX.Substring(index, pieceLength) + (string)findText;
                    else
                        s = sX.Substring(index);

                    index += pieceLength;
                    FindAndReplaceGeneral(app, findText, s);

                } while (index < sxl);
            }
            else
                FindAndReplaceGeneral(app, findText, replaceWithText);
        }

        /// <summary>
        /// Simple version of FindAndReplace. doesn't works with string>255 chars long.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="findText"></param>
        /// <param name="replaceWithText"></param>
        private static void FindAndReplaceGeneral(Microsoft.Office.Interop.Word._Application wordApp, object findText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object replace = 2;
            object wrap = 1;

            //http://jira.dob.datateh.ru/browse/DOKIPIV-541
            object rt = replaceWithText == null ? null : replaceWithText.ToString().Replace("^", "");

            if (wordApp != null)
                wordApp.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord, ref matchWildCards,
                                               ref matchSoundsLike, ref matchAllWordForms, ref forward,
                                               ref wrap, ref format, ref rt, ref replace, ref matchKashida,
                                               ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }

        public static void FindAndReplace(Microsoft.Office.Interop.Excel._Application app, object findText, object replaceWithText)
        {
            var sX = replaceWithText as string;
            if (sX != null && sX.Length > 255)
            {
                //Slice text to portions per 255 chars
                var index = 0;
                var pieceLength = 255 - findText.ToString().Length;
                var sxl = sX.Length;

                do
                {
                    string s;
                    if (index + pieceLength < sxl)
                        s = sX.Substring(index, pieceLength) + (string)findText;
                    else
                        s = sX.Substring(index);

                    index += pieceLength;
                    FindAndReplaceGeneral(app, findText, s);

                } while (index < sxl);
            }
            else
                FindAndReplaceGeneral(app, findText, replaceWithText);
        }

        /// <summary>
        /// Simple version of FindAndReplace. doesn't works with string>255 chars long.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="findText"></param>
        /// <param name="replaceWithText"></param>
        private static void FindAndReplaceGeneral(Microsoft.Office.Interop.Excel._Application app, object findText, object replaceWithText)
        {
            object lookAt = Microsoft.Office.Interop.Excel.XlLookAt.xlPart;
            object searchOrder = Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows;
            object matchCase = false;
            object matchByte = false;
            object searchFormat = false;
            object replaceFormat = false;

            //http://jira.dob.datateh.ru/browse/DOKIPIV-541
            var rt = replaceWithText == null ? null : replaceWithText.ToString().Replace("^", "");


            app.Cells.Replace(findText, rt, lookAt, searchOrder,
                matchCase, matchByte, searchFormat, replaceFormat);
        }
    }
}
