﻿using db2connector;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic
{
    public class BLServiceSystem
    {
        public static string EndPointConfigName = "WSHttpBinding_IDB2Connector";
        public static string HttpsEndPointConfigName = "WSHttpsBinding_IDB2Connector";
        public static BLDataServiceClient CreateNewDataClient(ClientAuthType at)
        {
            string name;
            switch (at)
            {
                case ClientAuthType.ECASAHTTPS:
                    name = HttpsEndPointConfigName;
                    break;
                default:
                    name = EndPointConfigName;
                    break;
            }
            var client = new BLDataServiceClient(name);
            return client;
        }

        public static GetDataService GetDataServiceDelegate { get; set; }

        internal static IDB2Connector Client => GetDataServiceDelegate();

        public delegate IDB2Connector GetDataService();
    }
}
