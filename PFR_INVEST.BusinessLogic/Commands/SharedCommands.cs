﻿using System.Windows.Input;

namespace PFR_INVEST.BusinessLogic.Commands
{
    public static class SharedCommands
    {
        /// <summary>
        /// Команда заглушка, ничего не делает
        /// </summary>
        public static RoutedUICommand None = new RoutedUICommand();

        /// <summary>
        /// Комманда для показа XML тела документа из ODK_XML_BODY
        /// </summary>
        public static RoutedUICommand OpenEdoXmlBody = new RoutedUICommand();

		/// <summary>
		/// Команда показа отчета импорта (сжатый XML из REPOSITORY_IMPEXP_FILE.REPOSITORY)
		/// </summary>
		public static RoutedUICommand OpenDepClaimXml = new RoutedUICommand();

        /// <summary>
        /// Команда открытия оригинального файла отказного заявления (сжатый XML из REPOSITORY_IMPEXP_FILE.REPOSITORY)
        /// </summary>
        public static RoutedUICommand OpenRejAppFile = new RoutedUICommand();

        public static RoutedUICommand OpenOnesImportXmlBody = new RoutedUICommand();
        public static RoutedUICommand OpenOnesExportXmlBody = new RoutedUICommand();
        public static RoutedUICommand RemoveOnesExportEntry = new RoutedUICommand();


		/// <summary>
		/// Команда показа xml из хранилища (сжатый XML из REPOSITORY_IMPEXP_FILE.REPOSITORY)
		/// </summary>
		public static RoutedUICommand OpenKIPXml = new RoutedUICommand();

		/// <summary>
		/// Команда показа сообщения (LoginMessageItem)
		/// </summary>
		public static RoutedUICommand OpenMessage = new RoutedUICommand();


		/// <summary>
		/// Команда открытия гридя реестров СИ и фокусировки реестра по ИД
		/// </summary>
		public static RoutedUICommand OpenSIRegisterGrid = new RoutedUICommand();

		/// <summary>
		/// Команда открытия гридя реестров ВР и фокусировки реестра по ИД
		/// </summary>
		public static RoutedUICommand OpenVRRegisterGrid = new RoutedUICommand();

		/// <summary>
		/// Команда открытия гридя реестров НПФ и фокусировки реестра по ИД
		/// </summary>
		public static RoutedUICommand OpenNPFRegisterGrid = new RoutedUICommand();

        /// <summary>
        /// Команда для открытия XML файла экспорта отчета ЦБ
        /// </summary>
        public static RoutedUICommand OpenCBReportExportXmlBody = new RoutedUICommand();

        /// <summary>
        /// Команда открытия окна для согласования отчетов в ЦБ ответственными лицами
        /// </summary>
        public static RoutedUICommand OpenCBReportApprovementForm = new RoutedUICommand();
    }
}
