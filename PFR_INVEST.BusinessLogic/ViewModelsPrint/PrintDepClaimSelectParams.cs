﻿using System.Globalization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class PrintDepClaimSelectParams : PrintToDoc
	{
		private readonly DepClaimSelectParams _depClaimSelectParams;

		private PrintDepClaimSelectParams()
		{
			Name = "Уведомление об условиях проведения аукциона";
			TemplateName = string.Concat(Name, ".doc");
		}

		public PrintDepClaimSelectParams(long auctionId, bool isVisible = true)
			: this()
		{
		    IsVisibleAfterGeneration = isVisible;
			_depClaimSelectParams = DataContainerFacade.GetByID<DepClaimSelectParams>(auctionId);
		}

		public PrintDepClaimSelectParams(DepClaimSelectParams depClaimSelectParams)
			: this()
		{
			_depClaimSelectParams = depClaimSelectParams;
		}

		protected override void FillData()
		{
			Fields.Add("SELECT_DATE", _depClaimSelectParams.SelectDate.ToString("dd.MM.yyyy"));
			Fields.Add("MAXINSURANCEFEE", _depClaimSelectParams.MaxInsuranceFee.ToString("n0", CultureInfo.CreateSpecificCulture("ru-RU")));
			Fields.Add("PERIOD", _depClaimSelectParams.Period.ToString());
			Fields.Add("PLACEMENT_DATE", _depClaimSelectParams.PlacementDate.ToString("dd.MM.yyyy"));
			Fields.Add("RETURN_DATE", _depClaimSelectParams.ReturnDate.ToString("dd.MM.yyyy"));
			Fields.Add("MINRATE", _depClaimSelectParams.MinRate.ToString("n2", CultureInfo.CreateSpecificCulture("ru-RU")));
			Fields.Add("MINDEPCLAIMVOLUME", _depClaimSelectParams.MinDepClaimVolume.ToString("n0", CultureInfo.CreateSpecificCulture("ru-RU")));
			Fields.Add("MAXDEPCLAIMAMOUNT", _depClaimSelectParams.MaxDepClaimAmount.ToString());
			Fields.Add("DEPCLAIMSELECTLOCATION", _depClaimSelectParams.DepClaimSelectLocation);
			Fields.Add("DEPCLAIMACCEPT_START", _depClaimSelectParams.DepClaimAcceptStart.ToString(@"hh\:mm"));
			Fields.Add("DEPCLAIMACCEPT_END", _depClaimSelectParams.DepClaimAcceptEnd.ToString(@"hh\:mm"));
			Fields.Add("DEPCLAIMLIST_START", _depClaimSelectParams.DepClaimListStart.ToString(@"hh\:mm"));
			Fields.Add("DEPCLAIMLIST_END", _depClaimSelectParams.DepClaimListEnd.ToString(@"hh\:mm"));
			Fields.Add("FILTER_START", _depClaimSelectParams.FilterStart.ToString(@"hh\:mm"));
			Fields.Add("FILTER_END", _depClaimSelectParams.FilterEnd.ToString(@"hh\:mm"));
			Fields.Add("OFFERSEND_START", _depClaimSelectParams.OfferSendStart.ToString(@"hh\:mm"));
			Fields.Add("OFFERSEND_END", _depClaimSelectParams.OfferSendEnd.ToString(@"hh\:mm"));
			Fields.Add("OFFERRECEIVE_START", _depClaimSelectParams.OfferReceiveStart.ToString(@"hh\:mm"));
			Fields.Add("OFFERRECEIVE_END", _depClaimSelectParams.OfferReceiveEnd.ToString(@"hh\:mm"));
		}
	}
}
