﻿using System;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.BusinessLogic.ViewModelsPrint
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class PrintCbReportForm: PrintToExcel
    {
        private readonly CBReportApprovementStatusDlgViewModel.CbReportData3 _data3;
        private readonly CBReportApprovementStatusDlgViewModel.CbReportData2 _data2;
        private readonly int _form;

        public PrintCbReportForm(int form)
        {
            _form = form;
            Name = $"Отчет в ЦБ форма {form} от {DateTime.Today:dd.MM.yyyy}";
            TemplateName = $"Отчет в ЦБ форма {form}.xlsx";
        }

        public PrintCbReportForm(CBReportApprovementStatusDlgViewModel.CbReportData2 data2)
            :this(2)
        {
            _data2 = data2;
        }


        public PrintCbReportForm(CBReportApprovementStatusDlgViewModel.CbReportData3 data3)
            : this(3)
        {
            _data3 = data3;
        }

        protected override void FillData()
        {
            var ci = CultureInfo.GetCultureInfo("ru-RU");
            if (_form == 3)
            {
                Fields.Add("DATE", _data3.ReportDate.ToString("dd.MM.yyyy"));
                Fields.Add("INN", _data3.INN);
                Fields.Add("OGRN", _data3.OGRN);
                Fields.Add("FIO", _data3.FIO);
                Fields.Add("POST", _data3.Post);
            }
            if (_form == 2)
            {
                Fields.Add("DATE", _data2.ReportDate.ToString("dd.MM.yyyy"));
                Fields.Add("INN", _data2.INN);
                Fields.Add("OGRN", _data2.OGRN);
                Fields.Add("FIO", _data2.FIO);
                Fields.Add("POST", _data2.Post);
                Fields.Add("TOTAL_SEND", _data2.List.Where(a=> a.TypeCode == 1).Sum(a=> a.Summ).ToString("n0", ci));
                Fields.Add("TOTAL_RCV", _data2.List.Where(a=> a.TypeCode == 2).Sum(a=> a.Summ).ToString("n0", ci));
            }
        }

        protected override void GenerateDocument(Application app)
        {
            base.GenerateDocument(app);
            var ci = CultureInfo.GetCultureInfo("ru-RU");
            var ws = (Worksheet) app.Worksheets[1];

            try
            {
                if (_form == 3)
                {
                    var index = 10;
                    var obj = ws.Range["A" + index].EntireRow.Copy();
                    _data3.List.ForEach(a =>
                    {
                        ws.Range["A" + index].EntireRow.Insert(obj, true);
                        ws.Range["A" + index].Cells.Value2 = a.Number;
                        ws.Range["B" + index].Cells.Value2 = a.TypeCodeName;
                        ws.Range["C" + index].Cells.Value2 = a.Identifier;
                        ws.Range["D" + index].Cells.Value2 = a.EmName;
                        ws.Range["E" + index].Cells.Value2 = a.BrokerId;
                        ws.Range["F" + index].Cells.Value2 = a.CbCount.ToString("n0");
                        ws.Range["G" + index].Cells.Value2 = a.MarketPrice.ToString("n0");
                        ws.Range["H" + index].Cells.Value2 = a.CouponIncome.ToString("n0");
                        ws.Range["I" + index].Cells.Value2 = a.Rate.ToString("n2", ci);
                        ws.Range["J" + index].Cells.Value2 = a.PlacementDate.ToString("dd.MM.yyyy");
                        ws.Range["K" + index].Cells.Value2 = a.EndDate.ToString("dd.MM.yyyy");
                        ws.Range["L" + index].Cells.Value2 = a.PeriodCodeName;
                        index++;
                    });

                    ws.Range["A" + index].EntireRow.Delete();
                    return;
                }
                if (_form == 2)
                {
                    var index = 10;
                    var obj = ws.Range["A" + index].EntireRow.Copy();
                    _data2.List.ForEach(a =>
                    {
                        ws.Range["A" + index].EntireRow.Insert(obj, true);
                        ws.Range["A" + index].Cells.Value2 = a.Number;
                        ws.Range["B" + index].Cells.Value2 = a.TypeCode == 1 ? "передача" : "получение";
                        ws.Range["C" + index].Cells.Value2 = a.OperationDate?.ToString("dd.MM.yyyy") ?? "-";
                        ws.Range["D" + index].Cells.Value2 = a.RcvName;
                        ws.Range["E" + index].Cells.Value2 = a.INN;
                        ws.Range["F" + index].Cells.Value2 = a.Assignmnent;
                        ws.Range["G" + index].Cells.Value2 = a.ContractNumDate;
                        ws.Range["H" + index].Cells.Value2 = a.Summ.ToString("n0");
                        index++;
                    });

                    ws.Range["A" + index].EntireRow.Delete();
                    return;
                }
            }
            finally
            {
                Marshal.FinalReleaseComObject(ws);
            }
        }
    }    
}
