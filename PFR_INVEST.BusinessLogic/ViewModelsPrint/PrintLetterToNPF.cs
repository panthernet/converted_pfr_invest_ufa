﻿using System.IO;
using System.Windows;
using Application=Microsoft.Office.Interop.Word.Application;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Constants.Identifiers;
using System;
using System.Runtime.InteropServices;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Exceptions;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class PrintLetterToNPF : ViewModelBase
    {
        private const string LET_TO_NPF_TEMPLATE_NAME = "Письмо в НПФ о перечислении СПН.doc";
        private readonly object _missing = System.Reflection.Missing.Value;

        public PrintLetterToNPF(long frID)
        {
            Application wApp = null;
            Microsoft.Office.Interop.Word.Document wDoc = null;
            try
            {
                wApp = new Application();

                object isVisible = true;

                Finregister fr = null;

                try
                {
                    fr = DataContainerFacade.GetByID<Finregister, long>(frID);
                }
                catch
                {
                    OfficeTools.FindAndReplace(wApp, "{FullName}", "");
                    OfficeTools.FindAndReplace(wApp, "{PostAddress}", "");
                    OfficeTools.FindAndReplace(wApp, "{RegDate}", "");
                    OfficeTools.FindAndReplace(wApp, "{RegNum}", "");
                }

                object filePath = TemplatesManager.ExtractTemplate(LET_TO_NPF_TEMPLATE_NAME);

                if (File.Exists((string) filePath) && fr != null)
                {
                    wDoc = wApp.Documents.Add(ref filePath, ref _missing, ref _missing, ref isVisible);

                    wDoc.Activate();

                    try
                    {
                        OfficeTools.FindAndReplace(wApp, "{RegDate}", fr.Date != null ? fr.Date.Value.ToShortDateString() : "");
                        OfficeTools.FindAndReplace(wApp, "{RegNum}", fr.RegNum != null ? fr.RegNum.Trim() : "");
                        var le =
                            BLServiceSystem.Client.GetLegalEntityForContragent(RegisterIdentifier.IsToNPF(fr.GetRegister().Kind.ToLower())
                                ? fr.GetDebitAccount().GetContragent().ID
                                : fr.GetCreditAccount().GetContragent().ID);

                        if (le != null)
                        {
                            OfficeTools.FindAndReplace(wApp, "{FullName}", le.FullName);
                            OfficeTools.FindAndReplace(wApp, "{PostAddress}", le.PostAddress != null ? le.PostAddress.Trim() : "");
                        }
                    }
                    catch
                    {
                        OfficeTools.FindAndReplace(wApp, "{RegDate}", "");
                        OfficeTools.FindAndReplace(wApp, "{RegNum}", "");
                        OfficeTools.FindAndReplace(wApp, "{FullName}", "");
                    }

                    wApp.Visible = true;

                }
                else
                {
                    MessageBox.Show("Файл шаблона для письма в НПФ не существует!");
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
            finally
            {
                if (wDoc != null)
                    Marshal.FinalReleaseComObject(wDoc);
                if (wApp != null)
                    Marshal.FinalReleaseComObject(wApp);
            }
        }
    }
}
