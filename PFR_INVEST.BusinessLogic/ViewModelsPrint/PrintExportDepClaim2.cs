﻿using System.Runtime.InteropServices;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsPrint
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using Microsoft.Office.Interop.Excel;
    using DataObjects;
    using Application = Microsoft.Office.Interop.Excel.Application;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintExportDepClaim2 : ViewModelBase
    {
        private const string TEMPLATE_NAME = "Список заявок.xlsx";
        private Application _excelApp;
        private Worksheet _worksheet;

        private const string FORMAT = "dd.MM.yyyy";

        private DateTime Date { get; }
        private IList<DepClaim2> DepClaims { get; }
        public DepClaimSelectParams Auction { get; }
        private readonly string _filePath;

        public PrintExportDepClaim2(DepClaimSelectParams auction, DateTime date, string onlySaveTo = null)
        {
            Date = date;
            Auction = auction;
            DepClaims = auction.GetDepClaim2List();
            _filePath = onlySaveTo;
        }

        private void GenerateDoc()
        {
            if (!DepClaims.Any())
                return;
            _worksheet.Range["F4"].Cells.Value2 = Auction.SelectDate.ToString(FORMAT);
            _worksheet.Range["F5"].Cells.Value2 = Date.ToString(FORMAT);
            
            var securityId = Auction != null ? Auction.SecurityId : string.Empty;
            _worksheet.Range["C8"].Cells.Value2 = securityId;

            var totalAmount = DepClaims.Sum(dc => dc.OldAmount);
			var totalPayment = DepClaims.Sum(dc => dc.OldPayment);
            var totalReturn = totalAmount + totalPayment;

            _worksheet.Range["A15"].Cells.Value2 = !string.IsNullOrWhiteSpace(securityId) ? ($"Итого по ({securityId})") : "Итого";
            _worksheet.Range["E15"].Cells.Value2 = totalAmount.ToString("0.00");
            _worksheet.Range["F15"].Cells.Value2 = totalPayment.ToString("0.00");
            _worksheet.Range["G15"].Cells.Value2 = totalReturn.ToString("0.00");

            var start = 13;
            var index = 1;
            object obj = _worksheet.Range["A" + start].EntireRow.Copy();
            foreach (var dc in DepClaims)
            {
                _worksheet.Range["A" + start].EntireRow.Insert(obj);
                _worksheet.Range["A" + start].Cells.Value2 = index;
                _worksheet.Range["B" + start].Cells.Value2 = dc.FirmName;
                _worksheet.Range["C" + start].Cells.Value2 = dc.FirmID;
                _worksheet.Range["D" + start].Cells.Value2 = dc.OldRate.ToString("#.00");
				_worksheet.Range["E" + start].Cells.Value2 = dc.OldAmount.ToString("0.00");
				_worksheet.Range["F" + start].Cells.Value2 = dc.OldPayment.ToString("0.00");
                _worksheet.Range["H" + start].Cells.Value2 = dc.SettleDate.ToString(FORMAT);
                _worksheet.Range["I" + start].Cells.Value2 = dc.ReturnDate.ToString(FORMAT);

                if (dc.Term == 0 || dc.Part2Sum == 0)
                {
                    _worksheet.Range["G" + start].Cells.Value2 = (dc.Amount + dc.Payment).ToString("0.00");
                    _worksheet.Range["J" + start].Cells.Value2 = (dc.ReturnDate - dc.SettleDate).Days;
                }
                else
                {
					_worksheet.Range["G" + start].Cells.Value2 = dc.OldPart2Sum.ToString("0.00");
                    _worksheet.Range["J" + start].Cells.Value2 = dc.Term;
                }
                index++;
                start++;
            }
            _worksheet.Range["A" + start].EntireRow.Delete();
            _worksheet.Range["A1"].Select();
        }

        public bool Print()
        {
            object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

            if (File.Exists(filePath.ToString()))
            {
                RaiseStartedPrinting();
                var oldCi = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Workbook workbook = null;
                try
                {
                    _excelApp = new Application { Visible = false, DisplayAlerts = false };
                    _worksheet = (Worksheet)_excelApp.Workbooks.Open(filePath.ToString()).Sheets[1];
                    workbook = _excelApp.Workbooks[1];                    
                    GenerateDoc();
                    if (_filePath == null)
                        _excelApp.Visible = true;
                    else
                    {
                        _worksheet.SaveAs(_filePath);
                        workbook?.Close(false);
                        _excelApp.Quit();
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCi;

                    if (_worksheet != null)
                        Marshal.FinalReleaseComObject(_worksheet);
                    if (workbook != null)
                        Marshal.FinalReleaseComObject(workbook);
                    if (_excelApp != null)
                        Marshal.FinalReleaseComObject(_excelApp);

                    RaiseFinishedPrinting();
                }
                return true;
            }
            RaiseErrorLoadingTemplate(TEMPLATE_NAME);
            return false;
        }
    }
}
