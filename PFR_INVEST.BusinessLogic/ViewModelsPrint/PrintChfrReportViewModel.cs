﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using Document = Microsoft.Office.Interop.Word.Document;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.XMLModel;

namespace PFR_INVEST.BusinessLogic.ViewModelsPrint
{
    public static class ext
    {
        public static bool Like(this string toSearch, string toFind)
        {
            return new Regex(@"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\").Replace(toFind, ch => @"\" + ch).Replace('_', '.').Replace("%", ".*") + @"\z", RegexOptions.Singleline).IsMatch(toSearch);
        }
    }

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class PrintChfrReportViewModel : PrintToDoc
    {
        private readonly CultureInfo _culture = CultureInfo.CreateSpecificCulture("ru-RU");

        public string Folder { get; set; }
        private readonly bool _isDop;
        private Person _person;
        private Person _person2;

        public PrintChfrReportViewModel(SelectChfrReportYearQuartalDlgViewModel.PortfolioItem p, decimal ssum1, decimal ssum2, Person person, Person person2)
        {
            _ssum1 = ssum1;
            _ssum2 = ssum2;
            _portfolio = p;
            _selectedYear = (int)(p.YearID ?? 0) + 2000;
            _selectedQuarter = p.Quartal;
            IsVisibleAfterGeneration = true;
            Folder = Path.GetTempPath();
            _isDop = p.IsDop;
            _person = person;
            _person2 = person2;
            TemplateName = _isDop ? "ЧФР_доп.doc" : "ЧФР.doc";
        }
        private readonly Portfolio _portfolio;
        private readonly int _selectedYear;
        private readonly int _selectedQuarter;
        private readonly decimal _ssum1;
        private readonly decimal _ssum2;

        protected override void FillData()
        {
            //var percB = BLServiceSystem.Client.GetDepPercForBOReport(_selectedYear, _selectedQuarter, true, _portfolio.ID);
            //var depSumA = BLServiceSystem.Client.GetReturnedDepSumForPortfolio(_portfolio.ID, _selectedYear, _selectedQuarter);
            var x = BLServiceSystem.Client.GetTransfersForChfr(_portfolio.ID, _selectedYear, _selectedQuarter, 2);
            var depSumA = x.Sum(a=> a.FinalSumm);
            var percB = BLServiceSystem.Client.GetTransfersForChfr(_portfolio.ID, _selectedYear, _selectedQuarter, 1).Sum(a=> a.FinalSumm);
            var trList = BLServiceSystem.Client.GetTransfersForChfr(_portfolio.ID, _selectedYear, _selectedQuarter, 3);

            var totalIncome = percB + depSumA + trList.Sum(a=> a.FinalSumm);
            var chfr = totalIncome - depSumA;
            var calc = _ssum2 == 0 ? 0 : (chfr / _ssum2);

            Fields.Add("INCOME", totalIncome == 0 ? "-" : totalIncome.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            Fields.Add("RETURN", depSumA == 0 ? "-" : depSumA.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            Fields.Add("TOTAL", chfr == 0 ? "-" : chfr.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            Fields.Add("CALC", calc == 0 ? "-" : calc.TruncateDecimalPlaces(12).ToString("# ### ### ### ### ### ### ##0.000000000000", _culture).Trim());
            Fields.Add("QUARTER", DateTools.GetQuarterRomeName(_selectedQuarter, true));
            Fields.Add("YEAR", _selectedYear.ToString());

            Fields.Add("SSUM1_R", Math.Floor(_ssum1).ToString("n0", _culture).Trim());
            Fields.Add("SSUM1_K", ((_ssum1 - Math.Floor(_ssum1)) * 100).ToString("00", _culture).Trim());
            Fields.Add("SSUM2_R", Math.Floor(_ssum2).ToString("n0", _culture).Trim());
            Fields.Add("SSUM2_K", ((_ssum2 - Math.Floor(_ssum2)) * 100).ToString("00", _culture).Trim());
            Fields.Add("PERSON", _person.FormattedFullName);
            Fields.Add("POSITION", _person.Position);
            Fields.Add("PERSON2", _person2.FormattedFullName);
            Fields.Add("POSITION2", _person2.Position);

            var sb = new StringBuilder();
            trList.ForEach(a =>
                    sb.Append($@"{a.FinalSumm.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim()} руб. - в соответствии с распоряжением Правления ПФР от {a.RegDate?.ToString("dd.MM.yyyy")} г. № {a.RegNum}; "));
            sb.Append($@"{percB.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim()} руб. - проценты по депозитам; {depSumA.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim()} руб. - сумма возвращенных депозитов.");
            Fields.Add("SEQUENCE_PLACEHOLDER", sb.ToString());
            //AccOperation {1} NewDate  {2} RegNum
        }

        protected override void GenerateDocument(Application app, Document doc)
        {
            Name = ReplaceInvalidFileNameChars($"РАСЧЕТ ЧИСТОГО ФИНАНСОВОГО РЕЗУЛЬТАТА ДЛЯ ПОРТФЕЛЯ {_portfolio.Year}_{Guid.NewGuid()}");
            FilePathSave = Path.Combine(Folder ?? "", Name + ".doc");
            base.GenerateDocument(app, doc);
        }


        private string ReplaceInvalidFileNameChars(string fileName)
        {
            char[] charInvalidFileChars = Path.GetInvalidFileNameChars();

            return charInvalidFileChars.Aggregate(fileName, (current, charInvalid) => current.Replace(charInvalid.ToString(), string.Empty));
        }
    }

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class SelectChfrReportYearQuartalDlgViewModel : ViewModelCardDialog
    {
        public class PortfolioItem: Portfolio
        {
            public int Quartal { get; set; }
            public bool IsDop { get; set; }

            public override string ToString()
            {
                return Year;
            }
        }

        public Portfolio Portfolio { get; set; }
        public List<Portfolio> List { get; set; }

        public decimal? SpravSum1
        {
            get { return _spravSum1; }
            set { _spravSum1 = value; OnPropertyChanged(nameof(SpravSum1)); }
        }

        public decimal? SpravSum2
        {
            get { return _spravSum2; }
            set { _spravSum2 = value; OnPropertyChanged(nameof(SpravSum2)); }
        }     

        public List<string> OkudList { get; set; }


        private decimal? _spravSum1;
        private decimal? _spravSum2;


        public ICommand OkCommand { get; set; }

        public ObservableCollection<Person> PersonList { get; set; }

        private Person _selectedPerson;
        private Person _selectedPerson2;

        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set { _selectedPerson = value; OnPropertyChanged(nameof(SelectedPerson)); }
        }

        public Person SelectedPerson2
        {
            get => _selectedPerson2;
            set { _selectedPerson2 = value;
                OnPropertyChanged(nameof(SelectedPerson2));
            }
        }


        public SelectChfrReportYearQuartalDlgViewModel()
            : base(true)
        {
            IsNotSavable = true;

            List = new List<Portfolio>();
            var list = DataContainerFacade.GetList<Portfolio>().OrderBy(a => a.Year);
            List.AddRange(list.Where(a => a.Year.Like(@"____\_ кв%")).Select(a =>
            {
                var p = new PortfolioItem {ID = a.ID, Year = a.Year, YearID = a.YearID, Name = a.Name};
                p.Quartal = int.Parse(p.Year.Split(' ')[0].Split('\\')[1]);
                return p;
            }));
            List.AddRange(list.Where(a => a.Year.Like(@"ДСВ ____\_ кв%")).Select(a =>
            {
                var p = new PortfolioItem { ID = a.ID, Year = a.Year, YearID = a.YearID, Name = a.Name, IsDop = true };
                p.Quartal = int.Parse(p.Year.Split(' ')[1].Split('\\')[1]);
                return p;
            }));

            Portfolio = List.FirstOrDefault();
            OkCommand = new DelegateCommand(a=> ValidateFields(), a =>
            {
                var pi = (PortfolioItem) Portfolio;
                new PrintChfrReportViewModel(pi, _spravSum1.Value, _spravSum2.Value, _selectedPerson, _selectedPerson2).Print();
                OnRequestClose(true);
            });

            PersonList = new ObservableCollection<Person>(PersonHelper.GetAvailablePersons(DataObjects.Document.Types.OPFR));
            SelectedPerson = PersonList.FirstOrDefault();
            SelectedPerson2 = PersonList.FirstOrDefault();
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case nameof(SpravSum1):
                        return SpravSum1 == null ? "Не указана сумма" : null;
                    case nameof(SpravSum2):
                        return SpravSum2 == null ? "Не указана сумма" : null;
					case nameof(SelectedPerson): return _selectedPerson.ValidateRequired();
					case nameof(SelectedPerson2): return _selectedPerson2.ValidateRequired();
                    default:
                        return null;
                }
            }
        }

        public override bool ValidateFields(string fieldString = null)
        {
            return base.ValidateFields($"{nameof(SpravSum1)}|{nameof(SpravSum2)}|{nameof(SelectedPerson)}|{nameof(SelectedPerson2)}");
        }
    }
}
