﻿using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsCard;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PrintDepClaimOffer : PrintToDoc
    {
        private const string DATE_FORMAT = "dd.MM.yyyy";
        private const string TIME_FORMAT = "hh\\:mm";
        public DepClaimOfferViewModel Offer { get; private set; }
        private readonly CultureInfo _culture = CultureInfo.CreateSpecificCulture("ru-RU");


        public string Folder { get; set; }
        public bool KeepOpen { get; set; }

        public PrintDepClaimOffer(long offerID)
        {
            KeepOpen = true;
            Folder = Path.GetTempPath();
            Offer = new DepClaimOfferViewModel(offerID, ViewModelState.Read);


            Name =ReplaceInvalidFileNameChars( string.Format("Оферта ПФР №{0} ({1})", Offer.OfferNumber, Offer.BankName));

            TemplateName = "Оферта ПФР.doc";

        }

        protected override void FillData()
        {
            Fields.Add("AuctionDate", Offer.AuctionDate.ToString(DATE_FORMAT));
            Fields.Add("AuctionStartTime", Offer.AuctionStartTime.ToString(TIME_FORMAT));
            Fields.Add("AuctionEndTime", Offer.AuctionEndTime.ToString(TIME_FORMAT));
            Fields.Add("OfferDate", Offer.OfferDate.ToString(DATE_FORMAT));
            Fields.Add("OfferStartTime", Offer.OfferStartTime.ToString(TIME_FORMAT));
            Fields.Add("OfferEndTime", Offer.OfferEndTime.ToString(TIME_FORMAT));
            Fields.Add("SenderName", Offer.SenderName);
            Fields.Add("BankName", Offer.BankName);
            Fields.Add("FORMALIZEDNAME", Offer.ShortBankName);
            Fields.Add("SHORTNAME", Offer.ShortBankName);
            Fields.Add("FULLNAME", Offer.FullBankName);
            Fields.Add("PFRAGR", Offer.PFRAGR);
            Fields.Add("PFRAGRBUM", Offer.PFRAGRBUM);
            Fields.Add("PFRAGRDATE", (Offer.PFRAGRDATE != null) ? Offer.PFRAGRDATE.Value.ToString(DATE_FORMAT) : string.Empty);
            Fields.Add("BankBIK", Offer.BankBIK);
            Fields.Add("BankCorrAcc", Offer.BankCorrAcc);
            Fields.Add("AuctionDate2", Offer.AuctionDate2.ToString(DATE_FORMAT));
            Fields.Add("OfferNumber", Offer.OfferNumber.ToString());

            Fields.Add("Amount", Offer.Amount.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            //Fields.Add("Rate", Offer.Rate.ToString("n2"));
            Fields.Add("Rate", Offer.Rate.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            Fields.Add("Payment", Offer.Payment.ToString("# ### ### ### ### ### ### ##0.00", _culture).Trim());
            Fields.Add("Currency", Offer.Currency);
            Fields.Add("SettleDate", Offer.SettleDate.ToString(DATE_FORMAT));
            Fields.Add("ReturnDate", Offer.ReturnDate.ToString(DATE_FORMAT));


        }

        protected override void GenerateDocument(Application app, Document doc)
        {
            base.GenerateDocument(app, doc);
            base.FilePathSave = Path.Combine(Folder ?? "" ,Name +".doc");
            //object missing = Missing.Value;
            //doc.SaveAs(Folder + "\\" + Name + ".doc",
            //    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
            //    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

            //if (!KeepOpen)
            //    app.Quit(ref missing, ref missing, ref missing);
            //doc.Close(missing, missing, missing);
        }


        private string ReplaceInvalidFileNameChars(string fileName)
        {
            char[] charInvalidFileChars = Path.GetInvalidFileNameChars();

            return charInvalidFileChars.Aggregate(fileName, (current, charInvalid) => current.Replace(charInvalid.ToString(), string.Empty));
        }

    }
}
