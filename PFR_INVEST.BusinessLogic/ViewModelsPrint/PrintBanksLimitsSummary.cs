﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using Application = Microsoft.Office.Interop.Word.Application;
using Document = Microsoft.Office.Interop.Word.Document;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class PrintBanksLimitsSummary : PrintToDoc
	{
		private readonly IList<LegalEntity> _banks;
		private readonly DateTime _date;
		private readonly Person _authorizedPerson;
		private readonly Person _perfomer;
		private readonly DepClaimSelectParams _selectParams;
		private readonly IList<DepClaim2> _depclaims;

		public Dictionary<long, decimal> LimitOnValues { get; private set; }
		public Dictionary<long, decimal> LimitInfoOnValues { get; private set; }
        private readonly Person _deputy;

        protected override string PrintingInformation => "Печать сведений о лимитах";

	    public PrintBanksLimitsSummary(string filePath, IList<LegalEntity> banks, DateTime date, Person authorizedPerson, Person perfomer, DepClaimSelectParams selectParams, IList<DepClaim2> depclaims, Person deputy)
		{
		    FilePathSave = filePath;
		    _deputy = deputy;
            Name = "Сведения о лимитах";
            TemplateName = string.Concat(Name, ".doc");
			_banks = banks;
			_date = date;
			_authorizedPerson = authorizedPerson;
			_perfomer = perfomer;
			_selectParams = selectParams;
			LimitOnValues = new Dictionary<long, decimal>();
			LimitInfoOnValues = new Dictionary<long, decimal>();
			_depclaims = depclaims;
		}

        #region Fill Data

        protected void FillPersonsData()
		{
			var apDict = DataContainerFacade.GetEntityDBProjection(_authorizedPerson);
			FillEntityData("AUTH", apDict);
			FillPersonFIO("AUTH", apDict);
            var dpDict = DataContainerFacade.GetEntityDBProjection(_deputy);
            FillEntityData("DEPUTY", dpDict);
            FillPersonFIO("DEPUTY", dpDict);
			var pfDict = DataContainerFacade.GetEntityDBProjection(_perfomer);
			FillEntityData("PERFORMER", pfDict);
			FillPersonFIO("PERFORMER", pfDict);
		}

		protected void FillPersonFIO(string prefix, Dictionary<string, object> ent)
		{
			if (ent == null) return;
			string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";
			var fn = ent["PERSON_FIRST_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_F")] = (!string.IsNullOrEmpty(fn)) ? fn.First() + "." : string.Empty;

			var mn = ent["PERSON_MIDDLE_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_M")] = (!string.IsNullOrEmpty(mn)) ? mn.First() + "." : string.Empty;

			var ln = ent["PERSON_LAST_NAME"] as string;
			Fields[string.Format(fieldTemplate, prefix, "PERSON_L")] = (!string.IsNullOrEmpty(ln)) ? ln.First() + "." : string.Empty;
		}

		protected void FillAuctionData()
		{
			var aucDic = DataContainerFacade.GetEntityDBProjection(_selectParams);
			FillEntityData(string.Empty, aucDic);
		}

		protected void FillEntityData(string prefix, Dictionary<string, object> ent)
		{
			if (ent == null) return;
			string fieldTemplate = string.IsNullOrWhiteSpace(prefix) ? "{1}" : "{0}.{1}";

			foreach (var kvp in ent)
			{
				var key = string.Format(fieldTemplate, prefix, kvp.Key);

				string val;
				if (kvp.Value is DateTime?)
					val = (kvp.Value as DateTime?).Value.ToLongDateString();
				else
					val = kvp.Value?.ToString() ?? string.Empty;

				Fields[key] = val;
			}
		}

		protected override void FillData()
		{
			var dft = Thread.CurrentThread.CurrentCulture;
			var ruRu = new CultureInfo("ru-RU");
			Thread.CurrentThread.CurrentCulture = ruRu;

			try
			{
				FillPersonsData();
				FillAuctionData();
				Fields.Add("CURDATE", _date.ToLongDateString());
				Fields.Add("SELECTDATE", _selectParams.SelectDate.ToLongDateString());
				if (_authorizedPerson != null)
				{
				    Fields.Add("AUTHPERSON",
				        !string.IsNullOrEmpty(_authorizedPerson.MiddleName)
				            ? $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.{_authorizedPerson.MiddleName.First()}."
				            : $"{_authorizedPerson.LastName} {_authorizedPerson.FirstName.First()}.");
				    Fields.Add("AUTHPERSONPOSITION", _authorizedPerson.Position);
				}
				else
				{
					Fields.Add("AUTHPERSON", string.Empty);
					Fields.Add("AUTHPERSONPOSITION", string.Empty);
				}

                if (_deputy != null)
                {
                    Fields.Add("DEPUTY",
                        !string.IsNullOrEmpty(_deputy.MiddleName)
                            ? $"{_deputy.LastName} {_deputy.FirstName.First()}.{_deputy.MiddleName.First()}."
                            : $"{_deputy.LastName} {_deputy.FirstName.First()}.");
                    Fields.Add("DEPUTYPOSITION", _deputy.Position);
                }
				if (_perfomer != null)
				{
				    Fields.Add("PERFOMER",
				        !string.IsNullOrEmpty(_perfomer.MiddleName)
				            ? $"{_perfomer.LastName} {_perfomer.FirstName.First()}.{_perfomer.MiddleName.First()}."
				            : $"{_perfomer.LastName} {_perfomer.FirstName.First()}.");
				    Fields.Add("PERFOMERPOSITION", _perfomer.Position);
					Fields.Add("PERFOMERPHONE", _perfomer.Phone);
				}
				else
				{
					Fields.Add("PERFOMER", string.Empty);
					Fields.Add("PERFOMERPOSITION", string.Empty);
					Fields.Add("PERFOMERPHONE", string.Empty);
				}
                Fields.Add("LOW_DATE", _banks.Count > 0 ? (_banks[0].MoneyDate?.ToString("dd.MM.yyyy") ?? "") : "");
			}
			finally
			{
				Thread.CurrentThread.CurrentCulture = dft;
			}
		}

        #endregion

        protected override void GenerateDocument(Application app, Document doc)
		{
			base.GenerateDocument(app, doc);

            var bankList = _banks.OrderBy(a => a.NumberForDocuments ?? int.MaxValue).ThenBy(a => a.ShortName).ToList();

		    var table = doc.Tables[1];

            foreach (var bank in bankList)
            {
               // if (!PrintBanksVerification.IsKOValidBank(bank, _date, _legalEntityRatings, _multiplierRatings)) continue;
                table.Rows.Add();
                var lastRowIndex = table.Rows.Count;
                table.Cell(lastRowIndex, 1).Range.Text = bank.ShortName;

                {
                    decimal limitMoney = BankCalculator.Limit4Money(bank)*1000000;

                    var bank1 = bank;
                    var deposits = _depclaims.Where(p => p.BankID == bank1.ID);
                    decimal placedAndReturned = 0;
                    decimal returned = 0;
                    foreach (var deposit in deposits)
                    {
                        if (deposit.SettleDate <= _selectParams.SelectDate && deposit.ReturnDate > _selectParams.SelectDate)
                            placedAndReturned += deposit.Amount;

                        if (deposit.ReturnDate == _selectParams.SelectDate.AddDays(-1)
                            || deposit.ReturnDate == _selectParams.SelectDate
                            || deposit.ReturnDate == _selectParams.PlacementDate)
                            returned += deposit.Amount;
                    }
                    var resultValue = limitMoney - placedAndReturned + returned;

                    //LimitOnValues.Add(bank.ID, Math.Round(resultValue));
                    //LimitInfoOnValues.Add(bank.ID, Math.Round(limitMoney));

                    // Display in millions
                    limitMoney = limitMoney/1000000;
                    resultValue = resultValue/1000000;

                    //Round
                    limitMoney = Math.Round(limitMoney);
                    resultValue = Math.Round(resultValue);

                    table.Cell(lastRowIndex, 2).Range.Text = limitMoney.ToString("### ### ### ### ### ###").Trim();
                    table.Cell(lastRowIndex, 3).Range.Text = resultValue.ToString("### ### ### ### ### ###").Trim();
                    table.Cell(lastRowIndex, 4).Range.Text = "-";
                }
            }
		}
	}
}
