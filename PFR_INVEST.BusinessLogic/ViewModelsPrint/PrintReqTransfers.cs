﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class PrintReqTransfers : ViewModelBase
    {
        private const string TEMPLATE_NAME = "Заявка на перечисление.xls";
        private Application _wordApp;
        private Worksheet _worksheet;
        private List<ReqTransfer> ReqTransfers { get; set; }
        //private readonly string OperationName;
        private readonly bool _isVr;
        private readonly string _payAssignment;
        private readonly string _claimNumber;
        private readonly DateTime? _claimDate;

        protected PrintReqTransfers(bool isVr, string claimNumber, DateTime? claimDate)
        {
            _isVr = isVr;
            _claimNumber = claimNumber;
            _claimDate = claimDate;
        }

        public PrintReqTransfers(SIRegister register, bool isVr, string claimNumber, DateTime? claimDate)
            : this(isVr, claimNumber, claimDate)
        {
            _payAssignment = register.PayAssignment;
            //OperationName = regiser.GetOperation().Name;
            ReqTransfers = BLServiceSystem.Client.GetTransfersForSIRegisterHib(register.ID)
                .Where(reqtr => TransferStatusIdentifier.IsStatusInitialState(reqtr.TransferStatus)).ToList();
        }

        public PrintReqTransfers(ReqTransfer rTransfer, bool isVr, string claimNumber, DateTime? claimDate)
            : this(isVr, claimNumber, claimDate)
        {
            //_register = rTransfer.GetTransferList().GetTransferRegister();
            //OperationName = regiser.GetOperation().Name;
            ReqTransfers = new List<ReqTransfer> { rTransfer };
        }

        private void UpdateTransfersStatus()
        {
            foreach (var transfer in ReqTransfers)
            {
                transfer.TransferStatus = TransferStatusIdentifier.sClaimCompleted;
                transfer.Date = _claimDate.HasValue ? (DateTime?)_claimDate.Value.Date : null;
                transfer.ClaimNumber = _claimNumber;
                DataContainerFacade.Save<ReqTransfer, long>(transfer);
            }
        }

        /*private string SPNRedistribution = "передача СПН по договору от ______ № ______ (Перераспределение СПН)";
		private string SPNRedistributionDSV = "передача СПН по договору от ______ № ______ (в пользу з/л, указанных в части 2 ст.11, 56-ФЗ от 30.04.2008)";
		private string SPNRedistributionMSK = "";
		private string SPNReturn = "передача СПН по договору от ______ № ______ в связи с прекращением договора с ЧУК";
		private string SPNYearPortfolioDissolution = "передача страховых взносов ______ года по договору от ______ № ______";
		private string DSVQuarterPortfolioDissolution = "передача доп. взносов и взносов работодателя за ______ квартал ______ года по договору от ______ № ______";
		private string MSKHalfYearPortfolioDissolution = "передача средств (части средств) материнского (семейного) капитала (по договору от ______ № ______ (в соответствии с заявлениями лиц, указанных в п. 1 и 2 ч.1 ст. 3, 256-ФЗ от 29.12.2006).";
		private string DSVCofinancing = "передача средств на софинансирование формирования пенсионных накоплений по договору от ______ № ______ (в пользу з/л, указанных в п. 1,3 ст.12, 56-ФЗ от 30.04.2008)";
        */
        private string GeneratePurpose()
        {
            if (!(string.IsNullOrEmpty(_payAssignment)))
                return _payAssignment;

            if (ReqTransfers.Count == 0) return "";
            var rq = ReqTransfers.First();
            var tl = rq.GetTransferList();
            var contract = tl.GetContract();
            var reg = tl.GetTransferRegister();
            var op = reg.GetOperation();
            var direction = reg.GetDirection();
            var part = _isVr ? (long)Element.SpecialDictionaryItems.AppPartVR : (long)Element.SpecialDictionaryItems.AppPartSI; var cMonth = reg.CompanyMonthID == null ? null : new CultureInfo("ru-RU").DateTimeFormat.MonthNames[reg.CompanyMonthID.Value - 1];
            var payAss = BLServiceSystem.Client.GetPayAssForReport(part, Element.SiDirectionToCommonDirection(direction), op.ID);
            return PaymentAssignmentHelper.ReplaceTagDataForSIVR(payAss == null ? "" : payAss.Name, contract.ContractDate, contract.ContractNumber, reg.CompanyYearID.HasValue ? (reg.CompanyYearID.Value + 2000).ToString() : "", cMonth);

            /*if (TransferOperationIdentifier.IsDSVCofinancingOpertion(OperationName))
				return DSVCofinancing;
			else if (TransferOperationIdentifier.IsMSKHalfYearPortfolioDissolutionOpertion(OperationName))
				return MSKHalfYearPortfolioDissolution;
			else if (TransferOperationIdentifier.IsDSVQuarterPortfolioDissolutionOpertion(OperationName))
				return DSVQuarterPortfolioDissolution;
			else if (TransferOperationIdentifier.IsSPNYearPortfolioDissolutionOpertion(OperationName))
				return SPNYearPortfolioDissolution;
			else if (TransferOperationIdentifier.IsSPNReturnOpertion(OperationName))
				return SPNReturn;
			else if (TransferOperationIdentifier.IsSPNRedistributionMSKOpertion(OperationName))
				return SPNRedistributionMSK;
			else if (TransferOperationIdentifier.IsSPNRedistributionDSVOpertion(OperationName))
				return SPNRedistributionDSV;
			else if (TransferOperationIdentifier.IsSPNRedistributionOpertion(OperationName))
				return SPNRedistribution;
			else
				return string.Empty;*/
        }

        private void GenerateDoc()
        {
            _worksheet.Range["E6"].Cells.Value2 = _claimDate == null ? "" : _claimDate.Value.ToShortDateString();
            _worksheet.Range["B15"].Cells.Value2 = GeneratePurpose();

            var totalSumm = ReqTransfers.Sum(reqTr => reqTr.Sum.GetValueOrDefault());

            var rowCounter = 11;

            //Сортируем по УК + Договор
            ReqTransfers = ReqTransfers.OrderBy(r => r.GetTransferList().GetContract().GetLegalEntity().FormalizedName)
                                    .ThenBy(r => r.GetTransferList().GetContract().ContractNumber)
                                    .ToList();

            for (var i = 0; i < ReqTransfers.Count; i++)
            {
                var reqTransfer = ReqTransfers[i];

                var contract = reqTransfer.GetTransferList().GetContract();
                var legalEntity = contract.GetLegalEntity();
                var bankAccount = contract.GetBankAccount();
                //Для Специализированого депозитария нет банковских реквизитов				

                _worksheet.Range["A" + rowCounter].Cells.Value2 = bankAccount != null ? bankAccount.LegalEntityName : string.Empty;
                _worksheet.Range["B" + rowCounter].Cells.Value2 = contract.ContractNumber;
                _worksheet.Range["C" + rowCounter].Cells.Value2 = contract.ContractDate.GetValueOrDefault().ToShortDateString();
                _worksheet.Range["D" + rowCounter].Cells.Value2 = legalEntity.INN;
                _worksheet.Range["E" + rowCounter].Cells.Value2 = bankAccount != null ? bankAccount.AccountNumber : string.Empty;
                _worksheet.Range["F" + rowCounter].Cells.Value2 = bankAccount != null ? bankAccount.BankName : string.Empty;
                _worksheet.Range["G" + rowCounter].Cells.Value2 = bankAccount != null ? bankAccount.CorrespondentAccountNumber : string.Empty;
                _worksheet.Range["H" + rowCounter].Cells.Value2 = bankAccount != null ? bankAccount.BIK : string.Empty;
                _worksheet.Range["I" + rowCounter].Cells.Value2 = reqTransfer.Sum;

                if (i != ReqTransfers.Count - 1)
                {

                    _worksheet.Range["A" + (rowCounter + 1)].EntireRow.Insert(XlInsertShiftDirection.xlShiftDown, Type.Missing);

                    //Copy format
                    var r1 = (Range)_worksheet.Rows[rowCounter];
                    r1.Copy(Type.Missing);
                    //Paste format
                    var r2 = (Range)_worksheet.Rows[rowCounter + 1];
                    r2.PasteSpecial(XlPasteType.xlPasteFormats,
                        XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
                }

                rowCounter++;
            }

            _worksheet.Range["I" + rowCounter].Cells.Value2 = totalSumm;
            _worksheet.Range["B" + (rowCounter + 1)].Cells.Value2 = Сумма.Пропись(totalSumm, Валюта.Рубли);
            _worksheet.Range["A1"].Select();
            //Сброс выделения ячеек для копирования
            _worksheet.Application.CutCopyMode = XlCutCopyMode.xlCopy;
        }

        public bool Print()
        {
            if (ReqTransfers.Count < 1)
                return false;

            object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

            if (File.Exists(filePath.ToString()))
            {
                RaiseStartedPrinting();

                var oldCi = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

                try
                {
                    _wordApp = new Application { Visible = false, DisplayAlerts = false};
                    _worksheet = (Worksheet)_wordApp.Workbooks.Open(filePath.ToString()).Sheets[1];
                    GenerateDoc();
                    _wordApp.Visible = true;
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCi;
                    if (_worksheet != null)
                        Marshal.FinalReleaseComObject(_worksheet);
                    if (_wordApp != null)
                        Marshal.FinalReleaseComObject(_wordApp);
                }

                UpdateTransfersStatus();

                RaiseFinishedPrinting();

                return true;
            }
            else
            {
                RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                return false;
            }
        }
    }

}
