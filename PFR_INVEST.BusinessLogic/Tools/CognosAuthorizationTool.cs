﻿using System.Net;
using System.Text;

namespace PFR_INVEST.BusinessLogic.Tools
{
    public enum ReportFolderType
    {
        NPF = 0,
        SI,
        CB,
        BO,
        VR
    }

    public class CognosAuthorizationTool
    {
        public static string GetCognosAuthHtml(NetworkCredential credential)
        {
            var authHtmlBuilder = new StringBuilder();
            authHtmlBuilder.AppendLine("<html>");
            authHtmlBuilder.AppendLine("<body onload=\"setTimeout('document.logon.submit();',25)\">");
            authHtmlBuilder.AppendLine($"<form  name=\"logon\" method=\"POST\" action=\"{Core.Properties.Settings.Default.CognosDictionaryUrl}\">");
            authHtmlBuilder.AppendLine($"<input type=\"hidden\" name=\"CAMNamespace\" value=\"{Core.Properties.Settings.Default.CognosDomainName}\">");
            authHtmlBuilder.AppendLine($"<input type=\"hidden\" name=\"CAMUsername\" value=\"{credential.UserName}\">");
            authHtmlBuilder.AppendLine($"<input type=\"hidden\" name=\"CAMPassword\" value=\"{credential.Password}\">");
            authHtmlBuilder.AppendLine("</form>");
            authHtmlBuilder.AppendLine("</body>");
            authHtmlBuilder.AppendLine("</html>");
            return authHtmlBuilder.ToString();
        }

        public static string GetFolderModifier(ReportFolderType type)
        {
            string modifierFormatString = "m_folder={0}";
            switch (type)
            {
                case ReportFolderType.NPF:
                    return string.Format(modifierFormatString, SettingCognosManager.NPFUrl);
                case ReportFolderType.SI:
                    return string.Format(modifierFormatString, SettingCognosManager.SIUrl);
                case ReportFolderType.CB:
                    return string.Format(modifierFormatString, SettingCognosManager.CBUrl);
                case ReportFolderType.BO:
                    return string.Format(modifierFormatString, SettingCognosManager.BOUrl);
                case ReportFolderType.VR:
                    return string.Format(modifierFormatString, SettingCognosManager.VRUrl);
                default:
                    return string.Empty;
            }
        }
    }
}
