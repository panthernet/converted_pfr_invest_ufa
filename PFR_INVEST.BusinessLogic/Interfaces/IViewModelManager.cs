﻿using System;
using System.Collections.Generic;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    public interface IViewModelManager
    {
        void RefreshViewModels(params Type[] modelTypes);        

        void RefreshCardViewModel(Type type, long cardID);

		void UpdateDataInAllModels(Type type, long id);

		void UpdateDataInAllModels(IList<KeyValuePair<Type,long>> updatedData);
    }
}
