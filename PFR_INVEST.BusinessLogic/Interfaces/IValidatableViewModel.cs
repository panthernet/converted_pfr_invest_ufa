﻿namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Интерфейс, указыыающий, что модель содержит универсальный метод вызова валидации данных
    /// </summary>
    public interface IValidatableViewModel
    {
        string Validate();
    }
}
