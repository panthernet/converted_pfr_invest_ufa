﻿using System;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Интерфейс для моделей, которые отправляют запрос на закрытие окна во View
    /// </summary>
    public interface IRequestCloseViewModel
    {
        event EventHandler RequestClose;
    }
}
