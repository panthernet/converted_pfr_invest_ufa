﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Обновляемая модель (общая)
    /// </summary>
    public interface IRefreshableViewModel
    {
        void RefreshModel();
    }

    /// <summary>
    /// Обновляемая модель-карточка
    /// </summary>
    public interface IRefreshableCardViewModel : IRefreshableViewModel
    {
        long? CardID { get; }
    }

    public interface IRefreshableCardViewModel<T> : IRefreshableViewModel where T:BaseDataObject
    {       
    }
}
