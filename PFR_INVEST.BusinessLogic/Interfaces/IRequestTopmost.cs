﻿using System;

namespace PFR_INVEST.BusinessLogic.Interfaces
{
    /// <summary>
    /// Интерфейс для представления на установку приложения в режим TopMost
    /// </summary>
    public interface IRequestTopmost
    {
        /// <summary>
        /// Запрос для представления на установку приложения в режим TopMost
        /// </summary>
        event EventHandler RequestTopMost;
    }
}
