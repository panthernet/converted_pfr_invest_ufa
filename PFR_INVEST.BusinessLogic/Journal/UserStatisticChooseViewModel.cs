﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.Journal
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class UserStatisticChooseViewModel : ViewModelBase
    {
        private JournalFilter _mFilter = new JournalFilter();
        public JournalFilter Filter
        {
            get { return _mFilter; }
            set
            {
                if (_mFilter == value) return;
                _mFilter = value;                    
                OnPropertyChanged("Filter");
            }
        }

        //типы объектов
        private IList<string> _journalObjectList = new List<string>();
        public IList<string> JournalObjectList
        {
            get { return _journalObjectList; }
            set { _journalObjectList = value; OnPropertyChanged("JournalObjectList"); }
        }

        //типы событий
        private IList<string> _journalEventList;
        public IList<string> JournalEventList
        {
            get { return _journalEventList; }
            set { _journalEventList = value; OnPropertyChanged("JournalEventList"); }
        }

        //результат
        private IList<Audit> _journalEntryList = new List<Audit>();
        public IList<Audit> JournalEntryList
        {
            get { return _journalEntryList; }
            set { _journalEntryList = value; OnPropertyChanged("JournalEntryList"); }
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        public UserStatisticChooseViewModel()
        {
            LoadSources();            
            JournalLogger.LogTypeEvent(this, JournalEventType.SHOW_JOURNAL_LOG, "Открытие журнала действий пользователей");
        }

        private void LoadSources()
        {
            //тут загружаем типы объектов
            JournalObjectList = BLServiceSystem.Client.GetJournalEventObjectList();

            //тут загружаем типы событий
            var list = (from object value in Enum.GetValues(typeof (JournalEventType)) 
                        select ((Enum) value).GetDescription())
                        .ToList(); //Utils.GetEnumKeyValues<JournalEventType>();
            JournalEventList = list;
        }

        public void RefreshEntryList()
        {
            JournalLogger.LogTypeEvent(this, JournalEventType.FILTER_JOURNAL_LOG, "Построение отчета по журналу действий пользователей", GetFilterLog());
            LoadSources();
            JournalEntryList = BLServiceSystem.Client.GetJournalLog(Filter);
        }

        protected String GetFilterLog()
        {
            return Filter == null ? null : String.Format("Данные фильтра: Тип события {0}, Имя объекта: {1}, Логин ОС: {2}, Рабочая станция: {3}, UserId: {4}", Filter.EventType, Filter.ObjectName, Filter.OsID, Filter.HostName, Filter.UserID);
        }
    }
}