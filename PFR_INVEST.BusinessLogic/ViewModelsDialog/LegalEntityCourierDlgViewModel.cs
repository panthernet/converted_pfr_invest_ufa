﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class LegalEntityCourierDlgViewModel : ViewModelBase, ILoaListProvider
    {
        public LegalEntityCourier Item { get; private set; }
        public LegalEntityCourier OriginalItem { get; private set; }

        public DelegateCommand<object> SaveCard { get; private set; }
        private LegalEntityCourierLetterOfAttorney LecItem { get; set; }

        public LegalEntityCourierLetterOfAttorney FocusedLOA { get; set; }

        public LegalEntityCourierDlgViewModel(LegalEntityCourier item)
        {
            OriginalItem = item;
            Item = new LegalEntityCourier();
            Item.ApplyValues(OriginalItem);
            Item.LoaListProvider = this;

            LecItem = GetItemLECLA(item);
            _oldNumber = Item.LetterOfAttorneyNumber;
            _oldIssueDate = Item.LetterOfAttorneyIssueDate;
            _oldExpireDate = Item.LetterOfAttorneyExpireDate;
            _oldRegisterDate = Item.LetterOfAttorneyRegisterDate;

            SaveCard = new DelegateCommand<object>(o => CanExecuteSave(), o => { ExecuteSave(); });

            DeleteLOA = new DelegateCommand(o => CanExecuteDeleteLOA(), o => ExecuteDeleteLOA());
        }


        private readonly string _oldNumber;
        private readonly DateTime? _oldIssueDate;
        private readonly DateTime? _oldExpireDate;
        private readonly DateTime? _oldRegisterDate;

        public ICommand DeleteLOA { get; private set; }

        private bool CanExecuteSave()
        {
            return IsValid();
        }

        private void ExecuteSave()
        {
            if (!IsValid())
                return;

            OriginalItem.ApplyValues(Item);
            OriginalItem.LetterOfAttorneyExpireAlert = OriginalItem.SignatureExpireAlert = (int)LegalEntityCourier.AlertTypes.NotAlerted;

            if (Item.IsLetterOfAttorneyNeedSave(_oldNumber, _oldIssueDate, _oldExpireDate, _oldRegisterDate))
            {
                if (!IsValidLetterOfAttorney())
                {
                    return;
                }

                //if (IsLetterOfAttorneyOld())
                //{
                var x = new LegalEntityCourierLetterOfAttorney()
                {
                    Number = _oldNumber,
                    IssueDate = _oldIssueDate,
                    ExpireDate = _oldExpireDate,
                    RegisterDate = _oldRegisterDate
                };
                LetterOfAttorneyList.Add(x);
                LetterOfAttorneyListChanged.Add(x);
                LecItem = GetItemLECLA(Item);
                //}
                //else
                //{
                //    LegalEntityCourierLetterOfAttorney x = new LegalEntityCourierLetterOfAttorney()
                //    {
                //        Number = Item.LetterOfAttorneyNumber,
                //        IssueDate = Item.LetterOfAttorneyIssueDate,
                //        ExpireDate = Item.LetterOfAttorneyExpireDate,
                //        RegisterDate = Item.LetterOfAttorneyRegisterDate
                //    };

                //    Item.LetterOfAttorneyNumber = _OldNumber;
                //    Item.LetterOfAttorneyIssueDate = _OldIssueDate;
                //    Item.LetterOfAttorneyExpireDate = _OldExpireDate;
                //    Item.LetterOfAttorneyRegisterDate = _OldRegisterDate;

                //    LetterOfAttorneyList.Add(x);
                //    LetterOfAttorneyListChanged.Add(x);
                //}
            }
        }


        private bool IsValid()
        {
            return Item.IsValid();
        }

        public bool IsReadOnly = false;

        public bool IsEnabled => !EditAccessDenied;

        public override bool EditAccessDenied => IsReadOnly || base.EditAccessDenied;

        public Visibility ButtonsVisibility => IsReadOnly ? Visibility.Collapsed : Visibility.Visible;

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyList
        {
            get
            {
                if (Item.LetterOfAttorneyList == null || Item.LetterOfAttorneyList.Count == 0)//Load List
                {
                    Item.LetterOfAttorneyList = DataContainerFacade.GetListByProperty<LegalEntityCourierLetterOfAttorney>("LegalEntityCourierID", Item.ID).ToList();
                }
                return Item.LetterOfAttorneyList;
            }
            set
            {
                Item.LetterOfAttorneyList = value;
            }
        }

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyReverseList => LetterOfAttorneyList.AsEnumerable().Reverse().ToList();

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListChanged
        {
            get { return Item.LetterOfAttorneyListChanged ?? (Item.LetterOfAttorneyListChanged = new List<LegalEntityCourierLetterOfAttorney>()); }
            set
            {
                Item.LetterOfAttorneyListChanged = value;
            }
        }

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListDeleted
        {
            get { return Item.LetterOfAttorneyListDeleted ?? (Item.LetterOfAttorneyListDeleted = new List<LegalEntityCourierLetterOfAttorney>()); }
            set
            {
                Item.LetterOfAttorneyListDeleted = value;
            }
        }

        public virtual void DeleteFromDatabase()
        {
            DataContainerFacade.Delete<LegalEntityCourier>(Item.ID);
        }

        public bool? AskSaveOnClose()
        {
            if (!IsValid())
            {
                bool b = DialogHelper.ShowConfirmation("Вы уверены, что хотите закрыть диалог и потерять все изменения?");
                return b ? (bool?)false : null;
            }

            IsDataChanged = true;
            return DataChanged() && DialogHelper.ConfirmLegalEntityCourierSaveOnClose() == true;
        }

        private bool DataChanged()
        {
            return !Item.Equals(OriginalItem);
        }

        public bool CanSaveOnClose()
        {
            //if (!IsValidLetterOfAttorney())
            //    return false;

            bool b = IsValid();

            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }

        /// <summary>
        /// No messages inside
        /// </summary>
        /// <returns></returns>
        public bool IsValidLetterOfAttorney()
        {
            return string.IsNullOrEmpty(ValidateNewLoa(Item));
        }


        ///// <summary>
        ///// Triggers message if validation fails
        ///// </summary>
        ///// <returns></returns>
        //private bool IsVerificationLetterOfAttorney()
        //{
        //    bool verDate = IsNewLoaValidDate(Item);
        //    bool verNumber = IsNewLoaValidNumber(Item);
        //    if (!verDate && !verNumber)
        //    {
        //        DialogHelper.Alert("Даты действия и номер лицензии не могут пересекаться с предыдущими");
        //        return false;
        //    }
        //    else if (!verDate)
        //    {
        //        DialogHelper.Alert("Даты действия лицензии не могут пересекаться с предыдущими");
        //        return false;
        //    }
        //    else if (!verNumber)
        //    {
        //        DialogHelper.Alert("Номер лицензии не может пересекаться с предыдущими");
        //        return false;
        //    }

        //    return true;
        //}

       /* private bool IsLetterOfAttorneyOld()
        {
            List<LegalEntityCourierLetterOfAttorney> temp = LetterOfAttorneyReverseList;
            temp.Add(LecItem);

            return !temp.Any(l => l.ExpireDate > Item.LetterOfAttorneyIssueDate);
        }*/

        private static LegalEntityCourierLetterOfAttorney GetItemLECLA(LegalEntityCourier item)
        {
            return item.GetItemLECLA();
        }

        private bool IsCurrentLOANeedSave(LegalEntityCourier x)
        {
            return x.IsLetterOfAttorneyNeedSave(_oldNumber, _oldIssueDate, _oldExpireDate, _oldRegisterDate);
        }

        public string ValidateNewLoa(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            temp.Add(LecItem);

            var verDate = Item.IsValidLetterOfAttorneyDate(temp);

            var verNumber = Item.IsValidLetterOfAttorneyNumber(temp);

            return verNumber && verDate ? null : "Введены некорректные данные доверенности";
        }

        public string ValidateNewLoaDate(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            //temp.Add(_LecItem);

            if (!x.IsValidLetterOfAttorneyDate(temp))
            {
                return "Даты действия доверенности не могут пересекаться с предыдущими";
            }

            if (!x.IsValidLetterOfAttorneyDate(new List<LegalEntityCourierLetterOfAttorney> { LecItem }))
            {
                return "Даты действия доверенности не могут пересекаться с текущими значениями"
                    + (LecItem.IssueDate.HasValue && LecItem.ExpireDate.HasValue ?
                    string.Format(": {0} - {1}", LecItem.IssueDate.Value.ToShortDateString(), LecItem.ExpireDate.Value.ToShortDateString()) : string.Empty);
            }

            return null;
        }

        public string ValidateNewLoaNumber(LegalEntityCourier x)
        {
            if (!IsCurrentLOANeedSave(x))
                return null;

            var temp = LetterOfAttorneyReverseList;
            //temp.Add(_LecItem);

            if (!x.IsValidLetterOfAttorneyNumber(temp))
            {
                return "Номер доверенности не может пересекаться с предыдущими";
            }

            if (!x.IsValidLetterOfAttorneyDate(new List<LegalEntityCourierLetterOfAttorney> { LecItem }))
            {
                return "Номер доверенности не может совпадать с текущим при измененных датах доверенности";
            }

            return null;
        }

        public bool CanExecuteDeleteLOA()
        {
            return FocusedLOA != null;
        }

        public void ExecuteDeleteLOA()
        {
            if (!DialogHelper.ShowConfirmation("Вы действительно хотите удалить запись в выданной доверенности?"))
            {
                return;
            }

            if (FocusedLOA.ID > 0)
            {
                LetterOfAttorneyListDeleted.Add(FocusedLOA);
            }

            LetterOfAttorneyList.Remove(FocusedLOA);
            OnPropertyChanged("LetterOfAttorneyList");
            OnPropertyChanged("LetterOfAttorneyReverseList");
            Item.OnPropertyChanged("LetterOfAttorneyNumber");
            Item.OnPropertyChanged("LetterOfAttorneyIssueDate");
            Item.OnPropertyChanged("LetterOfAttorneyExpireDate");
            FocusedLOA = null;
        }
    }
}
