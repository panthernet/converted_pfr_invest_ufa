﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class BankStockCodeDlgViewModel : ViewModelBase
    {
        public bool IsStockReadOnly { get;private set; }
        public BankStockCode Value { get; private set; }
        public IList<Stock> StockList { get; private set; }
        public DelegateCommand<object> SaveCard { get; private set; }

        public BankStockCodeDlgViewModel(BankStockCode value, bool isStockReadOnly, IList<long> excludedAgenceis)
        {
            StockList = DataContainerFacade.GetList<Stock>().Where(a => !excludedAgenceis.Contains(a.ID)).ToList();
            Value = value;
            IsStockReadOnly = isStockReadOnly;
            SaveCard = new DelegateCommand<object>(o => Value.Validate(), o => { });

            if (Value.StockID == 0 && StockList.Count == 1)
                Value.StockID = StockList.First().ID;
        }

		public string ValidateStockCodeUnique()
		{
			var codes = DataContainerFacade.GetListByProperty<BankStockCode>("StockCode", Value.StockCode);
			var existsStock = codes.Where(x => x.StockID == Value.StockID && x.BankID != Value.BankID).ToList();
			if (existsStock.Any())
			{
				var stock = DataContainerFacade.GetByID<Stock>(existsStock.First().StockID);
				var bank = DataContainerFacade.GetByID<LegalEntity>(existsStock.First().BankID);
				return string.Format("Биржевой код \"{0}\" уже зарегистрирован для биржи \"{1}\" в банке \"{2}\"", Value.StockCode, stock.Name, bank.ShortName);
			}
			return null;
		}
       


    }
}
