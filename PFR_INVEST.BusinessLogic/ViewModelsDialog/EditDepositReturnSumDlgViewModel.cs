﻿using System;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public sealed class EditDepositReturnSumDlgViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        private DateTime? _PaymentDate;
        private decimal _AmountActual;
        private decimal _AmountMax;

        public DateTime? PaymentDate
        {
            get
            {
                return _PaymentDate;
            }
            set
            {
                _PaymentDate = value;
                OnPropertyChanged("PaymentDate");
            }
        }

        public decimal AmountActual
        {
            get
            {
                return _AmountActual;
            }
            set
            {
                _AmountActual = value;
                OnPropertyChanged("AmountActual");
            }
        }

        public decimal AmountMax
        {
            get
            {
                return _AmountMax;
            }
            set
            {
                _AmountMax = value;
                OnPropertyChanged("AmountMax");
            }
        }

        public ICommand SaveCard
        {
            get;
            protected set;
        }


        public EditDepositReturnSumDlgViewModel(DateTime paymentDate, decimal amountActual, decimal amountMax)
        {
            PaymentDate = paymentDate;
            AmountActual = amountActual;
            AmountMax = amountMax;

            SaveCard = new DelegateCommand(o => CanExecuteSaveInternal(),
                o => ExecuteSaveInternal());
        }

        private void ExecuteSaveInternal()
        {
        }

        private bool CanExecuteSaveInternal()
        {
            return IsValid();
        }

        protected override void ExecuteSave()
        {
        }

        public override bool CanExecuteSave()
        {
            return IsValid();
        }

        private bool IsValid()
        {
            return string.IsNullOrEmpty(this["PaymentDate"]) && string.IsNullOrEmpty(this["AmountActual"]);
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "PaymentDate":
                        if (PaymentDate==null || PaymentDate == DateTime.MinValue)
                            return "Выберите дату перевода";
                        break;
                    case "AmountActual":
                        if (AmountActual > AmountMax)
                            return string.Format("Сумма перевода не должна превышать {0}", AmountMax);
                        if (AmountActual < 0)
                            return "Значение не может быть меньше 0";

                        if (AmountActual == 0)
                            return "Недопустимое значение 0";
                        break;
                }
                return string.Empty;
            }
        }

    }
}
