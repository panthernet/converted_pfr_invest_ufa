﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class PrintBankPersonsDlgViewModel : ViewModelBase
    {
        public int SelectedBankIndex
        {
            get { return _selectedBankIndex; }
            set { _selectedBankIndex = value; OnPropertyChanged(nameof(SelectedBankIndex));}
        }

        public List<string> BankOptions { get; set; } = new List<string>
        {
            "Все",
            "Имеющие соглашение с ПФР"
        };

        public DelegateCommand ExportCommand { get; private set; }
        private bool _isAll;
        private bool _isExpired;
        private bool _isActive;

        private long _selectedStock;
        public long SelectedStock
        {
            get { return _selectedStock; }
            set { _selectedStock = value; OnPropertyChanged("SelectedStock"); }
        }

        private List<Stock> _stockList;
        public List<Stock> StockList
        {
            get { return _stockList; }
            set { _stockList = value; OnPropertyChanged("StockList"); }
        }

        public PrintBankPersonsDlgViewModel()
        {
            IsActivePersonsExport = true;
            var list = DataContainerFacade.GetList<Stock>();
            StockList = new List<Stock> {new Stock {Name = "Все", ID = 0}};
            StockList.AddRange(list);
            SelectedStock = 0;
        }


        private bool _isWithoutCertificates;
        private int _selectedBankIndex = 1;

        public bool IsWithoutCertificates
        {
            get { return _isWithoutCertificates; }
            set { _isWithoutCertificates = value; OnPropertyChanged("IsWithoutCertificates"); }
        }

        public enum BankPersonsExportType
        {
            All =0,
            Active = 1,
            Expiring = 2
        }

        /// <summary>
        /// 0 - все, 1 - активные, 2 - истекающие
        /// </summary>
        public BankPersonsExportType GetExportType()
        {
            return _isAll ? BankPersonsExportType.All : (_isActive ? BankPersonsExportType.Active : BankPersonsExportType.Expiring);
        }

        public bool IsAllPersonsExport
        {
            get { return _isAll; }
            set
            {
                if (_isAll != value)
                {
                    _isAll = value;
                    OnPropertyChanged("IsAllPersonsExport");
                    OnPropertyChanged("IsExpiredPersonsExport");
                    OnPropertyChanged("IsActivePersonsExport");
                }
            }
        }

        public bool IsActivePersonsExport
        {
            get { return _isActive; }
            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                    OnPropertyChanged("IsActivePersonsExport");
                    OnPropertyChanged("IsExpiredPersonsExport");
                    OnPropertyChanged("IsAllPersonsExport");
                }
            }
        }

        public bool IsExpiredPersonsExport
        {
            get { return _isExpired; }
            set
            {
                if (_isExpired != value)
                {
                    _isExpired = value;
                    OnPropertyChanged("IsExpiredPersonsExport");
                    OnPropertyChanged("IsAllPersonsExport");
                    OnPropertyChanged("IsActivePersonsExport");
                }
            }
        }
    }
}
