﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using Application = System.Windows.Application;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class ImportIntegrationFilesDlgViewModel: ViewModelCardDialog
    {
        public enum IntegrationImportType
        {
            OneS = 0,
            Pkip
        }
        
        public bool IsButtonEnabled { get; set; }

        public string ProgressText { get; set; }

        public bool IsProgressEnabled => !IsButtonEnabled;

        public string TitleText { get; set; }

        private readonly string _folderPath;
        public ImportIntegrationFilesDlgViewModel(IntegrationImportType type, string folderPath)
        {
            _folderPath = folderPath;
            switch (type)
            {
                case IntegrationImportType.OneS:
                    TitleText = "Импорт XML файлов 1С";
                    break;
            }
            OnPropertyChanged("TitleText");
            UploadFinished += ImportIntegrationFilesDlgViewModel_UploadFinished;
        }

        void ImportIntegrationFilesDlgViewModel_UploadFinished(object sender, EventArgs e)
        {
            if ((bool) sender)
                UpdateProgressText("Файлы успешно загружены!");
            else UpdateProgressText("ОШИБКА ЗАГРУЗКИ ФАЙЛОВ!");
            IsButtonEnabled = true;
            OnPropertyChanged("IsButtonEnabled");
            OnPropertyChanged("IsProgressEnabled");            
        }


        public event EventHandler UploadFinished;

        private void OnUploadFinished(bool value)
        {
            if (UploadFinished != null)
                UploadFinished(value, null);
        }

        public int TotalCount { get; set; }
        public int CurrentCount { get; set; }

        

        public void UploadFiles()
        {
            UpdateProgressText(string.Format("Ищем файлы в указанной папке..."));

            ThreadPool.QueueUserWorkItem(x =>
            {
                try
                {
                    TotalCount = 0;
                    CurrentCount = 0;
                    var list = Directory.EnumerateFiles(_folderPath, "*.xml")
                        .Select(b => new ImportFileListItem
                        {
                            FilePath = b,
                            FileContent = GZipCompressor.Compress(ExcelTools.SafeReadFile(b)),
                            IsCompressed = true
                        })
                        .ToList();
                    TotalCount = list.Count;
                    //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => OnUploadProgressChanged(string.Format("Загружено {0}/{1}", CurrentCount, TotalCount))));

                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action) (() =>
                        UpdateProgressText($"Импортируем {list.Count} файлов(а) на сервис для дальнейшей обработки...")));

                    try
                    {
                        var movePath = Path.Combine(_folderPath, "Загруженные");
                        if (!Directory.Exists(movePath))
                            Directory.CreateDirectory(movePath);

                        list.ForEach(a =>
                        {
                            var destFile = Path.Combine(movePath, Path.GetFileName(a.FilePath));
                            if (File.Exists(destFile))
                                File.Delete(destFile);
                            File.Move(a.FilePath, destFile);
                        });
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLine("Ошибка перемещения импортируемых файлов 1С на стороне клиента!");
                        Logger.WriteException(ex);
                    }
                    var result = BLServiceSystem.Client.StartOnesImport(_folderPath, list);
                    if (result == WebServiceDataError.SessionInProgress)
                        DialogHelper.ShowExclamation("На сервере уже запущена сессия импорта 1С! Дождитесь окончания текущей сессии и повторите попытку.", "Внимание");
                    else if (result == WebServiceDataError.DirectoryNotFound)
                        DialogHelper.ShowExclamation("На сервере не найдена папка импорта файлов 1С! Проверьте правильность настройки путей импорта/экспорта.", "Внимание");
                    else
                    {
                        Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => OnUploadFinished(true)));
                        return;
                    }

                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() => OnUploadFinished(false)));
                }
                catch (Exception ex)
                {
                    Logger.WriteException(ex);
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, (Action) (() => OnUploadFinished(false)));
                }
            });            


        }

        private void UpdateProgressText(string message)
        {
            ProgressText = message;
            OnPropertyChanged("ProgressText");
        }
    }
}
