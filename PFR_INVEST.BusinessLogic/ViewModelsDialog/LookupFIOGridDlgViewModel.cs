﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OARRS_manager, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class LookupFIOGridDlgViewModel : ViewModelListDialog
    {
        private readonly List<FIO> _cachedList = new List<FIO>();

        public LookupFIOGridDlgViewModel()
        {
            _cachedList = DataContainerFacade.GetList<FIO>().ToList();
            _list = new List<FIO>(_cachedList);
        }

        private List<FIO> _list;
        public List<FIO> List
        {
            get
            {
                RaiseDataRefreshing();
                return _list;
            }
            set
            {
                _list = value;
                OnPropertyChanged("List");
            }
        }

        private string _searchString;
        public string Search
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                ExecuteRefreshList(null);
            }

        }

        private FIO _selectedFIO;

        public FIO SelectedFIO
        {
            get { return _selectedFIO; }
            set
            {
                _selectedFIO = value;
                OnPropertyChanged("SelectedPerson");
                OnPropertyChanged("OkButtonEnabled");
            }
        }

        public bool OkButtonEnabled => SelectedFIO != null;

        protected override void ExecuteRefreshList(object param)
        {
            List = !string.IsNullOrEmpty(_searchString)
                       ? _cachedList.Where(item => item.Name.ToLower().Contains(_searchString.ToLower())).ToList()
                       : _cachedList;
        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }
    }
}
