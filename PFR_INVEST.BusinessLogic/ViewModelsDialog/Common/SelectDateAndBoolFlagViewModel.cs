﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Common
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.User)]
    public class SelectDateAndBoolFlagViewModel : ViewModelCardDialog
    {
        string _DateLabel;

        public string DateLabel
        {
            get { return _DateLabel; }

            set { SetPropertyValue(nameof(DateLabel), ref _DateLabel, value); }
        }

        DateTime? _DateValue;

        public DateTime? DateValue
        {
            get { return _DateValue; }

            set
            {
                if (SetPropertyValue(nameof(DateValue), ref _DateValue, value))
                    OnPropertyChanged(nameof(IsValid));
            }
        }

        string _FlagLabel;

        public string FlagLabel
        {
            get { return _FlagLabel; }

            set { SetPropertyValue(nameof(FlagLabel), ref _FlagLabel, value); }
        }

        bool _FlagValue;

        public bool FlagValue
        {
            get { return _FlagValue; }

            set { SetPropertyValue(nameof(FlagValue), ref _FlagValue, value); }
        }

        bool _AllowSelectDate;

        public bool AllowSelectDate
        {
            get { return _AllowSelectDate; }

            set { SetPropertyValue(nameof(AllowSelectDate), ref _AllowSelectDate, value); }
        }

        bool _AllowSelectFlag;

        public bool AllowSelectFlag
        {
            get { return _AllowSelectFlag; }

            set { SetPropertyValue(nameof(AllowSelectFlag), ref _AllowSelectFlag, value); }
        }

        private bool _isValid;

        public bool IsValid => !AllowSelectDate || (AllowSelectDate && DateValue.HasValue);

        public SelectDateAndBoolFlagViewModel(bool allowSelectDate = false, bool allowSelectFlag = true, string flagLabel = "Отображать старые наименования", string dateLabel = "На дату")
        {
            if (allowSelectDate)
            {
                AllowSelectDate = allowSelectDate;
                DateValue = DateTime.Now.Date;
                DateLabel = dateLabel;
            }
            if (allowSelectFlag)
            {
                AllowSelectFlag = allowSelectFlag;
                FlagLabel = flagLabel;
            }

        }
    }
}
