﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.XMLModels;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic
{

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class BanksVerificationViewModel : ViewModelCard
    {
        private readonly bool _isBankVerification;
        private readonly DepClaimSelectParams _activeAuction;

        public string Title => _isBankVerification ? "Введите дату формирования заключения" : "Введите дату формирования лимитов";

        public BanksVerificationViewModel(bool isBankVerification, DepClaimSelectParams auction = null)
        {
            _isBankVerification = isBankVerification;
            _activeAuction = auction;
            ID = 0;
        }

        protected override void ExecuteSave()
        {
            ExecuteAction(_activeAuction, _isBankVerification, _date, _authorizedPerson, _deputy, _performer, FilePath);
            if(!IsBankVerification)
                JournalLogger.LogModelEvent(this, JournalEventType.EXPORT_DATA, "Расчет лимитов", $"Aукцион ID:{_activeAuction?.ID} ");
        }

        public static void ExecuteAction(DepClaimSelectParams auction, bool isBankVerification, DateTime date, Person authorizedPerson, Person deputy, Person performer, string filePath, bool makeVisible = true)
        {
            var last = BLServiceSystem.Client.GetLastBankDepositImpExpData(auction.SelectDate, auction.LocalNum, isBankVerification ? 6 : 7, 0, 0);
            BanksVerificationDataContainer dc;
            if (makeVisible && last != null &&
                // TODO: неудачное решение хранить совместно DDATE+TTIME и REPOSITORY - при каждом экспорте принудительно загружается весь прошлый отчет, что может быть лишним, если треубуется формирование нового отчета при наличии старого
                DialogHelper.ShowConfirmation(
                    $@"В хранилище найден запрашиваемый вами документ, который был экспортирован {last.DDate:dd.MM.yyyy} {last.TTime}!
Загрузить документ из хранилища?
В случае нажатия ""Да"" - загрузится документ из хранилища (рекомендуется), в случае нажатия ""Нет"" - сформируется новый документ!"))
            {
                try
                {
                    BusyHelper.Start("Загрузка из хранилища");
                    dc = BanksVerificationDataContainer.FromXML(Encoding.UTF8.GetString(GZipCompressor.Decompress(last.Repository)));
                }
                finally
                {
                    BusyHelper.Stop();
                }
            }
            else
            {
                try
                {
                    BusyHelper.Start("Загрузка из БД");
                    dc = new BanksVerificationDataContainer(true, true, auction.StockId);
                    BusyHelper.Stop();
                }
                catch
                {
                    BusyHelper.Stop();
                    throw;
                }
            }

            if (isBankVerification)
            {
                var fname = Path.Combine(filePath ?? "", "Заключение о результатах проверки соответствия кредитной организации требованиям к кредитным организациям.doc");
                var p = new PrintBanksVerification(dc, date, authorizedPerson, deputy, performer, auction.SelectDate.Date >= new DateTime(2016,11,12).Date, fname);
                if (!makeVisible) p.IsVisibleAfterGeneration = false;
                try
                {
                    BusyHelper.Start();
                    p.Print();
                    if (!dc.IsArchiveData)
                        SaveExportRecord(auction, isBankVerification, dc);
                    BusyHelper.Stop();
                }
                catch (Exception)
                {
                    BusyHelper.Stop();
                    if (!dc.IsArchiveData)
                        SaveExportRecord(auction, isBankVerification, dc, false);
                    throw;
                }
            }
            else
            {
                if (!dc.IsArchiveData)
                    dc.LoadBankLimitData();
                foreach (var b in dc.Banks.Where(a => !PrintBanksVerification.IsKOValidBank(a, date, dc.LegalEntityRatings, dc.MultiplierRatings)).ToList())
                    dc.Banks.Remove(b);
                dc.Banks = dc.Banks.OrderBy(a => a.NumberForDocuments ?? int.MaxValue).ThenBy(a => a.ShortName).ToList();

                //var p = new PrintBanksLimits(banks, ratingAgencies, _date, _authorizedPerson, _performer, _activeAuction.SelectDate, legalEntityRatings, ratings, depclaims, multiplierRatings, deposits);
                var p = new PrintBanksLimits(dc, date, authorizedPerson, performer, auction);
                var fileName = $"Расчет лимитов для КО.xls".AttachUniqueTimeStamp(true);
                p.Save(Path.Combine(filePath ?? "", fileName));

                fileName = $"Cведения о лимитах на.doc";
                var ls = new PrintBanksLimitsSummary(Path.Combine(filePath ?? "", fileName), dc.Banks, date, authorizedPerson, performer, auction, dc.Depclaims, deputy);
                if (!makeVisible) ls.IsVisibleAfterGeneration = false;
                ls.Print(filePath);


                //лимиты по каждому банку отдельно
                var dirPath = Path.Combine(filePath ?? "", "subReports");

                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);

                var count = 0;
                foreach (var b in dc.Banks)
                {
                    var bl = new PrintBanksLimits(new List<LegalEntity> { b }, date, authorizedPerson, performer, auction, dc.Depclaims, ++count);
                    fileName = $"Расчет лимитов для КО {b.FormalizedName}.xls".AttachUniqueTimeStamp(true);
                    bl.Save(Path.Combine(dirPath, fileName));
                }

                //ProcessSubBanks(banks, ratingAgencies, legalEntityRatings, ratings, depclaims, multiplierRatings, deposits);

                SaveXmlDocument(auction, filePath, date, dc.Banks, p.LimitOnValues, p.LimitInfoOnValues);
                if (!dc.IsArchiveData)
                    SaveExportRecord(auction, isBankVerification, dc);
            }
        }

		private static void SaveExportRecord(DepClaimSelectParams auction, bool isVer, BanksVerificationDataContainer dc, bool isSuccess = true)
		{
			var dt = DateTime.Now;
			var r = new RepositoryImpExpFile
			{
				AuctionDate = auction.SelectDate,
				AuctionLocalNum = auction.LocalNum,
				DDate = dt,
				ImpExp = 0, //экспорт
				Repository = GZipCompressor.Compress(Encoding.UTF8.GetBytes(dc.GetXML())),
				Status = isSuccess ? 0 : 1,
				TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
				UserName = AP.Provider.UserName,
			};
			if (isVer)
			{
                r.Key = (int)RepositoryImpExpFile.Keys.BankVerResolution;
				r.Operacia = "Заключение о результатах проверки";
				r.Comment = "Работа с ЦБ и депозитами - Депозиты - Экспорт - Заключение о результатах проверки";
			}
			else
			{
                r.Key = (int)RepositoryImpExpFile.Keys.BankVerLimits;
			    r.Operacia = "Сформировать лимиты";
				r.Comment = "Работа с ЦБ и депозитами - Депозиты - Экспорт - Сформировать лимиты";
			}

			DataContainerFacade.Save(r);
			//Util.DecompressAndOpenXml(r.Repository);
		}

        //private void ProcessSubBanks(IList<LegalEntity> banks, List<RatingAgency> ratingAgencies, IList<LegalEntityRating> legalEntityRatings, IList<Rating> ratings, IList<DepClaim2> depclaims, IList<MultiplierRating> multiplierRatings, IList<BankDepositInfoItem> deposits)
        //{
        //    var dirPath = Path.Combine(FilePath, "subReports");

        //    if (!Directory.Exists(dirPath))
        //        Directory.CreateDirectory(dirPath);
        //    foreach (LegalEntity bank in banks)
        //    {
        //        var leRatingLinks = legalEntityRatings.Where(item => item.LegalEntityID == bank.ID);
        //        var multRatings = multiplierRatings.Where(x => leRatingLinks.Any(l => l.MultiplierRatingID == x.ID)).ToList();
        //        var pbl = new PrintBanksLimits(new List<LegalEntity> { bank }, ratingAgencies, _date, _authorizedPerson, _performer,
        //                             _activeAuction.SelectDate, legalEntityRatings.Where(p => p.LegalEntityID == bank.ID).ToList(),
        //                             ratings.Where(p => multRatings//.Where(lr => lr.LegalEntityID == bank.ID)
        //                                 .Select(i => i.RatingID).Contains(p.ID)).ToList(),
        //                             depclaims.Where(p => p.BankID == bank.ID).ToList(),
        //                             multRatings,
        //                             deposits.Where(x => x.BankID == bank.ID).ToList());

        //        string fileName = string.Format("Расчет лимитов для {0} {1}.xls", ReplaceInvalidFileNameChars(bank.ShortName), Date.ToString("dd.MM.yyyy"));
        //        pbl.Save(Path.Combine(dirPath, fileName));
        //    }
        //}

        private static void SaveXmlDocument(DepClaimSelectParams auction, string filePath, DateTime date, IList<LegalEntity> banks, Dictionary<long, decimal> limitOnValues, Dictionary<long, decimal> limitInfoOnValues)
        {
            var doc = new DocumentPDX01
            {            
                Info = new DocumentInfo
                {
                    DateValue = DateTime.Now.Date,
                    TimeValue = DateTime.Now.TimeOfDay
                },
                Body = new PDX01Body
                {
                    Version = "1.0",
                    Tab = new PDX01Tab
                    {
                        LimitDateValue = auction.SelectDate,
                        //SecurityId = string.Format("PFDEP_T{0}DT", _activeAuction.Period.ToString("D3"))
                        SecurityId = auction != null ? auction.SecurityId : string.Empty
                    }
                }
            };
            var records = new PDX01Record[banks.Count];
            records.Initialize();

            var bankList = banks.OrderBy(a => a.NumberForDocuments ?? int.MaxValue).ThenBy(a => a.ShortName).ToList();

            for (var i = 0; i < bankList.Count; i++)
            {
                var bank = bankList[i];
                var r = new PDX01Record();
                records[i] = r;

                //r.FirmId = bank.StockCode;
                var stock = bank.GetStockCodes().FirstOrDefault(b => b.StockID == auction.StockId);

                r.FirmId = !string.IsNullOrWhiteSpace(stock?.StockCode) ? stock.StockCode : string.Empty;
                r.Limit = limitOnValues.ContainsKey(bank.ID) ? (long)limitOnValues[bank.ID] : 0;
                r.ShortName = bank.FormalizedName;
                r.LimitInfo = limitInfoOnValues.ContainsKey(bank.ID) ? (long)limitInfoOnValues[bank.ID] : 0;
                //r.LimitInfo = (decimal)bank.Money;
            }

            doc.Body.Tab.Records = records;

            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var serializer = new XmlSerializer(typeof(DocumentPDX01));

            using (XmlWriter writer = new XmlTextWriter(BuildXmlFilePath(filePath, date), Encoding.GetEncoding("windows-1251")))
            {
                serializer.Serialize(writer, doc, ns);
            }
        }

        private static string BuildXmlFilePath(string filePath, DateTime date)
        {
            var number = 1;
            string xmlFilePath;
            do
            {
                xmlFilePath = $"{filePath}/PDX01_{number:000}_{date.ToString("ddMMyy")}.xml";
                number++;
            } while (File.Exists(xmlFilePath));
            return xmlFilePath;
        }

        private DateTime _date = DateTime.Now;
        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date != value)
                {
                    _date = value;
                    OnPropertyChanged("Date");
                }
            }
        }

        private Person _performer;
        public Person Performer
        {
            get { return _performer; }
            set
            {
                if (_performer != value)
                {
                    _performer = value;
                    OnPropertyChanged("Performer");                   
                }
            }
        }

        private Person _authorizedPerson;
        public Person AuthorizedPerson
        {
            get { return _authorizedPerson; }
            set
            {
                if (_authorizedPerson != value)
                {
                    _authorizedPerson = value;
                    OnPropertyChanged("AuthorizedPerson");                  
                }
            }
        }

        private Person _deputy;
        public Person Deputy
        {
            get { return _deputy; }
            set
            {
                if (_deputy != value)
                {
                    _deputy = value;
                    OnPropertyChanged("Deputy");
                   
                }
            }
        }

        

       

       

        public override bool CanExecuteSave()
        {
            return (_isBankVerification || string.IsNullOrEmpty(this["FilePath"]));
        }

        public override string this[string columnName]
        {
            get
            {
                if (columnName.Equals("FilePath"))
                {
                    if (string.IsNullOrEmpty(FilePath)) return "Выберите директорию для сохранения";
                }
                return string.Empty;
            }
        }

        private string _filePath;
        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged("FilePath");
            }
        }

        public bool IsBankVerification => !_isBankVerification;
    }
}
