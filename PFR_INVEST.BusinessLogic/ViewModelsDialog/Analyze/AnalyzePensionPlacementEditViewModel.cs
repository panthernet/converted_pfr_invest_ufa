﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class PensionPlacementEditViewModel : AnalyzeEditorDialogBase<AnalyzePensionplacementReport>
    {
        #region Properties/Fields
        private AnalyzePensionplacementReport _report;

        //private bool hasChanges;

        public string FormTitle => "Форма 5. Структура размещения средств пенсионных накоплений в финансовые инструменты";

        private RangeObservableCollection<AnalyzePensionplacementData> _dataItemsCache;
        public RangeObservableCollection<AnalyzePensionplacementData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzePensionplacementData>());

        private List<SubjectSPN> _subjectsSPN;
        private List<ActivesKind> _activesKinds;

        private RangeObservableCollection<AnalyzePensionplacementData> _GUKExData;
        private RangeObservableCollection<AnalyzePensionplacementData> _GUKValData;
        private RangeObservableCollection<AnalyzePensionplacementData> _GUKUrgData;
        private RangeObservableCollection<AnalyzePensionplacementData> _GUKResData;
        private RangeObservableCollection<AnalyzePensionplacementData> _CHUKData;
        private RangeObservableCollection<AnalyzePensionplacementData> _NPFData;

        public RangeObservableCollection<AnalyzePensionplacementData> GUKExData => _GUKExData ?? (_GUKExData = new RangeObservableCollection<AnalyzePensionplacementData>());
        public RangeObservableCollection<AnalyzePensionplacementData> GUKValData => _GUKValData ?? (_GUKValData = new RangeObservableCollection<AnalyzePensionplacementData>());
        public RangeObservableCollection<AnalyzePensionplacementData> GUKUrgData => _GUKUrgData ?? (_GUKUrgData = new RangeObservableCollection<AnalyzePensionplacementData>());
        public RangeObservableCollection<AnalyzePensionplacementData> GUKResData => _GUKResData ?? (_GUKResData = new RangeObservableCollection<AnalyzePensionplacementData>());
        public RangeObservableCollection<AnalyzePensionplacementData> CHUKData => _CHUKData ?? (_CHUKData = new RangeObservableCollection<AnalyzePensionplacementData>());
        public RangeObservableCollection<AnalyzePensionplacementData> NPFData => _NPFData ?? (_NPFData = new RangeObservableCollection<AnalyzePensionplacementData>());

        //public AnalyzePensionPlacementReportListViewModel ArchiveReports { get; set; }
        
        #endregion


        public PensionPlacementEditViewModel(AnalyzePensionplacementReport report, RibbonStateBL state, ViewModelState vmState, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {
            IsArchiveMode = isArchiveMode;

            IsNewReport = isNewReport;
            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");

            _report = isNewReport ? QuarkReportBase.InitNewReport(new AnalyzePensionplacementReport()) : report;

            if (!IsNewReport)
            {
                SelectedYear = _report.Year;
                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }
            _subjectsSPN = new List<SubjectSPN>(DataContainerFacade.GetList<SubjectSPN>().Where(s => s.Fondtype == 0 || s.Fondtype == 1));
            _activesKinds = new List<ActivesKind>(DataContainerFacade.GetList<ActivesKind>());

            if (isNewReport)
            {
                InitNewReportData();
                LoadReportData(new List<AnalyzePensionplacementData>());
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetPensionplacementDataByReportId(_report.Id));
            }
            if (!isArchiveMode)
            {
                ArchiveReports = new AnalyzePensionPlacementReportListViewModel() { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }
        }

        void InitNewReportData()
        {
            var tmp = new List<AnalyzePensionplacementData>();


            foreach (var s in _subjectsSPN)
            {
                foreach (var ak in _activesKinds)
                {
                    var newDataItem = new AnalyzePensionplacementData
                    {
                        ActivesKindId = ak.Id,
                        ActivesKindName = ak.Name,
                        ActOrderId = ak.OrderId,
                        SubjectId = s.Id
                    };
                    tmp.Add(newDataItem);
                }
            }


            FillDataSources(tmp);
        }

        void LoadReportData(List<AnalyzePensionplacementData> savedItems)
        {
            var notSavedItems = new List<AnalyzePensionplacementData>();



            foreach (var s in _subjectsSPN)
            {
                foreach (var ak in _activesKinds)
                {
                    if (savedItems.Any(n => n.ActivesKindId == ak.Id && n.SubjectId == s.Id))
                        continue;

                    notSavedItems.Add(
                        new AnalyzePensionplacementData
                        {
                            ActivesKindId = ak.Id,
                            ActivesKindName = ak.Name,
                            ActOrderId = ak.OrderId,
                            SubjectId = s.Id,
                            ReportId = _report.Id,
                            SubjectTypeFond = (SubjectSPN.FondType)s.Fondtype
                        });
                }
            }


            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }

        void FillDataSources(List<AnalyzePensionplacementData> dataItems)
        {
            DataItemsCache.Fill(dataItems.ToList());
            SetReadOnlyState(DataItemsCache);

            GUKExData.Fill(
             dataItems.Where(
                d =>
                    d.SubjectId ==
                    _subjectsSPN.First(
                        s =>
                            s.Name.Equals("ГУК расширенный инвестиционный портфель",
                                StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
            );

            GUKResData.Fill(
             dataItems.Where(
                d =>
                    d.SubjectId ==
                    _subjectsSPN.First(
                        s =>
                            s.Name.Equals("ГУК портфель выплатного резерва",
                                StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
            );

            GUKUrgData.Fill(
                dataItems.Where(
                    d =>
                        d.SubjectId ==
                        _subjectsSPN.First(
                            s =>
                                s.Name.Equals("ГУК портфель срочных пенсионных выплат",
                                    StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
                                    );
            GUKValData.Fill(
                dataItems.Where(
                    d =>
                        d.SubjectId ==
                        _subjectsSPN.First(
                            s =>
                                s.Name.Equals("ГУК инвестиционный портфель государственных ценных бумаг",
                                    StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
                                    );
            CHUKData.Fill(
                dataItems.Where(
                    d =>
                        d.SubjectId ==
                        _subjectsSPN.First(s => s.Name.Equals("ЧУК", StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
                        );


            NPFData.Fill(dataItems.Where(d => d.SubjectId == _subjectsSPN.First(s => s.Name.Equals("НПФ", StringComparison.CurrentCultureIgnoreCase)).Id).ToList().ConvertAll<AnalyzePensionplacementData>(p => new AnalyzePensionplacementData(p)).OrderBy(d => d.ActOrderId)
                );
        }

        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = SelectedKvartal.Item1;
                _report.Year = SelectedYear ?? _curYear;
                _report.Id = DataContainerFacade.Save<AnalyzePensionplacementReport>(_report);
            }

            foreach (var d in GUKExData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);
            foreach (var d in GUKResData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);
            foreach (var d in GUKUrgData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);
            foreach (var d in GUKValData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);
            foreach (var d in CHUKData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);
            foreach (var d in NPFData)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionplacementData>(d);

            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzePensionplacementReport>(_report);
                IsDataChanged = false;
            }

        }

        bool isChangedValue(AnalyzePensionplacementData chIt)
        {
            if (!chIt.Total.HasValue)
                return false;

            if (chIt.Id == 0)
            {
                chIt.ReportId = _report.Id;
                return true;
            }

            var oldItem = DataItemsCache.FirstOrDefault(i => i.ActivesKindId == chIt.ActivesKindId &&
                                                              i.SubjectId == chIt.SubjectId);

            if (oldItem == null)
            {
                chIt.ReportId = _report.Id;
                return IsDataChanged = true;
            }

            if (oldItem.Total != chIt.Total)
            {
                chIt.ReportId = _report.Id;
                //oldItem.ReportId = chIt.ReportId = _report.Id;
                //oldItem.Total = chIt.Total;
                return IsDataChanged = true;
            }

            return false;
        }
        protected override List<AnalyzePensionplacementReport> LoadReports()
        {
            return DataContainerFacade.GetClient().GetPensionplacementReportByYearKvartal();
        }

        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(AnalyzePensionPlacementReportListViewModel)) as AnalyzePensionPlacementReportListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1);

        }
        protected override AnalyzePensionplacementReport GetCurrentReport()
        {
            return _report;
        }
        protected override void ReportDataResetID()
        {
            GUKExData.ForEach(i => i.Id = 0);
            GUKResData.ForEach(i => i.Id = 0);
            GUKUrgData.ForEach(i => i.Id = 0);
            GUKValData.ForEach(i => i.Id = 0);
            CHUKData.ForEach(i => i.Id = 0);
            NPFData.ForEach(i => i.Id = 0);
        }

        /* Изменение данных в других разделах не происходит */
        protected override void ObtainAllReportData(AnalyzePensionplacementReport report)
        {
            ;
        }

        /* End */

        protected override bool CheckHaveChanges()
        {
            return IsDataChanged = GUKExData.Any(d => isChangedValue(d)) ||
                GUKResData.Any(d => isChangedValue(d)) ||
                GUKUrgData.Any(d => isChangedValue(d)) ||
                GUKValData.Any(d => isChangedValue(d)) ||
                CHUKData.Any(d => isChangedValue(d)) ||
                NPFData.Any(d => isChangedValue(d));
        }
        protected override bool HasErrors()
        {
            return false;
        }
    }

}
