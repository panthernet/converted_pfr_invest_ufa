﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class AnalyzeInsuredpersonEditViewModel : AnalyzeEditorDialogBase<AnalyzeInsuredpersonReport>
    {
        #region Properties/Fields
        private AnalyzeInsuredpersonReport _report;
        public override string FormTitle => "Форма 4. Количество застрахованных лиц в системе обязательного пенсионного страхования, нарастающим итогом";

        private RangeObservableCollection<AnalyzeInsuredpersonData> _dataItemsCache;
        public RangeObservableCollection<AnalyzeInsuredpersonData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzeInsuredpersonData>());

        private List<SubjectSPN> _subjectsSPN;

        private RangeObservableCollection<AnalyzeInsuredpersonData> _EditData;
        public RangeObservableCollection<AnalyzeInsuredpersonData> EditData => _EditData ?? (_EditData = new RangeObservableCollection<AnalyzeInsuredpersonData>());

        

        #endregion
        
        public AnalyzeInsuredpersonEditViewModel(AnalyzeInsuredpersonReport report, RibbonStateBL state, ViewModelState vmState, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {
            IsArchiveMode = isArchiveMode;

            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");


            _report = isNewReport ? QuarkReportBase.InitNewReport(new AnalyzeInsuredpersonReport()) : report;


            if (!IsNewReport)
            {
                SelectedYear = _report.Year;
                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }

            var _avPortfByRibGr = new List<KeyValuePair<RibbonStateBL, string[]>>()
            {
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.BackOfficeState, new [] {"ГУК расширенный инвестиционный портфель", "ГУК инвестиционный портфель государственных ценных бумаг", "ЧУК"}),
                new KeyValuePair<RibbonStateBL, string[]>(RibbonStateBL.NPFState, new [] {"НПФ"}),
            };

            _subjectsSPN = new List<SubjectSPN>();
            _subjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => s.RibbonGroups.Split(',').Contains(state.ToString())));

            var availPortfolios = _avPortfByRibGr.Any(x => x.Key == state) ? _avPortfByRibGr.First(x => x.Key == state).Value : null;

            if (availPortfolios != null)
                _subjectsSPN.RemoveAll(d => !availPortfolios.Contains(d.Name, StringComparer.CurrentCultureIgnoreCase));


            if (isNewReport)
            {

                InitNewReportData();
                LoadReportData(new List<AnalyzeInsuredpersonData>());
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetInsuredpersonDataByReportId(_report.Id).Where(d => _subjectsSPN.Any(s => d.SubjectId == s.Id)).ToList());
            }

            if (!isArchiveMode)
            {
                ArchiveReports = new AnalyzeInsuredpersonListViewModel() { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }

        }

        void InitNewReportData()
        {
            var tmp = new List<AnalyzeInsuredpersonData>();

            foreach (var s in _subjectsSPN)
            {
                var newDataItem = new AnalyzeInsuredpersonData
                {
                    SubjectId = s.Id,
                    SubjectName = s.Name,
                    SubjOrderId = s.OrderId
                };
                tmp.Add(newDataItem);

            }
            FillDataSources(tmp);
        }

        void LoadReportData(List<AnalyzeInsuredpersonData> savedItems)
        {
            var notSavedItems = new List<AnalyzeInsuredpersonData>();

            foreach (var s in _subjectsSPN)
            {
                if (savedItems.Any(n => n.SubjectId == s.Id))
                    continue;

                notSavedItems.Add(
                    new AnalyzeInsuredpersonData
                    {
                        SubjectId = s.Id,
                        SubjectName = s.Name,
                        ReportId = _report.Id,
                        SubjectTypeFond = (SubjectSPN.FondType)s.Fondtype,
                        SubjectGroup = s.GroupId,
                        SubjOrderId = s.OrderId
                    });

            }
            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }

        

        void FillDataSources(List<AnalyzeInsuredpersonData> dataItems)
        {
            DataItemsCache.Fill(dataItems.OrderBy(d => d.SubjOrderId).ToList());
            SetReadOnlyState(DataItemsCache);
            EditData.Fill(dataItems.ConvertAll<AnalyzeInsuredpersonData>(p => new AnalyzeInsuredpersonData(p)).OrderBy(d => d.SubjOrderId));

        }

        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = SelectedKvartal.Item1;
                _report.Year = SelectedYear ?? _curYear;
                _report.Id = DataContainerFacade.Save<AnalyzeInsuredpersonReport>(_report);
            }

            _dataForSave.AddRange(EditData);
            foreach (var d in _dataForSave)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzeInsuredpersonData>(d);

            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzeInsuredpersonReport>(_report);
                IsDataChanged = false;
            }

        }


        bool isChangedValue(AnalyzeInsuredpersonData chIt)
        {

            if (!chIt.Total.HasValue)
                return false;

            if (chIt.Id == 0)
            {
                chIt.ReportId = _report.Id;
                return true;
            }

            var oldItem = DataItemsCache.FirstOrDefault(i => i.SubjectId == chIt.SubjectId);

            if (oldItem == null)
            {
                chIt.ReportId = _report.Id;
                return IsDataChanged = true;
            }

            if (oldItem.Total != chIt.Total)
            {
                oldItem.ReportId = chIt.ReportId = _report.Id;
                //oldItem.Total = chIt.Total;
                return IsDataChanged = true;
            }

            return false;
        }
        protected override List<AnalyzeInsuredpersonReport> LoadReports()
        {
            return DataContainerFacade.GetClient().GetInsuredpersonReportsByYearKvartal();
        }
        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(AnalyzeInsuredpersonListViewModel)) as AnalyzeInsuredpersonListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1);

        }
        protected override AnalyzeInsuredpersonReport GetCurrentReport()
        {
            return _report;
        }

        protected override void ReportDataResetID()
        {
            EditData.ForEach(i => i.Id = 0);
            _dataForSave.ForEach(i => i.Id = 0);
        }

        /* Загрузка данных по другим разделам для сохранения в новом отчете при сохранении истории */
        private List<AnalyzeInsuredpersonData> _dataForSave = new List<AnalyzeInsuredpersonData>();
        protected override void ObtainAllReportData(AnalyzeInsuredpersonReport report)
        {
            var data = DataContainerFacade.GetClient().GetInsuredpersonDataByReportId(report.Id);
            foreach (var d in data)
            {
                if (!EditData.Any(e => e.SubjectId == d.SubjectId))
                    _dataForSave.Add(d);
            }
            
        }

        /* End */
       
        protected override bool CheckHaveChanges()
        {
            return EditData.Any(d =>
            {
                return _dataItemsCache.Any(x => x.Id == d.Id && x.Total != d.Total);
            });
        }

        protected override bool HasErrors()
        {
            var result = _EditData.Any(i => !i.Validate());
            return result && _EditData.Any(i => i.HasError);
        }
    }
}
