﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker)]
    public class AnalyzeIncomingstatementsEditViewModel : AnalyzeEditorDialogBase<AnalyzeIncomingstatementsReport>
    {
        #region Properties/Fields
        private AnalyzeIncomingstatementsReport _report;
        public override string FormTitle => "Форма 6. Принятые заявления застрахованных лиц по формированию средств пенсионных накоплений с разбивкой по способу подачи";

        private RangeObservableCollection<AnalyzeIncomingstatementsData> _dataItemsCache;
        public RangeObservableCollection<AnalyzeIncomingstatementsData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzeIncomingstatementsData>());

        private RangeObservableCollection<IncomingstatementEditorItem> _terPersEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> TerPersEditItems => _terPersEditItems ?? (_terPersEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _terPostEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> TerPostEditItems => _terPostEditItems ?? (_terPostEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _terDeliverEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> TerDeliverEditItems => _terDeliverEditItems ?? (_terDeliverEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        //private RangeObservableCollection<IncomingstatementEditorItem> _terOtherEditItems;
        //public RangeObservableCollection<IncomingstatementEditorItem> TerOtherEditItems => _terOtherEditItems ?? (_terOtherEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _epguOtherEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> EPGUEditItems => _epguOtherEditItems ?? (_epguOtherEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _persCabEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> PersCabEditItems => _persCabEditItems ?? (_persCabEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _elektEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> ElektEditItems => _elektEditItems ?? (_elektEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private RangeObservableCollection<IncomingstatementEditorItem> _mfcEditItems;
        public RangeObservableCollection<IncomingstatementEditorItem> MFCEditItems => _mfcEditItems ?? (_mfcEditItems = new RangeObservableCollection<IncomingstatementEditorItem>());

        private List<FilingType> _filingTypes;
        private List<StatementType> _statementTypes;

        //private string[] threeSubStatements = new[] { "о переходе из ПФР в НПФ", "о переходе из НПФ в ПФР", "о переходе из НПФ в НПФ" };

      

        #endregion


        public AnalyzeIncomingstatementsEditViewModel(AnalyzeIncomingstatementsReport report, RibbonStateBL state, ViewModelState vmState, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {
            IsArchiveMode = isArchiveMode;

            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");

            _report = isNewReport ? QuarkReportBase.InitNewReport(new AnalyzeIncomingstatementsReport()) : report;

            if (!IsNewReport)
            {
                SelectedYear = _report.Year;
                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }

            _filingTypes = new List<FilingType>(DataContainerFacade.GetList<FilingType>());
            _statementTypes = new List<StatementType>(DataContainerFacade.GetList<StatementType>());

            if (isNewReport)
            {
                InitNewReportData();
                //  LoadReportData(new List<AnalyzeIncomingstatementsData>());
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetIncomingstatementsDataByReportId(_report.Id));
            }
            if (!isArchiveMode)
            {
                ArchiveReports = new AnalyzeIncomingstatementsListViewModel() { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }
        }

        void InitNewReportData()
        {
            var tmp = new List<AnalyzeIncomingstatementsData>();

            foreach (var f in _filingTypes)
            {

                foreach (var s in _statementTypes)
                {

                    foreach (var ch in Enum.GetValues(typeof(ChangeFundKind)).Cast<int>())
                    {

                        foreach (var sf in Enum.GetValues(typeof(FilingSubtypes)).Cast<int>())
                        {
                            var newItm = new AnalyzeIncomingstatementsData
                            {
                                FilingId = f.Id,
                                FilingName = f.Name,
                                StatementId = s.Id,
                                StatementName = s.Name,
                                Changetype = ch,
                                FilingSubtype = sf
                            };
                            tmp.Add(newItm);
                        }
                    }

                }

            }

            FillDataSources(tmp);
        }
       
        void LoadReportData(List<AnalyzeIncomingstatementsData> savedItems)
        {
            var notSavedItems = new List<AnalyzeIncomingstatementsData>();

            foreach (var f in _filingTypes)
            {
                foreach (var s in _statementTypes)
                {

                    foreach (var ch in Enum.GetValues(typeof(ChangeFundKind)).Cast<int>())
                    {
                        {

                            foreach (var sf in Enum.GetValues(typeof(FilingSubtypes)).Cast<int>())
                            {
                                if (
                                    savedItems.Any(
                                        n =>
                                            n.FilingId == f.Id && n.StatementId == s.Id && n.Changetype == ch &&
                                            n.FilingSubtype == sf))
                                    continue;

                                var newItm = new AnalyzeIncomingstatementsData
                                {
                                    FilingId = f.Id,
                                    FilingName = f.Name,
                                    StatementId = s.Id,
                                    StatementName = s.Name,
                                    Changetype = ch,
                                    FilingSubtype = sf
                                };
                                notSavedItems.Add(newItm);
                            }


                        }
                    }
                }
            }

            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }

        void FillDataSources(List<AnalyzeIncomingstatementsData> dataItems)
        {
            DataItemsCache.Fill(dataItems.ToList());

            SetReadOnlyState(DataItemsCache);

            TerPersEditItems.Fill(
                convertLocalGovToItemsDataList("территориальными органами пфр", (int)FilingSubtypes.Лично,
                    dataItems).OrderBy(d => d.StatementName));

            TerPostEditItems.Fill(
                convertLocalGovToItemsDataList("территориальными органами пфр", (int)FilingSubtypes.Почтой,
                    dataItems).OrderBy(d => d.StatementName));

            TerDeliverEditItems.Fill(
                convertLocalGovToItemsDataList("территориальными органами пфр", (int)FilingSubtypes.Курьером,
                    dataItems).OrderBy(d => d.StatementName));

            //TerOtherEditItems.Fill(
            //    convertToItemsDataList("территориальными органами пфр иным способом",
            //        dataItems).OrderBy(d => d.StatementName));
            EPGUEditItems.Fill(
                convertToItemsDataList("ЕПГУ",
                    dataItems).OrderBy(d => d.StatementName));
            PersCabEditItems.Fill(
                convertToItemsDataList("Личный кабинет",
                    dataItems).OrderBy(d => d.StatementName));
            ElektEditItems.Fill(
                convertToItemsDataList("В электронном виде",
                    dataItems).OrderBy(d => d.StatementName));
            MFCEditItems.Fill(
                convertToItemsDataList("МФЦ",
                    dataItems).OrderBy(d => d.StatementName));
        }

        List<IncomingstatementEditorItem> convertToItemsDataList(string filingName,
            List<AnalyzeIncomingstatementsData> dataItems)
        {
            List<IncomingstatementEditorItem> tmp = new List<IncomingstatementEditorItem>();
            var grouped =
                dataItems.Where(
                    d =>
                        d.FilingName.Equals(filingName,
                            StringComparison.CurrentCultureIgnoreCase)).GroupBy(d => d.StatementId);
            foreach (var gr in grouped)
            {
                var srItm = gr.FirstOrDefault(i => i.Changetype == 0);//срочные
                var uvItm = gr.FirstOrDefault(i => i.Changetype == 1);//уведомление
                var dosrItm = gr.FirstOrDefault(i => i.Changetype == 2);//досрочные

                tmp.Add(new IncomingstatementEditorItem(gr.Key, srItm.StatementName, srItm, uvItm, dosrItm));
            }
            return tmp;
        }

        List<IncomingstatementEditorItem> convertLocalGovToItemsDataList(string filingName, int? filingSubtype,
            List<AnalyzeIncomingstatementsData> dataItems)
        {
            List<IncomingstatementEditorItem> tmp = new List<IncomingstatementEditorItem>();
            var grouped =
                dataItems.Where(d =>
                    (!d.FilingSubtype.HasValue || d.FilingSubtype == filingSubtype) &&
                    d.FilingName.Equals(filingName, StringComparison.CurrentCultureIgnoreCase)
                    ).GroupBy(d => d.StatementId);
            foreach (var gr in grouped)
            {
                var srItm = gr.FirstOrDefault(i => i.Changetype == 0 && i.FilingSubtype == filingSubtype);//срочные - только одно поле ввода на форме редактирования
                var uvItm = gr.FirstOrDefault(i => i.Changetype == 1 && i.FilingSubtype == filingSubtype);//уведомление
                var dosrItm = gr.FirstOrDefault(i => i.Changetype == 2 && i.FilingSubtype == filingSubtype);//досрочные
                //if (srItm != null)
                //    srItm.FilingSubtype = filingSubtype;
                //if (uvItm != null)
                //    uvItm.FilingSubtype = filingSubtype;
                //if (dosrItm != null)
                //    dosrItm.FilingSubtype = filingSubtype;

                tmp.Add(new IncomingstatementEditorItem(gr.Key, srItm.StatementName, srItm, uvItm, dosrItm, filingSubtype) );
            }
            return tmp;
        }

        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = SelectedKvartal.Item1;
                _report.Year = SelectedYear ?? _curYear;
                _report.Id = DataContainerFacade.Save<AnalyzeIncomingstatementsReport>(_report);
            }

            foreach (var d in TerPersEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            foreach (var d in TerPostEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }
            foreach (var d in TerDeliverEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            //foreach (var d in TerOtherEditItems)
            //{
            //    d.ReportId = _report.Id;
            //    d.SaveItems();
            //}

            foreach (var d in EPGUEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            foreach (var d in PersCabEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            foreach (var d in ElektEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            foreach (var d in MFCEditItems)
            {
                d.ReportId = _report.Id;
                d.SaveItems();
            }

            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzeIncomingstatementsReport>(_report);
                IsDataChanged = false;
            }
        }

        protected override AnalyzeIncomingstatementsReport GetCurrentReport()
        {
            return _report;
        }

        //protected override AnalyzeIncomingstatementsReport InitNewFilledReport()
        //{
        //    var r = new AnalyzeIncomingstatementsReport() { Year = SelectedYear.Value, Kvartal = SelectedKvartal.Item1, CreateDate = DateTime.Now, UpdateDate = DateTime.Now };

        //}

        protected override List<AnalyzeIncomingstatementsReport> LoadReports()
        {
            return DataContainerFacade.GetClient().GetIncomingReportsByYearKvartal();
            //return DataContainerFacade.GetList<AnalyzeIncomingstatementsReport>();
        }

        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = GetViewModel(typeof(AnalyzeIncomingstatementsListViewModel)) as AnalyzeIncomingstatementsListViewModel;
            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1);

        }
        protected override void ReportDataResetID()
        {
            TerPersEditItems.ForEach(i => i.ReportDateResetID());
            TerPostEditItems.ForEach(i => i.ReportDateResetID());
            TerDeliverEditItems.ForEach(i => i.ReportDateResetID());
            EPGUEditItems.ForEach(i => i.ReportDateResetID());
            PersCabEditItems.ForEach(i => i.ReportDateResetID());
            ElektEditItems.ForEach(i => i.ReportDateResetID());
            MFCEditItems.ForEach(i => i.ReportDateResetID());
        }

        /* Для отчета нет данных по другим разделам */

        protected override void ObtainAllReportData(AnalyzeIncomingstatementsReport report)
        {
            ;
        }

        /* End */

        protected override bool CheckHaveChanges()
        {
            return IsDataChanged = TerPersEditItems.Any(i => i.GetHasChanges()) ||
              TerPostEditItems.Any(i => i.GetHasChanges()) ||
              TerDeliverEditItems.Any(i => i.GetHasChanges()) ||
              EPGUEditItems.Any(i => i.GetHasChanges()) ||
              PersCabEditItems.Any(i => i.GetHasChanges()) ||
              ElektEditItems.Any(i => i.GetHasChanges()) ||
              MFCEditItems.Any(i => i.GetHasChanges());
        }

        protected override bool HasErrors()
        {
            return false;
        }
    }
}
