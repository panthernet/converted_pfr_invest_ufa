﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using System.Linq;
using PFR_INVEST.BusinessLogic.Collections;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects.Analyze;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
    public class AnalyzePensionfundtonpfEditViewModel : AnalyzeEditorDialogBase<AnalyzePensionfundtonpfReport>
    {
        #region Properties/Fields
        private AnalyzePensionfundtonpfReport _report;
        public override string FormTitle => WithSubtract ? "Форма 3. Средства пенсионных накоплений, переданные в управляющие компании и НПФ за вычетом возврата в ПФР "
                    : "Форма 2. Средства пенсионных накоплений, переданные в управляющие компании и НПФ без учета возврата в ПФР ";

        private RangeObservableCollection<AnalyzePensionfundtonpfData> _dataItemsCache;
        public RangeObservableCollection<AnalyzePensionfundtonpfData> DataItemsCache => _dataItemsCache ?? (_dataItemsCache = new RangeObservableCollection<AnalyzePensionfundtonpfData>());

        private List<SubjectSPN> _subjectsSPN;

        private RangeObservableCollection<AnalyzePensionfundtonpfData> _EditData;

        public RangeObservableCollection<AnalyzePensionfundtonpfData> EditData => _EditData ?? (_EditData = new RangeObservableCollection<AnalyzePensionfundtonpfData>());

        public bool WithSubtract { get; set; }


        #endregion

        public AnalyzePensionfundtonpfEditViewModel(AnalyzePensionfundtonpfReport report, RibbonStateBL state, ViewModelState vmState, bool withSubtract = false, bool isNewReport = false, bool isArchiveMode = false) : base(state, vmState, isNewReport)
        {

            IsArchiveMode = isArchiveMode;

            if (!isNewReport && report == null)
                throw new NullReferenceException("Переданный параметр report является null");
            if (!isNewReport && report.Id == 0)
                throw new ArgumentException("Не правильно вызван конструктор редактирования, ");

            WithSubtract = withSubtract;
            _report = isNewReport ? QuarkReportBase.InitNewReport(new AnalyzePensionfundtonpfReport { Subtraction = WithSubtract ? 1 : 0 }) : report;

            InitDataList(); //Загрузка возможных вариантов кварталов

            Dictionary<RibbonStateBL, string[]> _avPortfByRibGr = new Dictionary<RibbonStateBL, string[]>();
            _avPortfByRibGr.Add(RibbonStateBL.SIState, new[] { "ГУК расширенный инвестиционный портфель", "ГУК инвестиционный портфель государственных ценных бумаг", "ЧУК" });
            _avPortfByRibGr.Add(RibbonStateBL.NPFState, new[] { "НПФ" });

            if (!IsNewReport)
            {
                SelectedYear = _report.Year;

                SelectedKvartal = Kvartals.FirstOrDefault(k => k.Item1 == _report.Kvartal);
            }

            _subjectsSPN = new List<SubjectSPN>();
            _subjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => s.RibbonGroups.Split(',').Contains(state.ToString())));

            if (_avPortfByRibGr.ContainsKey(state))
                _subjectsSPN.RemoveAll(d => !_avPortfByRibGr[state].Contains(d.Name, StringComparer.CurrentCultureIgnoreCase));

            if (isNewReport)
            {
                InitNewReportData();
                LoadReportData(new List<AnalyzePensionfundtonpfData>());
            }
            else
            {
                LoadReportData(DataContainerFacade.GetClient().GetPensionfundDataByReportId(_report.Id).Where(r => _subjectsSPN.Select(s => s.Id).Contains(r.SubjectId)).ToList());
            }
            if (!isArchiveMode)
            {
                ArchiveReports = withSubtract ? new AnalyzePensionfundtonpfWithReturnListViewModel() { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal }
                : new AnalyzePensionfundtonpfListViewModel { IsArchiveMode = true, Year = _report.Year, Quark = _report.Kvartal };
                ArchiveReports.RefreshList.Execute(null);
            }
        }

        void InitNewReportData()
        {
            var tmp = new List<AnalyzePensionfundtonpfData>();

            foreach (var s in _subjectsSPN)
            {

                var newDataItem = new AnalyzePensionfundtonpfData
                {
                    SubjectId = s.Id,
                    SubjectName = s.Name,
                    SubjOrderId = s.OrderId
                };
                tmp.Add(newDataItem);

            }

            FillDataSources(tmp);
        }

        void LoadReportData(List<AnalyzePensionfundtonpfData> savedItems)
        {
            var notSavedItems = new List<AnalyzePensionfundtonpfData>();

            //Данные для вкладки ПФР

            foreach (var s in _subjectsSPN)
            {

                if (savedItems.Any(n => n.SubjectId == s.Id))
                    continue;

                notSavedItems.Add(
                    new AnalyzePensionfundtonpfData
                    {
                        SubjectId = s.Id,
                        SubjectName = s.Name,
                        ReportId = _report.Id,
                        SubjOrderId = s.OrderId,
                        SubjectTypeFond = (SubjectSPN.FondType)s.Fondtype,
                    });

            }
            savedItems.AddRange(notSavedItems);
            FillDataSources(savedItems);
        }


        void FillDataSources(List<AnalyzePensionfundtonpfData> dataItems)
        {
            DataItemsCache.Fill(dataItems.ToList());
            SetReadOnlyState(DataItemsCache);
            EditData.Fill(dataItems.ConvertAll<AnalyzePensionfundtonpfData>(p => new AnalyzePensionfundtonpfData(p)).OrderBy(d => d.SubjOrderId));
        }

        protected override bool CheckExistSavedReport()
        {

            bool existSavedReport = false;
            existSavedReport = DataContainerFacade.GetList<AnalyzePensionfundtonpfReport>().Any(
                        r => r.Status == 0 &&
                            r.Subtraction == _report.Subtraction &&
                            r.Year == SelectedYear &&
                            (r as QuarkReportBase).Kvartal == SelectedKvartal.Item1);

            return existSavedReport;
        }
        protected override void ExecuteSaveAction()
        {
            if (IsNewReport)
            {
                _report.Kvartal = SelectedKvartal.Item1;
                _report.Year = SelectedYear ?? _curYear;
                _report.Subtraction = WithSubtract ? 1 : 0;
                _report.Id = DataContainerFacade.Save<AnalyzePensionfundtonpfReport>(_report);
            }
            _dataForSave.AddRange(EditData);
            foreach (var d in _dataForSave)
                if (isChangedValue(d))
                    DataContainerFacade.Save<AnalyzePensionfundtonpfData>(d);

            if (!IsNewReport && IsDataChanged)
            {
                _report.UpdateDate = DateTime.Now;
                DataContainerFacade.Save<AnalyzePensionfundtonpfReport>(_report);
                IsDataChanged = false;
            }

        }

        bool isChangedValue(AnalyzePensionfundtonpfData chIt)
        {
            if (!chIt.Total.HasValue)
                return false;

            if (chIt.Id == 0)
            {
                chIt.ReportId = _report.Id;
                return true;
            }

            var oldItem = DataItemsCache.FirstOrDefault(i => i.SubjectId == chIt.SubjectId);

            if (oldItem == null)
            {
                chIt.ReportId = _report.Id;
                return IsDataChanged = true;
            }

            if (oldItem.Total != chIt.Total)
            {
                chIt.ReportId = _report.Id;
                //oldItem.ReportId = chIt.ReportId = _report.Id;
                //oldItem.Total = chIt.Total;
                return IsDataChanged = true;
            }

            return false;
        }

        protected override List<AnalyzePensionfundtonpfReport> LoadReports()
        {
            return DataContainerFacade.GetClient().GetPensionfundReportsByYearKvartal(subtraction: WithSubtract ? 1 : 0);
        }

        protected override void RefreshConnectedCards()
        {
            if (GetViewModel == null) return;
            var rVM = WithSubtract ?
                    (GetViewModel(typeof(AnalyzePensionfundtonpfWithReturnListViewModel)) as
                        AnalyzePensionfundtonpfWithReturnListViewModel) :
                        (GetViewModel(typeof(AnalyzePensionfundtonpfListViewModel)) as
                        AnalyzePensionfundtonpfListViewModel);

            if (rVM == null) return;
            rVM.RefreshList.Execute(null);
            rVM.Current = rVM.List.FirstOrDefault(d => d.Year == SelectedYear && d.Kvartal == SelectedKvartal.Item1);

        }
        protected override AnalyzePensionfundtonpfReport GetCurrentReport()
        {
            return _report;
        }
        protected override void ReportDataResetID()
        {
            EditData.ForEach(i => i.Id = 0);
            _dataForSave.ForEach(i => i.Id = 0);
        }

        /* Загрузка данных по другим разделам для сохранения в новом отчете при сохранении истории */
        private List<AnalyzePensionfundtonpfData> _dataForSave = new List<AnalyzePensionfundtonpfData>();
        protected override void ObtainAllReportData(AnalyzePensionfundtonpfReport report)
        {
            var data = DataContainerFacade.GetClient().GetPensionfundDataByReportId(report.Id);
            foreach (var d in data)
            {
                if (!EditData.Any(e => e.SubjectId == d.SubjectId))
                    _dataForSave.Add(d);
            }
        }

        /* End */

        protected override bool CheckHaveChanges()
        {
            return EditData.Any(d =>
            {
                return _dataItemsCache.Any(x => x.Id == d.Id && x.Total != d.Total);
            });
        }
        protected override bool HasErrors()
        {
            return false;
        }
    }
}
