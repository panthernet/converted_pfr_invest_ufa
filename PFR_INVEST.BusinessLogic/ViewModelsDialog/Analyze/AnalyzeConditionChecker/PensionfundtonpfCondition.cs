﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public class PensionfundtonpfCondition : AnalyzeConditionViewModelBase
    {
        private List<SubjectSPN> _availableSubjectsSPN = new List<SubjectSPN>();

        private int _subtParam;

        public PensionfundtonpfCondition(bool withSubt = false)
        {
            _subtParam = withSubt ? 1 : 0;
        }

        protected override void PrepareDataOptions()
        {
            //var spns = DataContainerFacade.GetList<SubjectSPN>().ToList();
            //var myRoles = AP.Provider.CurrentUserSecurity.GetRoles();
            var myRoles = CurrentUserRoles ?? AP.Provider.CurrentUserSecurity.GetRoles();

            List<int> allowedFounds = new List<int>();

            if (myRoles.Contains(((DOKIP_ROLE_TYPE)4)))
            {
                allowedFounds.AddRange(new[] { 0, 1 });
            }
            else if (myRoles.Contains(((DOKIP_ROLE_TYPE)32)))
            {
                allowedFounds.Add(0);
            }
            else if (myRoles.Contains(((DOKIP_ROLE_TYPE)8192)))
            {
                allowedFounds.Add(1);
            }

            //if (AP.Provider.CurrentUserSecurity.CheckUserInAnyRole((DOKIP_ROLE_TYPE)4))
            //{
            //    allowedFounds.AddRange(new[] { 0, 1 });
            //}
            //else if (AP.Provider.CurrentUserSecurity.CheckUserInAnyRole((DOKIP_ROLE_TYPE)32))
            //{
            //    allowedFounds.Add(0);
            //}
            //else if (AP.Provider.CurrentUserSecurity.CheckUserInAnyRole((DOKIP_ROLE_TYPE)8192))
            //{
            //    allowedFounds.Add(1);
            //}
            _availableSubjectsSPN.Clear();
            _availableSubjectsSPN.AddRange(DataContainerFacade.GetList<SubjectSPN>().Where(s => allowedFounds.Contains(s.Fondtype)));


            _normalDataCount = _availableSubjectsSPN.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(DataContainerFacade.GetClient()
                .GetPensionfundDataByYearKvartal(DateTime.Now.Year, null, _subtParam));

            result.AddRange(DataContainerFacade.GetClient().GetPensionfundDataByYearKvartal(DateTime.Now.Year - 1, 4, _subtParam));//данные для оповещения о не закрытом отчете 4 кв предыдущего года
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.Where(d => _availableSubjectsSPN.Any(s => s.Id == (d as AnalyzePensionfundtonpfData).SubjectId)).ToList();
            //данные для оповещения о не закрытом отчете 4 кв предыдущего года
        }
    }

    public class PensionfundtonpfWithSubtractionCondition : PensionfundtonpfCondition
    {
        public PensionfundtonpfWithSubtractionCondition() : base(true)
        {

        }
    }
}
