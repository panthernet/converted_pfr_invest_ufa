﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public abstract class AnalyzeConditionViewModelBase
    {
        protected int _curQuark = Helper.DueHelper.GetAddSPNQuarter(DateTime.Now.Month);
        protected List<Quark> _quarks = new List<Quark>(DataContainerFacade.GetList<Quark>());
        protected List<AnalyzeReportDataBase> _savedData = new List<AnalyzeReportDataBase>();
        protected int _normalDataCount;

        //св-во исп-ся для тестирования 
        public List<DOKIP_ROLE_TYPE> CurrentUserRoles { get; set; }

        /// <summary>
        /// Определение даты начала показа оповещений для квартальных отчетов
        /// </summary>
        /// <param name="date">Дата первого дня квартала</param>
        /// <param name="condition"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public bool CheckAlertQuarkDate(ref DateTime date, AnalyzeCondition condition, Quark q)
        {
            date = condition.AlertSign.Equals("+")// "+" оповещение после окончания периода (Y/Q)
                ? (
                    q.LastMonth != 12 //если не 4й квартал то дата отсчитывается с первого дня след.квартала
                        ? new DateTime(date.Year, q.LastMonth + 1, 1).AddDays(condition.AlertForDays)
                        : //первый день след.квартала + кол-во дней из правила
                        new DateTime(date.Year, 12, 31)) //для 4го квартала оповещать с 31 декабря
                : (q.LastMonth < 12 // "-" оповещение до окончания перида периода
                    ? new DateTime(date.Year, q.LastMonth + 1, 1).AddDays(condition.AlertForDays * -1) //для 1-3 кварталов
                                                                                                       //вычитаем кол-во дней от след.месяца за кварталом
                    : new DateTime(date.Year + 1, 1, 1).AddDays(condition.AlertForDays * -1));

            return DateTime.Compare(date, DateTime.Now) <= 0;
        }

        /// <summary>
        /// Определение даты начала показа оповещений для годовых отчетов
        /// </summary>
        /// <param name="date"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public bool CheckAlertYearDate(ref DateTime date, AnalyzeCondition condition)
        {
            date = condition.AlertSign.Equals("+")
                ? new DateTime(date.Year + 1, 1, 1).AddDays(condition.AlertForDays) // в след.году через n дней с начала года
                                                                                    //первый день след.года + кол-во дней из правила
                : new DateTime(date.Year + 1, 1, 1).AddDays(condition.AlertForDays * -1); //в текущем году за n дней до конца года

            return DateTime.Compare(date, DateTime.Now) <= 0;
        }

        /// <summary>
        /// Настройка параметров загрузки данных VM
        /// </summary>
        protected abstract void PrepareDataOptions();

        /// <summary>
        /// Получение данных с БД
        /// </summary>
        public abstract IEnumerable<AnalyzeReportDataBase> LoadDataFromDB();

        /// <summary>
        /// Применение фильтров к данным отчетов (вынесено отдельно для тестов)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public abstract List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data);

       

        /// <summary>
        /// Проверка правила и наполнение сообщениями emptyMessageList
        /// </summary>
        /// <param name="emptyMessageList"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public virtual bool CheckCondition(List<LoginMessageItem> emptyMessageList, AnalyzeCondition condition)
        {
            PrepareDataOptions();
            _savedData = ApplyFilters(LoadDataFromDB());

            bool result = false;
            //Dictionary<int, List<int>> mr = new Dictionary<int, List<int>>();
            foreach (var year in new[] { DateTime.Now.Year - 1, DateTime.Now.Year })
            {
                //Проверка заполненности годовых отчетов
                if (condition.RepeatInterval.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (_savedData.Count(d => d.Year == year) < _normalDataCount)
                    {
                        DateTime alertDate = new DateTime(year, 1, 1);
                        if (CheckAlertYearDate(ref alertDate, condition))

                            emptyMessageList.Add(new LoginMessageItem()
                            {
                                Key = $"LoginMessage.AnalyzeConditionId:{condition.Id}.{condition.Id}",
                                Message = $"{condition.MessageText} за {year} г.",
                                Title = condition.MessageHeader,
                                Order = 1
                            });
                        result = true;
                    }

                    if (year == DateTime.Now.Year)//возврат результата для годового отчета
                        return result;

                    continue; //продолжение цикла перебора года отчета
                }

                //Начало проверки заполнения квартальных отчетов
                var maxQuark = year < DateTime.Now.Year ? 4 : year > DateTime.Now.Year ? 1 : _curQuark;
                var minQuark = year < DateTime.Now.Year ? 4 : 1;

                var mq = new List<string>();
                for (int i = minQuark; i <= maxQuark; i++)
                {
                    //Сравнение кол-ва сохраненных значений и кол-ва требуемым значений (должно быть вычислено в PrepareDataOptions())
                    if (_savedData.Count(d => d.Kvartal == i && d.Year == year) < _normalDataCount)
                    {
                        var qrk = _quarks.First(q => q.Index == i);
                        DateTime alertDate = new DateTime(year, qrk.FirstMonth, 1);

                        if (CheckAlertQuarkDate(ref alertDate, condition, qrk))
                            mq.Add(qrk.Name);

                        result = true;
                    }
                }

                if (mq.Count > 0)
                {
                    var kvartals = new StringBuilder();
                    ;
                    mq.ForEach(m =>
                    {
                        kvartals.Append(m.Replace("Квартал ", ""));
                        kvartals.Append(",");
                    });

                    kvartals.Remove(kvartals.Length - 1, 1); //удаление запятой в конце перечисления у
                    kvartals.Append(" кв.");
                    emptyMessageList.Add(new LoginMessageItem()
                    {
                        Key = $"LoginMessage.AnalyzeConditionId:{condition.Id}.{condition.Id}",
                        Message = $"{condition.MessageText} за {kvartals.ToString()} {year} г.",
                        Title = condition.MessageHeader,
                        Order = 1
                    });
                }
            }


            return result; //возврат результата для квартального отчета
        }
    }
}