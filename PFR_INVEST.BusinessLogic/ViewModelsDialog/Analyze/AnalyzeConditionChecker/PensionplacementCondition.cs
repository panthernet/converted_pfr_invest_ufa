﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public class PensionplacementCondition : AnalyzeConditionViewModelBase
    {
        //List<AnalyzePensionplacementData> _savedData = new List<AnalyzePensionplacementData>();
        private List<ActivesKind> _activesKinds = new List<ActivesKind>();
        private List<SubjectSPN> _subjectsSPN = new List<SubjectSPN>();

        protected override void PrepareDataOptions()
        {
            //Типы фондов только ПФР/НПФ
            _subjectsSPN =
                new List<SubjectSPN>(
                    DataContainerFacade.GetList<SubjectSPN>().Where(s => s.Fondtype == 0 || s.Fondtype == 1));

            _activesKinds = new List<ActivesKind>(DataContainerFacade.GetList<ActivesKind>());
            _normalDataCount = _subjectsSPN.Count() * _activesKinds.Count();
        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(
               DataContainerFacade.GetClient().GetPensionplacementDataByYearKvartal(DateTime.Now.Year, null, false));
            result.AddRange(DataContainerFacade.GetClient()
                .GetPensionplacementDataByYearKvartal(DateTime.Now.Year - 1, 4, false));
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.ToList();
        }
    }
}
