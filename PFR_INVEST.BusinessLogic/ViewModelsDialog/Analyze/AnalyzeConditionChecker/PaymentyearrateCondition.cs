﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    public class PaymentyearrateCondition : AnalyzeConditionViewModelBase
    {
        private List<PaymentKind> _paymentKinds = new List<PaymentKind>();


        protected override void PrepareDataOptions()
        {
            var myRoles = CurrentUserRoles ?? AP.Provider.CurrentUserSecurity.GetRoles();
            var pkinds = DataContainerFacade.GetList<PaymentKind>().ToList();
            _paymentKinds.AddRange(
                pkinds.Where(s => myRoles.Any(r => ((DOKIP_ROLE_TYPE)s.UserRoleFlags).HasFlag(r))));

            _normalDataCount = _paymentKinds.Count();

        }

        public override IEnumerable<AnalyzeReportDataBase> LoadDataFromDB()
        {
            var result = new List<AnalyzeReportDataBase>();
            result.AddRange(DataContainerFacade.GetClient().GetPaymentyearrateDataByYearKvartal(DateTime.Now.Year,null,false));

            result.AddRange(DataContainerFacade.GetClient().GetPaymentyearrateDataByYearKvartal(DateTime.Now.Year - 1,4,false));//данные для оповещения о не закрытом отчете 4 кв предыдущего года
            return result;
        }
        public override List<AnalyzeReportDataBase> ApplyFilters(IEnumerable<AnalyzeReportDataBase> data)
        {
            return data.Where(d => _paymentKinds.Any(s => s.Id == (d as AnalyzePaymentyearrateData).PaymentkindId)).ToList();
            //данные для оповещения о не закрытом отчете 4 кв предыдущего года
        }
        //public override bool CheckCondition(List<LoginMessageItem> emptyMessageList, AnalyzeCondition condition)
        //{
        //    PrepareDataOptions();

        //    bool result = false;
        //    var i = 0;
        //    foreach (var year in new[] { DateTime.Now.Year - 1, DateTime.Now.Year })
        //    {
        //       // throw new Exception("Проверка не реализована.");
        //        //Сравнение кол-ва сохраненных значений и кол-ва требуемым значений
        //        if (_savedData.Count(d => d.Year == year && d.Kvartal == 0 && ((AnalyzePaymentyearrateData)d).Total.HasValue) < _normalDataCount)
        //        {

        //            DateTime alertDate = new DateTime(year, 1, 1);
        //            if (CheckAlertYearDate(ref alertDate, condition))
        //                emptyMessageList.Add(new LoginMessageItem()
        //                {
        //                    Key = $"LoginMessage.AnalyzeConditionId:{condition.Id}.{i++}",
        //                    Message = $"{condition.MessageText} за {year} г.",
        //                    Title = condition.MessageHeader,
        //                    Order = 1
        //                });
        //            result = true;
        //        }

        //    }
        //    return result;
        //}

    }

}
