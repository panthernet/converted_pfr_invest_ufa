﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects.Analyze;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog.Analyze.AnalyzeConditionChecker
{
    /// <summary>
    /// Коллекция классов правил, которые проверяются при запуске клиента
    /// </summary>
    public class AnalyzeConditionCollection : List<AnalyzeConditionViewModelBase>
    {
        public AnalyzeConditionViewModelBase this[string className]
        {
            get
            {
                return this.FirstOrDefault(x => x.GetType().Name.Equals(className, StringComparison.CurrentCultureIgnoreCase));
            }
        }

        public AnalyzeConditionCollection()
        {
            Add(new PensionfundtonpfCondition());
            Add(new PensionfundtonpfWithSubtractionCondition());
            Add(new PensionplacementCondition());
            Add(new IncomingstatementsCondition());
            Add(new InsuredpersonCondition());
            Add(new YieldfundsN107Condition());
            Add(new YieldfundsN140Condition());
            Add(new PaymentyearrateCondition());
        }

        public List<LoginMessageItem> CheckUnfilledReports(IEnumerable<DOKIP_ROLE_TYPE> roles)
        {
            var messages = new List<LoginMessageItem>();
            foreach (var r in roles)
                messages.AddRange(GetAnalyzeReportMessages(r));

            return messages;
        }

        private IEnumerable<LoginMessageItem> GetAnalyzeReportMessages(DOKIP_ROLE_TYPE role)
        {
            var result = new List<LoginMessageItem>();
            foreach (var con in DataContainerFacade.GetList<AnalyzeCondition>().Where(c => c.Status == 1 && (((DOKIP_ROLE_TYPE)c.UserRoleFlags).HasFlag(role) || role == DOKIP_ROLE_TYPE.Administrator)))
            {
                var checkClass = this[con.ClassName];
                if (checkClass != null)
                {
                    var conMessages = new List<LoginMessageItem>();
                    if (checkClass.CheckCondition(conMessages, con))
                        result.AddRange(conMessages);
                }
            }
            return result;
        }

    }
}

