﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class LegalEntityCertificateDlgViewModel : ViewModelBase
    {
        public LegalEntityCourierCertificate Certificate { get; private set; }
        public IList<Element> StockList { get; private set; }
        public DelegateCommand<object> SaveCard { get; private set; }

        public LegalEntityCertificateDlgViewModel(LegalEntityCourierCertificate cert)
        {

            this.Certificate = cert;
            this.SaveCard = new DelegateCommand<object>(o =>
            {
                return this.Certificate.Validate();
            }, o => { });

            this.StockList = DataContainerFacade.GetList<Element>().Where(a => a.Key == (long)Element.Types.StockType).OrderBy(s => s.ID).ToList();
            if (this.Certificate.StockID == null || this.Certificate.StockID == 0 && this.StockList.Count == 1)
                this.Certificate.StockID = this.StockList.First().ID;

            this.Certificate.PropertyChanged += (sender, e) => { this.IsDataChanged = true; };
        }

        public bool IsValid()
        {
            string fields = "Signature|ExpireDate|RegisterDate";
            foreach (string key in fields.Split('|'))
            {
                if (!String.IsNullOrEmpty(Certificate[key]))
                    return false;
            }
            return true;
        }

		

        public bool? AskSaveOnClose()
        {
            if (!IsValid())
            {
                bool b = DialogHelper.ShowConfirmation("Вы уверены, что хотите закрыть диалог и потерять все изменения?");
                return b ? (bool?)false : null;
            }

            if (IsDataChanged)
            {
                return DialogHelper.ConfirmLegalEntityCourierSaveOnClose();
            }
            return false;
        }

        public bool CanSaveOnClose()
        {
            bool b = IsValid();

            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }


    }
}
