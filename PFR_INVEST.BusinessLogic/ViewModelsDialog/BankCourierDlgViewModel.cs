﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Windows;
using PFR_INVEST.DataAccess.Client;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class BankCourierDlgViewModel : ViewModelBase//, ILoaListProvider
    {
        public LegalEntityCourier Item { get; private set; }
        public LegalEntityCourier OriginalItem { get; private set; }

        public DelegateCommand<object> SaveCard { get; private set; }
        private LegalEntityCourierLetterOfAttorney _LecItem { get; set; }

        private LegalEntityCourierLetterOfAttorney focusedLOA;
        public LegalEntityCourierLetterOfAttorney FocusedLOA
        {
            get
            {
                return focusedLOA;
            }
            set
            {
                focusedLOA = value;
            }
        }

        private LegalEntityCourierCertificate focusedCertificate;
        public LegalEntityCourierCertificate FocusedCertificate                                             
        {
            get
            {
                return focusedCertificate;
            }
            set
            {
                focusedCertificate = value;
            }
        }

        public BankCourierDlgViewModel(LegalEntityCourier item)
        {
            OriginalItem = item;
            Item = new LegalEntityCourier();
            Item.ApplyValues(OriginalItem);
            //Item.LoaListProvider = this;

            _LecItem = GetItemLECLA(item);
            _OldNumber = Item.LetterOfAttorneyNumber;
            _OldIssueDate = Item.LetterOfAttorneyIssueDate;
            _OldExpireDate = Item.LetterOfAttorneyExpireDate;
            _OldRegisterDate = Item.LetterOfAttorneyRegisterDate;

            this.Item.PropertyChanged += (sender, e) => { this.IsDataChanged = true; };

            this.SaveCard = new DelegateCommand<object>(o =>
            { return CanExecuteSave(); }, o => { ExecuteSave(); });

            this.AddLOA = new DelegateCommand(o => CanExecuteAddLOA(), o => ExecuteAddLOA());
            this.DeleteLOA = new DelegateCommand(o => CanExecuteDeleteLOA(), o => ExecuteDeleteLOA());
            this.AddCertificate = new DelegateCommand(o => CanExecuteAddCertificate(), o => ExecuteAddCertificate());
            this.DeleteCertificate = new DelegateCommand(o => CanExecuteDeleteCertificate(), o => ExecuteDeleteCertificate());
        }


        private string _OldNumber;
        private DateTime? _OldIssueDate;
        private DateTime? _OldExpireDate;
        private DateTime? _OldRegisterDate;

        public ICommand AddLOA { get; private set; }
        public ICommand AddCertificate { get; private set; }        
        public ICommand DeleteLOA { get; private set; }
        public ICommand DeleteCertificate { get; private set; }

        private bool CanExecuteSave()
        {
            return IsValid() && this.IsDataChanged;
        }

        private void ExecuteSave()
        {
            if (!IsValid())
                return;
        }


        private bool IsValid()
        {
            return Item.IsValid();
        }

        private bool IsInvalidLength(string value, int maxLength)
        {
            return string.IsNullOrEmpty(value) || value.Length > maxLength;
        }

        private bool IsInvalidLengthNullable(string value, int maxLength)
        {
            return !string.IsNullOrEmpty(value) && value.Length > maxLength;
        }

        public bool IsReadOnly = false;

        public bool IsEnabled => !EditAccessDenied;

        public override bool EditAccessDenied
        {
            get
            {
                if (IsReadOnly)
                    return true;

                return base.EditAccessDenied;
            }
        }

        public Visibility ButtonsVisibility => IsReadOnly ? Visibility.Collapsed : Visibility.Visible;


        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyList
        {
            get
            {
                if (Item.LetterOfAttorneyList == null)//Load List
                {
                    Item.LetterOfAttorneyList = DataContainerFacade.GetListByProperty<LegalEntityCourierLetterOfAttorney>("LegalEntityCourierID", Item.ID).Where(l => l.StatusID != -1).ToList();
                }
                return Item.LetterOfAttorneyList;
            }
            set
            {
                Item.LetterOfAttorneyList = value;
            }
        }

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListChanged
        {
            get
            {
                if (Item.LetterOfAttorneyListChanged == null)
                    Item.LetterOfAttorneyListChanged = new List<LegalEntityCourierLetterOfAttorney>();

                return Item.LetterOfAttorneyListChanged;
            }
            set
            {
                Item.LetterOfAttorneyListChanged = value;
            }
        }

        public virtual List<LegalEntityCourierLetterOfAttorney> LetterOfAttorneyListDeleted
        {
            get
            {
                if (Item.LetterOfAttorneyListDeleted == null)
                    Item.LetterOfAttorneyListDeleted = new List<LegalEntityCourierLetterOfAttorney>();

                return Item.LetterOfAttorneyListDeleted;
            }
            set
            {
                Item.LetterOfAttorneyListDeleted = value;
            }
        }

        public virtual List<LegalEntityCourierCertificate> CertificatesList
        {
            get
            {
                if (Item.CertificatesList == null)//Load List
                {
                    Item.CertificatesList = DataContainerFacade.GetListByProperty<LegalEntityCourierCertificate>("LegalEntityCourierID", Item.ID).Where(c => c.StatusID != -1).ToList();
                }
                return Item.CertificatesList;
            }
            set
            {
                Item.CertificatesList = value;
            }
        }

        public virtual List<LegalEntityCourierCertificate> CertificatesListChanged
        {
            get
            {
                if (Item.CertificatesListChanged == null)
                    Item.CertificatesListChanged = new List<LegalEntityCourierCertificate>();

                return Item.CertificatesListChanged;
            }
            set
            {
                Item.CertificatesListChanged = value;
            }
        }

        public virtual List<LegalEntityCourierCertificate> CertificatesListDeleted
        {
            get
            {
                if (Item.CertificatesListDeleted == null)
                    Item.CertificatesListDeleted = new List<LegalEntityCourierCertificate>();

                return Item.CertificatesListDeleted;
            }
            set
            {
                Item.CertificatesListDeleted = value;
            }
        }

		[Obsolete("Not used",true)]
        public virtual void DeleteFromDatabase()
        {
            DataContainerFacade.Delete<LegalEntityCourier>(Item.ID);
        }

        public bool? AskSaveOnClose()
        {
            if (!IsValid())
            {
                bool b = DialogHelper.ShowConfirmation("Вы уверены, что хотите закрыть диалог и потерять все изменения?");
                return b ? (bool?)false : null;
            }

            IsDataChanged = true;
            if (DataChanged())
            {
                return DialogHelper.ConfirmLegalEntityCourierSaveOnClose();
            }
            return false;
        }

        public bool DataChanged()
        {
            return IsDataChanged = !Item.Equals(OriginalItem);
            
        }

        public bool CanSaveOnClose()
        {
            bool b = IsValid();

            if (b) return true;

            DialogHelper.ShowAlert("Невозможно сохранить: некорректные значения полей");
            return false;
        }

        private LegalEntityCourierLetterOfAttorney GetItemLECLA(LegalEntityCourier item)
        {
            return item.GetItemLECLA();
        }

        private bool CanExecuteAddLOA()
        {
            return EditAccessAllowed;
        }

        public void EditLOA(LegalEntityCourierLetterOfAttorney x)
        {
            if (x == null)
                return;

            bool res = DialogHelper.EditCourierLetterOfAttorney(x, false);
            if (res)
            {
                if (!LetterOfAttorneyListChanged.Contains(x))
                    LetterOfAttorneyListChanged.Add(x);

                LetterOfAttorneyList = LetterOfAttorneyList.ToList();//Refresh display table
                OnPropertyChanged("LetterOfAttorneyList");
                IsDataChanged = true;
            }
        }

        private void ExecuteAddLOA()
        {
            var x = new LegalEntityCourierLetterOfAttorney() { };
            bool res = DialogHelper.EditCourierLetterOfAttorney(x, true);
            if (res)
            {
                LetterOfAttorneyList.Add(x);
                if (!LetterOfAttorneyListChanged.Contains(x))
                    LetterOfAttorneyListChanged.Add(x);

                LetterOfAttorneyList = LetterOfAttorneyList.ToList();//Refresh display table
                OnPropertyChanged("LetterOfAttorneyList");
                IsDataChanged = true;
            }
        }

        public bool CanExecuteDeleteLOA()
        {
            return FocusedLOA != null;
        }

        public void ExecuteDeleteLOA()
        {
            if (FocusedLOA == null)
                return;

            if (!DialogHelper.ShowConfirmation("Вы действительно хотите удалить доверенность?"))
            {
                return;
            }

            var x = FocusedLOA;
            //if (FocusedLOA.ID > 0)
            {
                LetterOfAttorneyListDeleted.Add(x);
            }

            LetterOfAttorneyList.Remove(FocusedLOA);
            LetterOfAttorneyList = LetterOfAttorneyList.ToList();//Refresh display table
            OnPropertyChanged("LetterOfAttorneyList"); 
            if (LetterOfAttorneyListChanged.Contains(x))
            {
                LetterOfAttorneyListChanged.Remove(x);
            }
            //FocusedLOA = null;
            IsDataChanged = true;
        }

        private bool CanExecuteAddCertificate()
        {
            return EditAccessAllowed;
        }

        public void EditCertificate(LegalEntityCourierCertificate x)
        {
            if (x == null)
                return;

            bool res = DialogHelper.EditCourierCertificate(x, false);
            if (res)
            {
                if (!CertificatesListChanged.Contains(x))
                    CertificatesListChanged.Add(x);

                CertificatesList = CertificatesList.ToList();//Refresh display table
                OnPropertyChanged("CertificatesList");
                IsDataChanged = true;
            }
        }

        private void ExecuteAddCertificate()
        {
            var x = new LegalEntityCourierCertificate() { };
            bool res = DialogHelper.EditCourierCertificate(x, true);
            if (res)
            {
                CertificatesList.Add(x);
                if (!CertificatesListChanged.Contains(x))
                    CertificatesListChanged.Add(x);

                CertificatesList = CertificatesList.ToList();//Refresh display table
                OnPropertyChanged("CertificatesList");
                IsDataChanged = true;
            }
        }

        public bool CanExecuteDeleteCertificate()
        {
            return FocusedCertificate != null;
        }

        public void ExecuteDeleteCertificate()
        {
            if (!DialogHelper.ShowConfirmation("Вы действительно хотите удалить сертификат?"))
            {
                return;
            }

            var x = FocusedCertificate;
            //if (FocusedCertificate.ID > 0)
            {
                CertificatesListDeleted.Add(x);
            }

            CertificatesList.Remove(x);
            CertificatesList = CertificatesList.ToList();//Refresh display table
            OnPropertyChanged("CertificatesList");
            if (CertificatesListChanged.Contains(x))
                CertificatesListChanged.Remove(x);
            //FocusedCertificate = null;
            IsDataChanged = true;
        }
    }
}
