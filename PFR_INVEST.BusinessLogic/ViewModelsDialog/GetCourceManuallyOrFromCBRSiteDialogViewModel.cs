﻿using System;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public class CantGetCourseFromCBRSiteEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public CantGetCourseFromCBRSiteEventArgs(string message)
        {
            Message = message;
        }
    }

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class GetCourceManuallyOrFromCBRSiteDialogViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public readonly Currency Currency;
        public readonly DateTime CourceDate;

        public EventHandler OnCourceEntered;
        protected void RaiseCourceEntered()
        {
            if (OnCourceEntered != null)
                OnCourceEntered(this, null);
        }

        public EventHandler OnStartedGettingCourseFromCBRSite;
        protected void RaiseStartedGettingCourseFromCBRSite()
        {
            if (OnStartedGettingCourseFromCBRSite != null)
                OnStartedGettingCourseFromCBRSite(this, null);
        }

        public EventHandler OnFinishedGettingCourseFromCBRSite;
        protected void RaiseFinishedGettingCourseFromCBRSite(bool success)
        {
            if (OnFinishedGettingCourseFromCBRSite != null)
                OnFinishedGettingCourseFromCBRSite(this, new CurseEventArgs() { ShowSuccess = success });
        }

        public delegate void CantGetCourseFromCBRSiteDelegate(object Sender, CantGetCourseFromCBRSiteEventArgs e);
        public CantGetCourseFromCBRSiteDelegate OnCantGetCourseFromCBRSite;
        protected void RaiseCantGetCourseFromCBRSite(string message)
        {
            if (OnCantGetCourseFromCBRSite != null)
                OnCantGetCourseFromCBRSite(this, new CantGetCourseFromCBRSiteEventArgs(message));
        }

        private decimal m_CourceRate;
        public decimal CourceRate
        {
            get
            {
                return m_CourceRate;
            }
            set
            {
                m_CourceRate = value;
                base.OnPropertyChanged("CourceRate");
            }
        }

        public ICommand GetCourceFromCBRSite { get; private set; }

        protected void ExecuteGetCourceFromCBRSite()
        {
            RaiseStartedGettingCourseFromCBRSite();
            try
            {
                CourceRate = CBRConnector.GetCourseRate(Currency.ID, CourceDate);
            }
            catch (Exception ex)
            {
                RaiseFinishedGettingCourseFromCBRSite(false);
                RaiseCantGetCourseFromCBRSite(ex.Message);
                return;
            }
            RaiseFinishedGettingCourseFromCBRSite(true);
        }

        protected override void ExecuteSave()
        {
            RaiseCourceEntered();
        }

        public override bool CanExecuteSave()
        {
            return Validate();
        }

        private const string INVALID_COURCE_RATE = "Неверно указан курс";

        private bool Validate()
        {
            return string.IsNullOrEmpty(this["CourceRate"]);
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "CourceRate":
                        return CourceRate <= 0 || CourceRate > 10000 ? INVALID_COURCE_RATE : null;
                    default:
                        return null;
                }
            }
        }

        public GetCourceManuallyOrFromCBRSiteDialogViewModel(long p_CurrencyID, DateTime p_CourceDate)
            : base(typeof(RatesListViewModel))
        {
            this.ID = p_CurrencyID;
            GetCourceFromCBRSite = new DelegateCommand(o => true,
                o => ExecuteGetCourceFromCBRSite());

            Currency = DataContainerFacade.GetByID<Currency>(p_CurrencyID);
            CourceDate = p_CourceDate;
        }
    }
}
