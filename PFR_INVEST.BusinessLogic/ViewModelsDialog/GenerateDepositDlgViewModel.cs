﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_directory_editor,
        DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OFPR_directory_editor,
        DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class GenerateDepositDlgViewModel : ViewModelCard
    {
        public GenerateDepositDlgViewModel()
        {
            Date = DateTime.Now;
        }

        public DateTime Date { get; set; }


        public static bool ExecuteAction(DateTime date)
        {
            var acceptedOfferList = BLServiceSystem.Client.GetDepClaimOfferListBySettleDate(new List<DepClaimOffer.Statuses> {DepClaimOffer.Statuses.Accepted}, date);

            if (acceptedOfferList.Count == 0)
            {
                DialogHelper.ShowAlert("На указанную дату акцептованные оферты не обнаружены");
                return false;
            }

            var aL = (from x in acceptedOfferList select x.Auction).Distinct().ToList();
            var failedOfferList =
                BLServiceSystem.Client.GetDepClaimOfferListBySettleDate(new List<DepClaimOffer.Statuses> {DepClaimOffer.Statuses.Signed, DepClaimOffer.Statuses.NotSigned}, date);
            var passedAuctionList = new List<DepClaimSelectParams>();
            var failedAuctionList = new List<DepClaimSelectParams>();

            foreach (var a in aL)
            {
                if (failedOfferList.Any(x => x.Auction.ID == a.ID))
                    failedAuctionList.Add(a);
                else
                    passedAuctionList.Add(a);
            }

            if (failedAuctionList.Count > 0)
                DialogHelper.ShowAlert(GetFailedAuctionListMessage(passedAuctionList, failedAuctionList));

            if (passedAuctionList.Count == 0)
                return false;

            var passedOfferList = acceptedOfferList.Where(p => passedAuctionList.Any(x => x.ID == p.Auction.ID));
            DialogHelper.ShowDepClaim2OfferAcceptedList(passedOfferList.Select(item => item.Offer), date);

            return true;
        }

        protected override void ExecuteSave()
        {
            ExecuteAction(Date);
        }

        private static string GetFailedAuctionListMessage(List<DepClaimSelectParams> passedAuctionList, List<DepClaimSelectParams> failedAuctionList)
        {
            var sb = new StringBuilder();

            //"Аукционы № не могут участвовать в формировании депозитов, необходима обработка всех оферт. Для формирования депозитов для аукционов № нажмите "ОК"

            sb.Append($"Аукцион{(failedAuctionList.Count > 1 ? "ы" : "")} ");
            var fL = from x in failedAuctionList orderby x.AuctionNum select "№" + x.AuctionNum;
            sb.Append(string.Join(", ", fL));
            sb.Append($" не {(failedAuctionList.Count > 1 ? "могут" : "может")} участвовать в формировании депозитов, необходима обработка всех оферт.");
            if (passedAuctionList.Count != 0)
            {
                sb.Append($"\r\nДля формирования депозитов для аукцион{(passedAuctionList.Count > 1 ? "ов" : "а")} ");
                var pL = from x in passedAuctionList orderby x.AuctionNum select "№" + x.AuctionNum ;
                sb.Append(string.Join(", ", pL));
                sb.Append(" нажмите \"ОК\".");
            }

            return sb.ToString();
        }

        public override bool CanExecuteSave()
        {
            return true;
        }

        public override string this[string columnName] => string.Empty;
    }
}
