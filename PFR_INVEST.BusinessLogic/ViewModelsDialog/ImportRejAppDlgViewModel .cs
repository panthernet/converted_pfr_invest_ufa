﻿using System;
using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using Journal;

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OKIP_manager)]
    public class ImportRejAppDlgViewModel  : ViewModelBase, IRequestCloseViewModel
    {

        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }

        public List<RejectApplication> Items => ImportHandler != null ? ImportHandler.Items : null;

        public ImportRejAppDlgViewModel()
        {
            OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
            ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
            ImportHandler = new RejAppImportHandler();

        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        private RejAppImportHandler _importHandler;
        protected RejAppImportHandler ImportHandler
        {
            get { return _importHandler ?? (_importHandler = new RejAppImportHandler()); }
            set { _importHandler = value; }
        }


        private bool CanExecuteOpenFile()
        {
            return ImportHandler.CanExecuteOpenFile();
        }

        private void OpenFile(object o)
        {
            string[] fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            OnPropertyChanged("FileName");
            //OnPropertyChanged("Items");
        }


        private void LoadFiles(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;


            CultureInfo oldCi = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                ImportHandler.Clear();
                foreach (var fileName in fileNames)
                {
                    try
                    {
                        FileName = fileName;
                        string message;
                        if (!ImportHandler.LoadFile(fileName, out message) && !String.IsNullOrEmpty(message))
                        {
                            DialogHelper.ShowAlert(message);
                            return;
                        }
                    }

                    catch (Exception ex)
                    {
                        // DialogHelper.Alert(ex.Message);
                        DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
                        Logger.WriteException(ex);
                    }
                }
                OnPropertyChanged("Items");
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        public bool CanExecuteImport()
        {
            return !String.IsNullOrEmpty(FileName) && ImportHandler.CanExecuteImport();
        }

        private void ExecuteImport(object o)
        {
            if (!CanExecuteImport())
                return;


            string message;

            if (ImportHandler.ExecuteImport(out message))
            {
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Импорт отказных заявлений");
                DialogHelper.ShowAlert(string.Format("Импорт файла {0} выполнен", FileName));
                DialogHelper.ShowAlert(message);
            }
            else
            {
                DialogHelper.ShowAlert(string.Format("Импорт файла {0}\n не выполнен", this.FileName));
                Logger.WriteLine(message);
            }
            FileName = "";
            OnPropertyChanged("Items");
            OnPropertyChanged("FileName");
        }


        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose != null)
                RequestClose(this, null);
        }
    }
}
