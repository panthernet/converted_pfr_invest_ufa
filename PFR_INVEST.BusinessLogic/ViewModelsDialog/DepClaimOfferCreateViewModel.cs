﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects;


namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class DepClaimOfferCreateViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public long AuctionID { get; }
        //public DepClaimSelectParams Auction { get; private set; }
        private bool _isVerbose;

        public DepClaimOfferCreateViewModel(long auctionId, bool isVerbose = true)
            : base(typeof(DepClaim2ListViewModel), 
                    typeof(DepClaim2ConfirmListViewModel),
                    typeof(DepClaimSelectParamsListViewModel),
                    typeof(DepClaimSelectParamsViewModel),
                    typeof(DepClaim2OfferListViewModel))
        {
            _isVerbose = isVerbose;
            DataObjectTypeForJournal = typeof(DepClaimSelectParams);
            ID = auctionId;
            AuctionID = auctionId;
            //this.Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(this.AuctionID);
        }


        public void CreateOffers()
        {
            var result = BLServiceSystem.Client.DepClaimOfferCreate(AuctionID);
            if (result.IsSuccess)
            {
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.INSERT, "Генерировать оферты", $"Aукцион ID:{AuctionID} ");                
                if(_isVerbose) DialogHelper.ShowAlert("Оферты созданы");                
            }
            else
                DialogHelper.ShowError(result.ErrorMessage);
            RefreshConnectedViewModels();
        }


        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName] => string.Empty;
    }
}
