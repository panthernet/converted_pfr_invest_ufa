﻿
using System;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    public abstract class ExportDepClaimBaseViewModel : ViewModelCard
    {
        public override string DocumentAdditionalInfo
        {
            get
            {
                var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(AuctionID);
                return auction == null ? null : string.Format("Аукцион № {0} на дату {1:dd.MM.yyyy}", auction.AuctionNum, auction.SelectDate);
            }
        }

        public long AuctionID { get; protected set; }

        public ExportDepClaimBaseViewModel(params Type[] p_ListViewModelCollection)
            : base(p_ListViewModelCollection)
        {
            this.State = ViewModelState.Export;
        }

        public virtual bool ValidateCanExport()
        {
            var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(AuctionID);
            if (auction == null)
            {
                DialogHelper.ShowAlert("Данная операция временно недоступна для выбранной биржи");
                return false;
            }
            return true;
        }
    }
}
