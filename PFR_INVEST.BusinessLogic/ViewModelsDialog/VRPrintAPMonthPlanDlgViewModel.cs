﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRPrintAPMonthPlanDlgViewModel : PrintAPMonthPlanDlgViewModel
    {
        protected override bool IsYearValid(long yearID)
        {
            //return BLServiceSystem.Client.IsValidYearForPrintMonthPlan(Document.Types.VR, yearID);
            return true;
        }
        protected override List<Year> GetYearsListUkPlan(long operationTypeId)
        {
            return BLServiceSystem.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.VR, operationTypeId);
        }
        protected override List<SIUKPlan> GetUKPlansForYearAndMonth(long monthID, long yearID, long operationTypeId)
        {
            return BLServiceSystem.Client.GetUKPlansForYearMonthAndOperation(Document.Types.VR, monthID, yearID, operationTypeId, true, true);
        }
    }
}
