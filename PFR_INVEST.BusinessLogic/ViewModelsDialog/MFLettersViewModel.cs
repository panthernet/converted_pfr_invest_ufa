﻿using System.IO;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Core.Tools;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects.ListItems;
using System.Threading;
using System.Runtime.InteropServices;
using PFR_INVEST.BusinessLogic.Commands;
using System.ComponentModel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.BusinessLogic
{
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class MFLettersViewModel : ViewModelBase
	{
		public const int TYPE_MF19 = 0;
		public const int TYPE_MF20 = 1;
		public const int TYPE_MF21 = 2;
		public const int TYPE_MF22 = 3;
		public const int TYPE_MF23 = 4;

		private ex.Application m_app;
		private ex.Worksheet m_sheet;

		private const string TMPL_DepositSumm = "Уточненная сумма к размещению.doc";
		private const string TMPL_DepositSummApp = "Уточненная сумма к размещению(приложение).doc";
		private const string TMPL_PortfolioExcel = "Сведения о структуре.xls";
		private const string TMPL_PortfolioStruct = "Структура Портфеля ПФР.doc";
		private const string TMPL_PreSumm = "Письмо ДСП - предварительная сумма.doc";
		private const string TMPL_SellPlan = "Письмо ДСП - выручка.doc";
		private const string TMPL_FactYield = "Письмо ДСП - выручка2.doc";
		//private ApplicationClass wApp = new ApplicationClass();
		//private object missing = System.Reflection.Missing.Value;

		#region Props

		public ICommand SecurityDeleteCommand { get; protected set; }
		public ICommand SecurityAddCommand { get; protected set; }



		public int Type { get; set; }

		private BindingList<DlgSecurityListItem> securities = new BindingList<DlgSecurityListItem>();
		public BindingList<DlgSecurityListItem> Securities
		{
			get { return securities; }
			set { securities = value; OnPropertyChanged("Securities"); }

		}

		private DlgSecurityListItem selsec;
		public DlgSecurityListItem SelectedSecurity { get { return selsec; } set { selsec = value; OnPropertyChanged("SelectedSecurity"); } }

		private decimal diff;
		public decimal Diff { get { return diff; } set { diff = value; OnPropertyChanged("Diff"); } }

		private DateTime _datestart = DateTime.Now;
		public DateTime DateStart
		{
			get
			{
				return _datestart;
			}
			set
			{
				_datestart = value;
				OnPropertyChanged("DateStart");
			}
		}
		private DateTime _dateend = DateTime.Now.AddMonths(1);
		public DateTime DateEnd
		{
			get
			{
				return _dateend;
			}
			set
			{
				_dateend = value;
				OnPropertyChanged("DateEnd");
			}
		}

		private LegalEntity _selectedbank;
		public LegalEntity SelectedBank
		{
			get
			{
				return _selectedbank;
			}
			set
			{
				_selectedbank = value;
				OnPropertyChanged("SelectedBank");
			}
		}

		private Portfolio _selectedportfolio;
		public Portfolio SelectedPortfolio
		{
			get
			{
				return _selectedportfolio;
			}
			set
			{
				_selectedportfolio = value;
				OnPropertyChanged("SelectedPortfolio");
			}
		}

		private DateTime _reportdate = DateTime.Now;
		public DateTime ReportDate
		{
			get
			{
				return _reportdate;
			}
			set
			{
				_reportdate = value;
				OnPropertyChanged("ReportDate");
			}
		}

		private string _regnum;
		public string RegNum
		{
			get
			{
				return _regnum;
			}
			set
			{
				_regnum = value;
				OnPropertyChanged("RegNum");
			}
		}

		private string _regnumapp;
		public string RegNumApp
		{
			get
			{
				return _regnumapp;
			}
			set
			{
				_regnumapp = value;
				OnPropertyChanged("RegNumApp");
			}
		}

		private decimal _summ;
		public decimal Summ
		{
			get
			{
				return _summ;
			}
			set
			{
				_summ = value;
				OnPropertyChanged("Summ");
				RefreshDiff();
			}
		}

		private int _summtype;
		public int SummType
		{
			get
			{
				return _summtype;
			}
			set
			{
				_summtype = value;
				OnPropertyChanged("SummType");
			}
		}

		public List<Person> Managers { get; set; }
		public List<Person> Persons { get; set; }
		//public List<Post> Posts { get; set; }
		public List<Portfolio> Portfolios { get; set; }
		public List<LegalEntity> Banks { get; set; }
		public List<PortfolioStatusListItem> PortfolioStatus { get; set; }

		//private Post _selectedmanagerpost;
		//public Post SelectedManagerPost
		//{
		//    get
		//    {
		//        return _selectedmanagerpost;
		//    }
		//    set
		//    {
		//        _selectedmanagerpost = value;
		//        OnPropertyChanged("SelectedManagerPost");
		//    }
		//}

		//private Post _selectedmanagerpostapp;
		//public Post SelectedManagerPostApp
		//{
		//    get
		//    {
		//        return _selectedmanagerpostapp;
		//    }
		//    set
		//    {
		//        _selectedmanagerpostapp = value;
		//        OnPropertyChanged("SelectedManagerPostApp");
		//    }
		//}



		private Person _selectedmanager;
		public Person SelectedManager
		{
			get
			{
				return _selectedmanager;
			}
			set
			{
				_selectedmanager = value;
				OnPropertyChanged("SelectedManager");
			}
		}

		private Person _selectedmanagerapp;
		public Person SelectedManagerApp
		{
			get
			{
				return _selectedmanagerapp;
			}
			set
			{
				_selectedmanagerapp = value;
				OnPropertyChanged("SelectedManagerApp");
			}
		}

		private Person _selectedperson;
		public Person SelectedPerson
		{
			get
			{
				return _selectedperson;
			}
			set
			{
				_selectedperson = value;
				OnPropertyChanged("SelectedPerson");
			}
		}

		public ICommand OKCommand { get; private set; }
		#endregion

		private void RefreshDiff()
		{
			decimal res = 0;
			foreach (var item in securities)
				res += item.Summ;
			Diff = Summ - res;
		}

		private int p_counter;
		public int PCounter
		{
			get { return p_counter; }
			set
			{
				p_counter = value;
				var pages = ((int)p_counter / 50) + 1;
				double res = 0;
				if (pages > 1)
					res = p_counter / (float)(50 * (pages - 1));
				if (res == 1)
				{
					m_sheet.Cells.get_Range("A" + p_counter, "E" + (p_counter + 49)).Select();
					((ex.Range)m_app.Selection).Insert(ex.XlInsertShiftDirection.xlShiftDown, true);
				}
			}
		}

		private bool CanExecuteSecurityDeleteCommand()
		{
			if (SelectedSecurity != null) return true;
			return false;
		}

		private void ExecuteSecurityDeleteCommand()
		{
			Securities.Remove(SelectedSecurity);
			OnPropertyChanged("Securities");
			RefreshDiff();
		}

		private bool CanExecuteSecurityAddCommand()
		{
			return true;
		}

		private void ExecuteSecurityAddCommand()
		{
			var sec = DialogHelper.SelectSecurity();
			if (sec != null)
			{
				int index = -1;
				foreach (var item in securities)
				{
					if (item.Security.ID == sec.Security.ID)
					{
						index = securities.IndexOf(item); break;
					}
				}
				if (index == -1) Securities.Add(sec);
				else
				{
					Securities[index].Summ += sec.Summ;
					Securities.ResetItem(index);
				}
				OnPropertyChanged("Securities");
				RefreshDiff();
			}
		}

		public MFLettersViewModel(int type)
		{
			Type = type;
            Managers = Persons = DataContainerFacade.GetList<Person>(true).Where(p => !p.IsDisabled).OrderBy(p => p.LastName).ToList();
			//Posts = DataContainerFacade.GetList<Post>();

			switch (Type)
			{
				case TYPE_MF19:
					SecurityDeleteCommand = new DelegateCommand(p => CanExecuteSecurityDeleteCommand(), p => ExecuteSecurityDeleteCommand());
					SecurityAddCommand = new DelegateCommand(p => CanExecuteSecurityAddCommand(), p => ExecuteSecurityAddCommand());
					break;
				case TYPE_MF20:
					PortfolioStatus = BLServiceSystem.Client.GetPortfolioStatusListHib();
					break;
				case TYPE_MF21:
					break;
				case TYPE_MF22:
					Portfolios = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("StatusID", (long)0));
					break;
				case TYPE_MF23:
					Portfolios = new List<Portfolio>(DataContainerFacade.GetListByProperty<Portfolio>("StatusID", (long)0));
					Banks = new List<LegalEntity>(BLServiceSystem.Client.GetBankAgentListForDeposit());
					break;

				default: break;
			}

			OKCommand = new Commands.DelegateCommand(o => CanExecuteOK(),
				o => ExecuteOK());
		}

		#region Execute
		private static void ExecuteOK()
		{
		}

		private bool CanExecuteOK()
		{
			switch (Type)
			{
				case TYPE_MF19:
					if (!string.IsNullOrEmpty(RegNum) && !string.IsNullOrEmpty(RegNumApp) &&
						Summ != 0 &&
						SelectedPerson != null &&
						SelectedManager != null &&
						SelectedManagerApp != null &&
						//SelectedManagerPost != null &&
						//SelectedManagerPostApp != null &&
						Securities.Count > 0)
						return true;
					break;
				case TYPE_MF20:
					if (!string.IsNullOrEmpty(RegNum) &&
						!string.IsNullOrEmpty(RegNumApp) &&
						ReportDate != DateTime.MinValue &&
						SelectedPerson != null &&
						SelectedManager != null
						//&& SelectedManagerPost != null
						)
						return true;
					break;
				case TYPE_MF21:
					if (!string.IsNullOrEmpty(RegNum) &&
						Summ != 0 &&
						SelectedPerson != null &&
						SelectedManager != null
						//&& SelectedManagerPost != null
						)
						return true;
					break;
				case TYPE_MF22:
					if (!string.IsNullOrEmpty(RegNum) &&
						Summ != 0 &&
						SelectedPortfolio != null &&
						SelectedPerson != null &&
						SelectedManager != null
						//&& SelectedManagerPost != null
						)
						return true;
					break;
				case TYPE_MF23:
					if (!string.IsNullOrEmpty(RegNum) &&
						Summ != 0 &&
						SelectedPortfolio != null &&
						DateStart != DateTime.MinValue &&
						DateEnd != DateTime.MinValue &&
						SelectedBank != null &&
						SelectedPerson != null &&
						SelectedManager != null
						//&& SelectedManagerPost != null
						)
						return true;
					break;

				default: return false;
			}
			return false;
		}
		#endregion

		public string Print()
		{
		    MyWord wd = null;
		    MyWord wdApp = null;
		    try
		    {
		        // object readOnly = false;
		        // object isVisible = true;

		        //Microsoft.Office.Interop.Word.Document wDoc;

		        object filePath = null;
		        object appfilePath = null;
		        object excelfilepath = null;

		        #region Filename selection

		        switch (Type)
		        {
		            case TYPE_MF19:
		                filePath = TemplatesManager.ExtractTemplate(TMPL_DepositSumm);
		                filePath = MyWord.GetUniqueFile(filePath.ToString(), true, TMPL_DepositSumm);
		                appfilePath = TemplatesManager.ExtractTemplate(TMPL_DepositSummApp);
		                appfilePath = MyWord.GetUniqueFile(appfilePath.ToString(), true, TMPL_DepositSummApp);
		                break;
		            case TYPE_MF20:
		                filePath = TemplatesManager.ExtractTemplate(TMPL_PortfolioStruct);
		                excelfilepath = TemplatesManager.ExtractTemplate(TMPL_PortfolioExcel);
		                filePath = MyWord.GetUniqueFile(filePath.ToString(), true, TMPL_PortfolioStruct);
		                excelfilepath = MyWord.GetUniqueFile(excelfilepath.ToString(), true, TMPL_PortfolioExcel);
		                break;
		            case TYPE_MF21:
		                filePath = TemplatesManager.ExtractTemplate(TMPL_PreSumm);
		                filePath = MyWord.GetUniqueFile(filePath.ToString(), true, TMPL_PreSumm);
		                break;
		            case TYPE_MF22:
		                filePath = TemplatesManager.ExtractTemplate(TMPL_SellPlan);
		                filePath = MyWord.GetUniqueFile(filePath.ToString(), true, TMPL_SellPlan);
		                break;
		            case TYPE_MF23:
		                filePath = TemplatesManager.ExtractTemplate(TMPL_FactYield);
		                filePath = MyWord.GetUniqueFile(filePath.ToString(), true, TMPL_FactYield);
		                break;
		            default:
		                return string.Empty;
		        }

		        #endregion

		        if (File.Exists((string) filePath))
		        {
		            //wDoc = wApp.Documents.Add(ref filePath, ref missing, ref missing, ref isVisible);
		            //wDoc.Activate();
		            wd = new MyWord();
		            wd.OpenFile(filePath.ToString());

		            if (Type == TYPE_MF19)
		            {
		                #region MF19

		                string monthstr = GetCorrectMonth(DateTime.Now.Month);

		                try
		                {
		                    //WD.FindAndReplace("{CurrentDateString}", string.Format("{0} {1}", monthstr, DateTime.Now.Year));
		                    wd.FindAndReplace("{CurrentDate}", DateTime.Now.ToShortDateString());
		                    //WD.FindAndReplace("{Summ}", Summ.ToString("N"));
		                    wd.FindAndReplace("{ManagerPost}", SelectedManager.Position);
		                    wd.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wd.FindAndReplace("{Manager}", SelectedManager.FIOShort);
		                    wd.FindAndReplace("{RegNum}", RegNum);
		                    wd.FindAndReplace("{RegNumApp}", RegNumApp);

		                }
		                catch
		                {
		                    //WD.FindAndReplace("{CurrentDateString}", string.Empty);
		                    wd.FindAndReplace("{CurrentDate}", string.Empty);
		                    //WD.FindAndReplace("{Summ}", string.Empty);
		                    wd.FindAndReplace("{ManagerPost}", string.Empty);
		                    wd.FindAndReplace("{Person}", string.Empty);
		                    wd.FindAndReplace("{Manager}", string.Empty);
		                    wd.FindAndReplace("{RegNum}", string.Empty);
		                    wd.FindAndReplace("{RegNumApp}", string.Empty);
		                }

		                //приложение
		                if (File.Exists((string) filePath))
		                {
		                    wdApp = new MyWord();
		                    wdApp.OpenFile(appfilePath.ToString());

		                    wdApp.FindAndReplace("{CurrentDateString}", string.Format("{0} {1}", monthstr, DateTime.Now.Year));
		                    wdApp.FindAndReplace("{CurrentDate}", string.Empty);
		                    wdApp.FindAndReplace("{Summ}", Summ.ToString("N"));
		                    wdApp.FindAndReplace("{ManagerPostApp}", SelectedManagerApp.Position);
		                    wdApp.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wdApp.FindAndReplace("{ManagerApp}", SelectedManagerApp.FIOShort);
		                    wdApp.FindAndReplace("{RegNumApp}", RegNumApp);


		                    var doc = wdApp.GetInternal().ActiveDocument;
		                    doc.ActiveWindow.View.ReadingLayout = false;
		                    var table = doc.Tables[1];
		                    var table2 = doc.Tables[3];
		                    int counter = 1;
		                    foreach (var sec in securities)
		                    {
		                        table.Rows.Add();
		                        table2.Rows.Add();
		                        table.Cell(table.Rows.Count, 1).Range.Text = counter.ToString();
		                        table.Cell(table.Rows.Count, 2).Range.Text = sec.Security.Name;
		                        table.Cell(table.Rows.Count, 3).Range.Text = sec.Summ.ToString("N");
		                        table2.Cell(table.Rows.Count, 1).Range.Text = counter.ToString();
		                        table2.Cell(table.Rows.Count, 2).Range.Text = sec.Security.Name;
		                        table2.Cell(table.Rows.Count, 3).Range.Text = sec.Summ.ToString("N");

		                        counter++;
		                    }
		                }

		                #endregion
		            }
		            else if (Type == TYPE_MF20)
		            {
		                #region MF20

		                try
		                {
		                    var firstday = string.Format("01.{0}.{1}", ReportDate.Month < 10 ? "0" + ReportDate.Month : ReportDate.Month.ToString(), ReportDate.Year);

		                    wd.FindAndReplace("{ReportDate}", ReportDate.ToShortDateString());
		                    wd.FindAndReplace("{ReportDateFirst}", firstday);
		                    wd.FindAndReplace("{ManagerPost}", SelectedManager.Position);
		                    wd.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wd.FindAndReplace("{Manager}", SelectedManager.FIOShort);
		                    wd.FindAndReplace("{RegNum}", RegNum);
		                    wd.FindAndReplace("{RegNumApp}", RegNumApp);

		                }
		                catch
		                {
		                    wd.FindAndReplace("{ReportDate}", string.Empty);
		                    wd.FindAndReplace("{ReportDateFirst}", string.Empty);
		                    wd.FindAndReplace("{ManagerPost}", string.Empty);
		                    wd.FindAndReplace("{Person}", string.Empty);
		                    wd.FindAndReplace("{Manager}", string.Empty);
		                    wd.FindAndReplace("{RegNum}", string.Empty);
		                    wd.FindAndReplace("{RegNumApp}", string.Empty);
		                }
		                wd.ShowWord(true);

		                #region Excel sheet

		                //excel
		                if (File.Exists((string) excelfilepath))
		                {
		                    try
		                    {
		                        m_app = new ex.Application();
		                        var oldCI = Thread.CurrentThread.CurrentCulture;
		                        //Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
		                        try
		                        {
		                            var workBook = m_app.Workbooks.Open(excelfilepath.ToString());
		                            m_sheet = (ex.Worksheet) workBook.Sheets[1];

		                            //GENERATE
		                            m_sheet.UsedRange.Range["B8"].Cells.Value2 = string.Format("Российской Федерации и иностранной валюте (по состоянию на {0})", ReportDate.ToShortDateString());

		                            m_sheet.UsedRange.Range["A10", "E10"].Merge();
		                            m_sheet.UsedRange.Range["A10"].Cells.Value2 = "Накопительная часть";
		                            m_sheet.UsedRange.Range["A10"].HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
		                            m_sheet.UsedRange.Range["A10"].Font.Bold = true;
		                            m_sheet.UsedRange.Range["A10", "E10"].Borders.LineStyle = ex.XlLineStyle.xlContinuous;
		                            m_sheet.UsedRange.Range["A10", "E10"].Borders.Weight = ex.XlBorderWeight.xlThin;



		                            var pnames = new List<string>();
		                            foreach (var item in PortfolioStatus)
		                                if (!pnames.Contains(item.PortfolioYear))
		                                {
		                                    //if (pnames.Count == 0)
		                                    pnames.Add(item.PortfolioYear);
		                                    //else
		                                    //{
		                                    //var res = item.PortfolioYear.CompareTo(
		                                    //}
		                                }
		                            pnames.Sort(new PortfolioYearComparer());
		                            PCounter = 11;
		                            //pnames.AddRange(pnames);
		                            foreach (var pname in pnames)
		                            {
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Merge();
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.LineStyle = ex.XlLineStyle.xlContinuous;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.Weight = ex.XlBorderWeight.xlThin;
		                                m_sheet.UsedRange.Range["A" + PCounter].HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Font.Bold = true;
		                                m_sheet.UsedRange.Range["A" + PCounter].Cells.Value2 = string.Format("Портфель {0}г.", pname);
		                                PCounter++;


		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Font.Bold = true;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].WrapText = true;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.LineStyle = ex.XlLineStyle.xlContinuous;
		                                m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.Weight = ex.XlBorderWeight.xlThin;
		                                m_sheet.UsedRange.Range["A" + PCounter].Cells.Value2 = "№ выпуска";
		                                m_sheet.UsedRange.Range["B" + PCounter].Cells.Value2 = "Количество шт.";
		                                m_sheet.UsedRange.Range["C" + PCounter].Cells.Value2 = "Объём по номиналу, тыс. руб.";
		                                m_sheet.UsedRange.Range["D" + PCounter].Cells.Value2 = "Выручка, тыс. руб.";
		                                m_sheet.UsedRange.Range["E" + PCounter].Cells.Value2 = "Цена покупки, % от номинала";
		                                PCounter++;

		                                var portfolios = PortfolioStatus.FindAll(a => a.PortfolioYear == pname);
		                                portfolios.Sort(new PortfolioSecurityComparer());

		                                foreach (var p in portfolios)
		                                {
		                                    m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.LineStyle = ex.XlLineStyle.xlContinuous;
		                                    m_sheet.UsedRange.Range["A" + PCounter, "E" + PCounter].Borders.Weight = ex.XlBorderWeight.xlThin;
		                                    m_sheet.UsedRange.Range["A" + PCounter].Cells.Value2 = p.SecurityID;
		                                    m_sheet.UsedRange.Range["B" + PCounter].Cells.Value2 = p.Count;
		                                    m_sheet.UsedRange.Range["C" + PCounter].Cells.Value2 = p.VolumeByNominal/1000; //тыс. руб.
		                                    m_sheet.UsedRange.Range["D" + PCounter].Cells.Value2 = p.SummNKD/1000; //тыс. руб.
		                                    m_sheet.UsedRange.Range["E" + PCounter].Cells.Value2 = p.MidPrice;

		                                    PCounter++;
		                                }

		                                PCounter++;
		                            }
		                            PCounter += 4;

		                            m_sheet.UsedRange.Range["A" + PCounter, "C" + PCounter].Merge();
		                            m_sheet.UsedRange.Range["A" + PCounter, "C" + PCounter].WrapText = true;
		                            m_sheet.UsedRange.Range["A" + PCounter, "C" + PCounter].Font.Size = 16;
		                            m_sheet.UsedRange.Range["A" + PCounter].Cells.Value2 = SelectedManager.Position;
		                            m_sheet.UsedRange.Range["D" + PCounter, "E" + PCounter].Merge();
		                            //m_sheet.UsedRange.get_Range("D" + PCounter, "E" + PCounter).WrapText = true;
		                            m_sheet.UsedRange.Range["D" + PCounter, "E" + PCounter].Font.Size = 16;
		                            m_sheet.UsedRange.Range["D" + PCounter, "E" + PCounter].HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
		                            m_sheet.UsedRange.Range["D" + PCounter].Cells.Value2 = SelectedManager.FIOShort;

		                            //sign
		                            var page = (int) (PCounter/50);
		                            page++;
		                            var row = (page*50) + 49;

		                            m_sheet.UsedRange.Range["A" + row, "A" + (row - 5)].Font.Size = 16;
		                            m_sheet.UsedRange.Range["A" + (row - 4)].Cells.Value2 = string.Format("рег. {0}", RegNum);
		                            m_sheet.UsedRange.Range["A" + (row - 3)].Cells.Value2 = "отп. 2 экз";
		                            m_sheet.UsedRange.Range["A" + (row - 2)].Cells.Value2 = string.Format("исп. {0}", SelectedPerson.FIOShort);
		                            m_sheet.UsedRange.Range["A" + (row - 1)].Cells.Value2 = string.Format("отп. {0} {1}", SelectedPerson.FIOShort, DateTime.Now.ToShortDateString());
		                            m_sheet.UsedRange.Range["A" + row].Cells.Value2 = "тел.";

		                            m_sheet.PageSetup.PrintArea = string.Format("$A$1:$E${0}", row);
		                            //END
		                            m_app.Visible = true;
		                            Marshal.ReleaseComObject(workBook);
		                        }
		                        finally
		                        {
		                            Thread.CurrentThread.CurrentCulture = oldCI;
		                        }
		                    }
		                    catch
		                    {
		                        // ignored
		                    }
		                    finally
		                    {
		                        if (m_app != null)
		                        {
		                            Marshal.FinalReleaseComObject(m_app);
		                            m_app = null;
		                        }
		                        if (m_sheet != null)
		                        {
		                            Marshal.FinalReleaseComObject(m_sheet);
		                            m_sheet = null;
		                        }
		                    }
		                }
		                else return (string) excelfilepath;

		                #endregion

		                return string.Empty;

		                #endregion
		            }
		            else if (Type == TYPE_MF21)
		            {
		                #region MF21

		                string startdate = GetCorrectMonth(DateTime.Now.Month);
		                string enddate = GetCorrectMonth(DateTime.Now.AddMonths(1).Month);
		                try
		                {
		                    wd.FindAndReplace("{DateStart}", string.Format("{0} {1}", startdate, DateTime.Now.Year));
		                    wd.FindAndReplace("{DateEnd}", string.Format("{0} {1}", enddate, DateTime.Now.AddMonths(1).Year));
		                    wd.FindAndReplace("{CurrentDate}", DateTime.Now.ToShortDateString());
		                    wd.FindAndReplace("{Summ}", Summ.ToString("N"));
		                    wd.FindAndReplace("{ManagerPost}", SelectedManager.Position);
		                    wd.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wd.FindAndReplace("{Manager}", SelectedManager.FIOShort);
		                    wd.FindAndReplace("{RegNum}", RegNum);

		                }
		                catch
		                {
		                    wd.FindAndReplace("{DateStart}", string.Empty);
		                    wd.FindAndReplace("{DateEnd}", string.Empty);
		                    wd.FindAndReplace("{CurrentDate}", string.Empty);
		                    wd.FindAndReplace("{Summ}", string.Empty);
		                    wd.FindAndReplace("{ManagerPost}", string.Empty);
		                    wd.FindAndReplace("{Person}", string.Empty);
		                    wd.FindAndReplace("{Manager}", string.Empty);
		                    wd.FindAndReplace("{RegNum}", string.Empty);
		                }

		                #endregion
		            }
		            else if (Type == TYPE_MF22)
		            {
		                #region MF22

		                string startdate = GetCorrectMonth(DateTime.Now.Month);
		                try
		                {
		                    wd.FindAndReplace("{DateStart}", string.Format("{0} {1}", startdate, DateTime.Now.Year));
		                    wd.FindAndReplace("{CurrentDate}", DateTime.Now.ToShortDateString());
		                    wd.FindAndReplace("{Summ}", Summ.ToString("N"));
		                    wd.FindAndReplace("{ManagerPost}", SelectedManager.Position);
		                    wd.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wd.FindAndReplace("{Manager}", SelectedManager.FIOShort);
		                    wd.FindAndReplace("{RegNum}", RegNum);
		                    wd.FindAndReplace("{PortfolioYear}", SelectedPortfolio.Year);

		                }
		                catch
		                {
		                    wd.FindAndReplace("{DateStart}", string.Empty);
		                    wd.FindAndReplace("{PortfolioYear}", string.Empty);
		                    wd.FindAndReplace("{CurrentDate}", string.Empty);
		                    wd.FindAndReplace("{Summ}", string.Empty);
		                    wd.FindAndReplace("{ManagerPost}", string.Empty);
		                    wd.FindAndReplace("{Person}", string.Empty);
		                    wd.FindAndReplace("{Manager}", string.Empty);
		                    wd.FindAndReplace("{RegNum}", string.Empty);
		                }

		                #endregion
		            }
		            else if (Type == TYPE_MF23)
		            {
		                #region MF23

		                try
		                {
		                    wd.FindAndReplace("{DateStart}", string.Format("{0} {1} {2} года", DateStart.Day, GetCorrectMonth(DateStart.Month, 1), DateStart.Year));
		                    wd.FindAndReplace("{DateEnd}", string.Format("{0} {1} {2} года", DateEnd.Day, GetCorrectMonth(DateEnd.Month, 1), DateEnd.Year));
		                    wd.FindAndReplace("{CurrentDate}", DateTime.Now.ToShortDateString());
		                    wd.FindAndReplace("{PortfolioYear}", SelectedPortfolio.Year);
		                    wd.FindAndReplace("{Bank}", SelectedBank.ShortName);
		                    wd.FindAndReplace("{Summ}", Summ.ToString("N"));
		                    wd.FindAndReplace("{ManagerPost}", SelectedManager.Position);
		                    wd.FindAndReplace("{Person}", SelectedPerson.FIOShort);
		                    wd.FindAndReplace("{Manager}", SelectedManager.FIOShort);
		                    wd.FindAndReplace("{RegNum}", RegNum);

		                }
		                catch
		                {
		                    wd.FindAndReplace("{DateStart}", string.Empty);
		                    wd.FindAndReplace("{PortfolioYear}", string.Empty);
		                    wd.FindAndReplace("{CurrentDate}", string.Empty);
		                    wd.FindAndReplace("{Summ}", string.Empty);
		                    wd.FindAndReplace("{ManagerPost}", string.Empty);
		                    wd.FindAndReplace("{Person}", string.Empty);
		                    wd.FindAndReplace("{Manager}", string.Empty);
		                    wd.FindAndReplace("{RegNum}", string.Empty);
		                }

		                #endregion
		            }
		            // wApp.Visible = true;
		            wd.ShowWord(true);
		            if (wdApp != null) wdApp.ShowWord(true);

		        }
		        else
		        {
		            return (string) filePath;
		        }

		    }
		    catch (Exception ex)
		    {
		        throw new MSOfficeException(ex.Message);
		    }
		    finally
		    {
		        if (wd != null)
		        {
		            wd.Dispose();
		            wd = null;
		        }
                if(wdApp != null)
                {
                    wdApp.Dispose();
                    wdApp = null;
                }
            }
			return string.Empty;
		}

		internal class PortfolioYearComparer : IComparer<string>
		{
			int IComparer<string>.Compare(string x, string y)
			{
				return x.CompareTo(y);
			}
		}

		internal class PortfolioSecurityComparer : IComparer<PortfolioStatusListItem>
		{
			int IComparer<PortfolioStatusListItem>.Compare(PortfolioStatusListItem x, PortfolioStatusListItem y)
			{
				return x.SecurityID.CompareTo(y.SecurityID);
			}
		}

		private static string GetCorrectMonth(int p)
		{
			return GetCorrectMonth(p, 0);
		}
		private static string GetCorrectMonth(int p, int type)
		{
			switch (p)
			{
				case 1: return type == 0 ? "январе" : "января";
				case 2: return type == 0 ? "феврале" : "февраля";
				case 3: return type == 0 ? "марте" : "марта";
				case 4: return type == 0 ? "апреле" : "апреля";
				case 5: return type == 0 ? "мае" : "мая";
				case 6: return type == 0 ? "июне" : "июня";
				case 7: return type == 0 ? "июле" : "июля";
				case 8: return type == 0 ? "августе" : "августа";
				case 9: return type == 0 ? "сентябре" : "сентября";
				case 10: return type == 0 ? "октябре" : "октября";
				case 11: return type == 0 ? "ноябре" : "ноября";
				case 12: return type == 0 ? "декабре" : "декабря";
				default: return string.Empty;
			}
		}
	}
}
