﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    public class LetterToUKViewModel : ViewModelBase
    {
        private readonly List<string> _mBlanksList = new List<string>(new[]{
            "Бланк исполнительной дирекции УК",
            "Бланк ПФР УК",
        });

        public List<string> BlanksList => _mBlanksList;

        private string _mSelectedBlank;
        public string SelectedBlank
        {
            get
            {
                return _mSelectedBlank;
            }
            set
            {
                _mSelectedBlank = value;
                OnPropertyChanged("SelectedBlank");
            }
        }

        private List<LegalEntity> _mUKList;
        public List<LegalEntity> UKList
        {
            get
            {
                return _mUKList;
            }
            set
            {
                _mUKList = value;
                OnPropertyChanged("UKList");
            }
        }

        private LegalEntity _mSelectedUK;
        public LegalEntity SelectedUK
        {
            get
            {
                return _mSelectedUK;
            }
            set
            {
                _mSelectedUK = value;
                OnPropertyChanged("SelectedUK");
            }
        }

        private AddressTypeListItem _selectedAddressType;
        public AddressTypeListItem SelectedAddressType
        {
            get
            {
                return _selectedAddressType;
            }
            set
            {
                _selectedAddressType = value;
                OnPropertyChanged("SelectedAddressType");
            }
        }

        private List<AddressTypeListItem> _addressTypes;
        public List<AddressTypeListItem> AddressTypes
        {
            get
            {
                return _addressTypes;
            }
            set
            {
                _addressTypes = value;
                OnPropertyChanged("AddressTypes");
            }
        }

        private void InitAddressBox()
        {
            AddressTypes = new List<AddressTypeListItem>
            {
                new AddressTypeListItem {Value = AddressType.Post, Label = "Почтовый адрес"},
                new AddressTypeListItem {Value = AddressType.Fact, Label = "Фактический адрес"},
                new AddressTypeListItem {Value = AddressType.Legal, Label = "Юридический адрес"}
            };
            SelectedAddressType = AddressTypes.First();
        }

        public LetterToUKViewModel(long id)
        {
			//var sf = StatusFilter.NotDeletedFilter;
            UKList = BLServiceSystem.Client.GetActiveUKListHib().ToList();
            SelectedUK = id > 0 ? UKList.FirstOrDefault(uk => uk.ID == id) : UKList.First();
            InitAddressBox();
            SelectedBlank = BlanksList.First();
        }

        private _Application _mWApp;
        private _Application WordApp => _mWApp ?? (_mWApp = new Application());

        private void GenerateDoc()
        {
            var address = string.Empty;
            switch (SelectedAddressType.Value)
            {
                case AddressType.Fact:
                    address = SelectedUK.StreetAddress;
                    break;
                case AddressType.Legal:
                    address = SelectedUK.LegalAddress;
                    break;
                case AddressType.Post:
                    address = SelectedUK.PostAddress;
                    break;
            }

            OfficeTools.FindAndReplace(WordApp, "{ADDRESS}", address);
            OfficeTools.FindAndReplace(WordApp, "{FULLNAME}", SelectedUK.FullName);
            OfficeTools.FindAndReplace(WordApp, "{HEADNAME}", GetProperHeadName(SelectedUK.HeadFullName));
        }

        private static string GetProperHeadName(string pName)
        {
            var names = pName.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return names.Length < 3 ? names[0] : string.Join(" ", names[1], names[2]);
        }

        public void Print()
        {
            var wordTemplateName = string.Format("{0}.doc", SelectedBlank);
            var extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);
            if (!File.Exists(extractedFilePath))
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
                return;
            }

            //var filePath = extractedFilePath as object;
            try
            {
                WordApp.Visible = false;
                WordApp.Documents.Open(extractedFilePath);
                GenerateDoc();
                WordApp.Visible = true;
            }
            finally
            {
                if (_mWApp != null)
                    Marshal.FinalReleaseComObject(_mWApp);
            }
        }
    }
}
