﻿
using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ExportNotAcceptedDepClaimOfferViewModel : ViewModelCard, IVmNoDefaultLogging
    {
        public long OfferID { get; private set; }
        public DelegateCommand PrintCommand { get; private set; }

        public ExportNotAcceptedDepClaimOfferViewModel(long offerID)
        {
            this.PrintCommand = new DelegateCommand(OnPrint_CanExecute, OnPrint_Execute);
            this.OfferID = offerID;
        }

        public Person ChiefPerson { get; set; }
        public Person ExecuterPerson { get; set; }
        public Person ControllerPerson { get; set; }

        protected bool OnPrint_CanExecute(object o) 
        {
            return this.ChiefPerson != null && this.ExecuterPerson != null && this.ControllerPerson != null;
        }

        protected void OnPrint_Execute(object o)
        {
            var print = new PrintNotAcceptedDepClaimOffer(this.OfferID, this.ChiefPerson, this.ExecuterPerson, this.ControllerPerson);
            print.Print();

        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName]
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}
