﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIYearPlanPaymentDlgViewModel : YearPlanPaymentDlgViewModel
    {
        protected override Dictionary<long, string> GetUKListForYear(long yearID, long operationTypeId)
        {
            return BLServiceSystem.Client.GetUKListForYearAndOperationType(Document.Types.SI, yearID, operationTypeId);
        }

        protected override List<SITransfer> GetUKPlansForYear(long yearID, long operationTypeId, long ukId)
        {
            return BLServiceSystem.Client.GetUKPlansForYearAndOperationType(Document.Types.SI, yearID, operationTypeId, ukId);
        }

        protected override List<Year> GetYearsListUkPlan(long operationTypeId)
        {
            return BLServiceSystem.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.SI, operationTypeId);
        }

        protected override List<Element> GetOperationTypeListUkPlan()
        {
            return BLServiceSystem.Client.GetElementByType(Element.Types.RegisterOperationType);
        }
    }
}
