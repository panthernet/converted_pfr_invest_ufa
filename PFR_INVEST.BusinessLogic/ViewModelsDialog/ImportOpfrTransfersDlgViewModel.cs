﻿using System;
using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.Commands;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using Journal;

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    public class ImportOpfrTransfersDlgViewModel : ViewModelBase, IRequestCloseViewModel
    {

        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }

        public List<OpfrTransferImportListItem> Items => ImportHandler?.Items;

        private readonly long _registerId;

        public ImportOpfrTransfersDlgViewModel(long registerId)
        {
            _registerId = registerId;
            OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
            ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        private OpfrTransfersImportHandler _importHandler;
        protected OpfrTransfersImportHandler ImportHandler
        {
            get { return _importHandler ?? (_importHandler = new OpfrTransfersImportHandler(_registerId)); }
            set { _importHandler = value; }
        }


        private bool CanExecuteOpenFile()
        {
            return ImportHandler.CanExecuteOpenFile();
        }

        private void OpenFile(object o)
        {
            string[] fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            OnPropertyChanged("FileName");
        }


        private void LoadFiles(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;

            var oldCi = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                ImportHandler.Clear();
                foreach (var fileName in fileNames)
                {
                    try
                    {
                        FileName = fileName;
                        string message;
                        if (!ImportHandler.LoadFile(fileName, out message) && !string.IsNullOrEmpty(message))
                        {
                            DialogHelper.ShowAlert(message);
                            ImportHandler.Clear();
                            if (message.Contains("формат файла"))
                                FileName = null;
                            return;
                        }
                    }

                    catch (Exception ex)
                    {
                        // DialogHelper.Alert(ex.Message);
                        DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
                        Logger.WriteException(ex);
                    }
                }
                OnPropertyChanged("Items");
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        public bool CanExecuteImport()
        {
            return !string.IsNullOrEmpty(FileName) && ImportHandler.CanExecuteImport();
        }

        private void ExecuteImport(object o)
        {
            if (!CanExecuteImport())
                return;


            string message;
            bool result;
            try
            {
                DialogHelper.ShowLoadingIndicator("Импорт данных...");
                result = ImportHandler.ExecuteImport(FileName, out message);
            }
            finally
            {
                DialogHelper.HideLoadingIndicator();
            }
            if (result)
            {
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Импорт ОПФР");
                DialogHelper.ShowAlert($"Импорт файла {FileName} выполнен");
                CloseWindow();
                //DialogHelper.ShowAlert(message);
            }
            else
            {
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Импорт ОПФР - ОШИБКА");
                DialogHelper.ShowAlert($"Импорт файла {FileName}\n не выполнен");
                Logger.WriteLine(message);
            }
            FileName = "";
            OnPropertyChanged("Items");
            OnPropertyChanged("FileName");
        }


        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            RequestClose?.Invoke(this, null);
        }
    }
}
