﻿using System;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OARRS_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_directory_editor, DOKIP_ROLE_TYPE.OARRS_directory_editor)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OARRS_manager)]
    public class LegalEntityHeadDlgViewModel : ViewModelBase
    {
        public LegalEntityHead Head { get; private set; }

        public DelegateCommand<object> SaveCard { get; private set; }

        public LegalEntityHeadDlgViewModel(LegalEntityHead head)
        {
            this.Head = head;
            this.SaveCard = new DelegateCommand<object>(o =>
            {
                return IsValid();
            }, o => { });
        }

        public bool IsValid()
        {
            string fields = "FullName|Position";
            foreach (string key in fields.Split('|'))
            {
                if (!String.IsNullOrEmpty(Head[key]))
                    return false;
            }
            return true;
        }
    }
}
