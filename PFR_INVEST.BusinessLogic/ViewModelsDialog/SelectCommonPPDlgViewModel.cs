﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Core;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker)]
	[DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OKIP_manager)]
	public class SelectCommonPPDlgViewModel : ViewModelCard, IUpdateRaisingModel, IDialogViewModel
	{
		public string Title { get; private set; }
		public long? TransferID { get; private set; }
		public AsgFinTr.Directions Direction{get;private set;}

		public List<Element> DirectionsList { get; private set; }
		public IList<AsgFinTr> List { get; private set; }
	    public AsgFinTr.Sections Section { get; private set; }

		public SelectCommonPPDlgViewModel(long? trID, AsgFinTr.Sections section, AsgFinTr.Directions direction, string title = "Выбрать платежное поручение")
		{
		    DataObjectTypeForJournal = typeof (AsgFinTr);
			this.Title = title;
			this.Direction = direction;
		    Section = section;
			TransferID = trID;
			DirectionsList = BLServiceSystem.Client.GetElementByType(Element.Types.PPDirection);

			this.List = BLServiceSystem.Client.GetUnlinkedCommonPPList(Section, direction);
		}


		private AsgFinTr _selectedItem;
		public AsgFinTr SelectedItem
		{
			get { return _selectedItem; }
			set { if (_selectedItem != value) { _selectedItem = value; OnPropertyChanged("SelectedItem"); } }
		}

		private void LinkPP(AsgFinTr pp)
		{
		    switch (Section)
		    {
                case AsgFinTr.Sections.NPF:
                    pp.ReqTransferID = null;
                    pp.FinregisterID = TransferID.Value;
                    pp.SectionElID = (long)AsgFinTr.Sections.NPF;
                    pp.LinkElID = (long)AsgFinTr.Links.NPF;
                    pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.NPF;

                    var fr = DataContainerFacade.GetByID<Finregister>(TransferID.Value);
                    var reg = fr.GetRegister();
                    pp.LinkedRegisterID = reg.ID;
                    break;
                case AsgFinTr.Sections.SI:
                case AsgFinTr.Sections.VR:
                    pp.ReqTransferID = TransferID.Value;
                    pp.FinregisterID = null;

                    var rt = DataContainerFacade.GetByID<ReqTransfer>(TransferID.Value);
                    var st = rt.GetTransferList();
                    var type = st.GetContract().TypeID;

                    switch (type)
                    {
                        case (int)Document.Types.SI:
                            pp.SectionElID = (long)AsgFinTr.Sections.SI;
                            pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.UK;
                            break;
                        case (int)Document.Types.VR:
                            pp.SectionElID = (long)AsgFinTr.Sections.VR;
                            pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.UK;
                            break;
                    }

                    pp.LinkElID = (long)AsgFinTr.Links.UK;
                    pp.LinkedRegisterID = st.TransferRegisterID;
                    break;
                case AsgFinTr.Sections.BackOffice:
                    pp.ReqTransferID = null;
                    pp.FinregisterID = null;
                    pp.SectionElID = (long)AsgFinTr.Sections.BackOffice;
                    pp.LinkElID = (long)AsgFinTr.Links.OPFR;
                    pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.OPFR;

                    var tr = DataContainerFacade.GetByID<OpfrTransfer>(TransferID.Value);
		            DataContainerFacade.Save(new LinkPPOPFRTransfer { PPID = pp.ID, TransferID = TransferID.Value });
                    pp.LinkedRegisterID = tr.OpfrRegisterID;
                    break;
            }
		}

		protected override void ExecuteSave()
		{
			var pp = this.SelectedItem.Copy();
			LinkPP(pp);
			ID = BLServiceSystem.Client.SaveCommonPP(pp);

			RequestClose(true);
		}

		protected override bool BeforeExecuteSaveCheck()
		{
			var pp = this.SelectedItem.Copy();
			LinkPP(pp);

            if (!CommonPPViewModel.IsClosedPP(pp)) return true;
		    DialogHelper.ShowAlert("Внимание! Сумма выбранного перечисления заполнена. Привязка платежного поручения к выбранному перечислению невозможна.");
		    return false;
		}

		public override bool CanExecuteSave()
		{
			return SelectedItem != null;
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					default: return null;
				}
			}
		}

		IList<KeyValuePair<Type, long>> IUpdateRaisingModel.GetUpdatedList()
		{
		    var list = new List<KeyValuePair<Type, long>> {new KeyValuePair<Type, long>(typeof (AsgFinTr), SelectedItem.ID)};

		    switch (Section)
		    {
                case AsgFinTr.Sections.NPF:
				    list.Add(new KeyValuePair<Type, long>(typeof(Finregister), TransferID.Value));
                    break;
                case AsgFinTr.Sections.SI:
                case AsgFinTr.Sections.VR:
                    list.Add(new KeyValuePair<Type, long>(typeof(ReqTransfer), TransferID.Value));
                    break;
                case AsgFinTr.Sections.BackOffice:
                    list.Add(new KeyValuePair<Type, long>(typeof(OpfrTransfer), TransferID.Value));
                    break;
            }
			return list;
		}

		private void RequestClose(bool? DialogResult)
		{
		    OnCloseRequested?.Invoke(this, new EventArgs<bool?>(DialogResult));
		}
		public event EventHandler<EventArgs<bool?>> OnCloseRequested;
	}
}
