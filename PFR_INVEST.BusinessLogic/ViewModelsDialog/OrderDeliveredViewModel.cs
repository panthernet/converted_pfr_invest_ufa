﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class OrderDeliveredViewModel : ViewModelCard, IUpdateRaisingModel
    {
        private F401402UK m_Detail;
        public F401402UK Detail
        {
            get { return m_Detail; }
            set
            {
                if (m_Detail != value)
                {
                    m_Detail = value;
                    OnPropertyChanged("Detail");
                }
            }
        }

        new public DateTime? GetDate
        {
            get { return Detail.ControlGetDate; }
            set
            {
                Detail.ControlGetDate = value;
                OnPropertyChanged("GetDate");
                SetDate = GetDate.HasValue ? (DateTime?)GetDate.Value.AddDays(7) : null;
            }
        }

        public DateTime? SetDate
        {
            get { return Detail.ControlSetDate; }
            set
            {
                Detail.ControlSetDate = value;
                OnPropertyChanged("SetDate");
            }
        }

        public void RefreshF402View()
        {
            if (GetViewModelsList != null)
            {
                List<ViewModelBase> lst = GetViewModelsList(typeof(F401402UKViewModel));
                if (lst != null && lst.Count > 0)
                    lst.Cast<F401402UKViewModel>()
                        .Where(card => card.Detail.ID == Detail.ID)
                        .ToList()
                        .ForEach(card => card.RefreshControlGroup());
            }
        }

        public OrderDeliveredViewModel(long id)
            : base(typeof(F401402UKListViewModel))
        {
            DataObjectTypeForJournal = typeof(F401402UK);
            ID = id;
            Detail = DataContainerFacade.GetByID<F401402UK, long>(id);
            GetDate = DateTime.Today;
            RefreshConnectedCardsViewModels = RefreshF402View;
        }

        protected override void ExecuteSave()
        {
            Detail.StatusID = (int)F402Status.Delivered;
            ID = DataContainerFacade.Save<F401402UK, long>(Detail);
        }

        public override bool CanExecuteSave()
        {
            return string.IsNullOrEmpty(Validate());
        }

        string Validate()
        {
            const string errorMessage = "Неверный формат данных";
            const string validate = "GetDate|SetDate";

            if (Detail == null)
                return errorMessage;

            foreach (string key in validate.Split('|'))
            {
                if (!String.IsNullOrEmpty(this[key]))
                {
                    return errorMessage;
                }
            }

            return null;
        }

        public override string this[string columnName]
        {
            get
            {
                const string errorMessage = "Необходимо ввести данные";

                switch (columnName.ToUpper())
                {
                    case "GETDATE": if (!GetDate.HasValue) return errorMessage;
                        if (GetDate.HasValue && Detail.ControlSendDate.HasValue && Detail.ControlSendDate > GetDate)
                            return string.Format("Дата отправки ({0}) не может быть больше даты вручения поручения", Detail.ControlSendDate.Value.ToShortDateString());
                        return null;
                    case "SETDATE": if (!SetDate.HasValue) return errorMessage;
                        if (SetDate.HasValue && Detail.ControlSendDate.HasValue && Detail.ControlSendDate > SetDate)
                            return string.Format("Дата отправки ({0}) не может быть больше установленной даты оплаты", Detail.ControlSendDate.Value.ToShortDateString());
                        if (SetDate.HasValue && GetDate.HasValue && GetDate > SetDate)
                            return string.Format("Дата вручения требования ({0}) не может быть больше установленной даты оплаты", GetDate.Value.ToShortDateString());
                        return null;
                    default: return null;
                }
            }
        }

        public IList<KeyValuePair<Type, long>> GetUpdatedList()
        {
            return new List<KeyValuePair<Type, long>>() { 
				new KeyValuePair<Type, long>(typeof(F401402UKListItem), this.Detail.ID)
			};
        }
    }
}
