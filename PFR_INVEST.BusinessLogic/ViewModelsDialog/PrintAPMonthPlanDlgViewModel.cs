﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.Common.Tools;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class PrintAPMonthPlanDlgViewModel : ViewModelBase
    {
        private const string TEMPLATE_NAME = "План на месяц.xls";

        private ex.Application _mApp;
        private ex.Worksheet _mSheet;

        #region Selected Operation

        private List<Element> _operationsList;
        public List<Element> OperationsList
        {
            get { return _operationsList; }
            set { _operationsList = value;
                SelectedOperation = OperationsList.FirstOrDefault();
                OnPropertyChanged("OperationsList"); }
        }

        private Element _selectedOperation;
        public Element SelectedOperation
        {
            get
            {
                return _selectedOperation;
            }
            set
            {
                _selectedOperation = value;
                UpdateYearsList();
                OnPropertyChanged("SelectedOperation");
            }
        }

        /// <summary>
        /// Проверяет, есть ли планы по какой-либо операции
        /// </summary>
        /// <returns></returns>
        public bool HasYearsListByAllOperations()
        {
            // Получаем все года
            var allYearsList = new List<Year>();
            OperationsList.ForEach(x => allYearsList.AddRange(GetYearsListUkPlan(x.ID)));

            return allYearsList.Any();
        }


        private void UpdateYearsList()
        {
            YearsList = GetYearsListUkPlan(SelectedOperation.ID);
            
            if (!HasYearsListByAllOperations())
            {
                DialogHelper.ShowAlert("Для выбранного типа операции \"Планы\" не созданы!");
                return;
            }

            if (YearsList != null)
            {
                MonthsList = DataContainerFacade.GetList<Month>();
                try
                {
                    var year = YearsList.FirstOrDefault(p_y => p_y.Name == DateTime.Today.Year.ToString());
                    SelectedYear = year ?? YearsList.FirstOrDefault();
                    SelectedMonth = MonthsList.Find(pM => pM.ID == DateTime.Today.Month);
                }
                catch (Exception ex)
                {
                    Logger.WriteException(ex);
                }
            }
        }

        #endregion

        #region Selected Year

        private List<Year> yearsList;
        public List<Year> YearsList
        {
            get { return yearsList; }
            set { yearsList = value; OnPropertyChanged("YearsList"); }
        }

        private Year _selectedYear;
        public Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                OnPropertyChanged("SelectedYear");
            }
        }

        #endregion

        #region Selected Month

        private List<Month> monthsList;
        public List<Month> MonthsList
        {
            get { return monthsList; }
            set { monthsList = value; OnPropertyChanged("MonthsList"); }
        }

        private Month _selectedMonth;
        public Month SelectedMonth
        {
            get
            {
                return _selectedMonth;
            }
            set
            {
                _selectedMonth = value;
                OnPropertyChanged("SelectedMonth");
            }
        }

        #endregion

        protected PrintAPMonthPlanDlgViewModel()
        {
            OperationsList = new List<Element>()
            {
                DataContainerFacade.GetByID<Element>((long)Element.SpecialDictionaryItems.PaymentZLType),
                DataContainerFacade.GetByID<Element>((long)Element.SpecialDictionaryItems.PaymentReserve),
            };
        }

        public bool IsSelectedYearValid()
        {
            if (SelectedYear != null)
                return IsYearValid(SelectedYear.ID);

            return false;
        }

        protected abstract List<Year> GetYearsListUkPlan(long operationTypeId);
        protected abstract bool IsYearValid(long yearID);
        protected abstract List<SIUKPlan> GetUKPlansForYearAndMonth(long monthID, long yearID, long operationTypeId);

        private void Generate()
        {
            var plans = GetUKPlansForYearAndMonth(SelectedMonth.ID, SelectedYear.ID, SelectedOperation.ID);
            var tmplist = (from item in plans let ctr = item.GetTransferList().GetContract() where ctr == null || ctr.Status == -1 select item).ToList();
            foreach (var item in tmplist)
                plans.Remove(item);


            if (plans.Count > 0)
            {
                var r = plans[0].GetTransferList().GetTransferRegister();
                var header1 = _mSheet.Range["D1"].Cells.Value2.ToString();
                header1 = header1.Replace("{DATE}", r.RegisterDate?.ToString("dd.MM.yy") ?? "");
                header1 = header1.Replace("{NUMBER}", r.RegisterNumber);
                _mSheet.Range["D1"].Cells.Value2 = header1;
            }

            var header = _mSheet.Range["A2"].Cells.Value2.ToString();
            _mSheet.Range["A2"].Cells.Value2 = header.Replace("{YEAR}", SelectedYear.Name)
                .Replace("{MONTH}", DateTools.GetProperMonthName(SelectedMonth.ID, SelectedMonth.Name))
                .Replace("{TITLE_END}", SelectedOperation.ID == (long)Element.SpecialDictionaryItems.PaymentZLType ? "выплат правопреемникам умерших застрахованных лиц" : "выплатного резерва");

            var rowCounter = 7;
            var count = plans.Count;
            for (var i = 1; i < count; i++)
                _mSheet.Range["A" + (rowCounter)].EntireRow.Insert(ex.XlInsertShiftDirection.xlShiftDown, ex.XlInsertFormatOrigin.xlFormatFromRightOrBelow);

            for (var i = 1; i <= count; i++)
            {
                var plan = plans[i - 1];

                _mSheet.Range["A" + rowCounter].Cells.Value2 = i;
                _mSheet.Range["B" + rowCounter].Cells.Value2 = plan.GetTransferList().GetContract().GetLegalEntity().FormalizedName;
                _mSheet.Range["C" + rowCounter].Cells.Value2 = plan.GetTransferList().GetContract().ContractNumber;
                _mSheet.Range["D" + rowCounter].Cells.Value2 = plan.GetTransferList().GetContract().PortfolioName;
                var rt = plan.GetTotalSummReqTransfer();
                _mSheet.Range["E" + rowCounter].Cells.Value2 = rt?.Sum ?? 0;
                _mSheet.Range["F" + rowCounter].Cells.Value2 = rt?.InvestmentIncome ?? 0;

                rowCounter++;
            }

            _mSheet.Range["E" + rowCounter].Formula = $"=SUM(E7:E{rowCounter - 1})";
            _mSheet.Range["F" + rowCounter].Formula = $"=SUM(F7:F{rowCounter - 1})";

            _mSheet.Range["A2"].Select();
            
        }

        protected override string PrintingInformation => $"Печать плана за {SelectedMonth.Name.Trim()} {SelectedYear.Name}г. ...";

        public void Print()
        {
            try
            {
                object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

                if (File.Exists(filePath.ToString()))
                {
                    RaiseStartedPrinting();
                    _mApp = new ex.Application();

                    var oldCI = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    try
                    {
                        var workBook = _mApp.Workbooks.Open(filePath.ToString());
                        _mSheet = (ex.Worksheet) workBook.Sheets[1];

                        Generate();
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT);
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCI;
                    }

                    RaiseFinishedPrinting();
                    _mApp.Visible = true;
                }
                else
                {
                    RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
            finally
            {
                if (_mSheet != null)
                    Marshal.FinalReleaseComObject(_mSheet);
                if (_mApp != null)
                    Marshal.FinalReleaseComObject(_mApp);
            }
        }
    }
}
