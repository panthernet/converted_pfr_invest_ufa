﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.BusinessLogic.BranchReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.BusinessLogic.Journal;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OKIP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class RegionReportExportDlgViewModel : ViewModelBase
    {
        public DelegateCommand ExportCommand { get; private set; }
        

        public RegionReportExportDlgViewModel()
        {
            ReportTypes = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportType>(new List<ListPropertyCondition>
            {
                ListPropertyCondition.Equal("Source", (int)PfrBranchReportType.SourceMode.Excel)
            }).ToList();
            SelectedMonth = DateTime.Now.Month;
            SelectedYear = DateTime.Now.Year;
            
            ExportCommand = new DelegateCommand(o => CanExecuteExport(), ExecuteExport);
        }

        private bool CanExecuteExport()
        {
            return ExportHandler.CanExecuteExport();
        }

        private void ExecuteExport(object o)
        {
            try
            {
                ExportHandler.ExecuteExport();
                JournalLogger.LogEvent("Экспорт данных", DataObjects.Journal.JournalEventType.EXPORT_DATA, null, "Экспорт отчетов по регионам", "");              
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка экспорта отчета");
                Logger.WriteException(ex);
            }
        }

        PfrBranchReportType _SelectedReportType;
        public PfrBranchReportType SelectedReportType
        {
            get { return _SelectedReportType; }
            set
            {
                _SelectedReportType = value;
                SetExportHandler();
                OnPropertyChanged("SelectedReportType");
                OnPropertyChanged("SelectedReportDescription");

                OnPropertyChanged("IsYearEnabled");
                OnPropertyChanged("IsMonthEnabled");
                OnPropertyChanged("IsResponsibleEnabled");
                OnPropertyChanged("IsPerformerEnabled");
                OnPropertyChanged("MonthVisibility");

                OnPropertyChanged("PeriodLengthVisibility");
                OnPropertyChanged("IsPeriodLengthEnabled");

                OnPropertyChanged("IsMonthly");
                OnPropertyChanged("IsAnnual");
                OnPropertyChanged("IsMonthVisible");
            }
        }

        private void SetExportHandler()
        {
            ExportHandler = PfrBranchReportExportFactory.GetExportHandler(this);
        }

        public string SelectedReportDescription
        {
            get
            {
                if (SelectedReportType == null) return null;
                return SelectedReportType.FullName;
            }
        }

        private PfrBranchReportExportHandlerBase _ExportHandler;
        private PfrBranchReportExportHandlerBase ExportHandler
        {
            get
            {
                if (_ExportHandler == null)
                    _ExportHandler = new PfrBranchReportExportHandler_Empty();

                return _ExportHandler;
            }
            set
            { _ExportHandler = value; }
        }

        public List<PfrBranchReportType> ReportTypes { get; private set; }


        private Person _Responsible;
        public Person Responsible
        {
            get { return _Responsible; }
            set
            {
                if (_Responsible != value)
                {
                    _Responsible = value;
                    OnPropertyChanged("Responsible");
                   
                }
            }
        }

        private Person _Performer;
        public Person Performer
        {
            get { return _Performer; }
            set
            {
                if (_Performer != value)
                {
                    _Performer = value;
                    OnPropertyChanged("Performer");
                    
                }
            }
        }

        private int? _SelectedMonth;
        public int? SelectedMonth
        {
            get
            {
                if (IsAnnual)
                    return null;
                return _SelectedMonth;
            }
            set
            {
                if (_SelectedMonth != value)
                {
                    _SelectedMonth = value;
                    OnPropertyChanged("SelectedMonth");
                }
            }
        }

        public object SelectedYearEditValue
        {
            get
            {
                return SelectedYear;
            }
            set
            {
                if (value == null)
                {
                    SelectedYear = null;
                    return;
                }

                if (value is int)
                {
                    SelectedYear = (int)value;
                    return;
                }

                if (!(value is string))
                {
                    SelectedYear = null;
                    return;
                }

                int x;
                if (int.TryParse((string)value, out x))
                {
                    SelectedYear = x;
                    return;
                }

                SelectedYear = null;
                return;
            }
        }

        private int? _SelectedYear;
        public int? SelectedYear
        {
            get
            {
                return _SelectedYear;
            }
            set
            {
                if (_SelectedYear != value)
                {
                    _SelectedYear = value;
                    OnPropertyChanged("SelectedYear");
                }
            }
        }

        public List<int?> Monthes => new List<int?> { null, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

        public List<int?> Years
        {
            get
            {
                var year = DateTime.Now.Year;
                List<int?> years = new List<int?>();

                years.Add(null);

                for (int i = -2; i < 3; i++)
                {
                    years.Add(year + i);
                }

                return years;
            }
        }

        [Obsolete]
        internal bool AskReportDataIncomplete(object p)
        {
            return AskReportDataIncomplete(p.ToString());
        }

        internal bool AskReportDataIncomplete(string s)
        {
            string message = string.Format("{0}\r\nПродолжить формирование отчета?", s);

            return DialogHelper.ShowConfirmation(message);
        }

        public virtual bool IsYearEnabled => ExportHandler.IsYearEnabled;

        public virtual bool IsMonthEnabled => ExportHandler.IsMonthEnabled;

        public virtual bool IsResponsibleEnabled => ExportHandler.IsResponsibleEnabled;

        public virtual bool IsPerformerEnabled => ExportHandler.IsPerformerEnabled;


        public Visibility MonthVisibility => IsMonthVisible ? Visibility.Visible : Visibility.Collapsed;

        private bool IsMonthVisible => IsMonthly;


        public Visibility PeriodLengthVisibility => IsPeriodLengthVisible ? Visibility.Visible : Visibility.Collapsed;

        private bool IsPeriodLengthVisible => ExportHandler.IsPeriodLengthVisible;

        public bool IsPeriodLengthEnabled => ExportHandler.IsPeriodLengthEnabled;

        public bool IsMonthly
        {
            get { return ExportHandler.IsMonthly; }
            set
            {
                if (ExportHandler.IsMonthly != value)
                {
                    ExportHandler.IsMonthly = value;
                    OnPropertyChanged("IsMonthly");
                    OnPropertyChanged("IsAnnual");
                    OnPropertyChanged("MonthVisibility");
                }
            }
        }

        public bool IsAnnual
        {
            get { return ExportHandler.IsAnnual; }
            set
            {
                if (ExportHandler.IsAnnual != value)
                {
                    ExportHandler.IsAnnual = value;
                    OnPropertyChanged("IsMonthly");
                    OnPropertyChanged("IsAnnual");
                    OnPropertyChanged("MonthVisibility");
                }
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedYear":
                        if (SelectedYear == null || SelectedYear < 0)
                            return "Выбранный год имеет неправильное значение";
                        break;
                    default: return null;
                }

                return null;
            }
        }

        public virtual bool Validate()
        {            
            var props = this.GetType().GetProperties().Where(p => p.CanRead && p.CanWrite);
            foreach (var p in props)
            {
                var error = this[p.Name];
                if (!string.IsNullOrEmpty(error))
                {                    
                    return false;
                }
            }
            return true;
        }
    }
}
