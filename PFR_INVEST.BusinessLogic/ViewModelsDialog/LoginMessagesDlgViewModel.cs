﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OKIP_worker,
        DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class LoginMessagesDlgViewModel : ViewModelBase
    {
        public class MessageGroup
        {
            public int Order { get; set; }
            public string Title { get; set; }

            public IList<LoginMessageItem> Messages { get; set; }
        }

        public bool ShowNextTime { get; set; }
		public bool HideIgnoreBox { get; set; }

        public IList<MessageGroup> Groups { get; set; }

        public LoginMessagesDlgViewModel(IList<LoginMessageItem> messages, bool hideIgnoreBox = false)
            : this()
        {
			this.HideIgnoreBox = hideIgnoreBox;
            this.Groups = messages.Where(m => !m.Ignore)
                                    .GroupBy(m => m.Title)
                                    .Select(g => new MessageGroup()
                                    {
                                        Title = g.First().Title,
                                        Order = g.First().Order,
                                        Messages = g.ToList()
                                    }).OrderBy(g => g.Order).ToList();

        }

        public LoginMessagesDlgViewModel()
        {
            ShowNextTime = true;
        }
    }
}
