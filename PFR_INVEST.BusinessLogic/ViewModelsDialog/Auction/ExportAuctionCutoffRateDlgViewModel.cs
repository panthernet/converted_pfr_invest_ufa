﻿
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using PFR_INVEST.DataAccess.Client;
	using PFR_INVEST.DataObjects;
    using PFR_INVEST.DataObjects.XMLModel;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
	public class ExportAuctionCutoffRateDlgViewModel : ViewModelCard
	{
		public DepClaimSelectParams Auction { get; set; }

		public decimal CutoffRate
		{
			get { return this.Auction.CutoffRate; }
			set
			{
				if (this.Auction.CutoffRate != value)
				{
					this.Auction.CutoffRate = value;
					OnPropertyChanged("CutoffRate");
				}
			}
		}

		private string _Folder = null;
		public string Folder
		{
			get { return _Folder; }
			set
			{
				if (_Folder != value)
				{
					_Folder = value;
					OnPropertyChanged("Folder");
				}
			}
		}

		public XMLFileResult Document { get; set; }

		public ExportAuctionCutoffRateDlgViewModel(long auctionId)
		{

			this.ID = auctionId;
			this.Auction = DataContainerFacade.GetByID<DepClaimSelectParams>(this.ID);
		}



		protected override void ExecuteSave()
		{
			//Сохраняем в базу ставку отсеения			
			DataContainerFacade.Save<DepClaimSelectParams>(this.Auction);

			//Генерируем документ
			Document = BLServiceSystem.Client.ExportAuctionCutoffRate(this.Auction.ID);
		}

		public override bool CanExecuteSave()
		{
			return Validate();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "CutoffRate": return CutoffRate.ValidateRange(0.01m, 100.0m, true);
					case "Folder": return Folder.ValidateRequired();
					default: return null;
				}
			}
		}

		private bool Validate()
		{
			return base.ValidateFields();
		}
	}
}
