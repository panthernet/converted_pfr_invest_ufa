﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class AddOpfrTransferViewModel: ViewModelCardDialog
    {
        private string _linkedRegisterText;
        private string _linkedTransferText;
        private long? _directionElID;
        private long _linkElID;
        private bool _isTransferAllowed;
        private long? _linkedRegisterID;
        private long? _linkedTransferID;
        public ICommand ClearRegisterCommand { get; set; }
        public ICommand SelectRegisterCommand { get; set; }
        public ICommand ClearTransferCommand { get; set; }
        public ICommand SelectTransferCommand { get; set; }

        public string LinkedRegisterText
        {
            get { return _linkedRegisterText; }
            set { _linkedRegisterText = value; OnPropertyChanged(nameof(LinkedRegisterText)); }
        }

        public string LinkedTransferText
        {
            get { return _linkedTransferText; }
            set { _linkedTransferText = value; OnPropertyChanged(nameof(LinkedTransferText)); }
        }

        public List<Element> LinkList { get; set; }

        public long? DirectionElID
        {
            get { return _directionElID; }
            set { _directionElID = value; OnPropertyChanged(nameof(DirectionElID));}
        }

        public long LinkElID
        {
            get { return _linkElID; }
            set { _linkElID = value;OnPropertyChanged(nameof(LinkElID)); }
        }

        public long SectionElID => (long)AsgFinTr.Sections.BackOffice;


        public long? LinkedRegisterID
        {
            get { return _linkedRegisterID; }
            set { _linkedRegisterID = value; OnPropertyChanged(nameof(LinkedRegisterID));
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public long? LinkedTransferID
        {
            get { return _linkedTransferID; }
            set { _linkedTransferID = value; OnPropertyChanged(nameof(LinkedTransferID));
                OnPropertyChanged(nameof(IsValid));
            }
        }

        public bool IsTransferAllowed
        {
            get { return _isTransferAllowed; }
            set { _isTransferAllowed = value; OnPropertyChanged(nameof(IsTransferAllowed)); }
        }

        public AddOpfrTransferViewModel(long direction)
        {
            DirectionElID = direction;

            LinkList = BLServiceSystem.Client.GetElementByType(Element.Types.PPLink);


            SelectRegisterCommand = new DelegateCommand( o => { SelectRegisterExecute(); });
            ClearRegisterCommand = new DelegateCommand(o => LinkedRegisterID.HasValue, o => { SetRegister(null); });
            SelectTransferCommand = new DelegateCommand(o=> LinkedRegisterID.HasValue, o => { SelectTransferExecute(); });
            ClearTransferCommand = new DelegateCommand(o => LinkedRegisterID.HasValue && LinkedTransferID.HasValue, o => { ResetTransfer(); });

        }

        public bool IsValid => LinkedRegisterID.HasValue && LinkedTransferID.HasValue;

        private void SelectRegisterExecute()
        {
            var selected = DialogHelper.ShowSelectRegisterForPP((AsgFinTr.Sections)SectionElID, DirectionElID ?? 0);
            if (selected != null)
            {
                SetRegister(selected);
                //При смене реестра сбрасываем связи
                ResetTransfer();
            }
        }

        private void SetRegister(CommonRegisterListItem register)
        {
            if (register != null)
            {
                LinkedRegisterID = register.ID;
                LinkedRegisterText = register.Name;
                IsTransferAllowed = true;
            }
            else
            {
                LinkedRegisterID = null;
                LinkedRegisterText = null;
                IsTransferAllowed = false;
            }
            ResetTransfer();
        }

        private void ResetTransfer()
        {
            LinkedTransferID = null;
            LinkedTransferText = null;
            LinkElID = (long)AsgFinTr.Links.NoLink;
        }

        private void SelectTransferExecute()
        {
            if (!LinkedRegisterID.HasValue)
                return;
                    var opfr = DialogHelper.SelectReqTransferOPFR(LinkedRegisterID.Value);
                    if (opfr != null)
                        SetTransfer(opfr);
        }

        private void SetTransfer(ReqTransferListItem transfer)
        {
            if (transfer == null) ResetTransfer();
            else
            {
                LinkedTransferID = transfer.ID;
                LinkedTransferText = transfer.UKName;
                LinkElID = (long) AsgFinTr.Links.OPFR;
                IsTransferAllowed = true;
            }
        }
    }
}
