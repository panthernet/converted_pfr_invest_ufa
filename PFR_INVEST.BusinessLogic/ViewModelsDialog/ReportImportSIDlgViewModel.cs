﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using System.Globalization;
using System.Threading;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Interfaces;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using SIReport;
    using Journal;

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OVSI_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OVSI_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class ReportImportSIDlgViewModel : ViewModelBase, IRequestCloseViewModel
    {
        private readonly List<Year> _years;
        private readonly List<Month> _monthes;


        public DelegateCommand<object> OpenFileCommand { get; private set; }
        //public DelegateCommand<object> OpenCatalogCommand { get; private set; }
        public DelegateCommand ImportCommand { get; private set; }


        public ReportImportSIDlgViewModel()
        {
            SetReportTypes();
            _years = DataContainerFacade.GetList<Year>();
            _monthes = DataContainerFacade.GetList<Month>();
            SelectedMonth = Monthes.FirstOrDefault();
            SelectedYear = Years.FirstOrDefault();

            OpenFileCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenFile);
            //OpenCatalogCommand = new DelegateCommand<object>(o => CanExecuteOpenFile(), OpenCatalog);
            ImportCommand = new DelegateCommand(o => CanExecuteImport(), ExecuteImport);
        }

        protected virtual void SetReportTypes()
        {
            ReportTypes = DataContainerFacade.GetList<SIReportType>().Where(tr => tr.Import == 1).OrderBy(r => r.Order).Take(1).Cast<BaseReportType>().ToList();
        }

        public List<BaseReportType> ReportTypes
        {
            get;
            set;
        }

        BaseReportType _selectedReportType;
        public BaseReportType SelectedReportType
        {
            get { return _selectedReportType; }
            set
            {
                _selectedReportType = value;
                SetImportHandler();
                ImportHandler.HeaderNameReport = _selectedReportType.FullName;
                ImportHandler.ReportName = _selectedReportType.ShortName;
                OnPropertyChanged("SelectedReportType");
                OnPropertyChanged("SelectedReportDescription");
                OnPropertyChanged("SelectedMonth");
                OnPropertyChanged("SelectedYear");
                FileName = "";
                //OnPropertyChanged("FileName");
            }
        }

        public List<Month> Monthes => _monthes;

        public List<Year> Years => _years;

        public Month SelectedMonth
        {
            get
            {
                return ImportHandler.SelectedMonth;
            }
            set
            {
                ImportHandler.SelectedMonth = value;
                OnPropertyChanged("SelectedMonth");

                if (!string.IsNullOrEmpty(FileName))
                    LoadFiles(new[] { FileName });
            }
        }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        public Year SelectedYear
        {
            get
            {
                return ImportHandler.SelectedYear;
            }
            set
            {
                ImportHandler.SelectedYear = value;
                OnPropertyChanged("SelectedYear");
                if (!string.IsNullOrEmpty(FileName))
                    LoadFiles(new[] { FileName });
            }
        }

        protected virtual void SetImportHandler()
        {
            ImportHandler = ReportImportSIFactory.GetImportHandler(this);
            //ImportHandler.OnLoadAlredyLoadedFileHandler = new PfrBranchReportImportHandlerBase.LoadAlredyLoadedFileHandler(ConfirmLoadAlreadyLoadedFile);
        }

        public string SelectedReportDescription => SelectedReportType == null ? null : SelectedReportType.Description;


        private ReportImportSIHandlerBase _importHandler;
        protected ReportImportSIHandlerBase ImportHandler
        {
            get { return _importHandler ?? (_importHandler = new ReportImportSIHandlerEmpty()); }
            set
            { _importHandler = value; }
        }


        private bool CanExecuteOpenFile()
        {
            return ImportHandler.CanExecuteOpenFile();
        }

        private void OpenFile(object o)
        {
            var fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            OnPropertyChanged("FileName");
        }


        private void LoadFiles(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;


            var oldCI = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                foreach (string fileName in fileNames)
                {
                    try
                    {
                        FileName = fileName;
                        string message;
                        if (ImportHandler.LoadFile(fileName, out message) || string.IsNullOrEmpty(message)) continue;
                        DialogHelper.ShowAlert(message);
                    }

                    catch (Exception ex)
                    {
                        // DialogHelper.Alert(ex.Message);
                        DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
                        Logger.WriteException(ex);
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCI;
            }
        }

        public bool CanExecuteImport()
        {
            return !string.IsNullOrEmpty(FileName) && ImportHandler.CanExecuteImport();
        }

        private void ExecuteImport(object o)
        {
            if (!CanExecuteImport())
                return;


            string message;

            if (ImportHandler.ExecuteImport(out message))
            {
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.IMPORT_DATA, null, "Название отчета: " + SelectedReportType.ShortName);
                DialogHelper.ShowAlert(string.Format("Импорт файла {0} выполнен", FileName));
                DialogHelper.ShowAlert(message);
            }
            else
            {
                DialogHelper.ShowAlert(string.Format("Импорт файла {0}\n не выполнен", FileName));
            }
            FileName = "";
        }


        public event EventHandler RequestClose;

        public void CloseWindow()
        {
            if (RequestClose != null)
                RequestClose(this, null);
        }

        //   private void OpenCatalog(object o)
        //{
        //    var directoryPath = DialogHelper.OpenCatalog();

        //    if (directoryPath == null)
        //        return;

        //    List<string> fileNames = new List<string>();

        //    foreach (string mask in ImportHandler.GetOpenDirectoryMask().Split(';'))
        //    {
        //        fileNames.AddRange(Directory.GetFiles(directoryPath, mask, SearchOption.TopDirectoryOnly));
        //    }

        //    if (fileNames.Count == 0)
        //        return;

        //    LoadFiles(fileNames.ToArray());
        //    OnPropertyChanged("FileName");
        //}
        //public bool ConfirmLoadAlreadyLoadedFile(PfrBranchReportImportContentBase fileParamInfo) {
        //    String msg = "Данные по отчету {regname} на {year} год и {month} месяц уже загружены ранее.\nЖелаете переписать данные?";
        //    msg = msg.Replace("{regname}", fileParamInfo.RegionName).Replace("{year}", fileParamInfo.PeriodYear.ToString()).Replace("{month}", fileParamInfo.PeriodMonth.HasValue ? fileParamInfo.PeriodMonth.Value.ToString() : "");
        //    return DialogHelper.Confirm(msg); 
        //}



    }
}
