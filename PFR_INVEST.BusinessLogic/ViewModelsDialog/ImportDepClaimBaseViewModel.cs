﻿using System;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
	public abstract class ImportDepClaimBaseViewModel : ViewModelCard
	{
        public bool IsVerbose { get; set; } = true;
        public bool IsAttemptFailed { get; protected set; } = false;
        //public long AuctionID { get; protected set; }

	    protected ImportDepClaimBaseViewModel(params Type[] pListViewModelCollection)
			: base(pListViewModelCollection)
		{
		}

		public virtual bool ValidateCanImport()
		{
			var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(ID);
			if (auction == null || !auction.StockId.Equals((int)Stock.StockID.MoscowStock))
			{
				DialogHelper.ShowAlert("Данная операция временно недоступна для выбранной биржи");
				return false;
			}
			return true;
		}
	}
}
