﻿
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using System;
    using ViewModelsPrint;
    using DataObjects;
    using DataAccess.Client;
    using Commands;
    using Journal;

    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker)]
    public class ExportDepClaim2ViewModel : ExportDepClaimBaseViewModel
    {
        public enum Modes
        {
            New,
            Confirmed
        }

        private readonly DateTime _minDate = new DateTime(2000, 1, 1);
        public Modes Mode { get; }
        public DelegateCommand PrintCommand { get; private set; }

        public ExportDepClaim2ViewModel(long auctionId, Modes mode)
        {
            PrintCommand = new DelegateCommand(CanExecutePrint, OnPrint_Execute);
            Mode = mode;
            AuctionID = auctionId;
        }

        public override bool ValidateCanExport()
        {
			if (!base.ValidateCanExport())
			{
				return false;
			}

            var depClaim2Count = BLServiceSystem.Client.GetDepClaim2Count(AuctionID, Mode == Modes.Confirmed);
            if (depClaim2Count != 0) return true;
            DialogHelper.ShowAlert("Нет заявок для экспорта");
            return false;
        }

        private DateTime _date = DateTime.Now;
        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (_date == value) return;
                _date = value;
                OnPropertyChanged("Date");
            }
        }

        private bool CanExecutePrint(object o)
        {
            return Validate();
        }

        protected void OnPrint_Execute(object o)
        {
            var auction = DataContainerFacade.GetByID<DepClaimSelectParams>(AuctionID);
            switch (Mode)
            {
                case Modes.New:
                    {
                        var print = new PrintExportDepClaim2(auction, Date);
                        print.Print();
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.EXPORT_DATA);                          
                    }
                    break;
                case Modes.Confirmed:
                    {
                        var print = new PrintExportDepClaim2Confirm(auction, Date);
                        print.Print();
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.EXPORT_DATA);                          
                    }
                    break;
            }
        }

        protected override void ExecuteSave()
        {
            throw new NotImplementedException();
        }

        public override bool CanExecuteSave()
        {
            return false;
        }

        public override string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Date": return (Date < _minDate) ? "Дата должна быть больше 2000-го года" : null;
                    default: return null;
                }
            }
        }

        private bool Validate()
        {
            return Date >= _minDate;
        }
    }
}
