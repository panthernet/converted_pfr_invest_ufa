﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataObjects;
using PFR_INVEST.BusinessLogic.Commands;
using System.Windows;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;

namespace PFR_INVEST.BusinessLogic.ViewModelsDialog
{
    using PFR_INVEST.BusinessLogic.VRReport;
    using PFR_INVEST.BusinessLogic.Journal;

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_directory_editor, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_directory_editor, DOKIP_ROLE_TYPE.OUFV_manager)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_directory_editor, DOKIP_ROLE_TYPE.OUFV_manager)]
    public class ReportExportVRDlgViewModel : ViewModelBase
    {
        public DelegateCommand ExportCommand { get; private set; }
        


        public ReportExportVRDlgViewModel()
        {
            ReportTypes = DataContainerFacade.GetList<VRReportType>().Where(tr => tr.Import == 0).OrderBy(r => r.Order).Take(2).ToList();


            
            ExportCommand = new DelegateCommand(o => CanExecuteExport(), ExecuteExport);

        }

        private bool CanExecuteExport()
        {
            return ExportHandler.CanExecuteExport();
        }

        private void ExecuteExport(object o)
        {
            try
            {
                ExportHandler.ExecuteExport();
                JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.EXPORT_DATA, null, "Тип отчета: " + SelectedReportType.ShortName);              
            }
            catch (Exception ex)
            {
                DialogHelper.ShowAlert("Ошибка экспорта отчета");
                Logger.WriteException(ex);
            }
        }

     



        VRReportType _SelectedReportType;
        public VRReportType SelectedReportType
        {
            get { return _SelectedReportType; }
            set
            {
                _SelectedReportType = value;
                SetExportHandler();

                OnPropertyChanged("SelectedReportType");
                OnPropertyChanged("SelectedReportDescription");

                OnPropertyChanged("SelectedYear");
                OnPropertyChanged("SelectedMonth");

                OnPropertyChanged("Years");
                OnPropertyChanged("Monthes");





                OnPropertyChanged("IsYearEnabled");
                OnPropertyChanged("YearVisibility");

                OnPropertyChanged("IsMonthEnabled");
                OnPropertyChanged("MonthVisibility");


                OnPropertyChanged("IsResponsibleEnabled");
                OnPropertyChanged("ResponsibleVisibility");

                OnPropertyChanged("IsPerformerEnabled");
                OnPropertyChanged("PerformerVisibility");

                //OnPropertyChanged("PeriodLengthVisibility");
                //OnPropertyChanged("IsPeriodLengthEnabled");

                //OnPropertyChanged("IsMonthly");
                //OnPropertyChanged("IsAnnual");
                //OnPropertyChanged("IsMonthVisible");
            }
        }

        private void SetExportHandler()
        {
            ExportHandler = ReportExportVRFactory.GetExportHandler(this);
            if(!ExportHandler.IsReportContainsData)DialogHelper.ShowAlert("Данных для экспорта нет!");
        }

        public string SelectedReportDescription
        {
            get
            {
                if (SelectedReportType == null) return null;
                return SelectedReportType.Description;
            }
        }

        private ReportExportVRHandlerBase _ExportHandler;
        private ReportExportVRHandlerBase ExportHandler
        {
            get
            {
                if (_ExportHandler == null)
                    _ExportHandler = new ReportExportVRHandlerEmpty();

                return _ExportHandler;
            }
            set
            { _ExportHandler = value; }
        }

        public List<VRReportType> ReportTypes { get; private set; }

       

        public Person Responsible
        {
            get { return ExportHandler.Responsible; }
            set
            {
                if (_ExportHandler.Responsible != value)
                {
                    ExportHandler.Responsible = value;
                    OnPropertyChanged("Responsible");
                   
                }
            }
        }


        public Person Performer
        {
            get { return ExportHandler.Performer; }
            set
            {
                if (ExportHandler.Performer != value)
                {
                    ExportHandler.Performer = value;
                    OnPropertyChanged("Performer");
                  
                }
            }
        }




        public List<Month> Monthes
        {
            get
            {
                return ExportHandler.Monthes;
            }
            set
            {
                ExportHandler.Monthes = value;
                OnPropertyChanged("Monthes");
            }


        }

        public Month SelectedMonth
        {
            get
            {
                return ExportHandler.SelectedMonth;
            }
            set
            {
                ExportHandler.SelectedMonth = value;
                OnPropertyChanged("SelectedMonth");
            }
        }

        public List<Year> Years
        {
            get
            {
                return ExportHandler.Years;
            }
            set
            {

                ExportHandler.Years = value;
                OnPropertyChanged("Years");
            }

        }


        public Year SelectedYear
        {
            get
            {
                return ExportHandler.SelectedYear;
            }
            set
            {
                ExportHandler.SelectedYear = value;
                OnPropertyChanged("SelectedMonth");
                OnPropertyChanged("Monthes");
                OnPropertyChanged("SelectedYear");
            }
        }



        [Obsolete]
        internal bool AskReportDataIncomplete(object p)
        {
            return AskReportDataIncomplete(p.ToString());
        }

        internal bool AskReportDataIncomplete(string s)
        {
            string message = string.Format("{0}\r\nПродолжить формирование отчета?", s);

            return DialogHelper.ShowConfirmation(message);
        }

        public bool IsYearEnabled => ExportHandler.IsYearEnabled;

        public Visibility YearVisibility => ExportHandler.IsYearVisible ? Visibility.Visible : Visibility.Collapsed;


        public bool IsMonthEnabled => ExportHandler.IsMonthEnabled;

        public Visibility MonthVisibility => ExportHandler.IsMonthVisible ? Visibility.Visible : Visibility.Collapsed;


        public bool IsResponsibleEnabled => ExportHandler.IsResponsibleEnabled;

        public Visibility ResponsibleVisibility => ExportHandler.IsResponsibleVisible ? Visibility.Visible : Visibility.Collapsed;


        public virtual bool IsPerformerEnabled => ExportHandler.IsPerformerEnabled;


        public Visibility PerformerVisibility => ExportHandler.IsPerformerVisible ? Visibility.Visible : Visibility.Collapsed;


        public Visibility PeriodLengthVisibility => ExportHandler.IsPeriodLengthVisible ? Visibility.Visible : Visibility.Collapsed;


        public bool IsPeriodLengthEnabled => ExportHandler.IsPeriodLengthEnabled;

        public bool IsMonthly
        {
            get { return ExportHandler.IsMonthly; }
            set
            {
                if (ExportHandler.IsMonthly != value)
                {
                    ExportHandler.IsMonthly = value;
                    OnPropertyChanged("IsMonthly");
                    OnPropertyChanged("IsAnnual");
                    OnPropertyChanged("MonthVisibility");
                }
            }
        }

        public bool IsAnnual
        {
            get { return ExportHandler.IsAnnual; }
            set
            {
                if (ExportHandler.IsAnnual != value)
                {
                    ExportHandler.IsAnnual = value;
                    OnPropertyChanged("IsMonthly");
                    OnPropertyChanged("IsAnnual");
                    OnPropertyChanged("MonthVisibility");
                }
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "SelectedYear":
                        //if (SelectedYear == null || SelectedYear < 0)
                        return "Выбранный год имеет неправильное значение";
                    default: return null;
                }

            }
        }

        public virtual bool Validate()
        {
            var props = this.GetType().GetProperties().Where(p => p.CanRead && p.CanWrite);
            foreach (var p in props)
            {
                var error = this[p.Name];
                if (!string.IsNullOrEmpty(error))
                {
                    return false;
                }
            }
            return true;
        }








    }
}
