﻿using System.Collections.Generic;
using PFR_INVEST.DataObjects;
using System;
using PFR_INVEST.Auth.SharedData.Auth;


//    public class ModelContactRecord
//    {
//        private readonly Contact m_Contact;

//        //public ModelContactRecord(Contact _contact)
//        //{
//        //    m_Contact = _contact;
//        //}
//        
//        public  long ContactID
//        {
//            get
//            {
//                return m_Contact.ID;
//            }
//        }
//        
//        public  string UkName
//        {
//            get
//            {
//                return m_Contact.GetLegalEntity().FormalizedName;
//            }
//        }
//        
//        public string FIO
//        {
//            get
//            {
//                return m_Contact.FIO;
//            }
//        }
//        
//        public  string Email
//        {
//            get
//            {
//                return m_Contact.Email;
//            }
//        }
//    }





namespace PFR_INVEST.BusinessLogic
{

    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OARRS_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SendEmailsToUKDialogModel : ViewModelListDialog
    {
        const string LOG_PREFIX = "Логирование грида 'Отправить письма в УК'";

        private List<ModelContactRecord> m_Contacts;
        public List<ModelContactRecord> Contacts
        {
            get
            {
                return m_Contacts;
            }
            set
            {
                m_Contacts = value;
                OnPropertyChanged("Contacts");
            }
        }


        protected override void ExecuteRefreshList(object param)
        {
            Log(string.Format("{0}: обращение к wcf сервису для получения данных, время: {1}.", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));
            Contacts = BLServiceSystem.Client.GetModelContactRecords();// BLServiceSystem.Client.GetContactsForUKs().ConvertAll<ModelContactRecord>(c => new ModelContactRecord(c));
            Log(string.Format("{0}: получены данные из {1} записей, время: {2}.", LOG_PREFIX, Contacts == null ? 0 : Contacts.Count, DateTime.Now.ToString("HH:mm:ss:ffff")));
            Log(string.Format("{0}: Грид загружен: {1}", LOG_PREFIX, DateTime.Now.ToString("HH:mm:ss:ffff")));

        }

        protected override bool CanExecuteRefreshList()
        {
            return true;
        }

        //For test purpose
        protected void Log(string message)
        {
            if (Logger != null)
                Logger.WriteLine(message);
        }
    }
}
