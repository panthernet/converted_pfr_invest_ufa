﻿using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OVSI_manager)]
    public class SelectReqTransferForAssPayDlgViewModel : ViewModelCardDialog
	{
        public IList<ReqTransferWithStatsListItem> List { get; private set; }

        public SelectReqTransferForAssPayDlgViewModel(long ukPlanID)
        {
            List = BLServiceSystem.Client.GetReqTransfersWithStatsFromUkPlanId(ukPlanID);
        }


        private ReqTransferWithStatsListItem _selectedItem;
        public ReqTransferWithStatsListItem SelectedItem
		{
			get { return _selectedItem; }
			set {
			    if (_selectedItem == value) return;
			    _selectedItem = value; OnPropertyChanged("SelectedItem");
			}
		}

        public override bool CanExecuteSave()
        {
            return SelectedItem != null;
        }}
}
