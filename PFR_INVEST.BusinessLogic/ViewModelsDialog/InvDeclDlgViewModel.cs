﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using PFR_INVEST.DataObjects;
using ex = Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.Common.Exceptions;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic
{
    public abstract class InvDeclDlgViewModel : ViewModelBase
    {
        private const string TEMPLATE_NAME = "Инвестиционные декларации.xls";
        private ex.Application _mApp;
        private ex.Worksheet _mSheet;
        private ex.Range _range;


        protected abstract List<Contract> GetContracts(List<long> idList, bool showEnded);
        protected abstract List<SIListItem> GetSIList(bool withContacts);

        private void Generate(bool showOldNames, bool showEndedAgreements)
        {
			//long time1=0;
			//long time2=0;
			//long time3=0;
			//long time4 = 0;
			//long time5 = 0;
			//long time6 = 0;
			//long time7 = 0;
           // var timer = Stopwatch.StartNew();
            var silist = GetSIList(false);
            //timer.Stop();
            //time1 = timer.ElapsedMilliseconds;
            var bgcolor = ColorTranslator.ToOle(Color.FromArgb(255, 204, 153));
            try
            {
                int startnum = 3;
                var numsBottomsolid = new[] { 18, 21, 34, 41, 52, 57, 59, 61, 63 };
                //var nums_topsolid = new int[] { 16, 19, 22, 35, 42, 53, 58, 60, 62 };
                var numsBottomdot = new[] { 26, 28, 31, 32, 33, 38, 39, 40, 49, 50, 51 };

                //получаем список всех идентификаторов СИ
                var leIdList = silist.Select(a => a.LegalEntityID).ToList();
                //получаем все контракты для всех СИ (для ускорения работы)
               // timer.Restart();
                var contractsList = GetContracts(leIdList, showEndedAgreements);
                //фильтруем все УК без договоров
                silist = silist.Where(a => contractsList.Any(c => c.LegalEntityID == a.LegalEntityID)).ToList();
               // timer.Stop();
                //time2 += timer.ElapsedMilliseconds;
                //выбираем все старые наименования СИ, если треьуется (для ускуорения работы)
                var oldNamesList = new List<OldSIName>();
                if (showOldNames)
                {
                    //timer.Restart();
                    oldNamesList = BLServiceSystem.Client.GetOldSINamesForInvDecl(leIdList);
                    //timer.Stop();
                    //time3 += timer.ElapsedMilliseconds;
                }
                
                //выбираем все ContrInvest дял всех контрактов (для ускорения работы)
               // timer.Restart();
                var contrInvestIList = BLServiceSystem.Client.GetAllContrInvestForInvDecl(contractsList.Select(a=> a.ID).ToList());
                //timer.Stop();
                //time4 += timer.ElapsedMilliseconds;

                var ciIdList = contrInvestIList.Select(a => a.ID).ToList();
                //
                //timer.Restart();
                var oblAllList = BLServiceSystem.Client.GetOBLContrInvestForInvDecl(ciIdList);
                //timer.Stop();
                //time5 += timer.ElapsedMilliseconds;
                //
                //timer.Restart();
                var akcAllList = BLServiceSystem.Client.GetAKCContrInvestForInvDecl(ciIdList);
                //timer.Stop();
                //time6 += timer.ElapsedMilliseconds;
                //
                //timer.Restart();
                var payAllList = BLServiceSystem.Client.GetPAYContrInvestForInvDecl(ciIdList);
                //timer.Stop();
                //time7 += timer.ElapsedMilliseconds;

                _mSheet.Range["E5"].Cells.Value2 = _mSheet.Range["E5"].Cells.Value2.ToString().Replace("{DATE}", DateTime.Now.ToString("dd.MM.yy"));
                var last = silist.Last();
                foreach (var item in silist)
                {
                    //НАИМЕНОВАНИЕ УК
                    //мерджим ячейки в ширину только если больше 1 договора, иначе мерджим в высоту
                    var cs = contractsList.Where(a => a.LegalEntityID == item.LegalEntityID).ToList();
                    int cscount = cs.Count == 0 ? 1 : cs.Count;

                    if (item == last)
                    {
                    }

                    var oldNames = oldNamesList.Where(a => a.LegalEntityID == item.LegalEntityID).ToList();

                    int nameXend = 10;
                    if (showOldNames && oldNames.Count > 0)
                        nameXend = 8;

                    _range = _mSheet.Range[_mSheet.Cells[7, startnum], _mSheet.Cells[nameXend, startnum + cscount - 1]];
                    _range.Merge();
                    _range.BorderAround(ex.XlLineStyle.xlContinuous);
                    _range.HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
                    _range.Interior.Color = bgcolor;
                    _range.Value2 = item.FormalizedName;

                    //Cтарое наименование
                    if (showOldNames && oldNames.Count > 0)
                    {
                        _range.Borders[ex.XlBordersIndex.xlEdgeBottom].LineStyle = ex.XlLineStyle.xlLineStyleNone;
                        _range = _mSheet.Range[_mSheet.Cells[9, startnum], _mSheet.Cells[10, startnum + cscount - 1]];
                        _range.Merge();
                        _range.BorderAround(ex.XlLineStyle.xlContinuous);
                        _range.Borders[ex.XlBordersIndex.xlEdgeTop].LineStyle = ex.XlLineStyle.xlLineStyleNone;
                        _range.HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
                        _range.Interior.Color = bgcolor;
                        _range.Interior.Pattern = ex.XlPattern.xlPatternGray16;
                        _range.Value2 = oldNames[0].OldFormalizedName;
                    }

                    for (int i = 0; i < cscount; i++)
                    {
                        //НАИМЕНОВАНИЕ ИНВЕСТ ПОРТФЕЛЯ
                        _range = _mSheet.Range[_mSheet.Cells[11, startnum + i], _mSheet.Cells[13, startnum + i]];
                        _range.Merge();
                        _range.BorderAround(ex.XlLineStyle.xlContinuous);
                        _range.HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
                        _range.Interior.Color = bgcolor;
                        if (cs.Count > 0)
                            _range.Value2 = cs[i].PortfolioName;

                        //Номер и дата договора
                        _range = _mSheet.Range[_mSheet.Cells[14, startnum + i], _mSheet.Cells[15, startnum + i]];
                        _range.Merge();
                        _range.BorderAround(ex.XlLineStyle.xlContinuous);
                        _range.HorizontalAlignment = ex.XlHAlign.xlHAlignCenter;
                        _range.Interior.Color = bgcolor;
                        if (cs.Count > 0)
                            _range.Value2 = string.Format("{0}\n{1}", cs[i].ContractNumber, cs[i].ContractDate.HasValue ? cs[i].ContractDate.Value.ToShortDateString() : "");

                        if (cs.Count > 0)
                        {
                            //Наполнение данными
                            //берем все списки
                            var cinvList = contrInvestIList.Where(a => a.ContractID == cs[i].ID).ToList();
                            
                            foreach (var ciMain in cinvList)
                            {
                                if (!ciMain.GCBRFMIN.HasValue) ciMain.GCBRFMIN = 0;
                                if (!ciMain.GCBRFMAX.HasValue) ciMain.GCBRFMAX = 0;
                                if (!ciMain.GCBSMIN.HasValue) ciMain.GCBSMIN = 0;
                                if (!ciMain.GCBSMAX.HasValue) ciMain.GCBSMAX = 0;
                                if (!ciMain.OBLMIN.HasValue) ciMain.OBLMIN = 0;
                                if (!ciMain.OBLMAX.HasValue) ciMain.OBLMAX = 0;
                                if (!ciMain.IPMIN.HasValue) ciMain.IPMIN = 0;
                                if (!ciMain.IPMAX.HasValue) ciMain.IPMAX = 0;
                                if (!ciMain.SRMIN.HasValue) ciMain.SRMIN = 0;
                                if (!ciMain.SRMAX.HasValue) ciMain.SRMAX = 0;
                                if (!ciMain.DEPMIN.HasValue) ciMain.DEPMIN = 0;
                                if (!ciMain.DEPMAX.HasValue) ciMain.DEPMAX = 0;
                                if (!ciMain.SRVALMIN.HasValue) ciMain.SRVALMIN = 0;
                                if (!ciMain.SRVALMAX.HasValue) ciMain.SRVALMAX = 0;
                                
                                //Государственные ценные бумаги Российской Федерации
                                _range = _mSheet.Range[_mSheet.Cells[16, startnum + i], _mSheet.Cells[16, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.GCBRFMIN == ciMain.GCBSMAX ? ciMain.GCBRFMIN.ToString() : string.Format("{0}-{1}", ciMain.GCBRFMIN, ciMain.GCBRFMAX);
                                //Государственные ценные бумаги субъектов Российской Федерации
                                _range = _mSheet.Range[_mSheet.Cells[19, startnum + i], _mSheet.Cells[19, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.GCBSMIN == ciMain.GCBSMAX ? ciMain.GCBSMIN.ToString() : string.Format("{0}-{1}", ciMain.GCBSMIN, ciMain.GCBSMAX);
                                // - облигации, выпущенные от имени муниципальных образований
                                _range = _mSheet.Range[_mSheet.Cells[27, startnum + i], _mSheet.Cells[27, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.OBLMIN == ciMain.OBLMAX ? ciMain.OBLMIN.ToString() : string.Format("{0}-{1}", ciMain.OBLMIN, ciMain.OBLMAX);
                                var iciMain = ciMain;
                                //  - облигации российских хозяйственных обществ
                                foreach (var obl in oblAllList.Where(a=> a.ContrInvestID == iciMain.ID))
                                {
                                    int cellX = 31;
                                    if (obl.Year.Contains("2004")) cellX = 31;
                                    if (obl.Year.Contains("2005")) cellX = 32;
                                    if (obl.Year.Contains("2006")) cellX = 33;
                                    if (obl.Year.Contains("2007")) cellX = 34;
                                    _range = _mSheet.Range[_mSheet.Cells[cellX, startnum + i], _mSheet.Cells[cellX, startnum + i]];
                                    _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                    _range.Value2 = obl.Min == obl.Max ? obl.Min.ToString() : string.Format("{0}-{1}", obl.Min, obl.Max);
                                }
                                // Акции российских эмитентов, созданных в форме открытых акционерных обществ
                                foreach (var akc in akcAllList.Where(a=> a.ContrInvestID == iciMain.ID))
                                {
                                    int cellX = 38;
                                    if (akc.Year.Contains("2004")) cellX = 38;
                                    if (akc.Year.Contains("2005")) cellX = 39;
                                    if (akc.Year.Contains("2006")) cellX = 40;
                                    if (akc.Year.Contains("2007")) cellX = 41;
                                    _range = _mSheet.Range[_mSheet.Cells[cellX, startnum + i], _mSheet.Cells[cellX, startnum + i]];
                                    _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                    _range.Value2 = akc.Min == akc.Max ? akc.Min.ToString() : string.Format("{0}-{1}", akc.Min, akc.Max);
                                }
                                //Паи (акции, доли) индексных инвестиционных фондов
                                foreach (ContrInvestPAY pay in payAllList.Where(a=> a.ContrInvestID == iciMain.ID))
                                {
                                    int cellX = 49;
                                    if (pay.Year.Contains("2004")) cellX = 49;
                                    if (pay.Year.Contains("2005")) cellX = 50;
                                    if (pay.Year.Contains("2006")) cellX = 51;
                                    if (pay.Year.Contains("2007")) cellX = 52;
                                    _range = _mSheet.Range[_mSheet.Cells[cellX, startnum + i], _mSheet.Cells[cellX, startnum + i]];
                                    _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                    _range.Value2 = pay.Min == pay.Max ? pay.Min.ToString() : string.Format("{0}-{1}", pay.Min, pay.Max);
                                }
                                //Ипотечные ценные бумаги, 
                                _range = _mSheet.Range[_mSheet.Cells[53, startnum + i], _mSheet.Cells[53, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.IPMIN == ciMain.IPMAX ? ciMain.IPMIN.ToString() : string.Format("{0}-{1}", ciMain.IPMIN, ciMain.IPMAX);
                                //Средств в рублях на счетах в кредитных организациях
                                _range = _mSheet.Range[_mSheet.Cells[58, startnum + i], _mSheet.Cells[58, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.SRMIN == ciMain.SRMAX ? ciMain.SRMIN.ToString() : string.Format("{0}-{1}", ciMain.SRMIN, ciMain.SRMAX);
                                //Депозиты в рублях в кредитных организациях
                                _range = _mSheet.Range[_mSheet.Cells[60, startnum + i], _mSheet.Cells[60, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.DEPMIN == ciMain.DEPMAX ? ciMain.DEPMIN.ToString() : string.Format("{0}-{1}", ciMain.DEPMIN, ciMain.DEPMAX);
                                //Средств в иностранной валюте на счетах в кредитных организациях
                                _range = _mSheet.Range[_mSheet.Cells[62, startnum + i], _mSheet.Cells[62, startnum + i]];
                                _range.HorizontalAlignment = ex.XlHAlign.xlHAlignRight;
                                _range.Value2 = ciMain.SRVALMIN == ciMain.SRVALMAX ? ciMain.SRVALMIN.ToString() : string.Format("{0}-{1}", ciMain.SRVALMIN, ciMain.SRVALMAX);
                            }
                        }
                        //общее оформление - столбец
                        _range = _mSheet.Range[_mSheet.Cells[16, startnum + i], _mSheet.Cells[63, startnum + i]];
                        _range.Borders[ex.XlBordersIndex.xlEdgeLeft].LineStyle = ex.XlLineStyle.xlContinuous;
                        _range.Borders[ex.XlBordersIndex.xlEdgeLeft].Weight = ex.XlBorderWeight.xlThin;
                        _range.Borders[ex.XlBordersIndex.xlEdgeRight].LineStyle = ex.XlLineStyle.xlContinuous;
                        _range.Borders[ex.XlBordersIndex.xlEdgeRight].Weight = ex.XlBorderWeight.xlThin;
                        _range.NumberFormat = "@";
                        _range.ColumnWidth = 15;
                        //оформление строк
                        foreach (var num in numsBottomsolid)
                        {
                            _range = _mSheet.Range[_mSheet.Cells[num, startnum + i], _mSheet.Cells[num, startnum + i]];
                            _range.Borders[ex.XlBordersIndex.xlEdgeBottom].LineStyle = ex.XlLineStyle.xlContinuous;
                            _range.Borders[ex.XlBordersIndex.xlEdgeBottom].Weight = ex.XlBorderWeight.xlThin;
                        }
                        foreach (var num in numsBottomdot)
                        {
                            _range = _mSheet.Range[_mSheet.Cells[num, startnum + i], _mSheet.Cells[num, startnum + i]];
                            _range.Borders[ex.XlBordersIndex.xlEdgeBottom].LineStyle = ex.XlLineStyle.xlDash;
                            _range.Borders[ex.XlBordersIndex.xlEdgeBottom].Weight = ex.XlBorderWeight.xlThin;
                        }
                    }
                    startnum += cs.Count;
                    //break;
                }
                _mSheet.Range["C16", "C16"].Select();
                _mApp.DisplayAlerts = true;
                _mApp.Visible = true;
               // Debug.WriteLine("T1: {0}s \nT2: {1}s \nT3: {2}s \nT4: {3}s \nT5: {4}s \nT6: {5}s \nT7: {6}s", time1 / 1000D, time2 / 1000D, time3 / 1000D, time4 / 1000D, time5 / 1000D, time6 / 1000D, time7 / 1000D);
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                if(_mSheet != null)
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_mSheet);
                if (_range != null)
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_range);
                if (_mApp != null)
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(_mApp);
            }
        }

        public void Print(bool oldnames, bool endagr)
        {
            try
            {
                object filePath = TemplatesManager.ExtractTemplate(TEMPLATE_NAME);

                if (File.Exists(filePath.ToString()))
                {
                    RaiseStartedPrinting();
                    _mApp = new ex.Application
                    {
                        DisplayAlerts = false,
                        Visible = false
                    };

                    var oldCI = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    try
                    {
                        var workBook = _mApp.Workbooks.Open(filePath.ToString());
                        _mSheet = (ex.Worksheet)workBook.Sheets[1];
                        Generate(oldnames, endagr);
                        JournalLogger.LogModelEvent(this, DataObjects.Journal.JournalEventType.SELECT, this is VRInvDeclDlgViewModel ? "ВР" : "CИ");
                    }
                    finally
                    {
                        Thread.CurrentThread.CurrentCulture = oldCI;
                    }
                    RaiseFinishedPrinting();
                }
                else
                {
                    RaiseErrorLoadingTemplate(TEMPLATE_NAME);
                }
            }
            catch (Exception ex)
            {
                throw new MSOfficeException(ex.Message);
            }
        }
    }
}
