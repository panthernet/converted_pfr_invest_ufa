﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.ListExtensions;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.OpfrTransfer
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_worker)]
    [DeleteAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OFPR_manager)]
    public class OpfrTransferWizardViewModel: ViewModelCardDialog, IWizardExModel
    {
        public int Step { get; set; }
        public bool IsClosed { get; set; }

        public OpfrTransferWizardViewModel() 
            : base(true)
        {
            IsOnlyOneInstanceAllowed = true;
            IsNotSavable = true;
            _register = new OpfrRegister();
            ID = -1;
        }

        public bool FinalAction()
        {
            //create register
            _register.ID = DataContainerFacade.Save(_register);
            ImportHandler.ResultTransfersList.ForEach(a =>
            {
                // Обнуляем перед импортом все идентификаторы
                a.ID = 0;
                a.OpfrRegisterID = _register.ID;
            });

            //import 
            string message;
            if (!OpfrTransfersImportHandler.ExecuteImportPart(_register, ImportHandler.ResultTransfersList.Select(a =>
            {
                //преобразуем ListItem в базоый класс
                var res = new DataObjects.OpfrTransfer();
                a.CopyTo(res);
                return res;
            }).ToList(), _fileName, out message))
            {
                DialogHelper.ShowError(message);
                //throw new Exception(message);
            }

            //request
            BLServiceSystem.Client.OpfrRegisterUpdateRequest(_register);

            //export (далее вызывается на форме View т.к. требует спец логики форм)
            ID = _register.ID;

            //update models
            ViewModelManager.UpdateDataInAllModels(new List<KeyValuePair<Type, long>> { new KeyValuePair<Type, long>(typeof(OpfrRegister), _register.ID)});
            return true;
        }

        public void OnShow()
        {
            switch (Step)
            {
                case 2:
                    break;
                case 3:
                    OnShowEditOpfrTransfers();
                    OnPropertyChanged("OpfrRegisterText");
                    break;
                case 4:
                    OnCreateOpfrRequestShow();
                    break;
                case 5:
                    OnExportOpfrDataShow();
                    break;
            }
        }

        public bool OnMovePrev()
        {
            switch (Step)
            {
                case 4:
                    return OnCreateOpfrRequestPrev();
            }
            return true;
        }

        public bool OnMoveNext()
        {
            switch (Step)
            {
                case 2:
                    return OnImportTransfersComplete();
            }
            return true;
        }

        public void OnInitialization()
        {
            switch (Step)
            {
                case 1:
                    InitializeCreateOpfrRegister();
                    break;
                case 2:
                    OnImportTransfersInitialize();
                    break;
                case 3:
                    OnEditOpfrTransfersInitialization();
                    break;
            }
        }

        #region Validation

        public bool CanExecuteNext()
        {
            return CanExecuteSave();
        }

        public bool CanExecutePrev()
        {
            return Step != 1;
        }

        public override bool CanExecuteSave()
        {
            switch (Step)
            {
                case 1:
                case 4:
                    return ValidateFields(Step == 1 ? "YearID|DocKindID|MonthID|RegTypeID|Comment|RegNum" : "TrancheDate|TrancheNumber");
                case 2:
                    return ImportHandler.CanExecuteImport();
                case 3:
                    return
                        ValidateFields("Comment") && ImportHandler.ResultTransfersList.Count > 0 
                            && ImportHandler.ResultTransfersList.All(x => x.ValidateFields());
                case 5:
                    return ExportList.Any(a => a.Selected);
                default:
                    return false;
            }
        }

        public override string this[string columnName]
        {
            get
            {
                switch (Step)
                {
                    case 1:
                        switch (columnName)
                        {
                            case "YearID":
                                return _register.YearID == 0 ? "Значение не задано!" : null;
                            case "DocKindID":
                                return _register.KindID == 0 ? "Значение не задано!" : null;
                            case "MonthID":
                                return _register.MonthID == 0 ? "Значение не задано!" : null;
                            case "RegTypeID":
                                return _register.RegTypeID == 0 ? "Значение не задано!" : null;
                            case "Comment":
                                return _register.Comment.ValidateMaxLength(1000);
                            case "RegNum":
                                return _register.RegNum.ValidateMaxLength(50);
                        }
                        break;
                    case 3:
                        switch (columnName)
                        {
                            case "Comment":
                                return Comment.ValidateMaxLength(1000);
                        }
                        break;
                    case 4:
                        switch (columnName.ToLower())
                        {
                            case "tranchedate": return TrancheDate == null ? "Необходимо указать дату заявки" : null;
                            case "tranchenumber":
                                return string.IsNullOrWhiteSpace(TrancheNumber) ? "Необходимо указать номер заявки" : TrancheNumber.ValidateMaxLength(50);
                        }
                        break;
                    default:
                        return null;
                }
                return null;
            }
        }
        #endregion

        #region CreateOpfrTransfer

        private void InitializeCreateOpfrRegister()
        {
            MonthList = DataContainerFacade.GetList<Month>().OrderBy(a => a.ID).ToList();
            YearList = DataContainerFacade.GetList<Year>().OrderBy(a => a.ID).ToList();
            DocKindList = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.OpfrDirection, true).OrderBy(a => a.Name).ToList();
            RegTypeList = DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.OpfrRegType, true).OrderBy(a => a.Name).ToList();

            Date = DateTime.Now.Date;
            RegTypeID = (long)Element.SpecialDictionaryItems.OpfrRegTypeRasp;
            DocKindID = (long)Element.SpecialDictionaryItems.OpfrDirectionFromPfr;
            MonthID = Date.Value.Month;
            YearID = Date.Value.Year - 2000;
            IsDataChanged = true;
        }

        private readonly OpfrRegister _register;

        public List<Month> MonthList { get; set; }
        public List<Year> YearList { get; set; }
        public List<Element> DocKindList { get; set; }
        public List<Element> RegTypeList { get; set; }

        public long YearID
        {
            get { return _register.YearID; }
            set { _register.YearID = value; OnPropertyChanged("YearID"); }
        }

        public long MonthID
        {
            get { return _register.MonthID; }
            set { _register.MonthID = value; OnPropertyChanged("MonthID"); }
        }

        public long DocKindID
        {
            get { return _register.KindID; }
            set { _register.KindID = value; OnPropertyChanged("DocKindID"); }
        }

        public long RegTypeID
        {
            get { return _register.RegTypeID; }
            set { _register.RegTypeID = value; OnPropertyChanged("RegTypeID"); }
        }

        public DateTime? Date
        {
            get { return _register.RegDate; }
            set { _register.RegDate = value; OnPropertyChanged("Date"); }
        }

        public string RegNum
        {
            get { return _register.RegNum; }
            set { _register.RegNum = value; OnPropertyChanged("RegNum"); }
        }

        public string Comment
        {
            get { return _register.Comment; }
            set { _register.Comment = value; OnPropertyChanged("Comment"); }
        }
        #endregion

        #region ImportOpfrTransfers
        public DelegateCommand<object> OpenFileCommand { get; private set; }
        public List<OpfrTransferImportListItem> OpfrTransferListItems => ImportHandler?.Items;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; OnPropertyChanged("FileName"); }
        }

        private OpfrTransfersImportHandler _importHandler;
        private string _fileName;

        public OpfrTransfersImportHandler ImportHandler
        {
            get { return _importHandler ?? (_importHandler = new OpfrTransfersImportHandler(_register.ID)); }
            set { _importHandler = value; }
        }

        public void OnImportTransfersInitialize()
        {
            OpenFileCommand = new DelegateCommand(a=> true, OpenFile);
        }

        private bool _needImport;

        private void OpenFile(object o)
        {
            string[] fileNames = DialogHelper.OpenFiles(ImportHandler.OpenFileMask);

            if (fileNames == null)
                return;

            LoadFiles(fileNames);
            _needImport = true;
            OnPropertyChanged("OpfrTransferListItems");
            OnPropertyChanged("FileName");
        }


        private void LoadFiles(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0)
                return;

            var oldCi = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            try
            {
                ImportHandler.Clear();
                foreach (var fileName in fileNames)
                {
                    try
                    {
                        FileName = fileName;
                        string message;
                        if (!ImportHandler.LoadFile(fileName, out message) && !string.IsNullOrEmpty(message))
                        {
                            DialogHelper.ShowAlert(message);
                            ImportHandler.Clear();
                            if (message.Contains("формат файла"))
                                FileName = null;
                            return;
                        }
                    }

                    catch (Exception ex)
                    {
                        // DialogHelper.Alert(ex.Message);
                        DialogHelper.ShowAlert("Файл содержит некорректные данные. Импорт не может быть произведен.\nДетальная информация об ошибке может быть найдена в лог файле приложения.");
                        Logger.WriteException(ex);
                    }
                }
                OnPropertyChanged("OpfrTransferListItems");
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCi;
            }
        }

        public bool OnImportTransfersComplete()
        {
            try
            {
                if (!_needImport) return true;
                string message;
                ImportHandler.Import(_register, _fileName, out message, true);
                return true;
            }
            catch(Exception ex)
            {
                Logger.WriteException(ex, "Opfr wizard step complete " + Step);
                DialogHelper.ShowError("Во время операции произошла ошибка! Детальное сообщение смотрите в логе программы.");
                return false;
            }
        }

        #endregion

        #region EditOpfrTransfers

        public string OpfrRegisterText => $"{RegTypeList.FirstOrDefault(a=> a.ID == _register.RegTypeID)?.Name} № {_register.RegNum} от {_register.RegDate?.ToString("dd.MM.yyyy")} ({MonthList.FirstOrDefault(a=> a.ID == _register.MonthID)?.Name} {YearList.FirstOrDefault(a=> a.ID == _register.YearID)?.Name} {DocKindList.FirstOrDefault(a=> a.ID == DocKindID)?.Name})";

        public OpfrTransferListItem SelectedOpfrTransfer { get; set; }
        public ICommand DeleteTransferCommand { get; set; }

        private void OnEditOpfrTransfersInitialization()
        {
            DeleteTransferCommand = new DelegateCommand(a => SelectedOpfrTransfer != null, o =>
            {
                if (DialogHelper.ShowConfirmation("Удалить выбранное перечисление?") != true) return;
                ImportHandler.ResultTransfersList.Remove(ImportHandler.ResultTransfersList.FirstOrDefault(a=> a.ID == SelectedOpfrTransfer.ID));
                OnPropertyChanged("ImportHandler");
            });
        }

        private void OnShowEditOpfrTransfers()
        {
            _needImport = false;
            OnPropertyChanged("ImportHandler");
        }

        #endregion

        #region CreateOpfrRequest

        public void OnCreateOpfrRequestShow()
        {
			TrancheDate = TrancheDate ?? DateTime.Now;
        }

        private bool OnCreateOpfrRequestPrev()
        {
            return true;
            //TrancheDate = null;
            //TrancheNumber = null;
        }

        public DateTime? TrancheDate
        {
            get
            {
                return _register?.TrancheDate;
            }

            set
            {
                if (_register == null) return;
                _register.TrancheDate = value;
                OnPropertyChanged("TrancheDate");
                OnPropertyChanged("IsValid");
            }
        }

        public string TrancheNumber
        {
            get
            {
                return _register?.TrancheNum;
            }

            set
            {
                if (_register == null) return;
                _register.TrancheNum = value;
                OnPropertyChanged("TrancheNumber");
                OnPropertyChanged("IsValid");
            }
        }
        #endregion

        #region ExportOpfrData
        public ObservableCollection<OnesExportDataItem> ExportList { get; set; } = new ObservableCollection<OnesExportDataItem>();

        private void OnExportOpfrDataShow()
        {
            ExportList.Clear();// = new ObservableCollection<OnesExportDataItem>(BLServiceSystem.Client.GetExportDataItemsList(OnesSettingsDef.IntGroup.Opfr));

            ExportList.Add(new OnesExportDataItem
            {
                ID = _register.ID,
                Item1 = DocKindList.FirstOrDefault(b=> b.ID == _register.KindID)?.Name,
                Item2 = $@"№{_register.RegNum} от {_register.RegDate?.ToString("dd.MM.yyyy")}"
            });
        }
        #endregion


    }
}
