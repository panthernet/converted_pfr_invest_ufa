﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker)]
    public class SIMonthTransferCreationWizardModel : MonthTransferCreationWizardModel
    {
        public SIMonthTransferCreationWizardModel(long operationId)
            : base(operationId, 1)
        {
        }

        protected override List<Year> GetValidYearList()
        {
            return BLServiceSystem.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.SI, OperationTypeId ?? 0, false);
        }

        protected override List<Month> GetValidMonthList(long yearID)
        {
            return BLServiceSystem.Client.GetMonthListCreateSIUKPlanByOperationType(Document.Types.SI, yearID, OperationTypeId ?? 0, OperationContentId);
        }

        protected override List<SIUKPlan> GetUKPlansForYearMonthAndOperation(long monthID, long yearID)
        {
            return BLServiceSystem.Client.GetUKPlansForYearMonthAndOperationForMonthTransferWizard(Document.Types.SI, monthID, yearID, OperationTypeId.Value, OperationContentId).ToList();
        }

        protected override IEnumerable<Type> GetRelatedViewModelTypesToRefresh()
        {
            return new[]
            {
                typeof(SIAssignPaymentsListViewModel),
                typeof(TransfersSIListViewModel),
                typeof(SPNMovementsSIListViewModel),
				typeof(SPNMovementsVRListViewModel),
                typeof(ZLMovementsSIListViewModel)
            };
        }

        public override IEnumerable<Type> GetRelatedAssignPaymentListViewModelTypes()
        {
            return new[]
            {
                typeof(SIAssignPaymentsListViewModel),
                typeof(SIAssignPaymentsArchiveListViewModel)
            };
        }
    }
}
