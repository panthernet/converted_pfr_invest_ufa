﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using ApplicationWord = Microsoft.Office.Interop.Word.Application;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OKIP_directory_editor, DOKIP_ROLE_TYPE.OVSI_directory_editor)]
    public class LetterToNPFWizardViewModel : ViewModelBase
    {
        public long PrintedCount { get; private set; }

        public List<string> BlanksList { get; } = new List<string>(new[]{
            "Бланк исполнительной дирекции",
            "Бланк ПФР"
        });

        private string _mSelectedBlank;
        public string SelectedBlank
        {
            get
            {
                return _mSelectedBlank;
            }
            set
            {
                _mSelectedBlank = value;
                OnPropertyChanged("SelectedBlank");
            }
        }

        public static List<AddressTypeListItem> AddressTypes { get; set; }

        public AddressTypeListItem SelectedAddressType { get; set; }

        public List<ModelNPFRecord> NPFList { get; set; }

        private ApplicationWord _mWApp;

        public bool OpenFolder { get; set; } = true;

        private void InitAddressBox()
        {
            AddressTypes = new List<AddressTypeListItem>
            {
                new AddressTypeListItem {Value = AddressType.Post, Label = "Почтовый адрес"},
                new AddressTypeListItem {Value = AddressType.Fact, Label = "Фактический адрес"},
                new AddressTypeListItem {Value = AddressType.Legal, Label = "Юридический адрес"}
            };

            SelectedAddressType = AddressTypes[0];
        }

        public void EnsureAvailableList()
        {
            if (!_isUk)
            {
				var sf = StatusFilter.NotDeletedFilter;
                NPFList = BLServiceSystem.Client.GetNPFListLEHib(sf).ToList().ConvertAll(item => new ModelNPFRecord(item));
                OnPropertyChanged("NPFList");
            }
            else
            {
                NPFList = BLServiceSystem.Client.GetActiveUKListHib().ToList().ConvertAll(item => new ModelNPFRecord(item));
                OnPropertyChanged("NPFList");
            }
        }

        private readonly bool _isUk;

        public LetterToNPFWizardViewModel(bool isuk)
        {
            _isUk = isuk;
            InitAddressBox();
            SelectedBlank = BlanksList.First();
        }

        public static bool CheckDirectoryName(string folder)
        {
            try
            {
                new DirectoryInfo(folder);
                return folder == Path.GetFullPath(folder);
            }
            catch
            {
                return false;
            }
        }

        public static bool DirectoryExists(string pSDirectoryName)
        {
            return Directory.Exists(pSDirectoryName);
        }

        public enum PrintResult
        {
            OK = 0,
            TemplateNotFound = 1,
            CannotCreateFile = 2,
            CannotCreateDirectory = 3
        }

        public string SelectedFolder;
        public PrintResult PrintLetters()
        {
            try
            {
                if (!Directory.Exists(SelectedFolder))
                    Directory.CreateDirectory(SelectedFolder);
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
                return PrintResult.CannotCreateDirectory;
            }

            foreach (var res in NPFList.Where(item => item.Selected).Select(Print))
            {
                if (res != PrintResult.OK)
                    return res;
                PrintedCount++;
            }

            return PrintResult.OK;
        }

        private static string GetProperHeadName(string pName)
        {
            try
            {
                var names = pName.Trim().Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                return names.Length < 3 ? names[0] : string.Join(" ", names[1], names[2]);
            }
            catch
            {
                //shit happens:(
                return pName;
            }
        }

        private void GenerateDoc(ModelNPFRecord record)
        {
            var npf = record.LE;

            var address = string.Empty;
            if (SelectedAddressType.Value == AddressType.Fact)
                address = npf.StreetAddress;
            else if (SelectedAddressType.Value == AddressType.Legal)
                address = npf.LegalAddress;
            else if (SelectedAddressType.Value == AddressType.Post)
                address = npf.PostAddress;

            OfficeTools.FindAndReplace(_mWApp, "{ADDRESS}", address);
            OfficeTools.FindAndReplace(_mWApp, "{FULLNAME}", npf.FullName);
            OfficeTools.FindAndReplace(_mWApp, "{HEADNAME}", string.IsNullOrEmpty(npf.HeadFullName) ? "" : GetProperHeadName(npf.HeadFullName));
        }

        private PrintResult Print(ModelNPFRecord record)
        {
            var wordTemplateName = string.Format(!_isUk ? "{0}.doc" : "{0} УК.doc", SelectedBlank);
            var extractedFilePath = TemplatesManager.ExtractTemplate(wordTemplateName);

            if (!File.Exists(extractedFilePath))
                return PrintResult.TemplateNotFound;

            try
            {
                //Формат названия "Формализованное наименование - письмо общее.doc" / "Формализованное наименование - письмо исполнительной дирекции.doc" в зависмости от бланка
                var toFile = Log.GetValidPath(SelectedFolder,
                    $"{record.FormalizedName} - {(SelectedBlank == BlanksList.First() ? "письмо исполнительной дирекции" : "письмо общее")}.doc");
                if (File.Exists(toFile))
                    File.Delete(toFile);
                File.Move(extractedFilePath, toFile);
                extractedFilePath = Path.GetFullPath(toFile);
            }
            catch (Exception e)
            {
                Logger.WriteException(e);
                return PrintResult.CannotCreateFile;
            }

            if (File.Exists(extractedFilePath))
            {
                object filePath = extractedFilePath;

                try
                {
                    _mWApp = new ApplicationWord {Visible = false};
                    _mWApp.Documents.Open(ref filePath);

                    GenerateDoc(record);

                    _mWApp.Quit(WdSaveOptions.wdSaveChanges, WdOriginalFormat.wdWordDocument);
                }
                finally
                {
                    if (_mWApp != null)
                        Marshal.FinalReleaseComObject(_mWApp);
                }
            }
            else
            {
                return PrintResult.TemplateNotFound;
            }

            return PrintResult.OK;
        }

        public void OpenCreatedFolder()
        {
            try
            {
                if (OpenFolder)
                    Process.Start(SelectedFolder);
            }
            catch
            {
                // ignored
            }
        }

        public class ModelNPFRecord
        {
            public LegalEntity LE { get; set; }

            public ModelNPFRecord(LegalEntity le)
            {
                LE = le;
            }

            public long ID => LE.ID;

            public string FormalizedName => LE.FormalizedName;

            public bool Selected { get; set; }
        }
    }
}
