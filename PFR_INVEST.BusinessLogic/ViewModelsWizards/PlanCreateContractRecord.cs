﻿using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
	public class PlanCreateContractRecord : PlanContractRecord
	{
		public PlanCreateContractRecord(SIUKPlan ukPlan)
			: base(ukPlan)
		{
		}

		/// <summary>
		/// Временная фактическая сумма
		/// </summary>
		public decimal? FactSum { get; set; }

		private decimal _investmentIncome;
		public decimal? InvestmentIncome
		{
			get { return _investmentIncome; }
			set { _investmentIncome = value ?? 0.00m; }
		}

		private long _quantityZl;
		public long? QuantityZL
		{
			get { return _quantityZl; }
			set { _quantityZl = value ?? 0L; }
		}
	}
}
