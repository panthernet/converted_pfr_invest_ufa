﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards
{
    public static class TransferWizardHelper
    {
        public static List<SPNOperation> GetOperationTypesListForYearPlanMasked(long direction)
        {
            long[] availableOpIdList = { 8L, 10L, 11L };

            //хакерский способ, т.к. форма универсальная и используется SPNOperation, а для мастера плана на год тип операции хранится в Element
            var old = BLServiceSystem.Client.GetSPNOperationsByTypeListHib(direction).Where(op => availableOpIdList.Contains(op.ID)).ToList();
            return DataContainerFacade.GetListByProperty<Element>("Key", (long)Element.Types.RegisterOperationType)
                .Select(a =>
                {
                    var result = new SPNOperationTypeItem { TypeId = a.ID, TypeName = a.Name };
                    switch (result.TypeId)
                    {
                        case (long)Element.SpecialDictionaryItems.PaymentZLType:
                            var from = old.FirstOrDefault(b => b.ID == 8L);
                            if (from != null)
                            {
                                result.ID = from.ID;
                                result.ShowInsuredPersonsCount = from.ShowInsuredPersonsCount;
                                result.ShowMonth = from.ShowMonth;
                                result.ShowPeriod = from.ShowPeriod;
                            }
                            break;
                        case (long)Element.SpecialDictionaryItems.PaymentReserve:
                            var from2 = old.FirstOrDefault(b => b.ID == 10L || b.ID == 11L);
                            if (from2 != null)
                            {
                                result.ShowInsuredPersonsCount = from2.ShowInsuredPersonsCount;
                                result.ShowMonth = from2.ShowMonth;
                                result.ShowPeriod = from2.ShowPeriod;
                            }
                            break;
                    }
                    return result;
                }).Cast<SPNOperation>().ToList(); 
        }
    }

    public class SPNOperationTypeItem : SPNOperation
    {
        public long TypeId { get; set; }
        public string TypeName { get; set; }

        public override string Name => TypeName;
    }
}
