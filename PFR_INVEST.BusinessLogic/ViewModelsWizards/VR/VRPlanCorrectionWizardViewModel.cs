﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsWizards;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRPlanCorrectionWizardViewModel : PlanCorrectionWizardViewModel<PlanContractRecord>
    {
        public VRPlanCorrectionWizardViewModel(long operationId, long direction = 0)
            : base(operationId, direction)
        {

        }

        protected override List<Year> GetValidYearList()
        {
            return BLServiceSystem.Client.GetValidYearListForYearPlanCorrection(Document.Types.VR, ((SPNOperationTypeItem)SelectedOp).TypeId);
        }

        protected override List<Month> GetValidMonthList(long yearID)
        {
            //TODO проверить на корректное заполенние месяцов, т.к. был открыт закомм. код
            return BLServiceSystem.Client.GetValidMonthListForYearPlanCorrection(Document.Types.VR, yearID, ((SPNOperationTypeItem)SelectedOp).TypeId).OrderBy(a => a.ID).ToList();
           // return this.MonthsList;
        }

        protected override List<SIUKPlan> GetUKPlansForYearMonthAndOperation(long monthID, long yearID)
        {
            return BLServiceSystem.Client.GetUKPlansForYearMonthAndOperation(Document.Types.VR, monthID, yearID, OperationId, false);
        }

        protected override IEnumerable<Type> GetRelatedViewModelTypesToRefresh()
        {
            yield return typeof(VRAssignPaymentsListViewModel);
        }

        protected override PlanContractRecord Convert(SIUKPlan ukPlan)
        {
            return new PlanContractRecord(ukPlan);
        }
    }
}
