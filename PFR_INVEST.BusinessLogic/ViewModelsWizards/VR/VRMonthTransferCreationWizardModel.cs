﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OUFV_manager)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class VRMonthTransferCreationWizardModel : MonthTransferCreationWizardModel
    {
        public VRMonthTransferCreationWizardModel(long operationId) : base(operationId, 3) { }

        protected override List<Year> GetValidYearList()
        {
            return BLServiceSystem.Client.GetYearListCreateSIUKPlanByOperationType(Document.Types.VR, OperationTypeId ?? 0, false);
        }
        
        protected override List<Month> GetValidMonthList(long yearID)
        {
            return BLServiceSystem.Client.GetMonthListCreateSIUKPlanByOperationType(Document.Types.VR, yearID, OperationTypeId ?? 0, OperationContentId);
        }

        protected override List<SIUKPlan> GetUKPlansForYearMonthAndOperation(long monthID, long yearID)
        {
            return BLServiceSystem.Client.GetUKPlansForYearMonthAndOperationForMonthTransferWizard(Document.Types.VR, monthID, yearID, OperationTypeId.Value, OperationContentId).ToList();
        }

        protected override IEnumerable<Type> GetRelatedViewModelTypesToRefresh()
        {
            return new[]
            {
                typeof(VRAssignPaymentsListViewModel),
                typeof(TransfersVRListViewModel),
                typeof(SPNMovementsVRListViewModel),
                typeof(ZLMovementsVRListViewModel)
            };
        }

        public override IEnumerable<Type> GetRelatedAssignPaymentListViewModelTypes()
        {
            return new[]
            {
                typeof(VRAssignPaymentsListViewModel),
                typeof(VRAssignPaymentsArchiveListViewModel)
            };
        }
    }
}
