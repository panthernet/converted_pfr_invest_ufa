﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.ViewModelsWizards.VR
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker, DOKIP_ROLE_TYPE.OVSI_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class TransferVR13YearWizardViewModel : TransferWizardViewModel
    {
        public override bool IsImportAvailable { get; } = false;

        public TransferVR13YearWizardViewModel(bool forAPSI)
            : base(Document.Types.VR, forAPSI)
        {

        }

        protected override void SetVkTranfderType()
        {
            TransferTypesUKtoPFR = new List<SPNOperation>(BLServiceSystem.Client.GetSPNOperationsByTypeListHib(3)).Where(op => op.ID == 13).ToList();

            if (TransferTypes_UKtoPFR.Count == 0)
                base.SetVkTranfderType();
            else
                SelectedTransferType = TransferTypes_UKtoPFR[0];
        }


        public override void EnsureAvailableList()
        {
            AvailableContractsList = GetOperationsContracts();
        }

        protected override List<ModelContractRecord> GetOperationsContracts()
        {
            return BLServiceSystem.Client.GetContractsForUKsByTypeAndName(_contractType, new[] { 4, 5 }).ConvertAll(
                c => new ModelContractRecord(c)).ToList();
        }

        public override string AssignePaymentTitle => SelectedTransferType.Name;
    }
}
