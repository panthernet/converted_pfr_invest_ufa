﻿using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic
{
    public enum SettingCognosKey
    {
        CognosNPFUrl,
        CognosSIUrl,
        CognosCBUrl,
        CognosBOUrl,
        CognosVRUrl
    }

    public static class SettingCognosManager
    {
        //private static string _NPFUrl;
        //private static string _SIUrl;
        //private static string _CBUrl;
        //private static string _BOUrl;
        //private static string _VRUrl;

        private static readonly Dictionary<SettingCognosKey, string> FolderIds = new Dictionary<SettingCognosKey, string>();

        //public static string NPFUrl
        //{
        //    get
        //    {
        //        if (_NPFUrl == null)
        //        {
        //            return _NPFUrl = GetFolderIdByKey(SettingCognosKey.CognosNPFUrl);
        //    }
        //    set { if (value != _NPFUrl)_NPFUrl = value; SetFolderIdByKey(SettingCognosKey, value); }
        //}

        public static string NPFUrl
        {
            get { return GetFolderIdByKey(SettingCognosKey.CognosNPFUrl); }
            set { SetFolderIdByKey(SettingCognosKey.CognosNPFUrl, value); }
        }

        public static string SIUrl
        {
            get { return GetFolderIdByKey(SettingCognosKey.CognosSIUrl); }
            set { SetFolderIdByKey(SettingCognosKey.CognosSIUrl, value); }
        }

        public static string CBUrl
        {
            get { return GetFolderIdByKey(SettingCognosKey.CognosCBUrl); }
            set { SetFolderIdByKey(SettingCognosKey.CognosCBUrl, value); }
        }

        public static string BOUrl
        {
            get { return GetFolderIdByKey(SettingCognosKey.CognosBOUrl); }
            set { SetFolderIdByKey(SettingCognosKey.CognosBOUrl, value); }
        }

        public static string VRUrl
        {
            get { return GetFolderIdByKey(SettingCognosKey.CognosVRUrl); }
            set { SetFolderIdByKey(SettingCognosKey.CognosVRUrl, value); }
        }

        private static string GetFolderIdByKey(SettingCognosKey key)
        {
            if (FolderIds.ContainsKey(key)) { return FolderIds[key]; }
            var setting = DataContainerFacade.GetListByProperty<SettingCognos>("UrlName", key.ToString());
            var id = setting != null && setting.Count > 0 ? setting.First().FolderId : string.Empty;
            FolderIds.Add(key, id);
            return id;
        }

        private static void SetFolderIdByKey(SettingCognosKey key, string folderId)
        {
            var settings = DataContainerFacade.GetListByProperty<SettingCognos>("UrlName", key.ToString());
            if (settings != null && settings.Count > 0)
            {
                var set = settings.First();
                if (set.FolderId != folderId)
                {
                    set.FolderId = folderId;
                    DataContainerFacade.Save(set);
                }
            }
            else
            {
                var set = new SettingCognos { FolderId = folderId, UrlName = key.ToString() };
                DataContainerFacade.Save(set);
            }
            FolderIds[key] = folderId;
        }
    }
}
