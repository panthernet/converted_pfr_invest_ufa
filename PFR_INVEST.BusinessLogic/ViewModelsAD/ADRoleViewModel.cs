﻿using System.Collections.Generic;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.DataObjects.Journal;
using System;
using System.Collections.ObjectModel;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class ADRoleViewModel : ViewModelCard
    {
        public EventHandler OnListUpdate;

        /// <summary>
        /// Список всех пользователей
        /// </summary>
        public ObservableCollection<User> AllUsers { get; set; }

        /// <summary>
        /// Список пользователей текущей роли
        /// </summary>
        public ObservableCollection<User> CurrentUsers { get; set; }

        public Role Role { get; private set; }

        public string Name
        {
            get
            {
                return Role != null ? Role.Name : "";
            }
            set
            {
                if (Role != null) Role.Name = value;
                OnPropertyChanged("Name");
            }
        }
        public string ShowName
        {
            get
            {
                return Role != null ? Role.ShowName : "";
            }
            set
            {
                if (Role != null) Role.ShowName = value;
                OnPropertyChanged("ShowName");
            }
        }
        public string Description
        {
            get
            {
                return Role != null ? Role.Description : string.Empty;
            }
            set
            {
                if (Role != null) Role.Description = value;
                OnPropertyChanged("Description");
            }
        }

        public List<User> Users => Role != null ? Role.Users : null;

        public ADRoleViewModel(Role role)
        {
            Role = role;
            AddedUsers = new List<User>();
            RemovedUsers = new List<User>();

            AllUsers = new ObservableCollection<User>();
            CurrentUsers = new ObservableCollection<User>();
        }

        public void UpdateUsersMembership()
        {
            try
            {
                BusyHelper.Start();
                CurrentUsers = new ObservableCollection<User>(AP.Provider.GetAuthUsers(Role));
                AllUsers = new ObservableCollection<User>(AP.Provider.GetUsersNotInRole(Role));
            }
            finally
            {
                BusyHelper.Stop();
            }
        }

        /// <summary>
        /// Список пользователей, которые лишены роли
        /// </summary>
        public List<User> RemovedUsers { get; set; }

        /// <summary>
        /// Список пользователей, которые получили роль
        /// </summary>
        public List<User> AddedUsers { get; set; }

        protected override void ExecuteSave()
        {
            string m;
            int addErr = 0, remErr = 0;
            if (AddedUsers != null)
            {
                foreach (var aUser in AddedUsers) {
                    if (AP.Provider.AddToRole(Role, aUser, out m))
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Добавление роли пользователю", String.Format("Добавлена роль: {0}, Пользователь: {1}", Role.ShowName, aUser.Login));
                    }
                    else
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Добавление роли пользователю", String.Format("Ошибка добавления роли \"{0}\" пользователю \"{1}\": {2}",
                            Role.ShowName, aUser.Login, m));
                        addErr++;
                    }
                }
                AddedUsers = new List<User>();
            }
            if (RemovedUsers != null)
            {
                foreach (var rUser in RemovedUsers) {
                    if (AP.Provider.RemoveFromRole(Role, rUser, out m))
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Удаление роли пользователя", String.Format("Удалена роль: {0}, Пользователь: {1}", Role.ShowName, rUser.Login));
                    }
                    else
                    {
                        JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Удаление роли пользователя", String.Format("Ошибка удаления роли \"{0}\" пользователя \"{1}\": {2}",
                            Role.ShowName, rUser.Login, m));
                        remErr++;
                    }
                }
                RemovedUsers = new List<User>();
            }

            if (addErr > 0 || remErr > 0)
            {
                DialogHelper.ShowError(string.Format(" При назначении ролей пользователям произошли ошибки. \r\n Ошибок добавления ролей: {0} \r\n Ошибок удаления ролей: {1} \r\n Более подробное описание ошибок доступно в журнале действий пользователя.", 
                    addErr, remErr));
                if (OnListUpdate != null)
                {
                    OnListUpdate(this, null);
                }
            }

            IsDataChanged = false;
        }

        public override bool CanExecuteSave()
        {
            return ( IsDataChanged && (AddedUsers != null) && (RemovedUsers != null) && ((AddedUsers.Count > 0) || (RemovedUsers.Count > 0)));
        }

        public override string this[string columnName] => string.Empty;
    }
}
