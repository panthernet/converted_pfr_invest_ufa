﻿using System.Collections.ObjectModel;
using System.Linq;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.BusinessLogic.Journal;

namespace PFR_INVEST.BusinessLogic
{
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator)]
    public class ADRolesListViewModel : ViewModelBase
    {
        public ObservableCollection<ADRoleViewModel> Roles { get; set; }

        public ADRolesListViewModel()
        {
            Roles = new ObservableCollection<ADRoleViewModel>(AP.Provider.GetRoles().Select(b => new ADRoleViewModel(b)));
            JournalLogger.LogTypeEvent(this, JournalEventType.ROLES_CHANGE, "Просмотр ролей системы");
        }
    }
}