﻿using System;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.Constants.Identifiers;

namespace PFR_INVEST.BusinessLogic
{
    public class CorrespondenceListItem
    {
        public static readonly string s_Doc = "Документ";
        public static readonly string s_AttachDoc = "Связанный документ";
        string s_AttachTemplate = "{0} ({1})";

        Document Doc;

        DocumentStatus status = DocumentStatus.main;

        public CorrespondenceListItem(Document doc)
        {
            Doc = doc;
        }

        public CorrespondenceListItem(Document doc, DocumentStatus s)
        {
            Doc = doc;
            this.status = s;
        }

        public long ID => Doc.ID;

        public string Sender => Doc.LegalEntityID == null ? string.Empty : Doc.GetLegalEntity().FormalizedName;

        public int? Year
        {
            get
            {
                if (Doc.IncomingDate.HasValue)
                    return Doc.IncomingDate.Value.Year;
                else
                    return null;
            }
        }
                
        string m_Month = null;
        public string Month
        {
            get
            {
                if (m_Month == null && Doc.IncomingDate.HasValue)
                {
                    m_Month = DateTools.GetMonthInWordsForDate(Doc.IncomingDate.Value.Month);//months[Doc.IncomingDate.Value.Month - 1];
                }
                return m_Month;
            }
        }

        public DateTime? IncomingDate => Doc.IncomingDate;

        public string IncomingNumber
        {
            get
            {
                if (Doc.IncomingNumber != null)
                    return Doc.IncomingNumber.Trim();
                else
                    return null;
            }
        }

        public DateTime? OutgoingDate => Doc.OutgoingDate;

        public string OutgoingNumber
        {
            get
            {
                if (Doc.OutgoingNumber != null)
                    return Doc.OutgoingNumber.Trim();
                else
                    return null;
            }
        }

        public string DocumentClass => Doc.DocumentClassID == null ?
            string.Empty :
            this.status == DocumentStatus.main ?
                (Doc.GetDocumentClass() == null ?
                    "" :
                    Doc.GetDocumentClass().Name) :
                string.Format(s_AttachTemplate, (Doc.GetAttachClass() == null ?
                    "" :
                    Doc.GetAttachClass().Name), s_AttachDoc);

        public string Executor
        {
            get
            {
                if (Doc.ExecutorID.HasValue)
                {
					Person executor = Doc.GetExecutor();
                    if (executor != null)
						return executor.FIOShort;
                }
                else
                    if (Doc.ExecutorName != null)
                        return Doc.ExecutorName;
                return "Исполнитель не назначен";
            }
        }

        public string AdditionalDocumentInfo
        {
            get
            {
                if (status == DocumentStatus.main)
                {
                    if (Doc.AdditionalInfoID.HasValue)
                    {
                        var info = Doc.GetAdditionalDocumentInfo();
                        if (info != null)
                            return info.Name;
                    }
                    else if (Doc.AdditionalInfoText != null)
                        return Doc.AdditionalInfoText;
                }
                else
                {
                    return (Doc as AddDocument).AdditionalInfo;
                }
                return null;
            }
        }

        public DateTime? ControlDate => Doc.ControlDate;

        public DateTime? ExecutionDate => Doc.ExecutionDate;

        public bool IsExecutionExpired
        {
            get 
            {
                if (ControlDate != null)
                    return DateTime.Today > ControlDate 
                        && (ExecutionDate == null || ExecutionDate != null && ExecutionDate > ControlDate) ? true : false;
                return 
                    false;
            }
        }

        public string BackgroundColor
        {
            get
            {
                //("Текущая дата" строго > "Контрольной даты") + при этом либо ("дата исполнения" не указана), либо ("дата исполнения" > "контрольной даты")
                if (ControlDate != null)
                    return DateTime.Today > ControlDate &&
                        (ExecutionDate == null || ExecutionDate != null && ExecutionDate > ControlDate) ? "#FFE8E0" : "#FFFFFF";
                return "#FFFFFF";
            }
        }
    }
}
