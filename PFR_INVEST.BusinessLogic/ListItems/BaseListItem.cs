﻿using System;
using PFR_INVEST.Proxy;

namespace PFR_INVEST.BusinessLogic
{
    public class BaseListItem
    {
        protected DBEntity dbEntity = null;
        public DBEntity DBEntity => this.dbEntity;

        public virtual bool IsEmpty => false;

        protected string GetStringField(string fildName)
        {
            try
            {
                string val = (string)this.dbEntity.Fields[fildName].Value;
                return val;
            }
            catch
            {
                return string.Empty;
            }
        }

        protected long GetLongField(string fildName)
        {
            try
            {
                long val = Convert.ToInt64(this.dbEntity.Fields[fildName].Value);
                return val;
            }
            catch
            {
                return 0;
            }
        }

        protected double GetDoubleField(string fildName)
        {
            try
            {
                double val = Convert.ToDouble(this.dbEntity.Fields[fildName].Value);
                return val;
            }
            catch
            {
                return 0.0;
            }
        }

        protected decimal GetDecimalField(string fildName)
        {
            try
            {
                decimal val = Convert.ToDecimal(this.dbEntity.Fields[fildName].Value);
                return val;
            }
            catch
            {
                return 0;
            }
        }

        protected DateTime GetDateTimeField(string fildName)
        {
            try
            {
                return Util.isFieldExists(dbEntity, fildName)
                           ? (DateTime)this.dbEntity.Fields[fildName].Value
                           : DateTime.MinValue;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }


        public DateTime? GetDateTimeNullableField(string fildName)
        {
            try
            {
                DateTime val = (DateTime)this.dbEntity.Fields[fildName].Value;
                if (!DateTime.MinValue.Equals(val))
                {
                    return val;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        private BaseListItem()
        {
        }

        public BaseListItem(DBEntity ent)
        {
            this.dbEntity = ent;
        }

        public virtual BaseListItem Clone()
        {
            return new BaseListItem(new DBEntity(this.dbEntity.Fields));
        }

        public delegate void ItemPropertyChangedDelegate(string propertyName);
        public event ItemPropertyChangedDelegate OnItemPropertyChanged;
        public void ItemPropertyChanged(string propertyName)
        {
            if (OnItemPropertyChanged != null)
            {
                OnItemPropertyChanged(propertyName);
            }
        }
    }
}
