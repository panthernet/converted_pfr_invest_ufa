﻿using System;

namespace PFR_INVEST.BusinessLogic
{
    public class F50InfoListItem : BaseListItem
    {
        public F50InfoListItem(string name, decimal? count, decimal? price)
            : base(null)
        {
            this.Name = name;
            this.Count = Math.Round(count ?? 0M);
            this.Price = price ?? 0.00M;
        }
        public string Name
        {
            get;
            set;
        }
        public decimal Count
        {
            get;
            set;
        }
        public decimal Price
        {
            get;
            set;
        }
    }
}
