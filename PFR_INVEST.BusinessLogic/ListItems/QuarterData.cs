﻿namespace PFR_INVEST.BusinessLogic.ListItems
{
    public class QuarterData
    {
        public int Number { get; set; }
        public string Text { get; set; }
    }
}
