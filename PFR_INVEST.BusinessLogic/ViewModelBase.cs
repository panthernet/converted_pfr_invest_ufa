﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using System.Windows.Threading;
using PFR_INVEST.Auth.ClientData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.BusinessLogic.Interfaces;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataObjects.Attributes;
using PFR_INVEST.DataObjects.ServiceItems;

#if WEBCLIENT
using PFR_INVEST.Web2.BusinessLogic;
#endif

namespace PFR_INVEST.BusinessLogic
{
	public class ErrorLoadingTemplateEventArgs : EventArgs
	{
		public string TemplateName
		{
			get;
		}

		public ErrorLoadingTemplateEventArgs(string name)
		{
			TemplateName = name;
		}
	}

	public class ViewModelInitializeException : Exception
	{
	}

	public class ViewModelReadAccessException : Exception
	{
	}

	public class ViewModelSaveUserCancelledException : Exception
	{
	}

	public abstract class ViewModelBase :
#if WEBCLIENT
        WebDummyModelBase,
#endif
        INotifyPropertyChanged
	{
        /// <summary>
        /// Хранит тип основного объекта данных карточки или списка, для которого реализованы операции сохранения, удаления и просмотра
        /// Тип нужен для получения доп. информации при логировании операций на стороне сервиса.
        /// По умолчанию не указан. Если не указан, доп. информация не логируется. Если указан typeof(object) не выводится предупреждение об отсутствии значения в консоль отладки.
        /// PS: Для правильной работы с карточками требует, чтобы свойство ID модели содержало актуальный идентификатор объекта после операций сохранения и удаления.
        /// </summary>
        public virtual Type DataObjectTypeForJournal { get; protected set; }
        /// <summary>
        /// Кастомное описание данных в карточке или таблице, используется для составных списков или исключительных случаев описания сущности карточки
        /// </summary>
        public string DataObjectDescForJournal { get; protected set; }

		public string LogCaption { get; set; }
		public static bool DoNotThrowExceptionOnError;
		public static Log Logger;
        public static IDialogHelper DialogHelper;
		public static IBusyHelper BusyHelper;
		public static IViewModelManager ViewModelManager;

		public delegate ViewModelBase GetViewModelDelegate(Type viewModelType);
		public static GetViewModelDelegate GetViewModel;

		public delegate List<ViewModelBase> GetViewModelsListDelegate(Type viewModelType);
		public static GetViewModelsListDelegate GetViewModelsList;

		public static EventHandler OnGetDataError;
		protected void RaiseGetDataError()
		{
		    OnGetDataError?.Invoke(this, null);
		}

	    protected virtual string PrintingInformation => null;

	    public static EventHandler<PrintingEventArgs> OnStartedPrinting;
		protected void RaiseStartedPrinting()
		{
		    OnStartedPrinting?.Invoke(this, new PrintingEventArgs(PrintingInformation));
		}

	    public static EventHandler OnFinishedPrinting;
		protected void RaiseFinishedPrinting()
		{
		    OnFinishedPrinting?.Invoke(this, null);
		}

	    public static EventHandler<ErrorLoadingTemplateEventArgs> OnErrorLoadingTemplate;
		protected void RaiseErrorLoadingTemplate(string name)
		{
		    OnErrorLoadingTemplate?.Invoke(this, new ErrorLoadingTemplateEventArgs(name));
		}

	    protected bool IsInitialized;
		protected void HandleException(Exception e)
		{
			if (DoNotThrowExceptionOnError == false)
				throw e;

			Logger.WriteException(e);
			if (IsInitialized)
				RaiseGetDataError();
			else
				throw new ViewModelInitializeException();
		}

		private void VerifyReadAccess()
		{
            var accessDenied = true;
			var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is ReadAccessAttribute).Cast<ReadAccessAttribute>();
			var roles = attributes.SelectMany(attr => attr.Roles).Distinct().ToList();
			foreach (var role in roles)
			{
				if (AP.Provider.CurrentUserSecurity.CheckUserInRole(role))
					accessDenied = false;
			}

            //проверяем на доступ VIEW прав
		    if (accessDenied)
		        accessDenied = !AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
		    
			if (accessDenied)
				throw new ViewModelReadAccessException();
		}



		private readonly List<string> _ignoreFields = new List<string>("ScaleX|ScaleY|State|Header|IsReadOnly|Header".Split('|'));

		protected long InternalID;

        [Required]
        public long ID
		{
			get { return InternalID; }
			set { if (InternalID != value) { InternalID = value; OnPropertyChanged("ID"); IsIDChanged = true; } }
		}

		/// <summary>
		/// Только для тестов
		/// </summary>
		public bool IsIDChanged { get; private set; }

#if !WEBCLIENT
        public virtual bool IsDataChanged { get; set; }
#endif
        private string _mHeader;

		/// <summary>
		/// Надпись окна формы. Забинджена на Caption вышестоящей формы
		/// </summary>
		public string Header { 
			get { return _mHeader; }
			set { if (_mHeader != value) { _mHeader = value; OnPropertyChanged("Header"); } }
		}

		public ViewModelState State
		{
			get { return _state; }
			set
			{
			    if (_state == value) return;
			    if (value == ViewModelState.Edit && EditAccessDenied)
			        _state = ViewModelState.Read;
			    else _state = value;
			    OnPropertyChanged("State");
			    OnPropertyChanged("IsReadOnly");
			}
		}


	    public virtual bool IsReadOnly => State == ViewModelState.Read || EditAccessDenied;

	    /// <summary>
		/// Указывает является ли VM формой без предложения о сохранении по умолчанию при закрытии
		/// </summary>
		public bool IsNotSavable { get; set; }

		private ViewModelState _state = ViewModelState.Read;

        private bool? _isEditAccessDenied;
#if !WEBCLIENT
		public virtual bool EditAccessDenied
#else
	    public virtual bool WebEditAccessDenied
#endif
		{
			get
			{
			    if (_isEditAccessDenied.HasValue) return _isEditAccessDenied ?? true;
			    var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is EditAccessAttribute).Cast<EditAccessAttribute>();
			    var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
			    foreach (var role in roles)
			    {
			        if (!AP.Provider.CurrentUserSecurity.CheckUserInRole(role)) continue;
			        _isEditAccessDenied = false; break;
			    }

			    return _isEditAccessDenied ?? true;
			}
		}

		private bool? _isUnderViewerAccess;
        /// <summary>
        /// Возвращает True если модель доступна юзеру для просмотра с учетом Viewer прав (или выше)
        /// </summary>
        public virtual bool IsUnderViewerAccess
        {
            get
            {
                if (_isUnderViewerAccess.HasValue) return _isUnderViewerAccess ?? true;

                var attributes = GetType().GetCustomAttributes(true).Where(attr => attr is ReadAccessAttribute).Cast<ReadAccessAttribute>();
                var roles = attributes.SelectMany(attr => attr.Roles).Distinct().ToList();
                _isUnderViewerAccess = AP.Provider.CurrentUserSecurity.CheckViewerHasAccess(roles);
                return _isUnderViewerAccess ?? true;
            }
        }

        //для удобной привязки к свойсту IsEnabled графических елементов
        public bool EditAccessAllowed => !EditAccessDenied;

#region Delete access
		private bool? _isDeleteAccessGranted;

        /// <summary>
        /// Проверка на права удаления текущей модели
        /// </summary>
        public bool DeleteAccessAllowed => CheckDeleteAccess(GetType()) && State != ViewModelState.Read;
        /// <summary>
        /// Проверка на права удаления текущей модели
        /// </summary>
        public bool DeleteAccessDenied => !DeleteAccessAllowed;

        /// <summary>
        /// Проверка на права удаления типа модели
        /// </summary>
        public bool CheckDeleteAccess(Type type)
		{
		    if (_isDeleteAccessGranted.HasValue) return _isDeleteAccessGranted.Value;
		    if (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
		    {
		        _isDeleteAccessGranted = true;
		        return _isDeleteAccessGranted.Value;
		    }
		    var attributes = type.GetCustomAttributes(true).Where(attr => attr is DeleteAccessAttribute).Cast<DeleteAccessAttribute>();
		    var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
		    if (roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role))) {
		        _isDeleteAccessGranted = true;
		        return _isDeleteAccessGranted.Value;
		    }
		    _isDeleteAccessGranted = false;

		    return _isDeleteAccessGranted.Value;
		}

	    /// <summary>
	    /// Незаивисимая проверка на право удаления модели
	    /// </summary>
	    /// <param name="type">Тип модели</param>
	    /// <param name="aType"></param>
	    public static bool CheckGlobalAccess(Type type, Type aType)
		{
			if (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
				return true;
		    var attributes = type.GetCustomAttributes(true).Where(attr => attr.GetType() == aType).Cast<ViewModelAccessAttribute>();
		    var roles = attributes.SelectMany(attr => attr.Roles).Distinct();
		    return roles.Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role));
		}

		
#endregion

        /// <summary>
        /// Удаление объекта
        /// </summary>
        /// <param name="delType">Тип удаления, по умолчанию 1 = Удаление</param>
		protected virtual void ExecuteDelete(int delType = 1)
		{
			//log event
		    if (!(this is IVmNoDefaultLogging))
		    {
		        switch (delType)
		        {
		            case DocOperation.Archive:
		                JournalLogger.LogArchiveEvent((ViewModelCard) this);
		                break;
		            /*case DocOperation.Rollback:
		                JournalLogger.LogRollbackEvent((ViewModelCard) this);
		                break;*/
		            default:
		                JournalLogger.LogDeleteEvent((ViewModelCard) this);
		                break;
		        }
		    }
#if !WEBCLIENT
            if (!(this is IUpdateRaisingModel)) return;
            var m = (this as IUpdateRaisingModel);
            ViewModelManager.UpdateDataInAllModels(m?.GetUpdatedList());
#endif
		}
		public virtual bool CanExecuteDelete() { return false; }
		public ICommand DeleteCard { get; protected set; }

        /// <summary>
        /// Вызывается после нажатия на кнопку и до подачи команды на удаление. В целях повышения производительности, 
        /// ресурсоемкие проверки на возможность удаления следует проводить здесь.
        /// </summary>
        /// <returns></returns>
		public virtual bool BeforeExecuteDeleteCheck()
		{
			return true;
		}

        /// <summary>
        /// Вызывается моделью для удаления сущности
        /// </summary>
        /// <param name="delType">Тип удаления</param>
        /// <param name="inputID">Идентификатор</param>
		protected virtual void ExecuteDelete(int delType, long inputID, DeletedDataItem data = null, string type = null)
		{
			ExecuteDelete(delType);
		}

		/// <summary>
		/// Происходит удаление объекта
		/// </summary>
		public bool Deleting { get; set; }

        /// <summary>
        /// Указывает, что единовременно может быть открыт только один экземпляр данной формы (при использованиии метода OpenNewTab)
        /// </summary>
	    public bool IsOnlyOneInstanceAllowed { get; set; }

	    protected ViewModelBase()
		{
			VerifyReadAccess();
			LogCaption = string.Empty;
			IsNotSavable = false;
			Deleting = false;
			DeleteCard = new DelegateCommand(o => CanExecuteDeleteMine(), o => ExecuteDeleteMine());
			LogCreateModel();
		}

		private void LogCreateModel()
		{
		    if (Logger == null) return;
		    var uIci = Thread.CurrentThread.CurrentUICulture;
		    var ci = Thread.CurrentThread.CurrentCulture;
		    var createModelMessage =
		        $"Создание модели ({GetType().FullName}). {(uIci.ToString().Equals("ru-RU") ? string.Empty : $"Неверно выбран язык UI для текущей нити исполнения ({uIci}).")}{(ci.ToString().Equals("ru-RU") ? string.Empty : $"Неверно выбран язык для текущей нити исполнения ({ci}).")}";
		    Logger.WriteLine(createModelMessage);
		}

		private bool CanExecuteDeleteMine()
		{
			if (AP.Provider.CurrentUserSecurity.CheckUserInRole(DOKIP_ROLE_TYPE.Administrator))
				return CanExecuteDelete();

			return GetType().GetCustomAttributes(true).OfType<DeleteAccessAttribute>().Any(attri => attri
			    .Roles
			    .Any(role => AP.Provider.CurrentUserSecurity.CheckUserInRole(role))) && CanExecuteDelete();
		}

		/// <summary>
		/// Внутренний метод удаления
		/// </summary>
		private void ExecuteDeleteMine()
		{
			BeforeExecuteDelete();

			IList<KeyValuePair<Type, long>> items = null;
            var m = (this as IUpdateRaisingModel);
			if (m!= null)
				items = m.GetUpdatedList();

			Deleting = true;
			ExecuteDelete(DocOperation.Archive);

            //Если удаление не было отменено в ExecuteDelete() вручную
            //вызываем обновление связанных моделей
			if (Deleting && this is IUpdateRaisingModel)
			{
				ViewModelManager.UpdateDataInAllModels(items);
			}

			AfterExecuteDelete();
		}

		/// <summary>
		/// При вызове OnPropertyChanged для этого свойства - изменение просигнализирует UI об изменении
		/// Но на свойство IsDataChanged это не повлияет
		/// </summary>
		/// <param name="propertyName">Название свойства/поля</param>
		protected void IgnorePropertyChanged(string propertyName)
		{
			if (!_ignoreFields.Contains(propertyName))
			{
				_ignoreFields.Add(propertyName);
			}
		}

		protected virtual void BeforeExecuteDelete() { }
		protected virtual void AfterExecuteDelete() { }


		private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected virtual void AfterPropertyChanged(string propertyName) { }

        /// <summary>
        /// Вызов оповещения UI о смене свойства БЕЗ ИЗМЕНЕНИЯ свойства модели IsDataChanged
        /// </summary>
        /// <param name="propertyName">Наименование свойства</param>
        protected void OnPropertyChangedNoData(string propertyName)
        {
            OnPropertyChangedInternal(propertyName);
        }

        /// <summary>
        /// Вызов оповещения UI о смене свойства с изменением свойства модели IsDataChanged
        /// </summary>
        /// <param name="propertyName">Наименование свойства</param>
        protected void OnPropertyChanged(string propertyName)
        {
            if ((State != ViewModelState.Read) && (!_ignoreFields.Contains(propertyName)))
                IsDataChanged = true;
#if !WEBCLIENT
            OnPropertyChangedInternal(propertyName);
#else
            AfterPropertyChanged(propertyName);
#endif
        }


	    private void OnPropertyChangedInternal(string propertyName)
		{
			try
			{
				if (_dispatcher.Thread == Thread.CurrentThread)
					PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
				else
					_dispatcher.BeginInvoke(DispatcherPriority.DataBind,
							   (ThreadStart)delegate {
								   PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
							   });
			}
			catch (Exception ex)
			{
				Logger.WriteException(ex, "ViewModelBase.OnPropertyChanged");
			}
			AfterPropertyChanged(propertyName);
		}

#region Commands

        /// <summary>
        /// Внутреннее свойство, указывает требуется ли логировать открытие модели (true для случаев добавления сущностей из диалогов, чтобы пропустить логирование)
        /// </summary>
	    protected bool DontLogOpenEvent;
        /// <summary>
        /// Операция, выполняемая по завершению настройки представления
        /// </summary>
        /// <param name="header">Наименование заголовка</param>
        /// <param name="dontLogOpen">Не логировать операцию открытия окна</param>
		public void PostOpen(string header, bool dontLogOpen = false)
		{
		    DontLogOpenEvent = dontLogOpen;
			if(string.IsNullOrEmpty(Header))
				Header = header;
			if (IsDataChanged) IsDataChanged = false;
			PostOpenInternal();
		}

		protected virtual void PostOpenInternal() { }

#endregion


#region ErrorHandler

		/// <summary>
		/// Метод для реакции на общии ошибки модели
		/// </summary>
		/// <param name="ex"></param>
		public static void HandleCommonError(Exception ex)
		{
			HandleCommonError(ex, null);
		}

		/// <summary>
		/// Метод для реакции на общии ошибки модели
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="operationName">Имя операции. Используется для сообщения "Во время операции [название] возникла ошибка..."</param>
		public static void HandleCommonError(Exception ex, string operationName)
		{
			Logger.WriteException(ex);
		    var msg = string.IsNullOrWhiteSpace(operationName) ? "Во время операции возникла ошибка. Операция была прервана. Более детальную информацию об ошибке можно найти в лог файлах." : $"Во время операции {operationName} возникла ошибка. Операция была прервана. Более детальную информацию об ошибке можно найти в лог файлах.";
			DialogHelper.ShowError(msg);
		}
#endregion

        public ModelEntityAttribute GetModelEntityAttribute()
        {
            return (ModelEntityAttribute)GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
        }

        public Type GetModelEntityType()
        {
            var attr =
                (ModelEntityAttribute)
                    GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
            return attr == null ? (DataObjectTypeForJournal == typeof(object) ? null : DataObjectTypeForJournal) : attr.Type;
        }

        public string GetModelEntityJComment()
        {
            var attr =
                (ModelEntityAttribute)
                    GetType().GetCustomAttributes(typeof(ModelEntityAttribute), false).FirstOrDefault();
            return attr == null ? DataObjectDescForJournal : attr.JournalDescription;
        }

        /// <summary>
        /// Установка значения полю класса, с нотификацией об изменении, проверкой старого значения свойства,
        /// </summary>
        /// <typeparam name="T">Тип свойства</typeparam>
        /// <param name="propertyName">Имя свойства</param>
        /// <param name="field">поле, которому будет выполнено присвоение</param>
        /// <param name="value">значение</param>
        /// <returns>истина если значение было изменено</returns>
        protected virtual bool SetPropertyValue<T>(string propertyName, ref T field, T value)
        {

            if (field == null)
            {
                if (value == null)
                    return false;
            }
            else if (field.Equals(value))
                return false;

            field = value;
            
            OnPropertyChanged(propertyName);
            return true;
        }

        public virtual void Refresh() { }
      
    }
}
