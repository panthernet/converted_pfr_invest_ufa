﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF040 : UKReportImportHandlerBase
    {

        private readonly List<Year> _years;
        private readonly List<Month> _months;
        private readonly List<RrzKind> _rrzKinds;
        private readonly List<DetailsKind> _detailsKinds;
        public UKReportImportHandlerF040()
        {
            Mask = "*F040.xml";
            Shema = "F040.xsd";
            _years = DataContainerFacade.GetList<Year>();
            _months = DataContainerFacade.GetList<Month>();
            _rrzKinds = DataContainerFacade.GetList<RrzKind>().OrderBy(r => r.ID).ToList();
            _detailsKinds = DataContainerFacade.GetList<DetailsKind>().OrderBy(r => r.ID).ToList();

        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F040 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {

            var edo = new EDO_ODKF040();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);

            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                var xdoc = XDocument.Load(new StreamReader(fileName, Encoding.GetEncoding(1251)));
                var xdocLog = new XDocument(xdoc);
                if (xdoc.Root.Attributes().Any(a => a.Name.LocalName.Equals("xmlns")))
                {
                    var docNew = new XDocument();
                    var root = new XElement(xdoc.Root.Name.LocalName, xdoc.Root.Attribute("VERSION"), xdoc.Root.Attribute("DATACREATE"));
                    root.Add(xdoc.Root.Nodes());
                    docNew.Add(root);
                    xdoc = docNew;
                }

                var xElement = xdoc.Root.Element("ObSvedenya").Element("UKName").Value;

                if (xElement.Trim().ToLower().Replace("ё", "е").Equals("сводный отчет", StringComparison.InvariantCultureIgnoreCase))
                {
                    return ActionResult.TotalReport();
                }

                var serializer = new XmlSerializer(typeof(EDO_ODKF040));

                using (var reader = XmlReader.Create(new StringReader(xdoc.ToString()), GetReaderSettings()))
                {

                    var doc = (EDO_ODKF040)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();
                    if (Items.Any(i => (i as ImportEdoOdkF040).edoLog.DocRegNumberOut.ToLower().Equals(doc.Title.RegNumberOut.ToLower())))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером {doc.Title.RegNumberOut} уже загружен для импорта");
                    }

                    var contract = GetContractByDogovorNum(doc.ObSvedenya.DogovorNumber);
                    if (contract == null)
                    {
                        return ActionResult.Error($"Договор с регистрационным номером {doc.ObSvedenya.DogovorNumber} не найден");
                    }

                    xdocLog.Root.AddFirst(new XElement("FILENAME", fName));

                    var IdDelete = GetIdReportloaded(xdoc.Root.Name.ToString(), doc.Title.RegNumberOut);
                    var importEdoOdkF040 = new ImportEdoOdkF040();


                    if (IdDelete != null)
                    {
                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F040\nс регистрационным номером {doc.Title.RegNumberOut} существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }
                    }

                    importEdoOdkF040.xmlBody = xdocLog.ToString();

                    var month = _months.FirstOrDefault(m => m.Name.ToLower().Equals(doc.ObSvedenya.OnDate));
                    if (month == null) return ActionResult.Error("Отчет содержит неверное указание месяца: {0}", doc.ObSvedenya.OnDate);

                    var year = _years.FirstOrDefault(y => y.Name.ToLower().Equals(doc.ObSvedenya.Year));
                    if (year == null) return ActionResult.Error("Отчет содержит неверное указание года: {0}", doc.ObSvedenya.Year);

                    importEdoOdkF040.F040 = new EdoOdkF040Hib
                    {
                        ID = IdDelete ?? 0,
                        ContractId = contract.ID,
                        ApName = doc.Title.ResPersonFIO,
                        ApPost = doc.Title.ResPersonDol,
                        RegDate = DateTime.Now.Date,
                        RegNumberOut = doc.Title.RegNumberOut,
                        ReportType = 1,
                        Writedate = doc.DATACREATE,
                        IdOndate = month.ID,
                        IdYear = year.ID,
                        Portfolio = doc.ObSvedenya.InvestCaseName,
                        RRZs = new List<F040Rrz>()
                    };

                    if (doc.Forma.RRZ1 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ1.GCBRFQtyBuyCount,
                                    SellCount = doc.Forma.RRZ1.GCBRFQtySellCount,
                                    BuyAmount = doc.Forma.RRZ1.GCBRFAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ1.GCBRFAmountSellCount,
                                    RrzID = _rrzKinds.Count > 1 ? _rrzKinds[0].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 1 ? _rrzKinds[0].ID : 0
                                });
                    }

                    if (doc.Forma.RRZ2 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ2.GCBSubRFQtyBuyCount,
                                    SellCount = doc.Forma.RRZ2.GCBSubRFQtySellCount,
                                    BuyAmount = doc.Forma.RRZ2.GCBSubRFAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ2.GCBSubRFAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 2 ? _rrzKinds[1].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 1 ? _rrzKinds[0].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ3 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ3.ObligaciiMOQtyBuyCount,
                                    SellCount = doc.Forma.RRZ3.ObligaciiMOQtySellCount,
                                    BuyAmount = doc.Forma.RRZ3.ObligaciiMOAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ3.ObligaciiMOAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 3 ? _rrzKinds[2].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 3 ? _rrzKinds[2].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ4 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ4.ObligaciiRHOQtyBuyCount,
                                    SellCount = doc.Forma.RRZ4.ObligaciiRHOQtySellCount,
                                    BuyAmount = doc.Forma.RRZ4.ObligaciiRHOAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ4.ObligaciiRHOAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 4 ? _rrzKinds[3].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 4 ? _rrzKinds[3].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ5 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ5.AkciiRAOQtyBuyCount,
                                    SellCount = doc.Forma.RRZ5.AkciiRAOQtySellCount,
                                    BuyAmount = doc.Forma.RRZ5.AkciiRAOAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ5.AkciiRAOAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 5 ? _rrzKinds[4].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 5 ? _rrzKinds[4].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ6 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ6.PaiQtyBuyCount,
                                    SellCount = doc.Forma.RRZ6.PaiQtySellCount,
                                    BuyAmount = doc.Forma.RRZ6.PaiAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ6.PaiAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 6 ? _rrzKinds[5].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 6 ? _rrzKinds[5].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ7 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ7.IpotechQtyBuyCount,
                                    SellCount = doc.Forma.RRZ7.IpotechQtySellCount,
                                    BuyAmount = doc.Forma.RRZ7.IpotechAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ7.IpotechAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 7 ? _rrzKinds[6].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 7 ? _rrzKinds[6].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ8 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    BuyCount = doc.Forma.RRZ8.MFOQtyBuyCount,
                                    SellCount = doc.Forma.RRZ8.MFOQtySellCount,
                                    BuyAmount = doc.Forma.RRZ8.MFOAmountBuyCount,
                                    SellAmount = doc.Forma.RRZ8.MFOAmountSellCount,
                                    RrzID = _rrzKinds.Count >= 8 ? _rrzKinds[7].ID : 0
                                });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                    RrzID = _rrzKinds.Count >= 8 ? _rrzKinds[7].ID : 0
                                });
                    }
                    if (doc.Forma.RRZ9 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                BuyCount = doc.Forma.RRZ9.DepozitQtyBuyCount,
                                SellCount = doc.Forma.RRZ9.DepozitQtySellCount,
                                BuyAmount = doc.Forma.RRZ9.DepozitAmountBuyCount,
                                SellAmount = doc.Forma.RRZ9.DepozitAmountSellCount,
                                RrzID = _rrzKinds.Count >= 9 ? _rrzKinds[8].ID : 0
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                RrzID = _rrzKinds.Count >= 9 ? _rrzKinds[8].ID : 0
                            });
                    }
                    if (doc.Forma.RRZ10 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                BuyCount = doc.Forma.RRZ10.CurQtyBuyCount,
                                SellCount = doc.Forma.RRZ10.CurQtySellCount,
                                BuyAmount = doc.Forma.RRZ10.CurAmountBuyCount,
                                SellAmount = doc.Forma.RRZ10.CurAmountSellCount,
                                RrzID = _rrzKinds.Count >= 10 ? _rrzKinds[9].ID : 0
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                RrzID = _rrzKinds.Count >= 10 ? _rrzKinds[9].ID : 0
                            });
                    }
                    if (doc.Forma.RRZ11 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                BuyCount = doc.Forma.RRZ11.OtherFinQtyBuyCount,
                                SellCount = doc.Forma.RRZ11.OtherFinQtySellCount,
                                BuyAmount = doc.Forma.RRZ11.OtherFinAmountBuyCount,
                                SellAmount = doc.Forma.RRZ11.OtherFinAmountSellCount,
                                RrzID = _rrzKinds.Count >= 11 ? _rrzKinds[10].ID : 0
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                RrzID = _rrzKinds.Count >= 11 ? _rrzKinds[10].ID : 0
                            });
                    }
                    if (doc.Forma.RRZ12 != null)
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                BuyCount = doc.Forma.RRZ12.ItogoQtyBuyCount,
                                SellCount = doc.Forma.RRZ12.ItogoQtySellCount,
                                BuyAmount = doc.Forma.RRZ12.ItogoAmountBuyCount,
                                SellAmount = doc.Forma.RRZ12.ItogoAmountSellCount,
                                RrzID = _rrzKinds.Count >= 12 ? _rrzKinds[11].ID : 0
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.RRZs.Add(
                            new F040Rrz
                            {
                                RrzID = _rrzKinds.Count >= 11 ? _rrzKinds[11].ID : 0
                            });
                    }

                    importEdoOdkF040.F040.Details = new List<F040Detail>();
                    if (doc.Forma.Details1 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 1 ? _detailsKinds[0].ID : 0,
                                Broker = doc.Forma.Details1.GCBRFBroker,
                                Exchange = doc.Forma.Details1.GCBRFExchange,
                                BuyCount = doc.Forma.Details1.GCBRFQtyBuy,
                                SellCount = doc.Forma.Details1.GCBRFQtySell,
                                BuyAmount = doc.Forma.Details1.GCBRFAmountBuy,
                                SellAmount = doc.Forma.Details1.GCBRFAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 1 ? _detailsKinds[0].ID : 0
                           });
                    }

                    if (doc.Forma.Details2 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 2 ? _detailsKinds[1].ID : 0,
                                Broker = doc.Forma.Details2.GCBSubRFBroker,
                                Exchange = doc.Forma.Details2.GCBSubRFExchange,
                                BuyCount = doc.Forma.Details2.GCBSubRFQtyBuy,
                                SellCount = doc.Forma.Details2.GCBSubRFQtySell,
                                BuyAmount = doc.Forma.Details2.GCBSubRFAmountBuy,
                                SellAmount = doc.Forma.Details2.GCBSubRFAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 2 ? _detailsKinds[1].ID : 0
                           });
                    }
                    if (doc.Forma.Details3 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 3 ? _detailsKinds[2].ID : 0,
                                Broker = doc.Forma.Details3.ObligaciiMOBroker,
                                Exchange = doc.Forma.Details3.ObligaciiMOExchange,
                                BuyCount = doc.Forma.Details3.ObligaciiMOQtyBuy,
                                SellCount = doc.Forma.Details3.ObligaciiMOQtySell,
                                BuyAmount = doc.Forma.Details3.ObligaciiMOAmountBuy,
                                SellAmount = doc.Forma.Details3.ObligaciiMOAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 3 ? _detailsKinds[2].ID : 0
                           });
                    }

                    if (doc.Forma.Details4 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 4 ? _detailsKinds[3].ID : 0,
                                Broker = doc.Forma.Details4.ObligaciiRHOBroker,
                                Exchange = doc.Forma.Details4.ObligaciiRHOExchange,
                                BuyCount = doc.Forma.Details4.ObligaciiRHOQtyBuy,
                                SellCount = doc.Forma.Details4.ObligaciiRHOQtySell,
                                BuyAmount = doc.Forma.Details4.ObligaciiRHOAmountBuy,
                                SellAmount = doc.Forma.Details4.ObligaciiRHOAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 4 ? _detailsKinds[3].ID : 0
                           });
                    }
                    if (doc.Forma.Details5 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 5 ? _detailsKinds[4].ID : 0,
                                Broker = doc.Forma.Details5.AkciiRAOBroker,
                                Exchange = doc.Forma.Details5.AkciiRAOExchange,
                                BuyCount = doc.Forma.Details5.AkciiRAOQtyBuy,
                                SellCount = doc.Forma.Details5.AkciiRAOQtySell,
                                BuyAmount = doc.Forma.Details5.AkciiRAOAmountBuy,
                                SellAmount = doc.Forma.Details5.AkciiRAOAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 5 ? _detailsKinds[4].ID : 0
                           });
                    }
                    if (doc.Forma.Details6 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 6 ? _detailsKinds[5].ID : 0,
                                Broker = doc.Forma.Details6.PaiBroker,
                                Exchange = doc.Forma.Details6.PaiExchange,
                                BuyCount = doc.Forma.Details6.PaiQtyBuy,
                                SellCount = doc.Forma.Details6.PaiQtySell,
                                BuyAmount = doc.Forma.Details6.PaiAmountBuy,
                                SellAmount = doc.Forma.Details6.PaiAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 6 ? _detailsKinds[5].ID : 0
                           });
                    }
                    if (doc.Forma.Details7 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 7 ? _detailsKinds[6].ID : 0,
                                Broker = doc.Forma.Details7.IpotechBroker,
                                Exchange = doc.Forma.Details7.IpotechExchange,
                                BuyCount = doc.Forma.Details7.IpotechQtyBuy,
                                SellCount = doc.Forma.Details7.IpotechQtySell,
                                BuyAmount = doc.Forma.Details7.IpotechAmountBuy,
                                SellAmount = doc.Forma.Details7.IpotechAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 7 ? _detailsKinds[6].ID : 0
                           });
                    }
                    if (doc.Forma.Details8 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 8 ? _detailsKinds[7].ID : 0,
                                Broker = doc.Forma.Details8.MFOBroker,
                                Exchange = doc.Forma.Details8.MFOExchange,
                                BuyCount = doc.Forma.Details8.MFOQtyBuy,
                                SellCount = doc.Forma.Details8.MFOQtySell,
                                BuyAmount = doc.Forma.Details8.MFOFAmountBuy,
                                SellAmount = doc.Forma.Details8.MFOAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 8 ? _detailsKinds[7].ID : 0
                           });
                    }

                    if (doc.Forma.Details9 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 9 ? _detailsKinds[8].ID : 0,
                                Broker = doc.Forma.Details9.DepozitBroker,
                                Exchange = doc.Forma.Details9.DepozitExchange,
                                BuyCount = doc.Forma.Details9.DepozitQtyBuy,
                                SellCount = doc.Forma.Details9.DepozitQtySell,
                                BuyAmount = doc.Forma.Details9.DepozitAmountBuy,
                                SellAmount = doc.Forma.Details9.DepozitAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 9 ? _detailsKinds[8].ID : 0
                           });
                    }

                    if (doc.Forma.Details10 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 10 ? _detailsKinds[9].ID : 0,
                                Broker = doc.Forma.Details10.CurBroker,
                                Exchange = doc.Forma.Details10.CurExchange,
                                BuyCount = doc.Forma.Details10.CurQtyBuy,
                                SellCount = doc.Forma.Details10.CurQtySell,
                                BuyAmount = doc.Forma.Details10.CurAmountBuy,
                                SellAmount = doc.Forma.Details10.CurAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 10 ? _detailsKinds[9].ID : 0
                           });
                    }
                    if (doc.Forma.Details11 != null)
                    {
                        importEdoOdkF040.F040.Details.Add(
                            new F040Detail
                            {
                                DetailID = _detailsKinds.Count >= 11 ? _detailsKinds[10].ID : 0,
                                Broker = doc.Forma.Details11.OtherFinBroker,
                                Exchange = doc.Forma.Details11.OtherFinExchange,
                                BuyCount = doc.Forma.Details11.OtherFinQtyBuy,
                                SellCount = doc.Forma.Details11.OtherFinQtySell,
                                BuyAmount = doc.Forma.Details11.OtherFinAmountBuy,
                                SellAmount = doc.Forma.Details11.OtherFinAmountSell
                            });
                    }
                    else
                    {
                        importEdoOdkF040.F040.Details.Add(
                           new F040Detail
                           {
                               DetailID = _detailsKinds.Count >= 11 ? _detailsKinds[10].ID : 0
                           });
                    }

                    importEdoOdkF040.edoLog = new EDOLog
                    {
                                                    DocTable = xdoc.Root.Name.ToString(),
                                                    DocForm = xdoc.Root.Name.ToString(),
                                                    Filename = fName,
                                                    DocRegNumberOut = doc.Title.RegNumberOut,
                                                    RegDate = DateTime.Now.Date,
                                                    ContractNum = doc.ObSvedenya.DogovorNumber

                                                };

                    importEdoOdkF040.FileName = Path.GetFileName(fileName);
                    importEdoOdkF040.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF040.ReportOnDate = doc.ObSvedenya.OnDate + " " + doc.ObSvedenya.Year;
                    importEdoOdkF040.ReportOnDateNative = new DateTime(Convert.ToInt32(doc.ObSvedenya.Year), DateUtil.GetMonthIndex(doc.ObSvedenya.OnDate)+1, 1);

                    Items.Add(importEdoOdkF040);
                }

            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }

                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }

            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {

                foreach (ImportEdoOdkF040 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF040(import.F040, import.edoLog, import.xmlBody);
                }

            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F40SIListViewModel), typeof(F40VRListViewModel), typeof(LoadedODKLogListSIViewModel));
        }

    }
}
