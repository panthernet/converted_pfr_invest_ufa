﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    public abstract class UKReportImportHandlerBase
    {

        public string Mask = "*.xml";
        public string Shema = "F026.xsd";
        public bool IsImportDoWork;

        public List<IUKReportImportItem> SelectedRemovedItems = null;

        public delegate List<ViewModelBase> GetViewModelsListDelegate(Type viewModelType);
        public static GetViewModelsListDelegate GetViewModelsList;

        private ObservableList<IUKReportImportItem> _items;
        public ObservableList<IUKReportImportItem> Items => _items ?? (_items = new ObservableList<IUKReportImportItem>());

        public virtual bool CanExecuteOpenFile()
        {
            return !IsImportDoWork ;
        }
        public virtual bool CanExecuteImport()
        {
            return Items.Count > 0 && !IsImportDoWork;
        }
        public virtual bool CanExecuteDelete()
        {

            return SelectedRemovedItems != null && SelectedRemovedItems.Count > 0 && !IsImportDoWork;
        }
        public virtual void ExecuteDelete()
        {
            if (SelectedRemovedItems == null || SelectedRemovedItems.Count == 0) return;
            foreach (var item in SelectedRemovedItems)
            {
                Items.Remove(item);

            }

        }

        public  void ExecuteImport()
        {

            IsImportDoWork = true;
            var bw = new BackgroundWorker();
            bw.DoWork += DoWorkImport;
            bw.RunWorkerCompleted += AsyncCompletedImport;
            bw.RunWorkerAsync();
        }
        private void DoWorkImport(object sender, DoWorkEventArgs e)
        {
            e.Result = Import();
        }
        private void AsyncCompletedImport(object sender, RunWorkerCompletedEventArgs e)
        {
            var ar = (e.Result as ActionResult);
            if (ar.IsSuccess)
            {
                ViewModelBase.DialogHelper.ShowAlert("Импорт данных выполнен!");
                if (Items != null && Items.Count > 0)
                {
                    JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, "Импорт отчетов УК",
                           String.Join(",", Items.Select(item => item.FileName).ToArray())
                            );
                }
                RefreshViewModels();
            }
            else
            {
                ViewModelBase.DialogHelper.ShowError(string.Format("Ошибка импорта данных! {0}", ar.ErrorMessage));
            }
            IsImportDoWork = false;
            Items.Clear();

        }
        public abstract ActionResult Import();
        public abstract void RefreshViewModels();


       

        public abstract string GetOpenFileMask(bool isXbrl);

        public abstract ActionResult LoadFile(string fileName, bool isXbrl);


       public virtual string GetOpenDirectoryMask()
       {
           return Mask;
       } 

        /// <summary>
        /// Сохранить данные в Журнал
        /// </summary>
        /// <returns></returns>
        public virtual void SaveEDOLog(EDOLog edoLog, string report)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(report);
            edoLog.DocBodyId = DataContainerFacade.Save(new EdoXmlBody { Body = bytes });
            DataContainerFacade.Save(edoLog);
        }
        /// <summary>
        /// Получить регистрационный номер документа
        /// </summary>
        /// <returns></returns>
        public string GetRegNumEDOLog()
        {
            return BLServiceSystem.Client.GetRegNumEDOLog();
        }
        /// <summary>
        /// Список отчетов для формы хххххх
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public List<EDOLogListItem> GetEdoLogList(string form)
        {
            return BLServiceSystem.Client.GetSuccessLoadedODKByForm(new DateTime(1970, 1, 1), DateTime.Now, form);
            //BLServiceSystem.Client.GetLoadedODKLogList(true, new DateTime(1970, 1, 1), DateTime.Now, form);
        }

        public long? GetIdReportloaded(string form, string docRegNumberOut)
        {
            var edoLoaded = BLServiceSystem.Client.GetSuccessLoadedODKByForm(new DateTime(1970, 1, 1), DateTime.Now, form, docRegNumberOut).FirstOrDefault();
            return edoLoaded == null ? null : edoLoaded.DocID;
            /*
            List<EDOLogListItem> EDOLogList = BLServiceSystem.Client.GetLoadedODKLogList(true, new DateTime(1970, 1, 1), DateTime.Now, form);
            var edoLoaded = EDOLogList.FirstOrDefault(x => x.DocRegNumberOut==docRegNumberOut);
            return edoLoaded == null ? null : edoLoaded.DocID;*/
        }

        public long? GetDuplicateLoadedReportId(string form, int quartal, long year, string dogovorNum)
        {
            var edoLoaded = BLServiceSystem.Client.GetSuccessLoadedODKByForm2(new DateTime(1970, 1, 1), DateTime.Now, form, quartal, year, dogovorNum).FirstOrDefault();
            return edoLoaded?.DocID;
        }

        public Contract GetContractByDogovorNum(string dogovorNum)
        {
            return BLServiceSystem.Client.GetContractByDogovorNum(dogovorNum);
        }

        protected XmlReaderSettings GetReaderSettings()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema(Shema));
            //settings.Schemas.Add(string.Empty, DocumentBase.GetShema("Types.xsd"));
            settings.Schemas.Add(DocumentBase.GetShemaWithIncludes(Shema));
            return settings;
        }
    }
}
