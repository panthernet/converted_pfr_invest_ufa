﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF050 : UKReportImportHandlerBase
    {
        public UKReportImportHandlerF050()
        {
            Mask = "*F050.xml";
            Shema = "F050.xsd";

        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F050 (.xml)|{Mask}";
        }
       
        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            var edo = new EDO_ODKF050();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);

            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }

            var serializer = new XmlSerializer(typeof(EDO_ODKF050));

            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {
                    var xdoc = XDocument.Load(fileName);

                    var xElement = xdoc.Root.Element("UKName").Value;

                    if (xElement.Trim().ToLower().Replace("ё", "е").Equals("сводный отчет", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return ActionResult.TotalReport();
                    }

                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    var doc = (EDO_ODKF050)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();
                    if (Items.Any(i => (i as ImportEdoOdkF050).edoLog.DocRegNumberOut.ToLower().Equals(doc.RegNumberOut.ToLower())))
                    {
                        return ActionResult.Error($"Документ с регистрационным номером \"{doc.RegNumberOut}\" уже загружен для импорта");
                    }

                    Contract contract = null;
                    if (string.IsNullOrEmpty(doc.DogovorNumber))
                    {
                        return ActionResult.Error("В импортируемом файле не указан регистрационный номер договора.");
                    }
                    else
                    {
                        contract = GetContractByDogovorNum(doc.DogovorNumber);

                        if (contract == null)
                        {
                            return ActionResult.Error($"Договор с регистрационным номером \"{doc.DogovorNumber}\" не найден");
                        }
                    }

                    var IdDelete = GetIdReportloaded(xdoc.Root.Name.ToString(), doc.RegNumberOut);
                    var importEdoOdkF050 = new ImportEdoOdkF050();

                    if (IdDelete != null)
                    {
                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме \"F050\"\nс регистрационным номером \"{doc.RegNumberOut}\" существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }
                    }

                    importEdoOdkF050.xmlBody = xdoc.ToString();
                    importEdoOdkF050.F050 = new EdoOdkF050Hib
                    {

                        ID = IdDelete ?? 0,
                        ReportDate = DateUtil.ParseImportDate(doc.ReportDate),
                        RegDate = DateTime.Now.Date,
                        ResPerson = doc.RespPerson,
                        ContractId = contract != null ? contract.ID : (long?) null,
                        Post = doc.Post,
                        AkciiRAO1 = doc.AkciiRAO1,
                        AkciiRAO2 = doc.AkciiRAO2,
                        Ipotech1 = doc.Ipotech1,
                        Ipotech2 = doc.Ipotech2,
                        GCBSubRF1 = doc.GCBSubRF1,
                        GCBSubRF2 = doc.GCBSubRF2,
                        GCB1 = doc.GCB1,
                        GCB2 = doc.GCB2,
                        ObligaciiMO1 = doc.ObligaciiMO1,
                        ObligaciiMO2 = doc.ObligaciiMO2,
                        ObligaciiRHO1 = doc.ObligaciiRHO1,
                        ObligaciiRHO2 = doc.ObligaciiRHO2,
                        Pai1 = doc.Pai1,
                        Pai2 = doc.Pai2,
                        ObligMFO1 = string.IsNullOrEmpty(doc.ObligMFO1) ? (decimal?) null : decimal.Parse(doc.ObligMFO1, CultureInfo.InvariantCulture),
                        ObligMFO2 = string.IsNullOrEmpty(doc.ObligMFO2) ? (decimal?) null : decimal.Parse(doc.ObligMFO2, CultureInfo.InvariantCulture),
                        ItogoCB1 = doc.ItogoCB1,
                        ItogoCB2 = doc.ItogoCB2,
                        RegNumOut = doc.RegNumberOut,
                        F050CBInfos = new List<F050CBInfo>(),
                        Portfolio = doc.InvestCaseName
                    };

                    if(doc.CBInfo != null && doc.CBInfo.Count > 0)
                        foreach (var info in doc.CBInfo)
                        {
                            if (string.IsNullOrEmpty(info.Name) && string.IsNullOrEmpty(info.GRN) && string.IsNullOrEmpty(info.TypeAsset) &&
                                info.Qty == 0 && string.IsNullOrEmpty(info.Price) && info.IDTypeAsset == 0)
                                continue;

                            if (string.IsNullOrEmpty(info.Name) || string.IsNullOrEmpty(info.GRN) || string.IsNullOrEmpty(info.TypeAsset))
                                return ActionResult.Error($"Один из элементов исходных данных CBInfo не соответствует формату XSD схемы! ({info.Name})");

                            importEdoOdkF050.F050.F050CBInfos.Add(new F050CBInfo
                            {
                                Name = info.Name,
                                Num = info.GRN,
                                Count = info.Qty,
                                Price = string.IsNullOrEmpty(info.Price) ? (decimal?) null : decimal.Parse(info.Price, CultureInfo.InvariantCulture),
                                Type = info.TypeAsset,
                                TypeID = info.IDTypeAsset
                            });
                        }

                    importEdoOdkF050.edoLog = new EDOLog
                    {
                                                    DocTable = xdoc.Root.Name.ToString(),
                                                    DocForm = xdoc.Root.Name.ToString(),
                                                    Filename = fName,
                                                    DocRegNumberOut = doc.RegNumberOut,
                                                    RegDate = DateTime.Now.Date,
                                                    ContractNum = doc.DogovorNumber

                                                };

                    importEdoOdkF050.FileName = Path.GetFileName(fileName);
                    importEdoOdkF050.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF050.ReportOnDate = importEdoOdkF050.F050.ReportDate == null
                                                        ? string.Empty
                                                        : ((DateTime)importEdoOdkF050.F050.ReportDate).ToString(
                                                            "dd.MM.yyyy");
                    importEdoOdkF050.ReportOnDateNative = importEdoOdkF050.F050.ReportDate;

                    Items.Add(importEdoOdkF050);
                }
            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    if (ex.InnerException.InnerException == null)
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                    else
                    {
                        ViewModelBase.DialogHelper.ShowAlert(
                            $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                    }
                }
                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }
      
        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {
                foreach (ImportEdoOdkF050 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF050(import.F050, import.edoLog, import.xmlBody);
                }
            }
            catch (Exception ex)
            {

                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F50VRListViewModel), typeof(F50SIListViewModel), typeof(LoadedODKLogListSIViewModel));
        }
    }
}
