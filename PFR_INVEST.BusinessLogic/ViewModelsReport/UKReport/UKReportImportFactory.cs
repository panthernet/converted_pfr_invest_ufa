﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    public class UKReportImportFactory
    {
        public static UKReportImportHandlerBase GetImportHandler(UKReportImportDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case "F015":
                    return new UKReportImportHandlerF015();
                case "F016":
                    return new UKReportImportHandlerF016();
                case "F025":
                    return new UKReportImportHandlerF025();
                case "F026":
                    return new UKReportImportHandlerF026();
                case "F040":
                    return new UKReportImportHandlerF040();
                case "F050":
                    return new UKReportImportHandlerF050();
                case "F060":
                    return new UKReportImportHandlerF060();
                case "F070":
                    return new UKReportImportHandlerF070();
                case "F080":
                    return new UKReportImportHandlerF080();
                case "F140":
                    return new UKReportImportHandlerF140();
                case "F401":
                    return new UKReportImportHandlerF401();
                case "F402":
                    return new UKReportImportHandlerF402();


                default: return new UKReportImportHandlerEmpty();
            }
        }

    }
}
