﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataAccess.Server.UKReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.XmlSchemas;

namespace PFR_INVEST.BusinessLogic.UKReport
{
    class UKReportImportHandlerF080 : UKReportImportHandlerBase
    {
        public UKReportImportHandlerF080()
        {
            Mask = "*F080.xml";
            Shema = "F080.xsd";
        }

        public override string GetOpenFileMask(bool b)
        {
            return $"Отчет F080 (.xml)|{Mask}";
        }

        public override ActionResult LoadFile(string fileName, bool isXbrl)
        {
            var edo = new EDO_ODKF080();
            if (string.IsNullOrEmpty(fileName))
            {
                return ActionResult.Error("Ошибка при выборе файла");
            }

            var fName = Path.GetFileName(fileName);
            if (Items.Any(i => i.FileName.ToLower().Equals(fName.ToLower())))
            {
                return ActionResult.Error($"Файл {fName} уже загружен");
            }

            var serializer = new XmlSerializer(typeof(EDO_ODKF080));
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");

            try
            {
                using (var reader = XmlReader.Create(fileName, GetReaderSettings()))
                {
                    var doc = (EDO_ODKF080)serializer.Deserialize(reader);
                    doc.TrimAllStringProperties();
                    if (Items.Any(i => (i as ImportEdoOdkF080).contract.ContractNumber == doc.DogovorNumber && (i as ImportEdoOdkF080).F080.ReportOnDate == DateUtil.ParseImportDate(doc.ReportOnDate)))
                    {
                        return ActionResult.Error(
                            $"Отчет от {DateUtil.ParseImportDate(doc.DogovorDate).Value.Date} с  номером договора {doc.DogovorNumber}\nуже загружен для импорта");
                    }

                    var importEdoOdkF080 = new ImportEdoOdkF080 {contract = GetContractByDogovorNum(doc.DogovorNumber)};

                    if (importEdoOdkF080.contract == null)
                    {
                        return ActionResult.Error($"Договор с регистрационным номером {doc.DogovorNumber} не найден");
                    }

                    var xdoc = XDocument.Load(fileName);
                    xdoc.Root.AddFirst(new XElement("FILENAME", fName));

                    var edodel = DataContainerFacade.GetList<EdoOdkF080>().Where(e => e.ContractId == importEdoOdkF080.contract.ID && e.ReportOnDate == DateUtil.ParseImportDate(doc.ReportOnDate));
                    var edoDelete = edodel.SingleOrDefault();
                    long? IdDelete = null ;
                    if (edoDelete != null) IdDelete = edoDelete.ID;
                    if (IdDelete != null)
                    {
                        if (!ViewModelBase.DialogHelper.ShowConfirmation(
                            $"Отчет по форме F080\nс для договора № {doc.DogovorNumber} за {doc.ReportOnDate} существует.\nУдалить старый и импортировать новый"))
                        {
                            return ActionResult.Success();
                        }
                    }

                    importEdoOdkF080.xmlBody = xdoc.ToString();
                    importEdoOdkF080.F080 = new EdoOdkF080Hib
                    {
                        ID = IdDelete ?? 0,
                        RegDate = DateTime.Now.Date,
                        ContractId = importEdoOdkF080.contract.ID,
                        InvestCaseName = doc.InvestcaseName,
                        ReportOnDate = DateUtil.ParseImportDate(doc.ReportOnDate),
                        SdPerson = doc.SDPerson,
                        UkPerson = doc.UKPerson,
                        Deals = new List<F80Deal>()
                    };
                    var culture = new CultureInfo("en-US");
                    foreach (var deal in doc.Deal)
                    {
                        importEdoOdkF080.F080.Deals.Add(new F80Deal
                        {
                            Emitent = deal.Emitent,
                            GrDate = DateUtil.ParseImportDate(deal.GRDate),
                            GRN = deal.GRN,
                            OperKind = deal.OperKind,
                            Kind = deal.CBKind,
                            // xml валидация и парсинг не поддерживают разделители тысячных разрядов
                            FactPrice = Convert.ToDecimal(deal.FactPrice, culture),
                            MarketPrice = Convert.ToDecimal(deal.MarketPrice, culture),
                            TradeOrganizer = deal.TradeOrganizer,
                            PriceDeviation = Convert.ToDecimal(deal.PriceDeviation, culture)
                        });
                    }

                    importEdoOdkF080.edoLog = new EDOLog
                    {
                        DocTable = xdoc.Root.Name.ToString(),
                        DocForm = xdoc.Root.Name.ToString(),
                        Filename = fName,
                        RegDate = DateTime.Now.Date,
                        ContractNum = doc.DogovorNumber
                    };

                    importEdoOdkF080.FileName = Path.GetFileName(fileName);
                    importEdoOdkF080.PathName = Path.GetDirectoryName(fileName);
                    importEdoOdkF080.ReportOnDate = doc.ReportOnDate;
                    importEdoOdkF080.ReportOnDateNative = DateUtil.ParseImportDate(doc.ReportOnDate);

                    Items.Add(importEdoOdkF080);
                }
            }
            catch (IOException ex)
            {
                Logger.Instance.Error("Open file exception", ex);
                return new ActionResult { ErrorMessage = "Ошибка доступа к документу. Убедитесь, что документ не открыт в другом приложении и пользователь имеет права доступа к документу.", IsSuccess = false };
            }
            catch (InvalidOperationException ex)
            {
                if (ex.InnerException is XmlSchemaValidationException)
                {
                    ViewModelBase.DialogHelper.ShowAlert(
                        ex.InnerException.InnerException == null
                            ? $"Неверный формат документа: {ex.Message}\n\nПодробности - {ex.InnerException.Message}"
                            : $"Неверный формат документа: {ex.Message}\n\nОписание - {ex.InnerException.InnerException.Message}\n\nПодробности - {ex.InnerException.Message}");
                }
                else
                {
                    ViewModelBase.DialogHelper.ShowAlert("Неверный формат документа");
                    ViewModelBase.Logger.WriteException(ex);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.DialogHelper.ShowAlert("Ошибка открытия документа");
                ViewModelBase.Logger.WriteException(ex);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;
            }

            return ActionResult.Success();
        }

        public override ActionResult Import()
        {
            var ci = Thread.CurrentThread.CurrentCulture;
            var ciUi = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-RU");
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-RU");
            var actionResult = ActionResult.Success();
            try
            {
                foreach (ImportEdoOdkF080 import in Items)
                {
                    actionResult = BLServiceSystem.Client.ImportUKF080(import.F080, import.edoLog, import.xmlBody);
                }
            }
            catch (Exception ex)
            {
                ViewModelBase.Logger.WriteException(ex);
                return ActionResult.Error(ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = ciUi;
                Thread.CurrentThread.CurrentCulture = ci;

            }
            return actionResult;
        }

        public override void RefreshViewModels()
        {
            ViewModelBase.ViewModelManager.RefreshViewModels(typeof(F80VRListViewModel), typeof(F80SIListViewModel), typeof(LoadedODKLogListSIViewModel));

        }
    }
}
