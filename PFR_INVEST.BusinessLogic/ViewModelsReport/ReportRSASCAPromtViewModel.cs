﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.BusinessLogic.Commands;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class ReportRSASCAPromtViewModel : ViewModelCardDialog
	{
		private DateTime? reportDate;
		public DateTime? ReportDate { get { return reportDate; } set { reportDate = value; OnPropertyChanged("ReportDate"); } }

		public Document.Types Type { get; private set; }
        public ICommand RunReportCommand { get; private set; }

        public ObservableCollection<Person> PersonList { get; set; }

        private Person _selectedPerson;
        public Person SelectedPerson
        {
            get { return _selectedPerson; }
            set { _selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }

		public ReportRSASCAPromtViewModel(Document.Types type)
		{
			RunReportCommand = new DelegateCommand(o => IsValid(), o => { });
            PersonList = new ObservableCollection<Person>(PersonHelper.GetAvailablePersons(type));
            SelectedPerson = PersonList.FirstOrDefault();
			Type = type;
		}

		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public bool IsValid()
		{
			return ValidateFields();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case nameof(ReportDate): return ReportDate.ValidateRequired();
					case nameof(SelectedPerson): return _selectedPerson.ValidateRequired();
				}
                return null;
			}
		}

		public ReportRSASCAPrompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportRSASCAPrompt
			{
				Date = ReportDate.Value,
				Person = _selectedPerson,
                Type = Type
			};

			return prompt;
		}
	}
}
