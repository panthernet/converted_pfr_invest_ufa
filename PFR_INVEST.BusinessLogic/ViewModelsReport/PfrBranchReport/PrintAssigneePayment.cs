﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintAssigneePayment : PrintBase
    {
        public int PeriodYear { get; set; }
        public int? PeriodMonth { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }

        DataContainer Data;


        public PrintAssigneePayment()
        {
            Name = "Отчет по правопреемникам";
            TemplateName = Name +".xlsx";

        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWords(PeriodYear, PeriodMonth));
            PrintResponsiblePerformer(Responsible, Performer);

            Fields.Add("PERF_POSITION", Performer == null ? null : Performer.Position);
        }

        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear, PeriodMonth);
        }

        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        class PrintHelper
        {
            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = 11;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;

            internal void AddEmptyRows(int count)
            {
                var r = S.get_Range("A" + RowNumber, "E" + (RowNumber - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
            }

            private Range AddRow()
            {
                Range r = S.get_Range("A" + RowNumber, "E" + RowNumber);
                RowNumber++;
                return r;
            }

            private Range AddRowMerged(string startColumn, string endColumn)
            {
                S.get_Range(startColumn + RowNumber, endColumn + RowNumber).Merge();
                Range r = S.get_Range("A" + RowNumber, "E" + RowNumber);
                
                RowNumber++;
                return r;
            }


            internal void Print(FedoContainer x)
            {
                if (x.Fedo.Special == 1)
                {
                    Range emptyRow = AddRow();
                    emptyRow.Merge();
                    emptyRow.Style = "PfrSplitter";
                }
                else
                {
                    Range r = AddRow();

                    Range r2 = r.get_Range("A1", "B1");
                    r2.Merge();
                    r2.Value2 = x.Fedo.Name;
                    r2.Style = "PfrFEDO";

                    object[] a = new object[3];
                    a[0] = x.OwnReservePayment;
                    a[1] = x.PFRReservePayment;
                    a[2] = x.TotalPayment;

                    r2 = r.get_Range("C1", "E1");
                    r2.Style = "PfrBranch";
                    r2.Value2 = a;
                }
            }

            internal void PrintBranch(PFRBranch Branch, decimal TotalPayment, decimal OwnReservePayment, decimal PFRReservePayment)
            {
                ItemCount++;

                object[] a = new object[5];
                a[0] = ItemCount;
                a[1] = Branch.Name;
                a[2] = OwnReservePayment;
                a[3] = PFRReservePayment;
                a[4] = TotalPayment;

                Range r = AddRow();
                r.Value2 = a;
                r.Style = "PfrBranch";

                r.get_Range("A1", "A1").Style = "PfrBranchNumber";
                r.get_Range("B1", "B1").Style = "PfrBranch-Header";
            }

            internal void PrintTotal(decimal TotalPayment, decimal OwnReservePayment, decimal PFRReservePayment)
            {
                object[] a = new object[5];
                a[0] = "Итого по Российской Федерации:";
                a[1] = null;
                a[2] = OwnReservePayment;
                a[3] = PFRReservePayment;
                a[4] = TotalPayment;

                Range r = AddRowMerged("A", "B");
                r.Value2 = a;
                r.Style = "PfrFEDO";

                r.get_Range("C1", "E1").Style = "PfrBranch";
            }
        }


        class DataContainer
        {
            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.AssigneePayment;

            public DataContainer()
            {
                FedoList = new List<FedoContainer>();
            }

            public List<FedoContainer> FedoList;

            public decimal TotalPayment { get; set; }
            public decimal OwnReservePayment { get; set; }
            public decimal PFRReservePayment { get; set; }

            public void CalcSum()
            {
                TotalPayment = OwnReservePayment = PFRReservePayment = 0;
                
                foreach (var x in FedoList)
                {
                    x.CalcSum();

                    TotalPayment += x.TotalPayment;
                    OwnReservePayment += x.OwnReservePayment;
                    PFRReservePayment += x.PFRReservePayment;
                }
            }

            internal void LoadData(int PeriodYear, int? PeriodMonth)
            {
                List<FEDO> Fedos;
                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportAssigneePayment> ReportData;

                Fedos = DataContainerFacade.GetList<FEDO>().OrderBy(x => x.Name).ToList();
                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = PeriodYear });
                    if (PeriodMonth.HasValue)
                    {
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = PeriodMonth, Operation = "eq" });
                    }
                    else
                    {
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = 1, Operation = "ge" });
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = 12, Operation = "le" });
                    }
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportId", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportAssigneePayment>(pL).ToList();
                }

                LoadData(Fedos, Branches, Reports, ReportData);
            }

            internal void LoadData(List<FEDO> fedos, List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportAssigneePayment> reportData)
            {
                Dictionary<long, FedoContainer> fD = new Dictionary<long, FedoContainer>();

                foreach (var x in fedos.OrderBy(x => x.ID))
                {
                    var y = new FedoContainer { Fedo = x };
                    FedoList.Add(y);
                    fD.Add(x.ID, y);
                }

                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x };
                    fD[x.FEDO_ID].AddBranch(y);
                    bD.Add(x.ID, y);
                }

                Dictionary<long, ReportItemContainer> rD = new Dictionary<long, ReportItemContainer>();

                foreach (var x in from y in reports orderby y.PeriodYear, y.PeriodMonth select y)
                {
                    var y = new ReportItemContainer { Report = x };
                    bD[x.BranchID].AddItem(y);
                    rD.Add(x.ID, y);
                }

                foreach (var x in reportData)
                    rD[x.ReportId].Data = x;

                CalcSum();
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);

                //return;
                foreach (var x in FedoList)
                    x.Print(ph);

                ph.PrintTotal(TotalPayment, OwnReservePayment, PFRReservePayment);

            }

            private int GetRowCount()
            {
                int n = 1;
                foreach (var x in FedoList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (FedoList == null || FedoList.Count == 0)
                    return true;

                foreach (var x in FedoList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }


            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in FedoList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }

        }

        class FedoContainer
        {
            public FedoContainer()
            {
                BranchList = new List<BranchContainer>();
            }

            public FEDO Fedo { get; set; }
            public List<BranchContainer> BranchList { get; set; }

            public decimal TotalPayment { get; set; }
            public decimal OwnReservePayment { get; set; }
            public decimal PFRReservePayment { get; set; }

            public void CalcSum()
            {
                TotalPayment = OwnReservePayment = PFRReservePayment = 0;

                foreach (var x in BranchList)
                {
                    x.CalcSum();
                    TotalPayment += x.TotalPayment;
                    OwnReservePayment += x.OwnReservePayment;
                    PFRReservePayment += x.PFRReservePayment;
                }
            }

            internal void AddBranch(BranchContainer y)
            {
                BranchList.Add(y);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);
                foreach (var x in BranchList)
                    x.Print(ph);
            }

            internal int GetRowCount()
            {
                return 1 + /*(Fedo.Special == 0 ? 1 : 0)*/ + BranchList.Count;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        res.Add(x.Branch);

                return res;
            }
        }

        class BranchContainer
        {
            public BranchContainer()
            {
                ReportList = new List<ReportItemContainer>();
            }

            public PFRBranch Branch { get; set; }
            public List<ReportItemContainer> ReportList { get; set; }

            public decimal TotalPayment { get; set; }
            public decimal OwnReservePayment { get; set; }
            public decimal PFRReservePayment { get; set; }

            public void CalcSum()
            {
                TotalPayment = OwnReservePayment = PFRReservePayment = 0;

                if (ReportList == null)
                    return;

                foreach (var x in ReportList)
                {
                    TotalPayment += x.Data.TotalPayment;
                    OwnReservePayment += x.Data.OwnReservePayment;
                    PFRReservePayment += x.Data.PFRReservePayment;
                }
            }

            internal void AddItem(ReportItemContainer r)
            {
                ReportList.Add(r);
            }

            internal void Print(PrintHelper ph)
            {
                ph.PrintBranch(Branch, TotalPayment, OwnReservePayment, PFRReservePayment);
            }

            internal bool IsDataIncomplete()
            {
                if (ReportList == null || ReportList.Count != 12)
                    return true;

                foreach (var x in ReportList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }
}

        class ReportItemContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportAssigneePayment Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;

                return false;
            }
        }

    }
}




