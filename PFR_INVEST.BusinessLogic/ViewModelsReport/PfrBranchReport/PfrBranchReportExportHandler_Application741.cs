﻿using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportExportHandler_Application741 : PfrBranchReportExportHandlerBase
    {
        private RegionReportExportDlgViewModel Model;

        public PfrBranchReportExportHandler_Application741()
        {
        }

        public PfrBranchReportExportHandler_Application741(RegionReportExportDlgViewModel model)
            : this()
        {
            Model = model;
        }

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.Applications741;

        public override bool CanExecuteExport()
        {
            if (Model.SelectedYear == null || !Model.Validate())
                return false;
            /*
            if (Model.SelectedMonth == null)
                return false;
            */
            return true;
        }


        public override void ExecuteExport()
        {
            if (!CanExecuteExport())
                return;

            ExecuteExport(Model.SelectedYear.Value, Model.SelectedMonth, Model.Responsible, Model.Performer);
        }

        public void ExecuteExport(int periodYear, int? periodMonth, Person responsible, Person performer)
        {
            var x = new PrintApplication741
            {
                PeriodYear = periodYear,
                PeriodMonth = periodMonth,
                Responsible = responsible,
                Performer = performer
            };
            x.LoadData();


            // if we don't have complete data for report, ask user if he wants to make report
            if (x.IsReportDataIncomplete() && !Model.AskReportDataIncomplete(x.GetDataIncompleteMessage()))
                return;

            x.Print(true);
        }

        public override bool IsPeriodLengthVisible => true;

        public override bool IsPeriodLengthEnabled => true;
    }
}

