﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportXmlHandler_NpfNotice : PfrBranchReportImportXmlHandlerBase
    {
        private Dictionary<string, long> npfIndex;

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.XmlNpfNotice;

        public override bool LoadFile(string fileName)
        {
            return LoadFile(fileName, LoadData);
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public void LoadData(string fileName, XDocument document, out bool isloadFileCorrect)
        {
            var start = DateTime.Now;

            var convert = document.Element("Convert");
            if (convert != null)
            {
                var templateName = convert.Element("TemplateName");
                if (templateName != null)
                {
                    var templateNameValue = templateName.Value;
                    var nameParts = templateNameValue.Split(new[] { "!!" }, StringSplitOptions.RemoveEmptyEntries);
                    templateNameValue = nameParts[2];

                    if (!templateNameValue.ToLower().Contains("уведомления"))
                    {
                        throw new PfrBranchReportImportException(string.Format("Выбранный тип отчета не соответствует типу отчета из xml-файла: {0}", templateNameValue));
                    }
                }

                // Разделитель для дробных чисел
                //var decimalSeparator = convert.Element("DecimalSymbol").Value;

                var rows = convert.Descendants("Sheet").First().Descendants("row");

                // Дата отчета
                var reportMonth = 0;
                var reportYear = 0;
                var startRowId = 8;
                var lastRowId = rows.Last().Attribute("id").Value.To<int>();
                var parsedData = new Dictionary<string, PfrBranchReportImportContent_NpfNotice>();
                var currentRegionName = "";
                
                foreach (var row in rows)
                {
                    var rowId = row.Attribute("id").Value.To<int>();
                    var columns = row.Descendants("column");

                    // Получение месяца отчета
                    if (rowId == 1)
                    {
                        var monthColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "12");
                        if (monthColumn != null)
                        {
                            var month = DateTools.Months.First(x => x.ToLower() == monthColumn.Value.ToLower());
                            reportMonth = DateTools.Months.ToList().IndexOf(month) + 1;
                        }
                    }
                    // Получение года отчета
                    else if (rowId == 2)
                    {
                        var yearColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "12");
                        if (yearColumn != null)
                        {
                            reportYear = yearColumn.Value.To(0);
                        }
                    }
                    else if (rowId >= startRowId && rowId <= lastRowId)
                    {
                        var isRegionRow = false; // Строка на которой определен регион

                        // Парсим данные
                        // Получаем номер п/п
                        // Наличие региона определяется наличием числового значения в колонке с id=1
                        var columnNumber = columns.FirstOrDefault(x => x.Attribute("id").Value == "1");
                        if (columnNumber != null)
                        {
                            // Проверяем, число ли там
                            if (columnNumber.Value.To(0) > 0)
                            {   
                                var regionName = columns.First(x => x.Attribute("id").Value == "2").Value.Trim();

                                // Создаем объект отчета
                                var r = new PfrBranchReport
                                {
                                    CreatedDate = DateTime.Now,
                                    Branch = GetBranchByName(regionName),
                                    ReportTypeID = (long)ReportType,
                                    FileName = Path.GetFileName(fileName),
                                    PeriodMonth = reportMonth,
                                    PeriodYear = reportYear,
                                    StatusID = 1
                                };
                                if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
                                    throw new InvalidDataException("Некорректный год");
                                if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
                                    throw new InvalidDataException("Некорректный месяц");

                                if (r.Branch == null)
                                    throw new InvalidDataException("Некорректное название региона: " + regionName);
                                r.BranchID = r.Branch.ID;

                                var report = new PfrBranchReportImportContent_NpfNotice
                                {
                                    
                                    PfrBranchReport = r,
                                    Items = new List<PfrBranchReportNpfNotice>()
                                };

                                currentRegionName = regionName;
                                parsedData.Add(currentRegionName, report);

                                isRegionRow = true;
                            }   
                        }

                        // Парсим строку с данными, опираясь на то, что в колонке с id=3 хранится название НПФ
                        var npfColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "3");
                        var npfPresented = npfColumn != null && npfColumn.Value.Trim().Length > 1;
                        if (!npfPresented && isRegionRow)
                        {
                            // Не указано название НПФ. Проверяем данные по региону, если они есть, значит генерим ошибку
                            var hasData = false;
                            foreach (var xColumn in columns.Where(x => x.Attribute("id").Value.To<int>() >= 4))
                            {
                                var strValue = xColumn.Value;
                                long longValue = -1L;
                                if (!strValue.IsEmpty())
                                {
                                    longValue = strValue.To(-1L);
                                }

                                hasData = longValue > 0;

                                if (hasData) break;
                            }

                            if (hasData)
                            {
                                var msg = $"Для региона \"{currentRegionName}\" не найдено ни одного НПФ";

                                if (currentRegionName.ToLower().Contains("опфр"))
                                {
                                    msg = $"Для \"{currentRegionName}\" не найдено ни одного НПФ";
                                }

                                throw new InvalidDataException(msg);
                            }
                        }

                        if (npfColumn != null)
                        {
                            // Получаем название НПФ, и определяем, есть ли он у нас в БД
                            var npfName = npfColumn.Value.Trim();

                            // Если длина названия > 1 (Иногда встречаются итоговые строки, и там в качестве названия НПФ "0"), считаем что это НПФ
                            if (npfName.Length > 1)
                            {
                                // НПФ найден, получаем данные для этого НПФ

                                var reportRow = new PfrBranchReportNpfNotice();
                                reportRow.LegalEntityID = GetNpfID(npfName);
                                reportRow.ContractPtkSpu = 0;

                                foreach (var xColumn in columns.Where(x => x.Attribute("id").Value.To<int>() >= 4))
                                {
                                    var strValue = xColumn.Value;
                                    long longValue = -1L;
                                    if (!strValue.IsEmpty())
                                    {
                                        longValue = strValue.To(-1L);
                                    }

                                    switch (xColumn.Attribute("id").Value)
                                    {
                                        // Общее количество поступивших
                                        case "4":
                                            // уведомлений
                                            reportRow.NoticeTotal = longValue;
                                            break;

                                        case "5":
                                            // договоров об ОПС
                                            reportRow.ContractTotal = longValue;
                                            break;

                                        // Количество вновь поступивших
                                        case "6":
                                            // уведомлений
                                            reportRow.NoticeNew = longValue;
                                            break;

                                        case "7":
                                            // договоров об ОПС
                                            reportRow.ContractNew = longValue;
                                            break;

                                        // Количество ранее поступивших (без учета вновь поступивших)
                                        case "8":
                                            // уведомлений
                                            reportRow.NoticeExisting = longValue;
                                            break;

                                        case "9":
                                            // договоров об ОПС
                                            reportRow.ContractExisting = longValue;
                                            break;

                                        // Количество извещений, направленных НПФ
                                        case "10":
                                            // уведомлений
                                            reportRow.NoticeToNpf = longValue;
                                            break;

                                        case "11":
                                            // договоров об ОПС
                                            reportRow.ContractToNpfReturned = longValue;
                                            break;
                                    }
                                }

                                parsedData[currentRegionName].Add(reportRow);
                            }
                        }
                    }
                }

                foreach (var item in parsedData)
                {
                    if (item.Value.Items.Any())
                    {
                        Items.Add(item.Value);
                    }
                }
            }

            var delta = DateTime.Now - start;
            Debug.WriteLine("Parse NpfNotice Report Duration = " + delta);

            isloadFileCorrect = true;
        }

        private long GetNpfID(string name)
        {
            // Создаем индекс списка НПФ по формализованному названию
            if (npfIndex == null)
            {
                npfIndex = new Dictionary<string, long>();

                var list = BLServiceSystem.Client.GetNPFListLEReorgHib().ToList();
                foreach (var npf in list)
                {
                    if (npf.FormalizedName == null)
                        continue;

                    string s = npf.FormalizedName.ToLower().Trim();

                    if (!npfIndex.ContainsKey(s))
                        npfIndex.Add(s, npf.ID);
                }
            }

            if (string.IsNullOrEmpty(name))
                throw new PfrBranchReportImportException("Некорректное значение наименования НПФ");

            name = name.Trim().ToLower();

            var k = npfIndex.Keys.FirstOrDefault(x => name.Contains(x));
            if (k != null)
            {
                return npfIndex[k];
            }

            throw new PfrBranchReportImportException(string.Format("Наименование НПФ '{0}' отсутствует в справочнике НПФ", name));
        }
    }
}
