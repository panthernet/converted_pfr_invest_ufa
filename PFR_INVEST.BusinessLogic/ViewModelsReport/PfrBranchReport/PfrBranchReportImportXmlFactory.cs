﻿using System;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
	public class PfrBranchReportImportXmlFactory
    {
       public static PfrBranchReportImportXmlHandlerBase GetImportHandler(RegionReportImportXmlDlgViewModel model)
		{
			switch (model.SelectedReportType.ReportType)
			{
                case PfrBranchReportType.BranchReportType.XmlCashExpenditures:
                    return new PfrBranchReportImportXmlHandler_CashExpenditures();
                case PfrBranchReportType.BranchReportType.XmlInsuredPerson:
					return new PfrBranchReportImportXmlHandler_InsuredPerson();
                case PfrBranchReportType.BranchReportType.XmlApplications741:
                    return new PfrBranchReportImportXmlHandler_Application741();
                case PfrBranchReportType.BranchReportType.XmlAssigneePayment:
                    return new PfrBranchReportImportXmlHandler_AssigneePayment();
                case PfrBranchReportType.BranchReportType.XmlDeliveryCost:
                    return new PfrBranchReportImportXmlHandler_DeliveryCost();
                case PfrBranchReportType.BranchReportType.XmlNpfNotice:
                    return new PfrBranchReportImportXmlHandler_NpfNotice();
				
				default: throw new NotImplementedException();
			}
		}
    }
}
