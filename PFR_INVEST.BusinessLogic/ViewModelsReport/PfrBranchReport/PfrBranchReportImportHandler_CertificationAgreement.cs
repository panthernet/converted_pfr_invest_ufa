﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_CertificationAgreement : PfrBranchReportImportHandlerBase
    {
        private const string Mask = "SRP_03_*.xls;SRP_03_*.xlsx";
        private bool _allNpfExists = true;
        private string _message = string.Empty;

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.CertificationAgreement;

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_03_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            errorMessage = string.Empty;
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }


        public override bool LoadFile(string fileName, out string message)
        {
            PfrBranchReport r;

            if (!TryParseFileName(fileName, out r))
            {
                message = string.Format("Некорректное имя файла '{0}'", fileName);
                return false;
            }

            bool isFileLoadedCorrect = LoadFile(fileName, r);
            message = _message;

            return isFileLoadedCorrect;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            var report = new PfrBranchReportImportContent_CertificationAgreement
            {
                Report = new List<PfrBranchReportCertificationAgreement>(),
                PfrBranchReport = r
            };

            if (LoadFile(fileName, LoadData, report))
            {
                Items.Add(report);
                return true;
            }
            return false;
        }

        private void LoadData(Worksheet sheet, PfrBranchReportImportContentBase parameter, out bool isloadFileCorrect)
        {
            var report = (PfrBranchReportImportContent_CertificationAgreement)parameter;

            var npfCount = ((Range)sheet.Cells[14, 1]).MergeArea.Cells.Count - 1;
            Dictionary<string, long> npfNameIdMap;
            if (ValidateExcelDocument(sheet, npfCount, out npfNameIdMap))
            {
                isloadFileCorrect = false;
                return;
            }

            report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.NpfSum, 14));
            for (int i = 0; i < npfCount; i++)
                report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.Npf, 15 + i, npfNameIdMap[sheet.GetCellRange(15 + i, 2).Value2.ToString().ToLower()]));

            report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.Credit, 15 + npfCount));
            report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.Employer, 16 + npfCount));
            report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.Communication, 17 + npfCount));
            report.Add(GetRow(sheet, PfrBranchReportCertificationAgreementType.enRowType.SumByOPFR, 18 + npfCount));

            isloadFileCorrect = true;
        }

        private bool ValidateExcelDocument(Worksheet sheet, int npfCount, out Dictionary<string, long> npfNameIdMap)
        {
            npfNameIdMap = new Dictionary<string, long>(npfCount);

            for (int i = 0; i < 3; i++)
            {
                object value = sheet.GetCellRange(15 + npfCount + i, 1).Value2;
                if (value == null || value.ToString() != (i + 2).ToString())
                {
                    _message = string.Format("Неверный формат документа. Ожидалось в ячейке (строка: {0}, колонка: {1}) значение: {2}", 15 + npfCount + i, 1, i + 2);
                    _allNpfExists = false;
                    return true;
                }
            }

            _allNpfExists = true;
            StatusFilter sf = StatusFilter.NotDeletedFilter;

            var npfList = BLServiceSystem.Client.GetNPFListLEHib(sf).ToList();

            for (int i = 0; i < npfCount; i++)
            {
                if (!checkRowOnDigital(sheet, 15 + i, 3, 7))
                {
                    _allNpfExists = false;
                    return true;
                }

                var name = sheet.GetCellRange(15 + i, 2).Value2.ToString().Trim().ToLower();
                var npf = npfList.FirstOrDefault(x => x.FormalizedName.Trim().ToLower() == name);
                if (npf != null)
                    npfNameIdMap.Add(npf.FormalizedName.Trim().ToLower(), npf.ID);
                else
                {
                    _message = string.Format("НПФ с формализованым именем: \"{0}\", отсутствует в базе данных",
                                             sheet.GetCellRange(15 + i, 2).Value2);
                    _allNpfExists = false;
                    return true;
                }
            }

            for (int i = npfCount; i < npfCount + 3; i++)
            {
                if (!checkRowOnDigital(sheet, 15 + i, 3, 7))
                {
                    _allNpfExists = false;
                    return true;
                }
            }

            return false;
        }

        protected Boolean checkRowOnDigital(Worksheet sheet, int rowNumber, int startColumn, int endColumn)
        {
            for (int valIndex = startColumn; valIndex < endColumn; valIndex++)
            {
                String value = sheet.GetCellRange(rowNumber, valIndex).Value2.ToString().Trim().ToLower();
                Double digital = 0;
                if (!Double.TryParse(value, out digital))
                {
                    _message = string.Format("Неверный формат документа. Ожидалось в ячейке (строка: {0}, колонка: {1}) значение в цифровом формате. Текущее значение равно '{2}'", rowNumber, valIndex, value);
                    return false;
                }
            }
            return true;
        }

        private PfrBranchReportCertificationAgreement GetRow(Worksheet sheet, PfrBranchReportCertificationAgreementType.enRowType rowType, long row, long? npfId = null)
        {
            var r = new PfrBranchReportCertificationAgreement();
            long value;

            object x = sheet.GetCellRange(row, 3).Value2;
            if (x != null && long.TryParse(x.ToString(), out value))
                r.Summary = value;

            x = sheet.GetCellRange(row, 4).Value2;
            if (x != null && long.TryParse(x.ToString(), out value))
                r.New = value;

            x = sheet.GetCellRange(row, 5).Value2;
            if (x != null && long.TryParse(x.ToString(), out value))
                r.Existing = value;

            x = sheet.GetCellRange(row, 6).Value2;
            if (x != null && long.TryParse(x.ToString(), out value))
                r.Terminated = value;

            r.TypeId = (long)rowType;
            if (npfId.HasValue)
                r.NpfId = npfId;

            return r;
        }
    }
}
