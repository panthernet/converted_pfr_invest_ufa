﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
	public class PfrBranchReportImportXmlHandler_InsuredPerson : PfrBranchReportImportXmlHandlerBase
    {
        private string _message;
        private bool _isCorrectFormat = true;
        private string _errorMsg = "Файл содержит некорректные данные. Импорт не может быть произведен. Ожидалось целое число в строке: {0}, колонка: {1}. Текущее значение '{2}'.";

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.XmlInsuredPerson;

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool LoadFile(string fileName, out string message)
        {
            if (!TryParseFileName(fileName))
            {
                message = string.Format("Некорректное имя файла '{0}'", fileName);
                return false;
            }

            bool isFileLoadedCorrect = LoadFile(fileName);
            message = _message;

            return isFileLoadedCorrect;
        }

        public override bool LoadFile(string fileName)
        {
	        return LoadFile(fileName, PopulateData);
        }
        
        private void PopulateData(string fileName, XDocument document, out bool isloadFileCorrect)
		{
			var start = DateTime.Now;

			var convert = document.Element("Convert");
			if (convert != null)
			{
				var templateName = convert.Element("TemplateName");
				if (templateName != null)
				{
					var templateNameValue = templateName.Value;
					var nameParts = templateNameValue.Split(new[] { "!!" }, StringSplitOptions.RemoveEmptyEntries);
					templateNameValue = nameParts[2];

					if (!templateNameValue.ToLower().Contains("принятые заявления"))
					{
						_message = string.Format("Выбранный тип отчета не соответствует типу отчета из xml-файла: {0}", templateNameValue);

						isloadFileCorrect = false;

						return;
					}
				}

				var types = DataContainerFacade.GetList<PfrBranchReportInsuredPersonType>().OrderBy(item => item.ID);
				var typesCount = types.Count();
				// var typesDict = types.ToDictionary(x => x.Name.ToLower(), x => x.ID);
                
				// Разделитель для дробных чисел
				//var decimalSeparator = convert.Element("DecimalSymbol").Value;

				var rows = convert.Descendants("Sheet").First().Descendants("row");

				// Дата отчета
				var reportMonth = 0;
				var reportYear = 0;
				var startRowId = 22;
				var lastRowId = rows.Last().Attribute("id").Value.To<int>();
				var parsedData = new Dictionary<string, PfrBranchReportImportContent_InsuredPerson>();
				var currentRegionName = "";
				var currentReportTypeIndex = 0;
                
				foreach (var row in rows)
				{
					var rowId = row.Attribute("id").Value.To<int>();
					var columns = row.Descendants("column");

                    // Получение месяца отчета
                    if (rowId == 1)
                    {
                        var monthColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "17");
                        if (monthColumn != null)
                        {
                            var month = DateTools.Months.First(x => x.ToLower() == monthColumn.Value.ToLower());
                            reportMonth = DateTools.Months.ToList().IndexOf(month) + 1;
                        }
                    }
                    // Получение года отчета
                    else if (rowId == 2)
                    {
                        var yearColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "17");
                        if (yearColumn != null)
                        {
                            reportYear = yearColumn.Value.To(0);
                        }
                    }
					else if (rowId >= startRowId && rowId <= lastRowId)
					{
						// Парсим данные
						// Получаем номер п/п
						var columnNumber = columns.FirstOrDefault(x => x.Attribute("id").Value == "1");
						if (columnNumber != null)
						{
							// Проверяем, число ли там
							if (columnNumber.Value.To(0) > 0)
							{
								var regionNumber = columns.First(x => x.Attribute("id").Value == "2").Value.To<int>();
								var regionName = columns.First(x => x.Attribute("id").Value == "3").Value.Trim();
                                
								// Создаем объект отчета
								var r = new PfrBranchReport
								{
									CreatedDate = DateTime.Now,
									Branch = GetBranchByName(regionName),
									ReportTypeID = (long) ReportType,
									FileName = Path.GetFileName(fileName),
									PeriodMonth = reportMonth,
									PeriodYear = reportYear,
									StatusID = 1
								};
								if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
									throw new InvalidDataException("Некорректный год");
								if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
									throw new InvalidDataException("Некорректный месяц");

                                if (r.Branch == null)
                                    throw new InvalidDataException("Некорректное название региона: " + regionName);
                                r.BranchID = r.Branch.ID;

                                var report = new PfrBranchReportImportContent_InsuredPerson
								{
									PfrBranchReport = r,
									Report = new List<PfrBranchReportInsuredPerson>()
								};

								currentRegionName = regionName;
								parsedData.Add(currentRegionName, report);
								currentReportTypeIndex = 0;
							}
						}

						// Парсим строку с данными, опираясь на тип currentReportTypeIndex
						// Проверяем наличие данные по данному региону
						// О наличии данных свидетельствует существование column id="5"
						if (currentReportTypeIndex < typesCount && columns.Any(x => x.Attribute("id").Value == "5"))
						{
							var reportRow = new PfrBranchReportInsuredPerson();

							foreach (var xColumn in columns.Where(x => x.Attribute("id").Value.To<int>() >= 5))
							{
								var strValue = xColumn.Value;
								long longValue = -1L;
								if (!strValue.IsEmpty())
								{
									longValue = strValue.To(-1L);
								}

								switch (xColumn.Attribute("id").Value)
								{
									case "5":
										// Всего принято заявлений
										reportRow.Summary = longValue;
										break;

									// территориальными органами ПФР при личном обращении застрахованного лица 
									case "6":
										// Всего
										reportRow.TerritoryPersonalAppealSum = longValue;
										break;

									case "7":
										// 1 группа**
										reportRow.TerritoryPersonalAppealGroup1 = longValue;
										break;

									case "8":
										// 2 группа**
										reportRow.TerritoryPersonalAppealGroup2 = longValue;
										break;

									case "9":
										// 3 группа**
										reportRow.TerritoryPersonalAppealGroup3 = longValue;
										break;

									case "10":
										// органами (организациями), заключившими с ПФР соглашения о взаимном удостоверении подписей (ТАЦ), в соответствии с реестрами заявлений
										reportRow.Agency = longValue;
										break;

									// территориальными органами ПФР иным способом			
									case "11":
										// Всего
										reportRow.TerritoryOtherSum = longValue;
										break;

									case "12":
										// 1 группа**
										reportRow.TerritoryOtherGroup1 = longValue;
										break;

									case "13":
										// 2 группа**
										reportRow.TerritoryOtherGroup2 = longValue;
										break;

									case "14":
										// 3 группа**
										reportRow.TerritoryOtherGroup3 = longValue;
										break;

									case "15":
										// поданными через единый портал гос.услуг
										reportRow.Gosuslugi = longValue;
										break;

									case "16":
										// поданными через МФЦ
										reportRow.Mfc = longValue;
										break;
								}
							}

							reportRow.TypeId = types.ElementAt(currentReportTypeIndex).ID;

							parsedData[currentRegionName].Add(reportRow);
						}

						currentReportTypeIndex++;
					}
				}

				foreach (var item in parsedData)
				{
					if (item.Value.Report.Any())
					{
						Items.Add(item.Value);
					}
				}
			}

			var delta = DateTime.Now - start;
			Debug.WriteLine("Parse InsuredPerson Report Duration = " + delta);

            isloadFileCorrect = true;
        }
    }
}
