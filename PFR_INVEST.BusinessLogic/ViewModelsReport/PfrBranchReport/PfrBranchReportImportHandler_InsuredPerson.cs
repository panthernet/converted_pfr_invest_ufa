﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_InsuredPerson : PfrBranchReportImportHandlerBase
    {
        private string _message;
        private bool _isCorrectFormat = true;
        private string _errorMsg = "Файл содержит некорректные данные. Импорт не может быть произведен. Ожидалось целое число в строке: {0}, колонка: {1}. Текущее значение '{2}'.";

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.InsuredPerson;

        private const string Mask = "SRP_01_*.xls;SRP_01_*.xlsx";

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_01_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            errorMessage = string.Empty;
            return true;
        }

        public override bool LoadFile(string fileName, out string message)
        {
            PfrBranchReport r;

            if (!TryParseFileName(fileName, out r))
            {
                message = string.Format("Некорректное имя файла '{0}'", fileName);
                return false;
            }

            bool isFileLoadedCorrect = LoadFile(fileName, r);
            message = _message;

            return isFileLoadedCorrect;
        }


        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            PfrBranchReportImportContent_InsuredPerson report = new PfrBranchReportImportContent_InsuredPerson();
            report.Report = new List<PfrBranchReportInsuredPerson>();
            report.PfrBranchReport = r;

            if (LoadFile(fileName, PopulateData, report))
            {
                Items.Add(report);
                return true;
            }
            return false;
        }

        private void PopulateData(Worksheet sheet, PfrBranchReportImportContentBase parameter, out bool isloadFileCorrect)
        {
            var report = (PfrBranchReportImportContent_InsuredPerson)parameter;

            var types = DataContainerFacade.GetList<PfrBranchReportInsuredPersonType>().OrderBy(item => item.ID);
            int startedRow = 14;
            long value;
            object excelValue = null;
            _isCorrectFormat = true;

            foreach (PfrBranchReportInsuredPersonType type in types)
            {
                long row = startedRow + type.ID;
                PfrBranchReportInsuredPerson reportRow = new PfrBranchReportInsuredPerson();

                excelValue = sheet.GetCellRange(row, 3).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.Summary = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 3, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 4).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryPersonalAppealSum = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 4,  excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 5).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryPersonalAppealGroup1 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 5, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 6).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryPersonalAppealGroup2 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 6, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 7).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryPersonalAppealGroup3 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 7, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 8).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.Agency = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 8, excelValue);
                    isloadFileCorrect =_isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 9).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryOtherSum = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 9, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 10).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryOtherGroup1 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 10, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 11).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryOtherGroup2 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 11, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 12).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.TerritoryOtherGroup3 = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 12, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 13).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.Gosuslugi = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 13, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                excelValue = sheet.GetCellRange(row, 14).Value2;
                if (excelValue != null && long.TryParse(excelValue.ToString(), out value))
                    reportRow.Mfc = value;
                else if (excelValue == null || !long.TryParse(excelValue.ToString(), out value))
                {
                    _message = string.Format(_errorMsg, row, 14, excelValue);
                    isloadFileCorrect = _isCorrectFormat = false;
                    return;
                }

                reportRow.TypeId = type.ID;
                report.Add(reportRow);                
            }

            isloadFileCorrect = true;
        }
    }
}
