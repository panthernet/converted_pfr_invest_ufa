﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintDeliveryCost : PrintBase
    {
        public int PeriodYear { get; set; }
        public int? PeriodMonth { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        DataContainer Data;


        public PrintDeliveryCost()
        {
            Name = "Отчет по доставке ЕВ";
            TemplateName = Name + ".xlsx";
        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWords(PeriodYear, PeriodMonth));

            PrintResponsiblePerformer(Responsible, Performer);
        }


        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear, PeriodMonth);
        }


        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        class PrintHelper
        {
            private const int START_ROW = 11;

            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = START_ROW;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;

            internal void AddEmptyRows(int count)
            {
                var r = S.get_Range("A" + START_ROW, "E" + (START_ROW - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
            }

            private Range AddRow()
            {
                Range r = S.get_Range("A" + RowNumber, "E" + RowNumber);
                RowNumber++;
                return r;
            }

            private void SkipRows(int count)
            {
                RowNumber += count;
            }

            private Range GetARows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "A" + (RowNumber + count - 1));
                return r;
            }

            private Range GetBRows(int count)
            {
                Range r = S.get_Range("B" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetABRows(int count)
            {
                Range r = S.get_Range("A" + RowNumber, "B" + (RowNumber + count - 1));
                return r;
            }

            private Range GetNumRows(int count)
            {
                Range r = S.get_Range("C" + RowNumber, "E" + (RowNumber + count - 1));
                return r;
            }

            internal void PrintBranch(PFRBranch Branch, decimal Sum_226, decimal Sum_400, decimal Sum_472)
            {
                ItemCount++;

                object[] a = new object[5];
                a[0] = ItemCount;
                a[1] = Branch.Name;
                a[2] = Sum_226;
                a[3] = Sum_400;
                a[4] = Sum_472;

                Range r = AddRow();
                r.Value2 = a;
                r.Style = "PfrBranch";
            }

            internal void Print(DataContainer dataContainer)
            {
                Range r = GetABRows(1);
                r.Merge();
                r.Style = "PfrTotal";
                r.Value2 = "Итого по ОПФР:";

                r = GetNumRows(1);
                r.Style = "PfrNumber";
                r.Value2 = dataContainer.S.GetArray();

                SkipRows(1);
            }

            internal void Print(FedoContainer fedoContainer)
            {
                if (fedoContainer.Fedo.Special == 1)
                {
                    Range r = AddRow();
                    r.Style = "PfrSplitter";
                }
                else
                {
                    Range r = GetABRows(1);
                    r.Merge();
                    r.Style = "PfrFedo";
                    r.Value2 = fedoContainer.Fedo.Name;

                    r = GetNumRows(1);
                    r.Style = "PfrNumber";
                    r.Value2 = fedoContainer.S.GetArray();

                    SkipRows(1);
                }
            }

            internal void PrintBranch(BranchContainer branchContainer)
            {
                ItemCount++;

                Range r = GetARows(1);
                r.Style = "PfrBranchIndex";
                r.Value2 = ItemCount;

                r = GetBRows(1);
                r.Style = "PfrBranch";
                r.Value2 = branchContainer.Branch.Name;

                r = GetNumRows(1);
                r.Style = "PfrNumber";
                r.Value2 = branchContainer.S.GetArray();

                SkipRows(1);
            }
        }


        class Sum
        {
            public const int ARRAY_SIZE = 3;

            public decimal[] s = new decimal[ARRAY_SIZE];

            public decimal Sum_221 { get { return s[0]; } set { s[0] = value; } }
            public decimal Sum_226 { get { return s[1]; } set { s[1] = value; } }
            public decimal Sum_Total { get { return s[2]; } set { s[2] = value; } }

            public void Add(Sum x)
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] += x.s[i];
            }

            public void Clear()
            {
                for (int i = 0; i < ARRAY_SIZE; i++)
                    s[i] = 0;
            }

            internal object[] GetArray()
            {
                object[] a = new object[ARRAY_SIZE];
                for (int i = 0; i < ARRAY_SIZE; i++)
                    a[i] = s[i];
                return a;
            }
        }

        class DataContainer
        {
            public bool IsAnnual;

            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.DeliveryCost;

            public DataContainer()
            {
                FedoList = new List<FedoContainer>();
                S = new Sum();
            }

            public List<FedoContainer> FedoList;
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                foreach (var x in FedoList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void LoadData(int periodYear, int? periodMonth)
            {
                IsAnnual = periodMonth == null;

                List<FEDO> Fedos;
                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportDeliveryCost> ReportData;

                Fedos = DataContainerFacade.GetList<FEDO>().OrderBy(x => x.ID).ToList();
                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = periodYear });
                    if (!IsAnnual)
                        pL.Add(new ListPropertyCondition { Name = "PeriodMonth", Value = periodMonth });
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "BranchReportID", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportDeliveryCost>(pL).ToList();
                }

                LoadData(Fedos, Branches, Reports, ReportData);
            }

            internal void LoadData(List<FEDO> fedos, List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportDeliveryCost> reportData)
            {
                Dictionary<long, FedoContainer> fD = new Dictionary<long, FedoContainer>();

                foreach (var x in fedos.OrderBy(x => x.ID))
                {
                    var y = new FedoContainer { Fedo = x };
                    FedoList.Add(y);
                    fD.Add(x.ID, y);
                }

                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x, IsAnnual = IsAnnual };
                    fD[x.FEDO_ID].AddBranch(y);
                    bD.Add(x.ID, y);
                }

                Dictionary<long, ReportItemContainer> rD = new Dictionary<long, ReportItemContainer>();

                foreach (var x in from y in reports orderby y.PeriodYear, y.PeriodMonth select y)
                {
                    var y = new ReportItemContainer { Report = x };
                    bD[x.BranchID].AddItem(y);
                    rD.Add(x.ID, y);
                }

                foreach (var x in reportData)
                    rD[x.BranchReportID].Data = x;

                CalcSum();
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);


                //return;
                foreach (var x in FedoList)
                    x.Print(ph);

                ph.Print(this);
            }

            private int GetRowCount()
            {
                int n = 1;
                foreach (var x in FedoList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (FedoList == null || FedoList.Count == 0)
                    return true;

                foreach (var x in FedoList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in FedoList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }
        }

        class FedoContainer
        {
            public FedoContainer()
            {
                BranchList = new List<BranchContainer>();
                S = new Sum();
            }

            public FEDO Fedo { get; set; }
            public List<BranchContainer> BranchList { get; set; }
            public Sum S;


            public void CalcSum()
            {
                S.Clear();

                foreach (var x in BranchList)
                {
                    x.CalcSum();
                    S.Add(x.S);
                }
            }

            internal void AddBranch(BranchContainer y)
            {
                BranchList.Add(y);
            }

            internal void Print(PrintHelper ph)
            {
                ph.Print(this);
                foreach (var x in BranchList)
                    x.Print(ph);
            }

            internal int GetRowCount()
            {
                return 1 + BranchList.Count;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        res.Add(x.Branch);

                return res;
            }
        }

        class BranchContainer
        {
            public bool IsAnnual;

            public BranchContainer()
            {
                ReportList = new List<ReportItemContainer>();
                S = new Sum();
            }

            public PFRBranch Branch { get; set; }
            public List<ReportItemContainer> ReportList { get; set; }
            public Sum S;

            public void CalcSum()
            {
                S.Clear();

                if (ReportList == null)
                    return;

                foreach (var x in ReportList)
                {
                    S.Sum_221 += x.Data.Sum_221;
                    S.Sum_226 += x.Data.Sum_226;
                    S.Sum_Total += x.Data.Total;
                }
            }

            internal void AddItem(ReportItemContainer r)
            {
                ReportList.Add(r);
            }

            internal void Print(PrintHelper ph)
            {
                ph.PrintBranch(this);
            }

            internal bool IsDataIncomplete()
            {
                if (ReportList == null || ReportList.Count == 0)
                    return true;

                if (ReportList.Count != (IsAnnual ? 12 : 1))
                    return true;

                foreach (var x in ReportList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }
        }

        class ReportItemContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportDeliveryCost Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;

                return false;
            }
        }
    }
}
