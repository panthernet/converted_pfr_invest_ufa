﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PrintBase : PrintToExcel
    {
        protected static string GetReportDateWordsPrepositional(int year, int? month)
        {
            if (month == null)
            {
                DateTime d = new DateTime(year, 12, 1);
                return string.Format("{0} году", d.Year);
            }

            {
                DateTime d = new DateTime(year, month.Value, 1);
                return string.Format("{0} {1} года", DateTools.MonthesPrepositional[d.Month - 1], d.Year);
            }
        }

        public static string GetReportDateWords(int year, int? month)
        {
            if (month == null)
            {
                DateTime d = new DateTime(year, 12, 1).AddMonths(1);
                return string.Format("1 {0} {1} года", DateTools.MonthesGenitive[d.Month - 1], d.Year);
            }

            {
                DateTime d = new DateTime(year, month.Value, 1).AddMonths(1);
                return string.Format("1 {0} {1} года", DateTools.MonthesGenitive[d.Month - 1], d.Year);
            }
        }

        internal void PrintResponsiblePerformer(Person Responsible, Person Performer)
        {
            Fields.Add("RESP_POSITION", Responsible == null ? null : Responsible.Position);
            Fields.Add("RESP_NAME", Responsible == null ? null : Responsible.FIOShort);

            Fields.Add("PERF_NAME", Performer == null ? null : Performer.FullName);
            Fields.Add("PERF_PHONE", Performer == null ? null : Performer.Phone);
        }

        protected override void GenerateDocument(Application app)
        {
            base.GenerateDocument(app);

            app.ScreenUpdating = false;
            app.Calculation = XlCalculation.xlCalculationManual;
            Worksheet sheet = (Worksheet)Workbook.Sheets[1];
            GenerateDocument(sheet);
            app.Calculation = XlCalculation.xlCalculationManual;
            app.ScreenUpdating = true;
        }

        public virtual void GenerateDocument(Worksheet s)
        {
            throw new NotImplementedException();
        }

        protected override void FillData()
        {
            throw new NotImplementedException();
        }

        public string GetDataIncompleteMessage(List<PFRBranch> xL)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Отсутствуют данные за отчетный период по следующим регионам:");

            for (int i = 0; i < 3 && i < xL.Count; i++)
                sb.AppendLine(xL[i].Name);

            if (xL.Count > 3)
                sb.AppendLine("и др.");

            return sb.ToString();
        }
    }
}
