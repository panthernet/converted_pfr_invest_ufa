﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_NpfNotice : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.NpfNotice;

        private const string MASK = "SRP_02_*.xls;SRP_02_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return MASK;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_02_(.xls, .xlsx)|{0}", MASK);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            var x = new PfrBranchReportImportContent_NpfNotice
            {
                PfrBranchReport = r,
                Items = new List<PfrBranchReportNpfNotice>()
            };

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }
            return false;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase paramter, out bool isloadFileCorrect)
        {
            var x = (PfrBranchReportImportContent_NpfNotice)paramter;


            var d = new DataFeeder(worksheet);

            PfrBranchReportNpfNotice r;
            while ((r = d.GetNpfRow()) != null)
                x.Add(r);

            isloadFileCorrect = true;
        }


        private class DataFeeder
        {
            public Worksheet S;
            public int RowNumber;

            private Dictionary<string, long> NpfIndex;

            public DataFeeder(Worksheet s)
            {
                S = s;
                RowNumber = 17;

                LoadNpfIndex();
            }

            private void LoadNpfIndex()
            {
                NpfIndex = new Dictionary<string, long>();

                var xL = BLServiceSystem.Client.GetNPFListLEReorgHib().ToList();

                foreach (var x in xL)
                {
                    if (x.FormalizedName == null)
                        continue;

                    string s = x.FormalizedName.ToLower().Trim();

                    if (!NpfIndex.ContainsKey(s))
                        NpfIndex.Add(s, x.ID);
                }
            }


            internal PfrBranchReportNpfNotice GetNpfRow()
            {
                Array a = GetRowValue();

                if (IsEmpty(a))
                {
                    a = GetRowValue();
                    if (IsEmpty(a))//Two empty lines, import done
                    {
                        return null;
                    }
                }

                try
                {
                    var res = new PfrBranchReportNpfNotice
                    {
                        LegalEntityID = GetNpfID(a.GetValue(1, 3).ToString()),
                        NoticeTotal = GetLong(a.GetValue(1, 4)),
                        ContractTotal = GetLong(a.GetValue(1, 5)),
                        NoticeNew = GetLong(a.GetValue(1, 6)),
                        ContractNew = GetLong(a.GetValue(1, 7)),
                        NoticeExisting = GetLong(a.GetValue(1, 8)),
                        ContractExisting = GetLong(a.GetValue(1, 9)),
                        NoticeToNpf = GetLong(a.GetValue(1, 10)),
                        ContractToNpfReturned = GetLong(a.GetValue(1, 11)),
                        ContractPtkSpu = GetLong(a.GetValue(1, 12))
                    };
                    return res;
                }
                catch (PfrBranchReportImportException)
                { throw; }
                catch
                {
                    throw new PfrBranchReportImportException("Ошибка чтения значений");
                }
            }

            private long GetLong(object x)
            {
                if (x == null)
                    throw new PfrBranchReportImportException("Ошибка: вместо числового значение пустое значение");

                return Convert.ToInt64(x);
            }

            private Array GetRowValue()
            {
                Range r = GetRow();
                Array a = (Array)r.Value2;
                return a;
            }

            private bool IsEmpty(Array a)
            {
                string s = Convert.ToString(a.GetValue(1, 3));

                return string.IsNullOrEmpty(s);
            }

            private Range GetRow()
            {
                var r = S.Range["A" + RowNumber, "L" + RowNumber];
                RowNumber++;
                return r;
            }

            private long GetNpfID(string name)
            {
                if (string.IsNullOrEmpty(name))
                    throw new PfrBranchReportImportException("Некорректное значение наименования НПФ");

                name = name.Trim().ToLower();

                if (NpfIndex.ContainsKey(name))
                    return NpfIndex[name];


                throw new PfrBranchReportImportException(string.Format("Наименование НПФ '{0}' отсутствует в справочнике НПФ", name));
            }

        }
    }
}
