﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
	public abstract class PfrBranchReportImportXmlHandlerBase
    {
        public abstract PfrBranchReportType.BranchReportType ReportType { get; }
        private ObservableList<PfrBranchReportImportContentBase> _Items;
        public ObservableList<PfrBranchReportImportContentBase> Items => _Items ?? (_Items = new ObservableList<PfrBranchReportImportContentBase>());
        public virtual bool IsMonthVisible => !IsAnnual;
        private readonly List<PFRBranch> _branches;

        protected PfrBranchReportImportXmlHandlerBase()
		{
			// Загружаем список регионов
			_branches = DataContainerFacade.GetList<PFRBranch>();
		}

		//private ObservableList<PfrBranchReport> Reports = new ObservableList<PfrBranchReport>();

        public virtual bool CanExecuteOpenFile()
        {
            return false;
        }

		/// <summary>
		/// Маска для выбора файлов из каталога
		/// </summary>
		/// <returns></returns>
		public string GetOpenDirectoryMask()
		{
			return "*.xml";
		}

		/// <summary>
		/// Маска для выбора файла
		/// </summary>
		/// <returns></returns>
		public string GetOpenFileMask()
		{
			return "Файлы отчетов XML (*.xml)|*.xml|Все файлы (*.*)|*.*";
		}

        public abstract bool CanExecuteImport();

        //Записать загруженные данные в БД
        public virtual PfrBranchReportImportResponse DoImport()
        {
            var p = new PfrBranchReportImportRequest
            {
                ReportType = ReportType,
                Items = Items.ToArray(),
                IsForceUpdateDuplicate = false
            };

            var res = BLServiceSystem.Client.ImportPfrBranchReport(p);
            //
            if (Items != null && Items.Count > 0)
            {
                var reportInfo = DataContainerFacade.GetByID<PfrBranchReportType>((long?)ReportType);
                JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, "Импорт отчетов по регионам",
                    $"Выполнен импорт отчета \"{reportInfo.ShortName}\". Импортированы данные по регионам: {string.Join(",", Items.Select(item => item.RegionName).ToArray())}.");
            }

            return res;
        }

        public virtual PfrBranchReportImportResponse DoImportDuplicate(PfrBranchReportImportResponse x)
        {
            var xL = from z in Items where x.DuplicateItemKeys.Contains(z.ReportKey) select z;

            var p = new PfrBranchReportImportRequest
            {
                ReportType = ReportType,
                Items = xL.ToArray(),
                IsForceUpdateDuplicate = true
            };

            var res = BLServiceSystem.Client.ImportPfrBranchReport(p);
            return res;
        }


        public virtual bool IsAnnual => false;

        public virtual bool TryParseFileName(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
                return false;

            var s = Path.GetFileName(fileName);

            if (string.IsNullOrEmpty(s))
                return false;

			// TODO Узнать, нужно ли это
			//Match m;

			//if (IsAnnual)
			//	m = Regex.Match(s, @"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4}).xlsx?\z");
			//else
			//	m = Regex.Match(s, @"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4})-(?<month>\d{1,2}).xlsx?\z");//"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4})[_-](?<month>\d{1,2}).xlsx?\z"

			//if (!m.Success)
			//{
			//	r = null;
			//	return false;
			//}

			//try
			//{

			//	r = new PfrBranchReport()
			//	{
			//		//ReportTypeID = int.Parse(m.Groups["rType"].Value),
			//		//PeriodYear = int.Parse(m.Groups["year"].Value),
			//		//PeriodMonth = IsAnnual ? (int?)null : int.Parse(m.Groups["month"].Value),
			//		CreatedDate = DateTime.Now,
			//		FileName = s,
			//		//Branch = GetBranchByRegionNumber(int.Parse(m.Groups["region"].Value))
			//	};

			//	if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
			//		throw new InvalidDataException("Некорректный год");
			//	if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
			//		throw new InvalidDataException("Некорректный месяц");

			//	if (r.Branch == null)
			//		throw new InvalidDataException("Некорректный код региона");
			//	r.BranchID = r.Branch.ID;

			//	if (r.ReportType != ReportType)
			//		return false;
			//}
			//catch (Exception ex)
			//{
			//	if (ex is InvalidDataException)
			//		throw;
			//	r = null;
			//	return false;
			//}

            return true;
        }

		/// <summary>
		/// Получает объект с регионов по его названию
		/// </summary>
		/// <param name="branchName">Название региона</param>
		/// <returns></returns>
		protected PFRBranch GetBranchByName(string branchName)
		{
		    var name = branchName.ToLower();

			return _branches.FirstOrDefault(x => x.PassportRegionName != null && x.PassportRegionName.ToLower().Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).Contains(name));
		}

		//public static PFRBranch GetBranchByRegionNumber(int regionNumber)
		//{
		//	return DataContainerFacade.GetListByProperty<PFRBranch>("RegionNumber", regionNumber).FirstOrDefault();
		//}

		//public static long? GetBranchIDByRegionNumber(int regionNumber)
		//{
		//	PFRBranch x = GetBranchByRegionNumber(regionNumber);

		//	if (x == null)
		//		return null;
		//	return x.ID;
		//}

        //public abstract void LoadFile(string fileName);

        public delegate bool LoadAlredyLoadedFileHandler(PfrBranchReportImportContentBase parameter);

        public LoadAlredyLoadedFileHandler OnLoadAlredyLoadedFileHandler { set; get; }

        public delegate void LoadFileHandler(string fileName, XDocument document, out bool isLoadFilecorrect);

        /// <summary>
		/// Загрузка данных из файла в память
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="loadFileHandler"></param>
        /// <returns></returns>
        public virtual bool LoadFile(string fileName, LoadFileHandler loadFileHandler)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException($"Файл {fileName} не был найден.");
			////Данные по следующим отчетам уже импортированы ранее:
			//if (!isFileNeedToLoad(parameter))
			//	return false;

	        XDocument doc = null;

            try
            {
	            doc = XDocument.Load(fileName);

                bool isloadFileCorrect = false;

                loadFileHandler(fileName, doc, out isloadFileCorrect);

                return isloadFileCorrect;
            }
            finally
            {
	            doc = null;
            }
        }

        protected bool isFileNeedToLoad(PfrBranchReportImportContentBase fileParamInfo)
        {
            if (OnLoadAlredyLoadedFileHandler != null) {
                PfrBranchReportImportContentBase alreadyLoaded = Items.SingleOrDefault(item => item.Equals(fileParamInfo));
                if(Items != null && Items.Count > 0 && alreadyLoaded !=null)
                {
                    if (OnLoadAlredyLoadedFileHandler(alreadyLoaded)){
                        Items.Remove(alreadyLoaded);
                        return true;
                    }
                    return false;
                }
            }

            return true;
        }

        public virtual bool LoadFile(string fileName, out string message)
        {
            try
            {
                message = null;

                if (!TryParseFileName(fileName))
                {
                    message = $"Некорректное имя файла '{fileName}'";
                    return false;
                }

                return LoadFile(fileName);
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
                return false;
            }
            catch (PfrBranchReportImportException ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public virtual bool LoadFile(string fileName)
        {
            throw new NotImplementedException();
        }

    }
}
