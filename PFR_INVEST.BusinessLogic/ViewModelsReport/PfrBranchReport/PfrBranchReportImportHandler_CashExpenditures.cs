﻿using System;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_CashExpenditures : PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.CashExpenditures;

        private const string Mask = "SRP_06_*.xls;SRP_06_*.xlsx";

        public override string GetOpenDirectoryMask()
        {
            return Mask;
        }

        public override string GetOpenFileMask()
        {
            return string.Format("Отчет по региону SRP_06_(.xls, .xlsx)|{0}", Mask);
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            PfrBranchReport r;

            if (TryParseFileName(fileName, out r))
            {
                errorMessage = null;
                return true;
            }
            errorMessage = string.Format("Некорректное имя файла '{0}'", fileName);
            return false;
        }

        public override bool LoadFile(string fileName, PfrBranchReport r)
        {
            PfrBranchReportImportContent_CashExpenditures x = new PfrBranchReportImportContent_CashExpenditures();
            x.PfrBranchReport = r;

            if (LoadFile(fileName, LoadData, x))
            {
                Items.Add(x);
                return true;
            }
            return false;
        }

        public void LoadData(Worksheet worksheet, PfrBranchReportImportContentBase paramter, out bool isloadFileCorrect)
        {
            PfrBranchReportImportContent_CashExpenditures x = (PfrBranchReportImportContent_CashExpenditures)paramter;

            x.CashExpenditures = new PfrBranchReportCashExpenditures
            {
                Sum_KBK_392_10_06_505_23_01_244_226 = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[14, 2]).Value),
                Sum_KBK_392_10_06_505_54_02_244_226_by400 = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[14, 3]).Value),
                Sum_KBK_392_10_06_505_54_02_244_226_by472 = Convert.ToDecimal(((Range)worksheet.UsedRange.Cells[14, 4]).Value)
            };
            isloadFileCorrect = true;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }


        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }


        public override bool IsAnnual => true;
    }
}
