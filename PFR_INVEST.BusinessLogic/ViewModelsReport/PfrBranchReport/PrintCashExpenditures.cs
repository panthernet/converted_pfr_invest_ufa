﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintCashExpenditures : PrintBase
    {
        public int PeriodYear { get; set; }
        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        DataContainer Data;


        public PrintCashExpenditures()
        {
            Name = "Отчет о кассовых расходах";
            TemplateName =  Name + ".xlsx";
        }

        protected override void FillData()
        {
            Fields.Add("PERIOD_YEAR", PeriodYear.ToString());
            PrintResponsiblePerformer(Responsible, Performer);
        }

        public override void GenerateDocument(Worksheet s)
        {
            PrintHelper ph = new PrintHelper(s);
            Data.Print(ph);
        }

        public void LoadData()
        {
            Data = new DataContainer();
            Data.LoadData(PeriodYear);
        }

        internal bool IsReportDataIncomplete()
        {
            return Data.IsDataIncomplete();
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = Data.GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        

        class PrintHelper
        {
            public PrintHelper()
            { }

            public PrintHelper(Worksheet s)
                : this()
            {
                S = s;
                ItemCount = 0;
                RowNumber = 9;
            }

            public Worksheet S;
            public int RowNumber;
            public int ItemCount;

            internal void AddEmptyRows(int count)
            {
                var r = S.get_Range("A" + RowNumber, "E" + (RowNumber - 1 + count));
                r.Insert(XlInsertShiftDirection.xlShiftDown, Missing.Value);
            }

            private Range AddRow()
            {
                Range r = S.get_Range("A" + RowNumber, "E" + RowNumber);
                RowNumber++;
                return r;
            }


            internal void Print(DataContainer x)
            {
                object[] a = new object[5];
                a[0] = "Всего по России:";
                a[1] = null;
                a[2] = x.Sum_226;
                a[3] = x.Sum_400;
                a[4] = x.Sum_472;

                Range r = AddRow();
                r.Value2 = a;

                r.Style = "PfrTotal";
            }

            internal void PrintFedo(FedoContainer x)
            {
                if (x.Fedo.Special == 1)
                {
                    Range r = AddRow();
                    r.Merge();
                    r.Style = "PfrSplitter";
                }
                else
                {
                    object[] a = new object[5];
                    a[0] = x.Fedo.Name;
                    a[2] = x.Sum_226;
                    a[3] = x.Sum_400;
                    a[4] = x.Sum_472;

                    Range r = AddRow();
                    r.Value2 = a;

                    r.Style = "PfrFedo";

                    r.get_Range("A3", "A5").Style = "PfrFedoTotal";
                }
            }

            internal void PrintBranch(PFRBranch Branch, decimal Sum_226, decimal Sum_400, decimal Sum_472)
            {
                ItemCount++;

                object[] a = new object[5];
                a[0] = ItemCount;
                a[1] = Branch.Name;
                a[2] = Sum_226;
                a[3] = Sum_400;
                a[4] = Sum_472;

                Range r = AddRow();
                r.Value2 = a;
                r.Style = "PfrBranch";
            }

            
        }


        class DataContainer
        {
            public PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.CashExpenditures;

            public DataContainer()
            {
                FedoList = new List<FedoContainer>();
            }

            public List<FedoContainer> FedoList;


            public decimal Sum_226 { get; set; }
            public decimal Sum_400 { get; set; }
            public decimal Sum_472 { get; set; }

            public void CalcSum()
            {
                Sum_226 = 0;
                Sum_400 = 0;
                Sum_472 = 0;

                foreach (var x in FedoList)
                {
                    x.CalcSum();
                    Sum_226 += x.Sum_226;
                    Sum_400 += x.Sum_400;
                    Sum_472 += x.Sum_472;
                }
            }

            internal void LoadData(int periodYear)
            {
                List<FEDO> Fedos;
                List<PFRBranch> Branches;
                List<PfrBranchReport> Reports;
                List<PfrBranchReportCashExpenditures> ReportData;

                Fedos = DataContainerFacade.GetList<FEDO>().OrderBy(x => x.ID).ToList();
                Branches = DataContainerFacade.GetList<PFRBranch>().ToList();

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "ReportTypeID", Value = (long)ReportType });
                    pL.Add(new ListPropertyCondition { Name = "PeriodYear", Value = periodYear });
                    pL.Add(new ListPropertyCondition { Name = "StatusID", Value = 1L });

                    Reports = DataContainerFacade.GetListByPropertyConditions<PfrBranchReport>(pL).ToList();
                }

                {
                    var pL = new List<ListPropertyCondition>();
                    pL.Add(new ListPropertyCondition { Name = "BranchReportID", Values = (from x in Reports select x.ID).Cast<object>().ToArray(), Operation = "in" });
                    ReportData = DataContainerFacade.GetListByPropertyConditions<PfrBranchReportCashExpenditures>(pL).ToList();
                }

                LoadData(Fedos, Branches, Reports, ReportData);
            }

            internal void LoadData(List<FEDO> fedos, List<PFRBranch> branches, List<PfrBranchReport> reports, List<PfrBranchReportCashExpenditures> reportData)
            {
                Dictionary<long, FedoContainer> fD = new Dictionary<long, FedoContainer>();

                foreach (var x in fedos.OrderBy(x => x.ID))
                {
                    var y = new FedoContainer { Fedo = x };
                    FedoList.Add(y);
                    fD.Add(x.ID, y);
                }

                Dictionary<long, BranchContainer> bD = new Dictionary<long, BranchContainer>();

                foreach (var x in from y in branches orderby y.RegionNumber, y.Name select y)
                {
                    var y = new BranchContainer { Branch = x };
                    fD[x.FEDO_ID].AddBranch(y);
                    bD.Add(x.ID, y);
                }

                Dictionary<long, ReportItemContainer> rD = new Dictionary<long, ReportItemContainer>();

                foreach (var x in from y in reports orderby y.PeriodYear select y)
                {
                    var y = new ReportItemContainer { Report = x };

                    if (x.PeriodMonth == null)// Here: All imported reports are annual, skip all with month specified
                        bD[x.BranchID].AddItem(y);
                    rD.Add(x.ID, y);
                }

                foreach (var x in reportData)
                    rD[x.BranchReportID].Data = x;

                CalcSum();
            }

            internal void Print(PrintHelper ph)
            {
                int n = GetRowCount();
                ph.AddEmptyRows(n);


                ph.Print(this);

                foreach (var x in FedoList)
                    x.Print(ph);
            }

            private int GetRowCount()
            {
                int n = 1;
                foreach (var x in FedoList)
                    n += x.GetRowCount();

                return n;
            }

            internal bool IsDataIncomplete()
            {
                if (FedoList == null || FedoList.Count == 0)
                    return true;

                foreach (var x in FedoList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }


            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in FedoList)
                    res.AddRange(x.GetDataIncompleteRegions());

                return res;
            }
        }

        class FedoContainer
        {
            public FedoContainer()
            {
                BranchList = new List<BranchContainer>();
            }

            public FEDO Fedo { get; set; }
            public List<BranchContainer> BranchList { get; set; }

            public decimal Sum_226 { get; set; }
            public decimal Sum_400 { get; set; }
            public decimal Sum_472 { get; set; }

            public void CalcSum()
            {
                Sum_226 = 0;
                Sum_400 = 0;
                Sum_472 = 0;

                foreach (var x in BranchList)
                {
                    x.CalcSum();
                    Sum_226 += x.Sum_226;
                    Sum_400 += x.Sum_400;
                    Sum_472 += x.Sum_472;
                }
            }

            internal void AddBranch(BranchContainer y)
            {
                BranchList.Add(y);
            }

            internal void Print(PrintHelper ph)
            {
                ph.PrintFedo(this);
                foreach (var x in BranchList)
                    x.Print(ph);
            }

            internal int GetRowCount()
            {
                return 1 + BranchList.Count;
            }

            internal bool IsDataIncomplete()
            {
                if (BranchList == null || BranchList.Count == 0)
                    return true;

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }

            internal List<PFRBranch> GetDataIncompleteRegions()
            {
                List<PFRBranch> res = new List<PFRBranch>();

                foreach (var x in BranchList)
                    if (x.IsDataIncomplete())
                        res.Add(x.Branch);

                return res;
            }
        }

        class BranchContainer
        {
            public BranchContainer()
            {
                ReportList = new List<ReportItemContainer>();
            }

            public PFRBranch Branch { get; set; }
            public List<ReportItemContainer> ReportList { get; set; }

            public decimal Sum_226 { get; set; }
            public decimal Sum_400 { get; set; }
            public decimal Sum_472 { get; set; }

            public void CalcSum()
            {
                Sum_226 = 0;
                Sum_400 = 0;
                Sum_472 = 0;

                if (ReportList == null)
                    return;

                foreach (var x in ReportList)
                {
                    Sum_226 += x.Data.Sum_KBK_392_10_06_505_23_01_244_226;
                    Sum_400 += x.Data.Sum_KBK_392_10_06_505_54_02_244_226_by400;
                    Sum_472 += x.Data.Sum_KBK_392_10_06_505_54_02_244_226_by472;
                }
            }

            internal void AddItem(ReportItemContainer r)
            {
                ReportList.Add(r);
            }

            internal void Print(PrintHelper ph)
            {
                ph.PrintBranch(Branch, Sum_226, Sum_400, Sum_472);
            }

            internal bool IsDataIncomplete()
            {
                if (ReportList == null || ReportList.Count != 1)
                    return true;

                foreach (var x in ReportList)
                    if (x.IsDataIncomplete())
                        return true;

                return false;
            }
        }

        class ReportItemContainer
        {
            public PfrBranchReport Report;
            public PfrBranchReportCashExpenditures Data;

            internal bool IsDataIncomplete()
            {
                if (Report == null || Data == null)
                    return true;

                return false;
            }
        }



    }
}
