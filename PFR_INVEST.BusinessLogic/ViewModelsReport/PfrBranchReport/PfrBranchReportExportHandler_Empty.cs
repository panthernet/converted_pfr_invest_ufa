﻿using System;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportExportHandler_Empty : PfrBranchReportExportHandlerBase
    {
        public PfrBranchReportExportHandler_Empty()
        {
            // TODO: Complete member initialization
        }

        public PfrBranchReportExportHandler_Empty(RegionReportExportDlgViewModel model)
            : this()
        {
        }

        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.Empty;

        public override bool CanExecuteExport()
        {
            return false;
        }

        public override void ExecuteExport()
        {
            throw new NotImplementedException();
        }

        public override bool IsYearEnabled => false;

        public override bool IsMonthEnabled => false;

        public override bool IsResponsibleEnabled => false;

        public override bool IsPerformerEnabled => false;
    }
}
