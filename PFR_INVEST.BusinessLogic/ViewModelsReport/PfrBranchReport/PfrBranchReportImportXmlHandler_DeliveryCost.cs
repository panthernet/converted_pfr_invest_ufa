﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportXmlHandler_DeliveryCost : PfrBranchReportImportXmlHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.XmlDeliveryCost;

        public override bool CanExecuteImport()
        {
            return Items.Count > 0;
        }

        public override bool CanExecuteOpenFile()
        {
            return true;
        }

        public override bool LoadFile(string fileName, out string message)
        {
            if (!TryParseFileName(fileName))
            {
                message = string.Format("Некорректное имя файла '{0}'", fileName);
                return false;
            }

            bool isFileLoadedCorrect = LoadFile(fileName);

            message = null;

            return isFileLoadedCorrect;
        }

        public override bool LoadFile(string fileName)
        {
            return LoadFile(fileName, PopulateData);
        }

        private void PopulateData(string fileName, XDocument document, out bool isloadFileCorrect)
        {
            var start = DateTime.Now;

            try
            {
                var convert = document.Element("Convert");
                if (convert != null)
                {
                    var templateName = convert.Element("TemplateName");
                    if (templateName != null)
                    {
                        var templateNameValue = templateName.Value;
                        var nameParts = templateNameValue.Split(new[] {"!!"},
                            StringSplitOptions.RemoveEmptyEntries);
                        templateNameValue = nameParts[2];

                        if (!templateNameValue.ToLower().Contains("доставка ев"))
                        {
                            throw new PfrBranchReportImportException(
                                string.Format("Выбранный тип отчета не соответствует типу отчета из xml-файла: {0}",
                                    templateNameValue));
                        }
                    }

                    // Разделитель для дробных чисел
                    var decimalSeparator = convert.Element("DecimalSymbol").Value.Trim();
                    var numberFormatInfo = new NumberFormatInfo {NumberDecimalSeparator = decimalSeparator};

                    var rows = convert.Descendants("Sheet").First().Descendants("row");

                    // Дата отчета
                    var reportMonth = 0;
                    var reportYear = 0;
                    var startRowId = 12;
                    var lastRowId = rows.Last().Attribute("id").Value.To<int>();
                    var parsedData = new Dictionary<string, PfrBranchReportImportContent_DeliveryCost>();
                    var currentRegionName = "";

                    // Список федеральных округов из XML-файла
                    var fedoList = new[]
                    {
                        "Северо-Кавказский ФО",
                        "Крымский ФО",
                        "Центральный ФО",
                        "Северо-Западный ФО",
                        "Южный ФО",
                        "Приволжский ФО",
                        "Уральский ФО",
                        "Сибирский ФО",
                        "Дальневосточный ФО",
                        "Прямое подчинение"
                    };
                    foreach (var row in rows)
                    {
                        var rowId = row.Attribute("id").Value.To<int>();
                        var columns = row.Descendants("column");

                        // Получение месяца отчета
                        if (rowId == 1)
                        {
                            var monthColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "6");
                            if (monthColumn != null)
                            {
                                var month = DateTools.Months.First(x => x.ToLower() == monthColumn.Value.ToLower());
                                reportMonth = DateTools.Months.ToList().IndexOf(month) + 1;
                            }
                        }
                        // Получение года отчета
                        else if (rowId == 2)
                        {
                            var yearColumn = columns.FirstOrDefault(x => x.Attribute("id").Value == "6");
                            if (yearColumn != null)
                            {
                                reportYear = yearColumn.Value.To(0);
                            }
                        }
                        else if (rowId >= startRowId && rowId <= lastRowId)
                        {
                            // Парсим данные
                            // Наличие региона определяется наличием названия региона в колонке с id=2, а также отсутствие в списке федеральных округов
                            // Северо-Кавказский ФО, Крымский ФО, Центральный ФО, Северо-Западный ФО, Южный ФО, Приволжский ФО, Уральский ФО, Сибирский ФО, Дальневосточный ФО, Прямое подчинение
                            // Судя по списку округов, округом является строка, заканчивающаяся на " ФО" или же равная "Прямое подчинение"
                            // TODO при изменении списка округов в импортиуемом файле, нужно скорректировать этот список (fedoList)
                            var columnRegionName = columns.FirstOrDefault(x => x.Attribute("id").Value == "2");
                            if (columnRegionName != null)
                            {
                                var regionName = columnRegionName.Value.Trim();
                                if (!regionName.IsEmpty() && !regionName.ToLower().EndsWith(" фо") &&
                                    fedoList.All(x => x.ToLower() != regionName.ToLower()))
                                {
                                    // Создаем объект отчета
                                    var r = new PfrBranchReport
                                    {
                                        CreatedDate = DateTime.Now,
                                        Branch = GetBranchByName(regionName),
                                        ReportTypeID = (long) ReportType,
                                        FileName = Path.GetFileName(fileName),
                                        PeriodMonth = reportMonth,
                                        PeriodYear = reportYear,
                                        StatusID = 1
                                    };
                                    if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
                                        throw new InvalidDataException("Некорректный год");
                                    if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
                                        throw new InvalidDataException("Некорректный месяц");

                                    if (r.Branch == null)
                                        throw new InvalidDataException("Некорректное название региона: " + regionName);
                                    r.BranchID = r.Branch.ID;

                                    var report = new PfrBranchReportImportContent_DeliveryCost
                                    {

                                        PfrBranchReport = r,
                                        DeliveryCost = new PfrBranchReportDeliveryCost()
                                    };

                                    currentRegionName = regionName;
                                    parsedData.Add(currentRegionName, report);

                                    // Парсим строку с данными, опираясь на то
                                    var reportRow = parsedData[currentRegionName].DeliveryCost;
                                    foreach (var xColumn in columns.Where(x => x.Attribute("id").Value.To<int>() >= 3))
                                    {
                                        var strValue = xColumn.Value;
                                        decimal decValue = 0m;
                                        if (!strValue.IsEmpty())
                                        {
                                            decimal.TryParse(strValue, NumberStyles.Number, numberFormatInfo,
                                                out decValue);
                                        }

                                        switch (xColumn.Attribute("id").Value)
                                        {
                                            case "3":
                                                // Сумма расходов по доставке КОСГУ 221 (руб., коп.) 
                                                reportRow.Sum_221 = decValue;
                                                break;

                                            case "4":
                                                // Сумма расходов по доставке КОСГУ 226 (руб., коп.) 
                                                reportRow.Sum_226 = decValue;
                                                break;

                                            case "5":
                                                // ИТОГО
                                                reportRow.Total = decValue;
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach (var item in parsedData)
                    {
                        Items.Add(item.Value);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
                // Debugger.Break();
            }

            var delta = DateTime.Now - start;
            Debug.WriteLine("Parse DeliveryCost Report Duration = " + delta);

            isloadFileCorrect = true;
        }
    }
}
