﻿using System;
using PFR_INVEST.BusinessLogic.ViewModelsDialog;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportFactory
    {
        public static PfrBranchReportImportHandlerBase GetImportHandler(RegionReportImportDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case PfrBranchReportType.BranchReportType.Empty:
                    return new PfrBranchReportImportHandler_Empty();
                case PfrBranchReportType.BranchReportType.CashExpenditures:
                    return new PfrBranchReportImportHandler_CashExpenditures();                               
                case PfrBranchReportType.BranchReportType.InsuredPerson:
                    return new PfrBranchReportImportHandler_InsuredPerson();
                case PfrBranchReportType.BranchReportType.Applications741:
                    return new PfrBranchReportImportHandler_Application741();
                case PfrBranchReportType.BranchReportType.AssigneePayment:
                    return new PfrBranchReportImportHandler_AssigneePayment();
                case PfrBranchReportType.BranchReportType.CertificationAgreement:
                    return new PfrBranchReportImportHandler_CertificationAgreement();
                case PfrBranchReportType.BranchReportType.DeliveryCost:
                    return new PfrBranchReportImportHandler_DeliveryCost();
                case PfrBranchReportType.BranchReportType.NpfNotice:
                    return new PfrBranchReportImportHandler_NpfNotice();
                case PfrBranchReportType.BranchReportType.SignatureContract:
                    return new PfrBranchReportImportHandler_SignatureContract();


                default: throw new NotImplementedException();
            }
        }
    }
}
