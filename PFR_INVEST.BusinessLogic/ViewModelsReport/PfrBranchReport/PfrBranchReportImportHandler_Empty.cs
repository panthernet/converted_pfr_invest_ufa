﻿using System;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public class PfrBranchReportImportHandler_Empty: PfrBranchReportImportHandlerBase
    {
        public override PfrBranchReportType.BranchReportType ReportType => PfrBranchReportType.BranchReportType.Empty;

        public override bool CanExecuteImport()
        {
            return false;
        }

        public override PfrBranchReportImportResponse DoImport()
        {
            throw new NotImplementedException();
        }

        public override string GetOpenDirectoryMask()
        {
            throw new NotImplementedException();
        }

        public override string GetOpenFileMask()
        {
            throw new NotImplementedException();
        }

        public override bool IsValidFileName(string fileName, out string errorMessage)
        {
            throw new NotImplementedException();
        }
    }
}
