﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.Journal;
using PFR_INVEST.BusinessLogic.Misc;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.BranchReport;
using PFR_INVEST.DataObjects.Journal;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    public abstract class PfrBranchReportImportHandlerBase
    {
        public abstract PfrBranchReportType.BranchReportType ReportType { get; }

        private ObservableList<PfrBranchReportImportContentBase> _Items;

        public ObservableList<PfrBranchReportImportContentBase> Items
        {
            get
            {
                if (_Items == null)
                    _Items = new ObservableList<PfrBranchReportImportContentBase>();

                return _Items;
            }
        }

        public virtual bool CanExecuteOpenFile()
        {
            return false;
        }

        public abstract string GetOpenDirectoryMask(); //Маска для выбора файлов из каталога

        public abstract string GetOpenFileMask(); //Маска для выбора файла


        [Obsolete]
        public abstract bool IsValidFileName(string fileName, out string errorMessage);


        public abstract bool CanExecuteImport();

        //Записать загруженные данные в БД
        public virtual PfrBranchReportImportResponse DoImport()
        {
            var p = new PfrBranchReportImportRequest
            {
                ReportType = ReportType,
                Items = Items.ToArray(),
                IsForceUpdateDuplicate = false
            };

            var res = BLServiceSystem.Client.ImportPfrBranchReport(p);
            //
            if (Items != null && Items.Count > 0)
            {
                var reportInfo = DataContainerFacade.GetByID<PfrBranchReportType>((long?)ReportType);
                JournalLogger.LogEvent("Импорт данных", JournalEventType.IMPORT_DATA, null, "Импорт отчетов по регионам",
                    string.Format("Выполнен импорт отчета \"{0}\". Импортированы данные по регионам: {1}.", reportInfo.ShortName, String.Join(",", Items.Select(item => item.RegionName).ToArray())));
            }

            return res;
        }

        public virtual PfrBranchReportImportResponse DoImportDuplicate(PfrBranchReportImportResponse x)
        {
            var xL = from z in Items where x.DuplicateItemKeys.Contains(z.ReportKey) select z;

            var p = new PfrBranchReportImportRequest
            {
                ReportType = ReportType,
                Items = xL.ToArray(),
                IsForceUpdateDuplicate = true
            };

            var res = BLServiceSystem.Client.ImportPfrBranchReport(p);
            return res;
        }


        public virtual bool IsAnnual => false;

        public virtual bool TryParseFileName(string fileName, out PfrBranchReport r)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                r = null;
                return false;
            }

            var s = Path.GetFileName(fileName);

            if (string.IsNullOrEmpty(s))
            {
                r = null;
                return false;
            }


            Match m;

            if (IsAnnual)
                m = Regex.Match(s, @"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4}).xlsx?\z");
            else
                m = Regex.Match(s, @"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4})-(?<month>\d{1,2}).xlsx?\z");//"\ASRP_(?<rType>\d{1,2})_(?<region>\d{1,3})_(?<year>\d{4})[_-](?<month>\d{1,2}).xlsx?\z"

            if (!m.Success)
            {
                r = null;
                return false;
            }

            try
            {

                r = new PfrBranchReport
                {
                    ReportTypeID = int.Parse(m.Groups["rType"].Value),
                    PeriodYear = int.Parse(m.Groups["year"].Value),
                    PeriodMonth = IsAnnual ? (int?)null : int.Parse(m.Groups["month"].Value),
                    CreatedDate = DateTime.Now,
                    FileName = s,
                    Branch = GetBranchByRegionNumber(int.Parse(m.Groups["region"].Value))
                };

                if (r.PeriodYear < 1900 || r.PeriodYear > 9999)
                    throw new InvalidDataException("Некорректный год");
                if (r.PeriodMonth != null && (r.PeriodMonth > 12 || r.PeriodMonth < 1))
                    throw new InvalidDataException("Некорректный месяц");

                if (r.Branch == null)
                    throw new InvalidDataException("Некорректный код региона");
                r.BranchID = r.Branch.ID;

                if (r.ReportType != ReportType)
                    return false;
            }
            catch (Exception ex)
            {
                if (ex is InvalidDataException)
                    throw;
                r = null;
                return false;
            }
            return true;
        }

        public static PFRBranch GetBranchByRegionNumber(int regionNumber)
        {
            return DataContainerFacade.GetListByProperty<PFRBranch>("RegionNumber", regionNumber).FirstOrDefault();
        }

        public static long? GetBranchIDByRegionNumber(int regionNumber)
        {
            PFRBranch x = GetBranchByRegionNumber(regionNumber);

            if (x == null)
                return null;
            return x.ID;
        }

        //public abstract void LoadFile(string fileName);

        public delegate bool LoadAlredyLoadedFileHandler(PfrBranchReportImportContentBase parameter);

        public LoadAlredyLoadedFileHandler OnLoadAlredyLoadedFileHandler { set; get; }

        public delegate void LoadFileHandler(Worksheet worksheet, PfrBranchReportImportContentBase parameter, out bool isLoadFilecorrect);

        //Загрузка данных из файла в память
        public virtual bool LoadFile(string fileName, LoadFileHandler loadFileHandler, PfrBranchReportImportContentBase parameter)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(string.Format("Файл {0} не был найден.", fileName));
            //Данные по следующим отчетам уже импортированы ранее:
            if (!isFileNeedToLoad(parameter))
                return false;
     

            Application excelApp = null;
            Workbook workbook = null;
            Worksheet worksheet = null;

            try
            {
                excelApp = new Application { Visible = false };
                workbook = excelApp.Workbooks.Open(fileName);
                worksheet = (Worksheet)workbook.Sheets[1];
                bool isloadFileCorrect = false;
                loadFileHandler(worksheet, parameter, out isloadFileCorrect);
                return isloadFileCorrect;
            }            
            finally
            {
                if (excelApp != null)
                {
                    for (int i = 0; i < excelApp.Sheets.Count; i++)
                    {
                        Worksheet sheet = (Worksheet)excelApp.Sheets[i + 1];
                        ReleaseComObject(sheet);
                        sheet = null;
                    }

                    excelApp.Workbooks.Close();
                    for (int i = 0; i < excelApp.Workbooks.Count; i++)
                    {
                        Workbook w = excelApp.Workbooks[i + 1];
                        ReleaseComObject(w);
                        w = null;
                    }

                    if (workbook != null)
                    {
                        ReleaseComObject(workbook);
                        workbook = null;
                    }
                    excelApp.Quit();
                    ReleaseComObject(excelApp);
                    excelApp = null;
                }
            }
        }

        protected bool isFileNeedToLoad(PfrBranchReportImportContentBase fileParamInfo)
        {
            if (OnLoadAlredyLoadedFileHandler != null) {
                PfrBranchReportImportContentBase alreadyLoaded = Items.SingleOrDefault(item => item.Equals(fileParamInfo));
                if(Items != null && Items.Count > 0 && alreadyLoaded !=null)
                {
                    if (OnLoadAlredyLoadedFileHandler(alreadyLoaded)){
                        Items.Remove(alreadyLoaded);
                        return true;
                    }
                    return false;
                }
            }            
            return true;
        }

        private static void ReleaseComObject(object comObject)
        {
            int count = 0;
            while (count == 0)
            {
                count = Marshal.FinalReleaseComObject(comObject);
            }
        }

        public virtual bool LoadFile(string fileName, out string message)
        {
            try
            {
                message = null;
                PfrBranchReport r;

                if (!TryParseFileName(fileName, out r))
                {
                    message = string.Format("Некорректное имя файла '{0}'", fileName);
                    return false;
                }

                return LoadFile(fileName, r);
            }
            catch (InvalidDataException ex)
            {
                message = ex.Message;
                return false;
            }
            catch (PfrBranchReportImportException ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public virtual bool LoadFile(string fileName, PfrBranchReport r)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsMonthVisible => !IsAnnual;
    }
}
