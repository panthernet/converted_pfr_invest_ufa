﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.BranchReport
{
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OSRP_worker, DOKIP_ROLE_TYPE.OSRP_directory_editor, DOKIP_ROLE_TYPE.OSRP_manager)]
    public class PrintInsuredPerson : PrintBase
    {
        public int PeriodYear;
        public int? PeriodMonth;
        private readonly long _reportTypeId;
        List<PFRBranch> _branches = new List<PFRBranch>();
        Dictionary<long, List<PFRBranch>> _branchesDic = new Dictionary<long, List<PFRBranch>>();
        List<FEDO> _fedos = new List<FEDO>();
        List<PfrBranchReportInsuredPersonType> _insuredPersonTypes = new List<PfrBranchReportInsuredPersonType>();
        List<PfrBranchReportInsuredPerson> _reportsIP = new List<PfrBranchReportInsuredPerson>();

        List<PfrBranchReport> reports = new List<PfrBranchReport>();

        public Person Responsible { get; set; }
        public Person Performer { get; set; }


        public PrintInsuredPerson()
        {
            Name = "Отчет по принятым заявлениям";
            TemplateName = string.Concat(Name, ".xlsx");
            _reportTypeId = (long)PfrBranchReportType.BranchReportType.InsuredPerson;
        }

        public PrintInsuredPerson(int year, int? month)
            : this()
        {
            PeriodMonth = month;
            PeriodYear = year;
        }

        protected override void FillData()
        {
            Fields.Add("REPORT_DATE", GetReportDateWords(PeriodYear, PeriodMonth));
            Fields.Add("REPORT_YEAR", PeriodYear.ToString());

            PrintResponsiblePerformer(Responsible, Performer);
        }

        public override void GenerateDocument(Worksheet s)
        {
            CalculateByRF(_reportsIP, s, _insuredPersonTypes);

            ProcessFedoAndRegions(s);
        }

        private void ProcessFedoAndRegions(Worksheet sheet)
        {
            int rowIndexFrom = 28;
            int pastingRow = rowIndexFrom - 1;
            int batches = _fedos.Any(p => p.Special == 1) ? 3 : 2;
            for (int i = 0; i < (_branches.Count + _fedos.Count - batches) * 5; i++)
            {
                sheet.GetCellRange(rowIndexFrom, 1).EntireRow.Insert(XlInsertShiftDirection.xlShiftDown);
            }

            int branchNumber = 2;

            pastingRow = PopulateUsualFedos(sheet, pastingRow, ref branchNumber) + 1;

            PopulateSpecialFedo(sheet, branchNumber, pastingRow);
        }

        private void PopulateSpecialFedo(Worksheet s, int branchNumber, int rowIndex)
        {
            if (!_fedos.Any(item => item.Special == 1))
                return;

            var fedo = _fedos.First(p => p.Special == 1);

            List<PFRBranch> bD;
            if (!_branchesDic.ContainsKey(fedo.ID))//FEDO don't have branches
            {
                bD = new List<PFRBranch>();
            }
            else
                bD = _branchesDic[fedo.ID];

            var branchesCount = bD.Count;


            var rL = reports.Where(p => bD.Count(br => br.ID == p.BranchID) > 0);
            var rIP = _reportsIP.Where(p => rL.Count(r => r.ID == p.ReportId) > 0).ToList();

            CopyRegion(s);
            for (int j = 0; j < branchesCount; j++)
            {
                var branch = bD[j];
                PasteRegion(s, rowIndex);
                s.GetCellRange(rowIndex, 1).Value2 = branchNumber;
                s.GetCellRange(rowIndex, 2).Value2 = branch.Name;
                CalculateByFedo(
                    rIP.Where(p => rL.Count(r => r.BranchID == branch.ID && r.ID == p.ReportId) > 0).ToList(),
                    s, _insuredPersonTypes, rowIndex);
                branchNumber++;
                rowIndex += 5;
            }
        }

        private int PopulateUsualFedos(Worksheet s, int rowIndex, ref int branchNumber)
        {
            for (int i = 0; i < _fedos.Count(); i++)
            {
                var fedo = _fedos[i];

                List<PFRBranch> bD;
                if (!_branchesDic.ContainsKey(fedo.ID))//FEDO don't have branches
                {
                    bD = new List<PFRBranch>();
                }
                else
                    bD = _branchesDic[fedo.ID];

                var branchesCount = bD.Count;


                if (fedo.Special != 1)
                    if (i > 0)
                    {

                        CopyFedoHeader(s);
                        PasteFedoHeader(s, rowIndex);
                        var rs = reports.Where(item => bD.Count(br => br.ID == item.BranchID) > 0);
                        var rIP = _reportsIP.Where(item => rs.Count(r => r.ID == item.ReportId) > 0).ToList();
                        CalculateByFedo(rIP, s, _insuredPersonTypes, rowIndex);

                        s.GetCellRange(rowIndex, 1).Value2 = fedo.Name;
                        rowIndex += 5;
                        CopyRegion(s);

                        for (int j = 0; j < branchesCount; j++)
                        {
                            var branch = bD[j];
                            PasteRegion(s, rowIndex);
                            s.GetCellRange(rowIndex, 1).Value2 = branchNumber;
                            s.GetCellRange(rowIndex, 2).Value2 = branch.Name;
                            CalculateByFedo(
                                rIP.Where(p => rs.Count(r => r.BranchID == branch.ID && r.ID == p.ReportId) > 0).ToList(),
                                s, _insuredPersonTypes, rowIndex);
                            branchNumber++;
                            rowIndex += 5;
                        }
                    }
                    else
                    {
                        s.GetCellRange(rowIndex - 10, 1).Value2 = fedo.Name;
                        s.GetCellRange(rowIndex - 5, 1).Value2 = 1;
                        s.GetCellRange(rowIndex - 5, 2).Value2 = bD[0].Name;
                        var rs = reports.Where(p => bD.Count(br => br.ID == p.BranchID) > 0);
                        var rIP = _reportsIP.Where(p => rs.Count(r => r.ID == p.ReportId) > 0).ToList();

                        CalculateByFedo(rIP, s, _insuredPersonTypes, rowIndex - 10);

                        CalculateByFedo(
                            rIP.Where(item => rs.Count(r => r.BranchID == bD[0].ID && r.ID == item.ReportId) > 0).ToList(),
                            s, _insuredPersonTypes, rowIndex - 5);
                        CopyRegion(s);
                        for (int j = 0; j < branchesCount - 1; j++)
                        {
                            var branch = bD[j + 1];
                            PasteRegion(s, rowIndex);
                            s.GetCellRange(rowIndex, 1).Value2 = branchNumber;
                            s.GetCellRange(rowIndex, 2).Value2 = branch.Name;
                            CalculateByFedo(
                                rIP.Where(item => rs.Count(r => r.BranchID == branch.ID && r.ID == item.ReportId) > 0).ToList(),
                                s, _insuredPersonTypes, rowIndex);
                            branchNumber++;
                            rowIndex += 5;
                        }
                    }
            }
            return rowIndex;
        }

        private static void CopyRegion(Worksheet sheet)
        {
            sheet.Range[sheet.Cells[22, 1], sheet.Cells[26, 15]].Copy(Type.Missing);
        }

        private static void PasteRegion(Worksheet s, int rowIndex)
        {
            s.GetCellRange(rowIndex, 1).PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
        }

        private void PasteFedoHeader(Worksheet s, int rowIndex)
        {
            s.GetCellRange(rowIndex, 1).PasteSpecial(XlPasteType.xlPasteAll, XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
        }

        private static void CopyFedoHeader(Worksheet sheet)
        {
            sheet.Range[sheet.Cells[17, 1], sheet.Cells[21, 15]].Copy(Type.Missing);
        }

        private void CalculateByFedo(List<PfrBranchReportInsuredPerson> reports, Worksheet s, List<PfrBranchReportInsuredPersonType> insuredPersonTypes, int rowIndex)
        {
            for (int i = 0; i < insuredPersonTypes.Count; i++)
            {
                var type = insuredPersonTypes[i];
                long r = rowIndex + i;

                FillRow(s, r, reports, type.ID);
            }
        }

        private void CalculateByRF(List<PfrBranchReportInsuredPerson> reports, Worksheet s, List<PfrBranchReportInsuredPersonType> insuredPersonTypes)
        {
            for (int i = 0; i < insuredPersonTypes.Count; i++)
            {
                PfrBranchReportInsuredPersonType type = insuredPersonTypes[i];
                long r = 10;
                switch (type.ID)
                {
                    case 1:
                        r += 1;
                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        r += i + 2;
                        break;
                }

                FillRow(s, r, reports, type.ID);
            }
        }


        private void FillRow(Worksheet s, long r, List<PfrBranchReportInsuredPerson> reports, long typeID)
        {
            var rL = reports.Where(p => p.TypeId == typeID);

            s.SetValue(r, 4, rL.Sum(p => p.Summary));
            s.SetValue(r, 5, rL.Sum(p => p.TerritoryPersonalAppealSum));
            s.SetValue(r, 6, rL.Sum(p => p.TerritoryPersonalAppealGroup1));
            s.SetValue(r, 7, rL.Sum(p => p.TerritoryPersonalAppealGroup2));
            s.SetValue(r, 8, rL.Sum(p => p.TerritoryPersonalAppealGroup3));
            s.SetValue(r, 9, rL.Sum(p => p.Agency));
            s.SetValue(r, 10, rL.Sum(p => p.TerritoryOtherSum));
            s.SetValue(r, 11, rL.Sum(p => p.TerritoryOtherGroup1));
            s.SetValue(r, 12, rL.Sum(p => p.TerritoryOtherGroup2));
            s.SetValue(r, 13, rL.Sum(p => p.TerritoryOtherGroup3));
            s.SetValue(r, 14, rL.Sum(p => p.Gosuslugi));
            s.SetValue(r, 15, rL.Sum(p => p.Mfc));
        }


        internal void LoadData()
        {
            _branches = DataContainerFacade.GetList<PFRBranch>();
            _branchesDic = _branches.GroupBy(p => p.FEDO_ID).ToDictionary(p => p.Key, p => p.ToList());
            _fedos = DataContainerFacade.GetList<FEDO>().OrderBy(p => p.ID).ToList();
            _insuredPersonTypes = DataContainerFacade.GetList<PfrBranchReportInsuredPersonType>().OrderBy(p => p.ID).ToList();
            _reportsIP = BLServiceSystem.Client.GetPfrReportInsuredList(PeriodYear, PeriodMonth, _reportTypeId).ToList();
            reports = BLServiceSystem.Client.GetPfrBranchReportsByTypeAndPeriod(PeriodYear, PeriodMonth, (long)PfrBranchReportType.BranchReportType.InsuredPerson);
        }

        internal bool IsReportDataIncomplete()
        {
            return GetDataIncompleteRegions().Count > 0;
        }

        internal string GetDataIncompleteMessage()
        {
            List<PFRBranch> xL = GetDataIncompleteRegions();
            return GetDataIncompleteMessage(xL);
        }

        private List<PFRBranch> GetDataIncompleteRegions()
        {
            var res = new List<PFRBranch>();

            if (PeriodMonth == null)
            {
                //Get regions for annual 
                foreach (var b in _branches)
                {
                    if (!IsHaveReports(b, reports))
                        res.Add(b);
                }

            }
            else
            {
                //Get regions for monthly
                foreach (var b in _branches)
                {
                    if (!IsHaveReportsAnnual(b, reports))
                        res.Add(b);
                }
            }
            return res;
        }

        private bool IsHaveReportsAnnual(PFRBranch b, List<PfrBranchReport> reports)
        {
            return reports.Where(x => x.BranchID == b.ID).Count() == 12;
        }

        private bool IsHaveReports(PFRBranch b, List<PfrBranchReport> reports)
        {
            return reports.Any(x => x.BranchID == b.ID);
        }
    }
}
