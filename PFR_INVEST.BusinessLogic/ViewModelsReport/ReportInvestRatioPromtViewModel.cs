﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.Properties;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Reports;

namespace PFR_INVEST.BusinessLogic.ViewModelsReport
{
	[EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_viewer, DOKIP_ROLE_TYPE.OKIP_viewer, DOKIP_ROLE_TYPE.OARRS_viewer, DOKIP_ROLE_TYPE.OFPR_viewer, DOKIP_ROLE_TYPE.OUFV_viewer, DOKIP_ROLE_TYPE.OSRP_viewer, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
	[ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OVSI_worker, DOKIP_ROLE_TYPE.OVSI_manager, DOKIP_ROLE_TYPE.OUFV_manager, DOKIP_ROLE_TYPE.OUFV_worker)]
	public class ReportInvestRatioPromtViewModel : ViewModelCardDialog
	{

		public Document.Types Type { get; private set; }
		public List<Year> YearList { get; private set; }

		private long? yearID;
		public long? YearID { get { return yearID; } set { yearID = value; OnPropertyChanged("YearID"); } }

		private decimal? multiplier = 1.0M;
		public decimal? Multiplier { get { return multiplier; } set { multiplier = value; OnPropertyChanged("Multiplier"); } }



		public ReportInvestRatioPromtViewModel(Document.Types type)
		{

			YearList = DataContainerFacade.GetList<Year>();

			Type = type;
			YearID = DateTime.Now.Year - 2000;

			Multiplier = Settings.Default.StoredReportInvestMultiplier ?? 1.0m;
		}

		public override bool CanExecuteSave()
		{
			return IsValid();
		}

		public bool IsValid()
		{
			return ValidateFields();
		}

		public override string this[string columnName]
		{
			get
			{
				switch (columnName)
				{
					case "YearID": return YearID.ValidateRequired();
					case "Multiplier": return Multiplier.ValidateMaxFormat(6);

				}
				return null;
			}
		}


		public ReportInvestRatioPrompt GetReportPrompt()
		{
			if (!IsValid())
				return null;
			var prompt = new ReportInvestRatioPrompt
			{
				YearID = YearID ?? 0,
				Multiplier = Multiplier ?? 1,
				Type = Type
			};

			Settings.Default.StoredReportInvestMultiplier = Multiplier;
			Settings.Default.Save();

			return prompt;
		}
	}
}
