﻿
using System;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public abstract class ReportImportSIHandlerBase
    {
        public abstract string ReportType { get; }//Тип (код) отчета

        public abstract string OpenFileMask { get; } //Маска для выбора файла

        public abstract bool CanExecuteImport();

        public abstract bool ExecuteImport(out string message); //Импорт

        public abstract bool IsValidateDoc(Worksheet worksheet, out string message); //Валидация и заполнение модели

        public abstract void RefreshViewModels();

        public virtual bool CanExecuteOpenFile() { return false; }

        public virtual Month SelectedMonth { get; set; }

        public virtual Year SelectedYear { get; set; }

        public virtual string HeaderNameReport { get; set; }
      
        public virtual string ReportName { get; set; }

        //Загрузка данных из файла в память
        public virtual bool LoadFile(string fileName, out string message)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(string.Format("Файл {0} не был найден.", fileName));

            bool result = true;
            message = null;

            Application excelApp = null;
            Workbook workbook = null;
            Worksheet worksheet = null;

            try
            {
                excelApp = new Application { Visible = false };
                workbook = excelApp.Workbooks.Open(fileName);
                worksheet = (Worksheet)workbook.Sheets[1];
                result = IsValidateDoc(worksheet, out message);
            }
            catch (Exception ex)
            {
                message = ex.Message;
                result = false;
            }
            finally
            {
                if (excelApp != null)
                {
                    for (int i = 0; i < excelApp.Sheets.Count; i++)
                    {
                        Worksheet sheet = (Worksheet)excelApp.Sheets[i + 1];

                        ReleaseComObject(sheet);
                        sheet = null;
                    }

                    excelApp.Workbooks.Close();
                    for (int i = 0; i < excelApp.Workbooks.Count; i++)
                    {
                        Workbook w = excelApp.Workbooks[i + 1];
                        ReleaseComObject(w);
                        w = null;
                    }

                    if (workbook != null)
                    {
                        ReleaseComObject(workbook);
                        workbook = null;
                    }
                    excelApp.Quit();
                    ReleaseComObject(excelApp);
                    excelApp = null;
                }
            }
            return result;
        }

        private static void ReleaseComObject(object comObject)
        {
            int count = 0;
            while (count == 0)
            {
                count = Marshal.FinalReleaseComObject(comObject);
            }
            GC.GetTotalMemory(true);
        }


    }
}
