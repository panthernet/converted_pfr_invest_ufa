﻿
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ReportImportSIFactory
    {
        public static ReportImportSIHandlerBase GetImportHandler(ReportImportSIDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case "11":
                    return new ImportReviewSPNofUK();
                default:
                    return new ReportImportSIHandlerEmpty();
            }
        }
    }
}
