﻿
using System;
using Microsoft.Office.Interop.Excel;

namespace PFR_INVEST.BusinessLogic.SIReport
{
    public class ReportImportSIHandlerEmpty : ReportImportSIHandlerBase
    {
        public override string ReportType => "00";

        public override string OpenFileMask => string.Empty;

        public override bool CanExecuteImport()
        {
            return false;
        }



        public override bool ExecuteImport(out string message)
        {
            message = null;
            return false;
        }

        public override bool IsValidateDoc(Worksheet worksheet, out string message)
        {
            throw new NotImplementedException();
        }

        public override void RefreshViewModels()
        {
            throw new NotImplementedException();
        }
    }
}
