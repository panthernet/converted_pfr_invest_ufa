﻿

using System;

namespace PFR_INVEST.BusinessLogic.VRReport
{


    public class Application2Pfr : ReportExportVRHandlerBase
    {
        public override string ReportType => "03";

        public override bool IsReportContainsData => false;

        public override bool IsYearEnabled => true;

        public override bool IsYearVisible => true;


        public override bool IsMonthEnabled => true;

        public override bool IsMonthVisible => true;


        public override bool IsPerformerEnabled => true;

        public override bool IsPerformerVisible => true;

        public override bool IsResponsibleEnabled => true;

        public override bool IsResponsibleVisible => true;


        public override bool CanExecuteExport()
        {
            return false;
        }

        public override void ExecuteExport()
        {
            throw new NotImplementedException();
        }
    }
}
