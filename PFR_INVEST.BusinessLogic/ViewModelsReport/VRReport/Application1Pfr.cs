﻿using System;
using System.Collections.Generic;
using System.Linq;
using PFR_INVEST.DataAccess.Client;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    public class Application1Pfr : ReportExportVRHandlerBase
    {
        public class Calendar
        {
            public Year Year = new Year();
            public List<Month> Months = new List<Month>();

        }

        //public List<TransferListItem> Transfers;

        private const string OPERATION = "Финансирование выплат";
        private const string DIRECTION = "Из ГУК ВР в ПФР";

        private static readonly List<string> ContractNames = new List<string>
        {
         "Договор доверительного управления средствами ВР",
         "Договор доверительного управления СПН ЗЛ, которым установлена СПВ"};

        private readonly List<Calendar> _calendars;
        public List<SIRegister> SIRegisters;


        private Calendar _selectCalendar;

        public Application1Pfr()
        {
            //Transfers = BLServiceSystem.Client.GetTransfersListBYContractType((int)Document.Types.VR, false, false);
            //Transfers = Transfers.Where(tr => string.Equals(tr.Operation,operation,StringComparison.OrdinalIgnoreCase) &&   string.Equals(tr.Direction,direction,StringComparison.OrdinalIgnoreCase) ).ToList();
            // var ytr = Transfers.Select(tr => tr.Company).Distinct().ToList();
            var dir = DataContainerFacade.GetList<SPNDirection>().FirstOrDefault(d => string.Equals(d.Name, DIRECTION, StringComparison.OrdinalIgnoreCase));
            var oper = DataContainerFacade.GetList<SPNOperation>().FirstOrDefault(o => string.Equals(o.Name, OPERATION, StringComparison.OrdinalIgnoreCase));
            var contractNamesId = DataContainerFacade.GetList<ContractName>().Where(cn => ContractNames.Contains(cn.Name)).Select(cn => cn.ID).ToList();

            SIRegisters = BLServiceSystem.Client.GetSiRegistersByCondition((int)Document.Types.VR, dir.ID, oper.ID, contractNamesId, false);

            Years = DataContainerFacade.GetList<Year>();
            Monthes = DataContainerFacade.GetList<Month>();
            _calendars = new List<Calendar>();
            SetCalendar();
            if (!IsReportContainsData)
            {
                Monthes = null;
                Years = null;
                 return;
            }
               
        
            _years = _calendars.Select(c => c.Year).ToList();
            _selectCalendar = _calendars.FirstOrDefault();
            _selectedYear = _selectCalendar.Year;
            _monthes = _selectCalendar.Months;
            _selectedMonth = _selectCalendar.Months.FirstOrDefault();
        }

        private void SetCalendar()
        {


            var idYears = SIRegisters.OrderBy(sr => sr.CompanyYearID).Select(sr => sr.CompanyYearID).Distinct();
            foreach (var idYear in idYears)
            {
                var calendar = new Calendar {Year = Years.FirstOrDefault(y => y.ID == idYear)};
                var months = SIRegisters.Where(sr => sr.CompanyYearID == idYear).OrderBy(sr => sr.CompanyMonthID).Select(sr => sr.CompanyMonthID).Distinct().ToList();

                foreach (var month in months)
                {
                    calendar.Months.Add(Monthes.FirstOrDefault(m => m.ID == month));
                }
                _calendars.Add(calendar);
            }

        }

        public override string ReportType => "02";

        public override bool IsReportContainsData => _calendars.Any();

        private Year _selectedYear;
        public override Year SelectedYear
        {
            get
            {
                return _selectedYear;
            }
            set
            {
                _selectedYear = value;
                if (!IsReportContainsData) return;

                _selectCalendar = _calendars.FirstOrDefault(c => c.Year == _selectedYear);
                _monthes = _selectCalendar.Months;
                _selectedMonth = _selectCalendar.Months.FirstOrDefault();

            }
        }

        private List<Month> _monthes;
        public override List<Month> Monthes
        {
            get { return _monthes; }
            set
            {
                _monthes = value;
            }
        }

        private Month _selectedMonth;
        public override Month SelectedMonth
        {
            get
            {
                return _selectedMonth;
            }
            set {
                _selectedMonth = value ?? _selectCalendar.Months.FirstOrDefault();
            }
        }


        private List<Year> _years;
        public override List<Year> Years
        {
            get
            {
                return _years;
            }
            set
            {
                _years = value;
            }
        }

        public override Person Performer { get; set; }


        public override Person Responsible { get; set; }


        public override bool IsYearEnabled => true;

        public override bool IsYearVisible => true;


        public override bool IsMonthEnabled => true;

        public override bool IsMonthVisible => true;


        public override bool IsPerformerEnabled => true;

        public override bool IsPerformerVisible => true;

        public override bool IsResponsibleEnabled => true;

        public override bool IsResponsibleVisible => true;


        public override bool CanExecuteExport()
        {
            return IsReportContainsData &&  Performer != null && Responsible != null && SelectedMonth != null && SelectedYear != null ;
        }

        public override void ExecuteExport()
        {
            var ids = SIRegisters.Where(si => si.CompanyYearID == SelectedYear.ID && si.CompanyMonthID == SelectedMonth.ID).Select(si => si.ID).ToList();
            var printApp1 = new PrintApplication1Pfr(ids, Responsible, Performer, SelectedMonth, SelectedYear);
            printApp1.Print();
        }
    }
}
