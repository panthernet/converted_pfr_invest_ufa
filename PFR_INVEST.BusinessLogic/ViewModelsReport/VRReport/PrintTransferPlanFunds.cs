﻿

using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.BusinessLogic.BranchReport;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    public class PrintTransferPlanFunds : PrintVRYearTransfers
    {
        private readonly List<long> _yearTransferRegistersId;
        public PrintTransferPlanFunds(List<long> yearTransferRegistersId)
        {
            items = new List<TransferListItem>();
            _yearTransferRegistersId = yearTransferRegistersId;
        }
        protected override void FillData()
        {
            items = BLServiceSystem.Client.GetTransfersUkPlanListByRegListId(Document.Types.VR, false, true, listToString(_yearTransferRegistersId));

            Fields.Add("PLAN_YEAR", items.Count > 0 ? items.FirstOrDefault().Company : null);
        }

        string listToString(List<long> list)
        {
            var str = new StringBuilder();
            foreach (var item in list)
            {
                str.Append(item); 
                str.Append(", ");
            }

            return str.ToString().TrimEnd(',', ' ');
        }

    }
}
