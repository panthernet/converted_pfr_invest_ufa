﻿
using System;

namespace PFR_INVEST.BusinessLogic.VRReport
{
   
    public class ReportExportVRHandlerEmpty : ReportExportVRHandlerBase
    {
        public override string ReportType => "00";

        public override bool IsReportContainsData => false;

        public override bool CanExecuteExport()
        {
            return false;
        }

        public override void ExecuteExport()
        {
            throw new NotImplementedException("Не реализовано");
        }
    }
}
