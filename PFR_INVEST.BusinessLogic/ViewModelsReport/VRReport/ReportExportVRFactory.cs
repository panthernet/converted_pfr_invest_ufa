﻿
using PFR_INVEST.BusinessLogic.ViewModelsDialog;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ReportExportVRFactory
    {
        public static ReportExportVRHandlerBase GetExportHandler(ReportExportVRDlgViewModel model)
        {
            switch (model.SelectedReportType.ReportType)
            {
                case "01":
                    return new TransferPlanFunds();
                case "02":
                    return new Application1Pfr();
                case "03":
                    return new Application2Pfr();



                default:
                    return new ReportExportVRHandlerEmpty();
            }
        }
    }
}
