﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using PFR_INVEST.BusinessLogic.SIReport;
using PFR_INVEST.BusinessLogic.ViewModelsList;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.DataObjects;

namespace PFR_INVEST.BusinessLogic.VRReport
{
	public abstract class ReportImportVRHandlerBase : ReportImportSIHandlerBase
	{
		//private List<SiImportOperation> siImportOperations;
		protected List<Contract> Contracts;
		protected List<SIRegister> SIRegisters;
		private List<SiRegisterTransferData> _baseDataSiRegisters;
		protected SiRegisterTransferData ImportData;


		private List<long> _listDeleteSiRegisterID;
		protected static string Status = "Начальное состояние";

		protected abstract List<List<string>> HeaderTable { get; }
		protected abstract SiImportOperation Operation { get; }

	    protected ReportImportVRHandlerBase()
		{
			Contracts = BLServiceSystem.Client.GetContractsForUKsByType((int)Document.Types.VR);
			SIRegisters = BLServiceSystem.Client.GetSiRegistersByContractOperationHib((int)Document.Types.VR, null);
			ImportData = new SiRegisterTransferData();
		}

	    public override Year SelectedYear { get; set; }

	    public override Month SelectedMonth { get; set; }

	    protected virtual List<SiRegisterTransferData> GetDataToImport()
		{
			return new List<SiRegisterTransferData> { ImportData };
		}

		public override bool ExecuteImport(out string message)
		{
			//message = null;
			bool result = false;
			if (IsValidateImport(GetDataToImport(), out message))
			{
				result = Import(out message);
				if (result) RefreshViewModels();
			}

			_canExecuteImport = false;
			return result;
		}

	    /// <summary>
	    /// Валидация импорта
	    /// </summary>
	    /// <param name="listImport"></param>
	    /// <param name="message"></param>
	    /// <returns></returns>
	    private bool IsValidateImport(List<SiRegisterTransferData> listImport, out string message)
		{
			_listDeleteSiRegisterID = new List<long>();
			string contractName = string.Empty;

			foreach (var baseDataSiRegister in _baseDataSiRegisters)
			{
				foreach (var siTransferBase in baseDataSiRegister.siTransferList)
				{
					foreach (var import in listImport)
					{
						//var siTransferList = Import.siTransferList.FirstOrDefault(
						//        st => st.siTransfer.ContractID == siTransfer.siTransfer.ContractID);
						if (import.siTransferList.Any(s => s.siTransfer.ContractID == siTransferBase.siTransfer.ContractID))
						{
							if (
								siTransferBase.reqTransferList.Any(reqTr => !string.Equals(
									reqTr.TransferStatus, Status, StringComparison.InvariantCultureIgnoreCase)))
							{

								ViewModelBase.DialogHelper.ShowAlert(
									string.Format(
										"Импорт невозможен.\nНа указанный период реестр по операции \"{0}\" уже был сформирован, статус перечислений реестра отличен от \"{1}\".",
										ReportName,
										Status));

								message =
									string.Format(
										"Импорт невозможен.\nНа указанный период реестр по операции \"{0}\" уже был сформирован, статус перечислений реестра отличен от \"{1}\".",
										ReportName,
										Status);
								return false;

							}


							if (!_listDeleteSiRegisterID.Contains(baseDataSiRegister.siRegister.ID))
							{
								_listDeleteSiRegisterID.Add(baseDataSiRegister.siRegister.ID);
								contractName += siTransferBase.siTransfer.GetContract().ContractNumber + " ";
							}

						}
					}
				}


			}

			if (_listDeleteSiRegisterID.Count > 0)
			{

				if (
					!ViewModelBase.DialogHelper.ShowConfirmation(
						string.Format(
							"Внимание!\nНа указанный период реестр по операции {0} уже был сформирован.\nУдалить ранее созданный реестр и создать новый?",
							ReportName)))
				{
					message = "Импорт прерван пользователем.";
					return false;


				}


			}


			message = null;
			return true;
		}

		/// <summary>
		/// Валидация документа
		/// </summary>
		/// <param name="worksheet"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public override bool IsValidateDoc(Worksheet worksheet, out string message)
		{
			_canExecuteImport = false;

			Contracts = BLServiceSystem.Client.GetContractsForUKsByType((int)Document.Types.VR);
			SIRegisters = BLServiceSystem.Client.GetSiRegistersByContractOperationHib((int)Document.Types.VR, null);

			//List<List<object>> A2Q4 = (worksheet.get_Range("A2", "E3").Value2 as List<List<object>>);


			if (IsHeaderValidate(worksheet.Range["A1"].Value2.ToString(), out message))
			{
				for (int c = 0; c < HeaderTable.Count; c++)
				{
					if (!IsHeaderTableValidate((object[,])worksheet.Range[string.Format("A{0}", c + 2), (string.Format("{0}{1}", EndDataColumn, c + 2))].Value2,
					HeaderTable[c], out message))
						return false;
				}
			}
			else
			{
				return false;
			}
			//импоррт данных в класс
			if (!SetDataImport(worksheet, out message))
			{
				//ViewModelBase.DialogHelper.Alert(message);
				return false;
			}
			//получение данных приложения
			if (!SetDataBase(out message))
			{
				//ViewModelBase.DialogHelper.Alert(message);
				return false;
			}

			_canExecuteImport = true;
			return true;
		}

		private bool IsHeaderValidate(string header, out string message)
		{
			message = null;
			if (header.ToLower().Contains(HeaderNameReport.ToLower()))
			{

				if (header.ToLower().Contains(SelectedYear.Name.ToLower())
					&& header.ToLower().Contains(SelectedMonth.Name.ToLower()))
				{
					return true;
				}
			    message = string.Format("Отчет не содержит данных на {0} {1}.",
			        SelectedMonth.Name.ToLower(), SelectedYear.Name.ToLower());
			    return false;
			}
		    message = string.Format("Шапка документа не валидная!\nОжидалась колонка с названием:\n{0}\nФактически:\n{1}",
		        HeaderNameReport.TruncateAtWord(100),
		        header.TruncateAtWord(100));
		    return false;

		    // return true;
		}

		private bool IsHeaderTableValidate(object[,] obj, List<string> list, out string message)
		{

			for (int i = 0; i < list.Count; i++)
			{
				String columnValue = obj[1, i + 1] != null ? obj[1, i + 1].ToString() : null;
				if (list[i] == null && columnValue != null)
				{
					message = string.Format("Ожидалось пустое значение ячейки.\nФактически {0}", columnValue);
					return false;
				}
			    if (!string.Equals(columnValue, list[i], StringComparison.OrdinalIgnoreCase))
			    {
			        message = string.Format("Ожидалось {0} значение ячейки.\nФактически {1}", list[i], columnValue);
			        return false;
			    }
			}
			message = null;
			return true;
		}

		public override bool CanExecuteOpenFile()
		{
			return SelectedMonth != null && SelectedYear != null;
		}

		private bool _canExecuteImport;

		public override bool CanExecuteImport()
		{
			return _canExecuteImport;
		}

		protected abstract int RowDataIterator { get; }

		protected abstract String EndDataColumn { get; }


		/// <summary>
		/// получим данные из файла для импорта
		/// </summary>
		/// <param name="worksheet"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		protected virtual bool SetDataImport(Worksheet worksheet, out string message)
		{

			ImportData = new SiRegisterTransferData(SelectedMonth.ID, SelectedYear.ID, Operation.DirectionId, Operation.OperationId);

			int iterator = RowDataIterator;
			var end = worksheet.Range["A" + iterator].Value2;
			while (end != null)
			{
				if (!worksheet.Range["A" + iterator].Value2.ToString().ToLower().Contains("итого"))
				{
					if (
						!SetTransfer(iterator,
							(object[,])worksheet.Range[string.Format("A{0}", iterator), (string.Format("{0}{1}", EndDataColumn, iterator))].Value2,
							out message))
					{
						return false;
					}
				}

				end = worksheet.Range["A" + (++iterator)].Value2;
			}

			//message = null;
			return IsImportFilled(out  message);

		}

		protected string GetDocumnetType(String value)
		{
			switch (value.ToUpper())
			{
				case "НАКОПИТЕЛЬНЫЙ ИП":
					return "Договор доверительного управления средствами ВР";
				case "СРОЧНЫЙ ИП":
					return "Договор доверительного управления СПН ЗЛ, которым установлена СПВ";
				default:
					return null;
			}
		}

		protected abstract bool SetTransfer(int row, object[,] obj, out string message);

		protected virtual List<SIRegister> GetSiRegistersList()
		{
			return SIRegisters.Where(
									sir => sir.CompanyMonthID == SelectedMonth.ID &&
										sir.CompanyYearID == SelectedYear.ID &&
										sir.OperationID == Operation.OperationId)
										   .ToList();
		}

		/// <summary>
		/// полчим данные из базы о перечислениях на выбранный период
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		private bool SetDataBase(out string message)
		{
			try
			{
				var srs = GetSiRegistersList();
				_baseDataSiRegisters = new List<SiRegisterTransferData>();

				foreach (var sr in srs)
				{
					_baseDataSiRegisters.Add(new SiRegisterTransferData(sr));
				}
			}
			catch (Exception ex)
			{
				message = ex.Message;
				return false;
			}
			message = null;
			return true;
		}

		protected virtual string ImportRegistersData()
		{
			var msg = "";
			long resaltNchTp = ImportData.ExecuteSave();
			if (resaltNchTp != 0)
				msg += string.Format("№ {0} по операции «{1}»\n", resaltNchTp, Operation.OperationName);
			ImportData = new SiRegisterTransferData();
			return msg;
		}

		private bool Import(out string message)
		{
			message = "Сформированы реестры:\n";
			try
			{
				message += ImportRegistersData();
				if (_listDeleteSiRegisterID.Count > 0)
				{
				    SiRegisterTransferData.ExecuteDelete(SIRegisters, _listDeleteSiRegisterID);
				    message = _listDeleteSiRegisterID.Aggregate(message, (current, item) => current + string.Format("Удален реестр перечислений № {0}\n", item));
				}

			    if (!string.IsNullOrEmpty(message))
				{
					message += "\n\nДальнейшую обработку реестра производите из списка «Перечисления»";
				}
				_listDeleteSiRegisterID = new List<long>();
			}
			catch (Exception ex)
			{
				message = ex.Message;
				return false;
			}

			return true;
		}

		protected virtual bool IsImportFilled(out string message)
		{
			if (GetDataToImport().All(item => item.siTransferList.Count == 0))
			{
				message = "Импорт невозможен.\nОтсутствуют данные для импорта.";
				return false;
			}

			message = null;
			return true;
		}

		public override void RefreshViewModels()
		{
			ViewModelBase.ViewModelManager.RefreshViewModels(typeof(TransfersVRListViewModel));
		}

	}
}
