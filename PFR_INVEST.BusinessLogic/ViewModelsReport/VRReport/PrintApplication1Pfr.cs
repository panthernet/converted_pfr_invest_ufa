﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Office.Interop.Word;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using Document = Microsoft.Office.Interop.Word.Document;

namespace PFR_INVEST.BusinessLogic.VRReport
{
    //using DevExpress.XtraPrinting.Native;
    [ReadAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    [EditAccess(DOKIP_ROLE_TYPE.Administrator, DOKIP_ROLE_TYPE.OUFV_worker)]
    public class PrintApplication1Pfr : ViewModelBase
    {
        private const string wordTemplateName = "Приложение 1 к распоряжению ПФР.doc";

        private Application wApp;
        private object missing = Missing.Value;
        private object isVisible = true;

        private List<ExportReportVRListItem> exportData;
        private Person Responsible;
        private Person Performer;
        private Year ReportYear;
        private Month ReportMonth;


        private static string contractName = "Договор доверительного управления средствами ВР";

        private Document wDoc;

        public decimal FixedPercent { get; private set; }

        public PrintApplication1Pfr(List<long> ids, Person Responsible, Person Performer, Month SelectedMonth, Year SelectedYear)
        {
            exportData = BLServiceSystem.Client.GetDataReportVRApplication1Pfr(ids);
            this.Responsible = Responsible;
            this.Performer = Performer;
            ReportMonth = SelectedMonth;
            ReportYear = SelectedYear;
        }


        /// <summary>
        /// заполнение документа
        /// </summary>
        public void generateDoc()
        {



            Dictionary<string, string> replaces = new Dictionary<string, string>();
            replaces.Add("{DATE}", string.Format(" {0} {1} ", ReportMonth.Name.ToLower(), ReportYear.Name));
            replaces.Add("{RESPONSIBLE.POSITION}", Responsible.Position);
            replaces.Add("{RESPONSIBLE.FIOSHORT}", Responsible.FIOShort);
            //replaces.Add("{PERFORMER.FIOSHORT}", this.Performer.FIOShort);
            //replaces.Add("{PERFORMER.PHONE}", this.Performer.Phone);

            foreach (string key in replaces.Keys)
                OfficeTools.FindAndReplace(wApp, key, replaces[key]);


            String replaceStr = String.Empty;
            Table cbTable = wDoc.Tables[1];


            int counter = 0;
            foreach (var data in exportData)
            {
                Row row = cbTable.Rows.Add(ref missing);
                row.Cells.Height = 15;
                row.Range.Shading.BackgroundPatternColor = WdColor.wdColorWhite;
                row.Range.Font.Bold = 0;
                row.Range.ParagraphFormat.Alignment= WdParagraphAlignment.wdAlignParagraphLeft;
                row.Cells[1].Range.Text = (++counter).ToString();
                row.Cells[2].Range.Text = data.FormalizedName;
                row.Cells[3].Range.Text = data.ContractNumber;
                if (string.Equals(data.ContractName, contractName, StringComparison.OrdinalIgnoreCase))
                {
                    row.Cells[4].Range.Text = "Выплатного резерва";
                }
                else
                {
                    row.Cells[4].Range.Text = "Срочных пенсионных выплат";
                }
                row.Cells[5].Range.Text = data.Sum.Value.ToString();
                row.Cells[5].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;
            }

            Row rowFinish = cbTable.Rows.Add(ref missing);

            rowFinish.Range.ParagraphFormat.Shading.BackgroundPatternColor = WdColor.wdColorWhite;
            rowFinish.Range.Font.Bold = 0;
            rowFinish.Cells[1].Merge(rowFinish.Cells[4]);
            rowFinish.Cells[1].Range.Text = "Итого";
            rowFinish.Cells.Height = 25;
            rowFinish.Cells[1].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
            rowFinish.Cells[2].Range.Text = exportData.Sum(d => d.Sum).Value.ToString();
            rowFinish.Cells[2].Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphRight;

            foreach (Section wordSection in wDoc.Sections)
            {
                Range footerRange = wordSection.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                footerRange.Font.ColorIndex = WdColorIndex.wdGray50;
                footerRange.Font.Size = 9;
                footerRange.Text = string.Format("{0}\n{1}", Performer.FIOShort, Performer.Phone);
            }



        }

        /// <summary>
        /// вызов на печать
        /// </summary>
        public void Print()
        {
            object filePath = TemplatesManager.ExtractTemplate(wordTemplateName);

            if (File.Exists(filePath.ToString()))
            {
                wApp = new Application();
                wDoc = wApp.Documents.Add(ref filePath, ref missing, ref missing, ref isVisible);

                generateDoc();

                wApp.Visible = true;
            }
            else
            {
                RaiseErrorLoadingTemplate(wordTemplateName);
            }
        }




    }

}
