﻿using System;

namespace PFR_INVEST.Common.Logger
{
    /// <summary>
    /// Тип лога\ошибки.
    /// </summary>
    [Serializable]
    public enum LogSeverity
    {
        /// <summary>
        /// Информация отладки.
        /// </summary>
        Debug = 1,
        /// <summary>
        /// Информационное сообщение.
        /// </summary>
        Info = 2,
        /// <summary>
        /// Предупреждение.
        /// </summary>
        Warning = 3,
        /// <summary>
        /// Ошибка.
        /// </summary>
        Error = 4,
        /// <summary>
        /// Критическая ошибка.
        /// </summary>
        Fatal = 5
    }
}
