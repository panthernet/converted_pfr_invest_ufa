﻿namespace PFR_INVEST.Common.Logger
{
    /// <summary>
    /// Интерфейс объекта для записи лога.
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Осуществляет запись лога.
        /// </summary>
        /// <param name="sender">Отправитель запроса на запись лога.</param>
        /// <param name="e">Параметры лога.</param>
        void Log(object sender, LogEventArgs e);
    }
}
