﻿using System;

namespace PFR_INVEST.Common.Logger
{
    /// <summary>
    /// Класс для ведения логов.
    /// </summary>
    /// <remarks>
    /// Паттерн: Одиночка (Singletone), Наблюдатель (Observer).
    /// </remarks>
    public sealed class Logger
    {
        public delegate void LogEventHandler(object sender, LogEventArgs e);

        /// <summary>
        /// Событие записи ошибки в лог.
        /// </summary>
        public event LogEventHandler Log;

        #region The Singleton definition

        /// <summary>
        /// Экземпляр класса <see cref="Logger"/>.
        /// </summary>
        private static readonly Logger _instance = new Logger();

        /// <summary>
        /// </summary>
        private Logger()
        {
            // По умолчанию уровень сообщений "Error".
            Severity = LogSeverity.Error;
        }

        /// <summary>
        /// Возвращает экземпляр класса типа <see cref="Logger"/>.
        /// </summary>
        public static Logger Instance
        {
            get { return _instance; }
        }

        #endregion

        private LogSeverity _severity;

        // битовые поля. используются для улучшения производительности.
        private bool _isDebug;
        private bool _isInfo;
        private bool _isWarning;
        private bool _isError;
        private bool _isFatal;

        /// <summary>
        /// Возвращает или устанавливает уровень сообщений.
        /// </summary>
        public LogSeverity Severity
        {
            get { return _severity; }
            set
            {
                _severity = value;

                // устанавливаем битовые флаги
                var severity = (int)_severity;

                _isDebug = ((int)LogSeverity.Debug) >= severity ? true : false;
                _isInfo = ((int)LogSeverity.Info) >= severity ? true : false;
                _isWarning = ((int)LogSeverity.Warning) >= severity ? true : false;
                _isError = ((int)LogSeverity.Error) >= severity ? true : false;
                _isFatal = ((int)LogSeverity.Fatal) >= severity ? true : false;
            }
        }

        /// <summary>
        /// Запись сообщения уровня "Debug" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Debug(string message)
        {
            if (_isDebug)
                Debug(message, null);
        }

        /// <summary>
        /// Запись сообщения уровня "Debug" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="exception">Вложенная ошибка.</param>
        public void Debug(string message, Exception exception)
        {
            if (_isDebug)
                OnLog(new LogEventArgs(LogSeverity.Debug, message, exception, DateTime.Now));
        }

        /// <summary>
        /// Запись сообщения уровня "Info" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Info(string message)
        {
            if (_isInfo)
                Info(message, null);
        }

        /// <summary>
        /// Запись сообщения уровня "Info" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="exception">Вложенная ошибка.</param>
        public void Info(string message, Exception exception)
        {
            if (_isInfo)
                OnLog(new LogEventArgs(LogSeverity.Info, message, exception, DateTime.Now));
        }

        /// <summary>
        /// Запись сообщения уровня "Warning" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Warning(string message)
        {
            if (_isWarning)
                Warning(message, null);
        }

        /// <summary>
        /// Запись сообщения уровня "Info" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="exception">Вложенная ошибка.</param>
        public void Warning(string message, Exception exception)
        {
            if (_isWarning)
                OnLog(new LogEventArgs(LogSeverity.Warning, message, exception, DateTime.Now));
        }

        /// <summary>
        /// Запись сообщения уровня "Error" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Error(string message)
        {
            if (_isError)
                Error(message, null);
        }

        /// <summary>
        /// Запись сообщения уровня "Error" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="exception">Вложенная ошибка.</param>
        public void Error(string message, Exception exception)
        {
            if (_isError)
                OnLog(new LogEventArgs(LogSeverity.Error, message, exception, DateTime.Now));
        }

        /// <summary>
        /// Запись сообщения уровня "Fatal" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public void Fatal(string message)
        {
            if (_isFatal)
                Fatal(message, null);
        }

        /// <summary>
        /// Запись сообщения уровня "Fatal" или выше.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        /// <param name="exception">Вложенная ошибка.</param>
        public void Fatal(string message, Exception exception)
        {
            if (_isFatal)
                OnLog(new LogEventArgs(LogSeverity.Fatal, message, exception, DateTime.Now));
        }

        /// <summary>
        /// Генерация события записи сообщения.
        /// </summary>
        /// <param name="e">Параметры события.</param>
        public void OnLog(LogEventArgs e)
        {
            if (Log != null)
                Log(this, e);
        }

        /// <summary>
        /// Добавляет экземпляр "слушателя" события записи сообщения для ведения логов типа <see cref="ILog"/>.
        /// </summary>
        /// <param name="observer">"Наблюдатель"</param>
        public void Attach(ILog observer)
        {
            Log += observer.Log;
        }

        /// <summary>
        /// Отключает экземпляр "слушателя" события записи сообщения для ведения логов типа <see cref="ILog"/>.
        /// </summary>
        /// <param name="observer">"Наблюдатель"</param>
        public void Detach(ILog observer)
        {
            Log -= observer.Log;
        }
    }
}