﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PFR_INVEST.Common.Tools
{
    public static class EnumerableTools
    {
        public static IEnumerable<T> ToHierarchy<T, TProperty>(this IEnumerable<T> source, Func<T, TProperty> idProperty, Func<T, TProperty?> parentIdProperty, Func<T, ICollection<T>> childrenProperty)
        where TProperty : struct
        {
            return ToHierarchy(source, idProperty, parentIdProperty, childrenProperty, null);
        }

        public static IEnumerable<T> ToHierarchy<T, TProperty>(this IEnumerable<T> source, Func<T, TProperty> idProperty, Func<T, TProperty?> parentIdProperty, Func<T, ICollection<T>> childrenProperty, Func<IEnumerable<T>, IEnumerable<T>> siblingsArrangeFunction)
            where TProperty : struct
        {
            List<T> rootItems = source.Where(x => parentIdProperty(x) == null).ToList();
            IEnumerable<TProperty> ids = source.Select(idProperty);
            rootItems.AddRange(source.Where(x => parentIdProperty(x) != null).Where(x => !ids.Contains(parentIdProperty(x).Value)));

            if (siblingsArrangeFunction != null)
                rootItems = siblingsArrangeFunction(rootItems).ToList();

            foreach (T item in rootItems)
            {
                IEnumerable<T> children = RetrieveChildren(source, idProperty, parentIdProperty, childrenProperty, idProperty(item), siblingsArrangeFunction);

                foreach (T _item in children)
                {
                    childrenProperty(item).Add(_item);
                }

                yield return item;
            }
        }

        private static IEnumerable<T> RetrieveChildren<T, TProperty>(IEnumerable<T> allItems, Func<T, TProperty> idProperty, Func<T, TProperty?> parentIdProperty, Func<T, ICollection<T>> childrenProperty, TProperty id, Func<IEnumerable<T>, IEnumerable<T>> siblingsArrangeFunction)
            where TProperty : struct
        {
            IEnumerable<T> items = allItems.Where(x => object.Equals(parentIdProperty(x), id));
            if (siblingsArrangeFunction != null)
                items = siblingsArrangeFunction(items);

            foreach (T item in items)
            {
                IEnumerable<T> children = RetrieveChildren(allItems, idProperty, parentIdProperty, childrenProperty, idProperty(item), siblingsArrangeFunction);
                foreach (T _item in children)
                {
                    childrenProperty(item).Add(_item);
                }
                yield return item;
            }
        }
    }
}
