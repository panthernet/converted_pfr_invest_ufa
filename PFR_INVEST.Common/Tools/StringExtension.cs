﻿using System;

namespace PFR_INVEST.Common.Tools
{
    public static class StringExtension
    {
        public static string TruncateAtWord(this string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public static bool IsEmpty(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;

            return s.Trim().Length == 0;
        }

        /// <summary>
        /// Вычисляет процент похожести двух строк (число от 0 до 1)
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static double Similarity(this string s1, string s2)
        {
            string longer = s1, shorter = s2;
            if (s1.Length < s2.Length)
            { // longer should always have greater length
                longer = s2; shorter = s1;
            }

            int longerLength = longer.Length;
            if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
            /* // If you have StringUtils, you can use it to calculate the edit distance:
            return (longerLength - StringUtils.getLevenshteinDistance(longer, shorter)) /
                                                                 (double) longerLength; */
            return (longerLength - EditDistance(longer, shorter)) / (double)longerLength;
        }

        /// <summary>
        /// Example implementation of the Levenshtein Edit Distance
        /// See http://r...content-available-to-author-only...e.org/wiki/Levenshtein_distance#Java
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        private static int EditDistance(string s1, string s2)
        {
            s1 = s1.ToLower();
            s2 = s2.ToLower();

            int[] costs = new int[s2.Length + 1];
            for (int i = 0; i <= s1.Length; i++)
            {
                int lastValue = i;
                for (int j = 0; j <= s2.Length; j++)
                {
                    if (i == 0)
                        costs[j] = j;
                    else
                    {
                        if (j > 0)
                        {
                            int newValue = costs[j - 1];
                            if (s1[i - 1] != s2[j - 1])
                                newValue = Math.Min(Math.Min(newValue, lastValue), costs[j]) + 1;
                            costs[j - 1] = lastValue;
                            lastValue = newValue;
                        }
                    }
                }
                if (i > 0)
                    costs[s2.Length] = lastValue;
            }
            return costs[s2.Length];
        }
    }
}
