﻿using System.Text.RegularExpressions;

namespace PFR_INVEST.Common.Tools
{
    public static class DataTools
    {
        public const string DATAERROR_LENGTH_MORE = "Длина более";
        public const string DATAERROR_LENGTH_LESS = "Длина менее";
        public const string DATAERROR_NEED_DATA = "Необходимо ввести данные";
        public const string DATAERROR_NOT_SELECTED = "Необходимо выбрать значение";
        public const string DATAERROR_INVALID_FORMAT = "Неверный формат";

        public static string ValidateNotNullOrWhitespace(string value)
        {
            return value == null || string.IsNullOrEmpty(value.Trim()) ? DATAERROR_NEED_DATA : null;
        }

        public static string ValidateLengthExact(string value, int length)
        {
            string error = string.Format("Длина поля должна равняться {0}", length);
            return (value ?? string.Empty).Trim().Length != length ? error : null;
        }

        public static string ValidateLength(string value, int length)
        {
            const string error = "Слишком длинное значение поля";
            return (value ?? string.Empty).Length > length ? error : null;
        }

        public static string ValidateEmail(string value)
        {
            return IsEmail(value) ? null : DATAERROR_INVALID_FORMAT;
        }
        
        public static bool IsEmail(string inputEmail)
        {
            inputEmail = inputEmail ?? "";
            string strRegex = @"^[0-9a-zA-Z_.\-]{2,50}[@]{1}[0-9a-zA-Z_./-]{2,40}[.]{1}[a-zA-Z]{2,5}$";
            return new Regex(strRegex).IsMatch(inputEmail);
        }

        public static bool IsEmailList(string inputEmailList, char[] separators)
        {
            foreach (string eMial in inputEmailList.Split(separators, System.StringSplitOptions.RemoveEmptyEntries))
                if (!DataTools.IsEmail(eMial)) return false;
            return true;
        }

        public static bool IsPhoneNumber(string phone)
        {
            phone = phone.Replace(" ", "");
            return new Regex(@"\(\d{3}\)\d{3}-\d{4}").IsMatch(phone) || new Regex(@"\(\d{3}\)\d{3}-\d{2}-\d{2}").IsMatch(phone) ||
                new Regex(@"\(\d{3}\)\d{7}").IsMatch(phone) || new Regex(@"\(\d{3}\)\d{7}").IsMatch(phone);
        }

        public static bool IsNumericString(string value)
        {
            //перебираем все символы в строке и если они отлины от чисел, то false
            for (int i = 0; i < value.Length; i++)
            {
                if (!"1234567890,.".Contains(value[i].ToString()))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
