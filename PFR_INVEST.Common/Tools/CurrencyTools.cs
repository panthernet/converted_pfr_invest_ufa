﻿using System;

namespace PFR_INVEST.Common.Tools
{
    public class CurrencyTools
    {
        public static decimal[] DivideSum(decimal sumToDivide, int numberOfParts)
        {
            decimal[] result = new decimal[numberOfParts];
            decimal sumPerPart = Decimal.Round(sumToDivide / (decimal)numberOfParts, 2);
            decimal delta = sumToDivide - sumPerPart * numberOfParts;
            for (int i = 0; i < numberOfParts; i++)
            {
                result[i] = sumPerPart;
                if (i == numberOfParts - 1)
                    result[i] += delta;
            }
            return result;
        }
    }
}
