﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WebForms;
using NHibernate;
using NHibernate.Transform;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.Reports;
using PFR_INVEST.Proxy;
using PFR_INVEST.Proxy.Tools;
#if WEBCLIENT
using NHibernate.Util;
using PGDecimal4HibType = PFR_INVEST.Web2.Database.NHibernate.PGDecimal4HibType;
using DB2Decimal4HibType = PFR_INVEST.Web2.Database.NHibernate.DB2Decimal4HibType;
#else
using db2connector.NHibernate;
using PGDecimal4HibType = db2connector.NHibernate.PGDecimal4HibType;
using DB2Decimal4HibType = db2connector.NHibernate.DB2Decimal4HibType;
using NHibernate.Linq;
#endif

namespace db2connector
{
    public partial class IISServiceHib : IISServiceBase
    { 
        private ReportFile RenderReport(string reportName, string filename, IEnumerable<ReportParameter> param,
            IEnumerable<ReportDataSource> data, ReportFile.Types type)
        {
            var res = new ReportFile();

            //Загрузка шаблона из базы
            var template = GetTemplateByName(reportName);
            if (template == null)
            {
                res.Error = $"Шаблон отчёта '{reportName}' не найден в базе";
                return res;
            }

            try
            {
                var report = new LocalReport();
                report.LoadReportDefinition(new MemoryStream(template.Body));

                //Установка параметров и датасоурса
                report.SetParameters(param);
                foreach (var d in data)
                    report.DataSources.Add(d);

                string renderFormat;
                switch (type)
                {
                    case ReportFile.Types.Excel:
                        renderFormat = "Excel";
                        break;
                    case ReportFile.Types.Pdf:
                        renderFormat = "PDF";
                        break;
                    case ReportFile.Types.Word:
                        renderFormat = "Word";
                        break;
                    default:
                        renderFormat = "Image";
                        break;
                }

                Warning[] warnings;
                string[] streamids;
                string mimeType, encoding, extension;

                res.FileBody = report.Render(renderFormat, null, out mimeType, out encoding, out extension,
                    out streamids, out warnings);
                res.Extension = extension;
                res.FileName = filename + "." + extension;
                res.Type = type;
                return res;
            }
            catch (Exception ex)
            {
                res.Error = $"При генерации отчёта возникла ошибка '{ex.Message}'";
                LogManager.LogException(ex);
                return res;
            }
        }

        public List<ReportErrorSpreadQueryItem> GetReportErrorMonthSpreadData(ReportErrorTypesPrompt prompt)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return
                    session.CreateSQLQuery(
                        $@"SELECT                                                 
												     {(IsDB2
                            ? "MONTH("
                            : "EXTRACT( MONTH FROM ")}ra.REGDATE) as MONTH,     
                                                     ra.ERROR_CODE as CODE,
                                                     err.DESCRIPTION as CODE_NAME,
                                                     COUNT(ra.id) as SUMM
                                                    FROM PFR_BASIC.NPF_REJECT_APP ra
                                                    LEFT JOIN PFR_BASIC.NPF_ERROR_CODE err ON err.CODE = ra.ERROR_CODE AND err.ISERRORCODE = 1
                                                    WHERE ra.ERROR_CODE IS NOT NULL
                                                        AND {(IsDB2
                                ? "YEAR("
                                : "EXTRACT( YEAR FROM ")}ra.REGDATE) = :year
                                                    GROUP BY ra.REGDATE, ra.ERROR_CODE, err.DESCRIPTION
                                                    ")
                        .SetParameter("year", prompt.Year)
                        .List<object[]>()
                        .Select(a => new ReportErrorSpreadQueryItem(a)).ToList();
            }
        }

        public List<ReportErrorSpreadQueryItem> GetReportErrorRegionSpreadData(ReportErrorTypesPrompt prompt)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateSQLQuery(@"SELECT 
                                                     region.NAME as REGION,
                                                     ra.ERROR_CODE as CODE,
                                                     dock.DESCRIPTION as CODE_NAME,
                                                     COUNT(ra.id) as SUMM
                                                    FROM PFR_BASIC.PFRBRANCH region
                                                    LEFT JOIN PFR_BASIC.NPF_REJECT_APP ra ON ra.REGION = region.ID
                                                    LEFT JOIN PFR_BASIC.NPF_ERROR_CODE dock ON dock.CODE = ra.ERROR_CODE AND dock.ISERRORCODE = 1
                                                    WHERE ra.ERROR_CODE IS NOT NULL
                                                        AND ra.REGDATE >= :from
												        AND ra.REGDATE <= :to                                                                                           
                                                    GROUP BY region.NAME, ra.ERROR_CODE, dock.DESCRIPTION
                                                    ")
                    .SetParameter("from", prompt.DateFrom.Value.Date)
                    .SetParameter("to", prompt.DateTo.Value.Date)
                    .List<Object[]>()
                    .Select(a => new ReportErrorSpreadQueryItem(a)).ToList();
            }
        }

        public List<ReportErrorSpreadQueryItem> GetReportErrorNpfSpreadData(ReportErrorTypesPrompt prompt)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateSQLQuery(@"SELECT 
                                                     le.FORMALIZEDNAME as NPF,
                                                     ra.ERROR_CODE as CODE,
                                                     dock.DESCRIPTION as CODE_NAME,
                                                     COUNT(ra.id) as SUMM
                                                    FROM PFR_BASIC.LEGALENTITY le
                                                    LEFT JOIN PFR_BASIC.LEGALENTITY_IDENTIFIER leid ON leid.LE_ID = le.ID
                                                    LEFT JOIN PFR_BASIC.NPF_REJECT_APP ra ON ra.SELECTION = leid.IDENTIFIER
                                                    LEFT JOIN PFR_BASIC.NPF_ERROR_CODE dock ON dock.CODE = ra.ERROR_CODE AND dock.ISERRORCODE = 1
                                                    WHERE ra.ERROR_CODE IS NOT NULL
                                                        AND ra.REGDATE >= :from
												        AND ra.REGDATE <= :to
                                                    GROUP BY le.FORMALIZEDNAME, ra.ERROR_CODE, dock.DESCRIPTION                                                                                             
                                                    ")
                    .SetParameter("from", prompt.DateFrom.Value.Date)
                    .SetParameter("to", prompt.DateTo.Value.Date)
                    .List<Object[]>()
                    .Select(a => new ReportErrorSpreadQueryItem(a)).ToList();
            }
        }

        public List<ReportErrorTypesQueryItem> GetReportErrorTypeData(ReportErrorTypesPrompt prompt)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateSQLQuery(@"SELECT 
                                                     ec.DESCRIPTION as desc1,
                                                     ec.CODE,
                                                     ra.CONTRACT_CODE,
                                                     dock.DESCRIPTION as desc2,
                                                     COUNT(coalesce(ra.id,0)) as sum
                                                    FROM PFR_BASIC.NPF_ERROR_CODE ec
                                                    LEFT JOIN PFR_BASIC.NPF_REJECT_APP ra ON ra.ERROR_CODE = ec.CODE AND ec.ISERRORCODE = 1
                                                    LEFT JOIN PFR_BASIC.NPF_ERROR_CODE dock ON dock.CODE = ra.CONTRACT_CODE AND dock.ISERRORCODE = 0
                                                    WHERE ec.ISERRORCODE = 1 AND ra.CONTRACT_CODE IS NOT NULL
                                                    AND ra.REGDATE >= :from
												    AND ra.REGDATE <= :to
                                                    GROUP BY ec.DESCRIPTION, ec.CODE, ra.CONTRACT_CODE,dock.DESCRIPTION")
                    .SetParameter("from", prompt.DateFrom.Value.Date)
                    .SetParameter("to", prompt.DateTo.Value.Date)
                    .List<object[]>()
                    .Select(a => new ReportErrorTypesQueryItem(a)).ToList();
            }
        }

        public ReportFile CreateReportIPUKTotal(ReportIPUKPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel)
        {
            prompt.ContractID = null;
            return CreateReportIPUK(prompt, type);
        }

        public ReportFile CreateReportIPUK(ReportIPUKPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel)
        {
            //Запросы к базе

            using (var session = DataAccessSystem.OpenSession())
            {
                var list =
                    session.CreateQuery(
                        @"SELECT new ReportIPUKListItem(rt, sr.DirectionID, sr.Operation.Name, sr.Comment, ftr, le.FormalizedName, c.ContractNumber, c.ContractDate) FROM ReqTransfer rt 
												JOIN rt.CommonPPList ftr
												JOIN rt.TransferList st
												JOIN st.Contract c
                                                JOIN c.LegalEntity le
												JOIN st.TransferRegister sr
												
												WHERE ( :contractID is null OR c.ID = :contractID ) 
												AND c.TypeID = :type
                                                AND TRIM(LOWER(rt.TransferStatus)) = :status
												AND coalesce(rt.TransferActDate, rt.SPNDebitDate) is not null 
												AND coalesce(rt.TransferActDate, rt.SPNDebitDate) >= :from
												AND coalesce(rt.TransferActDate, rt.SPNDebitDate) <= :to
                                                AND st.StatusID <> -1 AND sr.StatusID <> -1 AND ftr.StatusID <> -1 AND rt.StatusID > 0
												") //AND (ftr.ID is null or ftr.ID in (select max(a.ID) from AsgFinTr a group by a.ReqTransferID))
                        .SetParameter("status", TransferStatusIdentifier.sActSigned.ToLower())
                        .SetParameter("contractID", prompt.ContractID)
                        .SetParameter("type", (long)prompt.Type)
                        .SetParameter("from", prompt.DateFrom)
                        .SetParameter("to", prompt.DateTo)
                        .List<ReportIPUKListItem>();
                //Выбор последнего ПП для пречисления вынесен из запроса хибернейта из за тормозов подзапроса

                list = list.GroupBy(r => r.ID).Select(r => r.OrderBy(p => p.CommonPPDraftPayDate).Last()).ToList();

                var sumToUk = session.CreateQuery(@"SELECT Sum(coalesce(rt.Sum, 0)) FROM ReqTransfer rt 
													JOIN rt.TransferList st
													JOIN st.Contract c
													JOIN st.TransferRegister sr
													WHERE ( :contractID is null OR c.ID = :contractID )
                                                    AND TRIM(LOWER(rt.TransferStatus)) = :status
													AND c.TypeID = :type
													AND coalesce(rt.TransferActDate, rt.SPNDebitDate) is not null 
													AND coalesce(rt.TransferActDate, rt.SPNDebitDate) < :from
													AND sr.DirectionID in (2, 4)
                                                    AND st.StatusID <> -1 AND sr.StatusID <> -1 AND rt.StatusID > 0")
                    .SetParameter("status", TransferStatusIdentifier.sActSigned.ToLower())//rt.TransferList.ContractID = :contractID
                    .SetParameter("contractID", prompt.ContractID)
                    .SetParameter("type", (long)prompt.Type)
                    .SetParameter("from", prompt.DateFrom)
                    .UniqueResult<decimal>();

                var sumFromUk =
                    session.CreateQuery(
                        @"SELECT Sum(coalesce( rt.Sum, 0)-coalesce(rt.InvestmentIncome,0)) FROM ReqTransfer rt 
													JOIN rt.TransferList st
													JOIN st.Contract c
													JOIN st.TransferRegister sr

													WHERE ( :contractID is null OR c.ID = :contractID ) 
                                                    AND TRIM(LOWER(rt.TransferStatus)) = :status
													AND c.TypeID = :type
													AND coalesce(rt.TransferActDate, rt.SPNDebitDate) is not null 
													AND coalesce(rt.TransferActDate, rt.SPNDebitDate) < :from
													AND sr.DirectionID in (1, 3)
                                                    AND st.StatusID <> -1 AND sr.StatusID <> -1 AND rt.StatusID > 0")
                        .SetParameter("status", TransferStatusIdentifier.sActSigned.ToLower())
                        .SetParameter("contractID", prompt.ContractID)
                        .SetParameter("type", (long)prompt.Type)
                        .SetParameter("from", prompt.DateFrom)
                        .UniqueResult<decimal>();

                if (prompt.ContractID.HasValue)
                {
                    //По конкретному договору
                    var contract = session.Get<ContractHib>(prompt.ContractID);
                    //Создание и отдача отчёта

                    var startUkSum = sumToUk - sumFromUk;
                    var param = new List<ReportParameter>
                    {
                        new ReportParameter("DateFrom", prompt.DateFrom.ToString()),
                        new ReportParameter("DateTo", prompt.DateTo.ToString()),
                        new ReportParameter("StartUKSum", string.IsNullOrEmpty(startUkSum.ToString()) ? "0" : startUkSum.ToString()),
                        new ReportParameter("UKName", contract.LegalEntity.FormalizedName),
                        new ReportParameter("ContractNumber", contract.ContractNumber),
                        new ReportParameter("ContractPortfolio", contract.PortfolioName),
                        new ReportParameter("ContractDate", contract.ContractDate.ToString())
                    };

                    var data = new List<ReportDataSource> { new ReportDataSource("ReqTransfer", list) };

                    var fileName =
                        $"Отчёт ИП УК {contract.LegalEntity.FormalizedName} {contract.ContractNumber} с {prompt.DateFrom:dd.MM.yyyy} по {prompt.DateTo:dd.MM.yyyy}";

                    return RenderReport("ReportIPUK.rdlc", fileName, param, data, type);
                }
                else
                {
                    var startUkSum = sumToUk - sumFromUk;
                    //Совокупный
                    var param = new List<ReportParameter>
                    {
                        new ReportParameter("DateFrom", prompt.DateFrom.ToString()),
                        new ReportParameter("DateTo", prompt.DateTo.ToString()),
                        new ReportParameter("StartUKSum",  string.IsNullOrEmpty(startUkSum.ToString()) ? "0" : startUkSum.ToString())
                    };

                    var data = new List<ReportDataSource> { new ReportDataSource("ReqTransfer", list) };
                    var fileName = $"Отчёт ИП совокупный с {prompt.DateFrom:dd.MM.yyyy} по {prompt.DateTo:dd.MM.yyyy}";
                    return RenderReport("ReportIPUKTotal.rdlc", fileName, param, data, type);
                }
            }
        }

        //Журнал СПН НПФ
        public ReportFile CreateReportAgrregateSPN(long[] npfIds, long[] opIds, bool addNpf, string direction, DateTime dateFrom, DateTime dateTo, bool isExtended, bool isGarant)
        {
            //AsgFinTr.DraftPayDate < dateTo
            using (var session = DataAccessSystem.OpenSession())
            {
                var contrIdList = session.CreateQuery($@"select c.ID from LegalEntity le 
                                                        join le.Contragent c
                                                        where le.id in (:list) and c.StatusID > 0")
                    .SetParameterList("list", npfIds)
                    .List<long>().ToList();
                if (addNpf)
                {
                    var lst = session.CreateQuery($@"select sc.ID from Reorganization re 
                                                        join re.SourceContragent sc
                                                        join re.ReceiverContragent rc
                                                        join re.ReorganizationType t
                                                        where rc.ID in (:list) and sc.StatusID > 0")
                        .SetParameterList("list", contrIdList)
                        .List<long>();
                   if(lst.Any())
                        contrIdList.AddRange(lst);
                }

                var opStrings = session.CreateQuery(@"select e.Name from Element e where e.ID in (:list)")
                    .SetParameterList("list", opIds)
                    .List<string>().Distinct().ToList();

                var list = session.CreateSQLQuery(GetAggrSpnReportQuery(dateFrom, dateTo, addNpf, direction, contrIdList, opStrings))
                    .List<object[]>().Select(a => new ReportAggregSpnListItem(FixDecimal((decimal) a[0], 4) ?? 0, (string) a[1], (string) a[2], (long)a[3], (int)a[4]));

                var finalList = new List<ReportAggregSpnListItem>();
                //group by npf & operation
                if (!isExtended) // ;
                    list.GroupBy(a => new {a.FormalizedName}).ForEach(group =>
                    {
                        var item = new ReportAggregSpnListItem
                        {
                            FormalizedName = group.Key.FormalizedName,
                            OperationName = "Все выбранные",
                            FromNpf = group.Sum(a => a.FromNpf),
                            ToNpf = group.Sum(a => a.ToNpf)
                        };
                        finalList.Add(item);
                    });
                else list.GroupBy(a => new { a.FormalizedName, a.OperationName }).ForEach(group =>
                {
                    var item = new ReportAggregSpnListItem
                    {
                        FormalizedName = group.Key.FormalizedName,
                        OperationName = group.Key.OperationName,
                        FromNpf = group.Sum(a => a.FromNpf),
                        ToNpf = group.Sum(a => a.ToNpf)
                    };
                    finalList.Add(item);
                });
                //get initial spn sums
                var iList = session.CreateSQLQuery(GetAggrSpnReportAllPPQuery(dateFrom, contrIdList, opStrings))
                    .List<object[]>()
                    .Select(a => new {Sum = FixDecimal((decimal)a[0], 4) ?? 0, Name = (string) a[1], Op = (string) a[2], OpID = (long)a[3]})
                    .ToList();

                //assign income and calc outcome
                finalList.ForEach(item =>
                {
                    var result = iList.Where(a => a.Name == item.FormalizedName && (item.OperationName == "Все выбранные" || a.Op == item.OperationName)).Sum(a=> a.Sum);
                    item.IncomeSpnLeft = result;
                    item.OutcomeSpnLeft = item.IncomeSpnLeft + item.ToNpf - item.FromNpf;
                });

                var totalIn = finalList.Sum(a => a.IncomeSpnLeft).ToString(CultureInfo.InvariantCulture);
                var totalOut = finalList.Sum(a => a.OutcomeSpnLeft).ToString(CultureInfo.InvariantCulture);

                finalList.ForEach(a =>
                {
                    a.IncomeSpnLeft = a.IncomeSpnLeft;
                    a.OutcomeSpnLeft = a.OutcomeSpnLeft;
                    a.ToNpf =a.ToNpf;
                    a.FromNpf = a.FromNpf;
                });

                var param = new List<ReportParameter>
                    {
                        new ReportParameter("DateFrom", dateFrom.ToString(CultureInfo.CurrentCulture)),
                        new ReportParameter("DateTo", dateTo.ToString(CultureInfo.CurrentCulture)),
                        new ReportParameter("TotalIn", totalIn),
                        new ReportParameter("TotalOut", totalOut),
                        new ReportParameter("Ops", isExtended ? string.Join("\n",opStrings.ToArray()) : "Все операции"),
                        new ReportParameter("IsExtended",isExtended.ToString()),
                        new ReportParameter("Direction", string.IsNullOrEmpty(direction) ? "Все направления" : direction)
                    };

                var data = new List<ReportDataSource> { new ReportDataSource("ReportsDataSet", finalList) };
                var fileName = $"Журнал СПН НПФ c {dateFrom:dd.MM.yyyy} по {dateTo:dd.MM.yyyy}";
                return RenderReport("Журнал агрегированного учета СПН НПФ.rdlc", fileName, param, data, ReportFile.Types.Word);
            }
        }

        private string GetAggrSpnReportAllPPQuery(DateTime? periodStart, IEnumerable<long> cIds, IEnumerable<string> ops)
        {
            var cListString = GenerateStringFromList(cIds);
            var oListString = GenerateStringFromList(ops, ",", true);

            return $@"select            
            SUM(coalesce(a.DRAFT_AMOUNT, 0) * (CASE WHEN LOWER(r.KIND) < 'передача' THEN -1 ELSE 1 END)) AS SUMM,
            ca.NAME AS CNAME,
            r.CONTENT as CONTENT,
            r.ID
            FROM PFR_BASIC.ASG_FIN_TR a
            INNER JOIN PFR_BASIC.FINREGISTER f ON a.FR_ID = f.ID
            INNER JOIN PFR_BASIC.REGISTER r ON f.REG_ID = r.ID
            LEFT JOIN PFR_BASIC.ACCOUNT acc ON acc.ID=f.CR_ACC_ID or acc.ID=f.DBT_ACC_ID
            LEFT JOIN PFR_BASIC.CONTRAGENT ca ON ca.ID = acc.CONTRAGENTID

            WHERE

            a.STATUS_ID <> -1 {GetFilterCondition(false, "a.DRAFT_PAYDATE", new DateTime(2001,01,01), periodStart.Value.Subtract(TimeSpan.FromDays(1)))}
            and ca.ID IN ({cListString})
            and r.CONTENT in ({oListString})
            group by ca.name, r.ID, r.CONTENT";
        }

        private string GetAggrSpnReportQuery(DateTime? periodStart, DateTime? periodEnd, bool addNpf, string direction, IEnumerable<long> cIds, IEnumerable<string> ops )
        {
            var sb = new StringBuilder();
            var isFromPfr = direction == RegisterIdentifier.PFRtoNPF;
            var dir = string.IsNullOrEmpty(direction) ? null : (direction == RegisterIdentifier.PFRtoNPF ? (long?) AsgFinTr.Directions.FromPFR : (long?) AsgFinTr.Directions.ToPFR);

            var cListString = GenerateStringFromList(cIds);
            var oListString = GenerateStringFromList(ops, ",", true);

            sb.AppendFormat(
                $@"select
            coalesce(a.DRAFT_AMOUNT,0) AS SUMM,
            ca.NAME AS CNAME,
            r.CONTENT as CONTENT,
            r.ID as MYID,
            CASE WHEN LOWER(r.KIND) < 'передача' THEN 1 ELSE -1 END as MOD
            FROM PFR_BASIC.ASG_FIN_TR a
            INNER JOIN PFR_BASIC.FINREGISTER f ON a.FR_ID=f.ID
            INNER JOIN PFR_BASIC.REGISTER r ON f.REG_ID=r.ID
            LEFT JOIN PFR_BASIC.ACCOUNT acc ON {(dir == null ? "acc.ID=f.CR_ACC_ID or acc.ID=f.DBT_ACC_ID" : (isFromPfr ? "acc.ID=f.DBT_ACC_ID" : "acc.ID=f.CR_ACC_ID"))}
            LEFT JOIN PFR_BASIC.CONTRAGENT ca ON ca.ID=acc.CONTRAGENTID
	        WHERE 
	        a.STATUS_ID <> -1 {GetFilterCondition(false, "a.DRAFT_PAYDATE", periodStart, periodEnd)} {(string.IsNullOrEmpty(direction) ? null : $" and LOWER(r.KIND)='{direction.ToLower()}'")}
                and ca.ID IN ({cListString}) and r.CONTENT in ({oListString})");
            return sb.ToString();
        }

        private static string GetFilterCondition(bool isWhere, string columnName, DateTime? periodStart, DateTime? periodEnd)
        {
            if (periodStart == null || periodEnd == null)
                return string.Empty;

            if (isWhere)
            {
                return string.Format(
                    " WHERE ( {0} is not null AND {0}  BETWEEN '{1:yyyy-MM-dd}' AND '{2:yyyy-MM-dd}') ",
                    columnName, periodStart.Value, periodEnd.Value);
            }
            return string.Format(
                " AND ( {0} is not null AND {0}  BETWEEN '{1:yyyy-MM-dd}' AND '{2:yyyy-MM-dd}') ",
                columnName, periodStart.Value, periodEnd.Value);
        }

        public ReportFile CreateReportDeposits(ReportDepositsPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var field = prompt.UseSettleDate ? "a.PlacementDate" : "a.ReturnDate";
                var offers = session.CreateQuery(@" select distinct o 													
													FROM DepClaimOffer o
													join fetch o.Auction a
													join fetch o.Bank bank
													join o.Deposits d
													left join fetch o.DepClaim dc
													left join fetch o.OfferPfrBankAccountSum psum
													left join fetch psum.Portfolio p

													WHERE 1=1
														AND " + field + @" >= :fromDate
														AND " + field + @" <= :toDate
												")
                    .SetParameter("fromDate", prompt.DateFrom)
                    .SetParameter("toDate", prompt.DateTo)
                    .List<DepClaimOfferHib>();

                var list = offers.SelectMany(o =>
                {
                    //o.DepClaim.Payment
                    var returnedPercent =
                        o.OfferPfrBankAccountSum.Sum(
                            d => d.PaymentHistory.Where(ph => ph.IsForPercent).Sum(ph => ph.Sum));
                    decimal? plannedPerc = o.OfferPfrBankAccountSum.Sum(d => d.PlannedSum ?? 0);
                    plannedPerc = plannedPerc == 0 ? null : plannedPerc;
                    var isReturned = returnedPercent == (plannedPerc ?? o.DepClaim.Payment);


                    var listDep = o.OfferPfrBankAccountSum.Where(d => !d.IsDeleted()).Select(d =>
                        new ReportDepositListItem
                        {
                            AuctionDate = o.Auction.SelectDate,
                            AuctionNumber = o.Auction.AuctionNum,
                            BankName = o.Bank.FormalizedNameFull,
                            OfferNumber = o.Number,
                            Percent = o.DepClaim.Rate,
                            SumBody = d.Sum,
                            //Для выплаченых показываем из истории платежей, для невыплаченых - расчётное значение
                            SumPercent = isReturned
                                ? d.PaymentHistory.Where(ph => ph.IsForPercent).Sum(ph => ph.Sum)
                                : Math.Round(o.DepClaim.Payment * (d.Sum / o.DepClaim.Amount), 2, MidpointRounding.ToEven),
                            Period = o.Auction.Period,
                            SettleDate = o.Auction.PlacementDate,
                            ReturnDate = o.Auction.ReturnDate,
                            Portfolio = d.Portfolio.Name,
                            StockName = o.Auction.StockName

                        }).ToList();

                    if (listDep.Count > 0)
                    {
                        //Если проценты разбились неточно - корректируем последнюю запись
                        var diff = (plannedPerc ?? o.DepClaim.Payment) - listDep.Sum(d => d.SumPercent);
                        if (diff != 0)
                        {
                            listDep.Last().SumPercent += diff;
                        }
                    }
                    else
                    {
                        //Если нет разбития по портфелю - возвращаем общий
                        listDep.Add(new ReportDepositListItem
                        {
                            AuctionDate = o.Auction.SelectDate,
                            AuctionNumber = o.Auction.AuctionNum,
                            BankName = o.Bank.FormalizedNameFull,
                            OfferNumber = o.Number,
                            Percent = o.DepClaim.Rate,
                            SumBody = o.DepClaim.Amount,
                            //Для выплаченых показываем из истории платежей, для невыплаченых - расчётное значение
                            SumPercent = plannedPerc ?? o.DepClaim.Payment,
                            Period = o.Auction.Period,
                            SettleDate = o.Auction.PlacementDate,
                            ReturnDate = o.Auction.ReturnDate,
                            Portfolio = string.Empty,
                            StockName = o.Auction.StockName

                        });
                    }
                    return listDep;
                }).ToList();

                var data = new List<ReportDataSource> {new ReportDataSource("Deposits", list)};
                var fileName =
                    $"Сводный отчет по депозитам с {prompt.DateFrom:dd.MM.yyyy} по {prompt.DateTo:dd.MM.yyyy}";

                return RenderReport("Сводный отчёт по депозитам.rdlc", fileName, new List<ReportParameter>(), data, type);
            }

        }

        private static void GetTransferSum(ISession session, IList<ReportBalanceUKListItem> list, DateTime date,
            Action<ReportBalanceUKListItem, decimal, decimal> action, List<long> direction, List<long> operation)
        {
            var res = session.CreateQuery(@"SELECT 												
												c.ID,												
												sum(coalesce(rt.Sum,0)),
												sum(rt.InvestmentIncome)
											FROM ReqTransfer rt 
												JOIN rt.TransferList st
												JOIN st.TransferRegister sr
												JOIN st.Contract c												
												WHERE 
													rt.TransferActDate is not null
                                                AND TRIM(LOWER(rt.TransferStatus)) = :status
													AND rt.TransferActDate <= :date 
                                                 AND rt.StatusID>0 and st.StatusID<>-1 and sr.StatusID<>-1"

                                          + (direction != null ? @"AND sr.DirectionID in (:direction)" : " ")
                                          + (operation != null ? @"AND sr.OperationID in (:operation)" : " ")
                                          + @" GROUP BY c.ID")

                .SetParameter("status", TransferStatusIdentifier.sActSigned.ToLower())
                .SetParameter<DateTime?>("date", date);

            if (direction != null)
                res.SetParameterList("direction", direction);
            if (operation != null)
                res.SetParameterList("operation", operation);

            var sum =
                res.List()
                    .Cast<object[]>()
                    .Select(o => new { ID = (long)o[0], Sum = (decimal?)o[1] ?? 0, Invest = (decimal?)o[2] ?? 0 })
                    .ToList();


            var it2 = list.Join(sum, i => i.ContractID, s => s.ID, (i, s) =>
            {
                action(i, s.Sum, s.Invest);
                return i;
            }).ToList();

        }

        //Остаток СПН
        public ReportFile CreateReportBalanceUK(ReportBalanceUKPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                const int contractType = (int)Document.Types.SI;
                var directions = new List<long> { (long)SIRegister.Directions.ToUK, (long)SIRegister.Directions.ToGUK };
                var items = session.CreateQuery(@"SELECT Distinct
												le.FormalizedName as UKName,
												c.ContractNumber  as ContractNumber ,
												c.ID			  as ContractID,
												c.ContractDate	  as ContractDate,
												ca.TypeName as UKType
											FROM ReqTransfer rt 
												JOIN rt.TransferList st
												JOIN st.TransferRegister sr
												JOIN st.Contract c
												JOIN c.LegalEntity le
												JOIN le.Contragent ca 
												WHERE 
													rt.TransferActDate is not null
                                                AND TRIM(LOWER(rt.TransferStatus)) = :status
													AND c.TypeID = :contractType AND c.Status >= 0
													AND rt.TransferActDate <= :date
                                                    AND rt.StatusID>0 and st.StatusID>0 and sr.StatusID>0
													AND sr.DirectionID in (:direction)
                                                    AND rt.StatusID > 0 and st.StatusID <> -1 and sr.StatusID <> -1
													")
                    .SetParameter("status", TransferStatusIdentifier.sActSigned.ToLower())
                    .SetParameter<DateTime?>("date", prompt.Date)
                    .SetParameter("contractType", contractType)
                    .SetParameterList("direction", directions)
                    .SetResultTransformer(Transformers.AliasToBean<ReportBalanceUKListItem>())
                    .List<ReportBalanceUKListItem>();

                //Заполнение сумм по групам
                var to = new List<long> { (long)SIRegister.Directions.ToUK, (long)SIRegister.Directions.ToGUK };
                var from = new List<long> { (long)SIRegister.Directions.FromUK, (long)SIRegister.Directions.FromGUK };

                //В УК
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) => i.ToUKTotal = sum, to, null);
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) => i.ToUKDSV = sum, to, new List<long>
                {
                    (long) SIRegister.Operations.SPNRedistributionDSV,
                    (long) SIRegister.Operations.DSVQuarterPortfolioDissolution
                });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) => i.ToUKMSK = sum, to, new List<long>
                {
                    (long) SIRegister.Operations.SPNRedistributionMSK,
                    (long) SIRegister.Operations.MSKHalfYearPortfolioDissolution
                });

                //Из УК
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnTotal = sum;
                    i.ReturnTotalInvest = inv;
                }, from, null);
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnSPN_NCTP = sum;
                    i.ReturnSPN_NCTPInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.WithdrawNCTP });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnSPN_SPV = sum;
                    i.ReturnSPN_SPVInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.WithdrawSPV });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnSPN_EV = sum;
                    i.ReturnSPN_EVInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.WithdrawEV });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnDSV = sum;
                    i.ReturnDSVInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.SPNRedistributionDSV });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnMSK = sum;
                    i.ReturnMSKInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.SPNRedistributionMSK });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnPravopr = sum;
                    i.ReturnPravoprInvest = inv;
                }, from,
                    new List<long> { (long)SIRegister.Operations.DeadZL });
                GetTransferSum(session, items, prompt.Date, (i, sum, inv) =>
                {
                    i.ReturnMart = sum;
                    i.ReturnMartInvest = inv;
                }, from,
                    new List<long>
                    {
                        (long) SIRegister.Operations.SPNRedistribution,
                        (long) SIRegister.Operations.SPNReturn
                    });

                //Создание и отдача отчёта
                var param = new List<ReportParameter> { new ReportParameter("ReportDate", prompt.Date.ToString()) };
                var data = new List<ReportDataSource> { new ReportDataSource("BalanceList", items) };
                var fileName = $"Остаток средств пенсионных накоплений (на дату) {prompt.Date:dd.MM.yyyy}";
                return RenderReport("Остаток средств пенсионных накоплений (на дату).rdlc", fileName, param, data, type);
            }
        }


        public ReportFile CreateReportForNpfOpsReport(int year, int quarter, bool toNpf, List<long> npfIds, bool loadUnlinkedPP, bool isFormalizedName)
        {
            DateTime from;
            DateTime to;
            GetCBReportDates(false, year, quarter, out from, out to);

            var where = loadUnlinkedPP? $" and (le.ID in ({GenerateStringFromList(npfIds)}) or le.id is null )" : $" and le.ID in ({GenerateStringFromList(npfIds)})";
            var list = toNpf ? GetTransfersListForCBReportForm2_PFRtoNPF(1, from, to, where, isFormalizedName) : GetTransfersListForCBReportForm2_NPFtoPFR(1, from, to, where, isFormalizedName);

            //Создание и отдача отчёта
            var data = new List<ReportDataSource> { new ReportDataSource("OpsForNpf", list) };
            var fileName = $"Операции по передаче, получению денежных средств от НПФ";
            return RenderReport("ОpsToNpfReport.rdlc", fileName, new List<ReportParameter>(), data, ReportFile.Types.Excel);
        }


        public ReportFile CreateReportRSASCA(ReportRSASCAPrompt prompt, ReportFile.Types type = ReportFile.Types.Excel)
        {
            return prompt.Date < new DateTime(2007, 04, 01) ? CreateReportRSASCA_Before2007(prompt, type) : CreateReportRSASCA_After2007(prompt, type);
        }

        private ReportFile CreateReportRSASCA_Before2007(ReportRSASCAPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                #region Long query

                var items = session.CreateSQLQuery(@" 
															SELECT c.REGNUM as ContractNumber,
																	c.REGDATE as ContractDate,
																	c.FORMALNAME as UKName,
																	c.PORTFNAME as Portfolio,	
																	edoR.FUNDS1 as A010 , 
																	edoR.FUNDS2 as A010P, 
																	edoR.FUNDSCUR1 as A011 , 
																	edoR.FUNDSCUR2 as A011P, 
																	edoR.DEPRUR1 as A020 , 
																	edoR.DEPRUR2 as A020P, 
																	edoR.GCBRF1 as A030 , 
																	edoR.GCBRF2 as A030P, 
																	edoR.GCBRFINCUR1 as A031 , 
																	edoR.GCBRFINCUR2 as A031P, 
																	edoR.GCBSUB1 as A040 , 
																	edoR.GCBSUB2 as A040P, 
																	edoR.GCBSUBCUR1 as A041 , 
																	edoR.GCBSUBCUR2 as A041P, 
																	edoR.STOCKMO1 as A050 , 
																	edoR.STOCKMO2 as A050P, 
																	edoR.STOCKMOHO1 as A060 , 
																	edoR.STOCKMOHO2 as A060P, 
																	edoR.STOCKMOHOINCUR1 as A061 , 
																	edoR.STOCKMOHOINCUR2 as A061P, 
																	edoR.AKCIIRE1 as A070 , 
																	edoR.AKCIIRE2 as A070P, 
																	edoR.PAI1 as A080 , 
																	edoR.PAI2 as A080P, 
																	edoR.CBIPOTEKA1 as A090 , 
																	edoR.CBIPOTEKA2 as A090P, 
																	edoR.CBIPOTEKAGRF1 as A091 , 
																	edoR.CBIPOTEKAGRF2 as A091P, 
																	edoR.DEBDOLGITOGO1 as A100 , 
																	edoR.DEBDOLGITOGO2 as A100P, 
																	edoR.PROFI1 as A101 , 
																	edoR.PROFI2 as A101P, 
																	edoR.PROCDOLG1 as A102 , 
																	edoR.PROCDOLG2 as A102P, 
																	edoR.OTHERDOLG1 as A103 , 
																	edoR.OTHERDOLG2 as A103P,
																	edoR.OTHERACTIV1 as A110,
																	
																	edoS.CREDIT1 as A120,	
																	edoS.PROFOBIAZ1 as A121,
																	edoS.CREDITREWARD1 as A122,
																	edoS.OTHERCREDIT1 as A123													
--, edoR.*, edoS.*
															FROM 
																PFR_BASIC.CONTRACT c
																JOIN PFR_BASIC.LEGALENTITY le ON le.ID = c.LE_ID
																JOIN PFR_BASIC.CONTRAGENT ca ON ca.ID = le.CONTRAGENTID
																JOIN PFR_BASIC.EDO_ODKF020 edoR ON c.ID = edoR.ID_CONTRACT
																JOIN (
																	SELECT t.ID, t.ID_CONTRACT, t.ONDATE , RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY ONDATE DESC) AS rn , RANK() OVER(PARTITION BY ID_CONTRACT,ONDATE ORDER BY ID DESC) AS rnID 
																	FROM PFR_BASIC.EDO_ODKF020 t 
																	WHERE ONDATE <= :date) ppR ON ppR.ID = edoR.ID AND ppR.rn = 1  AND ppR.rnID = 1
																JOIN PFR_BASIC.EDO_ODKF010 edoS ON c.ID = edoS.ID_CONTRACT
																JOIN (
																	SELECT t.ID, t.ID_CONTRACT, t.REPORTONDATE , RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORTONDATE DESC) AS rn , RANK() OVER(PARTITION BY ID_CONTRACT,REPORTONDATE ORDER BY ID DESC) AS rnID 
																	FROM PFR_BASIC.EDO_ODKF010 t 
																	WHERE REPORTONDATE <= :date) ppS ON ppS.ID = edoS.ID AND ppS.rn = 1  AND ppS.rnID = 1 
															WHERE
																c.TYPEID = :type
																AND ca.STATUS_ID = 1
																AND (c.DISSOLDATE is null OR c.DISSOLDATE > :date) 
												")
                    .AddScalar("ContractNumber", NHibernateUtil.String)
                    .AddScalar("UKName", NHibernateUtil.String)
                    .AddScalar("Portfolio", NHibernateUtil.String)
                    .AddScalar("A010",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A010P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A011",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A011P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A020",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A020P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A030",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A030P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A031",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A031P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A040",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A040P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A041",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A041P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A050",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A050P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A060",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A060P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A061",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A061P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A070",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A070P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A080",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A080P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A090",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A090P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A091",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A091P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A100",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A100P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A101",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A101P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A102",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A102P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A103",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A103P",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A110",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A120",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A121",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A122",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("A123",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ContractDate", NHibernateUtil.Date)
                    .SetParameter<DateTime?>("date", prompt.Date.Date)
                    .SetParameter("type", (long)prompt.Type)
                    .SetResultTransformer(Transformers.AliasToBean<ReportRSASCAOldListItem>())
                    .List<ReportRSASCAOldListItem>();


                #endregion Long query

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDate", prompt.Date.Date.ToString(CultureInfo.CurrentCulture)),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position),
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", items) };

                var fileName = $"СЧА-РСА активы {prompt.Date:dd.MM.yyyy}";
                return RenderReport("СЧА-РСА активы (до 2007).rdlc", fileName, param, data, type);
            }
        }

        private ReportFile CreateReportRSASCA_After2007(ReportRSASCAPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                #region Long query

                var items = session.CreateSQLQuery(@" 
															SELECT c.REGNUM as ContractNumber,
																	c.REGDATE as ContractDate,
																	c.FORMALNAME as UKName,
																	c.PORTFNAME as Portfolio,	
																	edoR.GROUP1 as R010,
																	edoR.GROUP2 as R020,
																	edoR.GROUP3 as R030,
																	edoR.GROUP4 as R040,
																	edoR.GROUP5 as R050,
																	edoR.GROUP6 as R060,
																	edoR.GROUP7 as R070,
																	edoR.GROUP8 as R080,
																	edoR.GROUP9 as R090,
																	edoR.GROUP10 as R100,
																	edoR.GROUP11 as R110,
																	edoR.GROUP12 as R120,
																	edoR.GROUP81 as R081,
																	edoR.GROUP13 as R130,
																	edoR.SUBGROUP1 as R131,
																	edoR.SUBGROUP2 as R132,
																	edoR.SUBGROUP3 as R133,
																	edoR.TOTAL_AMOUNT as R140,		
																	
																	edoS.A010 as S010,
																	edoS.A020 as S020,
																	edoS.A030 as S030,
																	edoS.A032 as S032,
																	edoS.A033 as S033,
																	edoS.A034 as S034,
																	edoS.A035 as S035,
																	edoS.A037 as S037,
																	edoS.A038 as S038,
																	edoS.A036 as S036,
																	edoS.A039 as S039,
																	edoS.A40 as S040,
																	edoS.A41 as S041,
																	edoS.A42 as S042,
																	edoS.A43 as S043,
																	edoS.A50 as S050,
																	edoS.A70 as S070,
																	edoS.A071 as S071,
																	edoS.A072 as S072,
																	edoS.A073 as S073,
																	edoS.A074 as S074,
																	edoS.A075 as S075,
																	edoS.A080 as S080,
																	edoS.A090 as S090													
--, edoR.*, edoS.*
															FROM 
																PFR_BASIC.CONTRACT c
																JOIN PFR_BASIC.LEGALENTITY le ON le.ID = c.LE_ID
																JOIN PFR_BASIC.CONTRAGENT ca ON ca.ID = le.CONTRAGENTID
																JOIN PFR_BASIC.EDO_ODKF025 edoR ON c.ID = edoR.ID_CONTRACT
																JOIN (
																	SELECT t.ID, t.ID_CONTRACT, t.REPORT_ON_DATE , RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_DATE DESC) AS rn , RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_DATE ORDER BY ID DESC) AS rnID
																	FROM PFR_BASIC.EDO_ODKF025 t 
																	WHERE REPORT_ON_DATE <= :date) ppR ON ppR.ID = edoR.ID AND ppR.rn = 1 AND ppR.rnID = 1 
																JOIN PFR_BASIC.EDO_ODKF015 edoS ON c.ID = edoS.ID_CONTRACT
																JOIN (
																	SELECT t.ID, t.ID_CONTRACT, t.REPORT_ON_TIME , RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_TIME DESC) AS rn , RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_TIME ORDER BY ID DESC) AS rnID 
																	FROM PFR_BASIC.EDO_ODKF015 t 
																	WHERE REPORT_ON_TIME <= :date) ppS ON ppS.ID = edoS.ID AND ppS.rn = 1 AND ppS.rnID = 1 
															WHERE
																c.TYPEID = :type
																AND ca.STATUS_ID = 1
																AND (c.DISSOLDATE is null OR c.DISSOLDATE > :date) 
												")
                    .AddScalar("ContractNumber", NHibernateUtil.String)
                    .AddScalar("UKName", NHibernateUtil.String)
                    .AddScalar("Portfolio", NHibernateUtil.String)
                    .AddScalar("ContractDate", NHibernateUtil.Date)
                    .AddScalar("R010",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R020",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R030",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R040",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R050",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R060",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R070",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R080",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R090",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R100",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R110",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R120",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R081",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R130",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R131",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R132",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R133",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R140",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S010",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S020",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S030",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S032",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S033",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S034",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S035",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S037",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S038",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S036",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S039",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S040",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S041",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S042",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S043",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S050",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S070",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S071",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S072",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S073",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S074",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S075",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S080",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("S090",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))

                    .SetParameter<DateTime?>("date", prompt.Date.Date)
                    .SetParameter("type", (long)prompt.Type)
                    .SetResultTransformer(Transformers.AliasToBean<ReportRSASCAListItem>())
                    .List<ReportRSASCAListItem>();

                #endregion Long query

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDate", prompt.Date.ToString(CultureInfo.CurrentCulture)),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position),
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", items) };

                var fileName = $"СЧА-РСА активы {prompt.Date:dd.MM.yyyy}";
                return RenderReport("СЧА-РСА активы (после 2007).rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportF060(ReportF060Prompt prompt, ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"select new ReportF060ListItem(edo, c, le.FormalizedName,ca.TypeName)
													from EdoOdkF060 edo
													join edo.Contract c
													join c.LegalEntity le 
													join le.Contragent ca 
													where edo.YearId = :yearID and edo.Quartal = :quarter and c.TypeID = :type")
                    .SetParameter("yearID", prompt.YearID)
                    .SetParameter("quarter", prompt.Quarter)
                    .SetParameter("type", (long)prompt.Type)
                    .List<ReportF060ListItem>();

                //Берём только последний отчёт, если несколько за один день, то в большим рег. номером
                list =
                    list.GroupBy(r => new { Y = r.YearId, Q = r.Quartal, C = r.ContractId })
                        .Select(g => g.OrderBy(r => r.RegDate).ThenBy(r => r.RegNumber).Last())
                        .ToList();

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString()),
                    new ReportParameter("ReportQuarter", prompt.Quarter.ToString()),
                    new ReportParameter("Units", prompt.Units.ToString()),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position),    };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", list) };

                var fileName = $"Отчет об инвестировании {prompt.Quarter} квартал {prompt.YearID + 2000}";
                return RenderReport("Отчет об инвестировании.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportF070(ReportF070Prompt prompt, ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"select new ReportF070ListItem(edo, c, le.FormalizedName,ca.TypeName)
													from EdoOdkF070 edo
													join edo.Contract c
													join c.LegalEntity le 
													join le.Contragent ca 
													where edo.YearId = :yearID and edo.Quartal = :quarter and c.TypeID = :type")
                    .SetParameter("yearID", prompt.YearID)
                    .SetParameter("quarter", prompt.Quarter)
                    .SetParameter("type", (long)prompt.Type)
                    .List<ReportF070ListItem>();

                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString()),
                    new ReportParameter("ReportQuarter", prompt.Quarter.ToString()),
                    new ReportParameter("Units", prompt.Units.ToString()),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position)  
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", list) };

                var fileName = $"Отчет о доходах {prompt.Quarter} квартал {prompt.YearID + 2000}";
                return RenderReport("Отчет о доходах.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportProfitability(ReportProfitabilityPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"select new ReportProfitabilityListItem(edo, c)
													from McProfitAbility edo
													join edo.Contract c													
													where edo.YearId = :yearID and edo.Quarter = :quarter and c.TypeID = :type

													and edo.Date in (
														select max(b.Date) 
														from McProfitAbility b
														where b.YearId = edo.YearId and b.Quarter = edo.Quarter and b.ContractId = edo.ContractId
														)")
                    .SetParameter("yearID", prompt.YearID)
                    .SetParameter("quarter", prompt.Quarter)
                    .SetParameter("type", (long)prompt.Type)
                    .List<ReportProfitabilityListItem>();

                //При наличии дублей с одной датой - берём последний ИД
                list = list.GroupBy(l => l.ContractId).Select(g => g.OrderBy(gr => gr.ID).Last()).ToList();

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString()),
                    new ReportParameter("ReportQuarter", prompt.Quarter.ToString()),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position)  
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", list) };

                var fileName = $"Доходность совокупно {prompt.Quarter} квартал {prompt.YearID + 2000}";
                return RenderReport("Доходность совокупно.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportOwnedFounds(ReportOwnedFoundsPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var list = session.CreateQuery(@"select new ReportOwnedFoundsListItem(edo, le)
													from OwnedFounds edo
													join fetch edo.LegalEntity le													
													where edo.YearID = :yearID and edo.Quartal = :quarter 
													")
                    .SetParameter("yearID", prompt.YearID)
                    .SetParameter("quarter", prompt.Quarter)
                    //Убрано, так как не имеет смысла - отчёт по УК в целом
                    //.SetParameter<long>("type", (long)prompt.Type)
                    .List<ReportOwnedFoundsListItem>();

                //Оставляем только последние отчёты
                list =
                    list.GroupBy(r => new { Y = r.YearID, Q = r.Quartal, UK = r.UKName })
                        .Select(g => g.OrderByDescending(r => r.Date).First())
                        .ToList();

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString()),
                    new ReportParameter("ReportQuarter", prompt.Quarter.ToString()),
                    new ReportParameter("Person", prompt.Person.FormattedFullName),
                    new ReportParameter("Position", prompt.Person.Position)  
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Edo", list) };

                var fileName = $"Собственные средства совокупно {prompt.Quarter} квартал {prompt.YearID + 2000}";
                return RenderReport("Собственные средства совокупно.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportInvestRatio(ReportInvestRatioPrompt prompt,
            ReportFile.Types type = ReportFile.Types.Excel)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var from = new DateTime((int)prompt.YearID + 2000, 1, 1);
                var to = new DateTime((int)prompt.YearID + 2000, 12, 31);

                #region big query

                var query = string.Format(@" SELECT 
															c.REGNUM as ContractNumber, c.REGDATE, c.REGCLOSE, le.FORMALIZEDNAME as UKName, c.PORTFNAME as Portfolio,
															S0.Sum as S0, Sn.Sum as Sn, Sm.SUM as Sm , CAST( D.SUM as DECIMAL(19, 4)) as Sc,
															Coalesce(Sk1.SUM ,Sk2.SUM) as Sk, 
															RV.R as R, RV.V as V,
															F070.SUMMAX as F070SumMax, F070.SUMFACT as F070SumFact,
															F070Old.SUMMAX as F070SumMaxOld, F070Old.SUMFACT as F070SumFactOld, F070Old.UKREWARD2 as F070RewardOld
														FROM
															PFR_BASIC.CONTRACT c
															JOIN PFR_BASIC.LEGALENTITY le ON le.ID = c.LE_ID
															LEFT JOIN
															(
																--Начальное СЧА
																	SELECT scha.* FROM (
																		SELECT 
																			ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum,
																			RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORTONDATE DESC) AS rn , 
																			RANK() OVER(PARTITION BY ID_CONTRACT,REPORTONDATE ORDER BY ID DESC) AS rnID
																		FROM 
																			PFR_BASIC.EDO_ODKF010  
																		WHERE 
																			REPORTONDATE + {0} = :from 	AND ID_CONTRACT is not null
																		) scha
																	WHERE	scha.rn = 1 AND scha.rnID = 1	
																	UNION
																	SELECT scha2.* FROM (
																		SELECT 
																			ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum,
																			RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_TIME DESC) AS rn , 
																			RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_TIME ORDER BY ID DESC) AS rnID
																		FROM 
																			PFR_BASIC.EDO_ODKF015  
																		WHERE 
																			REPORT_ON_TIME + {0} = :from  AND ID_CONTRACT is not null
																		) scha2
																	WHERE	scha2.rn = 1 AND scha2.rnID = 1	
															) S0 ON c.ID = S0.ID_CONTRACT
															LEFT JOIN
															(
																-- все перечисления в УК
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM 
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID 
											
																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID 
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID 
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION not in ('')
																	--AND sr.DIR_SPN_ID in (1,3) -- из УК
																	AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID
															) Sn ON c.ID = Sn.ID_CONTRACT
															LEFT JOIN
															(
																-- все перечисления из УК, кроме расторжения
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM 
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID 
											
																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID 
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID 
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION not in ('Возврат СПН - расторжение')
																	AND sr.DIR_SPN_ID in (1,3) -- из УК
																	--AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID
															) Sm ON c.ID = Sm.ID_CONTRACT
															LEFT JOIN 
															(
																--Финальное СЧА
																SELECT scha.* FROM (
																	SELECT 
																		ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum,
																		RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORTONDATE DESC) AS rn , 
																		RANK() OVER(PARTITION BY ID_CONTRACT,REPORTONDATE ORDER BY ID DESC) AS rnID
																	FROM 
																		PFR_BASIC.EDO_ODKF010  
																	WHERE 
																		REPORTONDATE  = :to 	AND ID_CONTRACT is not null
																	) scha
																WHERE	scha.rn = 1 AND scha.rnID = 1	
																UNION
																SELECT scha2.* FROM (
																	SELECT 
																		ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum,
																		RANK() OVER(PARTITION BY ID_CONTRACT ORDER BY REPORT_ON_TIME DESC) AS rn , 
																		RANK() OVER(PARTITION BY ID_CONTRACT,REPORT_ON_TIME ORDER BY ID DESC) AS rnID
																	FROM 
																		PFR_BASIC.EDO_ODKF015  
																	WHERE 
																		REPORT_ON_TIME = :to  AND ID_CONTRACT is not null
																	) scha2
																WHERE	scha2.rn = 1 AND scha2.rnID = 1	
															) Sk1 ON c.ID = Sk1.ID_CONTRACT
															LEFT JOIN 
															(
																-- перечисления из УК - расторжения
																SELECT st.CONTR_ID as ID_CONTRACT, SUM(rt.SUM) as SUM
																FROM 
																	PFR_BASIC.SI_REGISTER sr
																	JOIN PFR_BASIC.OPERATION o ON o.ID = sr.OPERATION_ID 
											
																	JOIN PFR_BASIC.SI_TRANSFER st ON sr.ID = st.SI_REG_ID 
																									AND st.CONTR_ID is not null
																	JOIN PFR_BASIC.REQ_TRANSFER rt ON st.ID = rt.SI_TR_ID 
																									AND rt.CONTRL_SPN_DATE is not null
																									AND rt.STATUS_ID <> -1
																WHERE
																	o.OPERATION in ('Возврат СПН - расторжение')
																	AND sr.DIR_SPN_ID in (1,3) -- из УК
																	--AND sr.DIR_SPN_ID in (2,4) -- в УК
																	AND rt.CONTRL_SPN_DATE BETWEEN :from AND :to
																GROUP BY
																	st.CONTR_ID	
															) Sk2 ON c.ID = Sk2.ID_CONTRACT
															LEFT JOIN 
															(
																--Коеф 
																SELECT ID_CONTRACT, DETAINED2 AS R, DETAINEDUKREWARD2 as V
																FROM 
																	PFR_BASIC.EDO_ODKF060
																WHERE 
																	QUARTAL = 4
																	AND ID_YEAR = {1} CAST(:to AS DATE) )-2000
															)RV ON c.ID = RV.ID_CONTRACT
															LEFT JOIN
															(
																--Среднее СЧА
																SELECT scha.ID_CONTRACT, AVG(scha.SUM) as SUM
																FROM
																(				
																	SELECT 
																		ID_CONTRACT, REPORTONDATE as DATE, ITOGOSCHA1 as Sum
				
																	FROM 
																		PFR_BASIC.EDO_ODKF010  
																	WHERE 
																		REPORTONDATE  BETWEEN :from AND :to AND ID_CONTRACT is not null					
			
																	UNION
			
																	SELECT 
																		ID_CONTRACT, REPORT_ON_TIME as DATE, A090 as Sum
				
																	FROM 
																		PFR_BASIC.EDO_ODKF015  
																	WHERE 
																		REPORT_ON_TIME BETWEEN :from AND :to AND ID_CONTRACT is not null
																) scha
																GROUP BY scha.ID_CONTRACT
															) D ON c.ID = D.ID_CONTRACT
															LEFT JOIN
															(
																--Отчёт о доходах
																SELECT doh.ID_CONTRACT, doh.MAXUKPAY as SUMMAX, doh.FACTUKPAY as SUMFACT
																FROM
																(				
																	SELECT o.*, RANK() OVER(PARTITION BY o.ID_CONTRACT, o.ID_YEAR ORDER BY o.QUARTAL DESC) as RN
																	FROM 
																		PFR_BASIC.EDO_ODKF070  o
																	WHERE 
																		ID_YEAR = {1} CAST(:to AS DATE) )-2000 AND ID_CONTRACT is not null																			
																	--ORDER BY QUARTAL
																) doh
																WHERE doh.RN = 1
															) F070 ON c.ID = F070.ID_CONTRACT
															LEFT JOIN
															(
																--Отчёт о доходах предидущей год
																SELECT doh.ID_CONTRACT, doh.MAXUKPAY as SUMMAX, doh.FACTUKPAY as SUMFACT, doh.UKREWARD2
																FROM
																(				
																	SELECT o.*, RANK() OVER(PARTITION BY o.ID_CONTRACT, o.ID_YEAR ORDER BY o.QUARTAL DESC) as RN
																	FROM 
																		PFR_BASIC.EDO_ODKF070  o
																	WHERE 
																		ID_YEAR = {1} CAST(:to AS DATE) )-2000-1 AND ID_CONTRACT is not null																			
																	--ORDER BY QUARTAL
																) doh
																WHERE doh.RN = 1
															) F070Old ON c.ID = F070Old.ID_CONTRACT
	
														WHERE c.TYPEID = 1
                                                        AND
                                                        (c.REGADDCLOSE > :from OR
                                                        c.REGADDCLOSE is NULL)
                                                        AND
                                                        (c.REGCLOSE > :from OR
                                                        c.REGCLOSE is NULL)
                                                        AND
                                                        (c.DISSOLDATE > :from OR
                                                        c.DISSOLDATE is NULL)
                                                        AND
                                                        c.REGDATE <= :to
												       ", IsDB2 ? "1 DAY" : "INTERVAL '1' DAY", IsDB2 ? " YEAR( " : " EXTRACT( YEAR FROM ");
                var items = session.CreateSQLQuery(query)
                .AddScalar("ContractNumber", NHibernateUtil.String)
                    .AddScalar("UKName", NHibernateUtil.String)
                    .AddScalar("Portfolio", NHibernateUtil.String)
                    .AddScalar("S0",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("Sm",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("Sn",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("Sc",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    //.AddScalar("K", NHibernateUtil.GuessType(typeof(DB2Decimal4HibType)))
                    .AddScalar("Sk",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("R",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("V",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("F070SumMax",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("F070SumFact",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("F070SumMaxOld",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("F070SumFactOld",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("F070RewardOld",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))



                    .SetParameter<DateTime?>("from", from)
                    .SetParameter<DateTime?>("to", to)
                    //.SetParameter("mult", prompt.Multiplier)
                    //.SetParameter<long>("type", (long)prompt.Type)
                    .SetResultTransformer(Transformers.AliasToBean<ReportInvestRatioListItem>())
                    .List<ReportInvestRatioListItem>();

                #endregion

                foreach (var n in items)
                    n.K = prompt.Multiplier;

                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", (prompt.YearID + 2000).ToString())
                };
                var data = new List<ReportDataSource> { new ReportDataSource("Report", items) };

                var fileName =
                    $"Расчет коэффициента прироста и коэффициента расходов для УК за {prompt.YearID + 2000} год ";
                return RenderReport("Расчет коэффициента прироста и коэффициента расходов для УК.rdlc", fileName, param,
                    data, type);
            }
        }

        public ReportFile CreateReportUKAccounts(List<long> ukIds, ReportFile.Types type = ReportFile.Types.Excel)
        {
            if (!ukIds.Any())
                throw new ArgumentException("Не выбраны УК");

            using (var session = DataAccessSystem.OpenSession())
            {
                var ids = new StringBuilder();
                for (var i = 0; i < ukIds.Count; i++)
                {
                    var com = i + 1 < ukIds.Count ? "," : string.Empty;
                    ids.Append($@"{ukIds[i]}{com}");
                }
                var items =
                    session.CreateSQLQuery(
                        $@"select cast(l.shortname as VARCHAR(256)) SName,
                cast(c.regnum as VARCHAR(256)) RNum,
                cast(((l.inn || '/') || l.okpp) as VARCHAR(256)) InnKppUpr,
                cast(c.formalname as VARCHAR(256)) FName,
               cast(ba.accountnum as VARCHAR(256)) Anum,
                   cast(ba.bank as VARCHAR(256)) Bank,
                   cast(ba.corraccountnum as VARCHAR(256)) CorAccNum,
                   cast(ba.bik as VARCHAR(256)) BIK,
                   cast(((ba.inn || '/') || ba.kpp) as VARCHAR(256)) as InnKppRec
            from  pfr_basic.contract c
			join pfr_basic.legalentity l on l.id = c.le_id
			join pfr_basic.bankaccount ba on ba.id = c.ba_id
            left join pfr_basic.supagreement s on c.id = s.contr_id and s.kind = 'продление договора ду'
           where l.contragentid in ({ids})
           and ((c.DISSOLDATE > :today or c.DISSOLDATE is null) and (c.REGADDCLOSE > :today or c.REGCLOSE > :today) and c.REGDATE <= :today)
                and c.typeid = 1
            group by
              l.shortname, c.regnum, ((l.inn || '/') || l.okpp), c.formalname,
               ba.accountnum,
                   ba.bank,
                   ba.corraccountnum,
                   ba.bik,
                   ((ba.inn || '/') || ba.kpp)

            order by
                   c.formalname asc,
                   c.regnum asc")
                        .AddScalar("SName", NHibernateUtil.String) //help:Имена полей регистрозависимы!
                        .AddScalar("RNum", NHibernateUtil.String)
                        .AddScalar("InnKppUpr", NHibernateUtil.String)
                        .AddScalar("FName", NHibernateUtil.String)
                        .AddScalar("ANum", NHibernateUtil.String)
                        .AddScalar("Bank", NHibernateUtil.String)
                        .AddScalar("CorAccNum", NHibernateUtil.String)
                        .AddScalar("BIK", NHibernateUtil.String)
                        .AddScalar("InnKppRec", NHibernateUtil.String)
                        .SetParameter<DateTime?>("today", DateTime.Now.Date)
                        .SetResultTransformer(Transformers.AliasToBean<UKAccountsReportItem>())
                        .List<UKAccountsReportItem>();
                //Создание и отдача отчёта
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };
                var fileName = $"Список счетов действующих УК на {DateTime.Now:dd.MM.yy}";
                return RenderReport("UKAccountsReport.rdlc", fileName, new List<ReportParameter>(), data, type);
            }
        }

        public ReportFile CreateReportUKList(DateTime reportDate, bool isShowOldNamesUk, ReportFile.Types type)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                #region big query

                var oldQuery = @"SELECT T0.C6 IdUK,
T0.C0 IsOldName,
T0.C4 PName,
T0.C2 FName,
T0.C3 INN,
T0.C5 SName,
T0.C7 PfName,
T0.C8 ContrNum,
T0.C9 ContrStart
FROM
(SELECT min(T12.c30) over(partition BY T12.ID_юрлица, T12.c7, T12.c8, T12.c9, CASE
WHEN T12.c30 = 1 THEN ''
ELSE T12.ИНН
END, Договора_с_УК.c12, Договора_с_УК.c8, Договора_с_УК.Номер, T12.c30) C0,
min(CASE
WHEN T12.c30 = 1 THEN ''
ELSE T12.ИНН
END) over(partition BY T12.ID_юрлица, T12.c7, T12.c8, T12.c9, CASE
WHEN T12.c30 = 1 THEN ''
ELSE T12.ИНН
END, Договора_с_УК.c12, Договора_с_УК.c8, Договора_с_УК.Номер, T12.c30) C1,
T12.c9 C2,
CASE
WHEN T12.c30 = 1 THEN ''
ELSE T12.ИНН
END C3,
T12.c7 C4,
T12.c8 C5,
T12.ID_юрлица C6, Договора_с_УК.c12 C7, Договора_с_УК.Номер C8, Договора_с_УК.c8 C9,
      T12.c30 C10
FROM
(SELECT T.ID ID,
T.ID_юрлица ID_юрлица,
T.ID_контрагента ID_контрагента,
T.Контрагент Контрагент, T.Тип_контрагента Тип_контрагента,
T.c6 c6,
T.c7 c7,
T.c8 c8,
T.c9 c9,
T.ОГРН ОГРН, T.c11 c11,
T.c12 c12,
T.Дата_ликвидации Дата_ликвидации,
T.ИНН ИНН, T.Статус_юрлица Статус_юрлица,
                             0 c30,
                             :today c31
FROM
(SELECT Контрагенты.ID ID,
T2.ID ID_юрлица,
T2.CONTRAGENTID ID_контрагента, Контрагенты.NAME Контрагент, Контрагенты.TYPE Тип_контрагента,
           T3.STATUS c6,
           T2.FULLNAME c7,
           T2.SHORTNAME c8,
           T2.FORMALIZEDNAME c9,
           T2.REGISTRATIONNUM ОГРН, T2.REGISTRATOR c11,
                                    T2.REGISTRATIONDATE c12,
                                    T2.CLOSEDATE Дата_ликвидации,
                                                     T2.INN ИНН, Статус_юрлица.NAME Статус_юрлица
FROM((PFR_BASIC.CONTRAGENT Контрагенты
INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
WHERE Контрагенты.TYPE = 'УК'
OR Контрагенты.TYPE = 'ГУК') T
UNION SELECT T10.ID ID,
T10.ID_юрлица ID_юрлица,
T10.ID_контрагента ID_контрагента,
T10.Контрагент Контрагент, T10.Тип_контрагента Тип_контрагента,
T10.c6 c6,
T2.OLDFULLNAME c7,
T2.OLDSHORTNAME c8,
T2.OLDNICKNAME c9,
T10.ОГРН ОГРН, T10.c11 c11,
T10.c12 c12,
T10.Дата_ликвидации Дата_ликвидации,
          T10.ИНН ИНН, T10.Статус_юрлица Статус_юрлица,
                                               1 c30,
                                               T2.DATECHANGE c31
FROM
(SELECT Контрагенты.ID ID,
T2.ID ID_юрлица,
T2.CONTRAGENTID ID_контрагента, Контрагенты.NAME Контрагент, Контрагенты.TYPE Тип_контрагента,
           T3.STATUS c6,
           T2.FULLNAME c7,
           T2.SHORTNAME c8,
           T2.FORMALIZEDNAME c9,
           T2.REGISTRATIONNUM ОГРН, T2.REGISTRATOR c11,
                                    T2.REGISTRATIONDATE c12,
                                    T2.CLOSEDATE Дата_ликвидации,
                                                     T2.INN ИНН, Статус_юрлица.NAME Статус_юрлица
FROM((PFR_BASIC.CONTRAGENT Контрагенты
INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
WHERE Контрагенты.TYPE = 'УК'
OR Контрагенты.TYPE = 'ГУК') T10,
PFR_BASIC.OLDSINAME T2
WHERE T10.ID_юрлица = T2.LEGALENTITYID) T12
LEFT OUTER JOIN
(SELECT Договора_с_УК.LE_ID ID_юрлица, Договора_с_УК.REGDATE c8, Договора_с_УК.REGADDCLOSE, Договора_с_УК.REGCLOSE, Договора_с_УК.DISSOLDATE, Договора_с_УК.REGNUM Номер, Договора_с_УК.PORTFNAME c12
FROM PFR_BASIC.CONTRACT Договора_с_УК,
PFR_BASIC.CONTRACTNAME T2,
PFR_BASIC.DOCTYPE Тип_документа
WHERE T2.ID = Договора_с_УК.CONT_N_ID
AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК ON T12.ID_юрлица = Договора_с_УК.ID_юрлица
AND T12.c30 = 0
WHERE T12.Дата_ликвидации IS NULL
AND(T12.c30 = 1
OR T12.c30 = 0)
and ((Договора_с_УК.REGADDCLOSE > :today or Договора_с_УК.REGCLOSE > :today or DISSOLDATE > :today ) and Договора_с_УК.c8 <= :today)
) T0
ORDER BY C4 ASC";

                #endregion

                const string newQuery = @"SELECT DISTINCT

  sp2.KIND,
le.ID                                              IdUK,
le.FULLNAME                                        PName,
le.FORMALIZEDNAME                                  FName,
le.SHORTNAME                                       SName,
  le.INN,
c.PORTFNAME                                        PfName,
c.REGNUM                                           ContrNum,
  sp2.NUMBER                                       SupagrNum,
  coalesce(sp2.REGDATE, c.REGDATE)                 ContrStart,
  coalesce(sp2.SUPENDDATE, c.REGCLOSE, DISSOLDATE) ContrEnd,
  0                                                IsOldName,
  le.FORMALIZEDNAME                                OrderName

FROM pfr_basic.contragent cg
  JOIN pfr_basic.legalentity le ON le.CONTRAGENTID = cg.ID
  LEFT JOIN pfr_basic.contract c ON c.LE_ID = le.ID
  LEFT JOIN (SELECT sp2.KIND,sp2.SUPENDDATE,sp2.REGDATE,sp2.NUMBER, c.LE_ID, sp2.CONTR_ID
              FROM
                PFR_BASIC.SUPAGREEMENT sp2
                JOIN PFR_BASIC.CONTRACT c ON c.ID = sp2.CONTR_ID

              WHERE sp2.REGDATE = (SELECT max(sp.REGDATE)
                     FROM PFR_BASIC.CONTRACT ct
                       JOIN PFR_BASIC.SUPAGREEMENT sp ON sp.CONTR_ID = ct.ID
                     WHERE sp.KIND = 'Продление договора ДУ'
                           AND ct.LE_ID = c.LE_ID 
                              AND ct.id = c.ID
                                 GROUP BY ct.LE_ID)) sp2 on sp2.CONTR_ID = c.ID

WHERE c.TYPEID = 1 AND cg.STATUS_ID = 1  and c.STATUS > -1 and (sp2.KIND = 'Продление договора ДУ' or sp2.KIND is NULL )
      AND ((c.DISSOLDATE > :today OR c.DISSOLDATE IS NULL) AND
           c.REGDATE <= :today)

  UNION

SELECT DISTINCT
  sp2.KIND,
le.ID,
  coalesce(sp2.OLDFULLNAME, ''),
  sp2.OLDNICKNAME,
  sp2.OLDSHORTNAME,
  le.INN,
  c.PORTFNAME,
  c.REGNUM,
  sp2.NUMBER,
   sp2.regdate,
  CAST(NULL AS DATE),
  1,
  ''

FROM pfr_basic.contragent cg
  JOIN pfr_basic.legalentity le ON le.CONTRAGENTID = cg.ID

  JOIN pfr_basic.contract c ON c.LE_ID = le.ID
  JOIN (SELECT sp2.KIND,sp2.SUPENDDATE,sp2.REGDATE,sp2.NUMBER, c.LE_ID, OLDNICKNAME, OLDSHORTNAME, OLDFULLNAME, sp2.CONTR_ID
              FROM
                PFR_BASIC.SUPAGREEMENT sp2
                JOIN PFR_BASIC.CONTRACT c ON c.ID = sp2.CONTR_ID
                LEFT JOIN pfr_basic.oldsiname oln ON oln.LEGALENTITYID = c.LE_ID and  oln.AGREEMENT_DATE = sp2.REGDATE
              WHERE sp2.KIND = 'Смена наименования'
                    AND sp2.REGDATE = (SELECT max(sp.REGDATE)
                     FROM PFR_BASIC.CONTRACT ct
                       JOIN PFR_BASIC.SUPAGREEMENT sp ON sp.CONTR_ID = ct.ID
                     WHERE sp.KIND = 'Смена наименования'
                           AND ct.ID = c.ID
                                 GROUP BY ct.LE_ID)) sp2 on sp2.CONTR_ID = c.ID

WHERE c.TYPEID = 1 AND cg.STATUS_ID = 1  and c.STATUS > -1
      AND ((c.DISSOLDATE > :today OR c.DISSOLDATE IS NULL) AND
           c.REGDATE <= :today)


ORDER BY IsOldName ASC, OrderName ASC, SName ASC, PName ASC, PfName ASC";

                var items = session.CreateSQLQuery(newQuery)
                .AddScalar("IdUK", NHibernateUtil.Int32)//help:Имена полей регистрозависимы!                
                .AddScalar("IsOldName", NHibernateUtil.Int32)
                .AddScalar("FName", NHibernateUtil.String)
                .AddScalar("SName", NHibernateUtil.String)
                .AddScalar("INN", NHibernateUtil.String)
                .AddScalar("PName", NHibernateUtil.String)
                .AddScalar("PfName", NHibernateUtil.String)
                .AddScalar("ContrNum", NHibernateUtil.String)
                .AddScalar("SupagrNum", NHibernateUtil.String)
                .AddScalar("ContrStart", NHibernateUtil.DateTime)
                .AddScalar("ContrEnd", NHibernateUtil.DateTime)
                    .SetParameter<DateTime?>("today", reportDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<UKListReportItem>())
                    .List<UKListReportItem>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDateStr", reportDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource>
                {
                    new ReportDataSource("ReportData", items.Where(x => isShowOldNamesUk || x.IsOldName == 0))
                };

                var fileName =
                    $"Перечень УК на {DateTime.Now:dd.MM.yy}{(isShowOldNamesUk ? " (со старыми наименованиями)" : "")}";
                return RenderReport("UKListReport.rdlc", fileName, param, data, type);
            }
        }

        #region Отчеты СПН из Cognos

        public ReportFile CreateReportSPN40(DateTime reportDate, ReportFile.Types type)
        {
            #region Big query

            var query = @"WITH Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME, 
          Договора_с_УК.REGDATE,
          Договора_с_УК.REGNUM,
          Договора_с_УК.TYPEID
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     T AS
  (SELECT rt.ID,
          rt.SUM,
          rt.DATE,
          rt.PAYNUM,
          rt.PAYSUM,
          rt.PAYDATE,
          rt.DATE_CR_REQ,
          rt.NUM_REQ,
          rt.DATE_REQ,
          rt.CONTRL_DATE,
          rt.CONTRL_ADD_SPN_DATE,
          rt.CONTRL_SPN_DATE,
          rt.CONTRL,
          rt.ACT_NUM,
          rt.ACT_DATE,
          rt.STATUS,
          rt.INVESTDOHOD,
          rt.COMMENT,
          rt.SI_TR_ID,
          rt.STARTSUM,
          rt.PF_ID,
          rt.PF_BA_ID,
          rt.DIDATE,
          rt.LOTUS_ID,
          rt.LOTUS_TABLE,
          rt.UNLINKED,
          rt.LOTUS_GUID,
          rt.STATUS_ID,
          rt.KBK_ID,
          rt.ZL_MOVECOUNT,
          rt.ZL_STARTCOUNT,
          rt.CLAIM_NUMBER,
          pp.DRAFT_REGNUM,
          pp.DRAFT_DATE,
          pp.DRAFT_PAYDATE,
          pp.DRAFT_AMOUNT,
          DIRECTION_EL_ID
   FROM PFR_BASIC.REQ_TRANSFER rt
   LEFT OUTER JOIN
     (SELECT t.REQ_TR_ID,
             t.DRAFT_REGNUM,
             t.DRAFT_DATE,
             t.DRAFT_PAYDATE,
             t.DRAFT_AMOUNT,
             t.DIRECTION_EL_ID,
             rank() over (partition BY t.REQ_TR_ID
                          ORDER BY t.DRAFT_PAYDATE DESC) rn,
                    rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                 ORDER BY t.ID DESC) rnID
      FROM PFR_BASIC.ASG_FIN_TR t
      WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
   AND 1 = pp.rn
   AND 1 = pp.rnID
   WHERE rt.STATUS_ID <> -1),
     T7 AS
  (SELECT T.SI_TR_ID ID_перечисления,
          T.DIRECTION_EL_ID Направление,
          T.DRAFT_AMOUNT СПН,
          T.DRAFT_PAYDATE DRAFT_PAYDATE
   FROM T),
     Перечисление AS
  (SELECT Перечисление.ID ID,
          Перечисление.CONTR_ID ID_договора_с_УК
   FROM PFR_BASIC.SI_TRANSFER Перечисление)

SELECT T0.FORMALNAME FName,
       T0.REGNUM RegNum,
       T0.IncomingPay, --C2
       T0.OutgoingPay, --C3
       T0.saldo Saldo,
       sum(T0.IncomingPay) over () IncomingSum,
                      sum(T0.OutgoingPay) over () OutgoingSum,
                                     sum(T0.IncomingPay) over () - sum(T0.OutgoingPay) over () SaldoSum
FROM
  (SELECT Договора_с_УК.FORMALNAME,
          Договора_с_УК.REGNUM,
          sum(CASE
                  WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.REGDATE AND :today
                       AND T7.Направление = '101002' THEN T7.СПН
                  ELSE 0
              END) IncomingPay,
          sum(CASE
                  WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.REGDATE AND :today
                       AND T7.Направление = '101001' THEN T7.СПН
                  ELSE 0
              END) OutgoingPay,
          sum(CASE
                  WHEN (T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.REGDATE AND :today
                        AND T7.Направление = '101002') THEN T7.СПН
                  ELSE 0
              END) - sum(CASE
                             WHEN (T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.REGDATE AND :today
                                   AND T7.Направление = '101001') THEN T7.СПН
                             ELSE 0
                         END) saldo
   FROM Договора_с_УК,
        T7,
        Перечисление
   WHERE Договора_с_УК.TYPEID = 3
     AND Договора_с_УК.ID = Перечисление.ID_договора_с_УК
     AND Перечисление.ID = T7.ID_перечисления
   GROUP BY Договора_с_УК.FORMALNAME,
            Договора_с_УК.REGNUM) T0";

            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String)
                      .AddScalar("IncomingPay",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingPay",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("Saldo",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                      .AddScalar("OutgoingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SaldoSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .SetParameter<DateTime?>("today", reportDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport40Item>())
                    .List<SPNReport40Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDateStr", reportDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };

                var fileName = $"Совокупный ИП ГУК ВР на {DateTime.Now:dd.MM.yy}";
                return RenderReport("SPNReport_40_TotalGukVR.rdlc", fileName, param, data, type);
            }
        }
        public ReportFile CreateReportSPN39(DateTime reportDate, ReportFile.Types type)
        {
            #region Big query

            var query = @"WITH Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     T AS
  (SELECT rt.ID,
          rt.SUM,
          rt.DATE,
          rt.PAYNUM,
          rt.PAYSUM,
          rt.PAYDATE,
          rt.DATE_CR_REQ,
          rt.NUM_REQ,
          rt.DATE_REQ,
          rt.CONTRL_DATE,
          rt.CONTRL_ADD_SPN_DATE,
          rt.CONTRL_SPN_DATE,
          rt.CONTRL,
          rt.ACT_NUM,
          rt.ACT_DATE,
          rt.STATUS,
          rt.INVESTDOHOD,
          rt.COMMENT,
          rt.SI_TR_ID,
          rt.STARTSUM,
          rt.PF_ID,
          rt.PF_BA_ID,
          rt.DIDATE,
          rt.LOTUS_ID,
          rt.LOTUS_TABLE,
          rt.UNLINKED,
          rt.LOTUS_GUID,
          rt.STATUS_ID,
          rt.KBK_ID,
          rt.ZL_MOVECOUNT,
          rt.ZL_STARTCOUNT,
          rt.CLAIM_NUMBER,
          pp.DRAFT_REGNUM,
          pp.DRAFT_DATE,
          pp.DRAFT_PAYDATE,
          pp.DRAFT_AMOUNT,
          DIRECTION_EL_ID
   FROM PFR_BASIC.REQ_TRANSFER rt
   LEFT OUTER JOIN
     (SELECT t.REQ_TR_ID,
             t.DRAFT_REGNUM,
             t.DRAFT_DATE,
             t.DRAFT_PAYDATE,
             t.DRAFT_AMOUNT,
             t.DIRECTION_EL_ID,
             rank() over (partition BY t.REQ_TR_ID
                          ORDER BY t.DRAFT_PAYDATE DESC) rn,
                    rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                 ORDER BY t.ID DESC) rnID
      FROM PFR_BASIC.ASG_FIN_TR t
      WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
   AND 1 = pp.rn
   AND 1 = pp.rnID
   WHERE rt.STATUS_ID <> -1),
     T9 AS
  (SELECT T.SI_TR_ID ID_перечисления,
          T.DIRECTION_EL_ID Направление,
          T.DRAFT_AMOUNT СПН,
          T.DRAFT_PAYDATE DRAFT_PAYDATE
   FROM T),
     Перечисление AS
  (SELECT Перечисление.ID ID,
          Перечисление.CONTR_ID ID_договора_с_УК
   FROM PFR_BASIC.SI_TRANSFER Перечисление),
     Договоры11 AS
  (SELECT Договора_с_УК.c7 c1,
          Договора_с_УК.ID ID,
          Договора_с_УК.Номер C___договора_ДУ,
          sum(CASE
                  WHEN T9.DRAFT_PAYDATE BETWEEN Договора_с_УК.c8 AND :today
                       AND T9.Направление = '101002' THEN T9.СПН
                  ELSE 0
              END) Передано_СПН,
          sum(CASE
                  WHEN T9.DRAFT_PAYDATE BETWEEN Договора_с_УК.c8 AND :today
                       AND T9.Направление = '101001' THEN T9.СПН
                  ELSE 0
              END) Возвращено_СПН
   FROM Договора_с_УК,
        T9,
        Перечисление
   WHERE Договора_с_УК.Тип_портфеля = 3
     AND Договора_с_УК.ID = Перечисление.ID_договора_с_УК
     AND Перечисление.ID = T9.ID_перечисления
   GROUP BY Договора_с_УК.c7,
            Договора_с_УК.ID,
            Договора_с_УК.Номер),
     СЧА12 AS
  (SELECT T0.C0 Отчет_на_дату,
          T0.C1 ID_CONTRACT,
          T0.C2 A090
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             max(T0.C3) over () C3
      FROM
        (SELECT T1.REPORT_ON_TIME C0,
                T1.ID_CONTRACT C1,
                sum(T1.A090) C2,
                max(T1.REPORT_ON_TIME) C3
         FROM PFR_BASIC.EDO_ODKF015 T1
         WHERE T1.REPORT_ON_TIME <= :today
         GROUP BY T1.REPORT_ON_TIME,
                  T1.ID_CONTRACT) T0) T0
   WHERE T0.C0 = T0.C3)
SELECT T0.C0 FName,
       T0.C1 RegNum,
       T0.C2 IncomingPay,
       T0.C3 OutgoingPay,
       T0.C4 Saldo,
       T0.C5 SCHA,
       sum(T0.C2) over () IncomingSum,
                      sum(T0.C3) over () OutgoingSum,
                                     sum(T0.C4) over () SaldoSum,
                                                    T0.C6 SCHASum
FROM
  (SELECT T0.C0 C0,
          T0.C1 C1,
          T0.C2 C2,
          T0.C3 C3,
          T0.C4 C4,
          T0.C5 C5,
          sum(T0.C5) over () C6
   FROM
     (SELECT Договоры11.c1 C0,
             Договоры11.C___договора_ДУ C1,
             sum(Договоры11.Передано_СПН) C2,
             sum(Договоры11.Возвращено_СПН) C3,
             sum(Договоры11.Передано_СПН) - sum(Договоры11.Возвращено_СПН) C4,
             sum(СЧА12.A090) C5
      FROM Договоры11,
           СЧА12
      WHERE Договоры11.ID = СЧА12.ID_CONTRACT
      GROUP BY Договоры11.c1,
               Договоры11.C___договора_ДУ) T0) T0
ORDER BY FName ASC,
         RegNum ASC
";

            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String)
                      .AddScalar("IncomingPay",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingPay",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("Saldo",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                      .AddScalar("OutgoingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SaldoSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SCHASum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SCHA",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .SetParameter<DateTime?>("today", reportDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport39Item>())
                    .List<SPNReport39Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDateStr", reportDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };

                var fileName = $"Совокупный ИП ГУК ВР и СЧА на {DateTime.Now:dd.MM.yy}";
                return RenderReport("SPNReport_39_TotalGukSCHA.rdlc", fileName, param, data, type);
            }
        }
        public ReportFile CreateReportSPN19(DateTime startDate, DateTime endDate, ReportFile.Types type)
        {
            #region big query

            var query = @"WITH Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.DOCDATE Дата_документа,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     T AS
  (SELECT rt.ID,
          rt.SUM,
          rt.DATE,
          rt.PAYNUM,
          rt.PAYSUM,
          rt.PAYDATE,
          rt.DATE_CR_REQ,
          rt.NUM_REQ,
          rt.DATE_REQ,
          rt.CONTRL_DATE,
          rt.CONTRL_ADD_SPN_DATE,
          rt.CONTRL_SPN_DATE,
          rt.CONTRL,
          rt.ACT_NUM,
          rt.ACT_DATE,
          rt.STATUS,
          rt.INVESTDOHOD,
          rt.COMMENT,
          rt.SI_TR_ID,
          rt.STARTSUM,
          rt.PF_ID,
          rt.PF_BA_ID,
          rt.DIDATE,
          rt.LOTUS_ID,
          rt.LOTUS_TABLE,
          rt.UNLINKED,
          rt.LOTUS_GUID,
          rt.STATUS_ID,
          rt.KBK_ID,
          rt.ZL_MOVECOUNT,
          rt.ZL_STARTCOUNT,
          rt.CLAIM_NUMBER,
          pp.DRAFT_REGNUM,
          pp.DRAFT_DATE,
          pp.DRAFT_PAYDATE,
          pp.DRAFT_AMOUNT,
          DIRECTION_EL_ID
   FROM PFR_BASIC.REQ_TRANSFER rt
   LEFT OUTER JOIN
     (SELECT t.REQ_TR_ID,
             t.DRAFT_REGNUM,
             t.DRAFT_DATE,
             t.DRAFT_PAYDATE,
             t.DRAFT_AMOUNT,
             t.DIRECTION_EL_ID,
             rank() over (partition BY t.REQ_TR_ID
                          ORDER BY t.DRAFT_PAYDATE DESC) rn,
                    rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                 ORDER BY t.ID DESC) rnID
      FROM PFR_BASIC.ASG_FIN_TR t
      WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
   AND 1 = pp.rn
   AND 1 = pp.rnID
   WHERE rt.STATUS_ID <> -1),
     T7 AS
  (SELECT T.SI_TR_ID ID_перечисления,
          T.DIRECTION_EL_ID Направление,
          T.DRAFT_AMOUNT СПН,
          T.DRAFT_PAYDATE DRAFT_PAYDATE
   FROM T),
     Перечисление AS
  (SELECT Перечисление.ID ID,
          Перечисление.CONTR_ID ID_договора_с_УК
   FROM PFR_BASIC.SI_TRANSFER Перечисление)
SELECT T0.C0 FName,
       T0.C1 RegNum,
       T0.C2 SPNBegin,
       T0.C3 IncomingPay,
       T0.C4 OutgoingPay,
       T0.C5 SPNEnd,
       T0.C6 IncreaseRub,
       T0.C7 IncreasePerc,
       T0.C8 SPNBeginSum,
       T0.C9 IncomingSum,
       T0.C10 OutgoingSum,
       T0.C11 SPNEndSum,
       T0.C12 IncreaseRubSum,
      ((T0.C12 * 1.0e0) / (case when T0.C8 > 0 then T0.C8 * 1.0e0 else 1 end)) IncreasePercSum
FROM
  (SELECT T0.C0 C0,
          T0.C1 C1,
          T0.C2 C2,
          T0.C3 C3,
          T0.C4 C4,
          T0.C5 C5,
          T0.C6 C6,
          T0.C7 C7,
          sum(T0.C2) over () C8,
                         sum(T0.C3) over () C9,
                                        sum(T0.C4) over () C10,
                                                       (sum(T0.C2) over () + sum(T0.C3) over ()) - sum(T0.C4) over () C11,
                                                                                                                          ((sum(T0.C2) over () + sum(T0.C3) over ()) - sum(T0.C4) over ()) - sum(T0.C2) over () C12,
                                                                                                                                                                                                                        sum(T0.C8) over () C13
   FROM
     (SELECT Договора_с_УК.c7 C0,
             Договора_с_УК.Номер C1,
             sum(CASE
                     WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                     ELSE 0
                 END) C2,
             sum(CASE
                     WHEN T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                          AND T7.Направление = '101002' THEN T7.СПН
                     ELSE 0
                 END) C3,
             sum(CASE
                     WHEN T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                          AND T7.Направление = '101001' THEN T7.СПН
                     ELSE 0
                 END) C4,
             (sum(CASE
                      WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                      ELSE 0
                  END) + sum(CASE
                                 WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                       AND T7.Направление = '101002') THEN T7.СПН
                                 ELSE 0
                             END)) - sum(CASE
                                             WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                                   AND T7.Направление = '101001') THEN T7.СПН
                                             ELSE 0
                                         END) C5,
             ((sum(CASE
                       WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                       ELSE 0
                   END) + sum(CASE
                                  WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                        AND T7.Направление = '101002') THEN T7.СПН
                                  ELSE 0
                              END)) - sum(CASE
                                              WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                                    AND T7.Направление = '101001') THEN T7.СПН
                                              ELSE 0
                                          END)) - sum(CASE
                                                          WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                                                          ELSE 0
                                                      END) C6,
             ((((sum(CASE
                         WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                         ELSE 0
                     END) + sum(CASE
                                    WHEN (T7.DRAFT_PAYDATE BETWEEN  :fromDate AND :endDate
                                          AND T7.Направление = '101002') THEN T7.СПН
                                    ELSE 0
                                END)) - sum(CASE
                                                WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                                      AND T7.Направление = '101001') THEN T7.СПН
                                                ELSE 0
                                            END)) - sum(CASE
                                                            WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                                                            ELSE 0
                                                        END)) * 1.0e0) / nullif(sum(CASE
                                                                                        WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                                                                                        ELSE 1
                                                                                    END), 1) C7,
             sum((CASE
                      WHEN T7.DRAFT_PAYDATE BETWEEN Договора_с_УК.Дата_документа AND :fromDate THEN T7.СПН
                      ELSE 0
                  END + CASE
                            WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :fromDate
                                  AND T7.Направление = '101002') THEN T7.СПН
                            ELSE 0
                        END) - CASE
                                   WHEN (T7.DRAFT_PAYDATE BETWEEN :fromDate AND :endDate
                                         AND T7.Направление = '101001') THEN T7.СПН
                                   ELSE 0
                               END) C8
      FROM Договора_с_УК,
           T7,
           Перечисление
      WHERE Договора_с_УК.Тип_портфеля = 3
        AND Договора_с_УК.ID = Перечисление.ID_договора_с_УК
        AND Перечисление.ID = T7.ID_перечисления
      GROUP BY Договора_с_УК.c7,
               Договора_с_УК.Номер) T0) T0
ORDER BY FName ASC,
         RegNum ASC";

            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!                
                    .AddScalar("SPNBegin",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingPay",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingPay",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SPNEnd",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreaseRub",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreasePerc", NHibernateUtil.Decimal)
                    .AddScalar("SPNBeginSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SPNEndSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreaseRubSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreasePercSum", NHibernateUtil.Decimal)
                    .SetParameter<DateTime?>("fromDate", startDate.Date)
                    .SetParameter<DateTime?>("endDate", endDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport19Item>())
                    .List<SPNReport19Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("StartDateStr", startDate.Date.ToString("dd.MM.yyyy")),
                    new ReportParameter("EndDateStr", endDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource>
                {
                    new ReportDataSource("ReportData", items)
                };

                var fileName =
                    $"Динамика прироста с {startDate.Date:dd.MM.yy} по {endDate.Date:dd.MM.yy}";
                return RenderReport("SPNReport_19_DynamicIncreasing.rdlc", fileName, param, data, type);
            }
        }
        
        public ReportFile CreateReportSPN20(string year, int measure, out List<SPNReport20Item> items, ReportFile.Types type = ReportFile.Types.Excel)
        {
            #region big query

            var query = @"WITH Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.PORTFNAME c12,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     УК_и_договора32 AS
  (SELECT count(T0.C0) over (
                                 ORDER BY T0.C1 ASC , T0.C2 ASC , T0.C3 ASC , T0.C4 ASC , T0.C5 ASC ) C___п_п,
                           T0.C2 ID,
                           T0.C1 c3,
                           T0.C3 c4,
                           T0.C4 c5,
                           T0.C5 Номер
   FROM
     (SELECT min(Договора_с_УК.c7) C0,
             Договора_с_УК.c7 C1,
             Договора_с_УК.ID C2,
             Договора_с_УК.c12 C3,
             Договора_с_УК.c8 C4,
             Договора_с_УК.Номер C5
      FROM Договора_с_УК
      WHERE Договора_с_УК.Тип_портфеля = 3
      GROUP BY Договора_с_УК.ID,
               Договора_с_УК.c7,
               Договора_с_УК.c12,
               Договора_с_УК.c8,
               Договора_с_УК.Номер) T0),
     Доходность_УК AS
  (SELECT Доходность_УК.ID ID_доходности_УК,
          Доходность_УК.ID_YEAR ID_год,
          Доходность_УК.QUARTER Квартал,
          Доходность_УК.PROFITABILITY c6,
          Доходность_УК.PROFITABILITY_3 c7,
          Доходность_УК.PROFITABILITY_12 c8,
          Доходность_УК.CONTRACT_ID ID_договора_с_УК,
          Доходность_УК.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT
   FROM PFR_BASIC.MC_PROFITABILITY Доходность_УК),
     Договора_с_УК30 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.PORTFNAME c12,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Q131 AS
  (SELECT T0.C0 c1,
          T0.C1 Квартал,
          T0.C2 Год,
          T0.C3 c4,
          T0.C4 c5,
          T0.C5 c6,
          T0.C6 PROFITABILITY_CONTRACT,
          sum(T0.C7) over (partition BY T0.C0) count_CONTRACT,
                         T0.C8 ID_доходности_УК,
                         T0.C9 c10
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             T0.C3 C3,
             T0.C4 C4,
             T0.C5 C5,
             T0.C6 C6,
             count(T0.C7) C7,
             T0.C7 C8,
             T0.C8 C9
      FROM
        (SELECT Доходность_УК.ID_договора_с_УК C0,
                Доходность_УК.Квартал C1,
                Годы.NAME C2,
                (Доходность_УК.c6 / :den) C3,
                (Доходность_УК.c7 / :den) C4,
                (Доходность_УК.c8 / :den) C5,
                (Доходность_УК.PROFITABILITY_CONTRACT / :den) C6,
                Доходность_УК.ID_доходности_УК C7,
                max(Доходность_УК.ID_доходности_УК) over (partition BY Доходность_УК.ID_договора_с_УК) C8
         FROM Доходность_УК,
              PFR_BASIC.YEARS Годы,
              Договора_с_УК30
         WHERE Годы.NAME = :year
           AND Доходность_УК.Квартал = 1
           AND Договора_с_УК30.Тип_портфеля = 3
           AND Доходность_УК.ID_договора_с_УК = Договора_с_УК30.ID
           AND Доходность_УК.ID_год = Годы.ID) T0
      GROUP BY T0.C0,
               T0.C1,
               T0.C2,
               T0.C3,
               T0.C4,
               T0.C5,
               T0.C6,
               T0.C7,
               T0.C8) T0),
     T AS
  (SELECT min(Q131.count_CONTRACT) count_CONTRACT,
          Q131.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT,
          Q131.c6 c3,
          Q131.c5 c4,
          Q131.c4 c5,
          Q131.Год Год,
          Q131.Квартал Квартал,
          Q131.c1 c8
   FROM Q131
   WHERE Q131.ID_доходности_УК = Q131.c10
   GROUP BY Q131.PROFITABILITY_CONTRACT,
            Q131.c6,
            Q131.c5,
            Q131.c4,
            Q131.Год,
            Q131.Квартал,
            Q131.c1),
     Запрос138 AS
  (SELECT sum(УК_и_договора32.C___п_п) C___п_п,
          УК_и_договора32.ID ID,
          УК_и_договора32.c3 c3,
          УК_и_договора32.c4 c4,
          УК_и_договора32.c5 c5,
          УК_и_договора32.Номер Номер,
          T.c8 c7,
          T.c5 c8,
          T.c4 c9,
          T.c3 c10,
          T.PROFITABILITY_CONTRACT c11,
          min(T.count_CONTRACT) count_CONTRACT_Q1
   FROM УК_и_договора32
   LEFT OUTER JOIN T ON УК_и_договора32.ID = T.c8
   GROUP BY УК_и_договора32.ID,
            УК_и_договора32.c3,
            УК_и_договора32.c4,
            УК_и_договора32.c5,
            УК_и_договора32.Номер,
            T.c8,
            T.c5,
            T.c4,
            T.c3,
            T.PROFITABILITY_CONTRACT),
     Доходность_УК34 AS
  (SELECT Доходность_УК.ID ID_доходности_УК,
          Доходность_УК.ID_YEAR ID_год,
          Доходность_УК.QUARTER Квартал,
          Доходность_УК.PROFITABILITY c6,
          Доходность_УК.PROFITABILITY_3 c7,
          Доходность_УК.PROFITABILITY_12 c8,
          Доходность_УК.CONTRACT_ID ID_договора_с_УК,
          Доходность_УК.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT
   FROM PFR_BASIC.MC_PROFITABILITY Доходность_УК),
     Договора_с_УК36 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.PORTFNAME c12,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Q237 AS
  (SELECT T0.C0 c1,
          T0.C1 Квартал,
          T0.C2 Год,
          T0.C3 c4,
          T0.C4 c5,
          T0.C5 c6,
          T0.C6 PROFITABILITY_CONTRACT,
          sum(T0.C7) over (partition BY T0.C0) count_CONTRACT,
                         T0.C8 ID_доходности_УК,
                         T0.C9 c10
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             T0.C3 C3,
             T0.C4 C4,
             T0.C5 C5,
             T0.C6 C6,
             count(T0.C7) C7,
             T0.C7 C8,
             T0.C8 C9
      FROM
        (SELECT Доходность_УК34.ID_договора_с_УК C0,
                Доходность_УК34.Квартал C1,
                Годы35.NAME C2,
                (Доходность_УК34.c6 / :den) C3,
                (Доходность_УК34.c7 / :den) C4,
                (Доходность_УК34.c8 / :den) C5,
                (Доходность_УК34.PROFITABILITY_CONTRACT / :den) C6,
                Доходность_УК34.ID_доходности_УК C7,
                max(Доходность_УК34.ID_доходности_УК) over (partition BY Доходность_УК34.ID_договора_с_УК) C8
         FROM Доходность_УК34,
              PFR_BASIC.YEARS Годы35,
              Договора_с_УК36
         WHERE Годы35.NAME = :year
           AND Доходность_УК34.Квартал = 2
           AND Договора_с_УК36.Тип_портфеля = 3
           AND Доходность_УК34.ID_договора_с_УК = Договора_с_УК36.ID
           AND Доходность_УК34.ID_год = Годы35.ID) T0
      GROUP BY T0.C0,
               T0.C1,
               T0.C2,
               T0.C3,
               T0.C4,
               T0.C5,
               T0.C6,
               T0.C7,
               T0.C8) T0),
     T39 AS
  (SELECT Q237.c1 c1,
          Q237.Квартал Квартал,
          Q237.Год Год,
          Q237.c4 c4,
          Q237.c5 c5,
          Q237.c6 c6,
          Q237.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT,
          min(Q237.count_CONTRACT) count_CONTRACT
   FROM Q237
   WHERE Q237.ID_доходности_УК = Q237.c10
   GROUP BY Q237.c1,
            Q237.Квартал,
            Q237.Год,
            Q237.c4,
            Q237.c5,
            Q237.c6,
            Q237.PROFITABILITY_CONTRACT),
     Запрос244 AS
  (SELECT sum(Запрос138.C___п_п) C___п_п,
          Запрос138.ID ID,
          Запрос138.c3 c3,
          Запрос138.c4 c4,
          Запрос138.c5 c5,
          Запрос138.Номер Номер,
          Запрос138.c8 c7,
          Запрос138.c9 c8,
          Запрос138.c10 c9,
          T39.c4 c10,
          T39.c5 c11,
          T39.c6 c12,
          Запрос138.c11 c13,
          T39.PROFITABILITY_CONTRACT c14,
          min(Запрос138.count_CONTRACT_Q1) count_CONTRACT_Q1,
          min(T39.count_CONTRACT) count_CONTRACT_Q2
   FROM Запрос138
   LEFT OUTER JOIN T39 ON Запрос138.ID = T39.c1
   GROUP BY Запрос138.ID,
            Запрос138.c3,
            Запрос138.c4,
            Запрос138.c5,
            Запрос138.Номер,
            Запрос138.c8,
            Запрос138.c9,
            Запрос138.c10,
            T39.c4,
            T39.c5,
            T39.c6,
            Запрос138.c11,
            T39.PROFITABILITY_CONTRACT),
     Доходность_УК40 AS
  (SELECT Доходность_УК.ID ID_доходности_УК,
          Доходность_УК.ID_YEAR ID_год,
          Доходность_УК.QUARTER Квартал,
          Доходность_УК.PROFITABILITY c6,
          Доходность_УК.PROFITABILITY_3 c7,
          Доходность_УК.PROFITABILITY_12 c8,
          Доходность_УК.CONTRACT_ID ID_договора_с_УК,
          Доходность_УК.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT
   FROM PFR_BASIC.MC_PROFITABILITY Доходность_УК),
     Договора_с_УК42 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.PORTFNAME c12,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Q343 AS
  (SELECT T0.C0 c1,
          T0.C1 Квартал,
          T0.C2 Год,
          T0.C3 c4,
          T0.C4 c5,
          T0.C5 c6,
          T0.C6 PROFITABILITY_CONTRACT,
          sum(T0.C7) over (partition BY T0.C0) count_CONTRACT,
                         T0.C8 ID_доходности_УК,
                         T0.C9 c10
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             T0.C3 C3,
             T0.C4 C4,
             T0.C5 C5,
             T0.C6 C6,
             count(T0.C7) C7,
             T0.C7 C8,
             T0.C8 C9
      FROM
        (SELECT Доходность_УК40.ID_договора_с_УК C0,
                Доходность_УК40.Квартал C1,
                Годы41.NAME C2,
                (Доходность_УК40.c6 / :den) C3,
                (Доходность_УК40.c7 / :den) C4,
                (Доходность_УК40.c8 / :den) C5,
                (Доходность_УК40.PROFITABILITY_CONTRACT / :den) C6,
                Доходность_УК40.ID_доходности_УК C7,
                max(Доходность_УК40.ID_доходности_УК) over (partition BY Доходность_УК40.ID_договора_с_УК) C8
         FROM Доходность_УК40,
              PFR_BASIC.YEARS Годы41,
              Договора_с_УК42
         WHERE Годы41.NAME = :year
           AND Доходность_УК40.Квартал = 3
           AND Договора_с_УК42.Тип_портфеля = 3
           AND Доходность_УК40.ID_договора_с_УК = Договора_с_УК42.ID
           AND Доходность_УК40.ID_год = Годы41.ID) T0
      GROUP BY T0.C0,
               T0.C1,
               T0.C2,
               T0.C3,
               T0.C4,
               T0.C5,
               T0.C6,
               T0.C7,
               T0.C8) T0),
     T45 AS
  (SELECT Q343.c1 c1,
          Q343.Квартал Квартал,
          Q343.Год Год,
          Q343.c4 c4,
          Q343.c5 c5,
          Q343.c6 c6,
          Q343.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT,
          min(Q343.count_CONTRACT) count_CONTRACT
   FROM Q343
   WHERE Q343.ID_доходности_УК = Q343.c10
   GROUP BY Q343.c1,
            Q343.Квартал,
            Q343.Год,
            Q343.c4,
            Q343.c5,
            Q343.c6,
            Q343.PROFITABILITY_CONTRACT),
     Запрос350 AS
  (SELECT sum(Запрос244.C___п_п) C___п_п,
          Запрос244.ID ID,
          Запрос244.c3 c3,
          Запрос244.c4 c4,
          Запрос244.c5 c5,
          Запрос244.Номер Номер,
          Запрос244.c7 c7,
          Запрос244.c8 c8,
          Запрос244.c9 c9,
          Запрос244.c10 c10,
          Запрос244.c11 c11,
          Запрос244.c12 c12,
          T45.c4 c13,
          T45.c5 c14,
          T45.c6 c15,
          Запрос244.c13 c16,
          Запрос244.c14 c17,
          T45.PROFITABILITY_CONTRACT c18,
          min(Запрос244.count_CONTRACT_Q1) count_CONTRACT_Q1,
          min(Запрос244.count_CONTRACT_Q2) count_CONTRACT_Q2,
          min(T45.count_CONTRACT) count_CONTRACT_Q3
   FROM Запрос244
   LEFT OUTER JOIN T45 ON Запрос244.ID = T45.c1
   GROUP BY Запрос244.ID,
            Запрос244.c3,
            Запрос244.c4,
            Запрос244.c5,
            Запрос244.Номер,
            Запрос244.c7,
            Запрос244.c8,
            Запрос244.c9,
            Запрос244.c10,
            Запрос244.c11,
            Запрос244.c12,
            T45.c4,
            T45.c5,
            T45.c6,
            Запрос244.c13,
            Запрос244.c14,
            T45.PROFITABILITY_CONTRACT),
     Доходность_УК46 AS
  (SELECT Доходность_УК.ID ID_доходности_УК,
          Доходность_УК.ID_YEAR ID_год,
          Доходность_УК.QUARTER Квартал,
          Доходность_УК.PROFITABILITY c6,
          Доходность_УК.PROFITABILITY_3 c7,
          Доходность_УК.PROFITABILITY_12 c8,
          Доходность_УК.CONTRACT_ID ID_договора_с_УК,
          Доходность_УК.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT
   FROM PFR_BASIC.MC_PROFITABILITY Доходность_УК),
     Договора_с_УК48 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.FORMALNAME c7,
          Договора_с_УК.REGDATE c8,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.PORTFNAME c12,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Q449 AS
  (SELECT T0.C0 c1,
          T0.C1 Квартал,
          T0.C2 Год,
          T0.C3 c4,
          T0.C4 c5,
          T0.C5 c6,
          T0.C6 PROFITABILITY_CONTRACT,
          sum(T0.C7) over (partition BY T0.C0) count_CONTRACT,
                         T0.C8 ID_доходности_УК,
                         T0.C9 c10
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             T0.C3 C3,
             T0.C4 C4,
             T0.C5 C5,
             T0.C6 C6,
             count(T0.C7) C7,
             T0.C7 C8,
             T0.C8 C9
      FROM
        (SELECT Доходность_УК46.ID_договора_с_УК C0,
                Доходность_УК46.Квартал C1,
                Годы47.NAME C2,
                (Доходность_УК46.c6 / :den) C3,
                (Доходность_УК46.c7 / :den) C4,
                (Доходность_УК46.c8 / :den) C5,
                (Доходность_УК46.PROFITABILITY_CONTRACT / :den) C6,
                Доходность_УК46.ID_доходности_УК C7,
                max(Доходность_УК46.ID_доходности_УК) over (partition BY Доходность_УК46.ID_договора_с_УК) C8
         FROM Доходность_УК46,
              PFR_BASIC.YEARS Годы47,
              Договора_с_УК48
         WHERE Годы47.NAME = :year
           AND Доходность_УК46.Квартал = 4
           AND Договора_с_УК48.Тип_портфеля = 3
           AND Доходность_УК46.ID_договора_с_УК = Договора_с_УК48.ID
           AND Доходность_УК46.ID_год = Годы47.ID) T0
      GROUP BY T0.C0,
               T0.C1,
               T0.C2,
               T0.C3,
               T0.C4,
               T0.C5,
               T0.C6,
               T0.C7,
               T0.C8) T0),
     T51 AS
  (SELECT Q449.c1 c1,
          Q449.Квартал Квартал,
          Q449.Год Год,
          Q449.c4 c4,
          Q449.c5 c5,
          Q449.c6 c6,
          Q449.PROFITABILITY_CONTRACT PROFITABILITY_CONTRACT,
          min(Q449.count_CONTRACT) count_CONTRACT
   FROM Q449
   WHERE Q449.ID_доходности_УК = Q449.c10
   GROUP BY Q449.c1,
            Q449.Квартал,
            Q449.Год,
            Q449.c4,
            Q449.c5,
            Q449.c6,
            Q449.PROFITABILITY_CONTRACT)
SELECT Запрос350.c3 FName,
       Запрос350.c4 PfName,
       Запрос350.c5 RegDate,
       Запрос350.Номер RegNum,
       Запрос350.c7 * 1.0e0 StartYieldQ1,
       Запрос350.c9 * 1.0e0 Prev12mYieldQ1,
       Запрос350.c8 * 1.0e0 Prev3yYieldQ1,
       Запрос350.c16 * 1.0e0 AverYieldQ1,
       Запрос350.c10 * 1.0e0 StartYieldQ2,
       Запрос350.c12 * 1.0e0 Prev12mYieldQ2,
       Запрос350.c11 * 1.0e0 Prev3yYieldQ2,
       Запрос350.c17 * 1.0e0 AverYieldQ2,
       Запрос350.c13 * 1.0e0 StartYieldQ3,
       Запрос350.c15 * 1.0e0 Prev12mYieldQ3,
       Запрос350.c14 * 1.0e0 Prev3yYieldQ3,
       Запрос350.c18 * 1.0e0 AverYieldQ3,
       T51.c4 * 1.0e0 StartYieldQ4,
       T51.c6 * 1.0e0 Prev12mYieldQ4,
       T51.c5 * 1.0e0 Prev3yYieldQ4,
       T51.PROFITABILITY_CONTRACT * 1.0e0 AverYieldQ4,
       min(Запрос350.count_CONTRACT_Q3) CountContrQ3,
       min(Запрос350.count_CONTRACT_Q2) CountContrQ2,
       min(Запрос350.count_CONTRACT_Q1) CountContrQ1,
       min(T51.count_CONTRACT) CountContrQ4
FROM Запрос350
LEFT OUTER JOIN T51 ON Запрос350.ID = T51.c1
GROUP BY Запрос350.c3,
         Запрос350.c4,
         Запрос350.c5,
         Запрос350.Номер,
         Запрос350.c7,
         Запрос350.c9,
         Запрос350.c8,
         Запрос350.c10,
         Запрос350.c12,
         Запрос350.c11,
         Запрос350.c13,
         Запрос350.c15,
         Запрос350.c14,
         T51.c4,
         T51.c6,
         T51.c5,
         Запрос350.c16,
         Запрос350.c17,
         Запрос350.c18,
         T51.PROFITABILITY_CONTRACT
ORDER BY FName ASC,
         RegNum ASC";

            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!   
                    .AddScalar("PfName", NHibernateUtil.String)
                    .AddScalar("RegDate", NHibernateUtil.DateTime)
                    .AddScalar("AverYieldQ1", NHibernateUtil.Decimal)
                    .AddScalar("Prev12mYieldQ1", NHibernateUtil.Decimal)
                    .AddScalar("Prev3yYieldQ1", NHibernateUtil.Decimal)
                    .AddScalar("StartYieldQ1", NHibernateUtil.Decimal)
                    .AddScalar("AverYieldQ2", NHibernateUtil.Decimal)
                    .AddScalar("Prev12mYieldQ2", NHibernateUtil.Decimal)
                    .AddScalar("Prev3yYieldQ2", NHibernateUtil.Decimal)
                    .AddScalar("StartYieldQ2", NHibernateUtil.Decimal)
                    .AddScalar("AverYieldQ3", NHibernateUtil.Decimal)
                    .AddScalar("Prev12mYieldQ3", NHibernateUtil.Decimal)
                    .AddScalar("Prev3yYieldQ3", NHibernateUtil.Decimal)
                    .AddScalar("StartYieldQ3", NHibernateUtil.Decimal)
                    .AddScalar("AverYieldQ4", NHibernateUtil.Decimal)
                    .AddScalar("Prev12mYieldQ4", NHibernateUtil.Decimal)
                    .AddScalar("Prev3yYieldQ4", NHibernateUtil.Decimal)
                    .AddScalar("StartYieldQ4", NHibernateUtil.Decimal)
                    .AddScalar("CountContrQ1", NHibernateUtil.Int32)
                    .AddScalar("CountContrQ2", NHibernateUtil.Int32)
                    .AddScalar("CountContrQ3", NHibernateUtil.Int32)
                    .AddScalar("CountContrQ4", NHibernateUtil.Int32)
                    .SetParameter("year", year)
                    .SetParameter("den", measure)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport20Item>())
                    .List<SPNReport20Item>().ToList();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportYear", year)
                };
                var data = new List<ReportDataSource>
                {
                    new ReportDataSource("ReportData", items)
                };

                var fileName =
                    $"Расчет доходности ИС за {year} г { (measure == 1 ? @"(руб.)" : @"(тыс.руб)")}";
                return RenderReport("SPNReport_20_YieldInvestYearly.rdlc", fileName, param, data, type);
            }
        }


        public ReportFile CreateReportSPN02(DateTime startDate, DateTime endDate, ReportFile.Types type)
        {
            #region Big query

            var query = @"SELECT T0.C2 FName,
       T0.C3 RegNum,
       T0.C4 SPNBegin,
       T0.C5 SPNEnd,
       T0.C6 IncreaseRub,
       T0.C7 IncreasePerc,
       T0.C8 IncomingPay,
       T0.C9 OutgoingPay,
       T0.C10 SPNBeginSum,
       T0.C11 IncomingSum,
       T0.C12 OutgoingSum,
       T0.C13 SPNEndSum,
       T0.C14 IncreaseRubSum,
       T0.C15 IncreasePercSum
FROM
  (SELECT T0.C0 C0,
          T0.C1 C1,
          T0.C2 C2,
          T0.C3 C3,
          T0.C4 C4,
          T0.C5 C5,
          T0.C6 C6,
          T0.C7 C7,
          T0.C8 C8,
          T0.C9 C9,
          sum(T0.C10) over () C10,
                          sum(T0.C8) over () C11,
                                         sum(T0.C9) over () C12,
                                                        sum(T0.C11) over () C13,
                                                                        sum(T0.C12) over () C14,
                                                                                        avg(T0.C13) over () C15
   FROM
     (SELECT min(T82.c2 || ' ' || T82.c5) C0,
             T82.c2 C1,
             T82.c3 C2,
             T82.c5 C3,
             T83.c6 C4,
             T83.c7 C5,
             T83.c7 - T83.c6 C6,
             ((T83.c7 - T83.c6) * 1.0e0) / nullif(T83.c6, 0) C7,
             sum(T83.c4) C8,
             sum(T83.c5) C9,
             min(T83.c6) C10,
             min(T83.c7) C11,
             min(T83.c7 - T83.c6) C12,
             min(((T83.c7 - T83.c6) * 1.0e0) / nullif(T83.c6, 0)) C13
      FROM
        (SELECT DISTINCT T80.ID ID,
                         T80.c8 c2,
                         T80.c9 c3,
                         coguda10.ID ID_договора_с_УК,
                         coguda10.REGNUM c5,
                         T80.Тип_контрагента Тип_контрагента
         FROM
           (SELECT Контрагенты.ID ID,
                   T2.ID ID_юрлица,
                   Контрагенты.TYPE Тип_контрагента,
                   T2.SHORTNAME c8,
                   T2.FORMALIZEDNAME c9
            FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                   INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                  INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
            LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
            WHERE Контрагенты.TYPE = 'УК'
              OR Контрагенты.TYPE = 'ГУК') T80,
              PFR_BASIC.CONTRACT coguda10,
              PFR_BASIC.CONTRACTNAME coguda11,
              PFR_BASIC.DOCTYPE coguda12
         WHERE coguda10.TYPEID = 3
           AND T80.ID_юрлица = coguda10.LE_ID
           AND coguda11.ID = coguda10.CONT_N_ID
           AND coguda12.ID = coguda10.DT_ID) T82,

        (SELECT T78.ID_договора_с_УК c1,
                T79.ID_договора_с_УК c2,
                CASE
                    WHEN T78.ID_договора_с_УК IS NULL THEN T79.ID_договора_с_УК
                    ELSE T78.ID_договора_с_УК
                END ID_договора_с_УК,
                sum(CASE
                        WHEN T79.c4 IS NULL THEN 0
                        ELSE T79.c4
                    END) c4,
                sum(CASE
                        WHEN T79.c5 IS NULL THEN 0
                        ELSE T79.c5
                    END) c5,
                CASE
                    WHEN T78.c3 IS NULL THEN 0
                    ELSE T78.c3
                END c6,
                CASE
                    WHEN T78.c2 IS NULL THEN 0
                    ELSE T78.c2
                END c7
         FROM
           (SELECT СПН_из_ПФР_в_УК58.ID_договора_с_УК c1,
                   СПН_из_УК_в_ПФР59.ID_договора_с_УК c2,
                   CASE
                       WHEN СПН_из_УК_в_ПФР59.ID_договора_с_УК IS NULL THEN СПН_из_ПФР_в_УК58.ID_договора_с_УК
                       ELSE СПН_из_УК_в_ПФР59.ID_договора_с_УК
                   END ID_договора_с_УК,
                   sum(CASE
                           WHEN СПН_из_ПФР_в_УК58.c3 IS NULL THEN 0
                           ELSE СПН_из_ПФР_в_УК58.c3
                       END) c4,
                   sum(CASE
                           WHEN СПН_из_УК_в_ПФР59.c4 IS NULL THEN 0
                           ELSE СПН_из_УК_в_ПФР59.c4
                       END) c5
            FROM
              (SELECT T0.C0 ID_договора_с_УК,
                      T0.C1 c2,
                      sum(T0.C2) over (partition BY T0.C0) c3
               FROM
                 (SELECT СПН_из_ПФР_в_УК_152.ID_договора_с_УК C0,
                         СПН_из_ПФР_в_УК_152.c2 C1,
                         sum(СПН_из_ПФР_в_УК_152.c4) C2
                  FROM
                    (SELECT Перечисление.CONTR_ID ID_договора_с_УК,
                            T49.NAME c2,
                            T50.c14 c3,
                            sum(T50.c4) c4
                     FROM PFR_BASIC.SI_TRANSFER Перечисление,
                          PFR_BASIC.DIRECTION_SPN T49,

                       (SELECT rt.SUM c4,
                               rt.INVESTDOHOD c5,
                               rt.CONTRL_SPN_DATE c14,
                               rt.SI_TR_ID ID_перечисления
                        FROM PFR_BASIC.REQ_TRANSFER rt
                        LEFT OUTER JOIN
                          (SELECT t.REQ_TR_ID,
                                  t.DRAFT_REGNUM,
                                  t.DRAFT_DATE,
                                  t.DRAFT_PAYDATE,
                                  t.DRAFT_AMOUNT,
                                  t.DIRECTION_EL_ID,
                                  rank() over (partition BY t.REQ_TR_ID
                                               ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                         rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                      ORDER BY t.ID DESC) rnID
                           FROM PFR_BASIC.ASG_FIN_TR t
                           WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                        AND 1 = pp.rn
                        AND 1 = pp.rnID
                        WHERE rt.STATUS_ID <> -1) T50,
                          PFR_BASIC.SI_REGISTER T51
                     WHERE T49.NAME = 'из ПФР в ГУК ВР'
                       AND NOT T50.c14 IS NULL
                       AND T50.c14 BETWEEN :startDate and :endDate
                       AND T51.ID = Перечисление.SI_REG_ID
                       AND T49.ID = T51.DIR_SPN_ID
                       AND Перечисление.ID = T50.ID_перечисления
                       AND T51.STATUS_ID <> -1
                     GROUP BY Перечисление.CONTR_ID,
                              T49.NAME,
                              T50.c14) СПН_из_ПФР_в_УК_152
                  GROUP BY СПН_из_ПФР_в_УК_152.ID_договора_с_УК,
                           СПН_из_ПФР_в_УК_152.c2) T0) СПН_из_ПФР_в_УК58
            FULL OUTER JOIN
              (SELECT СПН_из_УК_в_ПФР_157.ID_договора_с_УК ID_договора_с_УК,
                      СПН_из_УК_в_ПФР_157.c2 c2,
                      min(СПН_из_УК_в_ПФР_157.c5) c3,
                      sum(СПН_из_УК_в_ПФР_157.c4) c4
               FROM
                 (SELECT Перечисление53.CONTR_ID ID_договора_с_УК,
                         T54.NAME c2,
                         T55.c14 c3,
                         sum(T55.c4) c4,
                         sum(T55.c5) c5
                  FROM PFR_BASIC.SI_TRANSFER Перечисление53,
                       PFR_BASIC.DIRECTION_SPN T54,

                    (SELECT rt.SUM c4,
                            rt.INVESTDOHOD c5,
                            rt.CONTRL_SPN_DATE c14,
                            rt.SI_TR_ID ID_перечисления
                     FROM PFR_BASIC.REQ_TRANSFER rt
                     LEFT OUTER JOIN
                       (SELECT t.REQ_TR_ID,
                               t.DRAFT_REGNUM,
                               t.DRAFT_DATE,
                               t.DRAFT_PAYDATE,
                               t.DRAFT_AMOUNT,
                               t.DIRECTION_EL_ID,
                               rank() over (partition BY t.REQ_TR_ID
                                            ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                      rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                   ORDER BY t.ID DESC) rnID
                        FROM PFR_BASIC.ASG_FIN_TR t
                        WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                     AND 1 = pp.rn
                     AND 1 = pp.rnID
                     WHERE rt.STATUS_ID <> -1) T55,
                       PFR_BASIC.SI_REGISTER T56
                  WHERE T54.NAME = 'из ГУК ВР в ПФР'
                    AND NOT T55.c14 IS NULL
                    AND T55.c14 BETWEEN :startDate and :endDate
                    AND T56.ID = Перечисление53.SI_REG_ID
                    AND T54.ID = T56.DIR_SPN_ID
                    AND Перечисление53.ID = T55.ID_перечисления
                    AND T56.STATUS_ID <> -1
                  GROUP BY Перечисление53.CONTR_ID,
                           T54.NAME,
                           T55.c14) СПН_из_УК_в_ПФР_157
               GROUP BY СПН_из_УК_в_ПФР_157.ID_договора_с_УК,
                        СПН_из_УК_в_ПФР_157.c2) СПН_из_УК_в_ПФР59 ON СПН_из_ПФР_в_УК58.ID_договора_с_УК = СПН_из_УК_в_ПФР59.ID_договора_с_УК
            GROUP BY СПН_из_ПФР_в_УК58.ID_договора_с_УК,
                     СПН_из_УК_в_ПФР59.ID_договора_с_УК,
                     CASE
                         WHEN СПН_из_УК_в_ПФР59.ID_договора_с_УК IS NULL THEN СПН_из_ПФР_в_УК58.ID_договора_с_УК
                         ELSE СПН_из_УК_в_ПФР59.ID_договора_с_УК
                     END) T79
         FULL OUTER JOIN
           (SELECT DISTINCT T76.ID_договора_с_УК ID_договора_с_УК,
                            T77.c2 c2,
                            T76.c2 c3
            FROM
              (SELECT DISTINCT T66.ID_договора_с_УК ID_договора_с_УК,
                               СЧА_на_дату67.СЧА c2
               FROM
                 (SELECT T0.C0 ID_договора_с_УК,
                         T0.C1 СЧА,
                         T0.C2 Дата,
                         min(T0.C3) over (partition BY T0.C0) Дата_начало,
                                        max(T0.C4) over (partition BY T0.C0) Дата_конец
                  FROM
                    (SELECT Union162.ID_договора_с_УК C0,
                            Union162.СЧА C1,
                            Union162.Дата C2,
                            min(Union162.Дата) C3,
                            max(Union162.Дата) C4
                     FROM
                       (SELECT T1.ID_CONTRACT ID_договора_с_УК,
                               sum(T1.ITOGOSCHA1) СЧА,
                               T1.REPORTONDATE Дата
                        FROM PFR_BASIC.EDO_ODKF010 T1
                        WHERE NOT T1.ID_CONTRACT IS NULL
                        GROUP BY T1.ID_CONTRACT,
                                 T1.REPORTONDATE
                        UNION SELECT T1.ID_CONTRACT ID_договора_с_УК,
                                     sum(T1.A090) СЧА,
                                     T1.REPORT_ON_TIME Дата
                        FROM PFR_BASIC.EDO_ODKF015 T1
                        GROUP BY T1.ID_CONTRACT,
                                 T1.REPORT_ON_TIME) Union162
                     WHERE Union162.Дата BETWEEN :startDate and :endDate
                     GROUP BY Union162.ID_договора_с_УК,
                              Union162.СЧА,
                              Union162.Дата) T0) T66,

                 (SELECT DISTINCT Union265.ID_договора_с_УК ID_договора_с_УК,
                                  Union265.СЧА СЧА,
                                  Union265.Дата Дата
                  FROM
                    (SELECT T1.ID_CONTRACT ID_договора_с_УК,
                            sum(T1.ITOGOSCHA1) СЧА,
                            T1.REPORTONDATE Дата
                     FROM PFR_BASIC.EDO_ODKF010 T1
                     WHERE NOT T1.ID_CONTRACT IS NULL
                     GROUP BY T1.ID_CONTRACT,
                              T1.REPORTONDATE
                     UNION SELECT T1.ID_CONTRACT ID_договора_с_УК,
                                  sum(T1.A090) СЧА,
                                  T1.REPORT_ON_TIME Дата
                     FROM PFR_BASIC.EDO_ODKF015 T1
                     GROUP BY T1.ID_CONTRACT,
                              T1.REPORT_ON_TIME) Union265) СЧА_на_дату67
               WHERE T66.ID_договора_с_УК = СЧА_на_дату67.ID_договора_с_УК
                 AND T66.Дата_начало = СЧА_на_дату67.Дата) T76,

              (SELECT DISTINCT T74.ID_договора_с_УК ID_договора_с_УК,
                               СЧА_на_дату75.СЧА c2
               FROM
                 (SELECT T0.C0 ID_договора_с_УК,
                         T0.C1 СЧА,
                         T0.C2 Дата,
                         min(T0.C3) over (partition BY T0.C0) Дата_начало,
                                        max(T0.C4) over (partition BY T0.C0) Дата_конец
                  FROM
                    (SELECT Union170.ID_договора_с_УК C0,
                            Union170.СЧА C1,
                            Union170.Дата C2,
                            min(Union170.Дата) C3,
                            max(Union170.Дата) C4
                     FROM
                       (SELECT T1.ID_CONTRACT ID_договора_с_УК,
                               sum(T1.ITOGOSCHA1) СЧА,
                               T1.REPORTONDATE Дата
                        FROM PFR_BASIC.EDO_ODKF010 T1
                        WHERE NOT T1.ID_CONTRACT IS NULL
                        GROUP BY T1.ID_CONTRACT,
                                 T1.REPORTONDATE
                        UNION SELECT T1.ID_CONTRACT ID_договора_с_УК,
                                     sum(T1.A090) СЧА,
                                     T1.REPORT_ON_TIME Дата
                        FROM PFR_BASIC.EDO_ODKF015 T1
                        GROUP BY T1.ID_CONTRACT,
                                 T1.REPORT_ON_TIME) Union170
                     WHERE Union170.Дата BETWEEN :startDate and :endDate

                     GROUP BY Union170.ID_договора_с_УК,
                              Union170.СЧА,
                              Union170.Дата) T0) T74,

                 (SELECT DISTINCT Union273.ID_договора_с_УК ID_договора_с_УК,
                                  Union273.СЧА СЧА,
                                  Union273.Дата Дата
                  FROM
                    (SELECT T1.ID_CONTRACT ID_договора_с_УК,
                            sum(T1.ITOGOSCHA1) СЧА,
                            T1.REPORTONDATE Дата
                     FROM PFR_BASIC.EDO_ODKF010 T1
                     WHERE NOT T1.ID_CONTRACT IS NULL
                     GROUP BY T1.ID_CONTRACT,
                              T1.REPORTONDATE
                     UNION SELECT T1.ID_CONTRACT ID_договора_с_УК,
                                  sum(T1.A090) СЧА,
                                  T1.REPORT_ON_TIME Дата
                     FROM PFR_BASIC.EDO_ODKF015 T1
                     GROUP BY T1.ID_CONTRACT,
                              T1.REPORT_ON_TIME) Union273) СЧА_на_дату75
               WHERE T74.ID_договора_с_УК = СЧА_на_дату75.ID_договора_с_УК
                 AND T74.Дата_конец = СЧА_на_дату75.Дата) T77
            WHERE T76.ID_договора_с_УК = T77.ID_договора_с_УК) T78 ON T79.ID_договора_с_УК = T78.ID_договора_с_УК
         GROUP BY T78.ID_договора_с_УК,
                  T79.ID_договора_с_УК,
                  CASE
                      WHEN T78.ID_договора_с_УК IS NULL THEN T79.ID_договора_с_УК
                      ELSE T78.ID_договора_с_УК
                  END,
                  CASE
                      WHEN T78.c3 IS NULL THEN 0
                      ELSE T78.c3
                  END,
                  CASE
                      WHEN T78.c2 IS NULL THEN 0
                      ELSE T78.c2
                  END) T83
      WHERE T83.ID_договора_с_УК = T82.ID_договора_с_УК
      GROUP BY T82.c2,
               T82.c3,
               T82.c5,
               T83.c6,
               T83.c7,
               T83.c7 - T83.c6,
               ((T83.c7 - T83.c6) * 1.0e0) / nullif(T83.c6, 0)) T0) T0
                ORDER BY  RegNum";
            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!                
                    .AddScalar("SPNBegin",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingPay",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingPay",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SPNEnd",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreaseRub",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreasePerc", NHibernateUtil.Decimal)
                    .AddScalar("SPNBeginSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncomingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("OutgoingSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SPNEndSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreaseRubSum",
                    NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncreasePercSum", NHibernateUtil.Decimal)
                    .SetParameter<DateTime?>("startDate", startDate.Date)
                    .SetParameter<DateTime?>("endDate", endDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport02Item>())
                    .List<SPNReport02Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("StartDateStr", startDate.Date.ToString("dd.MM.yyyy")),
                    new ReportParameter("EndDateStr", endDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource>
                {
                    new ReportDataSource("ReportData", items)
                };

                var fileName =
                    $"Изменение СЧА с {startDate.Date:dd.MM.yy} по {endDate.Date:dd.MM.yy}";
                return RenderReport("SPNReport_02_SCHAIncreasing.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportSPN03(DateTime startDate, DateTime endDate, ReportFile.Types type)
        {
            #region Big Query

            #region Query1


            var query = @"WITH T18 AS
  (SELECT Контрагенты.ID ID,
          T2.ID ID_юрлица,
          Контрагенты.TYPE Тип_контрагента,
          T2.SHORTNAME c8,
          T2.FORMALIZEDNAME c9
   FROM ((PFR_BASIC.CONTRAGENT Контрагенты
          INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
         INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
   LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
   WHERE Контрагенты.TYPE = 'УК'
     OR Контрагенты.TYPE = 'ГУК'),
     Договора_с_УК19 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     УК_и_договора20 AS
  (SELECT DISTINCT T18.ID ID_УК,
                   T18.c8 c2,
                   T18.c9 c3,
                   Договора_с_УК19.ID ID_договора,
                   Договора_с_УК19.Номер c5,
                   T18.Тип_контрагента Тип_контрагента
   FROM T18,
        Договора_с_УК19
   WHERE Договора_с_УК19.Тип_портфеля = 3
     AND T18.ID_юрлица = Договора_с_УК19.ID_юрлица),
     T AS
  (SELECT T1.ID_CONTRACT c2,
          T1.REPORTONDATE c4,
          T1.ITOGOSCHA1 c49
   FROM PFR_BASIC.EDO_ODKF010 T1),
     Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     T15 AS
  (SELECT T1.REPORT_ON_TIME Отчет_на_дату,
          T1.ID_CONTRACT ID_договора_с_УК,
          T1.A090 c34
   FROM PFR_BASIC.EDO_ODKF015 T1),
     Договора_с_УК16 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Union117(ID_договора_с_УК,
                СЧА,
                Дата) AS
  (SELECT T.c2 ID_договора_с_УК,
          sum(T.c49) СЧА,
          T.c4 Дата
   FROM T,
        Договора_с_УК
   WHERE NOT T.c2 IS NULL
     AND Договора_с_УК.Тип_портфеля = 3
     AND Договора_с_УК.ID = T.c2
   GROUP BY T.c2,
            T.c4
   UNION SELECT T15.ID_договора_с_УК ID_договора_с_УК,
                sum(T15.c34) СЧА,
                T15.Отчет_на_дату Дата
   FROM T15,
        Договора_с_УК16
   WHERE Договора_с_УК16.Тип_портфеля = 3
     AND Договора_с_УК16.ID = T15.ID_договора_с_УК
   GROUP BY T15.ID_договора_с_УК,
            T15.Отчет_на_дату),
     T21 AS
  (SELECT T0.C0 ID_договора_с_УК,
          T0.C1 СЧА,
          T0.C2 Дата,
          (sum(T0.C3) over (partition BY T0.C0)) / nullif(sum(T0.C4) over (partition BY T0.C0), 0) СЧА_среднее
   FROM
     (SELECT Union117.ID_договора_с_УК C0,
             Union117.СЧА C1,
             Union117.Дата C2,
             sum(Union117.СЧА) C3,
             count(Union117.СЧА) C4
      FROM Union117
      WHERE Union117.Дата BETWEEN :startDate and :endDate
      GROUP BY Union117.ID_договора_с_УК,
               Union117.СЧА,
               Union117.Дата) T0)
SELECT T0.C0 FName,
       T0.C1 RegNum,
       T0.C2 / 1.0e0 AvgSCHA,
       (sum(T0.C2) over ()) / 1.0e0 AvgSCHASum
FROM
  (SELECT УК_и_договора20.c3 C0,
          УК_и_договора20.c5 C1,
          avg(T21.СЧА_среднее) C2,
          УК_и_договора20.c2 C3
   FROM УК_и_договора20,
        T21
   WHERE T21.ID_договора_с_УК = УК_и_договора20.ID_договора
   GROUP BY УК_и_договора20.c2,
            УК_и_договора20.c3,
            УК_и_договора20.c5) T0
ORDER BY RegNum
";
            #endregion

            #region Query2

            var query2 = @"
WITH T AS
  (SELECT T1.ID_CONTRACT c2,
          T1.REPORTONDATE c4,
          T1.ITOGOSCHA1 c49
   FROM PFR_BASIC.EDO_ODKF010 T1),
     Договора_с_УК AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     T15 AS
  (SELECT T1.REPORT_ON_TIME Отчет_на_дату,
          T1.ID_CONTRACT ID_договора_с_УК,
          T1.A090 c34
   FROM PFR_BASIC.EDO_ODKF015 T1),
     Договора_с_УК16 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     Union117(ID_договора_с_УК,
                СЧА,
                Дата) AS
  (SELECT T.c2 ID_договора_с_УК,
          sum(T.c49) СЧА,
          T.c4 Дата
   FROM T,
        Договора_с_УК
   WHERE NOT T.c2 IS NULL
     AND Договора_с_УК.Тип_портфеля = 3
     AND Договора_с_УК.ID = T.c2
   GROUP BY T.c2,
            T.c4
   UNION SELECT T15.ID_договора_с_УК ID_договора_с_УК,
                sum(T15.c34) СЧА,
                T15.Отчет_на_дату Дата
   FROM T15,
        Договора_с_УК16
   WHERE Договора_с_УК16.Тип_портфеля = 3
     AND Договора_с_УК16.ID = T15.ID_договора_с_УК
   GROUP BY T15.ID_договора_с_УК,
            T15.Отчет_на_дату),
     T20 AS
  (SELECT T0.C0 ID_договора_с_УК,
          T0.C1 СЧА,
          T0.C2 Дата,
          (sum(T0.C3) over (partition BY T0.C0)) / nullif(sum(T0.C4) over (partition BY T0.C0), 0) СЧА_среднее
   FROM
     (SELECT Union117.ID_договора_с_УК C0,
             Union117.СЧА C1,
             Union117.Дата C2,
             sum(Union117.СЧА) C3,
             count(Union117.СЧА) C4
      FROM Union117
      WHERE Union117.Дата BETWEEN :startDate and :endDate
      GROUP BY Union117.ID_договора_с_УК,
               Union117.СЧА,
               Union117.Дата) T0),
     T18 AS
  (SELECT Контрагенты.ID ID,
          T2.ID ID_юрлица,
          Контрагенты.TYPE Тип_контрагента,
          T2.SHORTNAME c8,
          T2.FORMALIZEDNAME c9
   FROM ((PFR_BASIC.CONTRAGENT Контрагенты
          INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
         INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
   LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
   WHERE Контрагенты.TYPE = 'УК'
     OR Контрагенты.TYPE = 'ГУК'),
     Договора_с_УК19 AS
  (SELECT Договора_с_УК.ID ID,
          Договора_с_УК.LE_ID ID_юрлица,
          Договора_с_УК.REGNUM Номер,
          Договора_с_УК.TYPEID Тип_портфеля
   FROM PFR_BASIC.CONTRACT Договора_с_УК,
        PFR_BASIC.CONTRACTNAME T2,
        PFR_BASIC.DOCTYPE Тип_документа
   WHERE T2.ID = Договора_с_УК.CONT_N_ID
     AND Тип_документа.ID = Договора_с_УК.DT_ID),
     УК_и_договора21 AS
  (SELECT DISTINCT T18.ID ID_УК,
                   T18.c8 c2,
                   T18.c9 c3,
                   Договора_с_УК19.ID ID_договора,
                   Договора_с_УК19.Номер c5,
                   T18.Тип_контрагента Тип_контрагента
   FROM T18,
        Договора_с_УК19
   WHERE Договора_с_УК19.Тип_портфеля = 3
     AND T18.ID_юрлица = Договора_с_УК19.ID_юрлица)
SELECT sum(DISTINCT T20.СЧА_среднее) * 1.0e0 AvgSchaSumGuk
FROM T20,
     УК_и_договора21
WHERE УК_и_договора21.Тип_контрагента <> 'ГУК'
  AND T20.ID_договора_с_УК = УК_и_договора21.ID_договора
HAVING count(*) > 0
";
            #endregion

            #endregion

            List<SPNReport03Item> items;
            using (var session = DataAccessSystem.OpenSession())
            {
                var rpitems = session.CreateSQLQuery(query) //первый подзапрос
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!                
                    .AddScalar("AvgSCHA", NHibernateUtil.Decimal)
                    .AddScalar("AvgSCHASum", NHibernateUtil.Decimal)
                    .SetParameter<DateTime?>("startDate", startDate.Date)
                    .SetParameter<DateTime?>("endDate", endDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport03Item>())
                    .List<SPNReport03Item>();
                items = rpitems.ToList();
            }
            //Создание и отдача отчёта
            using (var session = DataAccessSystem.OpenSession())
            {
                var avgGuk = session.CreateSQLQuery(query2)//второй подзапрос
                    .AddScalar("AvgSchaSumGuk",
                        NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .SetParameter<DateTime?>("startDate", startDate.Date)
                    .SetParameter<DateTime?>("endDate", endDate.Date)
                    .List<decimal?>().FirstOrDefault();
                var param = new List<ReportParameter>
                {
                    new ReportParameter("StartDateStr", startDate.Date.ToString("dd.MM.yyyy")),
                    new ReportParameter("EndDateStr", endDate.Date.ToString("dd.MM.yyyy")),
                    new ReportParameter("AvgSchaSumGuk", avgGuk.HasValue ? avgGuk.Value.ToString("N2") : " ")
                };
                var data = new List<ReportDataSource>
                {
                    new ReportDataSource("ReportData", items)
                };

                var fileName =
                    $"Среднее СЧА с {startDate.Date:dd.MM.yy} по {endDate.Date:dd.MM.yy}";
                return RenderReport("SPNReport_03_AVG_SCHA.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportSPN01(DateTime reportDate, ReportFile.Types type)
        {
            #region Cognos Query

            var query = @"SELECT T0.C2 FName,
       T0.C3 RegNum,
       T0.C4 RSA,
       T0.C5 SPNSended,
       T0.C6 SCHA,
       T0.C7 IncRub1,
       T0.C8 IncPerc1,
       T0.C9 IncRub2,
       T0.C10  IncPerc2 ,
       T0.C11 SPNSendedSum,
       T0.C12 RSASum,
       T0.C13 IncRubSum1,
       (T0.C14 * 1.0e0) / nullif(T0.C15, 0)  IncPercSum1,
       T0.C16 SCHASum,
       T0.C17 IncRubSum2,
       (T0.C18 * 1.0e0)/ nullif(T0.C19, 0) IncPercSum2,
       T0.C20  MinVal,
       T0.C21  MaxVal
FROM
  (SELECT T0.C0 C0,
          T0.C1 C1,
          T0.C2 C2,
          T0.C3 C3,
          T0.C4 C4,
          T0.C5 C5,
          T0.C6 C6,
          T0.C7 C7,
          T0.C8 C8,
          T0.C9 C9,
          T0.C10 C10,
          sum(T0.C5) over () C11,
                         sum(T0.C6) over () C12,
                                        sum(T0.C7) over () C13,
                                                       min(T0.C11) over () C14,
                                                                       min(T0.C12) over () C15,
                                                                                       sum(T0.C13) over () C16,
                                                                                                       sum(T0.C9) over () C17,
                                                                                                                      min(T0.C14) over () C18,
                                                                                                                                      T0.C15 C19,
                                                                                                                                      T0.C16 C20,
                                                                                                                                      min(T0.C17) over () C21
   FROM
     (SELECT T0.C0 C0,
             T0.C1 C1,
             T0.C2 C2,
             T0.C3 C3,
             T0.C4 C4,
             T0.C5 C5,
             T0.C6 C6,
             T0.C7 C7,
             T0.C8 C8,
             T0.C9 C9,
             T0.C10 C10,
             sum(T0.C11) over () C11,
                             sum(T0.C5) over () C12,
                                            T0.C12 C13,
                                            sum(T0.C13) over () C14,
                                                            nullif(sum(T0.C5) over (), 0) C15,
                                                            min(T0.C14) over () C16,
                                                                            max(T0.C15) over () C17
      FROM
        (SELECT min(T58.ID_договора_с_УК) C0,
                УК_и_договора59.c1 C1,
                УК_и_договора59.c2 C2,
                УК_и_договора59.Номер C3,
                T58.СЧА C4,
                sum(T58.СПН_переданные) C5,
                sum(T58.РСА) C6,
                sum(T58.РСА) - sum(T58.СПН_переданные) C7,
                ((sum(T58.РСА) - sum(T58.СПН_переданные)) * 1.0e0) / nullif(sum(T58.СПН_переданные), 0) C8,
                min(T58.СЧА) - sum(T58.СПН_переданные) C9,
                ((min(T58.СЧА) - sum(T58.СПН_переданные)) * 1.0e0) / nullif(sum(T58.СПН_переданные), 0) C10,
                sum(T58.РСА - T58.СПН_переданные) C11,
                min(T58.СЧА) C12,
                sum(T58.СЧА - T58.СПН_переданные) C13,
                min(((T58.СЧА - T58.СПН_переданные) * 1.0e0) / nullif(T58.СПН_переданные, 0)) C14,
                max(((T58.СЧА - T58.СПН_переданные) * 1.0e0) / nullif(T58.СПН_переданные, 0)) C15
         FROM
           (SELECT CASE
                       WHEN РСА_на_дату54.ID_договора_с_УК IS NULL THEN T55.ID_договора_с_УК
                       ELSE РСА_на_дату54.ID_договора_с_УК
                   END ID_договора_с_УК,
                   sum(T55.СПН_переданные) СПН_переданные,
                   T55.СЧА СЧА,
                   sum(CASE
                           WHEN РСА_на_дату54.c1 IS NULL THEN 0
                           ELSE РСА_на_дату54.c1
                       END) РСА
            FROM
              (SELECT CASE
                          WHEN СЧА_на_дату46.ID_договора_с_УК IS NULL THEN T47.ID_договора_с_УК
                          ELSE СЧА_на_дату46.ID_договора_с_УК
                      END ID_договора_с_УК,
                      sum(T47.СПН_переданные) СПН_переданные,
                      CASE
                          WHEN СЧА_на_дату46.СЧА IS NULL THEN 0
                          ELSE СЧА_на_дату46.СЧА
                      END СЧА
               FROM
                 (SELECT T0.C0 ID_договора_с_УК,
                         sum((T0.C2 - T0.C3) * T0.C5) СПН_переданные
                  FROM
                    (SELECT Перечисление.CONTR_ID C0,
                            T40.NAME C1,
                            sum(CASE
                                    WHEN T41.c3 IS NULL THEN T41.c4
                                    ELSE T41.c3
                                END) C2,
                            sum(CASE
                                    WHEN T41.c5 IS NULL THEN 0
                                    ELSE T41.c5
                                END) C3,
                            T41.c14 C4,
                            CASE
                                WHEN T40.NAME = 'из ПФР в УК' THEN cast(1 AS integer)
                                ELSE cast(-1 AS integer)
                            END C5,
                            (sum(CASE
                                     WHEN T41.c3 IS NULL THEN T41.c4
                                     ELSE T41.c3
                                 END) - sum(CASE
                                                WHEN T41.c5 IS NULL THEN 0
                                                ELSE T41.c5
                                            END)) * min(CASE
                                                            WHEN (T40.NAME = 'из ПФР в УК') THEN cast(1 AS integer)
                                                            ELSE cast(-1 AS integer)
                                                        END) C6,
                            T42.c9 C7
                     FROM PFR_BASIC.SI_TRANSFER Перечисление,
                          PFR_BASIC.DIRECTION_SPN T40,

                       (SELECT rt.SUM c3,
                               rt.SUM c4,
                               rt.INVESTDOHOD c5,
                               rt.CONTRL_SPN_DATE c14,
                               rt.SI_TR_ID ID_перечисления
                        FROM PFR_BASIC.REQ_TRANSFER rt
                        LEFT OUTER JOIN
                          (SELECT t.REQ_TR_ID,
                                  t.DRAFT_REGNUM,
                                  t.DRAFT_DATE,
                                  t.DRAFT_PAYDATE,
                                  t.DRAFT_AMOUNT,
                                  t.DIRECTION_EL_ID,
                                  rank() over (partition BY t.REQ_TR_ID
                                               ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                         rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                      ORDER BY t.ID DESC) rnID
                           FROM PFR_BASIC.ASG_FIN_TR t
                           WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                        AND 1 = pp.rn
                        AND 1 = pp.rnID
                        WHERE rt.STATUS_ID <> -1) T41,

                       (SELECT T2.ID ID_юрлица,
                               Контрагенты.TYPE Тип_контрагента,
                               T2.SHORTNAME c8,
                               T2.FORMALIZEDNAME c9
                        FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                               INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                              INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                        LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                        WHERE Контрагенты.TYPE = 'УК'
                          OR Контрагенты.TYPE = 'ГУК') T42,
                          PFR_BASIC.CONTRACT coguda40,
                          PFR_BASIC.CONTRACTNAME coguda41,
                          PFR_BASIC.DOCTYPE coguda42,
                          PFR_BASIC.SI_REGISTER T44
                     WHERE NOT T41.c14 IS NULL
                       AND T41.c14 <= :today
                       AND coguda40.TYPEID = 3
                       AND T44.ID = Перечисление.SI_REG_ID
                       AND T40.ID = T44.DIR_SPN_ID
                       AND Перечисление.ID = T41.ID_перечисления
                       AND coguda40.ID = Перечисление.CONTR_ID
                       AND T42.ID_юрлица = coguda40.LE_ID
                       AND T44.STATUS_ID <> -1
                       AND coguda41.ID = coguda40.CONT_N_ID
                       AND coguda42.ID = coguda40.DT_ID
                     GROUP BY Перечисление.CONTR_ID,
                              T40.NAME,
                              T41.c14,
                              CASE
                                  WHEN T40.NAME = 'из ПФР в УК' THEN cast(1 AS integer)
                                  ELSE cast(-1 AS integer)
                              END,
                              T42.c9) T0
                  GROUP BY T0.C0) T47
               LEFT OUTER JOIN
                 (SELECT T0.C0 ID_договора_с_УК,
                         T0.C1 СЧА,
                         T0.C2 Дата,
                         max(T0.C3) over (partition BY T0.C0) Дата_отчета
                  FROM
                    (SELECT T0.C0 C0,
                            T0.C1 C1,
                            T0.C2 C2,
                            max(T0.C2) C3
                     FROM
                       (SELECT T0.C0 C0,
                               T0.C1 C1,
                               T0.C2 C2,
                               T0.C3 C3,
                               max(T0.C4) over (partition BY T0.C0) C4
                        FROM
                          (SELECT Union138.ID_договора_с_УК C0,
                                  Union138.СЧА C1,
                                  Union138.Дата C2,
                                  min(Union138.Дата) C3,
                                  max(Union138.Дата) C4
                           FROM
                             (SELECT T34.ID_CONTRACT ID_договора_с_УК,
                                     sum(T34.ITOGOSCHA1) СЧА,
                                     T34.REPORTONDATE Дата
                              FROM PFR_BASIC.EDO_ODKF010 T34,
                                   PFR_BASIC.CONTRACT coguda10,
                                   PFR_BASIC.CONTRACTNAME coguda11,
                                   PFR_BASIC.DOCTYPE coguda12
                              WHERE NOT T34.ID_CONTRACT IS NULL
                                AND coguda10.TYPEID = 3
                                AND coguda10.ID = T34.ID_CONTRACT
                                AND coguda11.ID = coguda10.CONT_N_ID
                                AND coguda12.ID = coguda10.DT_ID
                              GROUP BY T34.ID_CONTRACT,
                                       T34.REPORTONDATE
                              UNION SELECT T36.ID_CONTRACT ID_договора_с_УК,
                                           sum(T36.A090) СЧА,
                                           T36.REPORT_ON_TIME Дата
                              FROM PFR_BASIC.EDO_ODKF015 T36,
                                   PFR_BASIC.CONTRACT coguda10,
                                   PFR_BASIC.CONTRACTNAME coguda11,
                                   PFR_BASIC.DOCTYPE coguda12
                              WHERE NOT T36.ID_CONTRACT IS NULL
                                AND coguda10.TYPEID = 3
                                AND coguda10.ID = T36.ID_CONTRACT
                                AND coguda11.ID = coguda10.CONT_N_ID
                                AND coguda12.ID = coguda10.DT_ID
                              GROUP BY T36.ID_CONTRACT,
                                       T36.REPORT_ON_TIME) Union138
                           WHERE Union138.Дата <= :today
                           GROUP BY Union138.ID_договора_с_УК,
                                    Union138.СЧА,
                                    Union138.Дата) T0) T0
                     WHERE T0.C3 = T0.C4
                     GROUP BY T0.C0,
                              T0.C1,
                              T0.C2) T0) СЧА_на_дату46 ON T47.ID_договора_с_УК = СЧА_на_дату46.ID_договора_с_УК
               GROUP BY CASE
                            WHEN СЧА_на_дату46.ID_договора_с_УК IS NULL THEN T47.ID_договора_с_УК
                            ELSE СЧА_на_дату46.ID_договора_с_УК
                        END,
                        CASE
                            WHEN СЧА_на_дату46.СЧА IS NULL THEN 0
                            ELSE СЧА_на_дату46.СЧА
                        END) T55
            LEFT OUTER JOIN
              (SELECT sum(T0.C1) c1,
                      T0.C0 ID_договора_с_УК
               FROM
                 (SELECT T0.C0 C0,
                         T0.C1 C1,
                         T0.C2 C2,
                         max(T0.C2) C3
                  FROM
                    (SELECT T0.C0 C0,
                            T0.C1 C1,
                            T0.C2 C2,
                            T0.C3 C3,
                            max(T0.C4) over (partition BY T0.C0) C4
                     FROM
                       (SELECT Union252.ID_договора_с_УК C0,
                               Union252.РСА C1,
                               Union252.Дата C2,
                               min(Union252.Дата) C3,
                               max(Union252.Дата) C4
                        FROM
                          (SELECT T48.ID_CONTRACT ID_договора_с_УК,
                                  sum(T48.SUMACTIV1) РСА,
                                  T48.ONDATE Дата
                           FROM PFR_BASIC.EDO_ODKF020 T48,
                                PFR_BASIC.CONTRACT coguda10,
                                PFR_BASIC.CONTRACTNAME coguda11,
                                PFR_BASIC.DOCTYPE coguda12
                           WHERE NOT T48.ID_CONTRACT IS NULL
                             AND coguda10.TYPEID = 3
                             AND coguda10.ID = T48.ID_CONTRACT
                             AND coguda11.ID = coguda10.CONT_N_ID
                             AND coguda12.ID = coguda10.DT_ID
                           GROUP BY T48.ID_CONTRACT,
                                    T48.ONDATE
                           UNION SELECT T50.ID_CONTRACT ID_договора_с_УК,
                                        sum(T50.TOTAL_AMOUNT) РСА,
                                        T50.REPORT_ON_DATE Дата
                           FROM PFR_BASIC.EDO_ODKF025 T50,
                                PFR_BASIC.CONTRACT coguda10,
                                PFR_BASIC.CONTRACTNAME coguda11,
                                PFR_BASIC.DOCTYPE coguda12
                           WHERE NOT T50.ID_CONTRACT IS NULL
                             AND coguda10.TYPEID = 3
                             AND coguda10.ID = T50.ID_CONTRACT
                             AND coguda11.ID = coguda10.CONT_N_ID
                             AND coguda12.ID = coguda10.DT_ID
                           GROUP BY T50.ID_CONTRACT,
                                    T50.REPORT_ON_DATE) Union252
                        WHERE Union252.Дата <= :today
                        GROUP BY Union252.ID_договора_с_УК,
                                 Union252.РСА,
                                 Union252.Дата) T0) T0
                  WHERE T0.C3 = T0.C4
                  GROUP BY T0.C0,
                           T0.C1,
                           T0.C2) T0
               GROUP BY T0.C0) РСА_на_дату54 ON T55.ID_договора_с_УК = РСА_на_дату54.ID_договора_с_УК
            GROUP BY CASE
                         WHEN РСА_на_дату54.ID_договора_с_УК IS NULL THEN T55.ID_договора_с_УК
                         ELSE РСА_на_дату54.ID_договора_с_УК
                     END,
                     T55.СЧА) T58,

           (SELECT DISTINCT T56.c8 c1,
                            T56.c9 c2,
                            coguda10.ID ID_договора_с_УК,
                            coguda10.REGNUM Номер,
                            T56.Тип_контрагента Тип_контрагента
            FROM
              (SELECT T2.ID ID_юрлица,
                      Контрагенты.TYPE Тип_контрагента,
                      T2.SHORTNAME c8,
                      T2.FORMALIZEDNAME c9
               FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                      INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                     INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
               LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
               WHERE Контрагенты.TYPE = 'УК'
                 OR Контрагенты.TYPE = 'ГУК') T56,
                 PFR_BASIC.CONTRACT coguda10,
                 PFR_BASIC.CONTRACTNAME coguda11,
                 PFR_BASIC.DOCTYPE coguda12
            WHERE coguda10.TYPEID = 3
              AND T56.ID_юрлица = coguda10.LE_ID
              AND coguda11.ID = coguda10.CONT_N_ID
              AND coguda12.ID = coguda10.DT_ID) УК_и_договора59
         WHERE T58.ID_договора_с_УК = УК_и_договора59.ID_договора_с_УК
         GROUP BY УК_и_договора59.c1,
                  УК_и_договора59.c2,
                  УК_и_договора59.Номер,
                  T58.СЧА) T0) T0) T0";

            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!  
                    .AddScalar("RSA", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SPNSended", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SCHA", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncRub1", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncPerc1", NHibernateUtil.Decimal)
                    .AddScalar("IncRub2", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncPerc2", NHibernateUtil.Decimal)
                    .AddScalar("SPNSendedSum", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("RSASum", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncRubSum1", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncPercSum1", NHibernateUtil.Decimal)
                    .AddScalar("SCHASum", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncRubSum2", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("IncPercSum2", NHibernateUtil.Decimal)
                    .AddScalar("MinVal", NHibernateUtil.Decimal)
                    .AddScalar("MaxVal", NHibernateUtil.Decimal)
                    .SetParameter<DateTime?>("today", reportDate.Date)
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport01Item>())
                    .List<SPNReport01Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                    new ReportParameter("ReportDateStr", reportDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };

                var fileName = $"СЧА-РСА-дата по ИП ГУК ВР на {DateTime.Now:dd.MM.yy}";
                return RenderReport("SPNReport_01_SCHA_RSA_GUK.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportSPN18(DateTime startDate, DateTime endDate, bool showOldContracts, ReportFile.Types type)
        {
            #region Cognos query

            var query = string.Format(@"SELECT T0.C0 FName,
       T0.C2 RegNum,
       T0.C3 RegDate,
       T0.C1 RegNum2,
       T0.C4 SaldoStart,
       T0.C5 SendedDUTotal,
       T0.C6 SendedDUDsv,
       T0.C7 SendedDUMsk,
       T0.C8 ReturnDUTotalTotal,
       T0.C9 ReturnDUTotalYield,
       T0.C10 ReturnDUPravTotal,
       T0.C11 ReturnDUPravYield,
       T0.C12 ReturnDUDsvTotal,
       T0.C13 ReturnDUDsvYield,
       T0.C14 PeretokTotal,
       T0.C15 PeretokYield,
       T0.C16 ReturnDUMskTotal,
       T0.C17 ReturnDUMskYield,
       T0.C18 SendedDUSum,
        nullif(T0.C4, 0) + T0.C5 SaldoEnd
FROM
  (SELECT Итого222.c2 C0,
          min(Итого222.Номер_договора) over (partition BY Итого222.c2, Итого222.Номер_договора, Итого222.Дата_договора_ДУ) C1,
                                           Итого222.Номер_договора C2,
                                           Итого222.Дата_договора_ДУ C3,
                                           T223.c7 C4,
                                           Итого222.передано_всего C5,
                                           Итого222.c7 C6,
                                           Итого222.c8 C7,
                                           Итого222.c10 C8,
                                           Итого222.c11 C9,
                                           Итого222.c14 C10,
                                           Итого222.c15 C11,
                                           Итого222.c12 C12,
                                           Итого222.c13 C13,
                                           Итого222.c16 C14,
                                           Итого222.c17 C15,
                                           Итого222.c19 C16,
                                           Итого222.c20 C17,
                                           Итого222.сальдо C18
   FROM
     (SELECT count(спн___дсв202.Номер_договора) over (
                                                          ORDER BY спн___дсв202.c2 ASC , спн___дсв202.Тип_контрагента ASC , спн___дсв202.Номер_договора ASC , спн___дсв202.Дата_договора_ДУ ASC , спн___дсв202.c20 ASC , спн___дсв202.Расторженные ASC ROWS unbounded preceding) C__,
                                                    спн___дсв202.c2 c2,
                                                    спн___дсв202.Тип_контрагента Тип_контрагента,
                                                    спн___дсв202.Номер_договора Номер_договора,
                                                    спн___дсв202.Дата_договора_ДУ Дата_договора_ДУ,
                                                    спн___дсв202.передано_всего передано_всего,
                                                    спн___дсв202.c7 c7,
                                                    CASE
                                                        WHEN T203.c4 IS NULL THEN 0
                                                        ELSE T203.c4
                                                    END c8,
                                                    спн___дсв202.c8 c9,
                                                    спн___дсв202.c9 c10,
                                                    спн___дсв202.c10 c11,
                                                    спн___дсв202.c11 c12,
                                                    спн___дсв202.c12 c13,
                                                    спн___дсв202.c13 c14,
                                                    спн___дсв202.c14 c15,
                                                    спн___дсв202.c15 c16,
                                                    спн___дсв202.c16 c17,
                                                    спн___дсв202.сальдо сальдо,
                                                    спн___дсв202.c18 c19,
                                                    спн___дсв202.c19 c20,
                                                    спн___дсв202.c20 c21,
                                                    спн___дсв202.Расторженные Расторженные
      FROM
        (SELECT count(T0.C0) over (
                                       ORDER BY T0.C1 ASC , T0.C2 ASC , T0.C3 ASC , T0.C4 ASC , T0.C5 ASC , T0.C6 ASC ROWS unbounded preceding) C__,
                                 T0.C1 c2,
                                 T0.C2 Тип_контрагента,
                                 T0.C3 Номер_договора,
                                 T0.C4 Дата_договора_ДУ,
                                 T0.C7 передано_всего,
                                 T0.C8 c7,
                                 T0.C9 c8,
                                 T0.C10 c9,
                                 T0.C11 c10,
                                 T0.C12 c11,
                                 T0.C13 c12,
                                 T0.C14 c13,
                                 T0.C15 c14,
                                 T0.C16 c15,
                                 T0.C17 c16,
                                 T0.C18 сальдо,
                                 T0.C19 c18,
                                 T0.C20 c19,
                                 T0.C5 c20,
                                 T0.C6 Расторженные
         FROM
           (SELECT min(Запрос_5192.Номер_договора) C0,
                   Запрос_5192.c1 C1,
                   Запрос_5192.Тип_контрагента C2,
                   Запрос_5192.Номер_договора C3,
                   Запрос_5192.Дата_договора_ДУ C4,
                   Запрос_5192.c21 C5,
                   Запрос_5192.Расторженные C6,
                   sum(Запрос_5192.передано_всего) C7,
                   sum(CASE
                           WHEN T193.c4 IS NULL THEN 0
                           ELSE T193.c4
                       END) C8,
                   sum(Запрос_5192.c6) C9,
                   sum(Запрос_5192.c7) C10,
                   sum(Запрос_5192.c8) C11,
                   sum(Запрос_5192.c9) C12,
                   sum(Запрос_5192.c10) C13,
                   sum(Запрос_5192.c11) C14,
                   sum(Запрос_5192.c12) C15,
                   sum(Запрос_5192.c13) C16,
                   sum(Запрос_5192.c14) C17,
                   sum(Запрос_5192.сальдо) C18,
                   sum(Запрос_5192.c19) C19,
                   sum(Запрос_5192.c20) C20
            FROM
              (SELECT Запрос_4182.c1 c1,
                      Запрос_4182.Тип_контрагента Тип_контрагента,
                      Запрос_4182.Номер_договора Номер_договора,
                      Запрос_4182.Дата_договора_ДУ Дата_договора_ДУ,
                      sum(Запрос_4182.передано_всего) передано_всего,
                      sum(Запрос_4182.c6) c6,
                      sum(Запрос_4182.c7) c7,
                      sum(Запрос_4182.c8) c8,
                      sum(Запрос_4182.c9) c9,
                      sum(Запрос_4182.c10) c10,
                      sum(Запрос_4182.c11) c11,
                      sum(Запрос_4182.c12) c12,
                      sum(Запрос_4182.c13) c13,
                      sum(Запрос_4182.c14) c14,
                      sum(Запрос_4182.сальдо) сальдо,
                      T183.c1 c16,
                      T183.c2 c17,
                      T183.Номер Номер,
                      sum(CASE
                              WHEN T183.c4 IS NULL THEN 0
                              ELSE T183.c4
                          END) c19,
                      sum(CASE
                              WHEN T183.c5 IS NULL THEN 0
                              ELSE T183.c5
                          END) c20,
                      Запрос_4182.c16 c21,
                      Запрос_4182.Расторженные Расторженные
               FROM
                 (SELECT Запрос3172.c1 c1,
                         Запрос3172.Тип_контрагента Тип_контрагента,
                         Запрос3172.Номер_договора Номер_договора,
                         Запрос3172.Дата_договора_ДУ Дата_договора_ДУ,
                         Запрос3172.передано_всего передано_всего,
                         Запрос3172.c6 c6,
                         Запрос3172.c7 c7,
                         Запрос3172.c8 c8,
                         Запрос3172.c9 c9,
                         Запрос3172.c10 c10,
                         Запрос3172.c11 c11,
                         Запрос3172.c12 c12,
                         CASE
                             WHEN T173.c4 IS NULL THEN 0
                             ELSE T173.c4
                         END c13,
                         CASE
                             WHEN T173.c5 IS NULL THEN 0
                             ELSE T173.c5
                         END c14,
                         (Запрос3172.передано_всего - Запрос3172.c7) + Запрос3172.c8 сальдо,
                         Запрос3172.c13 c16,
                         Запрос3172.Расторженные Расторженные
                  FROM
                    (SELECT Запрос2162.c1 c1,
                            Запрос2162.Тип_контрагента Тип_контрагента,
                            Запрос2162.Номер_договора Номер_договора,
                            Запрос2162.Дата_договора_ДУ Дата_договора_ДУ,
                            Запрос2162.передано_всего передано_всего,
                            Запрос2162.c6 c6,
                            Запрос2162.c7 c7,
                            Запрос2162.c8 c8,
                            Запрос2162.c9 c9,
                            Запрос2162.c10 c10,
                            CASE
                                WHEN T163.c4 IS NULL THEN 0
                                ELSE T163.c4
                            END c11,
                            CASE
                                WHEN T163.c5 IS NULL THEN 0
                                ELSE T163.c5
                            END c12,
                            Запрос2162.c11 c13,
                            Запрос2162.Расторженные Расторженные
                     FROM
                       (SELECT Запрос1152.c1 c1,
                               Запрос1152.Тип_контрагента Тип_контрагента,
                               Запрос1152.Номер_договора Номер_договора,
                               Запрос1152.Дата_договора_ДУ Дата_договора_ДУ,
                               Запрос1152.передано_всего передано_всего,
                               Запрос1152.c6 c6,
                               Запрос1152.c7 c7,
                               Запрос1152.c8 c8,
                               CASE
                                   WHEN T153.c4 IS NULL THEN 0
                                   ELSE T153.c4
                               END c9,
                               CASE
                                   WHEN T153.c5 IS NULL THEN 0
                                   ELSE T153.c5
                               END c10,
                               Запрос1152.c9 c11,
                               Запрос1152.Расторженные Расторженные
                        FROM
                          (SELECT CASE
                                      WHEN T142.c1 IS NULL THEN T143.c1
                                      ELSE T142.c1
                                  END c1,
                                  CASE
                                      WHEN T142.Тип_контрагента IS NULL THEN T143.Тип_контрагента
                                      ELSE T142.Тип_контрагента
                                  END Тип_контрагента,
                                  CASE
                                      WHEN T142.Номер_договора IS NULL THEN T143.Номер_договора
                                      ELSE T142.Номер_договора
                                  END Номер_договора,
                                  CASE
                                      WHEN T142.Дата_договора_ДУ IS NULL THEN T143.Дата_договора_ДУ
                                      ELSE T142.Дата_договора_ДУ
                                  END Дата_договора_ДУ,
                                  sum(CASE
                                          WHEN T142.c5 IS NULL THEN 0
                                          ELSE T142.c5
                                      END) передано_всего,
                                  sum(CASE
                                          WHEN T142.c6 IS NULL THEN 0
                                          ELSE T142.c6
                                      END) c6,
                                  sum(CASE
                                          WHEN T143.c5 IS NULL THEN 0
                                          ELSE T143.c5
                                      END) c7,
                                  sum(CASE
                                          WHEN T143.c6 IS NULL THEN 0
                                          ELSE T143.c6
                                      END) c8,
                                  T142.c7 c9,
                                  T142.Расторженные Расторженные
                           FROM
                             (SELECT T0.C0 c1,
                                     T0.C1 Тип_контрагента,
                                     T0.C2 Номер_договора,
                                     T0.C3 Дата_договора_ДУ,
                                     sum(T0.C4) over (partition BY T0.C2) c5,
                                                    sum(T0.C5) over (partition BY T0.C2) c6,
                                                                   T0.C6 c7,
                                                                   T0.C7 Расторженные
                              FROM
                                (SELECT T133.c1 C0,
                                        T133.Тип_контрагента C1,
                                        T133.Номер_договора C2,
                                        T133.Дата_договора_ДУ C3,
                                        sum(T133.c5) C4,
                                        sum(T133.c7) C5,
                                        T133.c9 C6,
                                        T133.Расторженные C7
                                 FROM
                                   (SELECT T127.c9 c1,
                                           T127.Тип_контрагента Тип_контрагента,
                                           coguda10.REGNUM Номер_договора,
                                           coguda10.REGDATE Дата_договора_ДУ,
                                           sum(CASE
                                                   WHEN T129.c3 IS NULL THEN CASE
                                                                                     WHEN T129.c4 IS NULL THEN 0
                                                                                     ELSE T129.c4
                                                                                 END
                                                   ELSE T129.c3
                                               END) c5,
                                           T129.c17 c6,
                                           sum(T129.c5) c7,
                                           T130.NAME Направление,
                                           coguda10.DISSOLDATE c9,
                                           CASE
                                               WHEN coguda10.DISSOLDATE IS NULL THEN 0
                                               ELSE 1
                                           END Расторженные
                                    FROM
                                      (SELECT Контрагенты.ID ID,
                                              T2.ID ID_юрлица,
                                              T2.CONTRAGENTID ID_контрагента,
                                              Контрагенты.NAME Контрагент,
                                              Контрагенты.TYPE Тип_контрагента,
                                              T3.STATUS c6,
                                              T2.FULLNAME c7,
                                              T2.SHORTNAME c8,
                                              T2.FORMALIZEDNAME c9,
                                              T2.REGISTRATIONNUM ОГРН,
                                              T2.REGISTRATOR c11,
                                              T2.REGISTRATIONDATE c12,
                                              T2.CLOSEDATE Дата_ликвидации,
                                              T2.INN ИНН,
                                              T2.HEADPOSITION c15,
                                              T2.HEADFULLNAME c16,
                                              replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                              replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                              replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                              T2.PHONE Телефон,
                                              T2.FAX Факс,
                                              replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                              T2.SITE WEB_cайт,
                                              T2.COMMENT Коментарий,
                                              T2.OKPP OKPP,
                                              T2.DECLNAME c26,
                                              T2.LETTERWHO Письмо_кому,
                                              T2.TRANSFDOCKIND c28,
                                              Статус_юрлица.NAME Статус_юрлица
                                       FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                              INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                             INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                       LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                       WHERE Контрагенты.TYPE = 'УК'
                                         OR Контрагенты.TYPE = 'ГУК') T127,
                                         PFR_BASIC.CONTRACT coguda10,
                                         PFR_BASIC.CONTRACTNAME coguda11,
                                         PFR_BASIC.DOCTYPE coguda12,

                                      (SELECT rt.SUM c3,
                                              rt.SUM c4,
                                              rt.INVESTDOHOD c5,
                                              rt.CONTRL_SPN_DATE c8,
                                              rt.CONTRL_ADD_SPN_DATE c13,
                                              rt.CONTRL_SPN_DATE c14,
                                              rt.ACT_DATE c17,
                                              rt.STATUS Статус,
                                              rt.SI_TR_ID ID_перечисления
                                       FROM PFR_BASIC.REQ_TRANSFER rt
                                       LEFT OUTER JOIN
                                         (SELECT t.REQ_TR_ID,
                                                 t.DRAFT_REGNUM,
                                                 t.DRAFT_DATE,
                                                 t.DRAFT_PAYDATE,
                                                 t.DRAFT_AMOUNT,
                                                 t.DIRECTION_EL_ID,
                                                 rank() over (partition BY t.REQ_TR_ID
                                                              ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                        rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                     ORDER BY t.ID DESC) rnID
                                          FROM PFR_BASIC.ASG_FIN_TR t
                                          WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                       AND 1 = pp.rn
                                       AND 1 = pp.rnID
                                       WHERE rt.STATUS_ID <> -1) T129,
                                         PFR_BASIC.DIRECTION_SPN T130,
                                         PFR_BASIC.SI_REGISTER T131,
                                         PFR_BASIC.SI_TRANSFER Перечисление
                                    WHERE NOT T129.c17 IS NULL
                                      AND T129.c17 BETWEEN {0} AND {1}
                                      AND coguda10.TYPEID = 3
                                      AND T130.NAME = 'из ПФР в ГУК ВР'
                                      AND T127.ID_юрлица = coguda10.LE_ID
                                      AND coguda10.ID = Перечисление.CONTR_ID
                                      AND T131.ID = Перечисление.SI_REG_ID
                                      AND T130.ID = T131.DIR_SPN_ID
                                      AND Перечисление.ID = T129.ID_перечисления
                                      AND T131.STATUS_ID <> -1
                                      AND coguda11.ID = coguda10.CONT_N_ID
                                      AND coguda12.ID = coguda10.DT_ID
                                    GROUP BY T127.c9,
                                             T127.Тип_контрагента,
                                             coguda10.REGNUM,
                                             coguda10.REGDATE,
                                             T129.c17,
                                             T130.NAME,
                                             coguda10.DISSOLDATE,
                                             CASE
                                                 WHEN coguda10.DISSOLDATE IS NULL THEN 0
                                                 ELSE 1
                                             END) T133
                                 GROUP BY T133.c1,
                                          T133.Тип_контрагента,
                                          T133.Номер_договора,
                                          T133.Дата_договора_ДУ,
                                          T133.c9,
                                          T133.Расторженные) T0) T142
                           FULL OUTER JOIN
                             (SELECT T0.C0 c1,
                                     T0.C1 Тип_контрагента,
                                     T0.C2 Номер_договора,
                                     T0.C3 Дата_договора_ДУ,
                                     sum(T0.C4) over (partition BY T0.C2) c5,
                                                    sum(T0.C5) over (partition BY T0.C2) c6
                              FROM
                                (SELECT T141.c1 C0,
                                        T141.Тип_контрагента C1,
                                        T141.Номер_договора C2,
                                        T141.Дата_договора_ДУ C3,
                                        sum(T141.c5) C4,
                                        sum(T141.c7) C5
                                 FROM
                                   (SELECT T134.c9 c1,
                                           T134.Тип_контрагента Тип_контрагента,
                                           Договора_с_УК135.Номер Номер_договора,
                                           Договора_с_УК135.c8 Дата_договора_ДУ,
                                           sum(CASE
                                                   WHEN T136.c3 IS NULL THEN CASE
                                                                                     WHEN T136.c4 IS NULL THEN 0
                                                                                     ELSE T136.c4
                                                                                 END
                                                   ELSE T136.c3
                                               END) c5,
                                           T136.c17 c6,
                                           sum(T136.c5) c7,
                                           T137.Направление Направление,
                                           Вид_операции.Вид_операции Вид_операции
                                    FROM (((((
                                                (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                                        PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                                        PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                                 FROM PFR_BASIC.SI_REGISTER
                                                 WHERE STATUS_ID <> -1) T139
                                              INNER JOIN
                                                (SELECT Перечисление.ID ID,
                                                        Перечисление.CONTR_ID ID_договора_с_УК,
                                                        Перечисление.SI_REG_ID c3
                                                 FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление140 ON T139.ID = Перечисление140.c3)
                                             INNER JOIN
                                               (SELECT rt.SUM c3,
                                                       rt.SUM c4,
                                                       rt.INVESTDOHOD c5,
                                                       rt.CONTRL_SPN_DATE c8,
                                                       rt.CONTRL_ADD_SPN_DATE c13,
                                                       rt.CONTRL_SPN_DATE c14,
                                                       rt.ACT_DATE c17,
                                                       rt.STATUS Статус,
                                                       rt.SI_TR_ID ID_перечисления
                                                FROM PFR_BASIC.REQ_TRANSFER rt
                                                LEFT OUTER JOIN
                                                  (SELECT t.REQ_TR_ID,
                                                          t.DRAFT_REGNUM,
                                                          t.DRAFT_DATE,
                                                          t.DRAFT_PAYDATE,
                                                          t.DRAFT_AMOUNT,
                                                          t.DIRECTION_EL_ID,
                                                          rank() over (partition BY t.REQ_TR_ID
                                                                       ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                                 rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                              ORDER BY t.ID DESC) rnID
                                                   FROM PFR_BASIC.ASG_FIN_TR t
                                                   WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                                AND 1 = pp.rn
                                                AND 1 = pp.rnID
                                                WHERE rt.STATUS_ID <> -1) T136 ON Перечисление140.ID = T136.ID_перечисления)
                                            INNER JOIN
                                              (SELECT Договора_с_УК.ID ID,
                                                      Договора_с_УК.LE_ID ID_юрлица,
                                                      Договора_с_УК.REGDATE c8,
                                                      Договора_с_УК.REGNUM Номер,
                                                      Договора_с_УК.DISSOLDATE c13,
                                                      Договора_с_УК.TYPEID Тип_портфеля
                                               FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                                    PFR_BASIC.CONTRACTNAME T2,
                                                    PFR_BASIC.DOCTYPE Тип_документа
                                               WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                                 AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК135 ON Договора_с_УК135.ID = Перечисление140.ID_договора_с_УК)
                                           INNER JOIN
                                             (SELECT Контрагенты.ID ID,
                                                     T2.ID ID_юрлица,
                                                     T2.CONTRAGENTID ID_контрагента,
                                                     Контрагенты.NAME Контрагент,
                                                     Контрагенты.TYPE Тип_контрагента,
                                                     T3.STATUS c6,
                                                     T2.FULLNAME c7,
                                                     T2.SHORTNAME c8,
                                                     T2.FORMALIZEDNAME c9,
                                                     T2.REGISTRATIONNUM ОГРН,
                                                     T2.REGISTRATOR c11,
                                                     T2.REGISTRATIONDATE c12,
                                                     T2.CLOSEDATE Дата_ликвидации,
                                                     T2.INN ИНН,
                                                     T2.HEADPOSITION c15,
                                                     T2.HEADFULLNAME c16,
                                                     replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                                     replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                                     replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                                     T2.PHONE Телефон,
                                                     T2.FAX Факс,
                                                     replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                                     T2.SITE WEB_cайт,
                                                     T2.COMMENT Коментарий,
                                                     T2.OKPP OKPP,
                                                     T2.DECLNAME c26,
                                                     T2.LETTERWHO Письмо_кому,
                                                     T2.TRANSFDOCKIND c28,
                                                     Статус_юрлица.NAME Статус_юрлица
                                              FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                                     INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                                    INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                              LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                              WHERE Контрагенты.TYPE = 'УК'
                                                OR Контрагенты.TYPE = 'ГУК') T134 ON T134.ID_юрлица = Договора_с_УК135.ID_юрлица)
                                          INNER JOIN
                                            (SELECT T1.ID ID,
                                                    T1.NAME Направление
                                             FROM PFR_BASIC.DIRECTION_SPN T1) T137 ON T137.ID = T139.c9)
                                    LEFT OUTER JOIN
                                      (SELECT Вид_операции.ID ID,
                                              Вид_операции.OPERATION Вид_операции
                                       FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции ON T139.c8 = Вид_операции.ID
                                    WHERE NOT T136.c17 IS NULL
                                      AND T136.c17 BETWEEN {0} AND {1}
                                      AND Договора_с_УК135.Тип_портфеля = 3
                                      AND T137.Направление = 'из ГУК ВР в ПФР'
                                    GROUP BY T134.c9,
                                             T134.Тип_контрагента,
                                             Договора_с_УК135.Номер,
                                             Договора_с_УК135.c8,
                                             T136.c17,
                                             T137.Направление,
                                             Вид_операции.Вид_операции) T141
                                 GROUP BY T141.c1,
                                          T141.Тип_контрагента,
                                          T141.Номер_договора,
                                          T141.Дата_договора_ДУ) T0) T143 ON T142.Номер_договора = T143.Номер_договора
                           WHERE T142.{2}
                           GROUP BY CASE
                                        WHEN T142.c1 IS NULL THEN T143.c1
                                        ELSE T142.c1
                                    END,
                                    CASE
                                        WHEN T142.Тип_контрагента IS NULL THEN T143.Тип_контрагента
                                        ELSE T142.Тип_контрагента
                                    END,
                                    CASE
                                        WHEN T142.Номер_договора IS NULL THEN T143.Номер_договора
                                        ELSE T142.Номер_договора
                                    END,
                                    CASE
                                        WHEN T142.Дата_договора_ДУ IS NULL THEN T143.Дата_договора_ДУ
                                        ELSE T142.Дата_договора_ДУ
                                    END,
                                    T142.c7,
                                    T142.Расторженные) Запрос1152
                        LEFT OUTER JOIN
                          (SELECT T0.C0 c1,
                                  T0.C1 Номер_договора,
                                  T0.C2 Дата_договора_ДУ,
                                  sum(T0.C3) over (partition BY T0.C1) c4,
                                                 sum(T0.C4) over (partition BY T0.C1) c5
                           FROM
                             (SELECT T151.c1 C0,
                                     T151.Номер_договора C1,
                                     T151.Дата_договора_ДУ C2,
                                     sum(T151.c4) C3,
                                     sum(T151.c6) C4
                              FROM
                                (SELECT T144.c9 c1,
                                        Договора_с_УК145.Номер Номер_договора,
                                        Договора_с_УК145.c8 Дата_договора_ДУ,
                                        sum(CASE
                                                WHEN T146.c3 IS NULL THEN CASE
                                                                                  WHEN T146.c4 IS NULL THEN 0
                                                                                  ELSE T146.c4
                                                                              END
                                                ELSE T146.c3
                                            END) c4,
                                        T146.c17 c5,
                                        sum(T146.c5) c6,
                                        T147.Направление Направление,
                                        Вид_операции148.Вид_операции Вид_операции
                                 FROM (((((
                                             (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                                     PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                                     PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                              FROM PFR_BASIC.SI_REGISTER
                                              WHERE STATUS_ID <> -1) T149
                                           INNER JOIN
                                             (SELECT Перечисление.ID ID,
                                                     Перечисление.CONTR_ID ID_договора_с_УК,
                                                     Перечисление.SI_REG_ID c3
                                              FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление150 ON T149.ID = Перечисление150.c3)
                                          INNER JOIN
                                            (SELECT rt.SUM c3,
                                                    rt.SUM c4,
                                                    rt.INVESTDOHOD c5,
                                                    rt.CONTRL_SPN_DATE c8,
                                                    rt.CONTRL_ADD_SPN_DATE c13,
                                                    rt.CONTRL_SPN_DATE c14,
                                                    rt.ACT_DATE c17,
                                                    rt.STATUS Статус,
                                                    rt.SI_TR_ID ID_перечисления
                                             FROM PFR_BASIC.REQ_TRANSFER rt
                                             LEFT OUTER JOIN
                                               (SELECT t.REQ_TR_ID,
                                                       t.DRAFT_REGNUM,
                                                       t.DRAFT_DATE,
                                                       t.DRAFT_PAYDATE,
                                                       t.DRAFT_AMOUNT,
                                                       t.DIRECTION_EL_ID,
                                                       rank() over (partition BY t.REQ_TR_ID
                                                                    ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                              rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                           ORDER BY t.ID DESC) rnID
                                                FROM PFR_BASIC.ASG_FIN_TR t
                                                WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                             AND 1 = pp.rn
                                             AND 1 = pp.rnID
                                             WHERE rt.STATUS_ID <> -1) T146 ON Перечисление150.ID = T146.ID_перечисления)
                                         INNER JOIN
                                           (SELECT Договора_с_УК.ID ID,
                                                   Договора_с_УК.LE_ID ID_юрлица,
                                                   Договора_с_УК.REGDATE c8,
                                                   Договора_с_УК.REGNUM Номер,
                                                   Договора_с_УК.DISSOLDATE c13,
                                                   Договора_с_УК.TYPEID Тип_портфеля
                                            FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                                 PFR_BASIC.CONTRACTNAME T2,
                                                 PFR_BASIC.DOCTYPE Тип_документа
                                            WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                              AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК145 ON Договора_с_УК145.ID = Перечисление150.ID_договора_с_УК)
                                        INNER JOIN
                                          (SELECT Контрагенты.ID ID,
                                                  T2.ID ID_юрлица,
                                                  T2.CONTRAGENTID ID_контрагента,
                                                  Контрагенты.NAME Контрагент,
                                                  Контрагенты.TYPE Тип_контрагента,
                                                  T3.STATUS c6,
                                                  T2.FULLNAME c7,
                                                  T2.SHORTNAME c8,
                                                  T2.FORMALIZEDNAME c9,
                                                  T2.REGISTRATIONNUM ОГРН,
                                                  T2.REGISTRATOR c11,
                                                  T2.REGISTRATIONDATE c12,
                                                  T2.CLOSEDATE Дата_ликвидации,
                                                  T2.INN ИНН,
                                                  T2.HEADPOSITION c15,
                                                  T2.HEADFULLNAME c16,
                                                  replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                                  replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                                  replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                                  T2.PHONE Телефон,
                                                  T2.FAX Факс,
                                                  replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                                  T2.SITE WEB_cайт,
                                                  T2.COMMENT Коментарий,
                                                  T2.OKPP OKPP,
                                                  T2.DECLNAME c26,
                                                  T2.LETTERWHO Письмо_кому,
                                                  T2.TRANSFDOCKIND c28,
                                                  Статус_юрлица.NAME Статус_юрлица
                                           FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                                  INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                                 INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                           LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                           WHERE Контрагенты.TYPE = 'УК'
                                             OR Контрагенты.TYPE = 'ГУК') T144 ON T144.ID_юрлица = Договора_с_УК145.ID_юрлица)
                                       INNER JOIN
                                         (SELECT T1.ID ID,
                                                 T1.NAME Направление
                                          FROM PFR_BASIC.DIRECTION_SPN T1) T147 ON T147.ID = T149.c9)
                                 LEFT OUTER JOIN
                                   (SELECT Вид_операции.ID ID,
                                           Вид_операции.OPERATION Вид_операции
                                    FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции148 ON T149.c8 = Вид_операции148.ID
                                 WHERE NOT T146.c17 IS NULL
                                   AND T146.c17 BETWEEN {0} AND {1}
                                   AND Договора_с_УК145.Тип_портфеля = 3
                                   AND T147.Направление = 'из ГУК ВР в ПФР'
                                 GROUP BY T144.c9,
                                          Договора_с_УК145.Номер,
                                          Договора_с_УК145.c8,
                                          T146.c17,
                                          T147.Направление,
                                          Вид_операции148.Вид_операции) T151
                              GROUP BY T151.c1,
                                       T151.Номер_договора,
                                       T151.Дата_договора_ДУ) T0) T153 ON Запрос1152.Номер_договора = T153.Номер_договора
                        WHERE Запрос1152.{2}) Запрос2162
                     LEFT OUTER JOIN
                       (SELECT T0.C0 c1,
                               T0.C1 Номер_договора,
                               T0.C2 Дата_договора_ДУ,
                               sum(T0.C3) over (partition BY T0.C1) c4,
                                              sum(T0.C4) over (partition BY T0.C1) c5
                        FROM
                          (SELECT T161.c1 C0,
                                  T161.Номер_договора C1,
                                  T161.Дата_договора_ДУ C2,
                                  sum(T161.c4) C3,
                                  sum(T161.c6) C4
                           FROM
                             (SELECT T154.c9 c1,
                                     Договора_с_УК155.Номер Номер_договора,
                                     Договора_с_УК155.c8 Дата_договора_ДУ,
                                     sum(CASE
                                             WHEN T156.c3 IS NULL THEN CASE
                                                                               WHEN T156.c4 IS NULL THEN 0
                                                                               ELSE T156.c4
                                                                           END
                                             ELSE T156.c3
                                         END) c4,
                                     T156.c17 c5,
                                     sum(T156.c5) c6,
                                     T157.Направление Направление,
                                     Вид_операции158.Вид_операции Вид_операции
                              FROM (((((
                                          (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                                  PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                                  PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                           FROM PFR_BASIC.SI_REGISTER
                                           WHERE STATUS_ID <> -1) T159
                                        INNER JOIN
                                          (SELECT Перечисление.ID ID,
                                                  Перечисление.CONTR_ID ID_договора_с_УК,
                                                  Перечисление.SI_REG_ID c3
                                           FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление160 ON T159.ID = Перечисление160.c3)
                                       INNER JOIN
                                         (SELECT rt.SUM c3,
                                                 rt.SUM c4,
                                                 rt.INVESTDOHOD c5,
                                                 rt.CONTRL_SPN_DATE c8,
                                                 rt.CONTRL_ADD_SPN_DATE c13,
                                                 rt.CONTRL_SPN_DATE c14,
                                                 rt.ACT_DATE c17,
                                                 rt.STATUS Статус,
                                                 rt.SI_TR_ID ID_перечисления
                                          FROM PFR_BASIC.REQ_TRANSFER rt
                                          LEFT OUTER JOIN
                                            (SELECT t.REQ_TR_ID,
                                                    t.DRAFT_REGNUM,
                                                    t.DRAFT_DATE,
                                                    t.DRAFT_PAYDATE,
                                                    t.DRAFT_AMOUNT,
                                                    t.DIRECTION_EL_ID,
                                                    rank() over (partition BY t.REQ_TR_ID
                                                                 ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                           rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                        ORDER BY t.ID DESC) rnID
                                             FROM PFR_BASIC.ASG_FIN_TR t
                                             WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                          AND 1 = pp.rn
                                          AND 1 = pp.rnID
                                          WHERE rt.STATUS_ID <> -1) T156 ON Перечисление160.ID = T156.ID_перечисления)
                                      INNER JOIN
                                        (SELECT Договора_с_УК.ID ID,
                                                Договора_с_УК.LE_ID ID_юрлица,
                                                Договора_с_УК.REGDATE c8,
                                                Договора_с_УК.REGNUM Номер,
                                                Договора_с_УК.DISSOLDATE c13,
                                                Договора_с_УК.TYPEID Тип_портфеля
                                         FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                              PFR_BASIC.CONTRACTNAME T2,
                                              PFR_BASIC.DOCTYPE Тип_документа
                                         WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                           AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК155 ON Договора_с_УК155.ID = Перечисление160.ID_договора_с_УК)
                                     INNER JOIN
                                       (SELECT Контрагенты.ID ID,
                                               T2.ID ID_юрлица,
                                               T2.CONTRAGENTID ID_контрагента,
                                               Контрагенты.NAME Контрагент,
                                               Контрагенты.TYPE Тип_контрагента,
                                               T3.STATUS c6,
                                               T2.FULLNAME c7,
                                               T2.SHORTNAME c8,
                                               T2.FORMALIZEDNAME c9,
                                               T2.REGISTRATIONNUM ОГРН,
                                               T2.REGISTRATOR c11,
                                               T2.REGISTRATIONDATE c12,
                                               T2.CLOSEDATE Дата_ликвидации,
                                               T2.INN ИНН,
                                               T2.HEADPOSITION c15,
                                               T2.HEADFULLNAME c16,
                                               replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                               replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                               replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                               T2.PHONE Телефон,
                                               T2.FAX Факс,
                                               replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                               T2.SITE WEB_cайт,
                                               T2.COMMENT Коментарий,
                                               T2.OKPP OKPP,
                                               T2.DECLNAME c26,
                                               T2.LETTERWHO Письмо_кому,
                                               T2.TRANSFDOCKIND c28,
                                               Статус_юрлица.NAME Статус_юрлица
                                        FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                               INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                              INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                        LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                        WHERE Контрагенты.TYPE = 'УК'
                                          OR Контрагенты.TYPE = 'ГУК') T154 ON T154.ID_юрлица = Договора_с_УК155.ID_юрлица)
                                    INNER JOIN
                                      (SELECT T1.ID ID,
                                              T1.NAME Направление
                                       FROM PFR_BASIC.DIRECTION_SPN T1) T157 ON T157.ID = T159.c9)
                              LEFT OUTER JOIN
                                (SELECT Вид_операции.ID ID,
                                        Вид_операции.OPERATION Вид_операции
                                 FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции158 ON T159.c8 = Вид_операции158.ID
                              WHERE NOT T156.c17 IS NULL
                                AND T156.c17 BETWEEN {0} AND {1}
                                AND Договора_с_УК155.Тип_портфеля = 3
                                AND T157.Направление = 'из ГУК ВР в ПФР'
                              GROUP BY T154.c9,
                                       Договора_с_УК155.Номер,
                                       Договора_с_УК155.c8,
                                       T156.c17,
                                       T157.Направление,
                                       Вид_операции158.Вид_операции) T161
                           GROUP BY T161.c1,
                                    T161.Номер_договора,
                                    T161.Дата_договора_ДУ) T0) T163 ON Запрос2162.Номер_договора = T163.Номер_договора
                     WHERE Запрос2162.{2}) Запрос3172
                  LEFT OUTER JOIN
                    (SELECT T0.C0 c1,
                            T0.C1 Номер_договора,
                            T0.C2 Дата_договора_ДУ,
                            sum(T0.C3) over (partition BY T0.C1) c4,
                                           sum(T0.C4) over (partition BY T0.C1) c5
                     FROM
                       (SELECT T171.c1 C0,
                               T171.Номер_договора C1,
                               T171.Дата_договора_ДУ C2,
                               sum(T171.c4) C3,
                               sum(T171.c6) C4
                        FROM
                          (SELECT T164.c9 c1,
                                  Договора_с_УК165.Номер Номер_договора,
                                  Договора_с_УК165.c8 Дата_договора_ДУ,
                                  sum(CASE
                                          WHEN T166.c3 IS NULL THEN CASE
                                                                            WHEN T166.c4 IS NULL THEN 0
                                                                            ELSE T166.c4
                                                                        END
                                          ELSE T166.c3
                                      END) c4,
                                  T166.c17 c5,
                                  sum(T166.c5) c6,
                                  T167.Направление Направление,
                                  Вид_операции168.Вид_операции Вид_операции
                           FROM (((((
                                       (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                               PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                               PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                        FROM PFR_BASIC.SI_REGISTER
                                        WHERE STATUS_ID <> -1) T169
                                     INNER JOIN
                                       (SELECT Перечисление.ID ID,
                                               Перечисление.CONTR_ID ID_договора_с_УК,
                                               Перечисление.SI_REG_ID c3
                                        FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление170 ON T169.ID = Перечисление170.c3)
                                    INNER JOIN
                                      (SELECT rt.SUM c3,
                                              rt.SUM c4,
                                              rt.INVESTDOHOD c5,
                                              rt.CONTRL_SPN_DATE c8,
                                              rt.CONTRL_ADD_SPN_DATE c13,
                                              rt.CONTRL_SPN_DATE c14,
                                              rt.ACT_DATE c17,
                                              rt.STATUS Статус,
                                              rt.SI_TR_ID ID_перечисления
                                       FROM PFR_BASIC.REQ_TRANSFER rt
                                       LEFT OUTER JOIN
                                         (SELECT t.REQ_TR_ID,
                                                 t.DRAFT_REGNUM,
                                                 t.DRAFT_DATE,
                                                 t.DRAFT_PAYDATE,
                                                 t.DRAFT_AMOUNT,
                                                 t.DIRECTION_EL_ID,
                                                 rank() over (partition BY t.REQ_TR_ID
                                                              ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                        rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                     ORDER BY t.ID DESC) rnID
                                          FROM PFR_BASIC.ASG_FIN_TR t
                                          WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                       AND 1 = pp.rn
                                       AND 1 = pp.rnID
                                       WHERE rt.STATUS_ID <> -1) T166 ON Перечисление170.ID = T166.ID_перечисления)
                                   INNER JOIN
                                     (SELECT Договора_с_УК.ID ID,
                                             Договора_с_УК.LE_ID ID_юрлица,
                                             Договора_с_УК.REGDATE c8,
                                             Договора_с_УК.REGNUM Номер,
                                             Договора_с_УК.DISSOLDATE c13,
                                             Договора_с_УК.TYPEID Тип_портфеля
                                      FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                           PFR_BASIC.CONTRACTNAME T2,
                                           PFR_BASIC.DOCTYPE Тип_документа
                                      WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                        AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК165 ON Договора_с_УК165.ID = Перечисление170.ID_договора_с_УК)
                                  INNER JOIN
                                    (SELECT Контрагенты.ID ID,
                                            T2.ID ID_юрлица,
                                            T2.CONTRAGENTID ID_контрагента,
                                            Контрагенты.NAME Контрагент,
                                            Контрагенты.TYPE Тип_контрагента,
                                            T3.STATUS c6,
                                            T2.FULLNAME c7,
                                            T2.SHORTNAME c8,
                                            T2.FORMALIZEDNAME c9,
                                            T2.REGISTRATIONNUM ОГРН,
                                            T2.REGISTRATOR c11,
                                            T2.REGISTRATIONDATE c12,
                                            T2.CLOSEDATE Дата_ликвидации,
                                            T2.INN ИНН,
                                            T2.HEADPOSITION c15,
                                            T2.HEADFULLNAME c16,
                                            replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                            replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                            replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                            T2.PHONE Телефон,
                                            T2.FAX Факс,
                                            replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                            T2.SITE WEB_cайт,
                                            T2.COMMENT Коментарий,
                                            T2.OKPP OKPP,
                                            T2.DECLNAME c26,
                                            T2.LETTERWHO Письмо_кому,
                                            T2.TRANSFDOCKIND c28,
                                            Статус_юрлица.NAME Статус_юрлица
                                     FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                            INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                           INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                     LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                     WHERE Контрагенты.TYPE = 'УК'
                                       OR Контрагенты.TYPE = 'ГУК') T164 ON T164.ID_юрлица = Договора_с_УК165.ID_юрлица)
                                 INNER JOIN
                                   (SELECT T1.ID ID,
                                           T1.NAME Направление
                                    FROM PFR_BASIC.DIRECTION_SPN T1) T167 ON T167.ID = T169.c9)
                           LEFT OUTER JOIN
                             (SELECT Вид_операции.ID ID,
                                     Вид_операции.OPERATION Вид_операции
                              FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции168 ON T169.c8 = Вид_операции168.ID
                           WHERE NOT T166.c17 IS NULL
                             AND T166.c17 BETWEEN {0} AND {1}
                             AND Договора_с_УК165.Тип_портфеля = 3
                             AND T167.Направление = 'из ГУК ВР в ПФР'
                           GROUP BY T164.c9,
                                    Договора_с_УК165.Номер,
                                    Договора_с_УК165.c8,
                                    T166.c17,
                                    T167.Направление,
                                    Вид_операции168.Вид_операции) T171
                        GROUP BY T171.c1,
                                 T171.Номер_договора,
                                 T171.Дата_договора_ДУ) T0) T173 ON Запрос3172.Номер_договора = T173.Номер_договора
                  WHERE Запрос3172.{2}) Запрос_4182
               LEFT OUTER JOIN
                 (SELECT T0.C0 c1,
                         T0.C1 c2,
                         T0.C2 Номер,
                         sum(T0.C3) over (partition BY T0.C2) c4,
                                        CASE
                                            WHEN sum(T0.C4) over (partition BY T0.C2) IS NULL THEN 0
                                            ELSE sum(T0.C4) over (partition BY T0.C2)
                                        END c5
                  FROM
                    (SELECT T181.c1 C0,
                            T181.c3 C1,
                            T181.Номер C2,
                            sum(T181.c4) C3,
                            sum(T181.c6) C4
                     FROM
                       (SELECT T174.c9 c1,
                               Договора_с_УК175.Номер Номер,
                               Договора_с_УК175.c8 c3,
                               sum(CASE
                                       WHEN T176.c3 IS NULL THEN CASE
                                                                         WHEN T176.c4 IS NULL THEN 0
                                                                         ELSE T176.c4
                                                                     END
                                       ELSE T176.c3
                                   END) c4,
                               T176.c17 c5,
                               sum(CASE
                                       WHEN T176.c5 IS NULL THEN 0
                                       ELSE T176.c5
                                   END) c6,
                               T177.Направление Направление,
                               Вид_операции178.Вид_операции Вид_операции
                        FROM (((((
                                    (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                            PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                            PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                     FROM PFR_BASIC.SI_REGISTER
                                     WHERE STATUS_ID <> -1) T179
                                  INNER JOIN
                                    (SELECT Перечисление.ID ID,
                                            Перечисление.CONTR_ID ID_договора_с_УК,
                                            Перечисление.SI_REG_ID c3
                                     FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление180 ON T179.ID = Перечисление180.c3)
                                 INNER JOIN
                                   (SELECT rt.SUM c3,
                                           rt.SUM c4,
                                           rt.INVESTDOHOD c5,
                                           rt.CONTRL_SPN_DATE c8,
                                           rt.CONTRL_ADD_SPN_DATE c13,
                                           rt.CONTRL_SPN_DATE c14,
                                           rt.ACT_DATE c17,
                                           rt.STATUS Статус,
                                           rt.SI_TR_ID ID_перечисления
                                    FROM PFR_BASIC.REQ_TRANSFER rt
                                    LEFT OUTER JOIN
                                      (SELECT t.REQ_TR_ID,
                                              t.DRAFT_REGNUM,
                                              t.DRAFT_DATE,
                                              t.DRAFT_PAYDATE,
                                              t.DRAFT_AMOUNT,
                                              t.DIRECTION_EL_ID,
                                              rank() over (partition BY t.REQ_TR_ID
                                                           ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                     rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                  ORDER BY t.ID DESC) rnID
                                       FROM PFR_BASIC.ASG_FIN_TR t
                                       WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                    AND 1 = pp.rn
                                    AND 1 = pp.rnID
                                    WHERE rt.STATUS_ID <> -1) T176 ON Перечисление180.ID = T176.ID_перечисления)
                                INNER JOIN
                                  (SELECT Договора_с_УК.ID ID,
                                          Договора_с_УК.LE_ID ID_юрлица,
                                          Договора_с_УК.REGDATE c8,
                                          Договора_с_УК.REGNUM Номер,
                                          Договора_с_УК.DISSOLDATE c13,
                                          Договора_с_УК.TYPEID Тип_портфеля
                                   FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                        PFR_BASIC.CONTRACTNAME T2,
                                        PFR_BASIC.DOCTYPE Тип_документа
                                   WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                     AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК175 ON Договора_с_УК175.ID = Перечисление180.ID_договора_с_УК)
                               INNER JOIN
                                 (SELECT Контрагенты.ID ID,
                                         T2.ID ID_юрлица,
                                         T2.CONTRAGENTID ID_контрагента,
                                         Контрагенты.NAME Контрагент,
                                         Контрагенты.TYPE Тип_контрагента,
                                         T3.STATUS c6,
                                         T2.FULLNAME c7,
                                         T2.SHORTNAME c8,
                                         T2.FORMALIZEDNAME c9,
                                         T2.REGISTRATIONNUM ОГРН,
                                         T2.REGISTRATOR c11,
                                         T2.REGISTRATIONDATE c12,
                                         T2.CLOSEDATE Дата_ликвидации,
                                         T2.INN ИНН,
                                         T2.HEADPOSITION c15,
                                         T2.HEADFULLNAME c16,
                                         replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                         replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                         replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                         T2.PHONE Телефон,
                                         T2.FAX Факс,
                                         replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                         T2.SITE WEB_cайт,
                                         T2.COMMENT Коментарий,
                                         T2.OKPP OKPP,
                                         T2.DECLNAME c26,
                                         T2.LETTERWHO Письмо_кому,
                                         T2.TRANSFDOCKIND c28,
                                         Статус_юрлица.NAME Статус_юрлица
                                  FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                         INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                        INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                  LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                  WHERE Контрагенты.TYPE = 'УК'
                                    OR Контрагенты.TYPE = 'ГУК') T174 ON T174.ID_юрлица = Договора_с_УК175.ID_юрлица)
                              INNER JOIN
                                (SELECT T1.ID ID,
                                        T1.NAME Направление
                                 FROM PFR_BASIC.DIRECTION_SPN T1) T177 ON T177.ID = T179.c9)
                        LEFT OUTER JOIN
                          (SELECT Вид_операции.ID ID,
                                  Вид_операции.OPERATION Вид_операции
                           FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции178 ON T179.c8 = Вид_операции178.ID
                        WHERE NOT T176.c17 IS NULL
                          AND T176.c17 BETWEEN {0} AND {1}
                          AND Договора_с_УК175.Тип_портфеля = 3
                          AND T177.Направление = 'из ГУК ВР в ПФР'
                        GROUP BY T174.c9,
                                 Договора_с_УК175.Номер,
                                 Договора_с_УК175.c8,
                                 T176.c17,
                                 T177.Направление,
                                 Вид_операции178.Вид_операции) T181
                     GROUP BY T181.c1,
                              T181.c3,
                              T181.Номер) T0) T183 ON Запрос_4182.Номер_договора = T183.Номер
               WHERE Запрос_4182.{2}
               GROUP BY Запрос_4182.c1,
                        Запрос_4182.Тип_контрагента,
                        Запрос_4182.Номер_договора,
                        Запрос_4182.Дата_договора_ДУ,
                        T183.c1,
                        T183.c2,
                        T183.Номер,
                        Запрос_4182.c16,
                        Запрос_4182.Расторженные) Запрос_5192
            LEFT OUTER JOIN
              (SELECT T0.C0 c1,
                      T0.C1 Номер_договора,
                      T0.C2 Дата_договора_ДУ,
                      sum(T0.C3) over (partition BY T0.C1) c4
               FROM
                 (SELECT T191.c1 C0,
                         T191.Номер_договора C1,
                         T191.Дата_договора_ДУ C2,
                         sum(T191.c4) C3
                  FROM
                    (SELECT T184.c9 c1,
                            Договора_с_УК185.Номер Номер_договора,
                            Договора_с_УК185.c8 Дата_договора_ДУ,
                            sum(CASE
                                    WHEN T186.c3 IS NULL THEN CASE
                                                                      WHEN T186.c4 IS NULL THEN 0
                                                                      ELSE T186.c4
                                                                  END
                                    ELSE T186.c3
                                END) c4,
                            T186.c17 c5,
                            sum(T186.c5) c6,
                            T187.Направление Направление,
                            Вид_операции188.Вид_операции Вид_операции
                     FROM (((((
                                 (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                         PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                         PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                  FROM PFR_BASIC.SI_REGISTER
                                  WHERE STATUS_ID <> -1) T189
                               INNER JOIN
                                 (SELECT Перечисление.ID ID,
                                         Перечисление.CONTR_ID ID_договора_с_УК,
                                         Перечисление.SI_REG_ID c3
                                  FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление190 ON T189.ID = Перечисление190.c3)
                              INNER JOIN
                                (SELECT rt.SUM c3,
                                        rt.SUM c4,
                                        rt.INVESTDOHOD c5,
                                        rt.CONTRL_SPN_DATE c8,
                                        rt.CONTRL_ADD_SPN_DATE c13,
                                        rt.CONTRL_SPN_DATE c14,
                                        rt.ACT_DATE c17,
                                        rt.STATUS Статус,
                                        rt.SI_TR_ID ID_перечисления
                                 FROM PFR_BASIC.REQ_TRANSFER rt
                                 LEFT OUTER JOIN
                                   (SELECT t.REQ_TR_ID,
                                           t.DRAFT_REGNUM,
                                           t.DRAFT_DATE,
                                           t.DRAFT_PAYDATE,
                                           t.DRAFT_AMOUNT,
                                           t.DIRECTION_EL_ID,
                                           rank() over (partition BY t.REQ_TR_ID
                                                        ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                  rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                               ORDER BY t.ID DESC) rnID
                                    FROM PFR_BASIC.ASG_FIN_TR t
                                    WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                 AND 1 = pp.rn
                                 AND 1 = pp.rnID
                                 WHERE rt.STATUS_ID <> -1) T186 ON Перечисление190.ID = T186.ID_перечисления)
                             INNER JOIN
                               (SELECT Договора_с_УК.ID ID,
                                       Договора_с_УК.LE_ID ID_юрлица,
                                       Договора_с_УК.REGDATE c8,
                                       Договора_с_УК.REGNUM Номер,
                                       Договора_с_УК.DISSOLDATE c13,
                                       Договора_с_УК.TYPEID Тип_портфеля
                                FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                     PFR_BASIC.CONTRACTNAME T2,
                                     PFR_BASIC.DOCTYPE Тип_документа
                                WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                  AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК185 ON Договора_с_УК185.ID = Перечисление190.ID_договора_с_УК)
                            INNER JOIN
                              (SELECT Контрагенты.ID ID,
                                      T2.ID ID_юрлица,
                                      T2.CONTRAGENTID ID_контрагента,
                                      Контрагенты.NAME Контрагент,
                                      Контрагенты.TYPE Тип_контрагента,
                                      T3.STATUS c6,
                                      T2.FULLNAME c7,
                                      T2.SHORTNAME c8,
                                      T2.FORMALIZEDNAME c9,
                                      T2.REGISTRATIONNUM ОГРН,
                                      T2.REGISTRATOR c11,
                                      T2.REGISTRATIONDATE c12,
                                      T2.CLOSEDATE Дата_ликвидации,
                                      T2.INN ИНН,
                                      T2.HEADPOSITION c15,
                                      T2.HEADFULLNAME c16,
                                      replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                      replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                      replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                      T2.PHONE Телефон,
                                      T2.FAX Факс,
                                      replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                      T2.SITE WEB_cайт,
                                      T2.COMMENT Коментарий,
                                      T2.OKPP OKPP,
                                      T2.DECLNAME c26,
                                      T2.LETTERWHO Письмо_кому,
                                      T2.TRANSFDOCKIND c28,
                                      Статус_юрлица.NAME Статус_юрлица
                               FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                      INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                     INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                               LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                               WHERE Контрагенты.TYPE = 'УК'
                                 OR Контрагенты.TYPE = 'ГУК') T184 ON T184.ID_юрлица = Договора_с_УК185.ID_юрлица)
                           INNER JOIN
                             (SELECT T1.ID ID,
                                     T1.NAME Направление
                              FROM PFR_BASIC.DIRECTION_SPN T1) T187 ON T187.ID = T189.c9)
                     LEFT OUTER JOIN
                       (SELECT Вид_операции.ID ID,
                               Вид_операции.OPERATION Вид_операции
                        FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции188 ON T189.c8 = Вид_операции188.ID
                     WHERE NOT T186.c17 IS NULL
                       AND T186.c17 BETWEEN {0} AND {1}
                       AND Договора_с_УК185.Тип_портфеля = 3
                       AND T187.Направление = 'из ПФР в ГУК ВР'
                     GROUP BY T184.c9,
                              Договора_с_УК185.Номер,
                              Договора_с_УК185.c8,
                              T186.c17,
                              T187.Направление,
                              Вид_операции188.Вид_операции) T191
                  GROUP BY T191.c1,
                           T191.Номер_договора,
                           T191.Дата_договора_ДУ) T0) T193 ON Запрос_5192.Номер_договора = T193.Номер_договора
            WHERE Запрос_5192.{2}
            GROUP BY Запрос_5192.c1,
                     Запрос_5192.Тип_контрагента,
                     Запрос_5192.Номер_договора,
                     Запрос_5192.Дата_договора_ДУ,
                     Запрос_5192.c21,
                     Запрос_5192.Расторженные) T0) спн___дсв202
      LEFT OUTER JOIN
        (SELECT T0.C0 c1,
                T0.C1 Номер_договора,
                T0.C2 Дата_договора_ДУ,
                sum(T0.C3) over (partition BY T0.C1) c4
         FROM
           (SELECT T201.c1 C0,
                   T201.Номер_договора C1,
                   T201.Дата_договора_ДУ C2,
                   sum(T201.c4) C3
            FROM
              (SELECT T194.c9 c1,
                      Договора_с_УК195.Номер Номер_договора,
                      Договора_с_УК195.c8 Дата_договора_ДУ,
                      sum(CASE
                              WHEN T196.c3 IS NULL THEN CASE
                                                                WHEN T196.c4 IS NULL THEN 0
                                                                ELSE T196.c4
                                                            END
                              ELSE T196.c3
                          END) c4,
                      T196.c17 c5,
                      sum(T196.c5) c6,
                      T197.Направление Направление,
                      Вид_операции198.Вид_операции Вид_операции
               FROM (((((
                           (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                   PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                   PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                            FROM PFR_BASIC.SI_REGISTER
                            WHERE STATUS_ID <> -1) T199
                         INNER JOIN
                           (SELECT Перечисление.ID ID,
                                   Перечисление.CONTR_ID ID_договора_с_УК,
                                   Перечисление.SI_REG_ID c3
                            FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление200 ON T199.ID = Перечисление200.c3)
                        INNER JOIN
                          (SELECT rt.SUM c3,
                                  rt.SUM c4,
                                  rt.INVESTDOHOD c5,
                                  rt.CONTRL_SPN_DATE c8,
                                  rt.CONTRL_ADD_SPN_DATE c13,
                                  rt.CONTRL_SPN_DATE c14,
                                  rt.ACT_DATE c17,
                                  rt.STATUS Статус,
                                  rt.SI_TR_ID ID_перечисления
                           FROM PFR_BASIC.REQ_TRANSFER rt
                           LEFT OUTER JOIN
                             (SELECT t.REQ_TR_ID,
                                     t.DRAFT_REGNUM,
                                     t.DRAFT_DATE,
                                     t.DRAFT_PAYDATE,
                                     t.DRAFT_AMOUNT,
                                     t.DIRECTION_EL_ID,
                                     rank() over (partition BY t.REQ_TR_ID
                                                  ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                            rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                         ORDER BY t.ID DESC) rnID
                              FROM PFR_BASIC.ASG_FIN_TR t
                              WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                           AND 1 = pp.rn
                           AND 1 = pp.rnID
                           WHERE rt.STATUS_ID <> -1) T196 ON Перечисление200.ID = T196.ID_перечисления)
                       INNER JOIN
                         (SELECT Договора_с_УК.ID ID,
                                 Договора_с_УК.LE_ID ID_юрлица,
                                 Договора_с_УК.REGDATE c8,
                                 Договора_с_УК.REGNUM Номер,
                                 Договора_с_УК.DISSOLDATE c13,
                                 Договора_с_УК.TYPEID Тип_портфеля
                          FROM PFR_BASIC.CONTRACT Договора_с_УК,
                               PFR_BASIC.CONTRACTNAME T2,
                               PFR_BASIC.DOCTYPE Тип_документа
                          WHERE T2.ID = Договора_с_УК.CONT_N_ID
                            AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК195 ON Договора_с_УК195.ID = Перечисление200.ID_договора_с_УК)
                      INNER JOIN
                        (SELECT Контрагенты.ID ID,
                                T2.ID ID_юрлица,
                                T2.CONTRAGENTID ID_контрагента,
                                Контрагенты.NAME Контрагент,
                                Контрагенты.TYPE Тип_контрагента,
                                T3.STATUS c6,
                                T2.FULLNAME c7,
                                T2.SHORTNAME c8,
                                T2.FORMALIZEDNAME c9,
                                T2.REGISTRATIONNUM ОГРН,
                                T2.REGISTRATOR c11,
                                T2.REGISTRATIONDATE c12,
                                T2.CLOSEDATE Дата_ликвидации,
                                T2.INN ИНН,
                                T2.HEADPOSITION c15,
                                T2.HEADFULLNAME c16,
                                replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                T2.PHONE Телефон,
                                T2.FAX Факс,
                                replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                T2.SITE WEB_cайт,
                                T2.COMMENT Коментарий,
                                T2.OKPP OKPP,
                                T2.DECLNAME c26,
                                T2.LETTERWHO Письмо_кому,
                                T2.TRANSFDOCKIND c28,
                                Статус_юрлица.NAME Статус_юрлица
                         FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                               INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                         LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                         WHERE Контрагенты.TYPE = 'УК'
                           OR Контрагенты.TYPE = 'ГУК') T194 ON T194.ID_юрлица = Договора_с_УК195.ID_юрлица)
                     INNER JOIN
                       (SELECT T1.ID ID,
                               T1.NAME Направление
                        FROM PFR_BASIC.DIRECTION_SPN T1) T197 ON T197.ID = T199.c9)
               LEFT OUTER JOIN
                 (SELECT Вид_операции.ID ID,
                         Вид_операции.OPERATION Вид_операции
                  FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции198 ON T199.c8 = Вид_операции198.ID
               WHERE NOT T196.c17 IS NULL
                 AND T196.c17 BETWEEN {0} AND {1}
                 AND Договора_с_УК195.Тип_портфеля = 3
                 AND T197.Направление = 'из ПФР в ГУК ВР'
               GROUP BY T194.c9,
                        Договора_с_УК195.Номер,
                        Договора_с_УК195.c8,
                        T196.c17,
                        T197.Направление,
                        Вид_операции198.Вид_операции) T201
            GROUP BY T201.c1,
                     T201.Номер_договора,
                     T201.Дата_договора_ДУ) T0) T203 ON спн___дсв202.Номер_договора = T203.Номер_договора
      WHERE спн___дсв202.{2}) Итого222
   LEFT OUTER JOIN
     (SELECT CASE
                 WHEN T220.c1 IS NULL THEN T221.c1
                 ELSE T220.c1
             END c1,
             CASE
                 WHEN T220.Номер_договора IS NULL THEN T221.Номер_договора
                 ELSE T220.Номер_договора
             END Номер_договора,
             CASE
                 WHEN T220.Дата_договора_ДУ IS NULL THEN T221.Дата_договора_ДУ
                 ELSE T220.Дата_договора_ДУ
             END Дата_договора_ДУ,
             sum(CASE
                     WHEN T220.передано_всего IS NULL THEN 0
                     ELSE T220.передано_всего
                 END) передано_всего,
             sum(CASE
                     WHEN T221.c4 IS NULL THEN 0
                     ELSE T221.c4
                 END) c5,
             sum(CASE
                     WHEN T221.c5 IS NULL THEN 0
                     ELSE T221.c5
                 END) c6,
             (sum(CASE
                      WHEN T220.передано_всего IS NULL THEN 0
                      ELSE T220.передано_всего
                  END) - sum(CASE
                                 WHEN T221.c4 IS NULL THEN 0
                                 ELSE T221.c4
                             END)) + sum(CASE
                                             WHEN T221.c5 IS NULL THEN 0
                                             ELSE T221.c5
                                         END) c7,
             T220.c5 c8
      FROM
        (SELECT T0.C0 c1,
                T0.C1 Номер_договора,
                T0.C2 Дата_договора_ДУ,
                sum(T0.C3) over (partition BY T0.C1) передано_всего,
                               T0.C4 c5
         FROM
           (SELECT T210.c1 C0,
                   T210.Номер_договора C1,
                   T210.Дата_договора_ДУ C2,
                   sum(T210.c4) C3,
                   T210.c8 C4
            FROM
              (SELECT T204.c9 c1,
                      LTRIM(RTRIM(coguda10.REGNUM)) Номер_договора,
                      coguda10.REGDATE Дата_договора_ДУ,
                      sum(CASE
                              WHEN T206.c3 IS NULL THEN CASE
                                                                WHEN T206.c4 IS NULL THEN 0
                                                                ELSE T206.c4
                                                            END
                              ELSE T206.c3
                          END) c4,
                      CASE
                          WHEN T206.c14 IS NULL THEN CASE
                                                             WHEN T206.c13 IS NULL THEN CASE
                                                                                                WHEN T206.c14 IS NULL THEN CASE
                                                                                                                                   WHEN T206.c13 IS NULL THEN T206.c8
                                                                                                                                   ELSE T206.c13
                                                                                                                               END
                                                                                                ELSE T206.c14
                                                                                            END
                                                             ELSE T206.c13
                                                         END
                          ELSE T206.c14
                      END c5,
                      sum(T206.c5) c6,
                      T207.NAME Направление,
                      coguda10.DISSOLDATE c8
               FROM
                 (SELECT Контрагенты.ID ID,
                         T2.ID ID_юрлица,
                         T2.CONTRAGENTID ID_контрагента,
                         Контрагенты.NAME Контрагент,
                         Контрагенты.TYPE Тип_контрагента,
                         T3.STATUS c6,
                         T2.FULLNAME c7,
                         T2.SHORTNAME c8,
                         T2.FORMALIZEDNAME c9,
                         T2.REGISTRATIONNUM ОГРН,
                         T2.REGISTRATOR c11,
                         T2.REGISTRATIONDATE c12,
                         T2.CLOSEDATE Дата_ликвидации,
                         T2.INN ИНН,
                         T2.HEADPOSITION c15,
                         T2.HEADFULLNAME c16,
                         replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                         replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                         replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                         T2.PHONE Телефон,
                         T2.FAX Факс,
                         replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                         T2.SITE WEB_cайт,
                         T2.COMMENT Коментарий,
                         T2.OKPP OKPP,
                         T2.DECLNAME c26,
                         T2.LETTERWHO Письмо_кому,
                         T2.TRANSFDOCKIND c28,
                         Статус_юрлица.NAME Статус_юрлица
                  FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                         INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                        INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                  LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                  WHERE Контрагенты.TYPE = 'УК'
                    OR Контрагенты.TYPE = 'ГУК') T204,
                    PFR_BASIC.CONTRACT coguda10,
                    PFR_BASIC.CONTRACTNAME coguda11,
                    PFR_BASIC.DOCTYPE coguda12,

                 (SELECT rt.SUM c3,
                         rt.SUM c4,
                         rt.INVESTDOHOD c5,
                         rt.CONTRL_SPN_DATE c8,
                         rt.CONTRL_ADD_SPN_DATE c13,
                         rt.CONTRL_SPN_DATE c14,
                         rt.ACT_DATE c17,
                         rt.STATUS Статус,
                         rt.SI_TR_ID ID_перечисления
                  FROM PFR_BASIC.REQ_TRANSFER rt
                  LEFT OUTER JOIN
                    (SELECT t.REQ_TR_ID,
                            t.DRAFT_REGNUM,
                            t.DRAFT_DATE,
                            t.DRAFT_PAYDATE,
                            t.DRAFT_AMOUNT,
                            t.DIRECTION_EL_ID,
                            rank() over (partition BY t.REQ_TR_ID
                                         ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                   rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                ORDER BY t.ID DESC) rnID
                     FROM PFR_BASIC.ASG_FIN_TR t
                     WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                  AND 1 = pp.rn
                  AND 1 = pp.rnID
                  WHERE rt.STATUS_ID <> -1) T206,
                    PFR_BASIC.DIRECTION_SPN T207,
                    PFR_BASIC.SI_REGISTER T208,
                    PFR_BASIC.SI_TRANSFER Перечисление209
               WHERE NOT CASE
                             WHEN T206.c14 IS NULL THEN CASE
                                                                WHEN T206.c13 IS NULL THEN CASE
                                                                                                   WHEN T206.c14 IS NULL THEN CASE
                                                                                                                                      WHEN T206.c13 IS NULL THEN T206.c8
                                                                                                                                      ELSE T206.c13
                                                                                                                                  END
                                                                                                   ELSE T206.c14
                                                                                               END
                                                                ELSE T206.c13
                                                            END
                             ELSE T206.c14
                         END IS NULL
                 AND CASE
                         WHEN T206.c14 IS NULL THEN CASE
                                                            WHEN T206.c13 IS NULL THEN CASE
                                                                                               WHEN T206.c14 IS NULL THEN CASE
                                                                                                                                  WHEN T206.c13 IS NULL THEN T206.c8
                                                                                                                                  ELSE T206.c13
                                                                                                                              END
                                                                                               ELSE T206.c14
                                                                                           END
                                                            ELSE T206.c13
                                                        END
                         ELSE T206.c14
                     END < {0}
                 AND T207.NAME = 'из ПФР в ГУК ВР'
                 AND coguda10.TYPEID = 3
                 AND (T206.Статус = 'Акт подписан'
                      OR T206.Статус = 'СПН перечислены')
                 AND T204.ID_юрлица = coguda10.LE_ID
                 AND coguda10.ID = Перечисление209.CONTR_ID
                 AND T208.ID = Перечисление209.SI_REG_ID
                 AND T207.ID = T208.DIR_SPN_ID
                 AND Перечисление209.ID = T206.ID_перечисления
                 AND T208.STATUS_ID <> -1
                 AND coguda11.ID = coguda10.CONT_N_ID
                 AND coguda12.ID = coguda10.DT_ID
               GROUP BY T204.c9,
                        LTRIM(RTRIM(coguda10.REGNUM)),
                        coguda10.REGDATE,
                        CASE
                            WHEN T206.c14 IS NULL THEN CASE
                                                               WHEN T206.c13 IS NULL THEN CASE
                                                                                                  WHEN T206.c14 IS NULL THEN CASE
                                                                                                                                     WHEN T206.c13 IS NULL THEN T206.c8
                                                                                                                                     ELSE T206.c13
                                                                                                                                 END
                                                                                                  ELSE T206.c14
                                                                                              END
                                                               ELSE T206.c13
                                                           END
                            ELSE T206.c14
                        END,
                        T207.NAME,
                        coguda10.DISSOLDATE) T210
            GROUP BY T210.c1,
                     T210.Номер_договора,
                     T210.Дата_договора_ДУ,
                     T210.c8) T0) T220
      FULL OUTER JOIN
        (SELECT T0.C0 c1,
                T0.C1 Номер_договора,
                T0.C2 Дата_договора_ДУ,
                sum(T0.C3) over (partition BY T0.C1) c4,
                               sum(T0.C4) over (partition BY T0.C1) c5
         FROM
           (SELECT T219.c1 C0,
                   T219.Номер_договора C1,
                   T219.Дата_договора_ДУ C2,
                   sum(T219.c4) C3,
                   sum(T219.c6) C4
            FROM
              (SELECT T211.c9 c1,
                      LTRIM(RTRIM(Договора_с_УК212.Номер)) Номер_договора,
                      Договора_с_УК212.c8 Дата_договора_ДУ,
                      sum(CASE
                              WHEN T213.c3 IS NULL THEN CASE
                                                                WHEN T213.c4 IS NULL THEN 0
                                                                ELSE T213.c4
                                                            END
                              ELSE T213.c3
                          END) c4,
                      CASE
                          WHEN T213.c14 IS NULL THEN CASE
                                                             WHEN T213.c13 IS NULL THEN CASE
                                                                                                WHEN T213.c14 IS NULL THEN CASE
                                                                                                                                   WHEN T213.c13 IS NULL THEN T213.c8
                                                                                                                                   ELSE T213.c13
                                                                                                                               END
                                                                                                ELSE T213.c14
                                                                                            END
                                                             ELSE T213.c13
                                                         END
                          ELSE T213.c14
                      END c5,
                      sum(T213.c5) c6,
                      T214.Направление Направление,
                      Вид_операции215.Вид_операции Вид_операции,
                      T216.c30 c9
               FROM (
                       (SELECT T.ID ID,
                               T.ID_юрлица ID_юрлица,
                               T.ID_контрагента ID_контрагента,
                               T.Контрагент Контрагент,
                               T.Тип_контрагента Тип_контрагента,
                               T.c6 c6,
                               T.c7 c7,
                               T.c8 c8,
                               T.c9 c9,
                               T.ОГРН ОГРН,
                               T.c11 c11,
                               T.c12 c12,
                               T.Дата_ликвидации Дата_ликвидации,
                               T.ИНН ИНН,
                               T.c15 c15,
                               T.c16 c16,
                               T.c17 c17,
                               T.c18 c18,
                               T.Почтовый_адрес Почтовый_адрес,
                               T.Телефон Телефон,
                               T.Факс Факс,
                               T.c22 c22,
                               T.WEB_cайт WEB_cайт,
                               T.Коментарий Коментарий,
                               T.OKPP OKPP,
                               T.c26 c26,
                               T.Письмо_кому Письмо_кому,
                               T.c28 c28,
                               T.Статус_юрлица Статус_юрлица,
                               0 c30,
                               CURRENT_DATE c31
                        FROM
                          (SELECT Контрагенты.ID ID,
                                  T2.ID ID_юрлица,
                                  T2.CONTRAGENTID ID_контрагента,
                                  Контрагенты.NAME Контрагент,
                                  Контрагенты.TYPE Тип_контрагента,
                                  T3.STATUS c6,
                                  T2.FULLNAME c7,
                                  T2.SHORTNAME c8,
                                  T2.FORMALIZEDNAME c9,
                                  T2.REGISTRATIONNUM ОГРН,
                                  T2.REGISTRATOR c11,
                                  T2.REGISTRATIONDATE c12,
                                  T2.CLOSEDATE Дата_ликвидации,
                                  T2.INN ИНН,
                                  T2.HEADPOSITION c15,
                                  T2.HEADFULLNAME c16,
                                  replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                  replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                  replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                  T2.PHONE Телефон,
                                  T2.FAX Факс,
                                  replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                  T2.SITE WEB_cайт,
                                  T2.COMMENT Коментарий,
                                  T2.OKPP OKPP,
                                  T2.DECLNAME c26,
                                  T2.LETTERWHO Письмо_кому,
                                  T2.TRANSFDOCKIND c28,
                                  Статус_юрлица.NAME Статус_юрлица
                           FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                  INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                 INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                           LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                           WHERE Контрагенты.TYPE = 'УК'
                             OR Контрагенты.TYPE = 'ГУК') T
                        UNION SELECT T105.ID ID,
                                     T105.ID_юрлица ID_юрлица,
                                     T105.ID_контрагента ID_контрагента,
                                     T105.Контрагент Контрагент,
                                     T105.Тип_контрагента Тип_контрагента,
                                     T105.c6 c6,
                                     T2.OLDFULLNAME c7,
                                     T2.OLDSHORTNAME c8,
                                     T2.OLDNICKNAME c9,
                                     T105.ОГРН ОГРН,
                                     T105.c11 c11,
                                     T105.c12 c12,
                                     T105.Дата_ликвидации Дата_ликвидации,
                                     T105.ИНН ИНН,
                                     T105.c15 c15,
                                     T105.c16 c16,
                                     T105.c17 c17,
                                     T105.c18 c18,
                                     T105.Почтовый_адрес Почтовый_адрес,
                                     T105.Телефон Телефон,
                                     T105.Факс Факс,
                                     T105.c22 c22,
                                     T105.WEB_cайт WEB_cайт,
                                     T105.Коментарий Коментарий,
                                     T105.OKPP OKPP,
                                     T105.c26 c26,
                                     T105.Письмо_кому Письмо_кому,
                                     T105.c28 c28,
                                     T105.Статус_юрлица Статус_юрлица,
                                     1 c30,
                                     T2.DATECHANGE c31
                        FROM
                          (SELECT Контрагенты.ID ID,
                                  T2.ID ID_юрлица,
                                  T2.CONTRAGENTID ID_контрагента,
                                  Контрагенты.NAME Контрагент,
                                  Контрагенты.TYPE Тип_контрагента,
                                  T3.STATUS c6,
                                  T2.FULLNAME c7,
                                  T2.SHORTNAME c8,
                                  T2.FORMALIZEDNAME c9,
                                  T2.REGISTRATIONNUM ОГРН,
                                  T2.REGISTRATOR c11,
                                  T2.REGISTRATIONDATE c12,
                                  T2.CLOSEDATE Дата_ликвидации,
                                  T2.INN ИНН,
                                  T2.HEADPOSITION c15,
                                  T2.HEADFULLNAME c16,
                                  replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                  replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                  replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                  T2.PHONE Телефон,
                                  T2.FAX Факс,
                                  replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                  T2.SITE WEB_cайт,
                                  T2.COMMENT Коментарий,
                                  T2.OKPP OKPP,
                                  T2.DECLNAME c26,
                                  T2.LETTERWHO Письмо_кому,
                                  T2.TRANSFDOCKIND c28,
                                  Статус_юрлица.NAME Статус_юрлица
                           FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                  INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                 INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                           LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                           WHERE Контрагенты.TYPE = 'УК'
                             OR Контрагенты.TYPE = 'ГУК') T105,
                             PFR_BASIC.OLDSINAME T2
                        WHERE T105.ID_юрлица = T2.LEGALENTITYID) T216
                     LEFT OUTER JOIN (((((
                                            (SELECT PFR_BASIC.SI_REGISTER.ID ID,
                                                    PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                                    PFR_BASIC.SI_REGISTER.DIR_SPN_ID c9
                                             FROM PFR_BASIC.SI_REGISTER
                                             WHERE STATUS_ID <> -1) T217
                                          INNER JOIN
                                            (SELECT Перечисление.ID ID,
                                                    Перечисление.CONTR_ID ID_договора_с_УК,
                                                    Перечисление.SI_REG_ID c3
                                             FROM PFR_BASIC.SI_TRANSFER Перечисление) Перечисление218 ON T217.ID = Перечисление218.c3)
                                         INNER JOIN
                                           (SELECT rt.SUM c3,
                                                   rt.SUM c4,
                                                   rt.INVESTDOHOD c5,
                                                   rt.CONTRL_SPN_DATE c8,
                                                   rt.CONTRL_ADD_SPN_DATE c13,
                                                   rt.CONTRL_SPN_DATE c14,
                                                   rt.ACT_DATE c17,
                                                   rt.STATUS Статус,
                                                   rt.SI_TR_ID ID_перечисления
                                            FROM PFR_BASIC.REQ_TRANSFER rt
                                            LEFT OUTER JOIN
                                              (SELECT t.REQ_TR_ID,
                                                      t.DRAFT_REGNUM,
                                                      t.DRAFT_DATE,
                                                      t.DRAFT_PAYDATE,
                                                      t.DRAFT_AMOUNT,
                                                      t.DIRECTION_EL_ID,
                                                      rank() over (partition BY t.REQ_TR_ID
                                                                   ORDER BY t.DRAFT_PAYDATE DESC) rn,
                                                             rank() over (partition BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                          ORDER BY t.ID DESC) rnID
                                               FROM PFR_BASIC.ASG_FIN_TR t
                                               WHERE t.STATUS_ID <> -1) pp ON rt.ID = pp.REQ_TR_ID
                                            AND 1 = pp.rn
                                            AND 1 = pp.rnID
                                            WHERE rt.STATUS_ID <> -1) T213 ON Перечисление218.ID = T213.ID_перечисления)
                                        INNER JOIN
                                          (SELECT Договора_с_УК.ID ID,
                                                  Договора_с_УК.LE_ID ID_юрлица,
                                                  Договора_с_УК.REGDATE c8,
                                                  Договора_с_УК.REGNUM Номер,
                                                  Договора_с_УК.DISSOLDATE c13,
                                                  Договора_с_УК.TYPEID Тип_портфеля
                                           FROM PFR_BASIC.CONTRACT Договора_с_УК,
                                                PFR_BASIC.CONTRACTNAME T2,
                                                PFR_BASIC.DOCTYPE Тип_документа
                                           WHERE T2.ID = Договора_с_УК.CONT_N_ID
                                             AND Тип_документа.ID = Договора_с_УК.DT_ID) Договора_с_УК212 ON Договора_с_УК212.ID = Перечисление218.ID_договора_с_УК)
                                       INNER JOIN
                                         (SELECT Контрагенты.ID ID,
                                                 T2.ID ID_юрлица,
                                                 T2.CONTRAGENTID ID_контрагента,
                                                 Контрагенты.NAME Контрагент,
                                                 Контрагенты.TYPE Тип_контрагента,
                                                 T3.STATUS c6,
                                                 T2.FULLNAME c7,
                                                 T2.SHORTNAME c8,
                                                 T2.FORMALIZEDNAME c9,
                                                 T2.REGISTRATIONNUM ОГРН,
                                                 T2.REGISTRATOR c11,
                                                 T2.REGISTRATIONDATE c12,
                                                 T2.CLOSEDATE Дата_ликвидации,
                                                 T2.INN ИНН,
                                                 T2.HEADPOSITION c15,
                                                 T2.HEADFULLNAME c16,
                                                 replace(replace(replace(T2.LEGALADDRESS, ',', ', '), ';', '; '), '  ', ' ') c17,
                                                 replace(replace(replace(T2.STREETADDRESS, ',', ', '), ';', '; '), '  ', ' ') c18,
                                                 replace(replace(replace(T2.POSTADDRESS, ',', ', '), ';', '; '), '  ', ' ') Почтовый_адрес,
                                                 T2.PHONE Телефон,
                                                 T2.FAX Факс,
                                                 replace(replace(replace(T2.EADDRESS, ',', ', '), ';', '; '), '  ', ' ') c22,
                                                 T2.SITE WEB_cайт,
                                                 T2.COMMENT Коментарий,
                                                 T2.OKPP OKPP,
                                                 T2.DECLNAME c26,
                                                 T2.LETTERWHO Письмо_кому,
                                                 T2.TRANSFDOCKIND c28,
                                                 Статус_юрлица.NAME Статус_юрлица
                                          FROM ((PFR_BASIC.CONTRAGENT Контрагенты
                                                 INNER JOIN PFR_BASIC.LEGALENTITY T2 ON Контрагенты.ID = T2.CONTRAGENTID)
                                                INNER JOIN PFR_BASIC.STATUS T3 ON T3.ID = Контрагенты.STATUS_ID)
                                          LEFT OUTER JOIN PFR_BASIC.LEGALSTATUS Статус_юрлица ON T2.LS_ID = Статус_юрлица.ID
                                          WHERE Контрагенты.TYPE = 'УК'
                                            OR Контрагенты.TYPE = 'ГУК') T211 ON T211.ID_юрлица = Договора_с_УК212.ID_юрлица)
                                      INNER JOIN
                                        (SELECT T1.ID ID,
                                                T1.NAME Направление
                                         FROM PFR_BASIC.DIRECTION_SPN T1) T214 ON T214.ID = T217.c9) ON T216.ID_юрлица = Договора_с_УК212.ID_юрлица)
               LEFT OUTER JOIN
                 (SELECT Вид_операции.ID ID,
                         Вид_операции.OPERATION Вид_операции
                  FROM PFR_BASIC.OPERATION Вид_операции) Вид_операции215 ON T217.c8 = Вид_операции215.ID
               WHERE NOT CASE
                             WHEN T213.c14 IS NULL THEN CASE
                                                                WHEN T213.c13 IS NULL THEN CASE
                                                                                                   WHEN T213.c14 IS NULL THEN CASE
                                                                                                                                      WHEN T213.c13 IS NULL THEN T213.c8
                                                                                                                                      ELSE T213.c13
                                                                                                                                  END
                                                                                                   ELSE T213.c14
                                                                                               END
                                                                ELSE T213.c13
                                                            END
                             ELSE T213.c14
                         END IS NULL
                 AND CASE
                         WHEN T213.c14 IS NULL THEN CASE
                                                            WHEN T213.c13 IS NULL THEN CASE
                                                                                               WHEN T213.c14 IS NULL THEN CASE
                                                                                                                                  WHEN T213.c13 IS NULL THEN T213.c8
                                                                                                                                  ELSE T213.c13
                                                                                                                              END
                                                                                               ELSE T213.c14
                                                                                           END
                                                            ELSE T213.c13
                                                        END
                         ELSE T213.c14
                     END < {0}
                 AND T214.Направление = 'из ГУК ВР в ПФР'
                 AND Договора_с_УК212.Тип_портфеля = 3
                 AND (T213.Статус = 'Акт подписан'
                      OR T213.Статус = 'СПН перечислены')
               GROUP BY T211.c9,
                        LTRIM(RTRIM(Договора_с_УК212.Номер)),
                        Договора_с_УК212.c8,
                        CASE
                            WHEN T213.c14 IS NULL THEN CASE
                                                               WHEN T213.c13 IS NULL THEN CASE
                                                                                                  WHEN T213.c14 IS NULL THEN CASE
                                                                                                                                     WHEN T213.c13 IS NULL THEN T213.c8
                                                                                                                                     ELSE T213.c13
                                                                                                                                 END
                                                                                                  ELSE T213.c14
                                                                                              END
                                                               ELSE T213.c13
                                                           END
                            ELSE T213.c14
                        END,
                        T214.Направление,
                        Вид_операции215.Вид_операции,
                        T216.c30) T219
            GROUP BY T219.c1,
                     T219.Номер_договора,
                     T219.Дата_договора_ДУ) T0) T221 ON T220.Номер_договора = T221.Номер_договора
      GROUP BY CASE
                   WHEN T220.c1 IS NULL THEN T221.c1
                   ELSE T220.c1
               END,
               CASE
                   WHEN T220.Номер_договора IS NULL THEN T221.Номер_договора
                   ELSE T220.Номер_договора
               END,
               CASE
                   WHEN T220.Дата_договора_ДУ IS NULL THEN T221.Дата_договора_ДУ
                   ELSE T220.Дата_договора_ДУ
               END,
               T220.c5) T223 ON Итого222.Номер_договора = T223.Номер_договора
   ) T0", $"date('{startDate:yyyy-MM-dd}')", $"date('{endDate:yyyy-MM-dd}')", $"Расторженные = {(showOldContracts ? "Расторженные" : "0")}");

            #endregion

           // File.WriteAllText(@"d:\temp\_q.txt", query);
            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(query)
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!  
                    .AddScalar("RegDate", NHibernateUtil.DateTime) 
                    .AddScalar("SaldoStart", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUDsv", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUMsk", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUTotalTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUTotalYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUPravTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUPravYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUDsvTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUDsvYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("PeretokTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("PeretokYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUMskTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUMskYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUSum", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SaldoEnd", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport18Item>())
                    .List<SPNReport18Item>();
                //Создание и отдача отчёта

                var param = new List<ReportParameter>
                {
                     new ReportParameter("StartDateStr", startDate.Date.ToString("dd.MM.yyyy")),
                     new ReportParameter("EndDateStr", endDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };

                var fileName = $"Движение средств ВР с {startDate.Date:dd.MM.yyyy} по {endDate.Date:dd.MM.yyyy}";
                return RenderReport("SPNReport_18_MoneyFlow_Range.rdlc", fileName, param, data, type);
            }
        }

        public ReportFile CreateReportSPN17(DateTime reportDate, ReportFile.Types type)
        {
            #region Cognos query

            var query = @"-- ЗАПРОС

SELECT
  T0.C1  FName,
  T0.C2  RegNum,
  T0.C3  RegDate,
  T0.C4  SendedDUTotal,
  T0.C5  SendedDUDsv,
  T0.C6  SendedDUMsk,
  T0.C7  ReturnDUTotalTotal,
  T0.C8  ReturnDUTotalYield,
  T0.C9  PeretokTotal,
  T0.C10 PeretokYield,
  T0.C11 ReturnDUPravTotal,
  T0.C12 ReturnDUPravYield,
  T0.C13 ReturnDUDsvTotal,
  T0.C14 ReturnDUDsvYield,
  T0.C15 ReturnDUMskTotal,
  T0.C16 ReturnDUMskYield,

  T0.C17 ReturnSPNTotal,
  T0.C18 ReturnSPNTotalYield,
  T0.C19 ReturnSPNNchtp,
  T0.C20 ReturnSPNNchtpYield,
  T0.C21 ReturnSPNSPV,
  T0.C22 ReturnSPNSPVYield,
  T0.C23 ReturnSPNEV,
  T0.C24 ReturnSPNEVYield,
  T0.C25 SaldoEnd
  
FROM (
       SELECT
         T0.C0  C0,
         T0.C1  C1,
         T0.C2  C2,
         T0.C3  C3,
         T0.C4  C4,
         T0.C5  C5,
         T0.C6  C6,
         T0.C7  C7,
         T0.C8  C8,
         T0.C9  C9,
         T0.C10 C10,
         T0.C11 C11,
         T0.C12 C12,
         T0.C13 C13,
         T0.C14 C14,
         T0.C15 C15,
         T0.C16 C16,
         T0.C17 C17,
         T0.C18 C18,
         T0.C19 C19,
         T0.C20 C20,
         T0.C21 C21,
         T0.C22 C22,
         T0.C23 C23,
         T0.C24 C24,
         T0.C25 C25
        ,T0.Тип_контрагента,
REGCLOSE,
                       REGADDCLOSE,
                       DISSOLDATE
       FROM (
              SELECT
                min(COALESCE(T38.Номер_договора,T39.Номер_договора)) C0,
                COALESCE(T38.c1,T39.c1)                           C1,
               COALESCE(T38.Номер_договора,T39.Номер_договора)      C2,
                 COALESCE(T38.Дата_договора_ДУ,T39.Дата_договора_ДУ)             C3,
                sum(COALESCE(T38.Сумма_всего,T39.Сумма_всего))             C4,
                sum(COALESCE(T38.Сумма_ДСВ,T39.Сумма_ДСВ))               C5,
                sum(COALESCE(T38.Сумма_МСК,T39.Сумма_МСК))               C6,
                sum(T39.Сумма_всего)             C7,
                sum(T39.c6)                      C8,
                sum(T39.c19)                     C9,
                sum(T39.c20)                     C10,
                sum(T39.Сумма_правопр_)          C11,
                sum(T39.c18)                     C12,
                sum(T39.Сумма_ДСВ)               C13,
                sum(T39.c14)                     C14,
                sum(T39.Сумма_МСК)               C15,
                sum(T39.c16)                     C16,
                sum(T39.c21)                     C17,
                sum(T39.c22)                     C18,
                sum(T39.Сумма_НЧТП)              C19,
                sum(T39.c8)                      C20,
                sum(T39.Сумма_СПВ)               C21,
                sum(T39.c10)                     C22,
                sum(T39.Сумма_ЕВ)                C23,
                sum(T39.c12)                     C24,
                sum(((coalesce(T38.Сумма_всего, 0)) - (coalesce(T39.Сумма_всего, 0))) +
                    (coalesce(T39.c6, 0)))       C25
              ,T39.Тип_контрагента,
                REGCLOSE,
                       REGADDCLOSE,
                       DISSOLDATE

              FROM (
                     SELECT

                       T29.c1               c1,
                       T29.Номер_договора   Номер_договора,
                       T29.Дата_договора_ДУ Дата_договора_ДУ,
                       T29.Тип_контрагента  Тип_контрагента,
                       sum(T29.c4)          Сумма_всего,
                       sum(T29.c6)          c6,
                       sum(CASE WHEN T29.Вид_операции IN
                                     ('ДСВ Перераспределение СПН', 'ДСВ Расформирование портфеля квартала')
                         THEN T29.c4
                           ELSE 0 END)          Сумма_ДСВ,
                       sum(CASE WHEN T29.Вид_операции IN
                                     ('ДСВ Перераспределение СПН', 'ДСВ Расформирование портфеля квартала')
                         THEN T29.c6
                           ELSE 0 END)          c8,
                       sum(CASE WHEN T29.Вид_операции IN
                                     ('МСК Перераспределение СПН', 'МСК Расформирование портфеля полугодия')
                         THEN T29.c4
                           ELSE 0 END)          Сумма_МСК,
                       sum(CASE WHEN T29.Вид_операции IN
                                     ('МСК Перераспределение СПН', 'МСК Расформирование портфеля полугодия')
                         THEN T29.c6
                           ELSE 0 END)          c10,
                      REGCLOSE,
                       REGADDCLOSE,
                       DISSOLDATE,
                       T29.ID_юрлица
                     FROM (
                            SELECT
                              T22.c9                    c1,
                              Договора_с_УК.Номер       Номер_договора,
                              Договора_с_УК.c8          Дата_договора_ДУ,
                              Договора_с_УК.REGCLOSE,
                              Договора_с_УК.REGADDCLOSE,
                              Договора_с_УК.DISSOLDATE,
                              sum(CASE WHEN T24.c3 IS NULL
                                THEN CASE WHEN T24.c4 IS NULL
                                  THEN 0
                                     ELSE T24.c4 END
                                  ELSE T24.c3 END)      c4,
                              T24.c17                   c5,
                              sum(T24.c5)               c6,
                              T25.Направление           Направление,
                              T22.Тип_контрагента       Тип_контрагента,
                              Вид_операции.Вид_операции Вид_операции,
                              date('{0}')            c10,
                              Договора_с_УК.ID_юрлица
                            FROM ((((((
                                        SELECT
                                          PFR_BASIC.SI_REGISTER.ID           ID,
                                          PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                          PFR_BASIC.SI_REGISTER.DIR_SPN_ID   c9
                                        FROM PFR_BASIC.SI_REGISTER
                                        WHERE STATUS_ID <> -1) T27 INNER JOIN (
                                                                                    SELECT
                                                                                      Перечисление.ID        ID,
                                                                                      Перечисление.CONTR_ID  ID_договора_с_УК,
                                                                                      Перечисление.SI_REG_ID c3
                                                                                    FROM
                                                                                      PFR_BASIC.SI_TRANSFER Перечисление) Перечисление
                                ON T27.ID = Перечисление.c3) INNER JOIN (
                                                                                  SELECT
                                                                                    rt.SUM         c3,
                                                                                    rt.SUM         c4,
                                                                                    rt.INVESTDOHOD c5,
                                                                                    rt.ACT_DATE    c17,
                                                                                    rt.SI_TR_ID    ID_перечисления
                                                                                  FROM
                                                                                    PFR_BASIC.REQ_TRANSFER rt LEFT OUTER JOIN
                                                                                    (
                                                                                      SELECT
                                                                                        t.REQ_TR_ID,
                                                                                        t.DRAFT_REGNUM,
                                                                                        t.DRAFT_DATE,
                                                                                        t.DRAFT_PAYDATE,
                                                                                        t.DRAFT_AMOUNT,
                                                                                        t.DIRECTION_EL_ID,
                                                                                        rank()
                                                                                        OVER (PARTITION BY t.REQ_TR_ID
                                                                                          ORDER BY
                                                                                            t.DRAFT_PAYDATE DESC) rn,
                                                                                        rank()
                                                                                        OVER (PARTITION BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                                          ORDER BY t.ID DESC)     rnID
                                                                                      FROM PFR_BASIC.ASG_FIN_TR t
                                                                                      WHERE t.STATUS_ID <> -1) pp
                                                                                      ON
                                                                                        rt.ID = pp.REQ_TR_ID AND
                                                                                        1 = pp.rn AND
                                                                                        1 = pp.rnID
                                                                                  WHERE rt.STATUS_ID <> -1) T24
                                ON Перечисление.ID = T24.ID_перечисления) INNER JOIN (
                                                                                               SELECT
                                                                                                 Договора_с_УК.ID      ID,
                                                                                                 Договора_с_УК.LE_ID   ID_юрлица,
                                                                                                 Договора_с_УК.REGDATE c8,
                                                                                                 Договора_с_УК.REGNUM  Номер,
                                                                                                 Договора_с_УК.REGCLOSE,
                                                                                                 Договора_с_УК.REGADDCLOSE,
                                                                                                 Договора_с_УК.DISSOLDATE
                                                                                               FROM
                                                                                                 PFR_BASIC.CONTRACT Договора_с_УК,
                                                                                                 PFR_BASIC.CONTRACTNAME T2,
                                                                                                 PFR_BASIC.DOCTYPE Тип_документа
                                                                                               WHERE T2.ID =
                                                                                                     Договора_с_УК.CONT_N_ID
                                                                                                     AND
                                                                                                     Тип_документа.ID
                                                                                                     =
                                                                                                     Договора_с_УК.DT_ID
                                                                                                       and Договора_с_УК.TYPEID = 3) Договора_с_УК
                                ON Договора_с_УК.ID = Перечисление.ID_договора_с_УК) INNER JOIN (
                                                                                                          SELECT
                                                                                                            T2.ID             ID_юрлица,
                                                                                                            Контрагенты.TYPE  Тип_контрагента,
                                                                                                            T2.FORMALIZEDNAME c9
                                                                                                          FROM ((
                                                                                                              PFR_BASIC.CONTRAGENT Контрагенты INNER JOIN
                                                                                                              PFR_BASIC.LEGALENTITY T2
                                                                                                                ON
                                                                                                                  Контрагенты.ID
                                                                                                                  =
                                                                                                                  T2.CONTRAGENTID) INNER JOIN
                                                                                                            PFR_BASIC.STATUS T3
                                                                                                              ON
                                                                                                                T3.ID
                                                                                                                =
                                                                                                                Контрагенты.STATUS_ID) LEFT OUTER JOIN
                                                                                                            PFR_BASIC.LEGALSTATUS Статус_юрлица
                                                                                                              ON
                                                                                                                T2.LS_ID
                                                                                                                =
                                                                                                                Статус_юрлица.ID
                                                                                                          ) T22
                                ON T22.ID_юрлица = Договора_с_УК.ID_юрлица) INNER JOIN (
                                                                                                 SELECT
                                                                                                   T1.ID   ID,
                                                                                                   T1.NAME Направление
                                                                                                 FROM
                                                                                                   PFR_BASIC.DIRECTION_SPN T1) T25
                                ON T25.ID = T27.c9) LEFT OUTER JOIN (
                                                                              SELECT
                                                                                Вид_операции.ID        ID,
                                                                                Вид_операции.OPERATION Вид_операции
                                                                              FROM
                                                                                PFR_BASIC.OPERATION Вид_операции) Вид_операции
                                ON T27.c8 = Вид_операции.ID
                            WHERE NOT T24.c17 IS NULL AND T24.c17 <= date('{0}') AND
                                  T25.Направление = 'из ПФР в ГУК ВР'
                            GROUP BY T22.c9, Договора_с_УК.Номер, Договора_с_УК.c8,Договора_с_УК.DISSOLDATE,Договора_с_УК.REGADDCLOSE,Договора_с_УК.REGCLOSE, T24.c17,
                              T25.Направление, T22.Тип_контрагента, Вид_операции.Вид_операции, Договора_с_УК.ID_юрлица) T29
                     GROUP BY T29.c1, T29.Номер_договора, T29.Дата_договора_ДУ,
                       T29.Тип_контрагента, REGCLOSE,
                       REGADDCLOSE,
                       DISSOLDATE, T29.ID_юрлица) T38 full JOIN (
                                                                        SELECT
                                                                          T37.c1               c1,
                                                                          T37.Номер_договора   Номер_договора,
                                                                          T37.Дата_договора_ДУ Дата_договора_ДУ,
                                                                          T37.Тип_контрагента  Тип_контрагента,
                                                                          sum(T37.c4)          Сумма_всего,
                                                                          sum(T37.c6)          c6,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование НЧТП')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_НЧТП,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование НЧТП')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c8,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование СПВ')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_СПВ,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование СПВ')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c10,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование ЕВ')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_ЕВ,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Отзыв средств на финансирование ЕВ')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c12,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('ДСВ Перераспределение СПН')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_ДСВ,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('ДСВ Перераспределение СПН')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c14,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('МСК Перераспределение СПН')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_МСК,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('МСК Перераспределение СПН')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c16,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Выплаты правопреемникам')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          Сумма_правопр_,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Выплаты правопреемникам')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c18,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Перераспределение СПН', 'Возврат СПН - расторжение')
                                                                            THEN T37.c4
                                                                              ELSE 0 END)          c19,
                                                                          sum(CASE WHEN T37.Вид_операции IN
                                                                                        ('Перераспределение СПН', 'Возврат СПН - расторжение')
                                                                            THEN T37.c6
                                                                              ELSE 0 END)          c20,
                                                                          sum((CASE WHEN (T37.Вид_операции IN
                                                                                          ('Отзыв средств на финансирование НЧТП'))
                                                                            THEN T37.c4
                                                                               ELSE 0 END +
                                                                               CASE WHEN (T37.Вид_операции IN
                                                                                          ('Отзыв средств на финансирование СПВ'))
                                                                                 THEN T37.c4
                                                                               ELSE 0 END) +
                                                                              CASE WHEN (T37.Вид_операции IN
                                                                                         ('Отзыв средств на финансирование ЕВ'))
                                                                                THEN T37.c4
                                                                              ELSE 0 END)          c21,
                                                                          sum((CASE WHEN (T37.Вид_операции IN
                                                                                          ('Отзыв средств на финансирование НЧТП'))
                                                                            THEN T37.c6
                                                                               ELSE 0 END +
                                                                               CASE WHEN (T37.Вид_операции IN
                                                                                          ('Отзыв средств на финансирование СПВ'))
                                                                                 THEN T37.c6
                                                                               ELSE 0 END) +
                                                                              CASE WHEN (T37.Вид_операции IN
                                                                                         ('Отзыв средств на финансирование ЕВ'))
                                                                                THEN T37.c6
                                                                              ELSE 0 END)          c22
                                                                        , T37.ID_юрлица
                                                                        FROM (
                                                                               SELECT
                                                                                 T30.c9                      c1,
                                                                                 Договора_с_УК31.Номер       Номер_договора,
                                                                                 Договора_с_УК31.c8          Дата_договора_ДУ,
                                                                                 sum(CASE WHEN T32.c3 IS NULL
                                                                                   THEN CASE WHEN T32.c4 IS NULL
                                                                                     THEN 0
                                                                                        ELSE T32.c4 END
                                                                                     ELSE T32.c3 END)        c4,
                                                                                 T32.c17                     c5,
                                                                                 sum(T32.c5)                 c6,
                                                                                 T33.Направление             Направление,
                                                                                 Вид_операции34.Вид_операции Вид_операции,
                                                                                 T30.Тип_контрагента         Тип_контрагента,
                                                                                 date('{0}')              c10,
                                                                                 Договора_с_УК31.ID_юрлица
                                                                               FROM ((((((
                                                                                           SELECT
                                                                                             PFR_BASIC.SI_REGISTER.ID           ID,
                                                                                             PFR_BASIC.SI_REGISTER.OPERATION_ID c8,
                                                                                             PFR_BASIC.SI_REGISTER.DIR_SPN_ID   c9
                                                                                           FROM
                                                                                             PFR_BASIC.SI_REGISTER
                                                                                           WHERE STATUS_ID <>
                                                                                                 -1) T35 INNER JOIN (
                                                                                                                        SELECT
                                                                                                                          Перечисление.ID        ID,
                                                                                                                          Перечисление.CONTR_ID  ID_договора_с_УК,
                                                                                                                          Перечисление.SI_REG_ID c3
                                                                                                                        FROM
                                                                                                                          PFR_BASIC.SI_TRANSFER Перечисление) Перечисление36
                                                                                   ON T35.ID =
                                                                                      Перечисление36.c3) INNER JOIN
                                                                                 (
                                                                                   SELECT
                                                                                     rt.SUM         c3,
                                                                                     rt.SUM         c4,
                                                                                     rt.INVESTDOHOD c5,
                                                                                     rt.ACT_DATE    c17,
                                                                                     rt.SI_TR_ID    ID_перечисления

                                                                                   FROM
                                                                                     PFR_BASIC.REQ_TRANSFER rt LEFT OUTER JOIN
                                                                                     (
                                                                                       SELECT
                                                                                         t.REQ_TR_ID,
                                                                                         t.DRAFT_REGNUM,
                                                                                         t.DRAFT_DATE,
                                                                                         t.DRAFT_PAYDATE,
                                                                                         t.DRAFT_AMOUNT,
                                                                                         t.DIRECTION_EL_ID,
                                                                                         rank()
                                                                                         OVER (PARTITION BY t.REQ_TR_ID
                                                                                           ORDER BY
                                                                                             t.DRAFT_PAYDATE DESC) rn,
                                                                                         rank()
                                                                                         OVER (PARTITION BY t.REQ_TR_ID, t.DRAFT_PAYDATE
                                                                                           ORDER BY
                                                                                             t.ID DESC)            rnID
                                                                                       FROM PFR_BASIC.ASG_FIN_TR t
                                                                                       WHERE t.STATUS_ID <> -1) pp
                                                                                       ON rt.ID = pp.REQ_TR_ID
                                                                                          AND 1 = pp.rn AND
                                                                                          1 = pp.rnID
                                                                                   WHERE rt.STATUS_ID <> -1) T32
                                                                                   ON Перечисление36.ID =
                                                                                      T32.ID_перечисления) INNER JOIN
                                                                                 (
                                                                                   SELECT
                                                                                     Договора_с_УК.ID      ID,
                                                                                     Договора_с_УК.LE_ID   ID_юрлица,
                                                                                     Договора_с_УК.REGDATE c8,
                                                                                     Договора_с_УК.REGNUM  Номер,
                                                                                     Договора_с_УК.DISSOLDATE,
                                                                                     Договора_с_УК.REGADDCLOSE,
                                                                                     Договора_с_УК.REGCLOSE
                                                                                   FROM
                                                                                     PFR_BASIC.CONTRACT Договора_с_УК,
                                                                                     PFR_BASIC.CONTRACTNAME T2,
                                                                                     PFR_BASIC.DOCTYPE Тип_документа
                                                                                   WHERE T2.ID =
                                                                                         Договора_с_УК.CONT_N_ID AND
                                                                                         Тип_документа.ID =
                                                                                         Договора_с_УК.DT_ID
                                                                                 and Договора_с_УК.TYPEID = 3) Договора_с_УК31
                                                                                   ON Договора_с_УК31.ID =
                                                                                      Перечисление36.ID_договора_с_УК) INNER JOIN
                                                                                 (
                                                                                   SELECT
                                                                                     T2.ID             ID_юрлица,
                                                                                     Контрагенты.TYPE  Тип_контрагента,
                                                                                     T2.FORMALIZEDNAME c9
                                                                                   FROM ((
                                                                                       PFR_BASIC.CONTRAGENT Контрагенты INNER JOIN
                                                                                       PFR_BASIC.LEGALENTITY T2
                                                                                         ON Контрагенты.ID =
                                                                                            T2.CONTRAGENTID) INNER JOIN
                                                                                     PFR_BASIC.STATUS T3
                                                                                       ON T3.ID =
                                                                                          Контрагенты.STATUS_ID) LEFT OUTER JOIN
                                                                                     PFR_BASIC.LEGALSTATUS Статус_юрлица
                                                                                       ON T2.LS_ID =
                                                                                          Статус_юрлица.ID
                                                                                   ) T30
                                                                                   ON T30.ID_юрлица =
                                                                                      Договора_с_УК31.ID_юрлица) INNER JOIN
                                                                                 (
                                                                                   SELECT
                                                                                     T1.ID   ID,
                                                                                     T1.NAME Направление
                                                                                   FROM
                                                                                     PFR_BASIC.DIRECTION_SPN T1) T33
                                                                                   ON T33.ID =
                                                                                      T35.c9) LEFT OUTER JOIN (
                                                                                                                    SELECT
                                                                                                                      Вид_операции.ID        ID,
                                                                                                                      Вид_операции.OPERATION Вид_операции
                                                                                                                    FROM
                                                                                                                      PFR_BASIC.OPERATION Вид_операции) Вид_операции34
                                                                                   ON T35.c8 = Вид_операции34.ID
                                                                               WHERE NOT T32.c17 IS NULL AND
                                                                                     T32.c17 <= date('{0}')
                                                                                      AND T33.Направление =
                                                                                     'из ГУК ВР в ПФР'
                                                                               GROUP BY T30.c9,
                                                                                 Договора_с_УК31.Номер,
                                                                                 Договора_с_УК31.ID_юрлица,
                                                                                 Договора_с_УК31.c8, T32.c17,
                                                                                 T33.Направление,
                                                                                 Вид_операции34.Вид_операции,
                                                                                 T30.Тип_контрагента) T37
                                                                        GROUP BY T37.c1, T37.Номер_договора,
                                                                          T37.Дата_договора_ДУ,
                                                                          T37.Тип_контрагента,
                                                                          T37.ID_юрлица  ) T39


                  ON T38.Номер_договора = T39.Номер_договора
              GROUP BY T38.c1, T39.Тип_контрагента, coalesce(T38.Номер_договора,T39.Номер_договора),coalesce(T38.c1,T39.c1),coalesce(T38.Дата_договора_ДУ,T39.Дата_договора_ДУ), T38.Дата_договора_ДУ, REGCLOSE,
                       REGADDCLOSE,
                       DISSOLDATE) T0) T0

";
            #endregion

            using (var session = DataAccessSystem.OpenSession())
            {
                var items = session.CreateSQLQuery(string.Format(query, reportDate.ToString("yyyy-MM-dd")))
                    .AddScalar("FName", NHibernateUtil.String)
                    .AddScalar("RegNum", NHibernateUtil.String) //help:Имена полей регистрозависимы!  
                    .AddScalar("RegDate", NHibernateUtil.DateTime)
                    .AddScalar("SendedDUTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUDsv", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SendedDUMsk", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUTotalTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUTotalYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUPravTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUPravYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUDsvTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUDsvYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("PeretokTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("PeretokYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUMskTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnDUMskYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNTotal", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNTotalYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNNchtp", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNNchtpYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNSPV", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNSPVYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNEV", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("ReturnSPNEVYield", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .AddScalar("SaldoEnd", NHibernateUtil.GuessType(IsDB2 ? typeof(DB2Decimal4HibType) : typeof(PGDecimal4HibType)))
                    .SetResultTransformer(Transformers.AliasToBean<SPNReport17Item>())
                    .List<SPNReport17Item>();
                //Создание и отдача отчёта
                var param = new List<ReportParameter>
                {
                     new ReportParameter("ReportDate", reportDate.Date.ToString("dd.MM.yyyy"))
                };
                var data = new List<ReportDataSource> { new ReportDataSource("ReportData", items) };

                var fileName = $"Остаток средств ВР и СПН для СПВ на {reportDate.Date.ToString("dd.MM.yyyy")}";
                return RenderReport("SPNReport_17_RemainderSPN.rdlc", fileName, param, data, type);
            }
        }

        #endregion
        }
}

