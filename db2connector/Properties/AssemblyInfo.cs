using System.Reflection;
using System.Runtime.CompilerServices;
using log4net.Config;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ПТК ДОКИП. Сервис приложения - основной сервис.")]

[assembly: InternalsVisibleTo("PFR_INVEST.Tests.ServiceCalls")]

[assembly: XmlConfigurator(Watch = true)]
