﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Helpers;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects.XMLModel.Auction;

namespace db2connector
{
	/// <summary>
	/// Часть сервиса - реализация работы с Аукционами
	/// </summary>
	public partial class IISServiceHib
	{
		/// <summary>
		/// Экспорт уведомления о параметрах отбора заявок
		/// </summary>
		public XMLFileResult ExportAuctionInfo(long auctionId)
		{
			var res = new XMLFileResult();
			using (var session = DataAccessSystem.OpenSession())
			{
				var auction = session.Get<DepClaimSelectParams>(auctionId);

				if (auction == null)
					return new XMLFileResult { Error = $"Аукцион (ID={auctionId}) не найден в базе"};
				if (auction.StockId != (long)Stock.StockID.SPVB)
					return new XMLFileResult { Error = "Экспорт данных возможен только для аукционов с биржей СПВБ" };
		
				var doc = new SPVB_SelectionInfo(auction);
				var sw = new StringWriter();
				var serializer = new XmlSerializer(typeof(SPVB_SelectionInfo));
				serializer.Serialize(sw, doc);

				res.FileName = doc.Properties.DocName;
				res.FileBody = sw.ToString();
			}

			return res;
		}
	
		/// <summary>
		/// Лимиты для банков на дату аукциона
		/// </summary>
		/// <param name="auctionID">ID аукциона</param>		
		public List<BankLimitListItem> GetBanksLimit(long auctionID)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var retVal = new List<BankLimitListItem>();
				var auction = session.Get<DepClaimSelectParams>(auctionID);
				var stockID = auction.StockId;
				var selectDate = auction.SelectDate;
				//все банки с кодом биржи аукциона
				var banks = session.CreateQuery(@"select distinct le from LegalEntity le 
															join fetch le.BankStockCodes sc
															join fetch le.DepClaims2 dc
															where sc.StockID = (:stockID)")
											.SetParameter("stockID", stockID)
											.List<LegalEntityHib>();

				//Отсечение банков, которые не удовлетворяют требованиям постановления Правительства РФ №761 
				var lastCheck = session.CreateQuery(@"select i from BanksConclusion i order by i.ImportDate desc").SetMaxResults(1).List<BanksConclusionHib>().FirstOrDefault();

				if (lastCheck != null) 
				{
					var codes = lastCheck.BanksList.Select(b => b.Code).Distinct().ToList();
					banks = banks.Where(b => codes.Contains(b.RegistrationNum.Trim())).ToList();
				}

				//Формирование лимитов
				foreach (var bank in banks)
				{
					var limitMoney = BankHelper.Limit4Money(bank) * 1000000;

					var deposits = bank.DepClaims2;
					decimal placedAndReturned = 0;
					decimal returned = 0;
					foreach (var deposit in deposits)
					{
						if (deposit.SettleDate <= selectDate && deposit.ReturnDate > selectDate)
							placedAndReturned += deposit.Amount;

						if (deposit.ReturnDate == selectDate.AddDays(-1)
							|| deposit.ReturnDate == selectDate
							|| deposit.ReturnDate == selectDate)
							returned += deposit.Amount;
					}
					var resultValue = limitMoney - placedAndReturned + returned;

					// Display in millions
					limitMoney = limitMoney / 1000000;
					resultValue = resultValue / 1000000;

					//Round
					limitMoney = Math.Round(limitMoney);
					resultValue = Math.Round(resultValue);

					retVal.Add(new BankLimitListItem
					{
						BankName = bank.FormalizedName,
						StockID = stockID,
						StockCode = bank.BankStockCodes.First(sc => sc.StockID == stockID).StockCode,
						LimitTotal = limitMoney,
						LimitClaim = resultValue
					});
				}
				return retVal;
			}
		}


		/// <summary>
		/// Экспорт лимитов для банков
		/// </summary>	
		public XMLFileResult ExportBankLimitsInfo(long auctionID) 
		{
			var auction = GetByID<DepClaimSelectParams>(auctionID);
			var limits = GetBanksLimit(auctionID);

			var res = new XMLFileResult();
			if (auction == null)
				return new XMLFileResult { Error = $"Аукцион (ID={auctionID}) не найден в базе"};
			if (auction.StockId != (long)Stock.StockID.SPVB)
				return new XMLFileResult { Error = string.Format("Экспорт данных возможен только для аукционов с биржей СПВБ", auctionID) };

			var doc = new SPVB_ListOfLimits(auction,limits);
			var sw = new StringWriter();
			var serializer = new XmlSerializer(typeof(SPVB_ListOfLimits));
			serializer.Serialize(sw, doc);

			res.FileName = doc.Properties.DocName;
			res.FileBody = sw.ToString();

			return res;
		}

		/// <summary>
		/// Экспорт ставки отсечения
		/// </summary>				
		public XMLFileResult ExportAuctionCutoffRate(long auctionID)
		{
			var auction = GetByID<DepClaimSelectParams>(auctionID);			

			var res = new XMLFileResult();
			if (auction == null)
				return new XMLFileResult { Error = $"Аукцион (ID={auctionID}) не найден в базе"};
			if (auction.StockId != (long)Stock.StockID.SPVB)
				return new XMLFileResult { Error = "Экспорт данных возможен только для аукционов с биржей СПВБ" };

			var doc = new SPVB_CutoffRate(auction);
			var sw = new StringWriter();
			var serializer = new XmlSerializer(typeof(SPVB_CutoffRate));
			serializer.Serialize(sw, doc);

			res.FileName = doc.Properties.DocName;
			res.FileBody = sw.ToString();

			return res;
		}
	}
}
