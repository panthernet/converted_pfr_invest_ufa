﻿using PFR_INVEST.Integration.NSI;

namespace db2connector
{
	public partial class IISServiceHib
	{
		public void ParseNSIMessage(string message)
		{
			new MessageProcessor().ParseMessage(message);
		}
	}
}