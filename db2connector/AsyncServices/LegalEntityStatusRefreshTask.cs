﻿using System;
using System.Collections.Generic;
using NHibernate.Linq;
using PFR_INVEST.Common.Extensions;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector.AsyncServices
{
    public class LegalEntityStatusRefreshTask : IAsyncTask
    {
        public void Process()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                #region OLD
                /*using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT le.Contragent FROM LegalEntity le WHERE le.Contragent.StatusID = 5")
                        .List<Contragent>();

                    if (list != null)
                    {
                        foreach (var ca in list)
                        {
                            var reorgs = session.CreateQuery(string.Format("SELECT * FROM Reorganization reorg WHERE reorg.SourceContragent.ID = {0}", ca.ID))
                                .List<Reorganization>();
                            if (reorgs == null || reorgs.Count == 0)
                                ca.StatusID = 1;
                            else
                            {
                                bool res = false;
                                foreach (var item in reorgs)
                                    if (item.ReorganizationTypeID == 2 || item.ReorganizationTypeID == 3)
                                    {
                                        res = true; break;
                                    }
                                if (!res)
                                    ca.StatusID = 1;
                            }
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                    }
                }
                */

                /*using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT reorg.SourceContragent FROM Reorganization reorg WHERE reorg.SourceContragent.StatusID = 5 AND (reorg.ReorganizationTypeID <> 2 && reorg.ReorganizationTypeID <> 3)")
                        .List<Contragent>();


                    if (list != null)
                    {
                        foreach (var ca in list)
                        {
                            ca.StatusID = 1;
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                    }
                }*/
                #endregion

                IList<Contragent> list;
                using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT le.Contragent FROM LegalEntity le WHERE le.CloseDate <= :p_date AND (le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2)")
                        .SetParameter("p_date", DateTime.Now)
                        .List<Contragent>();

                    var contragents = session.CreateQuery(
                        @"SELECT DISTINCT le.Contragent FROM LegalEntity le
                        JOIN le.Contracts c  WHERE LOWER(TRIM(le.Contragent.TypeName)) <> 'нпф' AND (le.Contragent.StatusID = 1 OR le.Contragent.StatusID = 2)
                        AND (
                            ((SELECT COUNT(cntr.ID) FROM Contract cntr WHERE cntr.DissolutionDate IS NULL AND cntr.Status > -1 AND cntr.LegalEntityID=le.ID) = 0
                                AND (SELECT MAX(cntr.DissolutionDate) FROM Contract cntr WHERE cntr.Status > -1 AND cntr.LegalEntityID=le.ID ) <= :p_date)
                            OR (SELECT COUNT(cntr.ID) FROM Contract cntr WHERE cntr.Status > -1 AND cntr.LegalEntityID=le.ID) = 0
                         )")
                        .SetParameter("p_date", DateTime.Now)
                        .List<Contragent>();
                    var list1 = list;
                    contragents.ForEach(c => { if (!list1.Contains(c)) { list1.Add(c); } });

                    if (list1 != null)
                    {
                        foreach (var ca in list1)
                        {
                            ca.StatusID = StatusIdentifier.Identifier.Eliminated.ToLong();
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                        // ignored
                    }
                }



                using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT reorg.SourceContragent FROM Reorganization reorg WHERE reorg.ReorganizationDate <= :p_date AND (reorg.SourceContragent.StatusID = 1 OR reorg.SourceContragent.StatusID = 2) AND (reorg.ReorganizationTypeID = 2 OR reorg.ReorganizationTypeID = 3)")
                        .SetParameter("p_date", DateTime.Now)
                        .List<Contragent>();

                    if (list != null)
                    {
                        foreach (var ca in list)
                        {
                            ca.StatusID = StatusIdentifier.Identifier.Reorganized.ToLong();
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                        // ignored
                    }
                }

                using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT lic.LegalEntity.Contragent FROM License lic WHERE lic.CloseDate <= :p_date AND (lic.LegalEntity.Contragent.StatusID = 1 OR lic.LegalEntity.Contragent.StatusID = 2)")
                        .SetParameter("p_date", DateTime.Now)
                        .List<Contragent>();

                    if (list != null)
                    {
                        foreach (var ca in list)
                        {
                            ca.StatusID = StatusIdentifier.Identifier.LicenseRevoked.ToLong();
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                        // ignored
                    }
                }

                using (var transaction = session.BeginTransaction())
                {
                    list = session.CreateQuery("SELECT pause.Contragent FROM NPFPause pause WHERE pause.StartDate <= :p_date AND pause.EndDate >= :p_date AND pause.Contragent.StatusID = 1")
                        .SetParameter("p_date", DateTime.Now)
                        .List<Contragent>();

                    if (list != null)
                    {
                        foreach (var ca in list)
                        {
                            ca.StatusID = StatusIdentifier.Identifier.Suspended.ToLong();
                            session.Update(ca);
                        }
                    }

                    try
                    {
                        transaction.Commit();
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }
        }
    }
}
