﻿using System;
using PFR_INVEST.Integration.NSI.MQ;
using PFR_INVEST.Proxy.Tools;

namespace db2connector.AsyncServices
{
	/// <summary>
	/// Задача загрузки обновлений справочников из системы НСИ
	/// </summary>
	public class NSISyncTask : IAsyncTask
    {
		public void Process()
		{
            //во избежании вылета сервера без логирования исключения
		    try
		    {                
		        MQConnector.Instance.SubscribeToMQ(ProcessMessage);
		    }
		    catch (Exception ex)
		    {
		        LogManager.LogException(ex);
		    }
		}

		private static void ProcessMessage(string message)
		{
            IISService.LocalInstance.ParseNSIMessage(message);
		}
	}
}
