﻿namespace db2connector.AsyncServices
{
    public interface IAsyncTask
    {
        void Process();
    }
}