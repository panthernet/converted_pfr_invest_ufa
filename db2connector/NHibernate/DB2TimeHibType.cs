﻿using System;
using System.Data;
using System.Data.Common;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace db2connector.NHibernate
{
	public class DB2TimeHibType : IUserType
	{
		public new bool Equals(object x, object y)
		{
            if (x == null)
            {
                if (y == null)
                    return true;
                return false;
            }

			return x.Equals(y);
		}

		public object NullSafeGet(IDataReader rs, string[] names, object owner)
		{
			if (names.Length != 1)
				throw new InvalidOperationException("names array has more than one element. can't handle this!");
			int index = rs.GetOrdinal(names[0]);
            TimeSpan? retVal = null;
		    if (!rs.IsDBNull(index))
		    {
		        retVal = rs.GetValue(index) as TimeSpan?;
		        if(retVal == null)
		        {
		            var dt = rs.GetValue(index) as DateTime?;
		            retVal = dt.HasValue ? (TimeSpan?) dt.Value.TimeOfDay : null;
		        }
		    }

		    return retVal;
		}

		public void NullSafeSet(IDbCommand cmd, object value, int index)
		{
            var parameter = (DbParameter)cmd.Parameters[index];
			parameter.Value = value ?? DBNull.Value;
		}

		public object DeepCopy(object value)
		{
			return value;
		}

		public SqlType[] SqlTypes
		{
			get
			{
				return new SqlType[] { new SqlType(DbType.Time) };
			}
		}

		public Type ReturnedType
		{
			get { return typeof(TimeSpan); }
		}

		public bool IsMutable
		{
			get { return true; }
		}

		#region IUserType Members

		object IUserType.Assemble(object cached, object owner)
		{
			return cached;
		}

		object IUserType.Disassemble(object value)
		{
			return value;
		}

		int IUserType.GetHashCode(object x)
		{
			if (x == null)
				return 0;
			return ((string)x).GetHashCode();
		}

		object IUserType.Replace(object original, object target, object owner)
		{
			return original;
		}

		#endregion
	}
}
