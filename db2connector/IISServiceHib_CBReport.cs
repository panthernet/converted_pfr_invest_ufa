﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using NHibernate.SqlCommand;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.ServiceItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.Proxy.Tools;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector
{
    public partial class IISServiceHib
    {
        public bool CBReportCanApprove(CBRApprovalRequest item)
        {
            return SessionWrapper(session => session.CreateQuery(@"select count(r.ID) from BOReportForm1 r
                                            where r.Year=:year and r.Quartal=:quarter and r.OKUD in (:okud) and r.Status=1 and r.FileID is null and r.ReportType=0")
                                                 .SetParameter("year", item.Year)
                                                 .SetParameter("quarter", item.Quarter)
                                                 .SetParameterList("okud", item.Okud.Split(',').ToList().ConvertAll(a=> (object)a))
                                                 .UniqueResult<long>() == 0);
        }

        public List<long> CBReportRequestApproval(CBRApprovalRequest item)
        {
            return TransactionWrapper(session =>
            {
                var idList = new List<long>();
                if (item.ReportType == BOReportForm1.ReportTypeEnum.All || item.ReportType == BOReportForm1.ReportTypeEnum.One)
                {
                    session.CreateQuery(@"update BOReportForm1 r set r.IsApproved1=2, r.Approver1=:name, r.ApproverLogin1=:login, r.ApprovementDate1 = null, r.ApprovementDate2 = null
                                            where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("name", item.Username1)
                        .SetParameter("login", item.Login1)
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One))
                        .ExecuteUpdate();

                    idList.AddRange(session.CreateQuery(@"select r.ID from BOReportForm1 r where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.One))
                        .List<long>());
                }
                if (item.ReportType == BOReportForm1.ReportTypeEnum.All || item.ReportType == BOReportForm1.ReportTypeEnum.Two)
                {
                    session.CreateQuery(@"update BOReportForm1 r set 
                                            r.IsApproved1=2, r.Approver1=:name, r.ApproverLogin1=:login,
                                            r.IsApproved2=2, r.Approver2=:name2, r.ApproverLogin2=:login2,
                                            r.ApprovementDate1 = null, r.ApprovementDate2 = null
                                            where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("name", item.Username2UK)
                        .SetParameter("login", item.Login2UK)
                        .SetParameter("name2", item.Username2NPF)
                        .SetParameter("login2", item.Login2NPF)
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Two))
                        .ExecuteUpdate();
                    idList.AddRange(session.CreateQuery(@"select r.ID from BOReportForm1 r where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Two))
                        .List<long>());
                }
                if (item.ReportType == BOReportForm1.ReportTypeEnum.All || item.ReportType == BOReportForm1.ReportTypeEnum.Three)
                {
                    session.CreateQuery(@"update BOReportForm1 r set r.IsApproved1=2, r.Approver1=:name, r.ApproverLogin1=:login,
                                            r.ApprovementDate1 = null, r.ApprovementDate2 = null
                                            where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("name", item.Username3)
                        .SetParameter("login", item.Login3)
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Three))
                        .ExecuteUpdate();
                    idList.AddRange(session.CreateQuery(@"select r.ID from BOReportForm1 r where r.Year=:year and r.Quartal=:quarter and r.OKUD=:okud and r.Status=1 and r.ReportType=0")
                        .SetParameter("year", item.Year)
                        .SetParameter("quarter", item.Quarter)
                        .SetParameter("okud", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Three))
                        .List<long>());
                }
                return idList;
            });
        }

        public BOReportForm1 GetCBReport(long reportId)
        {
            return SessionWrapper(session =>
            {
                var report = session.CreateQuery(@"select rep, data from BOReportForm1 rep 
                                      left join rep.DataHib data
                                      where rep.ID =:id")
                    .SetParameter("id", reportId)
                    .List<object[]>()
                    .Select(a =>
                    {
                        var x = (BOReportForm1)a[0];
                        x.Data = a[1] as BOReportForm1Data;
                        return x;
                    })
                    .FirstOrDefault();
                if (report == null) return null;
                if (report.RType != BOReportForm1.ReportTypeEnum.One) return report;

                report.SumList = session.CreateQuery(@"select sumx,pf.Year from BOReportForm1Sum sumx
                                                        join sumx.Portfolio pf
                                                        where sumx.ReportID = :id")
                    .SetParameter("id", reportId)
                    .List<object[]>()
                    .Select(a =>
                    {
                        var res = (BOReportForm1Sum)a[0];
                        res.PortfolioName = (string)a[1];
                        return res;
                    }).ToList();
                return report;
            });
        }

        public List<BOReportForm1> GetGeneralBOReportForm1List(BOReportForm1.ReportTypeEnum reportType, bool monthlyOnly = false)
        {
            return SessionWrapper(session =>
            {
                string code;
                switch (reportType)
                {
                    case BOReportForm1.ReportTypeEnum.One:
                        code = "0418001";
                        break;
                    case BOReportForm1.ReportTypeEnum.Two:
                        code = "0418002";
                        break;
                    case BOReportForm1.ReportTypeEnum.Three:
                        code = "0418003";
                        break;
                    default:
                        code = null;
                        break;
                }

                if (monthlyOnly)
                {
                    return session.CreateQuery(@"select rep from BOReportForm1 rep where rep.Status = 1 and rep.ReportType = 1")
                        .List<BOReportForm1>().ToList();
                }

                var query = session.CreateQuery($@"select rep from BOReportForm1 rep where rep.Status = 1 and rep.ReportType=0 {(string.IsNullOrEmpty(code) ? "" : " and rep.OKUD=:report")}");
                if (!string.IsNullOrEmpty(code))
                    query.SetParameter("report", code);
                return query.List<BOReportForm1>().ToList();
            });
        }

        public BOReportForm1 TryGetPreviousReport(BOReportForm1 report)
        {
            return SessionWrapper(session => session.CreateQuery(@"select report from BOReportForm1 report where report.INN is not null order by report.UpdateDate desc")
                .SetMaxResults(1)
                .UniqueResult<BOReportForm1>());
        }

        public long CBReportCreate(int year, int quarter, BOReportForm1.ReportTypeEnum repType)
        {
            if (repType != BOReportForm1.ReportTypeEnum.Two && repType != BOReportForm1.ReportTypeEnum.Three)
                throw new Exception("Поддерживаются только отчеты 2 или 3!");
            return TransactionWrapper(session =>
            {
                var report = new BOReportForm1
                {
                    Year = year,
                    Quartal = quarter,
                    OKUD = BOReportForm1.GetOkudString(repType),
                    CreateDate = DateTime.Today,
                    UpdateDate = DateTime.Today,
                    Date = new DateTime(quarter == 4 ? year + 1 : year, DateTools.GetLastMonthOfQuarterForBO(quarter), 1),
                    Status = 1
                };
                return SaveEntity(report, session);
            });
        }

        private static void GetCBReportDates(bool monthly, int year, int quarter, out DateTime startDate, out DateTime endDate)
        {
            var fromMonth = monthly ? quarter : DateTools.GetFirstMonthOfQuarterForBO(quarter);
            var toMonth = monthly ? (quarter == 12 ? 1 : quarter+1) : DateTools.GetLastMonthOfQuarterForBO(quarter);
            var toYear = monthly ? (quarter == 12 ? year + 1 : year) : (toMonth == 1 ? year + 1 : year);

            startDate = new DateTime(year, fromMonth, 1);
            endDate = new DateTime(toYear, toMonth, 1);
        }

        public List<DepositCBOrderListItem> GetDepositsListForCBMonthlyReportForm3(int year, int month)
        {
            DateTime from;
            DateTime to;
            GetCBReportDates(true, year, month, out from, out to);

            return SessionWrapper(session =>
            {
                var res = new List<DepositCBOrderListItem>();
                //правильная выборка депозитов... если через deposit, выбираются не все.. хм
                var results = session.CreateSQLQuery(@"
                                                       select dc2.SETTLEDATE, dc2.RETURNDATE, dc2.RATE, coalesce(dc2.AMOUNT, 0), b.FULLNAME, b.INN, o.NUMBER, b.SHORTNAME
                                                            from pfr_basic.depclaim2 dc2                                                        
                                                       inner join pfr_basic.depclaim_offer o on o.depclaim_id = dc2.ID
                                                       inner join pfr_basic.legalentity b on b.ID = dc2.bank_id
                                                    where dc2.SETTLEDATE<:edate and dc2.RETURNDATE >=:edate 
                                                        and dc2.STATUSID <> -1")
                    .SetParameter("edate", to.Date)
                    .List<object[]>()
                    .ToList();
                results.ForEach(a =>
                {
                    var cNum = (long?)a[6];
                    res.Add(new DepositCBOrderListItem
                    {
                        SettleDate = (DateTime)a[0],
                        ReturnDate = (DateTime)a[1],
                        Percent = (decimal)a[2] / (IsDB2 ? 100m : 1m),
                        Sum = (decimal)a[3] / (IsDB2 ? 100m : 1m) / 1000m,
                        BankFullName = (string)(a[4] ?? "-"),
                        BankINN = (string)(a[5] ?? "-"),
                        Periodity = "в конце срока",
                        CouponIncome = 0,
                        DepositContract = cNum?.ToString() ?? "-",
                        SecurityCount = 0,
                        BankShortName = (string)a[7]

                    });
                });
                return res;
            });
        }

        public List<DepositCBOrderListItem> GetDepositsListForCBReportForm3(int year, int quarter, bool monthly = false)
        {
            DateTime from;
            DateTime to;
            GetCBReportDates(monthly, year, quarter, out from, out to);

            return SessionWrapper(session =>
            {
                var res = new List<DepositCBOrderListItem>();
                var results = session.CreateQuery(@"select dc2.SettleDate, dc2.ReturnDate, dc2.Rate, coalesce(d.Sum, 0), b.FullName, b.INN, o.Number,
                                                        b.ShortName, o.ID
                                                    from d in OfferPfrBankAccountSum
                                                    join d.Offer o
                                                    join o.DepClaim dc2
                                                    join dc2.Bank b
                                                    join d.Portfolio p
                                                    where dc2.SettleDate<:edate and dc2.ReturnDate >=:edate 
                                                        and dc2.StatusID <> -1 and p.TypeID<>:ptype")
                    .SetParameter("edate", to.Date)
                    .SetParameter("ptype", (long)Portfolio.Types.ROPS)
                    .List<object[]>()
                    .ToList();
                results.GroupBy(a=> (long)a[8])
                    .ForEach(b =>
                    {
                        var sum = b.Sum(x => (decimal) x[3]);
                        var a = b.First();
                        var cNum = (long?)a[6];
                        res.Add(new DepositCBOrderListItem
                        {
                            SettleDate = (DateTime) a[0],
                            ReturnDate = (DateTime) a[1],
                            Percent = (decimal) a[2],
                            Sum = sum,
                            BankFullName = (string) (a[4] ?? "-"),
                            BankINN = (string) (a[5] ?? "-"),
                            Periodity = "в конце срока",
                            CouponIncome = 0,
                            DepositContract = cNum?.ToString() ?? "-",
                            SecurityCount = 0,
                            BankShortName = (string) a[7]

                        });
                    });
                return res;
            });
        }

        public void SaveCBReportToRepository(byte[] fileContent, long reportId, string comment)
        {
            comment = comment ?? "";
            var file =
                new RepositoryNoAucImpExpFile
                {
                    Repository = fileContent,
                    Key = (int)RepositoryImpExpFile.Keys.CBReportExportXml,
                    UserName = DomainUser,
                    DDate = DateTime.Now.Date,
                    TTime = DateTime.Now.TimeOfDay,
                    Comment = comment.Length >=512 ? comment.Substring(0, 511) : comment,
                    ImpExp = 0,
                    Status = 0 //в какой нормальной системе статус 0 означает ОК, а статус 1 - ошибку??
                };

            TransactionWrapper(session =>
            {
                var fileId = SaveEntity(file, session);
                var oldFileId = session.CreateQuery(@"select distinct FileID from BOReportForm1 where ID=:reportid")
                    .SetParameter("reportid", reportId)
                    .UniqueResult<long?>();
                if(oldFileId.HasValue)
                    DeleteEntity<RepositoryNoAucImpExpFile>(oldFileId.Value, session);

                session.CreateQuery(@"update BOReportForm1 set FileID=:fileid where ID=:reportid")
                    .SetParameter("fileid", fileId)
                    .SetParameter("reportid", reportId)
                    .ExecuteUpdate();

            });
        }



        internal List<CBReportForm2TransferListItem> GetTransfersListForCBReportForm2_SI(int count, DateTime from, DateTime to)
        {
            return SessionWrapper(session =>
            {
                var siTrList = session
                    .CreateQuery(
                        @"select coalesce(pp.DraftAmount,0), le.INN, le.ShortName, pp.DraftPayDate, c.ContractNumber, c.ContractDate, pp.DirectionElID, pp.CBPaymentDetail, rq.ID, rq.Sum, pp.ID 
                                                        from AsgFinTr pp
                                                        join pp.ReqTransfer rq
                                                        join rq.TransferList tl
                                                        join tl.TransferRegister r
                                                        join tl.Contract c
                                                        join c.LegalEntity le
                                                        left join pp.PaymentDetail pd
                                                        where rq.TransferStatus =:status and pp.DraftPayDate >=:sdate and pp.DraftPayDate < :edate and
                                                            pp.StatusID<>-1 and rq.StatusID<>-1 and tl.StatusID<>-1 and r.StatusID<>-1")
                    .SetParameter("status", TransferStatusIdentifier.sActSigned)
                    .SetParameter("sdate", from.Date)
                    .SetParameter("edate", to.Date)
                    .List<object[]>()
                    .Select(a =>
                    {
                        var cDate = (DateTime?) a[5];
                        var cString = $"{a[4]} {cDate?.ToOnesXmlString()}";
                        string dir = (long) a[6] == (long) AsgFinTr.Directions.FromPFR ? "передача" : "получение";
                        string dirCode = (long) a[6] == (long) AsgFinTr.Directions.FromPFR ? "1" : "2";
                        var sum = Convert.ToInt64(Math.Round((decimal) a[0]));
                        return new CBReportForm2TransferListItem
                        {
                            Sum = sum.ToString(),
                            SumValue = sum,
                            SumValuePrec = (decimal) a[0],
                            INN = (string) a[1],
                            RcvName = (string) a[2],
                            Date = ((DateTime?) a[3]).Value.ToOnesXmlString(),
                            DateValue = (DateTime?) a[3],
                            Count = count++.ToString(),
                            Assignment = (string) a[7] ?? "-",
                            ContractNum = cString,
                            ID = (long) a[8],
                            ParentSum = (decimal) a[9],
                            OpActive = "true",
                            OpCode = dirCode,
                            OpDescription = dir,
                            PPID = (long) a[10]
                        };
                    }).ToList();

                siTrList = CB_ReportRemoveReturnedPP(siTrList, 0);

                Debug.WriteLine(
                    $"SI/VR: передача {siTrList.Where(a => a.OpCode == "1").Sum(a => a.SumValue)}     получение {siTrList.Where(a => a.OpCode == "2").Sum(a => a.SumValue)}");

                return siTrList;
            });
        }

        internal List<CBReportForm2TransferListItem> GetTransfersListForCBReportForm2_PFRtoNPF(int count, DateTime from, DateTime to, string customWhere = null,
            bool isFormalizedName = false)
        {
            //НПФ - ИЗ ПФР
            //Назначение платежа из реестра - все верно
            return SessionWrapper(session =>
            {
                var npfTrListToPFR = session
                    .CreateQuery(
                        $@"select coalesce(pp.DraftAmount,0), le.INN, {(isFormalizedName? "le.FormalizedName": "le.ShortName")}, pp.DraftPayDate, r.RegNum, r.RegDate, pp.DirectionElID, pp.CBPaymentDetail, fr.ID, fr.Count, pp.ID, pp.DraftRegNum, pp.DraftDate
                                                        from AsgFinTr pp
                                                        join pp.Finregister fr
                                                        join fr.Register r
                                                        join fr.DebitAccount acc
                                                        join acc.Contragent c
                                                        join c.LegalEntities le

                                                        where (fr.Status =:status) and pp.DraftPayDate >=:sdate and pp.DraftPayDate < :edate and
                                                             pp.DirectionElID = :dir and
                                                            pp.StatusID<>-1 and fr.StatusID<>-1 and r.StatusID<>-1  and c.StatusID<>-1 {customWhere}")
                    .SetParameter("status", RegisterIdentifier.FinregisterStatuses.Transferred)
                    .SetParameter("sdate", from.Date)
                    .SetParameter("edate", to.Date)
                    .SetParameter("dir", (long) AsgFinTr.Directions.FromPFR)
                    .List<object[]>()
                    .Select(a =>
                    {
                        string dir = (long) a[6] == (long) AsgFinTr.Directions.FromPFR ? "передача" : "получение";
                        string dirCode = (long) a[6] == (long) AsgFinTr.Directions.FromPFR ? "1" : "2";
                        var sum = (decimal) a[0];//Convert.ToInt64(Math.Round((decimal) a[0]));
                        return new CBReportForm2TransferListItem
                        {
                            Sum = sum.ToString(),
                            SumValue = sum,
                            SumValuePrec = (decimal) a[0],
                            INN = (string) a[1],
                            RcvName = (string) a[2],
                            Date = ((DateTime?) a[3]).Value.ToOnesXmlString(),
                            DateValue = (DateTime?) a[3],
                            Count = count++.ToString(),
                            Assignment = (string) a[7],
                            ID = (long) a[8],
                            ParentSum = (decimal) a[9],
                            ContractNum = "-",
                            OpActive = "true",
                            OpCode = dirCode,
                            OpDescription = dir,
                            PPID = (long) a[10],
                            RegNum = (string) a[4],
                            RegDate = (DateTime?)a[5],
                            PPRegNum = (string)a[11],
                            PPDraftDate = (DateTime?)a[12]
                        };
                    }).ToList();
                Debug.WriteLine(
                    $"NPF: получение {npfTrListToPFR.Where(a => a.OpCode == "2").Sum(a => a.SumValue)}      передача {npfTrListToPFR.Where(a => a.OpCode == "1").Sum(a => a.SumValue)}");
                npfTrListToPFR = CB_ReportRemoveReturnedPP(npfTrListToPFR, 1);

                var unlinked = GetTransfersListForCBReportForm2_NPFtoPFR(count, from, to, customWhere, isFormalizedName, true);
                if(unlinked.Any())
                    npfTrListToPFR.AddRange(unlinked);

                return npfTrListToPFR;
            });
        }

        internal List<CBReportForm2TransferListItem> GetTransfersListForCBReportForm2_NPFtoPFR(int count, DateTime from, DateTime to, string customWHere = null, bool isFormalizedName = false, bool onlyUnlinked = false)
        {
            return SessionWrapper(session =>
            {
                //НПФ - В ПФР (приход) - включает так же непривязанные п/п
                var npfTrListToNPF =
                    session.CreateQuery(
                            $@"select coalesce(pp.DraftAmount,0), le3.INN, {(isFormalizedName? "le3.FormalizedName": "le3.ShortName")}, pp.DraftPayDate,  pp.DirectionElID, pp.CBPaymentDetail, fr.ID, fr.Count, le.ShortName, le.INN, pp.ID, r.RegNum, r.RegDate, pp.DraftRegNum, pp.DraftDate
                                                    from AsgFinTr pp
                                                        left join pp.Finregister fr
                                                        left join fr.Register r
                                                        left join fr.CreditAccount acc
                                                        left join acc.Contragent c
                                                        left join c.LegalEntities le3
                                                        left join pp.LegalEntity le
                                                        left join le.Contragent lec
                                                        left join pp.PaymentDetail pd
                                                        where (pp.FinregisterID is null or (fr.StatusID<>-1 and r.StatusID<>-1)) and pp.DraftPayDate >=:sdate and pp.DraftPayDate < :edate and
                                                            pp.DirectionElID = :dir and pp.SectionElID = :section and pp.StatusID<>-1 {customWHere}") //and lower(lec.TypeName)=:type
                        .SetParameter<DateTime?>("sdate", from.Date.Date)
                        .SetParameter<DateTime?>("edate", to.Date.Date)
                        .SetParameter("dir", (long) AsgFinTr.Directions.ToPFR)
                        .SetParameter("section", (long) AsgFinTr.Sections.NPF)
                       // .SetParameter("type", "нпф")
                        .List<object[]>()
                        .Select(a =>
                        {
                            string dir = a[4] == null ? "-" : ((long) a[4] == (long) AsgFinTr.Directions.FromPFR ? "передача" : "получение");
                            string dirCode = a[4] == null ? "1" : ((long) a[4] == (long) AsgFinTr.Directions.FromPFR ? "1" : "2");
                            var sum = (decimal) a[0];//Convert.ToInt64(Math.Round((decimal) a[0]));
                            var id = (long) (a[6] ?? 0L);
                            //берем НПФ из финреестра, если финреестр не указан берем из п/п, иначе - 
                            var rcvName = (string) a[2] ?? (id == 0 ? (string) a[8] : null) ?? "-";
                            var inn = (string) a[1] ?? (id == 0 ? (string) a[9] : null) ?? "-";
                            var ass = (string) a[5] ?? "-";
                            var isUnlinked = (id == 0 || a[4] == null) && sum < 0;
                            var sumPrec = (decimal) a[0];

                            return new CBReportForm2TransferListItem
                            {
                                Sum = sum.ToString(),
                                SumValue = sum,
                                SumValuePrec = sumPrec,
                                INN = inn,
                                RcvName = rcvName,
                                Date = ((DateTime?) a[3]).HasValue ? ((DateTime?) a[3]).Value.ToOnesXmlString() : "-",
                                DateValue = (DateTime?) a[3],
                                Count = count++.ToString(),
                                Assignment = ass,
                                ID = id,
                                ParentSum = (decimal?) a[7] == null ? 0 : (decimal) a[7],
                                ContractNum = "-",
                                OpActive = "true",
                                OpCode = dirCode,
                                OpDescription = dir,
                                PPID = (long) a[10],
                                IsUnlinked = isUnlinked,
                                RegNum = (string)a[11],
                                RegDate = (DateTime?)a[12],
                                PPRegNum = (string)a[13],
                                PPDraftDate = (DateTime?)a[14]
                            };
                        }).ToList();
                Debug.WriteLine(
                    $"NPF: получение {npfTrListToNPF.Where(a => a.OpCode == "2").Sum(a => a.SumValue)}      передача {npfTrListToNPF.Where(a => a.OpCode == "1").Sum(a => a.SumValue)}  id0:{npfTrListToNPF.Count(a => a.ID == 0)}");

                
                var unlinkedList = npfTrListToNPF.Where(a => a.IsUnlinked).ToList();
                npfTrListToNPF = CB_ReportRemoveReturnedPP(npfTrListToNPF.Where(a => !a.IsUnlinked).ToList(), 2);


                if (onlyUnlinked)
                {
                    unlinkedList.ForEach(a =>
                    {
                        a.SumValuePrec = a.SumValuePrec * -1;
                        a.SumValue = Convert.ToInt64(Math.Round(a.SumValuePrec));
                        a.Sum = a.SumValue.ToString();
                        a.OpDescription = "передача";
                        a.OpCode = "1";
                        a.Assignment = "возврат ошибочно перечисленных в ПФР средств";
                    });
                    return unlinkedList;
                }
                // else if(unlinkedList.Any())
               //     npfTrListToNPF.AddRange(unlinkedList);

                return npfTrListToNPF;
            });
        }

        public List<CBReportForm2TransferListItem> GetTransfersListForCBReportForm2(int year, int quarter)
        {
	        DateTime from;
	        DateTime to;
	        GetCBReportDates(false, year, quarter, out from, out to);

            var count = 1;
            var list1 = GetTransfersListForCBReportForm2_SI(count, from, to);
            count = list1.Any() ? int.Parse(list1.Last().Count) : 1;
            var list2 = GetTransfersListForCBReportForm2_NPFtoPFR(count, from, to);
            count = list2.Any() ? int.Parse(list2.Last().Count) : 1;
            var list3 = GetTransfersListForCBReportForm2_PFRtoNPF(count, from, to);

            list1.AddRange(list2);
            list1.AddRange(list3);

            return list1.OrderBy(a => a.OpCode).ThenBy(a => a.RcvName).ThenBy(a => a.DateValue).ToList();
        }

        private static List<CBReportForm2TransferListItem> CB_ReportRemoveReturnedPP(List<CBReportForm2TransferListItem> siTrList, int cat)
        {
            //отсев возвращенных п/п на ту же сумму что и первое п/п
            //if (cat == 1) LogManager.getSingleton().Log(string.Format("LIST COUNT1: {0}", siTrList.Count));
            siTrList.ToList().GroupBy(a => a.ID).ForEach(gr =>
            {
                //выбираем все отрицательные суммы
                var mSums = gr.Where(a => a.SumValue < 0).ToList();
               // if (cat == 1 && mSums.Count > 0) LogManager.getSingleton().Log(string.Format("ID: {0} mSums: {1} eCount: {2}", gr.Key, mSums.Count, gr.Count()));
                //если таковых нет - идем дальше
                if (mSums.Count == 0) return;
                //отсеиваем все п/п если кол-во=1 и отриц. сумма или сумма всех п/п <= 0
                if (gr.Count() == 1 && gr.First().SumValuePrec < 0 || gr.Sum(a => a.SumValuePrec) <= 0)
                {
                   // if (cat == 1) LogManager.getSingleton().Log(string.Format("FU all pp!"));
                    gr.ForEach(a =>
                    {
                    //    if (cat == 1) LogManager.getSingleton().Log(string.Format("REM PP: {0}", a.PPID));
                        siTrList.Remove(a);
                    });
                    return;
                }
                //выбираем первую п/п с положительной суммой после сортировке по дате и ID
                var lastSum = gr.OrderByDescending(a=> a.DateValue).ThenBy(a=> a.PPID).FirstOrDefault(a=> a.SumValuePrec > 0);

                if (cat == 1) LogManager.Log($"lastSum: {lastSum}");
                //если сумма последнего п/п = сумме финреестра и есть отриц п/п, удаляем все п/п кроме последнего  
                if (lastSum != null && lastSum.SumValuePrec == lastSum.ParentSum)
                {
                   // if (cat == 1) LogManager.getSingleton().Log(string.Format("Sum BINGO!"));
                    gr.Where(a => a != lastSum).ForEach(a =>
                    {
                      //  if (cat == 1) LogManager.getSingleton().Log(string.Format("REM2 PP: {0}", a.PPID));
                        siTrList.Remove(a);
                    });
                }
                /*
                //для каждой триц суммы
                mSums.ForEach(a =>
                {
                    //берем абсолютное значение
                    var neutValue = Math.Abs(a.SumValue);
                    //ищем положительный эквивалент для отриц суммы, берем самый старый (т.к. последний скорее всего верный)
                    var found = gr.Where(b => b.SumValue == neutValue).OrderBy(b => b.DateValue).FirstOrDefault();
                    if (found == null)
                    {
                        LogManager.getSingleton().Log("Не найдена положительная сумма для отриц. п/п ID=" + gr.Key);
                        return;
                    }
                    //если нашли соответствие, исключаем обе записи из корневого списка
                    siTrList.Remove(found);
                    siTrList.Remove(a);
                });*/

            });
            if (cat == 1) LogManager.Log($"LIST COUNT2: {siTrList.Count}");
            return siTrList;
        }

        public bool HasCBReportFreeYearsAndQaurters(List<BOReportForm1.ReportTypeEnum> rTypeList)
        {
            return SessionWrapper(session =>
            {
                var cnt = 0;
                foreach (var a in rTypeList)
                {
                    cnt += GetBOReportForm1FreeYearsAndQaurtersList(a).Count;
                    if (cnt > 0) return true;
                }
                return false;
            });
        }

        public Dictionary<string, Dictionary<int, List<int>>> GetNotApprovedCBReportsYearsAndQuartersList(BOReportForm1.ReportTypeEnum reportType)
        {
            return SessionWrapper(session =>
            {
                var dic = new Dictionary < string, Dictionary< int, List < int >>>();
                session.CreateQuery(@"select rep.Year, rep.Quartal, rep.OKUD from BOReportForm1 rep where rep.Status>0 and rep.ReportType=0 and rep.OKUD in (:list) and rep.IsApproved1=:app1 or (rep.OKUD = :okud2 and rep.IsApproved2=:app1)")
                    .SetParameterList("list", BOReportForm1.GetOkudString(reportType).Split(',').ToList())
                    .SetParameter("okud2", BOReportForm1.GetOkudString(BOReportForm1.ReportTypeEnum.Two))
                    .SetParameter("app1", (int)BOReportForm1.ReportStatus.Declined)
                    .List<object[]>().ForEach(a =>
                    {
                        var y = (int) a[0];
                        var q = (int) a[1];
                        var okud = (string) a[2];
                        if (!dic.ContainsKey(okud)) dic.Add(okud, new Dictionary<int, List<int>>());
                        if (!dic[okud].ContainsKey(y)) dic[okud].Add(y, new List<int>());
                        if(!dic[okud][y].Contains(q)) dic[okud][y].Add(q);
                    });
                return dic;
            });
        }

        public Dictionary<int, List<int>> GetBOReportMonthlyFreeYearsAnMonthsList(int debugCount = 0)
        {
            return SessionWrapper(session =>
            {
                var dic = new Dictionary<int, List<int>>();
                session.CreateQuery(@"select rep.Year, rep.Quartal from BOReportForm1 rep where rep.ReportType=1 and rep.Status>0")
                    .List<object[]>().ForEach(a =>
                    {
                        var year = (int)a[0];
                        if (dic.ContainsKey(year)) dic[year].Add((int)a[1]);
                        else dic.Add(year, new List<int> { (int)a[1] });
                    });
                int count = debugCount == 0 ? (DateTime.Today.Year - 2017) : debugCount;
                var resDic = new Dictionary<int, List<int>>();
                for (int i = 2017; i <= 2017 + count; i++)
                {
                    var iter = i;
                    if (!resDic.ContainsKey(iter)) resDic.Add(iter, new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
                    dic.Where(a => a.Key == iter).ForEach(a =>
                    {
                        a.Value.ForEach(v => { if (resDic[iter].Contains(v)) resDic[iter].Remove(v); });
                    });
                }
                resDic.Where(a => a.Value.Count == 0).ToList().ForEach(a => resDic.Remove(a.Key));
                return resDic;
            });
        }

        public Dictionary<int, List<int>> GetBOReportForm1FreeYearsAndQaurtersList(BOReportForm1.ReportTypeEnum reportType)
        {
            if (reportType == BOReportForm1.ReportTypeEnum.All) return null;
            return SessionWrapper(session =>
            {
                var dic = new Dictionary<int, List<int>>();
                session.CreateQuery(@"select rep.Year, rep.Quartal from BOReportForm1 rep where rep.OKUD in (:list) and rep.ReportType=0 and rep.Status>0")
                    .SetParameterList("list", new List<string> { BOReportForm1.GetOkudString(reportType) })
                    .List<object[]>().ForEach(a =>
                    {
                        var year = (int)a[0];
                        if (dic.ContainsKey(year)) dic[year].Add((int)a[1]);
                        else dic.Add(year, new List<int> { (int)a[1] });
                    });
                int count = DateTime.Today.Year - 2016;
                var resDic = new Dictionary<int, List<int>>();
                for (int i = 2016; i <= 2016 + count; i++)
                {
                    var iter = i;
                    if (!resDic.ContainsKey(iter)) resDic.Add(iter, new List<int> { 1, 2, 3, 4 });
                    dic.Where(a => a.Key == iter).ForEach(a =>
                    {
                        a.Value.ForEach(v => { if (resDic[iter].Contains(v)) resDic[iter].Remove(v); });
                    });
                }
                resDic.Where(a => a.Value.Count == 0).ToList().ForEach(a => resDic.Remove(a.Key));
                return resDic;
            });
        }

        public List<long> SaveReportForm1(BOReportForm1 report)
        {
            return TransactionWrapper(session =>
            {
                var list = new List<long>();
                report.Data.ReportID = report.ID = SaveEntity(report, session);
                list.Add(report.ID);
                list.Add(SaveEntity(report.Data, session));
                report.SumList.ForEach(a =>
                {
                    a.ReportID = report.ID;
                    list.Add(SaveEntity(a, session));
                });
                return list;
            });
        }


        public decimal GetKDocsSumForBOReport(int year, int quartal)
        {
           // var dtList = new List<DateTime>();
            var date = new DateTime(quartal == 4 ? year + 1 : year, DateTools.GetLastMonthOfQuarterForBO(quartal), 1);


            return SessionWrapper(session =>
            {
                var mSum = session.CreateQuery("select sum(coalesce(k.TotalSumm,0)) from KDoc k where k.ReportPeriodID = :reportId and k.CheckDate = :idate)")
                    .SetParameter("reportId", 80001L)
                    .SetParameter("idate", date)
                    .UniqueResult<decimal>();
                if (quartal == 4)
                {
                    var ySum = session.CreateQuery("select sum(coalesce(k.TotalSumm,0)) from KDoc k where k.ReportPeriodID = :reportId and k.CheckDate =:startdate")
                        .SetParameter("reportId", 80002L)
                        .SetParameter("startdate", date)
                        .UniqueResult<decimal>();
                    if (ySum != 0)
                        mSum = mSum + (ySum - mSum);
                }
                return mSum;
            });
        }

        public decimal GetKDocsSumForBOReportQuarter(int year, int quartal)
        {
            //сумма за указанный квартал
            var fromMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = fromMonth == 1 ? year + 1 : year;
            //сумма за пред квартал
            var oldFromMonth = quartal == 1 ? 1 : DateTools.GetLastMonthOfQuarterForBO(quartal - 1);
            var oldToYear = oldFromMonth == 1 ? year + 1 : year;

            //вычисляем диапазон дат для проведения операций
            var dtlist = new List<DateTime>();
            switch (quartal)
            {
                case 1:
                    dtlist.Add(new DateTime(toYear, fromMonth, 1));
                    break;
                case 2:
                case 3:
                case 4:
                    dtlist.Add(new DateTime(toYear, fromMonth, 1));
                    dtlist.Add(new DateTime(oldToYear, oldFromMonth, 1));
                    break;
            }


            return SessionWrapper(session =>
            {                
                //сумма за квартал
                var nSum = session.CreateQuery("select sum(coalesce(k.TotalSumm,0)) from KDoc k where k.ReportPeriodID = :reportId and k.CheckDate = :startdate")
                        .SetParameter("reportId", 80001L)
                        .SetParameter("startdate", dtlist[0])
                        .UniqueResult<decimal>();
                //сумма за пред квартал
                var oSum = dtlist.Count > 1 ? session.CreateQuery("select sum(coalesce(k.TotalSumm,0)) from KDoc k where k.ReportPeriodID = :reportId and k.CheckDate = :startdate")
                        .SetParameter("reportId", 80001L)
                        .SetParameter("startdate", dtlist[1])
                        .UniqueResult<decimal>() : 0m;
                //разница сумм
                var mSum = nSum - oSum;

                if (quartal == 4)
                {
                    //сумма годового ДК
                    var yearDkSum = session.CreateQuery("select sum(coalesce(k.TotalSumm,0)) from KDoc k where k.ReportPeriodID = :reportId and k.CheckDate =:startdate")
                    .SetParameter("reportId", 80002L)
                    .SetParameter("startdate", dtlist[0])
                    .UniqueResult<decimal>();
                    if (yearDkSum != 0)
                    {
                        //полученная сумма + разница между годовым ДК и суммой за 4 квартал
                        mSum = mSum + (yearDkSum - nSum);
                    }
                }
                return mSum;
            });
        }

        public decimal GetDSVForBOReport(int year, int quartal, bool qOnly, bool for3)
        {
            var fromMonth = qOnly ? DateTools.GetFirstMonthOfQuarterForBO(quartal) : 1;
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            //var toYear = toMonth == 1 ? (year + 1) : year;

            //var dt = new DateTime(year, fromMonth, 1);
            //var toDt = new DateTime(toYear, toMonth == 1? 12 : toMonth, 1);

            return SessionWrapper(session =>
            {
                var selStr = for3 ? "(coalesce(due.Summ,0) - coalesce(due.SummEmployer,0))" : "coalesce(due.SummEmployer,0)";
                //due.OperationDate >=:sDate and due.OperationDate < :eDate
                var query = session.CreateQuery(string.Format(@"select {1}, due.ID  from AddSPN due
                                                 join due.Portfolio p
                                                 where {0} and due.YearID =:syear and due.MonthID >=:smonth and due.MonthID <:emonth", GetPortfolioTypeFilterString("p", "types", PortfolioTypeFilter.DSV), selStr));
                SetPortfolioTypeFilterParameter(query, "types", PortfolioTypeFilter.DSV);
                query.SetParameter("syear", (long)(year - 2000));
                query.SetParameter("smonth", (long)fromMonth);
                query.SetParameter("emonth", (long)(toMonth == 1 ? 13 : toMonth));
                var list = query.List<object[]>();
                var mSum = list.Sum(a => (decimal)a[0]);
                var idList = list.Select(a => (long)a[1]).Distinct().ToList();

                decimal dopSum = 0;
                //уточнения для 3 строчки
                if (for3 && idList.Count > 0)
                {
                    dopSum = session.CreateQuery(@"select sum(coalesce(due.ChSumm,0)) from DopSPN due
                                                       join due.Portfolio p 
                                                       where due.AddSpnID in (:list)")
                        .SetParameterList("list", idList)
                        .UniqueResult<decimal>();
                }
                return mSum + dopSum;
            });
        }

        public decimal GetOrdReportForBOReport(int year, int quartal, bool qOnly)
        {
            var fromMonth = qOnly ? DateTools.GetFirstMonthOfQuarterForBO(quartal) : 1;
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = toMonth == 1 ? year + 1 : year;

            var from = new DateTime(year, fromMonth, 1);
            var to = new DateTime(toYear, toMonth, 1);

            return SessionWrapper(session =>
            {
                var dohod = session.CreateSQLQuery(
                    $@"SELECT               
				CAST((
                	coalesce(a.ONE_NOMVALUE, s.NOMVALUE) * a.PRICE / 100
                	+ CAST(CASE WHEN ONE_NKD IS NULL THEN coalesce(NKD,0)/a.Count ELSE coalesce(ONE_NKD,0) END as DECIMAL(30,2)))
                	* a.Count as DECIMAL(30, 2))
                *
                CASE 
				WHEN s.CURR_ID = 1 
				THEN 1 
				ELSE COALESCE(
					(SELECT cr.VALUE AS V
					 FROM PFR_BASIC.CURS cr
					 WHERE s.CURR_ID = cr.CURR_ID
					   AND CASE s.CURR_ID WHEN 1 THEN a.DATEORDER ELSE a.DATEDEPO END >= cr.DATE
					 ORDER BY cr.DATE DESC FETCH FIRST 1 ROWS ONLY) , 1) 
				END 
				*
				CASE o.TYPE
                    WHEN 'покупка' THEN -1
                    WHEN 'продажа' THEN 1
                END
				AS SUMM
            FROM PFR_BASIC.ORD_REPORT a
            LEFT JOIN PFR_BASIC.PF_BACC  bp ON  bp.ID = a.PF_BACC_ID       
            INNER JOIN PFR_BASIC.CBINORDER i ON i.ID=a.CBORD_ID   
            INNER JOIN PFR_BASIC.SECURITY s ON s.ID = i.SEC_ID
            INNER JOIN PFR_BASIC.ORDER o ON o.ID=i.ORD_ID
            INNER JOIN PFR_BASIC.PORTFOLIO p ON p.ID = coalesce(bp.PF_ID, o.PF_ID)
            INNER JOIN PFR_BASIC.PFRBANKACCOUNT b ON b.ID = coalesce(bp.BACC_ID , o.TRD_ID, o.TRANSIT_ID, o.CURRENT_ID, o.DEPO_ID) 
            LEFT JOIN PFR_BASIC.YEARS y ON p.YEAR_ID=y.ID   

            WHERE a.UNLINKED = 0 AND o.TYPE='продажа' AND a.DATEORDER >= '{from:yyyy-MM-dd}' AND a.DATEORDER < '{to:yyyy-MM-dd}'")
                    .List<decimal>().Sum(a => a);
                var list = GetIncomeSecurityList(null, "Купонный доход", false).Where(a => a.OrderDate.HasValue && a.OrderDate.Value.Date >= from.Date && a.OrderDate.Value.Date < to.Date);
                return dohod + list.Sum(a => a.Total);
            });
        }

        public decimal GetTempAllocSumForBOReport(int year, int quartal, bool qOnly)
        {
            //Allocate Deposit
            var fromMonth = qOnly ? DateTools.GetFirstMonthOfQuarterForBO(quartal) : 1;
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = toMonth == 1 ? year + 1 : year;

            var dt = new DateTime(year, fromMonth, 1);
            var toDt = new DateTime(toYear, toMonth, 1);

            using (var session = DataAccessSystem.OpenSession())
            {
                return session.CreateQuery(@"
                        select sum(coalesce(s.Sum,0))
                        from d in Deposit
                        join d.DepClaimOffer o
                        join o.DepClaim dc2 
                        join o.OfferPfrBankAccountSum s
                        join s.Portfolio p
                        where s.StatusID>0 AND (dc2.SettleDate >= :from AND dc2.SettleDate < :to)
                        ")
                    .SetParameter("from", dt)
                    .SetParameter("to", toDt)
                    .UniqueResult<decimal>();
            }

        }

        public decimal GetDepPercForBOReport(int year, int quartal, bool qOnly, long? portfolioId = null)
        {
            var fromMonth = qOnly ? DateTools.GetFirstMonthOfQuarterForBO(quartal) : 1;
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = toMonth == 1 ? year + 1 : year;

            var from = new DateTime(year, fromMonth, 1);
            var to = new DateTime(toYear, toMonth, 1);

            return SessionWrapper(session =>
            {
                var crit = session.CreateCriteria(typeof(IncomesecHib), "IncomesecHib")
                    .AddOrder(Order.Asc("OrderDate"));
                crit.Add(Restrictions.Ge("OrderDate", from));
                crit.Add(Restrictions.Lt("OrderDate", to));
                crit.Add(Restrictions.Eq("IncomeKind", IncomeSecIdentifier.s_DepositsPercents));

                crit.CreateCriteria("Portfolio", "Portfolio", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join);
                if (portfolioId != null)
                    crit.GetCriteriaByAlias("Portfolio").Add(Restrictions.Eq("ID", portfolioId.Value));

                crit.CreateCriteria("PfrBankAccount", JoinType.InnerJoin)
                    .CreateCriteria("Currency", "Currency", JoinType.LeftOuterJoin)
                    .SetFetchMode("Currency", FetchMode.Join);
                crit.CreateCriteria("Curs", "Curs", JoinType.LeftOuterJoin)
                    .SetFetchMode("Curs", FetchMode.Join);

                var sum1 = crit.List<IncomesecHib>().Where(a => a.OrderSum.HasValue).Sum(a => a.OrderSum.Value);

                var sum2 = session.CreateQuery(@"
                        select sum(coalesce(ph.Sum,0))
                            from PaymentHistory ph
                            join ph.OfferPfrBankAccountSum s
                            join s.Portfolio p
                        where ph.ForPercent = 1 and s.StatusID>0 and coalesce(ph.OrderDate, ph.Date) >=:sdate and coalesce(ph.OrderDate, ph.Date) < :edate")
                    .SetParameter("sdate", from)
                    .SetParameter("edate", to)
                    .UniqueResult<decimal>();
                return sum1 + sum2;
            });
        }


 

        public decimal GetReturnedDepSumForPortfolio(long pid, int year, int quartal)
        {
            var fromMonth = DateTools.GetFirstMonthOfQuarterForBO(quartal);
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = toMonth == 1 ? year + 1 : year;

            var from = new DateTime(year, fromMonth, 1);
            var to = new DateTime(toYear, toMonth, 1);

            return SessionWrapper(session =>
            {
                var sum = session.CreateQuery(@"
                        select sum(coalesce(ph.Sum,0))
                            from PaymentHistory ph
                            join ph.OfferPfrBankAccountSum s
                            join s.Portfolio p
                        where ph.ForPercent = 0 and s.StatusID>0 and coalesce(ph.OrderDate, ph.Date) >=:sdate and coalesce(ph.OrderDate, ph.Date) < :edate and p.ID = :pid")
                    .SetParameter("sdate", from)
                    .SetParameter("edate", to)
                    .SetParameter("pid", pid)
                    .UniqueResult<decimal>();
                return sum;
            });
        }

        public decimal GetCostsForBOReport(int year, int quartal, bool qOnly, string kind)
        {
            var fromMonth = qOnly ? DateTools.GetFirstMonthOfQuarterForBO(quartal) : 1;
            var toMonth = DateTools.GetLastMonthOfQuarterForBO(quartal);
            var toYear = toMonth == 1 ? year + 1 : year;

            var from = new DateTime(year, fromMonth, 1);
            var to = new DateTime(toYear, toMonth, 1);

            return SessionWrapper(session =>
            {
                var crit = session.CreateCriteria<CostHib>("Cost")
                    .CreateCriteria("Portfolio", "p", JoinType.InnerJoin)
                    .SetFetchMode("Portfolio", FetchMode.Join)
                    .CreateCriteria("Cost.Currency", "c", JoinType.InnerJoin)
                    .SetFetchMode("Cost.Currency", FetchMode.Join);

                crit.Add(Restrictions.Eq("Cost.Kind", kind));
                crit.Add(Restrictions.Ge("Cost.PaymentDate", from));
                crit.Add(Restrictions.Lt("Cost.PaymentDate", to));
                return crit.List<Cost>().Where(a => a.PaymentSumm.HasValue).ToList().Sum(a => a.PaymentSumm ?? 0);
            });
        }
    }
}