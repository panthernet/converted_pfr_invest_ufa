﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Proxy;
using PFR_INVEST.Proxy.Tools;
#if !WEBCLIENT
using db2connector.Contract;
#endif


namespace db2connector
{
	[MessageCompression(Compress.Reply | Compress.Request)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

	public partial class IISService : IISServiceHib, IDB2Connector
	{
		private static IISService _mLocalInstance;
		 
		internal static IISService LocalInstance 
		{
			get 
			{
			    if (_mLocalInstance != null) return _mLocalInstance;
			    _mLocalInstance = new IISService();
			    _mLocalInstance.SetCurrentUserInformation("localService", "", "");
			    return _mLocalInstance;
			}
		}

        /// <summary>
        /// Добавление или обновление курса на дату и тип
        /// </summary>
        /// <param name="rate">Курс</param>
		public long AddRate(Curs rate)
		{
		    return SessionWrapper(session =>
		    {
		        var list = GetListByPropertyConditions(typeof(Curs), session, new List<ListPropertyCondition> {
                    ListPropertyCondition.Equal("Date", rate.Date),
                    ListPropertyCondition.Equal("CurrencyID", rate.CurrencyID)
                    }).Cast<Curs>().ToList();
		        if (list.Count > 0)
		        {
		            list.First().Date = rate.Date;
		            list.First().Value = rate.Value;
		            return list.First().ID;
		        }
		        return SaveEntity(rate);
		    });
		}

        public long GetCurrencyID(string name)
        {
            return SessionWrapper(session =>
            {
                var item = GetListByProperty<Currency>("Name", name, session).Cast<Currency>().FirstOrDefault();
                return item?.ID ?? -1;
            });
        }

        public List<ERZL> GetERZLList()
        {
            return GetList<ERZL>().Cast<ERZL>().ToList();
        }

        public List<FZ> GetFZList()
        {
            return GetList<FZ>().Cast<FZ>().ToList();
        }

        private Dictionary<string, DBField> GenerateDBDic(object[] res, string fieldString, int indexOffset = 0)
        {
            var result = new Dictionary<string, DBField>();
            if (res == null) return result;
            var fNameList = fieldString.Replace("PFR_BASIC.", "").Split(',').Select(a=> a.Trim()).ToList();
            for (int i = 0; i < fNameList.Count; i++)
            {
                //убираем as
                if (fNameList[i].ToLower().Contains(" as "))
                    fNameList[i] = fNameList[i].Split(' ')[0];
                result.Add(fNameList[i], GenerateDBField(fNameList[i], res[indexOffset + i]));
            }
            return result;
        }

        /// <summary>
        /// Новый метод для компоновки списка DBField пока не будет произведена замена логики
        /// </summary>
	    private DBField GenerateDBField(string name, object value)
        {
            DATA_TYPE type = DATA_TYPE.String;
            short fr = 0;
            if (value is int || value is long || value is short)
                type = DATA_TYPE.Long;
            else if (value is decimal || value is double)
            {
                type = DATA_TYPE.Double;
                fr = 4;
                if (IsDecimalFixNeeded)
                    value = (decimal)value/10000M;
            }
            else if (value is DateTime)
                type = DATA_TYPE.Date;

            string tableName;
            try
            {
                tableName = name.Split('.')[0].Trim();
            }
            catch
            {
                tableName = "";
            }

            return new DBField(name, "", "PFR_BASIC", tableName, FIELD_TYPE.Data, type, fr) {Value = value};
        }

        public Dictionary<string, DBField> GetF50(long id)
        {
            const string selectQuery = @"PFR_BASIC.EDO_ODKF050.ID_CONTRACT, PFR_BASIC.EDO_ODKF050.REGNUMBEROUT, PFR_BASIC.EDO_ODKF050.ID as i1, PFR_BASIC.EDO_ODKF050.REPORTDATE, PFR_BASIC.EDO_ODKF050.GCB1, PFR_BASIC.EDO_ODKF050.GCB2, PFR_BASIC.EDO_ODKF050.GCBSUBRF1, PFR_BASIC.EDO_ODKF050.GCBSUBRF2, PFR_BASIC.EDO_ODKF050.OBLIGACIIMO1, PFR_BASIC.EDO_ODKF050.OBLIGACIIMO2, PFR_BASIC.EDO_ODKF050.OBLIGACIIRHO1, PFR_BASIC.EDO_ODKF050.OBLIGACIIRHO2, PFR_BASIC.EDO_ODKF050.AKCIIRAO1, PFR_BASIC.EDO_ODKF050.AKCIIRAO2, PFR_BASIC.EDO_ODKF050.PAI1, PFR_BASIC.EDO_ODKF050.PAI2, PFR_BASIC.EDO_ODKF050.IPOTECH1, PFR_BASIC.EDO_ODKF050.IPOTECH2, PFR_BASIC.EDO_ODKF050.ITOGOCB1, PFR_BASIC.EDO_ODKF050.ITOGOCB2, PFR_BASIC.EDO_ODKF050.CHIEF, PFR_BASIC.EDO_ODKF050.RESPPERSON, PFR_BASIC.EDO_ODKF050.POST, PFR_BASIC.EDO_ODKF050.REGNUM as r1, PFR_BASIC.EDO_ODKF050.REGDATE as d1, PFR_BASIC.EDO_ODKF050.OBLIGMFO1, PFR_BASIC.EDO_ODKF050.OBLIGMFO2, PFR_BASIC.EDO_ODKF050.PORTFOLIO as p1, PFR_BASIC.CONTRACT.ID as i2, PFR_BASIC.CONTRACT.LE_ID, PFR_BASIC.CONTRACT.BA_ID, PFR_BASIC.CONTRACT.FULLNAME as fn1, PFR_BASIC.CONTRACT.FORMALNAME, PFR_BASIC.CONTRACT.REGDATE as d2, PFR_BASIC.CONTRACT.REGNUM as r2, PFR_BASIC.CONTRACT.REGCLOSE, PFR_BASIC.CONTRACT.REGADDCLOSE, PFR_BASIC.CONTRACT.PORTFNAME, PFR_BASIC.CONTRACT.DISSOLDATE, PFR_BASIC.CONTRACT.DISSOLBASE, PFR_BASIC.CONTRACT.DOCNUM, PFR_BASIC.CONTRACT.DOCDATE, PFR_BASIC.CONTRACT.OTHERINFO, PFR_BASIC.CONTRACT.CONT_N_ID, PFR_BASIC.CONTRACT.DT_ID, PFR_BASIC.CONTRACT.OPERATESUMM, PFR_BASIC.CONTRACT.STATUS, PFR_BASIC.CONTRACT.TYPEID, PFR_BASIC.CONTRACT.LOTUS_GUID, PFR_BASIC.CONTRACT.PORTFOLIO_CODE, PFR_BASIC.LEGALENTITY.ID as i3, PFR_BASIC.LEGALENTITY.FULLNAME as fn2, PFR_BASIC.LEGALENTITY.INN, PFR_BASIC.LEGALENTITY.HEADPOSITION, PFR_BASIC.LEGALENTITY.HEADFULLNAME, PFR_BASIC.LEGALENTITY.LEGALADDRESS, PFR_BASIC.LEGALENTITY.STREETADDRESS, PFR_BASIC.LEGALENTITY.POSTADDRESS, PFR_BASIC.LEGALENTITY.PHONE, PFR_BASIC.LEGALENTITY.FAX, PFR_BASIC.LEGALENTITY.COMMENT, PFR_BASIC.LEGALENTITY.EADDRESS, PFR_BASIC.LEGALENTITY.CONTRAGENTID, PFR_BASIC.LEGALENTITY.REGISTRATIONDATE, PFR_BASIC.LEGALENTITY.CLOSEDATE, PFR_BASIC.LEGALENTITY.SHORTNAME, PFR_BASIC.LEGALENTITY.REGISTRATIONNUM, PFR_BASIC.LEGALENTITY.FORMALIZEDNAME, PFR_BASIC.LEGALENTITY.SITE, PFR_BASIC.LEGALENTITY.SUBSIDIARIES, PFR_BASIC.LEGALENTITY.REGISTRATOR, PFR_BASIC.LEGALENTITY.PDREASON, PFR_BASIC.LEGALENTITY.OKPP, PFR_BASIC.LEGALENTITY.DECLNAME, PFR_BASIC.LEGALENTITY.LETTERWHO, PFR_BASIC.LEGALENTITY.TRANSFDOCKIND, PFR_BASIC.LEGALENTITY.LS_ID, PFR_BASIC.LEGALENTITY.PFBA_ID, PFR_BASIC.LEGALENTITY.INFO, PFR_BASIC.LEGALENTITY.PFRAGR, PFR_BASIC.LEGALENTITY.PFRAGRSTAT, PFR_BASIC.LEGALENTITY.MONEY, PFR_BASIC.LEGALENTITY.MONEYDATE, PFR_BASIC.LEGALENTITY.FITCH, PFR_BASIC.LEGALENTITY.FITCHDATE, PFR_BASIC.LEGALENTITY.STANDARD, PFR_BASIC.LEGALENTITY.STANDARDDATE, PFR_BASIC.LEGALENTITY.MOODY, PFR_BASIC.LEGALENTITY.MOODYDATE, PFR_BASIC.LEGALENTITY.CORRACC, PFR_BASIC.LEGALENTITY.BIK, PFR_BASIC.LEGALENTITY.FORLETTER, PFR_BASIC.LEGALENTITY.SEX, PFR_BASIC.LEGALENTITY.ENDDATE, PFR_BASIC.LEGALENTITY.STOCKCODE, PFR_BASIC.LEGALENTITY.PFRAGRDATE, PFR_BASIC.LEGALENTITY.LOTUS_GUID, PFR_BASIC.LEGALENTITY.LOTUS_ID, PFR_BASIC.LEGALENTITY.LOTUS_TABLE, PFR_BASIC.LEGALENTITY.CORRACCOUNTNOTE, PFR_BASIC.LEGALENTITY.GARANT_EL_ID, PFR_BASIC.LEGALENTITY.FUND_NUM, PFR_BASIC.LEGALENTITY.FUND_DATE, PFR_BASIC.LEGALENTITY.NOTIF_NUM, PFR_BASIC.LEGALENTITY.NOTIF_DATE, PFR_BASIC.LEGALENTITY.NUMBER_FOR_DOCS";
            return SessionWrapper(session =>
            {
                var res = session.CreateSQLQuery(
                    $@"SELECT {selectQuery} FROM PFR_BASIC.EDO_ODKF050
                                LEFT JOIN PFR_BASIC.CONTRACT ON PFR_BASIC.EDO_ODKF050.ID_CONTRACT = PFR_BASIC.CONTRACT.ID 
                                LEFT JOIN PFR_BASIC.LEGALENTITY ON PFR_BASIC.CONTRACT.LE_ID = PFR_BASIC.LEGALENTITY.ID 
                                WHERE PFR_BASIC.EDO_ODKF050.ID = :id")
                    .SetParameter("id", id)
                    .SetMaxResults(1)
                    .UniqueResult<object[]>();
                return GenerateDBDic(res, selectQuery, IsDB2 ? 1 : 0);
            });
        }

	    public Dictionary<string, DBField> GetF40(long id)
        {
            const string selectQuery =
                @"PFR_BASIC.EDO_ODKF040.PORTFOLIO as p2, PFR_BASIC.EDO_ODKF040.ID as i4, PFR_BASIC.EDO_ODKF040.ID_ONDATE, PFR_BASIC.EDO_ODKF040.ID_YEAR, PFR_BASIC.EDO_ODKF040.ID_CONTRACT, PFR_BASIC.EDO_ODKF040.REGNUMBEROUT, PFR_BASIC.EDO_ODKF040.AP_NAME, PFR_BASIC.EDO_ODKF040.AP_POST, PFR_BASIC.EDO_ODKF040.REGNUM as fg3, PFR_BASIC.EDO_ODKF040.REGDATE as fd3, PFR_BASIC.EDO_ODKF040.REPORTTYPE, PFR_BASIC.EDO_ODKF040.WRITEDATE, PFR_BASIC.EDO_ODKF040.LOTUS_ID, PFR_BASIC.EDO_ODKF040.LOTUS_TABLE, PFR_BASIC.EDO_ODKF040.CUSTOM_BUY_COUNT, PFR_BASIC.EDO_ODKF040.CUSTOM_BUY_AMOUNT, PFR_BASIC.EDO_ODKF040.CUSTOM_SELL_COUNT, PFR_BASIC.EDO_ODKF040.CUSTOM_SELL_AMOUNT, PFR_BASIC.EDO_ODKF040.LOTUS_GUID, PFR_BASIC.F40_RRZ.ID as i5, PFR_BASIC.F40_RRZ.GCBRFQTYBUYCOUNT, PFR_BASIC.F40_RRZ.GCBRFAMOUNTBUYCOUNT, PFR_BASIC.F40_RRZ.GCBRFQTYSELLCOUNT, PFR_BASIC.F40_RRZ.GCBRFAMOUNTSELLCOUNT, PFR_BASIC.F40_RRZ.ID_EDO as i66, PFR_BASIC.F40_RRZ.RRZ, PFR_BASIC.F40_DETAILS.ID as i6, PFR_BASIC.F40_DETAILS.GCBRFBROKER, PFR_BASIC.F40_DETAILS.GCBRFEXCHANGE, PFR_BASIC.F40_DETAILS.GCBRFQTYBUY, PFR_BASIC.F40_DETAILS.GCBRFAMOUNTBUY, PFR_BASIC.F40_DETAILS.GCBRFQTYSELL, PFR_BASIC.F40_DETAILS.GCBRFAMOUNTSELL, PFR_BASIC.F40_DETAILS.ID_EDO as i67, PFR_BASIC.F40_DETAILS.DETAILS, PFR_BASIC.F40_DETAILS.LOTUS_ID, PFR_BASIC.F40_DETAILS.LOTUS_TABLE, PFR_BASIC.F40_DETAILS.LOTUS_GUID, PFR_BASIC.CONTRACT.ID as i7, PFR_BASIC.CONTRACT.LE_ID, PFR_BASIC.CONTRACT.BA_ID, PFR_BASIC.CONTRACT.FULLNAME as fn3, PFR_BASIC.CONTRACT.FORMALNAME, PFR_BASIC.CONTRACT.REGDATE as dd4, PFR_BASIC.CONTRACT.REGNUM as rr4, PFR_BASIC.CONTRACT.REGCLOSE, PFR_BASIC.CONTRACT.REGADDCLOSE, PFR_BASIC.CONTRACT.PORTFNAME, PFR_BASIC.CONTRACT.DISSOLDATE, PFR_BASIC.CONTRACT.DISSOLBASE, PFR_BASIC.CONTRACT.DOCNUM, PFR_BASIC.CONTRACT.DOCDATE, PFR_BASIC.CONTRACT.OTHERINFO, PFR_BASIC.CONTRACT.CONT_N_ID, PFR_BASIC.CONTRACT.DT_ID, PFR_BASIC.CONTRACT.OPERATESUMM, PFR_BASIC.CONTRACT.STATUS, PFR_BASIC.CONTRACT.LOTUS_ID, PFR_BASIC.CONTRACT.LOTUS_TABLE, PFR_BASIC.CONTRACT.TYPEID, PFR_BASIC.CONTRACT.LOTUS_GUID, PFR_BASIC.CONTRACT.PORTFOLIO_CODE, PFR_BASIC.LEGALENTITY.ID as i8, PFR_BASIC.LEGALENTITY.FULLNAME as fn4, PFR_BASIC.LEGALENTITY.INN, PFR_BASIC.LEGALENTITY.HEADPOSITION, PFR_BASIC.LEGALENTITY.HEADFULLNAME, PFR_BASIC.LEGALENTITY.LEGALADDRESS, PFR_BASIC.LEGALENTITY.STREETADDRESS, PFR_BASIC.LEGALENTITY.POSTADDRESS, PFR_BASIC.LEGALENTITY.PHONE, PFR_BASIC.LEGALENTITY.FAX, PFR_BASIC.LEGALENTITY.COMMENT, PFR_BASIC.LEGALENTITY.EADDRESS, PFR_BASIC.LEGALENTITY.CONTRAGENTID, PFR_BASIC.LEGALENTITY.REGISTRATIONDATE, PFR_BASIC.LEGALENTITY.CLOSEDATE, PFR_BASIC.LEGALENTITY.SHORTNAME, PFR_BASIC.LEGALENTITY.REGISTRATIONNUM, PFR_BASIC.LEGALENTITY.FORMALIZEDNAME, PFR_BASIC.LEGALENTITY.SITE, PFR_BASIC.LEGALENTITY.SUBSIDIARIES, PFR_BASIC.LEGALENTITY.REGISTRATOR, PFR_BASIC.LEGALENTITY.PDREASON, PFR_BASIC.LEGALENTITY.OKPP, PFR_BASIC.LEGALENTITY.DECLNAME, PFR_BASIC.LEGALENTITY.LETTERWHO, PFR_BASIC.LEGALENTITY.TRANSFDOCKIND, PFR_BASIC.LEGALENTITY.LS_ID, PFR_BASIC.LEGALENTITY.PFBA_ID, PFR_BASIC.LEGALENTITY.INFO, PFR_BASIC.LEGALENTITY.PFRAGR, PFR_BASIC.LEGALENTITY.PFRAGRSTAT, PFR_BASIC.LEGALENTITY.MONEY, PFR_BASIC.LEGALENTITY.MONEYDATE, PFR_BASIC.LEGALENTITY.FITCH, PFR_BASIC.LEGALENTITY.FITCHDATE, PFR_BASIC.LEGALENTITY.STANDARD, PFR_BASIC.LEGALENTITY.STANDARDDATE, PFR_BASIC.LEGALENTITY.MOODY, PFR_BASIC.LEGALENTITY.MOODYDATE, PFR_BASIC.LEGALENTITY.CORRACC, PFR_BASIC.LEGALENTITY.BIK, PFR_BASIC.LEGALENTITY.FORLETTER, PFR_BASIC.LEGALENTITY.SEX, PFR_BASIC.LEGALENTITY.ENDDATE, PFR_BASIC.LEGALENTITY.STOCKCODE, PFR_BASIC.LEGALENTITY.PFRAGRDATE, PFR_BASIC.LEGALENTITY.LOTUS_GUID, PFR_BASIC.LEGALENTITY.LOTUS_ID, PFR_BASIC.LEGALENTITY.LOTUS_TABLE, PFR_BASIC.LEGALENTITY.CORRACCOUNTNOTE, PFR_BASIC.LEGALENTITY.GARANT_EL_ID, PFR_BASIC.LEGALENTITY.FUND_NUM, PFR_BASIC.LEGALENTITY.FUND_DATE, PFR_BASIC.LEGALENTITY.NOTIF_NUM, PFR_BASIC.LEGALENTITY.NOTIF_DATE, PFR_BASIC.LEGALENTITY.NUMBER_FOR_DOCS, PFR_BASIC.YEARS.ID as i9, PFR_BASIC.YEARS.NAME as n1, PFR_BASIC.MONTHS.ID as i10, PFR_BASIC.MONTHS.NAME as n2";

            return SessionWrapper(session =>
            {
                var res = session.CreateSQLQuery($@"
                    SELECT  {selectQuery}
                    FROM PFR_BASIC.EDO_ODKF040 
                    LEFT JOIN PFR_BASIC.F40_RRZ ON PFR_BASIC.F40_RRZ.ID_EDO = PFR_BASIC.EDO_ODKF040.ID 
                    LEFT JOIN PFR_BASIC.F40_DETAILS ON PFR_BASIC.F40_DETAILS.ID_EDO = PFR_BASIC.EDO_ODKF040.ID 
                    LEFT JOIN PFR_BASIC.CONTRACT ON PFR_BASIC.EDO_ODKF040.ID_CONTRACT = PFR_BASIC.CONTRACT.ID 
                    LEFT JOIN PFR_BASIC.LEGALENTITY ON PFR_BASIC.LEGALENTITY.ID = PFR_BASIC.CONTRACT.LE_ID 
                    LEFT JOIN PFR_BASIC.YEARS ON PFR_BASIC.EDO_ODKF040.ID_YEAR = PFR_BASIC.YEARS.ID 
                    LEFT JOIN PFR_BASIC.MONTHS ON PFR_BASIC.EDO_ODKF040.ID_ONDATE = PFR_BASIC.MONTHS.ID 
                    WHERE PFR_BASIC.EDO_ODKF040.ID = :id
                                            ").SetParameter("id", id).SetMaxResults(1).UniqueResult<object[]>();

                return GenerateDBDic(res, selectQuery, IsDB2 ? 1 : 0);
            });
        }

        public Dictionary<string, DBField> GetF60(long id)
        {
            const string selectQuery = @"PFR_BASIC.EDO_ODKF060.ID as i11, PFR_BASIC.EDO_ODKF060.ID_CONTRACT, PFR_BASIC.EDO_ODKF060.ID_YEAR, PFR_BASIC.EDO_ODKF060.QUARTAL, PFR_BASIC.EDO_ODKF060.SCHASTART1, PFR_BASIC.EDO_ODKF060.SCHASTART2, PFR_BASIC.EDO_ODKF060.PAYMENTRECEIVED1, PFR_BASIC.EDO_ODKF060.PAYMENTRECEIVED2, PFR_BASIC.EDO_ODKF060.PAYMENTTRANSFER1, PFR_BASIC.EDO_ODKF060.PAYMENTTRANSFER2, PFR_BASIC.EDO_ODKF060.PROFIT1, PFR_BASIC.EDO_ODKF060.PROFIT2, PFR_BASIC.EDO_ODKF060.DETAINED1, PFR_BASIC.EDO_ODKF060.DETAINED2, PFR_BASIC.EDO_ODKF060.DETAINEDUKREWARD1, PFR_BASIC.EDO_ODKF060.DETAINEDUKREWARD2, PFR_BASIC.EDO_ODKF060.SCHAEND1, PFR_BASIC.EDO_ODKF060.SCHAEND2, PFR_BASIC.EDO_ODKF060.REGNUMBEROUT, PFR_BASIC.EDO_ODKF060.REGNUM as fr5, PFR_BASIC.EDO_ODKF060.REGDATE as fd5, PFR_BASIC.EDO_ODKF060.INVESTCASENAME, PFR_BASIC.EDO_ODKF060.KPP, PFR_BASIC.EDO_ODKF060.LOTUS_ID, PFR_BASIC.EDO_ODKF060.LOTUS_TABLE, PFR_BASIC.EDO_ODKF060.LOTUS_GUID, PFR_BASIC.CONTRACT.ID as i12, PFR_BASIC.CONTRACT.LE_ID, PFR_BASIC.CONTRACT.BA_ID, PFR_BASIC.CONTRACT.FULLNAME as fn5, PFR_BASIC.CONTRACT.FORMALNAME, PFR_BASIC.CONTRACT.REGDATE as fd6, PFR_BASIC.CONTRACT.REGNUM as fr6, PFR_BASIC.CONTRACT.REGCLOSE, PFR_BASIC.CONTRACT.REGADDCLOSE, PFR_BASIC.CONTRACT.PORTFNAME, PFR_BASIC.CONTRACT.DISSOLDATE, PFR_BASIC.CONTRACT.DISSOLBASE, PFR_BASIC.CONTRACT.DOCNUM, PFR_BASIC.CONTRACT.DOCDATE, PFR_BASIC.CONTRACT.OTHERINFO, PFR_BASIC.CONTRACT.CONT_N_ID, PFR_BASIC.CONTRACT.DT_ID, PFR_BASIC.CONTRACT.OPERATESUMM, PFR_BASIC.CONTRACT.STATUS, PFR_BASIC.CONTRACT.LOTUS_ID, PFR_BASIC.CONTRACT.LOTUS_TABLE, PFR_BASIC.CONTRACT.TYPEID, PFR_BASIC.CONTRACT.LOTUS_GUID, PFR_BASIC.CONTRACT.PORTFOLIO_CODE, PFR_BASIC.YEARS.ID as i13, PFR_BASIC.YEARS.NAME as n3, PFR_BASIC.LEGALENTITY.ID as i14, PFR_BASIC.LEGALENTITY.FULLNAME as fn6, PFR_BASIC.LEGALENTITY.INN, PFR_BASIC.LEGALENTITY.HEADPOSITION, PFR_BASIC.LEGALENTITY.HEADFULLNAME, PFR_BASIC.LEGALENTITY.LEGALADDRESS, PFR_BASIC.LEGALENTITY.STREETADDRESS, PFR_BASIC.LEGALENTITY.POSTADDRESS, PFR_BASIC.LEGALENTITY.PHONE, PFR_BASIC.LEGALENTITY.FAX, PFR_BASIC.LEGALENTITY.COMMENT, PFR_BASIC.LEGALENTITY.EADDRESS, PFR_BASIC.LEGALENTITY.CONTRAGENTID, PFR_BASIC.LEGALENTITY.REGISTRATIONDATE, PFR_BASIC.LEGALENTITY.CLOSEDATE, PFR_BASIC.LEGALENTITY.SHORTNAME, PFR_BASIC.LEGALENTITY.REGISTRATIONNUM, PFR_BASIC.LEGALENTITY.FORMALIZEDNAME, PFR_BASIC.LEGALENTITY.SITE, PFR_BASIC.LEGALENTITY.SUBSIDIARIES, PFR_BASIC.LEGALENTITY.REGISTRATOR, PFR_BASIC.LEGALENTITY.PDREASON, PFR_BASIC.LEGALENTITY.OKPP, PFR_BASIC.LEGALENTITY.DECLNAME, PFR_BASIC.LEGALENTITY.LETTERWHO, PFR_BASIC.LEGALENTITY.TRANSFDOCKIND, PFR_BASIC.LEGALENTITY.LS_ID, PFR_BASIC.LEGALENTITY.PFBA_ID, PFR_BASIC.LEGALENTITY.INFO, PFR_BASIC.LEGALENTITY.PFRAGR, PFR_BASIC.LEGALENTITY.PFRAGRSTAT, PFR_BASIC.LEGALENTITY.MONEY, PFR_BASIC.LEGALENTITY.MONEYDATE, PFR_BASIC.LEGALENTITY.FITCH, PFR_BASIC.LEGALENTITY.FITCHDATE, PFR_BASIC.LEGALENTITY.STANDARD, PFR_BASIC.LEGALENTITY.STANDARDDATE, PFR_BASIC.LEGALENTITY.MOODY, PFR_BASIC.LEGALENTITY.MOODYDATE, PFR_BASIC.LEGALENTITY.CORRACC, PFR_BASIC.LEGALENTITY.BIK, PFR_BASIC.LEGALENTITY.FORLETTER, PFR_BASIC.LEGALENTITY.SEX, PFR_BASIC.LEGALENTITY.ENDDATE, PFR_BASIC.LEGALENTITY.STOCKCODE, PFR_BASIC.LEGALENTITY.PFRAGRDATE, PFR_BASIC.LEGALENTITY.LOTUS_GUID, PFR_BASIC.LEGALENTITY.LOTUS_ID, PFR_BASIC.LEGALENTITY.LOTUS_TABLE, PFR_BASIC.LEGALENTITY.CORRACCOUNTNOTE, PFR_BASIC.LEGALENTITY.GARANT_EL_ID, PFR_BASIC.LEGALENTITY.FUND_NUM, PFR_BASIC.LEGALENTITY.FUND_DATE, PFR_BASIC.LEGALENTITY.NOTIF_NUM, PFR_BASIC.LEGALENTITY.NOTIF_DATE, PFR_BASIC.LEGALENTITY.NUMBER_FOR_DOCS";
            return SessionWrapper(session =>
            {
                var res = session.CreateSQLQuery(
                    $@"SELECT {selectQuery} FROM PFR_BASIC.EDO_ODKF060 
                                LEFT JOIN PFR_BASIC.CONTRACT ON PFR_BASIC.EDO_ODKF060.ID_CONTRACT = PFR_BASIC.CONTRACT.ID 
                                LEFT JOIN PFR_BASIC.YEARS ON PFR_BASIC.EDO_ODKF060.ID_YEAR = PFR_BASIC.YEARS.ID 
                                LEFT JOIN PFR_BASIC.LEGALENTITY ON PFR_BASIC.CONTRACT.LE_ID = PFR_BASIC.LEGALENTITY.ID 
                                WHERE PFR_BASIC.EDO_ODKF060.ID = :id")
                    .SetParameter("id", id).SetMaxResults(1).UniqueResult<object[]>();
                return GenerateDBDic(res, selectQuery, IsDB2 ? 1 : 0);
            });
        }

        public Dictionary<string, DBField> GetF70(long id)
        {
            const string selectQuery = @"PFR_BASIC.EDO_ODKF070.OTHERPAY2, PFR_BASIC.EDO_ODKF070.UKREWARD1, PFR_BASIC.EDO_ODKF070.UKREWARD2, PFR_BASIC.EDO_ODKF070.UKREWARD, PFR_BASIC.EDO_ODKF070.TRANSHTOTAL, PFR_BASIC.EDO_ODKF070.JANUARY, PFR_BASIC.EDO_ODKF070.FEBRUARY, PFR_BASIC.EDO_ODKF070.MARCH, PFR_BASIC.EDO_ODKF070.APRIL, PFR_BASIC.EDO_ODKF070.MAY, PFR_BASIC.EDO_ODKF070.JUNE, PFR_BASIC.EDO_ODKF070.JULY, PFR_BASIC.EDO_ODKF070.AUGUST, PFR_BASIC.EDO_ODKF070.SEPTEMBER, PFR_BASIC.EDO_ODKF070.OCTOBER, PFR_BASIC.EDO_ODKF070.NOVEMBER, PFR_BASIC.EDO_ODKF070.DECEMBER, PFR_BASIC.EDO_ODKF070.MAXUKPAY, PFR_BASIC.EDO_ODKF070.FACTUKPAY, PFR_BASIC.EDO_ODKF070.MAXSDPAY, PFR_BASIC.EDO_ODKF070.FACTSDPAY, PFR_BASIC.EDO_ODKF070.PROFITPROC, PFR_BASIC.EDO_ODKF070.UKREWARDPROC, PFR_BASIC.EDO_ODKF070.REGNUMBEROUT, PFR_BASIC.EDO_ODKF070.REGNUM as fr7, PFR_BASIC.EDO_ODKF070.REGDATE as fd7, PFR_BASIC.EDO_ODKF070.MIDVALUE, PFR_BASIC.EDO_ODKF070.ID as i15, PFR_BASIC.EDO_ODKF070.ID_CONTRACT, PFR_BASIC.EDO_ODKF070.ID_YEAR, PFR_BASIC.EDO_ODKF070.QUARTAL, PFR_BASIC.EDO_ODKF070.PROFIT1, PFR_BASIC.EDO_ODKF070.PROFIT2, PFR_BASIC.EDO_ODKF070.ACTIVSALE1, PFR_BASIC.EDO_ODKF070.ACTIVSALE2, PFR_BASIC.EDO_ODKF070.DIVIDEND1, PFR_BASIC.EDO_ODKF070.DIVIDEND2, PFR_BASIC.EDO_ODKF070.PROC1, PFR_BASIC.EDO_ODKF070.PROC2, PFR_BASIC.EDO_ODKF070.ACTIVREVALUE1, PFR_BASIC.EDO_ODKF070.ACTIVREVALUE2, PFR_BASIC.EDO_ODKF070.OTHERPROFIT1, PFR_BASIC.EDO_ODKF070.OTHERPROFIT2, PFR_BASIC.EDO_ODKF070.RETAINED1, PFR_BASIC.EDO_ODKF070.RETAINED2, PFR_BASIC.EDO_ODKF070.SDPAY1, PFR_BASIC.EDO_ODKF070.SDPAY2, PFR_BASIC.EDO_ODKF070.BROKERPAY1, PFR_BASIC.EDO_ODKF070.BROKERPAY2, PFR_BASIC.EDO_ODKF070.AUDITPAY1, PFR_BASIC.EDO_ODKF070.AUDITPAY2, PFR_BASIC.EDO_ODKF070.STRAHPAY1, PFR_BASIC.EDO_ODKF070.STRAHPAY2, PFR_BASIC.EDO_ODKF070.OTHERPAY1, PFR_BASIC.EDO_ODKF070.INVESTCASENAME, PFR_BASIC.EDO_ODKF070.KPP, PFR_BASIC.EDO_ODKF070.LOTUS_ID, PFR_BASIC.EDO_ODKF070.LOTUS_TABLE, PFR_BASIC.EDO_ODKF070.LOTUS_GUID, PFR_BASIC.CONTRACT.ID as i16, PFR_BASIC.CONTRACT.LE_ID, PFR_BASIC.CONTRACT.BA_ID, PFR_BASIC.CONTRACT.FULLNAME as fn7, PFR_BASIC.CONTRACT.FORMALNAME, PFR_BASIC.CONTRACT.REGDATE as fd8, PFR_BASIC.CONTRACT.REGNUM as fr8, PFR_BASIC.CONTRACT.REGCLOSE, PFR_BASIC.CONTRACT.REGADDCLOSE, PFR_BASIC.CONTRACT.PORTFNAME, PFR_BASIC.CONTRACT.DISSOLDATE, PFR_BASIC.CONTRACT.DISSOLBASE, PFR_BASIC.CONTRACT.DOCNUM, PFR_BASIC.CONTRACT.DOCDATE, PFR_BASIC.CONTRACT.OTHERINFO, PFR_BASIC.CONTRACT.CONT_N_ID, PFR_BASIC.CONTRACT.DT_ID, PFR_BASIC.CONTRACT.OPERATESUMM, PFR_BASIC.CONTRACT.STATUS, PFR_BASIC.CONTRACT.LOTUS_ID, PFR_BASIC.CONTRACT.LOTUS_TABLE, PFR_BASIC.CONTRACT.TYPEID, PFR_BASIC.CONTRACT.LOTUS_GUID, PFR_BASIC.CONTRACT.PORTFOLIO_CODE, PFR_BASIC.YEARS.ID as i17, PFR_BASIC.YEARS.NAME as n4, PFR_BASIC.LEGALENTITY.ID as i18, PFR_BASIC.LEGALENTITY.FULLNAME as fn8, PFR_BASIC.LEGALENTITY.INN, PFR_BASIC.LEGALENTITY.HEADPOSITION, PFR_BASIC.LEGALENTITY.HEADFULLNAME, PFR_BASIC.LEGALENTITY.LEGALADDRESS, PFR_BASIC.LEGALENTITY.STREETADDRESS, PFR_BASIC.LEGALENTITY.POSTADDRESS, PFR_BASIC.LEGALENTITY.PHONE, PFR_BASIC.LEGALENTITY.FAX, PFR_BASIC.LEGALENTITY.COMMENT, PFR_BASIC.LEGALENTITY.EADDRESS, PFR_BASIC.LEGALENTITY.CONTRAGENTID, PFR_BASIC.LEGALENTITY.REGISTRATIONDATE, PFR_BASIC.LEGALENTITY.CLOSEDATE, PFR_BASIC.LEGALENTITY.SHORTNAME, PFR_BASIC.LEGALENTITY.REGISTRATIONNUM, PFR_BASIC.LEGALENTITY.FORMALIZEDNAME, PFR_BASIC.LEGALENTITY.SITE, PFR_BASIC.LEGALENTITY.SUBSIDIARIES, PFR_BASIC.LEGALENTITY.REGISTRATOR, PFR_BASIC.LEGALENTITY.PDREASON, PFR_BASIC.LEGALENTITY.OKPP, PFR_BASIC.LEGALENTITY.DECLNAME, PFR_BASIC.LEGALENTITY.LETTERWHO, PFR_BASIC.LEGALENTITY.TRANSFDOCKIND, PFR_BASIC.LEGALENTITY.LS_ID, PFR_BASIC.LEGALENTITY.PFBA_ID, PFR_BASIC.LEGALENTITY.INFO, PFR_BASIC.LEGALENTITY.PFRAGR, PFR_BASIC.LEGALENTITY.PFRAGRSTAT, PFR_BASIC.LEGALENTITY.MONEY, PFR_BASIC.LEGALENTITY.MONEYDATE, PFR_BASIC.LEGALENTITY.FITCH, PFR_BASIC.LEGALENTITY.FITCHDATE, PFR_BASIC.LEGALENTITY.STANDARD, PFR_BASIC.LEGALENTITY.STANDARDDATE, PFR_BASIC.LEGALENTITY.MOODY, PFR_BASIC.LEGALENTITY.MOODYDATE, PFR_BASIC.LEGALENTITY.CORRACC, PFR_BASIC.LEGALENTITY.BIK, PFR_BASIC.LEGALENTITY.FORLETTER, PFR_BASIC.LEGALENTITY.SEX, PFR_BASIC.LEGALENTITY.ENDDATE, PFR_BASIC.LEGALENTITY.STOCKCODE, PFR_BASIC.LEGALENTITY.PFRAGRDATE, PFR_BASIC.LEGALENTITY.LOTUS_GUID, PFR_BASIC.LEGALENTITY.LOTUS_ID, PFR_BASIC.LEGALENTITY.LOTUS_TABLE, PFR_BASIC.LEGALENTITY.CORRACCOUNTNOTE, PFR_BASIC.LEGALENTITY.GARANT_EL_ID, PFR_BASIC.LEGALENTITY.FUND_NUM, PFR_BASIC.LEGALENTITY.FUND_DATE, PFR_BASIC.LEGALENTITY.NOTIF_NUM, PFR_BASIC.LEGALENTITY.NOTIF_DATE, PFR_BASIC.LEGALENTITY.NUMBER_FOR_DOCS";
            return SessionWrapper(session =>
            {
                var res = session.CreateSQLQuery($@"SELECT {selectQuery} FROM PFR_BASIC.EDO_ODKF070 
                                LEFT JOIN PFR_BASIC.CONTRACT ON PFR_BASIC.EDO_ODKF070.ID_CONTRACT = PFR_BASIC.CONTRACT.ID 
                                LEFT JOIN PFR_BASIC.YEARS ON PFR_BASIC.EDO_ODKF070.ID_YEAR = PFR_BASIC.YEARS.ID 
                                LEFT JOIN PFR_BASIC.LEGALENTITY ON PFR_BASIC.CONTRACT.LE_ID = PFR_BASIC.LEGALENTITY.ID 
                                WHERE PFR_BASIC.EDO_ODKF070.ID = :id")
                    .SetParameter("id", id).SetMaxResults(1).UniqueResult<object[]>();
                return GenerateDBDic(res, selectQuery, IsDB2 ? 1 : 0);
            });
        }

        public long GetRegistersListCountPeriod(bool isArchive, DateTime? start, DateTime? end)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var select = "SELECT COUNT(*) ";
                var where = GetRegistersQueryWhere(isArchive, 0, start, end);
                var query = GetRegistersQuery(select, where, isArchive, true);

                var total = session.CreateSQLQuery(query).UniqueResult();

                return Convert.ToInt64(total);
            }
        }

        public long GetNPFTempAllocationCountPeriod(DateTime? start, DateTime? end)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var select = "SELECT COUNT(*) ";
                var where = GetNPFTempAllocationWhere(start, end);
                var query = GetRegistersAllocationQuery(select, where, true);
                var total = session.CreateSQLQuery(query).UniqueResult();

                return Convert.ToInt64(total);
            }
        }

        public List<RegistersListItem> GetRegistersListPeriodByPageForRegister(bool isArchive, int startIndex, DateTime? start, DateTime? end, long? registerID = null)
        {
            return GetRegListPeriodByPage(isArchive, startIndex, start, end, null, registerID);
        }

        public List<RegistersListItem> GetRegistersListPeriodByPage(bool isArchive, int startIndex, DateTime? start, DateTime? end, long? finregisterID = null)
        {
            return GetRegListPeriodByPage(isArchive, startIndex, start, end, finregisterID);
        }

        private List<RegistersListItem> GetRegListPeriodByPage(bool isArchive, int startIndex, DateTime? start, DateTime? end, long? finregisterID = null, long? registerId = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var select =
                    $"SELECT * FROM ({GetRegistersQuerySelect()}, {(IsDB2 ? "ROWNUMBER()" : "row_number()")} OVER (ORDER BY PFR_BASIC.REGISTER.ID, PFR_BASIC.FINREGISTER.ID DESC) AS ROWNUM";
                var where = GetRegistersQueryWhere(isArchive, 0, start, end);

                //Если нужно добыть только записи конкретного финреестра
                if (finregisterID.HasValue)
                {
                    where += $" ) as msel WHERE msel.ID = {finregisterID}";
                }
                else if (registerId.HasValue)
                {
                    where += $" ) as msel WHERE msel.REGID = {registerId}";
                }
                else
                {
                    where += $" ) as msel WHERE ROWNUM BETWEEN {startIndex + 1} AND {startIndex + PageSize}";
                }
                var query = GetRegistersQuery(select, where, isArchive, false);

                // такой лимит портит итоговую выборку
                var data = session.CreateSQLQuery(query).List();

                return (from object[] item in data select MapRegisterListItemFromObject(item)).ToList();
            }
        }

        public List<RegistersListItem> GetNPFTempAllocationListPeriodByPage(int startIndex, DateTime? start, DateTime? end)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var select = "SELECT * FROM (" + GetAllocationRegistersQuerySelect();
                select += $", {(IsDB2 ? "ROWNUMBER()" : "row_number()")} OVER (ORDER BY PFR_BASIC.REGISTER.ID, PFR_BASIC.FINREGISTER.ID DESC) AS ROWNUM";
                var where = GetNPFTempAllocationWhere(start, end);

                where += $" ) as msel WHERE ROWNUM BETWEEN {startIndex + 1} AND {startIndex + PageSize}";

                var query = GetRegistersAllocationQuery(select, where, false);

                // такой лимит портит итоговую выборку
                var data = session.CreateSQLQuery(query).List();

                var result = new List<RegistersListItem>();
                foreach (object[] item in data)
                {
                    var r = MapRegisterListItemFromObject(item);
                    result.Add(r);
                }
                return result;
            }
        }

        public List<TransferFromOrToNPFListItem> GetRegistersListPPNotEntered(bool pfRtoNPF, long? finregisterID = null)
        {
            string query = @"
                SELECT p.YEAR, r.REGDATE, r.COMMENT, r.CONTENT, c.NAME, fr.ID, fr.COUNT, r.ID, ad.NAME AS AD_NAME, r.REGNUM, i4.drSUM 
                FROM {0}.{1} r  
                LEFT JOIN {0}.{4} p ON p.ID = r.PF_ID
                INNER JOIN (
	                SELECT r.ID registerID, i2.AllFinregCOUNT, i2.FinregCOUNT 
	                FROM {0}.{1} r
	                LEFT JOIN (
                           SELECT fr.REG_ID rID, COUNT(fr.ID) AllFinregCOUNT, i1.FinregCOUNT 
                           FROM {0}.{2} fr
                           LEFT JOIN (
                       	                SELECT fr.REG_ID regID, COUNT(fr.ID) FinregCOUNT 
                                        FROM {0}.{2} fr
                                        WHERE TRIM(LOWER(fr.STATUS)) = '{7}' 
                                        GROUP BY fr.REG_ID 
                                     ) i1 ON fr.REG_ID = i1.regID 
                           GROUP BY fr.REG_ID, i1.FinregCOUNT 
	                ) i2 ON i2.rID = r.ID 
	                WHERE i2.AllFinregCOUNT <> i2.FinregCOUNT OR i2.FinregCOUNT is NULL
                ) i3 ON i3.registerID = r.ID 
                LEFT JOIN {0}.{2} fr ON fr.REG_ID = r.ID AND fr.STATUS_ID <> -1
                LEFT JOIN {0}.{6} a ON fr.CR_ACC_ID = a.ID OR fr.DBT_ACC_ID = a.ID 
                LEFT JOIN {0}.{3} c ON a.CONTRAGENTID = c.ID 
                LEFT JOIN (
	                SELECT SUM(ag.DRAFT_AMOUNT) drSUM, ag.FR_ID fregID 
	                FROM {0}.{5} ag
					WHERE STATUS_ID <> -1
	                GROUP BY ag.FR_ID 
                ) i4 ON fr.ID = i4.fregID 
                LEFT JOIN {0}.{9} ad ON r.APPD_ID = ad.ID
                WHERE (c.TYPE IS NULL OR LOWER(c.TYPE) = 'нпф') AND r.STATUS_ID <> -1
					AND 
	                (
	                {8}
	                ) 
                ORDER BY p.YEAR";

            string whereCondition = pfRtoNPF
                ? $"(TRIM(LOWER(r.KIND))= '{RegisterIdentifier.PFRtoNPF.ToLower()}' AND (r.TRANCHEDATE IS NOT NULL AND (i4.drSUM is NULL or i4.drSUM <> fr.COUNT)) AND TRIM(LOWER(fr.STATUS)) <> '{RegisterIdentifier.FinregisterStatuses.NotTransferred.ToLower()}')"
                : $"(TRIM(LOWER(r.KIND)) = '{RegisterIdentifier.NPFtoPFR.ToLower()}' AND (TRIM(LOWER(fr.STATUS)) = '{RegisterIdentifier.FinregisterStatuses.Issued.ToLower()}' AND (i4.drSUM IS NULL OR i4.drSUM <> fr.COUNT)))";

            if (finregisterID.HasValue)
            {
                whereCondition += $" AND fr.ID = {finregisterID.Value}";
            }

            query = string.Format(query,
                DATA_SCHEME,         // 0
                TABLE_REGISTER,      // 1
                TABLE_FINREGISTER,   // 2
                TABLE_CONTRAGENT,    // 3
                TABLE_PORTFOLIO,     // 4
                TABLE_ASG_FIN_TR,    // 5
                TABLE_ACCOUNT,       // 6
                RegisterIdentifier.FinregisterStatuses.Transferred.ToLower(), // 7
                whereCondition,                  // 8
                TABLE_APPROVEDOC     //9
            );

            var list = new List<TransferFromOrToNPFListItem>();

            using (var cmd = new DBCommandLogging(query))
            {
                cmd.Prepare();
                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    var item = new TransferFromOrToNPFListItem
                    {
                        Portfolio = ReadStringValue(reader, 0),
                        PaymentOrderDate = ReadDateTimeValue(reader, 1),
                        Comment = ReadStringValue(reader, 2),
                        Content = ReadStringValue(reader, 3),
                        NPFShortName = ReadStringValue(reader, 4),
                        FinregisterID = ReadInt64Value(reader, 5),
                        SPNCount = ReadDecimalValue(reader, 6, 4),
                        RegisterID = ReadInt64Value(reader, 7),
                        ApproveDocName = ReadStringValue(reader, 8),
                        RegNum = ReadStringValue(reader, 9)
                    };
                    //todo PaymentOrderDate ранее присваивается MinValue вместо Null, проверка бессмысленна, может ошибка?
                    item.Year = item.PaymentOrderDate.HasValue ? item.PaymentOrderDate.Value.Year : (int?) null;
                    item.HalfYear = DateTools.GetHalfYearInWordsForDate(item.PaymentOrderDate);
                    item.QuarterYear = DateTools.GetQarterYearInWordsForDate(item.PaymentOrderDate);
                    item.Month = DateTools.GetMonthInWordsForDate(item.PaymentOrderDate);
                    item.CommonPPSum = ReadDecimalNullableValue(reader, 10, 4) ?? 0;
                    list.Add(item);
                }

                return list.Where(a => a.FinregisterID != 0).ToList();
            }
        }

        // выбор портфелей из временного размещения, для которых есть неполностью изъятые суммы
        public List<Portfolio> GetSPNAllocatedPortfolios()
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"
												SELECT p FROM Portfolio p
												WHERE p.ID in
												(
													SELECT coalesce(mp.ID,rp.ID)
													FROM Finregister fr
													JOIN fr.Register mr
													LEFT JOIN mr.Return rr											
													LEFT JOIN mr.Portfolio mp
													LEFT JOIN rr.Portfolio rp
													WHERE (mr.Kind = :allKind or rr.Kind = :retKind) and fr.StatusID <> -1
													group by coalesce(mp.ID,rp.ID)
													having Sum(case when mr.Kind = :allKind then fr.Count else -fr.Count end) > 0
														or Sum(case when mr.Kind = :allKind then fr.ZLCount else -fr.ZLCount end) > 0
												)");
                query.SetParameter("allKind", RegisterIdentifier.TempAllocation);
                query.SetParameter("retKind", RegisterIdentifier.TempReturn);
                var res = query.List().Cast<Portfolio>().ToList();
                return res;
            }
        }

        public List<FinregisterListItem> GetSPNAllocatedFinregisters(long portfolioID, string content = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"SELECT new FinregisterListItem(fr, ra, acc.Name, adc.Name, ccs.Name, dcs.Name)  FROM 
												Finregister fr		
												JOIN fr.Register ra
                                                join ra.Portfolio pf
												join fr.CreditAccount ac
												join fr.DebitAccount ad
												join ac.Contragent acc
												join acc.Status ccs
												join ad.Contragent adc
												join adc.Status dcs
												WHERE fr.StatusID <> -1 and (ra.Kind = :kind and ra.Portfolio = :portfolio and (:content is null OR ra.Content = :content OR ra.Content = :oldcontent))");
                query.SetParameter("kind", RegisterIdentifier.TempAllocation);
                query.SetParameter("portfolio", portfolioID);
                query.SetParameter("content", content);
                query.SetParameter("oldcontent", RegisterIdentifier.TempContent);
                var res = query.List().Cast<FinregisterListItem>().ToList();
                return res;
            }
        }

        public List<FinregisterListItem> GetSPNReturnedFinregisters(long portfolioID, string content = null)
        {
            using (var session = DataAccessSystem.OpenSession())
            {
                var query = session.CreateQuery(@"SELECT new FinregisterListItem(fr, rr, acc.Name, adc.Name, ccs.Name, dcs.Name)  FROM 
												Finregister fr												
												JOIN fr.Register mr
												JOIN mr.Return rr
												join fr.CreditAccount ac
												join fr.DebitAccount ad
												join ac.Contragent acc
												join acc.Status ccs
												join ad.Contragent adc
												join adc.Status dcs
												WHERE fr.StatusID <> -1 and (rr.Kind = :kind and rr.Portfolio = :portfolio and (:content is null OR rr.Content = :content OR rr.Content = :oldcontent))");
                query.SetParameter("kind", RegisterIdentifier.TempReturn);
                query.SetParameter("portfolio", portfolioID);
                query.SetParameter("content", content);
                query.SetParameter("oldcontent", RegisterIdentifier.TempContent);
                var res = query.List().Cast<FinregisterListItem>().ToList();
                return res;
            }
        }



#region Get Lists methods
		public List<PFRAccountsListItem> GetPFRAccountsList()
		{
		    return SessionWrapper(session =>
		    {
		        return session.CreateSQLQuery(@"SELECT PFR_BASIC.PFRBANKACCOUNT.ID, PFR_BASIC.PFRBANKACCOUNT.STATUS, PFR_BASIC.PFRBANKACCOUNT.ACCOUNTNUM, 
                                                       PFR_BASIC.ACC_BANK_TYPE.NAME,
                                                       PFR_BASIC.LEGALENTITY.FORMALIZEDNAME, 
                                                       PFR_BASIC.CURRENCY.NAME
                                                FROM PFR_BASIC.PFRBANKACCOUNT 
                                                LEFT JOIN PFR_BASIC.ACC_BANK_TYPE ON PFR_BASIC.PFRBANKACCOUNT.TYPE_ID = PFR_BASIC.ACC_BANK_TYPE.ID 
                                                LEFT JOIN PFR_BASIC.LEGALENTITY ON PFR_BASIC.PFRBANKACCOUNT.LE_BANK_ID = PFR_BASIC.LEGALENTITY.ID 
                                                LEFT JOIN PFR_BASIC.CURRENCY ON PFR_BASIC.PFRBANKACCOUNT.CURR_ID = PFR_BASIC.CURRENCY.ID 
                                                LEFT JOIN PFR_BASIC.CONTRAGENT ON PFR_BASIC.LEGALENTITY.CONTRAGENTID=PFR_BASIC.CONTRAGENT.ID 
                                                WHERE PFR_BASIC.CONTRAGENT.STATUS_ID <> -1 AND PFR_BASIC.PFRBANKACCOUNT.STATUS_ID <> -1  ")
		            .List<object[]>().Select(a => new PFRAccountsListItem(a)).ToList();
		    });
			
		}



		protected string GetTransfersQuerySrting(Document.Types contractType, bool isArchive, bool isForAssignPayment, bool isPPIncluded, bool onlyActiveContract = false, long yearId = 0, DateTime? periodStart = null, DateTime? periodEnd = null, bool addWhere = true, bool removeSiplan = true)
		{

			string query = string.Format(
				 @"SELECT 
                 {0}.{1}.ID, {0}.{1}.KIND, {0}.{1}.REGNUM, {0}.{1}.DATE, {0}.{2}.ID, 
                 {0}.{3}.ZL_MOVECOUNT, {0}.{3}.ID, {0}.{3}.STATUS, {0}.{3}.INVESTDOHOD, {0}.{3}.ACT_NUM, 
                 {0}.{3}.ACT_DATE, {0}.{3}.STARTSUM, {0}.{3}.SUM, {0}.{3}.PAYNUM, {0}.{3}.PAYDATE, 
                 {0}.{3}.CONTRL_ADD_SPN_DATE, {0}.{3}.CONTRL_SPN_DATE,
                 {0}.{4}.OPERATION,
                 {0}.{5}.NAME,
                 {0}.{6}.FORMALIZEDNAME,
                 {0}.{7}.REGNUM,
                 {0}.{8}.NAME" + //год кампании
				 ", {0}.{3}.PAYDATE " + //дата перечисления --DATE                 
				 (isForAssignPayment ? ", {0}.{9}.NAME, {0}.{10}.MONTH_ID, {0}.{10}.PLANSUM, {0}.{10}.FACTSUM" : "") +
				 (isPPIncluded ? ", pp.DRAFT_REGNUM, pp.DRAFT_DATE" : "") +
				 " , {0}.{7}.TYPEID, {0}.{1}.DIR_SPN_ID, {0}.{1}.OPERATION_ID, {0}.{1}.COMP_M_ID, {0}.{1}.COMP_Y_ID, {0}.{7}.STATUS as CONTRACT_STATUS " +
				 ", {0}.{1}.CUSTOM_SUM " +
				 ", {0}.{2}.CONTR_ID " +
				 (isForAssignPayment ? ", {0}.{1}.OPERATION_TYPE_ID, {0}.ELEMENT.NAME, TROP.OPERATION, {0}.{10}.ID, rt_plan2.INVESTDOHOD, rt_plan2.STATUS" : "") +
				 " FROM {0}.{1}",

			DATA_SCHEME, //0
			TABLE_SI_REGISTER, //1
			TABLE_SI_TRANSFER, //2
			TABLE_REQ_TRANSFER, //3
			TABLE_OPERATION, //4
			TABLE_DIRECTION_SPN, //5
			TABLE_LEGALENTITY, //6
			TABLE_CONTRACT, //7
			TABLE_YEARS,//8
			TABLE_MONTHS,//9
			TABLE_SI_UKPLAN);//10

			query += string.Format("\r\n  LEFT JOIN {0}.{2} ON {0}.{1}.ID = {0}.{2}.SI_REG_ID AND {0}.{2}.STATUS_ID <> -1 ", DATA_SCHEME,
										TABLE_SI_REGISTER, TABLE_SI_TRANSFER);



			if (isForAssignPayment)
			{
				query += string.Format("\r\n     LEFT JOIN {0}.{2} ON {0}.{1}.ID = {0}.{2}.SI_TR_ID AND {0}.{2}.STATUS_ID <> -1 ", DATA_SCHEME,
													  TABLE_SI_TRANSFER, TABLE_SI_UKPLAN);

				query += string.Format("\r\n   LEFT JOIN {0}.{2} ON {0}.{1}.REQ_TR_ID = {0}.{2}.ID AND {0}.{2}.STATUS_ID <> -1 ", DATA_SCHEME,
													  TABLE_SI_UKPLAN, TABLE_REQ_TRANSFER);

				//второе перечисление для плана rt_plan2
				query += string.Format("\r\n  LEFT JOIN {0}.{2} rt_plan2 ON {0}.{1}.REQ_TR_ID2 = rt_plan2.ID AND rt_plan2.STATUS_ID <> -1 ", DATA_SCHEME,
													  TABLE_SI_UKPLAN, TABLE_REQ_TRANSFER);


				query += string.Format("\r\n  LEFT JOIN {0}.{2} ON {0}.{1}.MONTH_ID = {0}.{2}.ID", DATA_SCHEME,
                                                                     TABLE_SI_UKPLAN, TABLE_MONTHS);

				query += string.Format("\r\n  LEFT JOIN {0}.ELEMENT ON {0}.{1}.OPERATION_TYPE_ID = {0}.ELEMENT.ID", DATA_SCHEME,
                                                                     TABLE_SI_REGISTER);
                //гугль таск 94 для отображения содержания операции перечислений в Отзыве средств для нового типа операции (с отсутств. содержанием операции)
				query += string.Format(@" 
										LEFT JOIN {0}.SI_TRANSFER TR2 ON TR2.ID = {0}.REQ_TRANSFER.SI_TR_ID AND TR2.STATUS_ID <> -1
			                            LEFT JOIN {0}.SI_REGISTER REGWITHOP ON TR2.SI_REG_ID = REGWITHOP.ID 
			                            LEFT JOIN {0}.OPERATION TROP ON REGWITHOP.OPERATION_ID = TROP.ID ", DATA_SCHEME);
			}
            else
            {
                //Вычисление наличия планов УК для реестра. Если они есть - не отображаем реестр в Перечислениях, 
                //т.к. мастер каждый раз создает новый реестр при работе
                //http://jira.vs.it.ru/browse/DOKIPIV-492
                query += @" LEFT JOIN (

                                               SELECT ukp.SI_TR_ID, count(1) as ukpCount
                                               FROM PFR_BASIC.SI_UKPLAN ukp
                                               GROUP BY ukp.SI_TR_ID
                                              ) allCount ON allCount.SI_TR_ID = PFR_BASIC.SI_TRANSFER.ID";
				//                                               WHERE ukp.REQ_TR_ID IS NULL <-> потребуется, если будет слияние реестров при работе мастера


			}

			query += string.Format(" LEFT JOIN {0}.{2} ON {0}.{1}.COMP_Y_ID = {0}.{2}.ID", DATA_SCHEME,
										TABLE_SI_REGISTER, TABLE_YEARS);

			query += string.Format(" LEFT JOIN {0}.{2} ON {0}.{1}.CONTR_ID = {0}.{2}.ID", DATA_SCHEME,
										TABLE_SI_TRANSFER, TABLE_CONTRACT);

			query += string.Format(" LEFT JOIN {0}.{2} ON {0}.{1}.LE_ID = {0}.{2}.ID", DATA_SCHEME,
										TABLE_CONTRACT, TABLE_LEGALENTITY);

			query += string.Format(" LEFT JOIN {0}.{2} ON {0}.{1}.OPERATION_ID = {0}.{2}.ID", DATA_SCHEME,
										TABLE_SI_REGISTER, TABLE_OPERATION);

			query += string.Format(" LEFT JOIN {0}.{2} ON {0}.{1}.DIR_SPN_ID = {0}.{2}.ID", DATA_SCHEME,
										TABLE_SI_REGISTER, TABLE_DIRECTION_SPN);
			if (!isForAssignPayment)
				query += string.Format(" LEFT OUTER JOIN {0}.{2} ON {0}.{1}.ID = {0}.{2}.SI_TR_ID AND {0}.{2}.STATUS_ID <> -1 ", DATA_SCHEME,
							TABLE_SI_TRANSFER, TABLE_REQ_TRANSFER);

			// выбор последнего по идентификатору платежного поручения для вывода даты и номера ПП в гриды
			if (isPPIncluded)
			{
                if(IsDB2)
				    query += string.Format(@" LEFT JOIN (SELECT t.REQ_TR_ID, t.DRAFT_REGNUM, t.DRAFT_DATE , RANK() OVER(PARTITION BY t.REQ_TR_ID ORDER BY t.DRAFT_PAYDATE DESC) AS rn , RANK() OVER(PARTITION BY t.REQ_TR_ID,t.DRAFT_PAYDATE ORDER BY t.ID DESC) AS rnID FROM {0}.{2} t WHERE t.STATUS_ID <> -1 ) pp 
													ON pp.REQ_TR_ID = {0}.{1}.ID AND pp.rn = 1  AND pp.rnID = 1 "
								, DATA_SCHEME, TABLE_REQ_TRANSFER, TABLE_ASG_FIN_TR);
                //ДЛЯ PG
                else query += string.Format(@" LEFT JOIN (SELECT DISTINCT ON (t.REQ_TR_ID)
                                                      t.REQ_TR_ID,
                                                      t.DRAFT_REGNUM,
                                                      t.DRAFT_DATE
                                                FROM {0}.{2} t
                                                WHERE t.STATUS_ID <> -1 
                                                ORDER BY
                                                      t.REQ_TR_ID,
                                                      t.DRAFT_PAYDATE DESC,
                                                      t.ID DESC) pp ON pp.REQ_TR_ID = {0}.{1}.ID "
                                , DATA_SCHEME, TABLE_REQ_TRANSFER, TABLE_ASG_FIN_TR);
            }

			long targetYearLimit = DateTime.Now.Year - 5;

			if (!addWhere) return query;

			query += " WHERE ";

			if (isArchive)
			{
				query += !isForAssignPayment ?
					//Архивные и не правопреемники - перечисления со статусом "акт подписан"
					//= 'акт подписан' так же исключает NULL при LEFT JOIN
				    $"TRIM(LOWER({DATA_SCHEME}.{TABLE_REQ_TRANSFER}.STATUS)) = 'акт подписан' {(removeSiplan ? " AND coalesce(allCount.ukpCount,0) = 0 " : "")} "
				    :
					//Архивные правопреемники - все планы, у которых год больше, чем (текущий-5)
				    $"cast({DATA_SCHEME}.{TABLE_YEARS}.NAME as int) < {targetYearLimit} ";
			}
			else
			{
				query += !isForAssignPayment ?
					//Не архивные и не правопреемники - перечисления со статусом не "акт подписан"
					//<> 'акт подписан' так же исключает NULL при LEFT JOIN
					string.Format("(TRIM(LOWER({0}.{1}.STATUS)) <> 'акт подписан' OR {0}.{1}.STATUS IS NULL) {2} ", // < 12
                    DATA_SCHEME, TABLE_REQ_TRANSFER, removeSiplan ? " AND coalesce(allCount.ukpCount,0) = 0 " : "") :
					//Не архивные правопреемники - все планы, у которых год не меньше, чем (текущий-5)
					string.Format("cast({0}.{1}.NAME as int) >= {2} AND {0}.{3}.STATUS >= 0 ",
					DATA_SCHEME, TABLE_YEARS, targetYearLimit, TABLE_CONTRACT);
			}

			if (yearId > 0)
			{
				query += $" AND {DATA_SCHEME}.{TABLE_SI_REGISTER}.COMP_Y_ID = {yearId} ";
			}


			if (onlyActiveContract)
			{
				query += $" AND {DATA_SCHEME}.{TABLE_CONTRACT}.STATUS >= 0 ";
			}

			query += $" AND {DATA_SCHEME}.{TABLE_CONTRACT}.TYPEID={(int) contractType}  ";

			if (periodStart.HasValue)
			{
				query += $" AND {DATA_SCHEME}.{TABLE_SI_REGISTER}.DATE>='{periodStart.Value.ToShortDateString()}' ";
			}

			if (periodEnd.HasValue)
			{
				query += $" AND {DATA_SCHEME}.{TABLE_SI_REGISTER}.DATE<='{periodEnd.Value.ToShortDateString()}' ";
			}

			return query;
		}

        protected List<TransferListItem> GetTransfersListBySqlQuery(string query, Document.Types contractType, bool forAssignPayment, bool isPPIncluded, bool mergeReqTr = false)
		{
			List<TransferListItem> result = new List<TransferListItem>();

			using (var cmd = new DBCommandLogging(query))
			{
				var reader = cmd.ExecuteReader();
				int directionId = reader.GetOrdinal("DIR_SPN_ID");
				int operationId = reader.GetOrdinal("OPERATION_ID");
				int companyMonthId = reader.GetOrdinal("COMP_M_ID");
				int companyYearId = reader.GetOrdinal("COMP_Y_ID");
				int contractStatusId = reader.GetOrdinal("CONTRACT_STATUS");
				int customSumId = reader.GetOrdinal("CUSTOM_SUM");
				int contractId = reader.GetOrdinal("CONTR_ID");

				while (reader.Read())
				{
					var dir = ReadInt64Value(reader, directionId);
					if (contractType == Document.Types.SI)
					{
						if (dir == 3 || dir == 4) continue;
					}
					else if (contractType == Document.Types.VR)
					{
						if (dir == 1 || dir == 2) continue;
					}

					DateTime? SetSPNTransferDate = null;
					var set = ReadDateTimeValue(reader, 15);
					if (set != DateTime.MinValue)
						SetSPNTransferDate = set;

					DateTime? SPNDebitDate = null;
					var debit = ReadDateTimeValue(reader, 16);
					if (debit != DateTime.MinValue)
						SPNDebitDate = debit;

					var item = new TransferListItem
					{
						RegisterID = ReadInt64Value(reader, 0),
						Kind = ReadStringValue(reader, 1),
						RegisterNumber = ReadStringValue(reader, 2),
						RegisterDate = reader.IsDBNull(3) ? (DateTime?)null : ReadDateTimeValue(reader, 3),
						TransferID = ReadInt64Value(reader, 4),
						ZLCount = ReadInt64Value(reader, 5),
						ReqTransferID = ReadInt64Value(reader, 6),
						Status = ReadStringValue(reader, 7),
						InvestmentIncome = ReadDecimalValue(reader, 8, 4),
						ActNum = ReadStringValue(reader, 9),
						//TransferDate = SPNDebitDate,
						RegisterCompanyYearID = ReadInt64Value(reader, companyYearId),
						RegisterCompanyMonthID = ReadInt64Value(reader, companyMonthId),
						RegisterDirectionID = ReadInt64Value(reader, directionId),
						RegisterOperationID = ReadNullableInt64Value(reader, operationId),
						ContractStatusID = ReadInt64Value(reader, contractStatusId),
						SumBeforeOperation = ReadDecimalValue(reader, 11, 4),
						//PaymentOrderNumber = ReadStringValue(reader, 13),
                        //Гугь таск 95, убираем Содержание операции в гриде Отзыв средств для планов безе перечислений
						Operation = forAssignPayment ? null : ReadStringValue(reader, 17),
						Direction = ReadStringValue(reader, 18),
						UKFormalizedName = ReadStringValue(reader, 19),
						ContractNumber = ReadStringValue(reader, 20),
						Company = ReadStringValue(reader, 21),
						ContractId = ReadInt64Value(reader, contractId)
					};

					

					var dt = ReadDateTimeValue(reader, 10);
					if (dt == DateTime.MinValue)
						item.ActDate = null;
					else
					{
						item.ActDate = dt;
						item.ActDateYear = DateTools.GetYearInWordsForDate(item.ActDate);
					}

					if (isPPIncluded)
					{
						item.PaymentOrderNumber = ReadStringValue(reader, forAssignPayment ? 27 : 23);
						item.PaymentOrderDate = ReadDateTimeValue(reader, forAssignPayment ? 28 : 24);
					}

					item.PaymentOrderNumber = item.PaymentOrderNumber ?? ReadStringValue(reader, 13);
					item.PaymentOrderDate = item.PaymentOrderDate ?? ReadDateTimeValue(reader, 14);

					if (item.PaymentOrderDate == DateTime.MinValue)
						item.PaymentOrderDate = null;

					decimal sumByOperation = ReadDecimalValue(reader, 12, 4);

					decimal? customSum = ReadDecimalNullableValue(reader, customSumId, 4);

					if (item.RegisterDirectionID == 1 || item.RegisterDirectionID == 3)
					{
						item.SumFromUKtoPFR = sumByOperation;
						item.TotalFromUK = customSum;
						item.SumFromPFRToUK = 0;
						if (!SetSPNTransferDate.HasValue || !SPNDebitDate.HasValue)
							item.ExecutionControl = TransferListItem.ExecutionControlType.ExecutionControlTypeNone;
						else if (SPNDebitDate.Value.Date > SetSPNTransferDate.Value.Date)
							item.ExecutionControl = TransferListItem.ExecutionControlType.ExecutionControlTypeNotExecuted;
						else
							item.ExecutionControl = TransferListItem.ExecutionControlType.ExecutionControlTypeExecuted;
					}
					else
					{
						item.SumFromUKtoPFR = 0;
						item.SumFromPFRToUK = sumByOperation;
						item.TotalToUK = customSum;
						item.ExecutionControl = TransferListItem.ExecutionControlType.ExecutionControlTypeNone;
					}

					var td = ReadDateTimeValue(reader, 22);
					item.TransferDate = (td == DateTime.MinValue ? (DateTime?)null : td) ?? SPNDebitDate;
					if (forAssignPayment)
					{
						item.Month = ReadStringValue(reader, 23);
						item.MonthID = ReadInt64Value(reader, 24);
						item.PlanSum = ReadDecimalValue(reader, 25, 4);

                        item.FactSum = ReadDecimalValue(reader, 26, 4);
                        item.RegisterOperationTypeID = ReadNullableInt64Value(reader, 37);
                        item.OperationType = ReadStringValue(reader, 38);
                        //Гугль таск 94-95, получаем Содержание операции для планов с перечислениями
                        if(string.IsNullOrEmpty(item.Operation))
                            item.Operation = ReadStringValue(reader, 39);
                        item.UkPlanID = ReadInt64Value(reader, 40);

						//Добавляем значение инвестдохода из второго перечисления по плану
						item.InvestmentIncome += ReadDecimalValue(reader, 41, 4 );
						//Корректируем статус, если первое перечисление удалено
						
						var status2 = ReadStringValue(reader, 42);
						if (string.IsNullOrEmpty(item.Status)) item.Status = status2;						

                    }


					if (string.IsNullOrEmpty(item.Status)) item.Status = "Нет перечислений";

					result.Add(item);
				}
				reader.Close();
			}
			

			return result;
		}

		public List<TransferListItem> GetTransfersUkPlanListByRegId(Document.Types contractType, bool isArchive, bool forAssignPayment, long regID)
		{
			//выбираем запрос без условия where
			var query = GetTransfersQuerySrting(contractType, isArchive, forAssignPayment, true, false, 0, null, null, false);
			//query = query.Substring(0, query.IndexOf("WHERE"));
			//query += " WHERE {shema}.SI_TRANSFER.SI_REG_ID = {reg_id}".Replace("{shema}", DATA_SCHEME).Replace("{reg_id}", regID.ToString());
			query += string.Format(" WHERE {0}.SI_TRANSFER.SI_REG_ID = {1} AND {0}.SI_TRANSFER.STATUS_ID <> -1 ", DATA_SCHEME, regID);
			return GetTransfersListBySqlQuery(query, contractType, forAssignPayment, true);
		}

		public List<TransferListItem> GetTransfersUkPlanListByRegListId(Document.Types contractType, bool isArchive, bool forAssignPayment, string regIDs)
		{

			var query = GetTransfersQuerySrting(contractType, isArchive, forAssignPayment, true, false, 0, null, null, false);
			//query = query.Substring(0, query.IndexOf("WHERE"));
			//query += " WHERE {shema}.SI_TRANSFER.SI_REG_ID in ( {reg_id})".Replace("{shema}", DATA_SCHEME).Replace("{reg_id}", regIDs);
		    var regTxt = string.IsNullOrEmpty(regIDs) ? null : $"{DATA_SCHEME}.SI_TRANSFER.SI_REG_ID in ({regIDs}) AND";
			query += string.Format(" WHERE {1} {0}.SI_TRANSFER.STATUS_ID <> -1 ", DATA_SCHEME, regTxt);
			return GetTransfersListBySqlQuery(query, contractType, forAssignPayment, true);
		}

	    public List<TransferListItem> GetTransfersListForSPNMoveList(Document.Types contractType)
	    {
            const bool isArchive = true;
            const bool forAssignPayment = false;
            const bool onlyActiveContract = false;
            const long yearId = 0;
            DateTime? periodStart = null;
            DateTime? periodEnd = null;
            const bool isPPIncluded = true;


            string query = GetTransfersQuerySrting(contractType, isArchive, forAssignPayment, isPPIncluded, onlyActiveContract, yearId, periodStart, periodEnd, true, false);
            var result = GetTransfersListBySqlQuery(query, contractType, forAssignPayment, isPPIncluded);
            return result;
	    }

		public List<TransferListItem> GetTransfersListBYContractType(Document.Types contractType, bool isArchive, bool forAssignPayment, bool onlyActiveContract, long yearId = 0, DateTime? periodStart = null, DateTime? periodEnd = null, bool mergeReqTr = false)
		{
			const bool isPPIncluded = true;

			string query = GetTransfersQuerySrting(contractType, isArchive, forAssignPayment, isPPIncluded, onlyActiveContract, yearId, periodStart, periodEnd);

			var result = GetTransfersListBySqlQuery(query, contractType, forAssignPayment, isPPIncluded, mergeReqTr);

			return result;
		}

        public List<TransferListItem> GetTransfersListBYContractTypeForUKArchive(Document.Types contractType, bool isArchive, bool forAssignPayment, bool onlyActiveContract)
	    {
	        return GetTransfersListBYContractType(contractType, isArchive, forAssignPayment, onlyActiveContract);
	    }


	   // private Dictionary<string, DBField> GetOVSIReqTransfersListForContract_cacheStructure;

		private List<ReqTransferRecalcListItem> GetOVSIReqTransfersListForContract(long contrID)
		{
		    return SessionWrapper(session =>
		    {
		        return
		            session.CreateSQLQuery(
		                @"SELECT PFR_BASIC.REQ_TRANSFER.ID, PFR_BASIC.REQ_TRANSFER.SUM, PFR_BASIC.REQ_TRANSFER.STATUS, PFR_BASIC.REQ_TRANSFER.STARTSUM, PFR_BASIC.REQ_TRANSFER.INVESTDOHOD, PFR_BASIC.DIRECTION_SPN.NAME 
                                                FROM  PFR_BASIC.REQ_TRANSFER  
                                                LEFT JOIN PFR_BASIC.SI_TRANSFER ON  PFR_BASIC.REQ_TRANSFER.SI_TR_ID = PFR_BASIC.SI_TRANSFER.ID AND PFR_BASIC.SI_TRANSFER.STATUS_ID <> -1  
                                                LEFT JOIN PFR_BASIC.SI_REGISTER ON PFR_BASIC.SI_TRANSFER.SI_REG_ID = PFR_BASIC.SI_REGISTER.ID  
                                                LEFT JOIN PFR_BASIC.CONTRACT ON  PFR_BASIC.SI_TRANSFER.CONTR_ID = PFR_BASIC.CONTRACT.ID  
                                                LEFT JOIN PFR_BASIC.OPERATION ON PFR_BASIC.SI_REGISTER.OPERATION_ID = PFR_BASIC.OPERATION.ID  
                                                LEFT JOIN PFR_BASIC.DIRECTION_SPN ON PFR_BASIC.SI_REGISTER.DIR_SPN_ID = PFR_BASIC.DIRECTION_SPN.ID  
                                                WHERE PFR_BASIC.SI_TRANSFER.CONTR_ID = :id AND TRIM(LOWER(PFR_BASIC.REQ_TRANSFER.STATUS)) = 'акт подписан' AND PFR_BASIC.REQ_TRANSFER.STATUS_ID <> -1 
                                                ORDER BY PFR_BASIC.REQ_TRANSFER.ACT_DATE, PFR_BASIC.REQ_TRANSFER.ID")
		                .SetParameter("id", contrID)
		                .List<object[]>()
		                .Select(a => new ReqTransferRecalcListItem(a))
		                .ToList();
		    });
		   
		}

		private List<SITransferRecalcListItem> GetOVSITransfersListForContract(long contrID)
		{
            
		    return SessionWrapper(session =>
		    {
		        return session.CreateSQLQuery(@"SELECT PFR_BASIC.SI_TRANSFER.ID, PFR_BASIC.SI_TRANSFER.ZLCOUNT, PFR_BASIC.SI_TRANSFER.START_ZLCOUNT, PFR_BASIC.DIRECTION_SPN.NAME 
                                                FROM  PFR_BASIC.SI_TRANSFER 
                                                LEFT JOIN PFR_BASIC.SI_REGISTER ON PFR_BASIC.SI_TRANSFER.SI_REG_ID = PFR_BASIC.SI_REGISTER.ID 
                                                LEFT JOIN PFR_BASIC.CONTRACT ON  PFR_BASIC.SI_TRANSFER.CONTR_ID = PFR_BASIC.CONTRACT.ID 
                                                LEFT JOIN PFR_BASIC.DIRECTION_SPN ON PFR_BASIC.SI_REGISTER.DIR_SPN_ID = PFR_BASIC.DIRECTION_SPN.ID 
                                                INNER JOIN PFR_BASIC.REQ_TRANSFER ON PFR_BASIC.SI_TRANSFER.ID = PFR_BASIC.REQ_TRANSFER.SI_TR_ID AND PFR_BASIC.REQ_TRANSFER.STATUS_ID <> -1 
                                                WHERE PFR_BASIC.SI_TRANSFER.CONTR_ID = :id AND PFR_BASIC.REQ_TRANSFER.ACT_DATE IS NOT NULL AND PFR_BASIC.SI_TRANSFER.STATUS_ID <> -1 
                                                GROUP BY PFR_BASIC.SI_TRANSFER.ID, PFR_BASIC.SI_TRANSFER.ZLCOUNT, PFR_BASIC.SI_TRANSFER.START_ZLCOUNT, PFR_BASIC.DIRECTION_SPN.NAME 
                                                ORDER BY MAX(PFR_BASIC.REQ_TRANSFER.ACT_DATE)")
		            .SetParameter("id", contrID)
		            .List<object[]>()
		            .Select(a => new SITransferRecalcListItem(a))
		            .ToList();
		    });
		   
		}

		public void RecalcUKPortfoliosForContract(long contractID)
		{
			//пересчет перечислений
			var reqTransfers = GetOVSIReqTransfersListForContract(contractID);


			var signedReqTrs = reqTransfers.Where(x => x.Status.Trim().ToLower() == TransferStatusIdentifier.sActSigned.ToLower()).ToList();
			decimal startSum = 0;
			for (int i = 0; i < signedReqTrs.Count; i++)
			{
				// обновление начального значения суммы при необходимости
				var reqTr = signedReqTrs[i];
				if (reqTr.StartSum != startSum)
				{
					// update RT in memory
					reqTr.StartSum = startSum;
					// update reqTr in DB
                    UpdateReqTransferRecalc(reqTr.ID, startSum);
				}

				bool fromPFRtoUK = reqTr.DirSpnName.ToLower().Contains("из пфр");
				decimal invest = fromPFRtoUK ? 0 : (reqTr.InvestDohod ?? 0);
				if (!fromPFRtoUK) reqTr.Sum *= -1M;
				startSum = reqTr.StartSum??0 + reqTr.Sum??0 + invest;
			}

		    var contract = GetByID<PFR_INVEST.DataObjects.Contract>(contractID);
			if (contract.OperateSum == null || contract.OperateSum.Value != startSum) // здесь startSum - сумма, рассчитанная по последнему подписанному контракту
			{
				contract.OperateSum = startSum;
				SaveEntity(contract);
			}
			SITransferRecalcListItem prevSItransfer = null;
			long lastStartZL = 0;

			//пересчет ЗЛ
			var siTransfers = GetOVSITransfersListForContract(contractID);
			foreach (var siTransfer in siTransfers)
			{
				//loop on siTransfers
				if (siTransfer != siTransfers.First())
				{
					bool fromPFRtoUK = prevSItransfer.SPNDirectionName.ToLower().Contains("из пфр");
                    long fromOrToPFR = prevSItransfer.ZLCount ?? 0;
					if (!fromPFRtoUK)
						fromOrToPFR *= -1;
					lastStartZL = (prevSItransfer.StartZLCount ?? 0) + fromOrToPFR;
				}
				prevSItransfer = siTransfer;

				if (siTransfer.StartZLCount != lastStartZL)
				{
					//update reqtransfer
                    UpdateSITransferRecalc(siTransfer.ID, lastStartZL);
				}
			}
		}

	    private void UpdateSITransferRecalc(long id, long? zlCount)
	    {
	        TransactionWrapper(session => session.CreateQuery(@"update SITransfer tr SET tr.InsuredPersonsCount=:count where tr.ID=:id")
                .SetParameter("count", zlCount).SetParameter("id", id).ExecuteUpdate());
	    }

        private void UpdateReqTransferRecalc(long id, decimal? sum)
        {
            TransactionWrapper(session => session.CreateQuery(@"update ReqTransfer tr SET tr.SumBeforeOperation=:count where tr.ID=:id")
                .SetParameter("count", sum).SetParameter("id", id).ExecuteUpdate());
        }




        private string GetRegistersQuerySubquery(bool isArchive)
		{
			string subquery = string.Format(@"SELECT
					r.ID registerID, 
					jfr2.AllFinregCOUNT, 
					jfr2.FinregCOUNT 
					FROM {0}.{1} r
					LEFT JOIN (
						SELECT fr.REG_ID as rID, COUNT(fr.ID) as AllFinregCOUNT, jfr.FinregCOUNT 
						FROM {0}.{2} fr
						LEFT JOIN (
						    SELECT fr2.REG_ID as regID, COUNT(fr2.ID) as FinregCOUNT 
						    FROM {0}.{2} fr2
						    WHERE TRIM(LOWER(fr2.STATUS)) = '{3}' AND fr2.STATUS_ID <> -1 
						    GROUP BY fr2.REG_ID 
                        ) as jfr ON fr.REG_ID = jfr.regID 
                        WHERE fr.STATUS_ID <> -1 
						GROUP BY fr.REG_ID, jfr.FinregCOUNT 
                    ) as jfr2 ON jfr2.rID = r.ID ", DATA_SCHEME,
											  TABLE_REGISTER,
											  TABLE_FINREGISTER,
											  RegisterIdentifier.FinregisterStatuses.Transferred);

			string subqueryWhere = isArchive ? "WHERE jfr2.AllFinregCOUNT = jfr2.FinregCOUNT" : "WHERE jfr2.AllFinregCOUNT <> jfr2.FinregCOUNT OR jfr2.FinregCOUNT is NULL OR jfr2.FinregCOUNT = 0";
		    subqueryWhere += " and r.STATUS_ID <> -1 "; //реестр
			subquery = string.Concat(subquery, subqueryWhere);
			return subquery;
		}

		private string GetRegistersQueryWhere(bool is_archive, int year = 0, DateTime? start = null, DateTime? end = null)
		{
			string where = string.Format(" AND (LOWER({0}.{1}.KIND)='{2}' OR LOWER({0}.{1}.KIND)='{3}') AND {0}.{1}.STATUS_ID <> -1 ",
				DATA_SCHEME,
				TABLE_REGISTER,
				RegisterIdentifier.NPFtoPFR.ToLower(),
				RegisterIdentifier.PFRtoNPF.ToLower());
			where += string.Format(is_archive ?
				" AND (TRIM(LOWER({{0}}.{{1}}.STATUS)) ='{2}' OR TRIM(LOWER({{0}}.{{1}}.STATUS)) ='{3}')"
				: " AND ({{0}}.{{1}}.STATUS IS NULL OR TRIM(LOWER({{0}}.{{1}}.STATUS)) <>'{2}') AND (coalesce({0}.{4}.IS_CUSTOM_ARCHIVE,0) = 0)",
				DATA_SCHEME,//0
				TABLE_FINREGISTER,//1
				RegisterIdentifier.FinregisterStatuses.Transferred.ToLower(),//2
				RegisterIdentifier.FinregisterStatuses.NotTransferred.ToLower(),//3
				TABLE_REGISTER//4
				);
			if (year != 0)
			{
				where += string.Format(" AND (YEAR({0}.{1}.REGDATE)={2} OR {0}.{1}.REGDATE IS NULL) ", DATA_SCHEME, TABLE_REGISTER, year);
			}

			if (start != null)
			{
				where += $" AND ({DATA_SCHEME}.{TABLE_REGISTER}.REGDATE>='{start.Value:dd.MM.yyyy}') ";
			}

			if (end != null)
			{
				where += $" AND ({DATA_SCHEME}.{TABLE_REGISTER}.REGDATE<='{end.Value:dd.MM.yyyy}') ";
			}

			return where;
		}

		private string GetNPFTempAllocationWhere(DateTime? start = null, DateTime? end = null)
		{
            string where = string.Format(" AND ({0}.{1}.KIND='{2}' OR rr.KIND='{3}') AND (coalesce({0}.{4}.STATUS_ID, 0) <> -1 OR coalesce(allCount.frCount,0) = 0 ) AND {0}.{1}.STATUS_ID > 0 ", DATA_SCHEME, TABLE_REGISTER, RegisterIdentifier.TempAllocation, RegisterIdentifier.TempReturn, TABLE_FINREGISTER);
			//if (year != 0)
			//{
			//    where += string.Format(" AND (YEAR({0}.{1}.REGDATE)={2} OR {0}.{1}.REGDATE IS NULL) ", IISService.DATA_SCHEME, IISService.TABLE_REGISTER, year);
			//}

			if (start != null)
			{
				where += $" AND ({DATA_SCHEME}.{TABLE_REGISTER}.REGDATE>='{start.Value.ToShortDateString()}') ";
			}

			if (end != null)
			{
				where += $" AND ({DATA_SCHEME}.{TABLE_REGISTER}.REGDATE<='{end.Value.ToShortDateString()}') ";
			}

			return where;
		}

		private string GetRegistersQuery(string select, string where, bool is_archive, bool is_count)
		{// AND ({{0}}.{{1}}.PARENT_FINREGISTER_ID IS NULL OR {{0}}.{{1}}.STATUS = '{9}')
			string query = string.Format(string.Format(@" {2} 
                FROM {{0}}.{{2}}
                LEFT JOIN ({0}) as jregister ON jregister.registerID = PFR_BASIC.REGISTER.ID 
                LEFT JOIN {{0}}.{{1}} ON {{0}}.{{1}}.REG_ID = {{0}}.{{2}}.ID AND {{0}}.{{1}}.STATUS_ID <> -1
                 
                LEFT JOIN (
                   SELECT fr.ID, count(1) as frCount
                   FROM PFR_BASIC.FINREGISTER fr
                   JOIN  PFR_BASIC.FINREGISTER fr2 ON fr2.PARENT_FINREGISTER_ID = fr.ID
                   WHERE fr.STATUS_ID <> -1 AND fr2.STATUS_ID <> -1
                   GROUP BY fr.ID
                  ) allCount ON allCount.ID = PFR_BASIC.FINREGISTER.ID

                LEFT JOIN (
                   SELECT fr.ID, count(1) as frCount
                   FROM PFR_BASIC.FINREGISTER fr
                   JOIN  PFR_BASIC.FINREGISTER fr2 ON fr2.PARENT_FINREGISTER_ID = fr.ID
                   WHERE fr.STATUS_ID <> -1 AND fr2.STATUS_ID <> -1 AND TRIM(LOWER(fr2.STATUS)) = '{{9}}'
                   GROUP BY fr.ID
                  ) ntCount ON ntCount.ID = PFR_BASIC.FINREGISTER.ID

               

                LEFT JOIN {{0}}.{{6}} ON {{0}}.{{1}}.CR_ACC_ID = {{0}}.{{6}}.ID OR {{0}}.{{1}}.DBT_ACC_ID = {{0}}.{{6}}.ID 
                LEFT JOIN {{0}}.{{3}} ON {{0}}.{{6}}.CONTRAGENTID = {{0}}.{{3}}.ID 
                LEFT JOIN {{0}}.{{8}} ON {{0}}.{{8}}.CONTRAGENTID = {{0}}.{{3}}.ID
                LEFT JOIN {{0}}.{{4}} ON {{0}}.{{2}}.APPD_ID = {{0}}.{{4}}.ID 
                LEFT JOIN (
                    SELECT SUM(pp.DRAFT_AMOUNT) drSUM, pp.FR_ID as fregID 
                    FROM {{0}}.{{5}} pp WHERE STATUS_ID <> -1 GROUP BY pp.FR_ID ) as jpp ON {{0}}.{{1}}.ID = jpp.fregID 
                WHERE {3} (PFR_BASIC.CONTRAGENT.TYPE IS NULL OR LOWER({{0}}.{{3}}.TYPE) = 'нпф') {1} "
                                + (is_count ? string.Empty : "ORDER BY KIND, CONTENT, CNAME"),
				GetRegistersQuerySubquery(is_archive), where, select,
				is_archive
				? "((coalesce(allCount.frCount,0) = coalesce(ntCount.frCount,0) AND coalesce(allCount.frCount,0) <> 0)  OR (coalesce(allCount.frCount,0) = 0 AND {0}.{1}.STATUS = '{9}') OR coalesce({0}.{2}.IS_CUSTOM_ARCHIVE,0) = 1) AND"
				: "(coalesce(allCount.frCount,0) <> coalesce(ntCount.frCount,0) OR coalesce(allCount.frCount,0) = 0) AND"),
																		DATA_SCHEME, //0
																		TABLE_FINREGISTER, //1
																		TABLE_REGISTER, //2
																		TABLE_CONTRAGENT, //3
																		TABLE_APPROVEDOC, //4
																		TABLE_ASG_FIN_TR, //5
																		TABLE_ACCOUNT, //6 
																		string.Empty, //7 - TABLE_FINREGISTER_CORR
																		TABLE_LEGALENTITY,
																		RegisterIdentifier.FinregisterStatuses.Transferred); //9
			return query;
		}

		private string GetRegistersAllocationQuery(string select, string where, bool is_count)
		{
			string query = string.Format(String.Format(@" {2} 
                                                        FROM {{0}}.{{2}}
                                                        LEFT JOIN ({0}) as jregister ON jregister.registerID = PFR_BASIC.REGISTER.ID 
                                                        LEFT JOIN {{0}}.{{1}} ON {{0}}.{{1}}.REG_ID = {{0}}.{{2}}.ID AND pfr_basic.finregister.STATUS_ID <> -1

                LEFT JOIN (
                   SELECT fr.REG_ID as ID, count(1) as frCount
                   FROM PFR_BASIC.FINREGISTER fr
                   WHERE fr.STATUS_ID <> -1
                   GROUP BY fr.REG_ID
                  ) allCount ON allCount.ID = PFR_BASIC.REGISTER.ID

                                                        LEFT JOIN {{0}}.{{6}} ON {{0}}.{{1}}.CR_ACC_ID = {{0}}.{{6}}.ID OR {{0}}.{{1}}.DBT_ACC_ID = {{0}}.{{6}}.ID 
                                                        LEFT JOIN {{0}}.{{3}} ON {{0}}.{{6}}.CONTRAGENTID = {{0}}.{{3}}.ID 
                                                        LEFT JOIN {{0}}.{{8}} ON {{0}}.{{8}}.CONTRAGENTID = {{0}}.{{3}}.ID
                                                        LEFT JOIN {{0}}.{{4}} ON {{0}}.{{2}}.APPD_ID = {{0}}.{{4}}.ID 
                                                        LEFT JOIN {{0}}.{{2}} rr ON rr.ID = {{0}}.{{2}}.RETURN_ID
                                                        LEFT JOIN (
                                                            SELECT SUM(pp.DRAFT_AMOUNT) drSUM, pp.FR_ID fregID 
                                                            FROM {{0}}.{{5}} pp WHERE STATUS_ID <> -1 GROUP BY pp.FR_ID ) jpp ON {{0}}.{{1}}.ID = jpp.fregID 
                                                        LEFT JOIN {{0}}.{{9}} ON {{0}}.{{2}}.PF_ID = {{0}}.{{9}}.ID
                                                        WHERE (PFR_BASIC.CONTRAGENT.TYPE IS NULL OR LOWER({{0}}.{{3}}.TYPE) = 'нпф') 
                                                        {1} "
                + (is_count ? string.Empty : "ORDER BY msel.KIND, msel.CONTENT, msel.CNAME"),
				GetRegistersQuerySubquery(false), where, select),
																		DATA_SCHEME, //0
																		TABLE_FINREGISTER, //1
																		TABLE_REGISTER, //2
																		TABLE_CONTRAGENT, //3
																		TABLE_APPROVEDOC, //4
																		TABLE_ASG_FIN_TR, //5
																		TABLE_ACCOUNT, //6 
																		string.Empty, //7 - TABLE_FINREGISTER_CORR
																		TABLE_LEGALENTITY, //8
																		TABLE_PORTFOLIO); //9
			return query;
		}

		private string GetAllocationRegistersQuerySelect()
		{
			string select = string.Format(@"
SELECT 
COALESCE(rr.ID, {0}.{2}.ID) AS REGID, {0}.{1}.ID, {0}.{3}.ID AS CONTRAGENTID, COALESCE(rr.KIND, {0}.{2}.KIND) AS KIND, COALESCE(rr.CONTENT, {0}.{2}.CONTENT) AS CONTENT, 
{0}.{5}.FORMALIZEDNAME AS CNAME, {0}.{1}.COUNT, {0}.{1}.ZLCOUNT, {0}.{4}.NAME, {0}.{1}.REGNUM AS FINREGNUM, 
{0}.{1}.DATE, drSUM, {0}.{1}.CR_ACC_ID, {0}.{1}.DBT_ACC_ID, COALESCE(rr.TRANCHEDATE, {0}.{2}.TRANCHEDATE) AS TRANCHEDATE, {0}.{1}.STATUS, 
COALESCE(rr.REGNUM, {0}.{2}.REGNUM) AS REGNUM, COALESCE(rr.REGDATE, {0}.{2}.REGDATE) AS REGDATE, {0}.{3}.STATUS_ID, {0}.{1}.CFRCOUNT,  {0}.{6}.YEAR AS PORTFOLIO",
															DATA_SCHEME,
															TABLE_FINREGISTER,
															TABLE_REGISTER,
															TABLE_CONTRAGENT,
															TABLE_APPROVEDOC,
															TABLE_LEGALENTITY,
															TABLE_PORTFOLIO);
			return select;
		}

		private static string GetRegistersQuerySelect()
		{
			string select = string.Format(@"
                SELECT 
                {0}.{2}.ID AS REGID, {0}.{1}.ID, {0}.{3}.ID AS CONTRAGENTID, {0}.{2}.KIND AS KIND, {0}.{2}.CONTENT AS CONTENT, 
                {0}.{5}.FORMALIZEDNAME AS CNAME, {0}.{1}.COUNT, {0}.{1}.ZLCOUNT, {0}.{4}.NAME, {0}.{1}.REGNUM AS FINREGNUM, 
                {0}.{1}.DATE, drSUM, {0}.{1}.CR_ACC_ID, {0}.{1}.DBT_ACC_ID, {0}.{2}.TRANCHEDATE, {0}.{1}.STATUS, 
                {0}.{2}.REGNUM, {0}.{2}.REGDATE, {0}.{3}.STATUS_ID, CAST(NULL AS DECIMAL) AS CFRCOUNT, '' AS PORTFOLIO ",
																		DATA_SCHEME,
																		TABLE_FINREGISTER,
																		TABLE_REGISTER,
																		TABLE_CONTRAGENT,
																		TABLE_APPROVEDOC,
																		TABLE_LEGALENTITY);
			return select;
		}

		private RegistersListItem MapRegisterListItemFromObject(object[] item)
		{
		    var r = new RegistersListItem
		    {
		        RegisterID = (long) item[0],
		        ID = (long?) item[1] ?? 0,
		        ContragentID = (long?) item[2] ?? 0,
		        RegisterKind = ((item[3] as string) ?? string.Empty).Trim(),
		        Content = item[4] as string,
		        ContragentName = item[5] as string,
		        Count = FixDecimal((decimal?) item[6], 4) ?? 0,
		        ZLCount = (long?) item[7],
		        ApproveDocName = item[8] as string,
		        FinregRegNum = item[9] as string,
		        FinregDate = item[10] as DateTime?,
		        DraftSum = FixDecimal((item[11] as decimal?), 4),
		        CreditAccID = (long?) item[12] ?? 0,
		        DebitAccID = (long?) item[13] ?? 0,
		        TrancheDate = item[14] as DateTime? ?? DateTime.MinValue,
		        Status = ((item[15] as string) ?? string.Empty).Trim(),
		        RegNum = item[16] as string,
		        RegDate = (DateTime?) item[17],
		        IsActiveNPF = (long?) item[18] == 1,
		        CFR = FixDecimal((decimal?) item[19], 4),
		        Portfolio = item[20] as string
		    };

		    if (r.RegDate == DateTime.MinValue)
                r.RegDate = null; 
            
            if (r.FinregDate == DateTime.MinValue)
                r.FinregDate = null;
			return r;
		}













		





   



		private List<SecurityAvailableForOrderListItem> ReadSecurityAvailableForOrderListItems(DBCommandLogging cmd, bool forBuyOrder)
		{
			using (var reader = cmd.ExecuteReader())
			{
				var result = new List<SecurityAvailableForOrderListItem>();
				while (reader.Read())
				{
					var item = new SecurityAvailableForOrderListItem(forBuyOrder) { Security = new Security() };

					item.Security.ID = ReadInt64Value(reader, 0);
					item.Security.Name = ReadStringValue(reader, 1);
					item.Security.Issue = ReadStringValue(reader, 2);
					item.Security.SecurityId = ReadStringValue(reader, 3);
					item.Security.RegNum = ReadStringValue(reader, 4);
					item.Security.ISIN = ReadStringValue(reader, 5);
					item.Security.LocationDate = ReadDateTimeValue(reader, 6).Date;
					item.Security.RepaymentDate = ReadDateTimeValue(reader, 7).Date;
					item.Security.IssueVolume = ReadDecimalValue(reader, 8, 4);
					item.Security.PlacedVolume = ReadDecimalValue(reader, 9, 4);
					item.Security.NomValue = ReadDecimalValue(reader, 10, 4);
					item.Security.PaymentPeriod = ReadInt32Value(reader, 11);
					item.Security.KindID = ReadInt64Value(reader, 12);
					item.Security.CurrencyID = ReadInt64Value(reader, 13);
					item.Security.Status = ReadInt32Value(reader, 14);
					item.Security.KindName = ReadStringValue(reader, 15);
					item.Security.CurrencyName = ReadStringValue(reader, 16);
					if (!forBuyOrder)
						item.AvailableCount = ReadInt64Value(reader, 17);

					result.Add(item);
				}
				return result;
			}
		}

		public SecurityAvailableForOrderListItem GetSecurityAvailableForAuctionOrder(long p_iSecurityID)
		{
			string query = string.Format(@"
                SELECT s.ID,s.NAME,s.ISSUE,s.SECURITYID,
s.REGNUM,s.ISIN,s.LOCATIONDATE,
s.REPAYMENTDATE,s.ISSUEVOLUME,s.PLACEDVOLUME,
s.NOMVALUE,s.PAYMENTPERIOD,s.KSEC_ID,
s.CURR_ID,s.STATUS,k.NAME, 
c.NAME FROM {0}.SECURITY s
                    INNER JOIN {0}.KINDSECURITY k ON k.ID = s.KSEC_ID
                    INNER JOIN {0}.CURRENCY c ON c.ID = s.CURR_ID
                WHERE s.ID = {1}", DATA_SCHEME, p_iSecurityID);

			using (var cmd = new DBCommandLogging(query))
			{
				cmd.Prepare();
				return ReadSecurityAvailableForOrderListItems(cmd, true).First();
			}
		}

		public List<SecurityAvailableForOrderListItem> GetSecuritiesAvailableForBuyOrder(long p_iSelectedOrderCurrencyID, DateTime p_OrderTerm)
		{
			string query = string.Format(@"
                SELECT s.ID,s.NAME,s.ISSUE,s.SECURITYID,
s.REGNUM,s.ISIN,s.LOCATIONDATE,
s.REPAYMENTDATE,s.ISSUEVOLUME,s.PLACEDVOLUME,
s.NOMVALUE,s.PAYMENTPERIOD,s.KSEC_ID,
s.CURR_ID,s.STATUS,k.NAME, 
c.NAME FROM {0}.SECURITY s
                    INNER JOIN {0}.KINDSECURITY k ON k.ID = s.KSEC_ID
                    INNER JOIN {0}.CURRENCY c ON c.ID = s.CURR_ID
                WHERE s.CURR_ID = {2} AND s.REPAYMENTDATE > '{1}'
                ORDER BY s.NAME", DATA_SCHEME, p_OrderTerm.ToString("yyyy-MM-dd"), p_iSelectedOrderCurrencyID);

			using (var cmd = new DBCommandLogging(query))
			{
                cmd.Prepare();
				return ReadSecurityAvailableForOrderListItems(cmd, true);
			}
		}

		public List<SecurityAvailableForOrderListItem> GetSecuritiesAvailableForSaleOrder(long p_iSelectedOrderCurrencyID, long p_iPortfolioID)
		{
			string query = string.Format(@"
                SELECT s.ID,s.NAME,s.ISSUE,s.SECURITYID,
s.REGNUM,s.ISIN,s.LOCATIONDATE,
s.REPAYMENTDATE,s.ISSUEVOLUME,s.PLACEDVOLUME,
s.NOMVALUE,s.PAYMENTPERIOD,s.KSEC_ID,
s.CURR_ID,s.STATUS,k.NAME, 
c.NAME, sec.cnt FROM {0}.SECURITY s
                INNER JOIN (
        	        SELECT SUM(CASE o.TYPE WHEN 'покупка' THEN cb.SUM WHEN 'продажа' THEN -cb.SUM END) AS CNT, i.SEC_ID AS sid
        	        FROM {0}.order o 
        	        INNER JOIN {0}.CBINORDER i ON i.ORD_ID=o.ID AND o.PF_ID = {1}
        	        INNER JOIN 
        	        (
        		        SELECT SUM(r.COUNT) as SUM, r.CBORD_ID as cbid
        		        FROM {0}.ORD_REPORT r
        		        INNER JOIN {0}.CBINORDER i ON r.CBORD_ID = i.ID
        		        GROUP BY r.CBORD_ID
        	        ) cb
        	        ON (cb.cbid = i.id)
        	        GROUP BY i.SEC_ID
        	        ) sec
                ON sec.sid = s.id
                INNER JOIN {0}.KINDSECURITY k ON k.ID = s.KSEC_ID
                INNER JOIN {0}.CURRENCY c ON c.ID = s.CURR_ID
                WHERE s.CURR_ID = {2} 
                ORDER BY s.NAME", DATA_SCHEME, p_iPortfolioID, p_iSelectedOrderCurrencyID);

			using (var cmd = new DBCommandLogging(query))
			{
				cmd.Prepare();
				return ReadSecurityAvailableForOrderListItems(cmd, false);
			}
		}

#endregion



#region calc methods
		public decimal GetTotalInvestDohodForSIRegister(long id)
		{
			string q = string.Format(@"
            SELECT 
            SUM({0}.{1}.INVESTDOHOD) 
            FROM 
            {0}.{1}, 
            {0}.{2} 
            WHERE 
            {0}.{2}.ID={0}.{1}.SI_TR_ID 
            AND {0}.{2}.SI_REG_ID={3}
            AND {0}.{1}.STATUS_ID <> -1 ",
				DATA_SCHEME, TABLE_REQ_TRANSFER, TABLE_SI_TRANSFER, id);

		    using (var cmd = new DBCommandLogging(q))
		    {
		        cmd.Prepare();
		        var reader = cmd.ExecuteReader();

		        decimal retValue = (reader.Read()) ? ReadDecimalValue(reader, 0, 4) : 0;

		        reader.Close();
		        reader.Dispose();
		        cmd.Cancel();
		        cmd.Dispose();

		        return retValue;
		    }
		}


		public bool IncludedToThe761List(string bankCode, DateTime date)
		{
			string q = $@"select count(i.ID) from PFR_BASIC.KOSTATUSIMPORT si
join PFR_BASIC.KOIMPORT i on si.IMPORT_ID=i.ID
where i.DATE<='{date.Date.ToString("yyyy-MM-dd")}' and si.CODE='{bankCode}'";

			try
			{
			    using (var cmd = new DBCommandLogging(q))
			    {
			        var count = cmd.ExecuteScalar();
			        cmd.Cancel();
			        cmd.Dispose();

			        return (int) count > 0;
			    }
			}
			catch (Exception e)
			{
				LogManager.LogException(e);
				throw;
			}
		}

		public DepClaimSelectParams SelectLastDepClaimSelectParams(long stockID)
		{
			string q = $@"SELECT ID
                    FROM {DATA_SCHEME}.DEPCLAIMSELECTPARAMS 
                    WHERE STOCKID = {stockID} AND STATUS<>-1
                    ORDER BY SELECT_DATE DESC, ID DESC
                    FETCH FIRST 1 ROWS ONLY
                    ";

			try
			{
			    using (var cmd = new DBCommandLogging(q))
			    {
			        cmd.Prepare();
			        var reader = cmd.ExecuteReader();

			        long id = (reader.Read()) ? ReadInt64Value(reader, 0) : 0;

			        reader.Close();
			        reader.Dispose();
			        cmd.Cancel();
			        cmd.Dispose();

			        return id > 0 ? GetByID<DepClaimSelectParams>(id) : null;
			    }
			}
			catch (Exception e)
			{
				LogManager.LogException(e);
			}

			return null;
		}










#endregion


	    private bool? _infoAlertsEnabled;
	    public bool IsInfoAlertsEnabled()
	    {

	        if (_infoAlertsEnabled.HasValue)
	            return _infoAlertsEnabled.Value;

	        var s = ConfigurationManager.AppSettings["InfoAlertsEnabled"];

	        if (string.IsNullOrEmpty(s))
	            return (bool)(_infoAlertsEnabled = true);

	        var propValue = true;
	        bool.TryParse(s, out propValue);
	        return (bool)(_infoAlertsEnabled = propValue);

	    }
    }
}