﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using db2connector.Contract;
using NHibernate;
using PFR_INVEST.Common.Logger;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.DataAccess.Server;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.DataObjects.XMLModel.KIP;
using PFR_INVEST.Integration.KIPClient;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
	public partial class IISServiceHib
	{
		#region ForModels

		public List<KipLog> GetKIPLogList(DateTime from, DateTime to)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var list = session.CreateQuery(@"SELECT kl
														FROM KipLog kl
												where
													kl.Date >= :from AND kl.Date <= :to ")
											.SetParameter("from", from)
											.SetParameter("to", to)
											.List<KipLog>();
				return list.ToList();
			}

		}

		public List<KIPRegisterListItem> GetKIPRegisterList(DateTime from, DateTime to)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var list = session.CreateQuery(@"SELECT new KIPRegisterListItem(kr, if, ef, erf)
														FROM KipRegisterHib kr
														left join fetch kr.ImportFile if
														left join fetch kr.ExportFile ef
														left join fetch kr.ExportReceiptFile erf
												where
													if.DDate >= :from AND if.DDate <= :to ")
											.SetParameter("from", from)
											.SetParameter("to", to)
											.List<KIPRegisterListItem>();
				return list.ToList();
			}
		}

        #endregion ForModels

	    private static void LogGeneralDocumentError(KipLog.ErrorLevels level, params string[] message)
	    {
	        LogDocumentError(null, null, level, message);
	    }

        private static void LogDocumentError(ISession session, RepositoryNoAucImpExpFile file, KipLog.ErrorLevels level, params string[] message)
		{
		    var isNewSession = session == null;
		    session = session ?? DataAccessSystem.OpenSession();
		    try
		    {
		        var log = new KipLog
		        {
		            FileRepositoryID = file?.ID,
		            ErrorLevel = level,
		            Meassage = string.Join(Environment.NewLine, message)
		        };
		        session.SaveOrUpdate(log);
		        session.Flush();
		        LogManager.Log2KIP(string.Join(Environment.NewLine, message), LogSeverity.Error);
		    }
		    finally
		    {
		        if (isNewSession)
		            session.Close();
		    }
		}



		private static void SaveXML(XMLFileResult file, string folder)
		{
			if (!file.IsSuccesss)
		        return;


		    var fname = Path.GetFileNameWithoutExtension(file.FileName);
			var ext = Path.GetExtension(file.FileName);
			var name = Path.Combine(folder, file.FileName);

			var index = 1;
			while (File.Exists(name))
			{
				index++;
				name = Path.Combine(folder, $"{fname} ({index}){ext}");
			}


			if (file.IsText)
				File.WriteAllText(name, file.FileBody, Encoding.UTF8);
			if (file.IsBinary)
				File.WriteAllBytes(name, file.FileBodyBinary);
		}

		public ActionResult<string[]> GetKIPFilesForImport()
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var impSetting = session.Get<SettingsPathFile>((long)SettingsPathFile.IdDefinition.PkipImport);
				if (impSetting != null && impSetting.ServClientValue == SettingsPathFile.FileLocation.Server)
				{
					if (!Directory.Exists(impSetting.Path))
						return ActionResult<string[]>.Error($"На сервере не найдена папка для импорта '{impSetting.Path}'");
					var files = Directory.GetFiles(impSetting.Path, "*.xml");
					return new ActionResult<string[]>(files);
				}
			}

			return new ActionResult<string[]>(new string[0]);
		}


		public ActionResult<string> LoadKIPFileFromServer(string fileName)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var impSetting = session.Get<SettingsPathFile>((long)SettingsPathFile.IdDefinition.PkipImport);
				if (impSetting != null && impSetting.ServClientValue == SettingsPathFile.FileLocation.Server)
				{
					var file = Path.Combine(impSetting.Path, fileName);
					var res = LoadKIPFile(GetKIPSettings(), File.ReadAllBytes(file));
					return res;
				}
			}
			return ActionResult<string>.Error("Невозможно загрузить файл");
		}

	    public void TestKIP(byte[] file)
	    {
	        try
	        {
	            LoadKIPZipFile(file);
	        }
	        catch
	        {
	            // ignored
	        }
	    }

		public ActionResult<string> LoadKIPZipFile(byte[] file)
		{
		    List<KeyValuePair<string, byte[]>> files;

            try
		    {
		        //unzip file
		        files = ZipTools.UnzipFile(file);
		    }
		    catch (Exception ex)
		    {
                LogGeneralDocumentError(KipLog.ErrorLevels.CanNotRead, $@"Распаковка ZIP файла - исключение!!! {ex}");
                return ActionResult<string>.Error("Невозможно прочитать файл");
            }

            //Принмаем файлы только с 1 запакованым файлом, для нескольких мы не можем дать корректный результат импорта в виде 1 строчки
            if (files.Count > 1)
		    {
                LogGeneralDocumentError(KipLog.ErrorLevels.CanNotRead, $@"Распаковка файла - архив содержит несколько файлов! {string.Join(" + ", files.Select(a=> a.Key).ToArray())}");
		        return ActionResult<string>.Error("Невозможно прочитать файл", 2);
		    }
            if (files.Count < 1)
            {
                LogGeneralDocumentError(KipLog.ErrorLevels.CanNotRead, @"Распаковка файла - архив не содержит файлов или ошибка распаковки!");
                return ActionResult<string>.Error("Невозможно прочитать файл", 2);
            }

            var res = LoadKIPFile(GetKIPSettings(), files.First().Value);
			return res;
		}

		public ActionResult<string> LoadKIPFile(KIPSettings settings, byte[] file, bool gzip = false)
		{
			//Распаковываем поток, если нужно
			if (gzip)
			{
				file = GZipCompressor.Decompress(file);
			}
			//сохраняем файл в базу для логирования
		    using (var session = DataAccessSystem.OpenSession())
			{
				var dt = DateTime.Now;
				var fileRepository = new RepositoryNoAucImpExpFile
				{
				    Repository = GZipCompressor.Compress(file),
				    ImpExp = 1,
				    DDate = dt,
				    Status = 1,
				    TTime = dt.TimeOfDay,
				    UserName = DomainUser,
				    Comment = "Импорт XML файлов ПТК КИП"
				};
				session.SaveOrUpdate(fileRepository);
				session.Flush();

				//Проверяем, что загруженый xml один из возможных КИП документов
				var fileStream = new MemoryStream(file);
				var serializer = new XmlSerializer(typeof(KIP_CommonDoc));
				var reader = XmlReader.Create(fileStream);

				KIP_CommonDoc doc;
				try
				{
					doc = (KIP_CommonDoc)serializer.Deserialize(reader);
				}
				catch (InvalidOperationException ex)
				{
					if (ex.InnerException is XmlSchemaValidationException)
						LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, ex.InnerException.Message);
					else
				        LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, $@"Ошибка десериализации - {ex}");
					return ActionResult<string>.Error("Невозможно прочитать файл", 2);
				}

				//Зависимо от типа парсим документ
				switch (doc.Info.DocumentType.Code)
				{
					case KIP_Import.RECALL_MSK:
					case KIP_Import.TRANS_MSK:
						return LoadKIPRegister(settings, session, fileRepository);
					case KIP_Receipt.ERROR_STEP2:
					case KIP_Receipt.VERIFIED_STEP2:
						return LoadKIPReceipt(session, fileRepository);
					default:
						LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead,
						    $"Неизвестный код документа '{doc.Info.DocumentType.Code}'");
						return ActionResult<string>.Error("Отклонен на ФЛК", 1);
				}
			}
		}

		private ActionResult<string> LoadKIPReceipt(ISession session, RepositoryNoAucImpExpFile fileRepository)
		{
			var file = GZipCompressor.Decompress(fileRepository.Repository);

			var fileStream = new MemoryStream(file);
			var serializer = new XmlSerializer(typeof(KIP_Receipt));
			var reader = XmlReader.Create(fileStream);

			KIP_Receipt doc;
			try
			{
				doc = (KIP_Receipt)serializer.Deserialize(reader);
			}
			catch (InvalidOperationException ex)
			{
				if (ex.InnerException is XmlSchemaValidationException)
					LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, ex.InnerException.Message);
				else
				    LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, $@"Ошибка десериализации KIP_Receipt - {ex}");
				return ActionResult<string>.Error("Невозможно прочитать файл", 2);
			}

			//Проверяем, что квитанция относится к существующему в базе документу 
			var register = session.CreateQuery("select kr from KipRegister kr where kr.ExportDocNum = :num")
					.SetParameter("num", doc.Info.SourceDocument.DocNumber)
					.SetMaxResults(1).List<KipRegister>().FirstOrDefault();
			if (register == null)
			{
				LogDocumentError(session, fileRepository, KipLog.ErrorLevels.Logical, $"Документ с номером {doc.Info.SourceDocument.DocNumber} не существует среди экспортированых");
				return ActionResult<string>.Error("Отклонен на ФЛК", 1);
			}

			//Выставляем результат проверки

			register.ReceiptDocNum = doc.Info.DocNumber;
			register.ExportReceiptID = fileRepository.ID;
			switch (doc.Info.DocumentType.Code)
			{
				case KIP_Receipt.VERIFIED_STEP2:
					register.ExportStatus = KipRegister.ExportStatuses.ExportedAndAccepted;
					//LogDocumentError(session, FileRepository, KipLog.ErrorLevels.NoError, string.Format("Документ с номером {0} принят КИП", doc.Info.SourceDocument.DocNumber));
					break;

				case KIP_Receipt.ERROR_STEP2:
					register.ExportStatus = KipRegister.ExportStatuses.ExportedAndRejected;
					LogDocumentError(session, fileRepository, KipLog.ErrorLevels.Logical, $"Документ с номером {doc.Info.SourceDocument.DocNumber} не принят КИП. Подробности в квитанции");
					break;

			}

			session.Save(register);
			session.Flush();

			return new ActionResult<string>();
		}

		private ActionResult<string> LoadKIPRegister(KIPSettings kipSettings, ISession session, RepositoryNoAucImpExpFile fileRepository)
		{
			var file = GZipCompressor.Decompress(fileRepository.Repository);
			var fileStream = new MemoryStream(file);
			var serializer = new XmlSerializer(typeof(KIP_Import));
		    var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
		    settings.Schemas.Add(string.Empty, KIP_DocumentBase.GetShema("I_TRANS_MSK_1.0.1.xsd"));
			var reader = XmlReader.Create(fileStream, settings);

			KIP_Import doc;
			try
			{
				doc = (KIP_Import)serializer.Deserialize(reader);
			}
			catch (InvalidOperationException ex)
			{
				if (ex.InnerException is XmlSchemaValidationException)
					LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, ex.InnerException.Message);
				else
					LogDocumentError(session, fileRepository, KipLog.ErrorLevels.CanNotRead, $@"Ошибка десериализации KIP_Import - {ex}");
				return ActionResult<string>.Error("Невозможно прочитать файл", 2);
			}
			if (!doc.ValidateFields())
			{
				LogDocumentError(session, fileRepository, KipLog.ErrorLevels.Logical, "Некоректные суммы в документе");
				return ActionResult<string>.Error("Отклонен на ФЛК", 1);
			}

			//Проверяем, что все упомянутые УК/НПФ есть в базе
			var errors = new List<string>();
			var mapSI = new Dictionary<KIP_Import.RegisterItem, PFR_INVEST.DataObjects.Contract>();
			var mapNPF = new Dictionary<KIP_Import.RegisterItem, LegalEntity>();
			var mapVR = new Dictionary<KIP_Import.RegisterItem, PFR_INVEST.DataObjects.Contract>();

            var paList = GePayAssignmentsForNpfWithElementName();

			//СИ + Временное размещение
			foreach (var item in doc.Items)
			{
				//Обрабатываем временное размещение
				if (item.Organization.OrgType == "УК" && item.Organization.Code == "ПФР")
				{
					var le = session.CreateQuery("select le from LegalEntityHib le join le.Identifiers i where i.StatusID <> -1 and i.Identifier = :code order by i.SetDate desc")
									.SetParameter("code", item.Organization.Code)
									.SetMaxResults(1).List<LegalEntityHib>().FirstOrDefault();

					if (le != null)
						mapNPF.Add(item, le);
					else
						errors.Add($"Не найдена организация Код:'{item.Organization.Code}' ИНН:'{item.Organization.INN}'");
				}

				else
				{

					var le = session.CreateQuery("select le from LegalEntityHib le join le.Identifiers i where le.INN = :inn  and i.StatusID <> -1 and i.Identifier = :code")
									.SetParameter("inn", item.Organization.INN)
									.SetParameter("code", item.Organization.Code)
									.SetMaxResults(1).List<LegalEntityHib>().FirstOrDefault();
					if (le != null)
						switch (item.Organization.OrgType)
						{
							case "НПФ":
								mapNPF.Add(item, le);
								break;
							case "УК":

								//Ищем по имени портфеля								
								var contract = le.Contracts.FirstOrDefault(c => c.StatusID != -1 && c.PortfolioName == item.Organization.PortfolioName) ?? le.Contracts.FirstOrDefault(c => c.StatusID != -1 && c.ContractNumber == item.Organization.PortfolioName);
								//Ищем по номеру договора
							    //выбираем договор, если он единственный (http://jira.vs.it.ru/browse/DOKIPIV-1041) 
						        if (contract == null && le.Contracts.Count(a => a.StatusID != -1) == 1)
						            contract = le.Contracts.First();
								if (contract == null)
								{
									errors.Add(string.Format("Не найден договор '{2}' для организации Код:'{0}' ИНН:'{1}'", item.Organization.Code, item.Organization.INN, item.Organization.PortfolioName));
								}
								else
								{
									switch (contract.TypeID)
									{
										case (int)Document.Types.SI: mapSI.Add(item, contract); break;
										case (int)Document.Types.VR: mapVR.Add(item, contract); break;
									}
								}
								break;
						}
					else
						errors.Add($"Не найдена организация Код:'{item.Organization.Code}' ИНН:'{item.Organization.INN}'");
				}
			}

			if (errors.Count > 0)
			{
				LogDocumentError(session, fileRepository, KipLog.ErrorLevels.Logical, errors.ToArray());
				return ActionResult<string>.Error("Отклонен на ФЛК", 1);
			}

			// Проверяем, документ уже однажды не импортировали

			var exists = session.CreateQuery("select kr from KipRegister kr where kr.DocNum = :num")
					.SetParameter("num", doc.Info.DocNumber)
					.SetMaxResults(1).List<KipRegister>().FirstOrDefault();
			if (exists != null)
			{
				LogDocumentError(session, fileRepository, KipLog.ErrorLevels.Logical, $"Документ с номером {doc.Info.DocNumber} уже был импортирован ранее");
				return ActionResult<string>.Error("Отклонен на ФЛК", 1);
			}
			#region Import


			using (var transation = session.BeginTransaction())
			{
				var kipReg = new KipRegisterHib
				{
					DocNum = doc.Info.DocNumber,
					DocDate = doc.Info.Date,
					DocType = doc.Info.DocumentType.Code,
					ImportFileID = fileRepository.ID,
					Period = doc.Info.Period,
					PeriodType = doc.Info.PeriodType,
					Links = new List<KipLinksHib>()
				};
                // Всё найдено, время приступать к созданию реестров

				#region SI Register

				if (mapSI.Count > 0)
				{
					// SI
				    var sireg = new SIRegister
				    {
				        RegisterKind = "Постановление правления ПФР",
				        OperationTypeID = null,
				        CompanyYearID = Year.YearToID(doc.Info.Period.Year),
				        CompanyMonthID = doc.Info.Period.Month,
				        Comment = $"Создано на основании реестра КИП № {doc.Info.DocNumber} от {doc.Info.DateText}",
				        RegisterNumber = doc.Info.DocNumber,
				        RegisterDate = doc.Info.Date,
				        OperationID = kipSettings.OperationMappings?
				            .First(x => x.Code == doc.Info.DocumentType.Code)?
				            .Items?
				            .First(x => x.Section == KIPSettings.SECTION_SI)?
				            .Operation
				    };

				    if (doc.Info.DocumentType.Code == KIP_Import.RECALL_MSK)
					{
						//из УК
						sireg.DirectionID = (long)SIRegister.Directions.FromUK;
                        // sireg.OperationID = (long)SIRegister.Operations.SPNRedistributionMSK;
                    }
                    else if (doc.Info.DocumentType.Code == KIP_Import.TRANS_MSK)
					{
						//в УК
						sireg.DirectionID = (long)SIRegister.Directions.ToUK;
						//sireg.OperationID = (long)SIRegister.Operations.MSKHalfYearPortfolioDissolution;
					}

					session.SaveOrUpdate(sireg);
					session.Flush();

					foreach (var siItem in mapSI)
					{
						//Пока берём последний договор по имени
						var contract = siItem.Value;
						var item = siItem.Key;

						var st = new SITransfer
						{
							ContractID = contract.ID,
							//TransferRegisterID = RegisterID, 
							InsuredPersonsCount = item.ZLCount,
							PlannedTransferSum = null, //если правопреемники - записываем плановую сумму
							TransferRegisterID = sireg.ID,
                            StatusID = 1
						};

						session.SaveOrUpdate(st);
						session.Flush();

						//Создание перечисления
						var rt = new ReqTransfer
						{
							Sum = item.Summ.TotalSPN,
							InvestmentIncome = item.Summ.SummInvest,
							TransferListID = st.ID,
							TransferStatus = TransferStatusIdentifier.sBeginState,
							ZLMoveCount = item.ZLCount,
						    StatusID = 1
						};

						session.SaveOrUpdate(rt);
						session.Flush();

						kipReg.Links.Add(new KipLinksHib
						{
							KipRegisterID = kipReg.ID,
							LegalEntityID = contract.LegalEntityID,
							ContractID = contract.ID,
							SITransferID = st.ID,
							OrganizationCode = item.Organization.Code,
							PortfolioCode = item.Organization.PortfolioCode
						});

					}

					kipReg.SIRegisterID = sireg.ID;
				}


				#endregion SI Register

				#region VR Register
				if (mapVR.Count > 0)
				{
					// SI
				    var vrreg = new SIRegister
				    {
				        RegisterKind = "Постановление правления ПФР",
				        OperationTypeID = null,
				        CompanyYearID = Year.YearToID(doc.Info.Period.Year),
				        CompanyMonthID = doc.Info.Period.Month,
				        Comment = $"Создано на основании реестра КИП № {doc.Info.DocNumber} от {doc.Info.DateText}",
				        RegisterNumber = doc.Info.DocNumber,
				        RegisterDate = doc.Info.Date,
				        OperationID = kipSettings.OperationMappings?
				            .First(x => x.Code == doc.Info.DocumentType.Code)?
				            .Items?
				            .First(x => x.Section == KIPSettings.SECTION_VR)?
				            .Operation
				    };

				    // Получаем содержание операции из настроек
				    if (doc.Info.DocumentType.Code == KIP_Import.RECALL_MSK)
					{
						//из ВР
						vrreg.DirectionID = (long)SIRegister.Directions.FromGUK;
						//vrreg.OperationID = (long)SIRegister.Operations.MSKDeadZL;
					}
					else if (doc.Info.DocumentType.Code == KIP_Import.TRANS_MSK)
					{
						//в ВР
						vrreg.DirectionID = (long)SIRegister.Directions.ToGUK;
						//vrreg.OperationID = (long)SIRegister.Operations.SPNTransferToControl;
					}

					session.SaveOrUpdate(vrreg);
					session.Flush();

					foreach (var siItem in mapVR)
					{
						//Пока берём последний договор по имени
						var contract = siItem.Value;
						var item = siItem.Key;
						var st = new SITransfer
						{
							ContractID = contract.ID,
							InsuredPersonsCount = item.ZLCount,
							PlannedTransferSum = null, //если правопреемники - записываем плановую сумму
							TransferRegisterID = vrreg.ID,
						    StatusID = 1
						};

						session.SaveOrUpdate(st);
						session.Flush();

						//Создание перечисления
						var rt = new ReqTransfer
						{
							Sum = item.Summ.TotalSPN,
							InvestmentIncome = item.Summ.SummInvest,
							TransferListID = st.ID,
							TransferStatus = TransferStatusIdentifier.sBeginState,
							ZLMoveCount = item.ZLCount,
						    StatusID = 1
						};

						session.SaveOrUpdate(rt);
						session.Flush();

						kipReg.Links.Add(new KipLinksHib
						{
							KipRegisterID = kipReg.ID,
							LegalEntityID = contract.LegalEntityID,
							ContractID = contract.ID,
							SITransferID = st.ID,
							OrganizationCode = item.Organization.Code,
							PortfolioCode = item.Organization.PortfolioCode
						});
					}
					kipReg.VRRegisterID = vrreg.ID;
				}


				#endregion VR Register

				#region NPF Register
				if (mapNPF.Count > 0)
				{
					// Реестр НПФ
					var npfreg = new Register
					{
						RegDate = doc.Info.Date,
						RegNum = doc.Info.DocNumber,
						Comment = $"Создан на основании реестра КИП № {doc.Info.DocNumber} от {doc.Info.DateText}",
						Company = "КИП",
						StatusID = 1
					};

                    // Получаем содержание операции из настроек
                    var operation = kipSettings.OperationMappings?
                                    .First(x => x.Code == doc.Info.DocumentType.Code)?
                                    .Items?
                                    .First(x => x.Section == KIPSettings.SECTION_NPF)?
                                    .Operation;
                    if (doc.Info.DocumentType.Code == KIP_Import.RECALL_MSK)
					{
						// из НПФ
						npfreg.Kind = RegisterIdentifier.NPFtoPFR;
					    if (operation != null)
					    {
                            // Получаем содержание операции из таблицы ELEMENT (KEY: 60)
					        npfreg.Content = GetByID<Element>((long)operation.Value)?.Name;
                            //npfreg.Content = RegisterIdentifier.Operation.MotherCapitalRecal;
                        }
					}
					else if (doc.Info.DocumentType.Code == KIP_Import.TRANS_MSK)
					{
						// в НПФ
						npfreg.Kind = RegisterIdentifier.PFRtoNPF;
                        // Получаем содержание операции из таблицы ELEMENT (KEY: 61)
                        npfreg.Content = GetByID<Element>((long)operation.Value)?.Name;
                        //npfreg.Content = RegisterIdentifier.Operation.MotherCapital;

                        var pa =
					        paList.Where(
					            a =>
					                a.DirectionID == (long) Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR &&
					                a.ElementName == npfreg.Content || a.OperationContentID == 0)
					            .OrderByDescending(a => a.OperationContentID)
					            .FirstOrDefault();
                        npfreg.PayAssignment = pa != null ? PaymentAssignmentHelper.ReplaceTagDataForNPF(pa.Name, npfreg.RegDate) : null;
					}

					session.SaveOrUpdate(npfreg);
					session.Flush();

					foreach (var itemNPF in mapNPF)
					{
						var accPFR = GetPFRRoubleAccount();
						var accNPF = GetContragentRoubleAccount(itemNPF.Value.ContragentID.Value);

						var item = itemNPF.Key;
						var finreg = new Finregister
						{
							StatusID = 1,
							RegisterID = npfreg.ID,
							Comment = $"Создан на основании реестра КИП № {doc.Info.DocNumber} от {doc.Info.DateText}",
							Count = item.Summ.TotalSPN,
							ZLCount = item.ZLCount,
							Status = "финреестр создан"
						};
						if (doc.Info.DocumentType.Code == KIP_Import.RECALL_MSK)
						{
							finreg.CrAccID = accNPF.ID;
							finreg.DbtAccID = accPFR.ID;
						}
						else if (doc.Info.DocumentType.Code == KIP_Import.TRANS_MSK)
						{
							finreg.CrAccID = accPFR.ID;
							finreg.DbtAccID = accNPF.ID;
						}


						session.SaveOrUpdate(finreg);
						session.Flush();

						kipReg.Links.Add(new KipLinksHib
						{
							KipRegisterID = kipReg.ID,
							LegalEntityID = itemNPF.Value.ID,
							FinregisterID = finreg.ID,
							OrganizationCode = item.Organization.Code,
							PortfolioCode = item.Organization.PortfolioCode
						});
					}

					kipReg.NPFRegisterID = npfreg.ID;
				}
				#endregion NPF Register
                
				session.SaveOrUpdate(kipReg);
				transation.Commit();
			}

			#endregion Import

			return new ActionResult<string>();
		}

		private static bool IsKIPRegisterExportReady(ISession session, KipRegister register)
		{
			//session.CacheMode = CacheMode.Ignore;
			var isReady = true;

			//SI
			if (register.SIRegisterID.HasValue)
			{
				//Все перечисление должны иметь статус "СПН перечислены" или "Акт подписан"
				var reg = session.CreateQuery(@"select rt FROM ReqTransferHib rt JOIN rt.TransferList st where rt.StatusID <> -1 and st.TransferRegisterID = :id ")
								.SetParameter("id", register.SIRegisterID.Value)
								.List<ReqTransfer>().FirstOrDefault(rt => !rt.IsSpnTransferredOver());
				isReady &= reg == null;
				if (!isReady) return false;
			}

			//VR
			if (register.VRRegisterID.HasValue)
			{

				//Все перечисление должны иметь статус "СПН перечислены" или "Акт подписан"
				var reg = session.CreateQuery(@"select rt FROM ReqTransferHib rt JOIN rt.TransferList st where rt.StatusID <> -1 and st.TransferRegisterID = :id ")
								.SetParameter("id", register.VRRegisterID.Value)
								.List<ReqTransfer>().FirstOrDefault(rt => !rt.IsSpnTransferredOver());
				isReady &= reg == null;
				if (!isReady) return false;
			}

			//NPF
			if (register.NPFRegisterID.HasValue)
			{
				var reg = session.Get<RegisterHib>(register.NPFRegisterID.Value);
				var tempAllAcc = session.CreateQuery(@"select a from Account a 
																	join a.Contragent c 
																	join c.LegalEntities le 
																	join le.Identifiers id
														where id.Identifier = 'ПФР'	")
											.SetMaxResults(1).List<Account>().FirstOrDefault();
				var tempAccID = tempAllAcc?.ID ?? 0;
				var tempAllFr = reg.FinregistersList.Where(fr => fr.CrAccID == tempAccID || fr.DbtAccID == tempAccID).ToList();
				var unclosed = reg.FinregistersList.FirstOrDefault(fr => !tempAllFr.Contains(fr) && fr.Count != fr.DraftsList.Sum(p => p.DraftAmount));
				isReady &= unclosed == null;
				//Проверяем финрестры временного размещения
				var tempUnclosed = tempAllFr.FirstOrDefault(fr => !fr.Date.HasValue);
				isReady &= tempUnclosed == null;

				if (!isReady) return false;
			}
			return true;
		}

		public List<string> GetKIPExportReadyList()
		{
			using (var session = DataAccessSystem.OpenSession())
			{
			    //Определяем, импортированые документы, для которых закрыты Реестры СИ, ВР и НПФ
				//Находим, реестры КИП, для которых не было ещё экспорта
				var kipRegisters = session.CreateQuery("select k from KipRegister k where  k.ExportStatusID is null OR   k.ExportStatusID = 0").List<KipRegister>();
			    return (from kiPreg in kipRegisters where IsKIPRegisterExportReady(session, kiPreg) select kiPreg.DocNum).ToList();
			}
		}

		public byte[] GetKIPExportZipFile(string docNumber)
		{
			var file = GetKIPExportFile(docNumber);
			var retVal = ZipTools.ZipFiles(file.FileName, file.FileBodyBinary);

			return retVal;
		}

		public XMLFileResult GetKIPExportFile(string docNumber, bool onlyServerExport = false)
		{
			var retVal = new XMLFileResult();

			using (var session = DataAccessSystem.OpenSession())
			{
				#region Fill register

				var kipReg = session.CreateQuery("select r FROM KipRegister r where r.DocNum = :docnum")
									.SetParameter("docnum", docNumber).SetMaxResults(1).List<KipRegisterHib>().FirstOrDefault();
				if (kipReg == null)
				{
					retVal.Error = $"Реестр ПТК КИП с номером '{docNumber}' не найден";
					return retVal;
				}
				if (!IsKIPRegisterExportReady(session, kipReg))
				{
					retVal.Error = $"Реестр ПТК КИП с номером '{docNumber}' не готов к экспорту, так как не все платежи проведены";
					return retVal;
				}

			    var doc = new KIP_Export
			    {
			        Info =
			        {
			            Date = DateTime.Now,
			            DocNumber = $"{kipReg.ID}_{kipReg.DocNum}",
			            SourceDocument =
			            {
			                Date = kipReg.DocDate,
			                DocNumber = kipReg.DocNum,
			                Type = KIP_Import.GetDocTypeText(kipReg.DocType)
			            },
			            Period = kipReg.Period,
			            PeriodType = kipReg.PeriodType
			        }
			    };

			    kipReg.ExportDocNum = doc.Info.DocNumber;//Сохраняем номер экспортируемого документа, для будущего приёма квитанций

				switch (kipReg.DocType)
				{
					case KIP_Import.TRANS_MSK:
						doc.Info.DocumentType.Code = KIP_Export.TRANS_MSK;
						doc.Info.DocumentType.Name = KIP_Export.TRANS_MSK_TEXT;
						break;
					case KIP_Import.RECALL_MSK:
						doc.Info.DocumentType.Code = KIP_Export.RECALL_MSK;
						doc.Info.DocumentType.Name = KIP_Export.RECALL_MSK_TEXT;
						break;
				}
				#endregion
				//Заполняем списки перечислений
				//SI
				#region  SI
				if (kipReg.SIRegisterID.HasValue)
				{
					var reg = session.Get<SIRegisterHib>(kipReg.SIRegisterID.Value);
					foreach (var str in reg.TransferLists)
					{
						var ba = str.Contract.BankAccount;
						var link = kipReg.Links.FirstOrDefault(l => l.SITransferID == str.ID) ?? new KipLinksHib();
					    var investDohod = Math.Round(str.Transfers.Sum(tr => tr.InvestmentIncome) ?? 0, 2, MidpointRounding.AwayFromZero);
						var item = new KIP_Export.RegisterItem
						{
							Organization = new KIP_Export.RegisterItem.OrganizationInfo
							{
								Code = link.OrganizationCode,
								PortfolioCode = link.PortfolioCode,
								OrgType = "УК"
							},
							Summ = new KIP_Export.RegisterItem.SummInfo
							{
                                MSK = Math.Round(str.Transfers.Sum(tr => tr.Sum) ?? 0, 2, MidpointRounding.AwayFromZero) - investDohod,
                                MSKInvest = investDohod
							},
							Reciver = new KIP_Export.RegisterItem.ReciverInfo
							{
								BankName = ba.BankName.ToUpper(),
								BIK = ba.BIK,
								CorrAccount = ba.CorrespondentAccountNumber,
								INN = ba.INN,
								KPP = ba.KPP,
								OperatingAccount = ba.AccountNumber
							}
						};
						item.Summ.RecalcTotal();

						var listPP = str.Transfers.SelectMany(tr => tr.CommonPPList.Select(pp => new KIP_Export.RegisterItem.PPInfo
						{
						    Date = pp.DraftDate ?? DateTime.MinValue, Number = pp.DraftRegNum.ToUpper()
						})).ToList();
						item.PPList = listPP;

						doc.Items.Add(item);
					}
				}
				#endregion  SI

				//VR
				#region  VR
				if (kipReg.VRRegisterID.HasValue)
				{
					var reg = session.Get<SIRegisterHib>(kipReg.VRRegisterID.Value);
					foreach (var str in reg.TransferLists)
					{
						var ba = str.Contract.BankAccount;
						var link = kipReg.Links.FirstOrDefault(l => l.SITransferID == str.ID) ?? new KipLinksHib();
					    var investDohod = Math.Round(str.Transfers.Sum(tr => tr.InvestmentIncome) ?? 0, 2, MidpointRounding.AwayFromZero);
						var item = new KIP_Export.RegisterItem
						{
							Organization = new KIP_Export.RegisterItem.OrganizationInfo
							{
								Code = link.OrganizationCode,
								PortfolioCode = link.PortfolioCode,
								OrgType = "УК"
							},
							Summ = new KIP_Export.RegisterItem.SummInfo
							{
                                MSK = Math.Round(str.Transfers.Sum(tr => tr.Sum) ?? 0, 2, MidpointRounding.AwayFromZero) - investDohod,
                                MSKInvest = investDohod
							},
							Reciver = new KIP_Export.RegisterItem.ReciverInfo
							{
								BankName = ba.BankName.ToUpper(),
								BIK = ba.BIK,
								CorrAccount = ba.CorrespondentAccountNumber,
								INN = ba.INN,
								KPP = ba.KPP,
								OperatingAccount = ba.AccountNumber
							}
						};
						item.Summ.RecalcTotal();
						var listPP = str.Transfers.SelectMany(tr => tr.CommonPPList.Select(pp => new KIP_Export.RegisterItem.PPInfo { Date = pp.DraftDate ?? DateTime.MinValue, Number = pp.DraftRegNum.ToUpper() })).ToList();
						item.PPList = listPP;
						doc.Items.Add(item);
					}
				}
				#endregion  VR

				//NPF
				#region NPF
				if (kipReg.NPFRegisterID.HasValue)
				{
					var reg = session.Get<RegisterHib>(kipReg.NPFRegisterID.Value);
					foreach (var fr in reg.FinregistersList)
					{
						var acc = fr.CreditAccount.Contragent.TypeName == "ПФР" ? fr.DebitAccount : fr.CreditAccount;
						var le = acc.Contragent.LegalEntities.First();
						var ba = session.CreateQuery("select ba from BankAccount ba where ba.CloseDate is null AND ba.LegalEntityID = :leID")
										.SetParameter("leID", le.ID).SetMaxResults(1).List<BankAccount>().First();
						var link = kipReg.Links.FirstOrDefault(l => l.FinregisterID == fr.ID) ?? new KipLinksHib();

						var item = new KIP_Export.RegisterItem
						{
							Organization = new KIP_Export.RegisterItem.OrganizationInfo
							{ 
								Code = link.OrganizationCode,
								PortfolioCode = link.PortfolioCode,
								OrgType = "НПФ" 
							},								
							Summ = new KIP_Export.RegisterItem.SummInfo
							{
								MSK = Math.Round(fr.Count ?? 0, 2, MidpointRounding.AwayFromZero),
								MSKInvest = 0.00m
							},
							Reciver = new KIP_Export.RegisterItem.ReciverInfo
							{
								BankName = ba.BankName.ToUpper(),
								BIK = ba.BIK,
								CorrAccount = ba.CorrespondentAccountNumber,
								INN = ba.INN,
								KPP = ba.KPP,
								OperatingAccount = ba.AccountNumber
							}
						};

						//Для временного размещения
						if (item.Organization.Code == "ПФР")
						{
							item.Organization.OrgType = "УК";
						    if (fr.Date != null)
						    {
						        var listPP = new List<KIP_Export.RegisterItem.PPInfo> { new KIP_Export.RegisterItem.PPInfo { Date = fr.Date.Value, Number = $"ПФР-{reg.ID}".ToUpper() } };
						        item.PPList = listPP;
						    }
						}
						else
						{
							var listPP = fr.DraftsList.Select(pp => new KIP_Export.RegisterItem.PPInfo { Date = pp.DraftDate ?? DateTime.MinValue, Number = pp.DraftRegNum }).ToList();
							item.PPList = listPP;
						}

						item.Summ.RecalcTotal();
						doc.Items.Add(item);
					}
				}
				#endregion  NPF

				doc.RecalcFields();

				var ms = new MemoryStream();
				var settings = new XmlWriterSettings
				{
					Encoding = Encoding.GetEncoding("Windows-1251"),
					Indent = true
				};
				var writer = XmlWriter.Create(ms, settings);
				var serializer = new XmlSerializer(typeof(KIP_Export));
				serializer.Serialize(writer, doc);

				retVal.FileName = $"{doc.Info.DocumentType.Code}_{doc.Info.SourceDocument.DocNumber}.xml";
				retVal.FileBodyBinary = ms.ToArray();

				//Сохраняем в хранилище результат и ссылку в реестр КИП

				using (var transation = session.BeginTransaction())
				{
					//сохраняем файл в базу для логирования
					var dt = DateTime.Now;
					var fileRepository = new RepositoryNoAucImpExpFile
					{
						Repository = GZipCompressor.Compress(retVal.FileBodyBinary),
						ImpExp = 1,
						DDate = dt,
						Status = 1,
						TTime = dt.TimeOfDay,
						UserName = DomainUser,
						Comment = $"Экспорт XML файла ПТК КИП '{retVal.FileName}'"
					};
					session.SaveOrUpdate(fileRepository);
					session.Flush();

					kipReg.ExportFileID = fileRepository.ID;
					//Статус изменяем теперь отдельным вызовом, после подтверждения 
					session.SaveOrUpdate(kipReg);
					session.Flush();
					transation.Commit();
				}

				if (onlyServerExport)
				{
					//Серверный экспортп
					var expSetting = session.Get<SettingsPathFile>((long)SettingsPathFile.IdDefinition.PkipExport);
					if (expSetting != null && expSetting.ServClientValue == SettingsPathFile.FileLocation.Server)
					{
						if (!Directory.Exists(expSetting.Path))
						{
							retVal.Error = $"На сервере не найдена папка для импорта '{expSetting.Path}'";
						}
						SaveXML(retVal, expSetting.Path);
					}
					//Очищаем, что бы не гнать на клиента при серверном экспорте
					retVal.FileBodyBinary = new byte[0];
				}
			}
			return retVal;
		}

		public void SetKIPRegisterExportStatus(string docNumber, KipRegister.ExportStatuses status)
		{
			using (var session = DataAccessSystem.OpenSession())
			{
				var kipReg = session.CreateQuery("select r FROM KipRegister r where r.DocNum = :docnum")
									.SetParameter("docnum", docNumber).SetMaxResults(1).List<KipRegister>().FirstOrDefault();
				if (kipReg == null)
					return;
				kipReg.ExportStatus = status;
				session.Save(kipReg);
				session.Flush();
			}
		}

	    public KIPSettings GetKIPSettings()
	    {
#if WEBCLIENT
	        return null;
#else
            return ServerEnvironment.LoadServerConfig();
#endif
	    }
	}
}
