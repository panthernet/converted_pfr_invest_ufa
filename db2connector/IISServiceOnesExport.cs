﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Schema;
using NHibernate;
using NHibernate.Linq;
using PFR_INVEST.Common.MessageCompressor;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Constants.Identifiers;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.DataObjects.XMLModel;
using PFR_INVEST.Proxy.Tools;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector
{
    public partial class IISServiceHib
    {
#if WEBCLIENT
        private const string ONES_EXPORT_XSD = @"Database/XSD/Scheme_1C.xsd";
#else
        private const string ONES_EXPORT_XSD = @"XSD/Scheme_1C.xsd";
#endif
        private readonly bool _onesDebugNotWriteUsedExportIds = false;
        private XmlSchemaSet _onesExportSchema;

        private List<OnesOpSettings> _onesOpSettings;
        private List<OnesSettingsDef> _onesOpDefs;
        private static readonly object OnesExportStateLocker = new object();

        public List<object> StartOnesExport(string exportPath, bool isClient, OnesSettingsDef.IntGroup type, List<long> idList = null)
        {
            var resultList = new List<object>();
            try
            {
                //server check
                if (!isClient && !Directory.Exists(exportPath))
                {
                    try
                    {
                        Directory.CreateDirectory(exportPath);
                    }
                    catch
                    {
                        resultList.Add(WebServiceDataError.DirectoryNotFound);
                        return resultList;
                    }
                }
                //ищем активную сессию экспорта
                lock (OnesExportStateLocker)
                {
                    if (OnesExportStates[(int)type])
                    {
                        resultList.Add(WebServiceDataError.SessionInProgress);
                        return resultList;
                    }
                }
                SetSessionProgess(type, true);

                //создаем новую сессию
                long sessionId = 0;
                TransactionWrapper(session =>
                {
                    sessionId = SaveEntity(new OnesSession { SessionDate = DateTime.Now, IsImport = 0 }, session);
                    _onesOpSettings = GetList<OnesOpSettings>().Cast<OnesOpSettings>().ToList();
                    _onesOpDefs = GetList<OnesSettingsDef>().Cast<OnesSettingsDef>().ToList();

                }, IsolationLevel.ReadUncommitted);

                SaveJornalLog(JournalEventType.EXPORT_DATA, $"Операция экспорта данных для 1С в ПК ИБиБУ ПФР. Сессия ID={sessionId}", 0, "");

                ThreadPool.QueueUserWorkItem(state =>
                {
                    try
                    {
                        if (_onesExportSchema == null)
                        {
                            var xsdPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ONES_EXPORT_XSD);
                            if (!File.Exists(xsdPath))
                            {
                                OnesLogJournal("Не найдена XSD схема для импорта/экспорта данных 1С!", sessionId, OnesJournal.Actions.Export, OnesJournal.LogTypes.Error);
                                return;
                            }
                            using (var xsdReader = new StreamReader(xsdPath))
                            {
                                var xmlSchema = XmlSchema.Read(xsdReader, (sender, eventArgs) =>
                                {
                                });
                                var xmlSchemaSet = new XmlSchemaSet();
                                xmlSchemaSet.Add(xmlSchema);
                                xmlSchemaSet.Compile();
                                _onesExportSchema = xmlSchemaSet;
                            }
                        }
                        var exportDate = DateTime.Now;
                        var exportType = OnesFileExport.OnesExportType.Finregister;
                        switch (type)
                        {
                            case OnesSettingsDef.IntGroup.All:
                                OnesLogJournal("Запущена операция экспорта файлов 1С для всех разделов", sessionId);
                                OnesGenerateExport(OnesFileExport.OnesExportType.Finregister, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(OnesFileExport.OnesExportType.ReqTransfer, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(OnesFileExport.OnesExportType.ReqTransferVr, exportDate, sessionId, isClient, exportPath, idList);
                                OnesGenerateExport(OnesFileExport.OnesExportType.Deposit, exportDate, sessionId, isClient, exportPath, idList);
                                break;
                            case OnesSettingsDef.IntGroup.Npf:
                                exportType = OnesFileExport.OnesExportType.Finregister;
                                break;
                            case OnesSettingsDef.IntGroup.Si:
                                exportType = OnesFileExport.OnesExportType.ReqTransfer;
                                break;
                            case OnesSettingsDef.IntGroup.Vr:
                                exportType = OnesFileExport.OnesExportType.ReqTransferVr;
                                break;
                            case OnesSettingsDef.IntGroup.Depo:
                                exportType = OnesFileExport.OnesExportType.Deposit;
                                break;
                            case OnesSettingsDef.IntGroup.Opfr:
                                exportType = OnesFileExport.OnesExportType.Opfr;
                                break;
                            default:
                                throw new Exception("Неизвестный тип экспорта 1С!");
                        }
                        if (type == OnesSettingsDef.IntGroup.All)
                            OnesLogJournal("Завершена операция экспорта файлов 1С для всех разделов", sessionId);
                        else
                        {
                            OnesLogJournal("Запущена операция экспорта файлов 1С для раздела " + GetSessionTypeName(exportType), sessionId);
                            OnesGenerateExport(exportType, exportDate, sessionId, isClient, exportPath, idList);
                            OnesLogJournal("Завершена операция экспорта файлов 1С для раздела " + GetSessionTypeName(exportType), sessionId);
                        }

                    }
                    catch (Exception ex)
                    {
                        LogManager.LogException(ex);
                    }
                    finally
                    {
                        SetSessionProgess(type, false);
                        //сохраняем статус сессии
                        _onesOpSettings = null;
                        _onesOpDefs = null;
                    }
                });
                return resultList;
            }
            catch (Exception ex)
            {
                SetSessionProgess(type, false);
                LogManager.LogException(ex);
                resultList.Add(WebServiceDataError.GeneralException);
                return resultList;
            }
        }

        private void SetSessionProgess(OnesSettingsDef.IntGroup type, bool isEnabled)
        {
            lock (OnesExportStateLocker)
            {
                OnesExportStates[(int)type] = isEnabled;
            }
        }

        /// <summary>
        /// Возвращается сгенерированные файлы экспорта 1С пользователю, если таковые имеются
        /// </summary>
        public List<RepositoryNoAucImpExpFile> OnesGetGeneratedExportFiles()
        {
            return TransactionWrapper(session =>
            {
                const int key = (int)RepositoryImpExpFile.Keys.OnesExportXml;
                //выбираем записи для передачи клиенту
                var result = session.CreateQuery("from RepositoryNoAucImpExpFile s where s.UserName = :name and s.Status=0 and s.Key=:key")
                    .SetParameter("name", DomainUser)
                    .SetParameter("key", key)
                    .List<RepositoryNoAucImpExpFile>()
                    .ToList();
                //обновляем состояние записи
                if (result.Count > 0)
                    session.CreateQuery(@"update RepositoryNoAucImpExpFile s set s.Status=1 where s.ID in (:idlist) and s.Key=:key ")
                        .SetParameter("key", key)
                        .SetParameterList("idlist", result.Select(a => a.ID).ToArray())
                    .ExecuteUpdate();
                return result;
            }, IsolationLevel.ReadUncommitted);

        }


        private static string GetSessionTypeName(OnesFileExport.OnesExportType type)
        {
            switch (type)
            {
                case OnesFileExport.OnesExportType.ReqTransfer:
                    return "СИ";
                case OnesFileExport.OnesExportType.ReqTransferVr:
                    return "ВР";
                case OnesFileExport.OnesExportType.Finregister:
                    return "НПФ";
                case OnesFileExport.OnesExportType.Deposit:
                    return "Депозиты";
                case OnesFileExport.OnesExportType.Opfr:
                    return "Бэк-офис/Расчеты с ОПФР";
                default: throw new Exception("Неизвестный тип экспорта!");
            }
        }

        private void OnesGenerateExport(OnesFileExport.OnesExportType type, DateTime exportDate, long sessionId, bool isClient, string exportPath, List<long> inputIdList)
        {
            string partName;
            switch (type)
            {
                case OnesFileExport.OnesExportType.Finregister:
                    partName = " НПФ";
                    break;
                case OnesFileExport.OnesExportType.ReqTransfer:
                case OnesFileExport.OnesExportType.ReqTransferVr:
                    partName = "СИ/ВР";
                    break;
                case OnesFileExport.OnesExportType.Deposit:
                    partName = "Депозитов";
                    break;
                case OnesFileExport.OnesExportType.Opfr:
                    partName = " Распоряжений";
                    break;
                default: throw new Exception("unk type");
            }

            try
            {
                List<object> lResult;
                var resultList = new List<OnesExportDocReqTransfer>();
                var intGroup = 4;
                TransactionWrapper(session =>
                {
                    switch (type)
                    {
                        case OnesFileExport.OnesExportType.Finregister:
                            resultList = OnesGenerateNPFExport(exportDate, session, inputIdList, out lResult);
                            intGroup = (int)OnesSettingsDef.IntGroup.Npf;
                            break;
                        case OnesFileExport.OnesExportType.ReqTransfer:
                            resultList = OnesGenerateSIExport(exportDate, session, false, inputIdList, out lResult);
                            intGroup = (int)OnesSettingsDef.IntGroup.Si;
                            break;
                        case OnesFileExport.OnesExportType.ReqTransferVr:
                            resultList = OnesGenerateSIExport(exportDate, session, true, inputIdList, out lResult);
                            intGroup = (int)OnesSettingsDef.IntGroup.Vr;
                            break;
                        case OnesFileExport.OnesExportType.Deposit:
                            resultList = OnesGenerateDepositExport(exportDate, session, inputIdList, out lResult);
                            intGroup = (int)OnesSettingsDef.IntGroup.Depo;
                            break;
                        case OnesFileExport.OnesExportType.Opfr:
                            resultList = OnesGenerateOpfrExport(exportDate, session, inputIdList, out lResult);
                            intGroup = (int)OnesSettingsDef.IntGroup.Opfr;
                            break;
                        default:
                            intGroup = (int)OnesSettingsDef.IntGroup.All;
                            return;
                    }

                    lResult.ForEach(a => OnesLogJournal(a.ToString(), sessionId, OnesJournal.Actions.Export, OnesJournal.LogTypes.Error, null, session));

                }, IsolationLevel.ReadUncommitted);

                var bodyList = new List<ImportFileListItem>();
                string typeName = GetSessionTypeName(type);
                lock (OnesExportStateLocker) { _onesExportMaxCounts[intGroup] = resultList.Sum(a => a.Payments.Count); }


                resultList.ForEach(result =>
                {
                    //получили result можно генерить XML
                    List<string> errorsList;
                    var resultFile = OnesGenerateExportReqTransferXML(result, out errorsList);
                    //обработка результатов идет отдельной транзакцией для каждого файла, чтобы исключить только ошибочные
                    TransactionWrapper(session =>
                    {
                        if (resultFile == null)
                        {
                            //логируем ошибки генерации XML
                            OnesLogJournal(string.Format(@"Ошибка генерации XML файла для '{0}'({3}) на дату '{1}': {2}", result.Type, result.Date, string.Join("|\n", errorsList.ToArray()), partName), sessionId,
                                OnesJournal.Actions.Export, OnesJournal.LogTypes.Error, null, session);
                        }
                        else
                        {
                            try
                            {
                                var id =
                                    SaveEntity(
                                        new RepositoryNoAucImpExpFile
                                        {
                                            Repository = resultFile.FileContent,
                                            Key = (int)RepositoryImpExpFile.Keys.OnesExportXml,
                                            UserName = DomainUser,
                                            DDate = DateTime.Now.Date,
                                            TTime = DateTime.Now.TimeOfDay,
                                            Comment = "-",
                                            ImpExp = 1,
                                            Status = !isClient ? 1 : 0
                                        }, session);
                                //помечаем данные, как успешно экспортированные
                                var idList = result.Payments.Where(a => a.ID != 0).Select(a => a.ID).ToList();
                                result.Payments.Where(a => a.OPFR_IDLIST != null).ForEach(a => idList.AddRange(a.OPFR_IDLIST));
                                idList = idList.Distinct().ToList();

                                if (!_onesDebugNotWriteUsedExportIds)
                                    OnesSaveExportEntries(idList, type, sessionId, session, id);
                                OnesLogJournal(@"Экспортирован файл 1С для раздела " + typeName, sessionId, OnesJournal.Actions.ExportComplete, OnesJournal.LogTypes.Info, id, session);
                                if (!isClient)
                                    bodyList.Add(resultFile);
                            }
                            catch (Exception ex)
                            {
                                LogManager.LogException(ex);
                                OnesLogJournal(string.Format(@"Oшибка сохранения XML файла для {0} ! ОШИБКА: " + ex, partName), sessionId, OnesJournal.Actions.Export, OnesJournal.LogTypes.Error, null, session);
                            }
                        }
                    });
                    lock (OnesExportStateLocker)
                    {
                        _onesExportCounts[intGroup] += result.Payments.Count;
                    }
                });

                //записываем файлы на сервер, если требуется
                if (!isClient && bodyList.Count > 0)
                {
                    var count = 0;
                    //вычисляем порядковый номер в рамках дня
                    //АНАЛОГИЧНАЯ ЛОГИКА В OnesExecuteExportWrapper
                    Directory.GetFiles(exportPath, "docip_*.xml").ToList().ForEach(a =>
                    {
                        var array = Path.GetFileNameWithoutExtension(a)?.Split('_');
                        var src = array?[2];
                        if (DateTime.ParseExact(src, "yyMMdd", new DateTimeFormatInfo()).Date == DateTime.Now.Date)
                        {
                            var nCount = Convert.ToInt32(array?[4]);
                            count = nCount > count ? nCount : count;
                        }
                    });

                    bodyList.ForEach(a =>
                    {
                        if (a.FileContent == null || a.FileContent.Length == 0) return;
                        var file = Path.Combine(exportPath, $"docip_RequestForTransfer_{DateTime.Now:yyMMdd_hhmmss}_{++count}.xml");
                        using (var writer = File.CreateText(file))
                        {
                            writer.Write(Encoding.Default.GetChars(GZipCompressor.Decompress(a.FileContent)));
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                LogManager.LogException(ex);
                //логируем ошибки генерации XML
                SessionWrapper(session =>
                    OnesLogJournal($@"Критическая ошибка генерации XML файлов для {partName} ! ОШИБКА: " + ex, sessionId, OnesJournal.Actions.Export, OnesJournal.LogTypes.Error, null, session));
            }
        }

        private Dictionary<string, T[]> OnesGenerateOpPacks<T>(string exportGroup, ISession session)
        {
            var opPacks = new Dictionary<string, T[]>();
            var egEntries = _onesOpDefs.Where(a => a.ExportGroup == exportGroup).ToList();
            //хак для ВР
            if (exportGroup == OnesSettingsDef.Groups.VR)
                egEntries = _onesOpDefs.Where(a => a.ExportGroup == OnesSettingsDef.Groups.UK).ToList();
            var egIds = egEntries.Select(a => a.ID).ToList();
            var opEntries = _onesOpSettings.Where(a => egIds.Contains(a.SetDefID));

            Type type;
            switch (exportGroup)
            {
                case OnesSettingsDef.Groups.NPF:
                case OnesSettingsDef.Groups.DEPOSIT:
                    type = typeof(Element);
                    break;
                case OnesSettingsDef.Groups.UK:
                case OnesSettingsDef.Groups.VR:
                    type = typeof(SPNOperation);
                    break;
                default:
                    throw new Exception("Неизвестный тип операции!");
            }

            var els = GetListByPropertyConditions(type, session, new List<ListPropertyCondition>
            {
                ListPropertyCondition.In("ID", opEntries.Select(a => exportGroup == OnesSettingsDef.Groups.DEPOSIT ? a.PortfolioTypeID : a.OperationContentID)
                    .Cast<object>()
                    .ToArray())
            });

            egEntries.ForEach(a =>
            {
                var groupElementsId = _onesOpSettings.Where(b => egIds.Contains(b.SetDefID) && b.SetDefID == a.ID)
                    .Select(b => exportGroup == OnesSettingsDef.Groups.DEPOSIT ? b.PortfolioTypeID : b.OperationContentID)
                    .ToList();
                switch (exportGroup)
                {
                    case OnesSettingsDef.Groups.NPF:
                    case OnesSettingsDef.Groups.DEPOSIT:
                        var list = els.Cast<Element>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list.Any()) return;
                        opPacks.Add(a.Name, list.Cast<T>().ToArray());
                        break;
                    case OnesSettingsDef.Groups.UK:
                    case OnesSettingsDef.Groups.VR:
                        var list2 = els.Cast<SPNOperation>().Where(b => groupElementsId.Contains(b.ID)).ToList();
                        if (!list2.Any()) return;
                        opPacks.Add(a.Name, list2.Cast<T>().ToArray());
                        break;
                    default:
                        throw new Exception("Неизвестный тип операции!");
                }
            });
            return opPacks;
        }

        private static List<PaymentAssignment> GetPayAssList(ISession session, long partId, long dirId)
        {
            return session.CreateQuery(@"from PaymentAssignment p where p.PartID=:partId and p.DirectionID=:dirId")
                .SetParameter("partId", partId)
                .SetParameter("dirId", dirId)
                .List<PaymentAssignment>()
                .ToList();
        }

        private static PaymentAssignment GetPayAssFromList(IEnumerable<PaymentAssignment> payAssList, long opContentId)
        {
            var item = payAssList.Where(a => a.OperationContentID == opContentId || a.OperationContentID == 0).OrderByDescending(a => a.ID).FirstOrDefault();
            return item;
        }

#region Генерация экспорта финреестров (НПФ)

        private List<OnesExportDocReqTransfer> OnesGenerateNPFExport(DateTime exportDate, ISession session, ICollection inputIdList, out List<object> midResult)
        {
            midResult = new List<object>();
            var resList = new List<OnesExportDocReqTransfer>();
            var packs = OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.NPF, session);
            packs.ForEach(pack =>
            {
                var list = SessionWrapper(ses => ses.CreateQuery($@"select distinct fr, le, bank, r.Content, r.TrancheDate, r.TrancheNumber, r.PayAssignment {
                            string.Format(ONES_FROM_NPF_QUERY, "", "left join fetch le.Licensies lic")
                        }  and fr.ID in (:inputIdList)")
                    .SetParameterList("list", pack.Value.Select(a => a.Name).ToList())
                    .SetParameterList("inputIdList", inputIdList)
                    .SetParameter("type", (int)OnesFileExport.OnesExportType.Finregister)
                    .SetParameterList("status", new object[] { RegisterIdentifier.FinregisterStatuses.Transferred, RegisterIdentifier.FinregisterStatuses.NotTransferred })
                    .List<object[]>()
                    .ToList());
                var grouppedList = list.GroupBy(a => new { ((DateTime?)a[4]).Value.Date, new StringWrapper(string.IsNullOrEmpty((string)a[5]) ? "1" : a[5]).Value });
                grouppedList.ForEach(group =>
                {
                    var result = new OnesExportDocReqTransfer
                    {
                        WorkDate = exportDate,
                        //дата заявки
                        Date = group.Key.Date,
                        //Номер заявки
                        Number = group.Key.Value,
                        //Тип передаваемых сведений 
                        Type = pack.Key
                    };

                    //обработка финреестров для Payments
                    group.ToList().ForEach(fro =>
                    {
                        var fr = fro[0] as FinregisterHib;
                        //var frId = (long) fro[1];
                        var le = (LegalEntityHib)fro[1];
                        var lic = le.Licensies?.OrderByDescending(a => a.CloseDate).FirstOrDefault();

                        var oName = le.FullName;//(string) fro[1];
                        var oShortName = le.ShortName;//(string) fro[2];
                        var bank = fro[2] as BankAccount;
                        //var opContent = (string)fro[3];
                        var payAss = (string)fro[6];

                        var payment = new OnesExportDocReqTransferPayment
                        {
                            ID = fr.ID,
                            Organization_Rcp =
                            {
                                Name = oName,
                                ShortName = oShortName,
                                INN = le.INN,
                                KPP = le.OKPP
                            },
                            Account_Rcp =
                            {
                                BIC = bank.BIK,
                                BS = bank.AccountNumber,
                                BS_KS = bank.CorrespondentAccountNumber,
                                Name_BIC = bank.BankName
                            },
                            AgreementDate = lic?.RegistrationDate,
                            AgreementNumber = lic?.Number,
                            DepositNumber = null,
                            Purpose = payAss,
                            Sum_payment = fr.Count ?? 0
                        };

                        payment.Sum_PP.Add(new OnesExportDocReqTransferSumPP
                        {
                            Sum = fr.Count ?? 0,
                            Bag = null,
                            KBK = null,
                            Type_KBK = null
                        });
                        result.Payments.Add(payment);
                    });
                    resList.Add(result);
                });
            });
            return resList;
        }
#endregion

#region Генерация экспорта перечислений (СИ/ВР)
        private List<OnesExportDocReqTransfer> OnesGenerateSIExport(DateTime exportDate, ISession session, bool isVr, List<long> inputIdList, out List<object> midResult)
        {
            midResult = new List<object>();

            var resList = new List<OnesExportDocReqTransfer>();
            var payAssList = GetPayAssList(session, isVr ? (long)Element.SpecialDictionaryItems.AppPartVR : (long)Element.SpecialDictionaryItems.AppPartSI, (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR);
            Console.WriteLine(payAssList);

            var packs = OnesGenerateOpPacks<SPNOperation>(isVr ? OnesSettingsDef.Groups.VR : OnesSettingsDef.Groups.UK, session);

            packs.ForEach(pack =>
            {
                //пробуем выполнить запрос вне транзакции, чтобы не лочить для внешних обращений
                var list = SessionWrapper(ses => ses.CreateQuery($@"select distinct rq, le.FullName, le.ShortName, bank, reg.OperationID,
                                                        c.ContractDate, c.ContractNumber, reg.CompanyYearID, reg.CompanyMonthID, c.PortfolioName, kbk.Code, le.INN, le.OKPP, reg.PayAssignment {
                            string.Format(ONES_FROM_SIVR_QUERY, "left join rq.KBK kbk")
                        } and rq.ID in (:inputIdList)")
                    .SetParameterList("opList", pack.Value.Select(a => a.ID).ToList())
                    .SetParameterList("inputIdList", inputIdList)
                    .SetParameter("direction", !isVr ? (long)SIRegister.Directions.ToUK : (long)SIRegister.Directions.ToGUK)
                    .SetParameter("type", !isVr ? (int)OnesFileExport.OnesExportType.ReqTransfer : (int)OnesFileExport.OnesExportType.ReqTransferVr)
                    .List<object[]>()
                    .ToList());
                var grouppedList = list.GroupBy(a => new { ((ReqTransfer)a[0]).Date, ((ReqTransfer)a[0]).GroupClaimNumber });
                grouppedList.ForEach(group =>
                {
                    var result = new OnesExportDocReqTransfer
                    {
                        WorkDate = exportDate,
                        //дата заявки
                        Date = group.Key.Date,
                        //Номер заявки
                        Number = group.Key.GroupClaimNumber,
                        //Тип передаваемых сведений 
                        Type = pack.Key
                    };

                    //обработка перечислений для Payments
                    group.ToList().ForEach(rqi =>
                    {
                        var rq = rqi[0] as ReqTransferHib;
                        var oName = (string)rqi[1];
                        var oShortName = (string)rqi[2];
                        var bank = rqi[3] as BankAccount;

                        var opContentId = (long)rqi[4];

                        var cDate = (DateTime?)rqi[5];
                        var cNum = (string)rqi[6];
                        var cYear = ((long?) rqi[7] + 2000)?.ToString();
                        var cMonth = rqi[8] == null ? null : new CultureInfo("ru-RU").DateTimeFormat.MonthNames[(long)rqi[8] - 1];
                        var cPortfolioName = (string)rqi[9];
                        var cKbk = (string)rqi[10];
                        var ukINN = (string)rqi[11];
                        var ukKPP = (string)rqi[12];

                        var payAss = GetPayAssFromList(payAssList, pack.Value.First(a => a.ID == opContentId).ID);
                        var payAssString = "";

                        if (!string.IsNullOrEmpty((string)rqi[13]))
                            payAssString = (string)rqi[13];
                        else
                        {
                            payAssString =
                                PaymentAssignmentHelper.ReplaceTagDataForSIVR(payAss == null ? "" : payAss.Name, cDate,
                                    cNum, cYear, cMonth);
                        }

                        var payment = new OnesExportDocReqTransferPayment
                        {
                            ID = rq.ID,
                            Organization_Rcp =
                            {
                                Name = oName,
                                ShortName = oShortName,
                                INN = ukINN,
                                KPP = ukKPP
                            },
                            Account_Rcp =
                            {
                                BIC = bank.BIK,
                                BS = bank.AccountNumber,
                                BS_KS = bank.CorrespondentAccountNumber,
                                Name_BIC = bank.BankName
                            },
                            AgreementDate = cDate,
                            AgreementNumber = cNum,
                            DepositNumber = null,

                            Purpose = payAssString,
                            Sum_payment = rq.Sum ?? 0 //ppList.Sum(a => a.DraftAmount ?? 0)
                        };
                        payment.Sum_PP.Add(new OnesExportDocReqTransferSumPP
                        {
                            Sum = rq.Sum ?? 0,
                            Bag = cPortfolioName,
                            KBK = cKbk,
                            Type_KBK = null
                        });
                        result.Payments.Add(payment);
                    });
                    resList.Add(result);
                });
            });
            return resList;
        }
#endregion

#region Генерация экспорта перечислений (Депозиты)

        /// <summary>
        /// Обманка для пропихивания занчения свойства в анонимный класс по мемберу Value
        /// </summary>
        private class StringWrapper
        {
            public StringWrapper(object data)
            {
                _value = data;
            }
            private readonly object _value;
            public string Value => _value?.ToString() ?? "";
        }

        private List<OnesExportDocReqTransfer> OnesGenerateDepositExport(DateTime exportDate, ISession session, List<long> inputIdList, out List<object> midResult)
        {
            midResult = new List<object>();
            var payAssList = GetPayAssList(session, (long)Element.SpecialDictionaryItems.AppPartDEPO, (long)Element.SpecialDictionaryItems.CommonTransferDirectionFromPFR);
            var offerStatusList = new List<int>
            {
                (int)DepClaimSelectParams.Statuses.OfferAcceptedOrNo,
                (int)DepClaimSelectParams.Statuses.DepositSettled
            };

            var resList = new List<OnesExportDocReqTransfer>();
            var packs = OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.DEPOSIT, session);
            packs.ForEach(pack =>
            {
                var list = SessionWrapper(ses => ses.CreateQuery(
                        $@"select offer, le, acc.Sum, pf.TypeID, ba.AccountNumber, auc.AuctionNum {ONES_FROM_DEPO_QUERY}  and offer.ID in (:inputIdList)")
                    .SetParameterList("statList", offerStatusList)
                    .SetParameterList("idList", pack.Value.Select(a => a.ID).ToList())
                    .SetParameterList("badStatusList", new[] { (int)DepClaimOffer.Statuses.NotAccepted })
                    .SetParameterList("inputIdList", inputIdList)
                    .SetParameter("type", (int)OnesFileExport.OnesExportType.Deposit).List<object[]>()
                    .ToList());
                //группировка данных при помощи анонимного класса
                var grouppedList = list.GroupBy(a => new { ((DepClaimOffer)a[0]).GroupClaimDate, ((DepClaimOffer)a[0]).GroupClaimNumber, new StringWrapper(a[5]).Value });
                grouppedList.ForEach(group =>
                {
                    var result = new OnesExportDocReqTransfer
                    {
                        WorkDate = exportDate,
                        //дата заявки
                        Date = group.Key.GroupClaimDate,
                        //Номер заявки
                        Number = group.Key.GroupClaimNumber,
                        //Тип передаваемых сведений 
                        Type = pack.Key
                    };

                    //обработка оферт для Payments
                    group.ToList().ForEach(ofi =>
                    {
                        var offer = ofi[0] as DepClaimOfferHib;
                        var le = (LegalEntity)ofi[1];

                        var sum = (decimal?)ofi[2];
                        var pfId = (long)ofi[3];
                        var payAss = GetPayAssFromList(payAssList, pack.Value.First(a => a.ID == pfId).ID);
                        var agreement = le.PFRAGR;
                        var agreementDate = le.PFRAGRDATE;
                        var accNum = (string)ofi[4];
                        // var ppList = rq.CommonExportPPList  //fetch join!
                        var payment = new OnesExportDocReqTransferPayment
                        {
                            ID = offer.ID,
                            Organization_Rcp =
                            {
                                Name = le.FullName,
                                ShortName = le.ShortName,
                                INN = le.INN,
                                KPP = le.OKPP
                            },
                            Account_Rcp =
                            {
                                BIC = le.BIK,
                                BS = accNum,
                                BS_KS = le.CorrAcc,Name_BIC = le.ShortName
                            },
                            AgreementDate = agreementDate,
                            AgreementNumber = agreement,
                            DepositNumber = $"№{offer.Number}",
                            Purpose = payAss == null ? "" : payAss.Name,
                            Sum_payment = sum ?? 0
                        };
                        payment.Sum_PP.Add(new OnesExportDocReqTransferSumPP
                        {
                            Sum = sum ?? 0,
                            Bag = null,
                            KBK = null,
                            Type_KBK = null
                        });
                        result.Payments.Add(payment);
                    });
                    resList.Add(result);
                });
            });
            return resList;
        }

#endregion

#region Генерация экспорта перечислений (ОПФР)

        private List<OnesExportDocReqTransfer> OnesGenerateOpfrExport(DateTime exportDate, ISession session, List<long> inputIdList, out List<object> midResult)
        {
            midResult = new List<object>();
            var output = new List<OnesExportDocReqTransfer>();
            var result = session.CreateSQLQuery(@"select r.id as rid, b.id as bid, b.name, tr.sum, tr.id as trid, r.tranchenum, r.tranchedate, b.inn, b.kpp, ba.ACCOUNTNUM, ba.BIK, ba.BANK, ba.CORRACCOUNTNUM
                                            from pfr_basic.opfr_register r
                                            join pfr_basic.opfr_transfer tr on tr.opfr_register_id = r.id
                                            join pfr_basic.pfrbranch b on b.id = tr.pfrbranch_id
                                            left join pfr_basic.bankaccount ba on ba.pfrbranch_id = b.id
                                            where r.id in (:list) and tr.status_id=1 and b.status_id=1")
                                        .SetParameterList("list", inputIdList)
                                        .List<object[]>()
                                        .ToList();
            //группируем даные по реестру и ОПФР
            result.GroupBy(a => new { ID = (long)a[0], BranchID = (long)a[1], BranchName = (string)a[2], TrancheNum = (string)a[5], TrancheDate = Convert.ToDateTime(a[6]) }).ForEach(group =>
             {
                 var hItem = output.FirstOrDefault(a => a.ID == group.Key.ID);
                 var req = hItem ?? new OnesExportDocReqTransfer
                 {
                     WorkDate = exportDate,
                     Type = "ОПФР",
                     Number = group.Key.TrancheNum,
                     Date = group.Key.TrancheDate,
                     ID = group.Key.ID
                 };
                 var iSum = group.Sum(a => (decimal?)a[3] ?? 0m) * (IsDB2 ? 0.0001m : 1m);
                 if(iSum == 0) return;

                 var oneItem = group.FirstOrDefault();
                 req.Payments.Add(new OnesExportDocReqTransferPayment
                 {
                     Organization_Rcp = new OnesExportDocReqTransferOrg
                     {
                         INN = (string)oneItem?[7],
                         KPP = (string)oneItem?[8],
                         Name = (string)oneItem?[2],
                         ShortName = null
                     },
                     Account_Rcp = new OnesExportDocReqTransferAcc
                     {
                         BS = (string)oneItem?[9],
                         BIC = (string)oneItem?[10],
                         Name_BIC = (string)oneItem?[11],
                         BS_KS = (string)oneItem?[12]
                     },
                     AgreementNumber = null,
                     DepositNumber = null,
                     Sum_payment = iSum,
                     Sum_PP = new List<OnesExportDocReqTransferSumPP>
                     {
                         new OnesExportDocReqTransferSumPP
                         {
                             Type_KBK = null,
                             KBK = "39210017373063311",
                             Bag = null,
                             Sum = iSum
                         }
                     },
                     Purpose = "NONE",//TODO
                     OPFR_IDLIST = group.Select(a => (long)a[4]).ToArray()
                     // OPFR_NAME = @group.Key.BranchName,
                 });

                 if (hItem == null)
                     output.Add(req);
             });
            return output;
        }
#endregion

        private ImportFileListItem OnesGenerateExportReqTransferXML(OnesExportDocReqTransfer input, out List<string> errorsList)
        {
            errorsList = new List<string>();

            var doc = XML.CreateXmlDocumentWithRoot("RequestForTransfer");
            doc.DocumentElement.SetAttribute("xmlns", "http://www.pfr.ru");
            doc.DocumentElement.SetAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
            doc.DocumentElement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "WorkDate", input.WorkDate.HasValue ? input.WorkDate.Value.ToOnesFullXmlString() : "");
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Type", input.Type);
            var pNode = XML.CreateAddXmlNode(doc.FirstChild, "Payments");

            input.Payments.ForEach(payment =>
            {
                var ipNode = XML.CreateAddXmlNode(pNode, "Payment");

                var p2Node = XML.CreateAddXmlNode(ipNode, "Organization_Rcp");
                XML.CreateAddXmlNodeWithValue(p2Node, "INN", payment.Organization_Rcp.INN);
                XML.CreateAddXmlNodeWithValue(p2Node, "KPP", payment.Organization_Rcp.KPP);
                XML.CreateAddXmlNodeWithValue(p2Node, "Name", payment.Organization_Rcp.Name);
                XML.CreateAddXmlNodeWithValue(p2Node, "ShortName", payment.Organization_Rcp.ShortName);//Н
                p2Node = XML.CreateAddXmlNode(ipNode, "Account_Rcp");
                XML.CreateAddXmlNodeWithValue(p2Node, "BS", payment.Account_Rcp.BS); //Н
                XML.CreateAddXmlNodeWithValue(p2Node, "BIC", payment.Account_Rcp.BIC);
                XML.CreateAddXmlNodeWithValue(p2Node, "Name_BIC", payment.Account_Rcp.Name_BIC);
                XML.CreateAddXmlNodeWithValue(p2Node, "BS_KS", payment.Account_Rcp.BS_KS);//Н


                XML.CreateAddXmlNodeWithValue(ipNode, "AgreementNumber", payment.AgreementNumber);//Н
                XML.CreateAddXmlNodeWithValue(ipNode, "AgreementDate", payment.AgreementDate.HasValue ? payment.AgreementDate.Value.ToOnesXmlString() : null);//Н
                XML.CreateAddXmlNodeWithValue(ipNode, "DepositNumber", payment.DepositNumber); //Н
                XML.CreateAddXmlNodeWithValue(ipNode, "Sum_payment", payment.Sum_payment.ToOnesXmlString());
                if (payment.Sum_PP.Count > 0)
                {
                    var ppNode = XML.CreateAddXmlNode(ipNode, "Sum_PP");
                    payment.Sum_PP.ForEach(sumpp =>
                    {
                        var iNode = XML.CreateAddXmlNode(ppNode, "Sum");
                        if (!string.IsNullOrEmpty(sumpp.Type_KBK))
                            XML.CreateAddXmlNodeWithValue(iNode, "Type_KBK", sumpp.Type_KBK);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "KBK", sumpp.KBK);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "Bag", sumpp.Bag);//Н
                        XML.CreateAddXmlNodeWithValue(iNode, "Sum", sumpp.Sum.ToOnesXmlString());
                    });
                }
                XML.CreateAddXmlNodeWithValue(ipNode, "Purpose", payment.Purpose);
            });
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Number", input.Number);
            XML.CreateAddXmlNodeWithValue(doc.FirstChild, "Date", input.Date.HasValue ? input.Date.Value.ToOnesXmlString() : "");


            var result = new ImportFileListItem
            {
                FileContent = Encoding.Default.GetBytes((string)doc.OuterXml),
                IsCompressed = false
            };

            var valResult = XML.ValidateXmlFile(result.FileContent, _onesExportSchema);
            if (!string.IsNullOrEmpty(valResult))
            {
                errorsList = valResult.Split('|').ToList();
                return null;
            }

            //создаем сжатый XML файл
            result.Compress();
            return result;
        }

        /// <summary>
        /// Запись об успешном экспорте сущности
        /// </summary>
        /// <param name="entityIdList">Список идентификаторов сущностей</param>
        /// <param name="entityType">Тип сущности</param>
        /// <param name="sessionId">Идентфикатор сессии</param>
        /// <param name="session">Сессия хибернейта</param>
        /// <param name="exportId"></param>
        private static void OnesSaveExportEntries(List<long> entityIdList, OnesFileExport.OnesExportType entityType, long sessionId, ISession session, long exportId)
        {
            // var sw = Stopwatch.StartNew();
            var eType = (int)entityType;
            const string defaultQuery = "INSERT INTO PFR_BASIC.ONES_FILEEXPORT(ENTITY_ID, ENTITY_TYPE, SESSION_ID, EXPORT_ID) ";
            var query = defaultQuery;
            const int maxCount = 15;
            var count = 0;
            var stopValue = entityIdList.Count - 1;
            for (int i = 0; i < entityIdList.Count; i++)
            {
                var item = entityIdList[i];
                count++;
                query += $@"VALUES ({item}, {eType}, {sessionId}, {exportId}) ";
                if (count == maxCount)
                {
                    count = 0;
                    session.CreateSQLQuery(query).ExecuteUpdate();
                    query = defaultQuery;
                    //Debug.WriteLine(string.Format("Bulk insert of {0} rows: {1}", maxCount, sw.Elapsed));
                    //sw.Reset();
                }
                else if (i != stopValue) query += " UNION ALL ";
            }
            if (count != 0)
                session.CreateSQLQuery(query).ExecuteUpdate();
            //SaveEntities(list, session);
        }

        /// <summary>
        /// Удаление записей об успешном импорте по идентификатору экспорта
        /// </summary>
        /// <param name="exportId"></param>
        public void RemoveOnesExportByExportId(long exportId)
        {
            SessionWrapper(session =>
            {
                session.CreateQuery(@"delete from OnesFileExport fe where fe.ExportID=:id")
                    .SetParameter("id", exportId)
                    .ExecuteUpdate();

                session.CreateQuery(@"delete from OnesJournal j where j.ImportID=:id")
                    .SetParameter("id", exportId)
                    .ExecuteUpdate();

                DeleteEntity<RepositoryNoAucImpExpFile>(exportId, session);
            });
        }

        public List<OnesExportDataItem> GetExportDataItemsList(OnesSettingsDef.IntGroup part)
        {
            return TransactionWrapper(session =>
            {
                _onesOpSettings = GetList<OnesOpSettings>().Cast<OnesOpSettings>().ToList();
                _onesOpDefs = GetList<OnesSettingsDef>().Cast<OnesSettingsDef>().ToList();
                var nameList = new List<string>();
                var idList = new List<long>();
                List<object[]> list;
                var culture = CultureInfo.CreateSpecificCulture("ru-RU");
                switch (part)
                {
                    case OnesSettingsDef.IntGroup.Npf:
                        OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.NPF, session)
                            .Select(a => a.Value.Select(b => b.Name)).
                            ForEach(a => nameList.AddRange(a));
                        if (!nameList.Any()) return new List<OnesExportDataItem>();
                        list = session.CreateQuery(
                                $"select distinct fr.ID, r.ID, le.FormalizedName, ad.Name, r.RegNum, r.RegDate, fr.Count, fr.ZLCount, fr.RegNum, fr.Date, fr.Status, r.Content {string.Format(ONES_FROM_NPF_QUERY, "left join r.ApproveDoc ad", "")}")
                            .SetParameterList("list", nameList)
                            .SetParameter("type", (int)OnesFileExport.OnesExportType.Finregister)
                            .SetParameterList("status", new object[] { RegisterIdentifier.FinregisterStatuses.Transferred, RegisterIdentifier.FinregisterStatuses.NotTransferred })
                            .List<object[]>()
                            .ToList();
                        return list.Select(a => new OnesExportDataItem
                        {
                            ID = (long)a[0],
                            Item1 = (string)a[11], //content
                            Item2 = $"{a[1]} \tдокумент: {a[3]}     \r\n\t№ {a[4]} от: {(a[5] == null ? "" : ((DateTime) a[5]).ToOnesXmlString())}",
                            Item3 = (string)a[10], //status
                            Item4 = (string)a[2], //name
                            Item5 = Convert.ToDecimal(a[6]).ToString("n2", culture), //sum
                            Item6 = Convert.ToInt64(a[7]).ToString("N0", culture), //zl
                            Item7 = a[8], //regnum
                            Item8 = a[9] //date
                        }).ToList();
                    case OnesSettingsDef.IntGroup.Si:
                    case OnesSettingsDef.IntGroup.Vr:
                        bool isVr = part == OnesSettingsDef.IntGroup.Vr;
                        OnesGenerateOpPacks<SPNOperation>(OnesSettingsDef.Groups.UK, session)
                            .Select(a => a.Value.Select(b => b.ID))
                            .ForEach(a => idList.AddRange(a));
                        if (!idList.Any()) return new List<OnesExportDataItem>();
                        //list = session.CreateQuery(string.Format(@"select distinct rq.ID, reg.ID, le.FormalizedName, reg.RegisterKind, reg.RegisterNumber, reg.RegisterDate, c.ContractNumber, rq.TransferStatus, rq.ZLMoveCount, rq.Sum, rq.InvestmentIncome, op.Name {0}", string.Format(ONES_FROM_SIVR_QUERY, "")))
                        list = session.CreateSQLQuery(ONES_FROM_CIVR_QUERY_SELECT)
                            .SetParameterList("opList", idList)
                            .SetParameter("direction", !isVr ? (long)SIRegister.Directions.ToUK : (long)SIRegister.Directions.ToGUK)
                            .SetParameter("type", !isVr ? (int)OnesFileExport.OnesExportType.ReqTransfer : (int)OnesFileExport.OnesExportType.ReqTransferVr)
                            .List<object[]>()
                            .ToList();
                        return list.Select(a => new OnesExportDataItem
                        {
                            ID = (long)a[0],
                            Item1 = (string)a[11], //op
                            Item2 = $"{a[1]} \tдокумент: {a[3]}     \r\n\t№ {a[4]} от: {(a[5] == null ? "" : ((DateTime) a[5]).ToOnesXmlString())}",
                            Item3 = (string)a[7], //rq status
                            Item4 = (string)a[2], //name
                            Item5 = (string)a[6], //contract num
                            Item6 = Convert.ToInt64(a[8]).ToString("N0", culture), //rq zlcount
                            Item7 = (Convert.ToDecimal(a[9]) / (IsDB2 ? 10000 : 1)).ToString("n2", culture), //rq sum
                            Item8 = (Convert.ToDecimal(a[10]) / (IsDB2 ? 10000 : 1)).ToString("n2", culture) //rq investdohod
                        }).ToList();
                    case OnesSettingsDef.IntGroup.Depo:
                        var offerStatusList = new List<int>
                        {
                           // (int) DepClaimSelectParams.Statuses.OfferCreated,
                          //  (int) DepClaimSelectParams.Statuses.OfferSigned,
                            (int) DepClaimSelectParams.Statuses.OfferAcceptedOrNo,
                            (int) DepClaimSelectParams.Statuses.DepositSettled
                        };
                        OnesGenerateOpPacks<Element>(OnesSettingsDef.Groups.DEPOSIT, session)
                            .Select(a => a.Value.Select(b => b.ID)).
                            ForEach(a => idList.AddRange(a));
                        if (!idList.Any()) return new List<OnesExportDataItem>();
                        list = session.CreateQuery(
                                $@"select distinct offer.ID, auc.SelectDate, le.FormalizedName, offer.Number, offer.StatusID, auc.StockName, dc.Rate, dc.Amount, dc.Payment, dc.SettleDate, dc.ReturnDate {
                                    ONES_FROM_DEPO_QUERY
                                }")
                            .SetParameterList("statList", offerStatusList)
                            .SetParameterList("idList", idList)
                            .SetParameterList("badStatusList", new[] { (int)DepClaimOffer.Statuses.NotAccepted })
                            .SetParameter("type", (int)OnesFileExport.OnesExportType.Deposit)
                            .List<object[]>()
                            .ToList();
                        return list.Select(a => new OnesExportDataItem
                        {
                            ID = (long)a[0],
                            Date = (DateTime?)a[1], //date
                            Item2 = (string)a[5], //stock name
                            Item3 = ((DepClaimOffer.Statuses)(int)a[4]).GetDescription(),//status
                            Item4 = Convert.ToInt32(a[3]).ToString("N0", culture), //offer num
                            Item5 = (string)a[2], //name
                            Item6 = Convert.ToDecimal(a[6]).ToString("n2", culture), //rate
                            Item7 = Convert.ToDecimal(a[7]).ToString("n2", culture), //sum
                            Item8 = Convert.ToDecimal(a[8]).ToString("n2", culture), //sum 2
                            Item9 = a[9], //settle date
                            Item10 = a[10] //return date
                        }).ToList();
                    case OnesSettingsDef.IntGroup.Opfr:
                        return
                            session.CreateSQLQuery(//TODO выборка из перечислений, т.к. нужен по нимо отсев
                                @"select distinct r.id, i1.KindName, r.regnum, r.regdate from pfr_basic.opfr_register r
                                                left join (select tr.id as tid, tr.opfr_register_id as regid, count(tr.ID) as cnt, sum(tr.Sum) as summ 
                                                    from pfr_basic.opfr_transfer tr 
                                                    where tr.status_id=1 and tr.status=:status group by tr.id, tr.opfr_register_id) as trans on r.ID=trans.regid
                                                join (select el.ID as id, el.name as KindName from pfr_basic.element el) i1 on i1.id = r.kind_id
                                                where r.status_id=1 and trans.cnt > 0 and r.tranchedate is not null and r.tranchenum is not null and trans.summ<>0
                                                    and (select count(fe.ID) from pfr_basic.ones_fileexport fe where fe.entity_id = trans.tid and fe.entity_type = :type) = 0")
                                .SetParameter("status", (long)Element.SpecialDictionaryItems.OpfrTransferStatusIssued)
                                .SetParameter("type", (int)OnesFileExport.OnesExportType.Opfr)
                                .List<object[]>()
                                .Select(a => new OnesExportDataItem
                                {
                                    ID = (long)a[0],
                                    Item1 = (string)a[1],
                                    Item2 = $@"№{a[2]} от {Convert.ToDateTime(a[3]):dd.MM.yyyy}"
                                }).ToList();
                    default: throw new Exception("Неизвестный раздел ПТК!");
                }
            }, IsolationLevel.ReadUncommitted);
        }

        //запрос с параметром {0}, сам параметр подставляется в GetExportDataItemsList
        //для выборки предварительного списка сущностей (нужен для отображения наименовани реестра)
        private const string ONES_FROM_NPF_QUERY = @"
            from FinregisterHib fr 
            join fr.Register r
            join fr.DebitAccount da
            {0}
            join da.Contragent ca
            join ca.LegalEntities le
            {1}
            join le.BankAccounts bank
            where r.Content in (:list) and r.TrancheDate is not null and coalesce(r.IsCustomArchive,0)=0 and fr.StatusID <> -1 and ca.StatusID <> -1 and bank.CloseDate is null
                and lower(trim(fr.Status)) not in (:status)
                and (select count(fe.ID) from OnesFileExport fe where fe.EntityID = fr.ID and fe.EntityType = :type) = 0
                and (select count(pp.ID) from AsgFinTr pp where pp.FinregisterID = fr.ID and pp.StatusID != -1) = 0";

        private const string ONES_FROM_SIVR_QUERY = @"
            from ReqTransferHib rq 

            {0}
            join rq.TransferList tl
            join tl.TransferRegister reg
            left join reg.Operation op

            join tl.Contract c
            join c.LegalEntity le
            join c.BankAccount bank
            where  rq.Date is not null and rq.StatusID <> -1 
                and tl.ID is not null and tl.StatusID <> -1
                and reg.DirectionID = :direction and reg.OperationID in (:opList) and reg.StatusID <> -1 
                and bank.CloseDate is null
                and (select count(fe.ID) from OnesFileExport fe where fe.EntityID = rq.ID and fe.EntityType = :type) = 0
                and (select count(pp.ID) from AsgFinTr pp where pp.ReqTransferID = rq.ID and pp.StatusID != -1) = 0";

        private const string ONES_FROM_CIVR_QUERY_SELECT = @"select distinct reqtransfe0_.ID as col_0_0_, siregister2_.ID as col_1_0_, legalentit5_.FORMALIZEDNAME as col_2_0_, siregister2_.KIND as col_3_0_, siregister2_.REGNUM as col_4_0_, siregister2_.DATE as col_5_0_, contracthi4_.REGNUM as col_6_0_, reqtransfe0_.STATUS as col_7_0_, reqtransfe0_.ZL_MOVECOUNT as col_8_0_, reqtransfe0_.SUM as col_9_0_, reqtransfe0_.INVESTDOHOD as col_10_0_, spnoperati3_.OPERATION as col_11_0_ 
            from PFR_BASIC.REQ_TRANSFER reqtransfe0_ 
            inner join PFR_BASIC.SI_TRANSFER sitransfer1_ on reqtransfe0_.SI_TR_ID=sitransfer1_.ID 
            inner join PFR_BASIC.SI_REGISTER siregister2_ on sitransfer1_.SI_REG_ID=siregister2_.ID 
            left outer join PFR_BASIC.OPERATION spnoperati3_ on siregister2_.OPERATION_ID=spnoperati3_.ID 
            inner join PFR_BASIC.CONTRACT contracthi4_ on sitransfer1_.CONTR_ID=contracthi4_.ID 
            inner join PFR_BASIC.LEGALENTITY legalentit5_ on contracthi4_.LE_ID=legalentit5_.ID 
            inner join PFR_BASIC.BANKACCOUNT bankaccoun6_ on contracthi4_.BA_ID=bankaccoun6_.ID 
            left join PFR_BASIC.ASG_FIN_TR pp on pp.REQ_TR_ID = reqtransfe0_.ID and pp.STATUS_ID <>-1
            left join PFR_BASIC.ONES_FILEEXPORT fe on fe.ENTITY_ID = reqtransfe0_.ID and fe.ENTITY_TYPE=:type
            where (reqtransfe0_.DATE is not null) and reqtransfe0_.STATUS_ID<>-1 and 
            (sitransfer1_.ID is not null) and sitransfer1_.STATUS_ID<>-1 and 
            siregister2_.DIR_SPN_ID=:direction and (siregister2_.OPERATION_ID in (:opList)) and siregister2_.STATUS_ID<>-1 and 
            (bankaccoun6_.CLOSEDATE is null) and 
            fe.ID is null and pp.ID is null";


        private const string ONES_FROM_DEPO_QUERY = @"
            from DepClaimOfferHib offer
            join offer.Auction auc
            left join offer.DepClaim dc
            join offer.Bank le
            join offer.OfferPfrBankAccountSum acc
            join acc.PfrBankAccount ba
            join acc.Portfolio pf
            where auc.AuctionStatusID in (:statList) and auc.ID is not null and le.CloseDate is null and acc.StatusID <> -1
                and pf.TypeID in (:idList) and pf.StatusID <> -1
                and (select count(fe.ID) from OnesFileExport fe where fe.EntityID = offer.ID and fe.EntityType = :type) = 0
                and offer.StatusID not in (:badStatusList)";

        /// <summary>
        /// Хранит текущее кол-во обработанных сущностей для раздела
        /// </summary>
        private readonly int[] _onesExportCounts = new int[6];
        /// <summary>
        /// Хранит макс. кол-во обработанных сущностей для раздела
        /// </summary>
        private readonly int[] _onesExportMaxCounts = new int[6];
        /// <summary>
        /// Хранит состояние сессии экспорта/импорта 1С для раздела
        /// </summary>
        private static readonly bool[] OnesExportStates = new bool[6];

        public OnesExportStatus GetOnesExportStatus(OnesSettingsDef.IntGroup type)
        {
            var iType = (int)type;
            lock (OnesExportStateLocker)
            {
                var max = _onesExportMaxCounts[iType];
                var cur = !OnesExportStates[iType] ? max : _onesExportCounts[iType];
                return new OnesExportStatus { Completed = !OnesExportStates[iType], Count = cur, MaxCount = max };
            }
        }

#region Data classes

        internal class OnesExportDocReqTransferSumPP
        {
            public string Type_KBK { get; set; }
            public string KBK { get; set; }
            public string Bag { get; set; }
            public decimal Sum { get; set; }
        }

        internal class OnesExportDocReqTransferOrg
        {
            public string INN { get; set; }
            public string KPP { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
        }

        internal class OnesExportDocReqTransferAcc
        {
            public string BS { get; set; }
            public string BIC { get; set; }
            public string Name_BIC { get; set; }
            public string BS_KS { get; set; }
        }

        private class OnesExportDocReqTransferPayment
        {
            public OnesExportDocReqTransferOrg Organization_Rcp { get; set; }
            public OnesExportDocReqTransferAcc Account_Rcp { get; set; }
            public string AgreementNumber { get; set; }
            public DateTime? AgreementDate { get; set; }
            public string DepositNumber { get; set; }
            public decimal Sum_payment { get; set; }
            /// <summary>
            /// Этот список в XML кладется как SUM_PP -> SUM, SUM...
            /// </summary>
            public List<OnesExportDocReqTransferSumPP> Sum_PP { get; set; }
            public string Purpose { get; set; }
            /// <summary>
            /// Временно хранит ID сущности для внутренней обработки
            /// </summary>
            public long ID { get; set; }

            // public string OPFR_NAME { get; set; }
            // public string OPFR_KBK { get; set; }
            // public decimal OPFR_SUM { get; set; }
            //Временно хранит ID перечислений, с которых взята сумма ОПФР
            public long[] OPFR_IDLIST { get; set; }

            public OnesExportDocReqTransferPayment()
            {
                Organization_Rcp = new OnesExportDocReqTransferOrg();
                Account_Rcp = new OnesExportDocReqTransferAcc();
                Sum_PP = new List<OnesExportDocReqTransferSumPP>();
            }
        }

        private class OnesExportDocReqTransfer
        {
            /// <summary>
            /// Текущая дата со временем
            /// </summary>
            public DateTime? WorkDate { get; set; }

            public string Type { get; set; }

            public string Number { get; set; }

            public DateTime? Date { get; set; }

            public List<OnesExportDocReqTransferPayment> Payments { get; }

            /// <summary>
            /// Временно хранит ID сущности для внутренней обработки
            /// </summary>
            public long ID { get; set; }

            public OnesExportDocReqTransfer()
            {
                Payments = new List<OnesExportDocReqTransferPayment>();
            }
        }
#endregion
    }
}
