﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.Xml.Serialization;

namespace PFR_INVEST.Proxy.Integration.kip
{

	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	[ServiceContract(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", ConfigurationName = "Iservice")]
	public interface Iservice
	{

		// CODEGEN: Generating message contract since the operation Register is neither RPC nor document wrapped.
		[OperationContract(Action = "*", ReplyAction = "*")]
		[XmlSerializerFormat(SupportFaults = true)]
		RegisterResponse1 Register(RegisterRequest1 request);
	}

	/// <remarks/>
	[GeneratedCode("System.Xml", "4.0.30319.34234")]
	[Serializable]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "http://www.rstyle.com/ipc/ws/RegisterService")]
	public class RegisterRequest
	{

		/// <remarks/>
		[XmlElement(DataType = "base64Binary")]
		public byte[] inputData;
	}

	/// <remarks/>
	[GeneratedCode("System.Xml", "4.0.30319.34234")]
	[Serializable]
	[DebuggerStepThrough]
	[DesignerCategory("code")]
	[XmlType(AnonymousType = true, Namespace = "http://www.rstyle.com/ipc/ws/RegisterService")]
	public class RegisterResponse
	{

		/// <remarks/>
		public string outputData;
	}

	[DebuggerStepThrough]
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	[MessageContract(IsWrapped = false)]
	public class RegisterRequest1
	{

		[MessageBodyMember(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", Order = 0)]
		public RegisterRequest RegisterRequest;

		public RegisterRequest1()
		{
		}

		public RegisterRequest1(RegisterRequest RegisterRequest)
		{
			this.RegisterRequest = RegisterRequest;
		}
	}

	[DebuggerStepThrough]
	[GeneratedCode("System.ServiceModel", "4.0.0.0")]
	[EditorBrowsable(EditorBrowsableState.Advanced)]
	[MessageContract(IsWrapped = false)]
	public class RegisterResponse1
	{

		[MessageBodyMember(Namespace = "http://www.rstyle.com/ipc/ws/RegisterService", Order = 0)]
		public RegisterResponse RegisterResponse;

		public RegisterResponse1()
		{
		}

		public RegisterResponse1(RegisterResponse RegisterResponse)
		{
			this.RegisterResponse = RegisterResponse;
		}
	}

	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Single)]
	public class service : Iservice
	{

		public virtual RegisterResponse1 Register(RegisterRequest1 request)
		{
			throw new NotImplementedException();
		}
	}
}
