﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using NHibernate;
using NHibernate.Linq;
using PFR_INVEST.Common.Tools;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.DataAccess.Server.DataObjects;
using PFR_INVEST.DataObjects;
using PFR_INVEST.DataObjects.Journal;
using PFR_INVEST.DataObjects.ListItems;
using PFR_INVEST.Proxy.Tools;
#if WEBCLIENT
using NHibernate.Util;
#endif

namespace db2connector
{
    public partial class IISServiceHib
    {
        /// <summary>
        /// Схема версия 10 на 19.10.2015
        /// </summary>
#if WEBCLIENT
        private const string ONES_XSD = @"Database/XSD/Scheme_1C.xsd";
#else
        private const string ONES_XSD = @"XSD/Scheme_1C.xsd";
#endif
        internal static volatile bool IsOnesImportInProgress;

        public WebServiceDataError StartOnesImport(string importPath, List<ImportFileListItem> files = null)
        {
            //server check
            if (files == null && !Directory.Exists(importPath))
                    return WebServiceDataError.DirectoryNotFound;
            if(IsOnesImportInProgress)
                return WebServiceDataError.SessionInProgress;
            IsOnesImportInProgress = true;
            ThreadPool.QueueUserWorkItem(OnesProcessImport, files ?? (object)importPath);
            return WebServiceDataError.None;
        }

        private PortfolioHib _onesImportDefaultPortfolio;
        private PfrBankAccount _onesImportDefaultBAcc;

        private int _onesImportTotalFilesCount;
        private int _onesImportProcessedGoodFilesCount;
        private int _onesImportOverallProcessedFilesCount;

        public int[] GetOnesImportProgress()
        {
            return IsOnesImportInProgress ? new[] {_onesImportOverallProcessedFilesCount, _onesImportTotalFilesCount} : null;
        }

        #region ОСновная операция импорта 1С
        private void OnesProcessImport(object state)
        {
            SessionWrapper(session =>
            {
                _onesImportTotalFilesCount = 0;
                _onesImportProcessedGoodFilesCount = 0;
                _onesImportOverallProcessedFilesCount = 0;
                long sessionId = 0;
                List<ImportFileListItem> files = null;
                var isServerSide = state is string;
                try
                {
                    //создаем новую сессию
                    sessionId = SaveEntityTransact(new OnesSession { SessionDate = DateTime.Now }, session);
                    OnesLogJournal("Запущена операция импорта файлов 1С", sessionId);

                    //запуск на сервере
                    if (isServerSide)
                    {
                        var folderPath = state.ToString();
                        var iFiles = Directory.GetFiles(folderPath, "*.xml", SearchOption.TopDirectoryOnly);
                        _onesImportTotalFilesCount = iFiles.Length;
                        files = iFiles.Select(OnesReadFileFromServer)
                            .Where(file => file != null)
                            .ToList();
                    }
                    else
                    {
                        //запуск на клиенте
                        files = state as List<ImportFileListItem>;
                        _onesImportTotalFilesCount = files?.Count ?? 0;
                    }

                    //если отсутствуют файлы
                    if (files == null || files.Count == 0)
                    {
                        OnesLogJournal("Отсутствуют файлы для обработки", sessionId);
                        return;                        
                    }

                    //загружаем схему валидации
                    var xsdPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ONES_XSD);
                    if (!File.Exists(xsdPath))
                    {
                        OnesLogJournal("Не найдена XSD схема для импорта/экспорта данных 1С!", sessionId, OnesJournal.Actions.General, OnesJournal.LogTypes.Error);
                        return;
                    }

                    _onesImportDefaultPortfolio = session.CreateQuery(@"FROM PortfolioHib pf 
                                                                        left join fetch pf.PortfolioToPFRBankAccounts bacc
                                                                        left join fetch bacc.PfrBankAccount acc
                                                                        where pf.StatusID=2")
                        .SetMaxResults(1)
                        .UniqueResult<PortfolioHib>();
                    if (_onesImportDefaultPortfolio == null)
                    {
                        OnesLogJournal("Не найден портфель по умолчанию для импорта данных 1С!", sessionId, OnesJournal.Actions.General, OnesJournal.LogTypes.Error);
                        return;
                    }
                    var acc = _onesImportDefaultPortfolio.PortfolioToPFRBankAccounts.FirstOrDefault();
                    if (!(acc is PortfolioPFRBankAccountHib))
                    {
                        OnesLogJournal("В портфеле '- ИМПОРТ ИЗ 1С -' отсутствуют банковские счета. Для проведения операции необходимо добавить хотя бы один счет.", sessionId, OnesJournal.Actions.General, OnesJournal.LogTypes.Error);
                        return;
                    }
                    _onesImportDefaultBAcc = ((PortfolioPFRBankAccountHib) acc).PfrBankAccount;

                    XmlSchemaSet schema;
                    using (var xsdReader = new StreamReader(xsdPath))
                    {
                        var xmlSchema = XmlSchema.Read(xsdReader, null);
                        var xmlSchemaSet = new XmlSchemaSet();
                        xmlSchemaSet.Add(xmlSchema);
                        xmlSchemaSet.Compile();
                        schema = xmlSchemaSet;
                    }

                    //загрузка файлов
                    files.ForEach(file =>
                    {
                        OnesFileImport importEntry = null;
                        try
                        {
                            //проверка на дубликат
                            var shortFileName = Path.GetFileName(file.FilePath)?.ToLowerInvariant();
                            var exFile = GetListByProperty<OnesFileImport>("Filename", shortFileName, session).Cast<OnesFileImport>().FirstOrDefault();
                            if (exFile != null && exFile.ImportStatusID == (long) OnesStatus.Statuses.Processed)
                            {
                                OnesLogJournal($"Файл с таким именем уже был загружен ранее: {shortFileName}", sessionId, OnesJournal.Actions.Load, OnesJournal.LogTypes.Error);
                                OnesMoveImportFile(file.FilePath, true, isServerSide);
                                return;
                            }

                            var dt = DateTime.Now;

                            //сохраняем файл в хранилище
                            var fileId = SaveEntityTransact(new RepositoryNoAucImpExpFile
                            {
                                Repository = file.GetCompressed(),
                                Key = (int) RepositoryImpExpFile.Keys.OnesImportXml,
                                DDate = dt,
                                Status = 1,
                                TTime = new TimeSpan(dt.TimeOfDay.Hours, dt.TimeOfDay.Minutes, dt.TimeOfDay.Seconds),
                                ImpExp = 1,
                                UserName = DomainUser,
                                Comment = "Импорт XML файлов 1С"
                            }, session);
                            //создаем запись импорта для файла, статус - received
                            importEntry = new OnesFileImport
                            {
                                LoadDate = dt,
                                FileID = fileId,
                                Filename = shortFileName,
                                ImportStatusID = (long) OnesStatus.Statuses.Received,
                                SessionID = sessionId
                            };
                            importEntry.ID = SaveEntityTransact(importEntry, session);
                            OnesLogJournal($"Файл {file.FilePath} успешно загружен в хранилище", sessionId, OnesJournal.Actions.Load, OnesJournal.LogTypes.Info, importEntry.ID);

                            //распаковываем содержимое для дальнейшей работы
                            file.Decompress();

                            //валидируем файл
                            var valResult = XML.ValidateXmlFile(file.FileContent, schema);
                            if (valResult != null)
                            {
                                //статус validated_err
                                OnesLogJournal(@"Валидация завершилась ошибками: " + valResult, sessionId, OnesJournal.Actions.Validate, OnesJournal.LogTypes.Error, importEntry.ID);
                                importEntry.ImportStatusID = (long) OnesStatus.Statuses.ValidatedWithError;
                                SaveEntityTransact(importEntry, session);
                                OnesMoveImportFile(file.FilePath, false, isServerSide);
                                return;
                            }
                            //статус - validated
                            //OnesLogJournal(@"Валидация завершилась успешно", sessionId, OnesJournal.Actions.Validate, OnesJournal.LogTypes.Info, importEntry.ID);
                            importEntry.ImportStatusID = (long) OnesStatus.Statuses.Validated;
                            SaveEntityTransact(importEntry, session);

                            //разбор документа
                            if (OnesProcessXmlData(file.FileContent, importEntry, schema, sessionId, session))
                            {
                                _onesImportProcessedGoodFilesCount++;
                                OnesMoveImportFile(file.FilePath, true, isServerSide);
                            }
                            else OnesMoveImportFile(file.FilePath, false, isServerSide);

                            //очищаем текущий материал после успешнйо обработки
                            file.FileContent = null;
                        }
                        catch (Exception ex)
                        {
                            LogManager.Log($"Исключение при обработке файла импорта 1С {file.FilePath}");
                            LogManager.LogException(ex);
                            OnesLogJournal($"Возникла непредвиденная ошибка при обработке файла {file.FilePath}! Подробная информация находится в логе сервера", sessionId, OnesJournal.Actions.General,
                                OnesJournal.LogTypes.Error, (importEntry == null || importEntry.ID == 0) ? null : (long?) importEntry.ID);
                            OnesMoveImportFile(file.FilePath, false, isServerSide);
                        }
                        finally
                        {
                            _onesImportOverallProcessedFilesCount++;
                        }
                    });
                }
                catch (Exception ex)
                {
                    try
                    {
                        OnesLogJournal("Возникла непредвиденная ошибка! Подробная информация находится в логе сервера", sessionId);
                    }
                    catch
                    {
                        // ignored
                    }
                    LogManager.LogException(ex);
                }
                finally
                {
                    _onesImportDefaultPortfolio = null;
                    IsOnesImportInProgress = false;
                    try
                    {
                        OnesLogJournal(
                            $@"Обработка завершена! Всего: {_onesImportTotalFilesCount} Загружено: {files?.Count ?? 0} Обработано: {_onesImportProcessedGoodFilesCount}", sessionId);
                    }
                    catch
                    {
                        // ignored
                    }
                    files?.Clear();
                    GC.Collect();
                }
            });
        }

        private static void OnesMoveImportFile(string filePath, bool isGood, bool isServerSide)
        {
            try
            {
                if (!isServerSide) return;
                var movePath = Path.Combine(Path.GetDirectoryName(filePath), isGood ? "Загруженные" : "Ошибочные");
                if (!Directory.Exists(movePath))
                    Directory.CreateDirectory(movePath);

                var destFile = Path.Combine(movePath, Path.GetFileName(filePath));
                if (File.Exists(destFile))
                    File.Delete(destFile);
                File.Move(filePath, destFile);
            }
            catch (Exception ex)
            {
                LogManager.Log("шибка перемещения импортируемых файлов 1С на стороне сервера! path: "+ filePath);
                LogManager.LogException(ex);
            }
        }
        #endregion 

        #region Разбор XML для импорта 1С
        private bool OnesProcessXmlData(byte[] fileContent, OnesFileImport importEntry, XmlSchemaSet schema, long sessionId, ISession session)
        {
            var lastCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
                //статус processing
                importEntry.ImportStatusID = (long) OnesStatus.Statuses.Processing;
                SaveEntityTransact(importEntry, session);

                //под транзакцией для отката изменений по всему файлу
                return TransactionWrapper(tsession =>
                {
                    //валидация уже не требуется
                    var settings = new XmlReaderSettings
                    {
                        ValidationType = ValidationType.None,
                        ValidationFlags = XmlSchemaValidationFlags.None,
                        IgnoreWhitespace = true,
                        IgnoreComments = true
                    };
                    //settings.Schemas.Add(schema);

                    using (var stream = new MemoryStream(fileContent))
                    {
                        using (var reader = XmlReader.Create(stream, settings))
                        {
                            //загружаем док целиком! при возникновении проблем с памятью, можно сменить логику на построчный разбор, что будет медленнее и сложнее
                            var doc = XDocument.Load(reader);
                            if (doc.Root == null)
                                throw new XmlParseException($"Не найден корень документа {importEntry.Filename}!");
                            //получаем kind
                            var kindEl = doc.Root.Elements("{http://www.pfr.ru}Kind").FirstOrDefault();
                            string kind;
                            if (kindEl != null)
                                kind = importEntry.DocKind = kindEl.Value;
                            else
                                throw new XmlParseException($"Ошибка данных XML документа {importEntry.Filename}! Не найден Kind!");

                            //получаем type
                            var typeEl = doc.Root.Elements("{http://www.pfr.ru}Type").FirstOrDefault();
                            string type;
                            if (typeEl != null)
                                type = typeEl.Value;
                            else
                                throw new XmlParseException($"Ошибка данных XML документа {importEntry.Filename}! Не найден Type!");


                            //продолжаем парсинг
                            var numberElement = doc.Root.Elements("{http://www.pfr.ru}Number").FirstOrDefault();
                            if (numberElement != null)
                                importEntry.DocNumber = numberElement.Value;
                            else
                                throw new XmlParseException($"Ошибка данных XML документа {importEntry.Filename}! Не найден Номер!");

                            var dateElement = doc.Root.Elements("{http://www.pfr.ru}Date").FirstOrDefault();
                            if (dateElement != null)
                                importEntry.DocDate = DateTime.Parse(dateElement.Value);
                            else
                                throw new XmlParseException($"Ошибка данных XML документа {importEntry.Filename}! Не найдена Дата!");

                            //обработка случаев, когда просто записываем XML
                            switch (kind)
                            {   
                                case OnesDocKinds.K_OPFR:
                                case OnesDocKinds.K_OPFRV:
                                case OnesDocKinds.K_GSO:
                                case OnesDocKinds.K_ZZL:
                                case OnesDocKinds.K_ZZLV:
                                    importEntry.ImportStatusID = (long) OnesStatus.Statuses.Processed;
                                    //сохраняем данные документа в таблицу файлов
                                    SaveEntity(importEntry, tsession);
                                    return true;
                                default:
                                    //сохраняем данные документа в таблицу файлов
                                    SaveEntity(importEntry, tsession);
                                    break;
                            }
                            XName criteria;
                            int docType;
                            switch (doc.Root.Name.LocalName)
                            {
                                case "PaymentList":
                                    criteria = "{http://www.pfr.ru}Payment";
                                    docType = 0;
                                    break;
                                case "SumInformation":
                                    criteria = "{http://www.pfr.ru}Row";
                                    docType = 1;
                                    break;
                                default:
                                    throw new Exception("Неизвестный тип документа XML!");
                            }
                            //парсим все вложения
                            foreach (var node in doc.Descendants(criteria))
                            {
                                long? entityId;
                                switch (docType)
                                {
                                    case 0:
                                        entityId = OnesImportParseXMLPayment(node, tsession, importEntry.Filename, type, kind);
                                        break;
                                    case 1:
                                        entityId = OnesImportParseXMLRow(node, tsession, importEntry.Filename);
                                        break;
                                    default:
                                        throw new Exception("Неизвестный тип документа XML!.");
                                }
                                //создаем связь импорта с сущностью
                                if (entityId != null)
                                {
                                    SaveEntity(new OnesLink
                                    {
                                        DocID = entityId.Value,
                                        ImportID = importEntry.ID
                                    }, tsession);
                                }
                            }
                        }
                    }

                    //статус processed
                    importEntry.ImportStatusID = (long) OnesStatus.Statuses.Processed;
                    SaveEntity(importEntry, tsession);
                    return true;
                }, IsolationLevel.ReadUncommitted);
            }
            catch (XmlParseException ex)
            {
                OnesLogJournal(@"Ошибка при разборе документа: " + ex, sessionId, OnesJournal.Actions.Parse, OnesJournal.LogTypes.Error, importEntry.ID);
                importEntry.ImportStatusID = (long) OnesStatus.Statuses.ProcessedWithError;
                SaveEntityTransact(importEntry, session);
                return false;
            }
            catch (Exception ex)
            {

                OnesLogJournal(@"Исключение: " + ex, sessionId, OnesJournal.Actions.General, OnesJournal.LogTypes.Error, importEntry.ID);
               // OnesLogError(session, ex.Message, importEntry.ID);
                //статус processed_err
                importEntry.ImportStatusID = (long) OnesStatus.Statuses.ProcessedWithError;
                SaveEntityTransact(importEntry, session);
                return false;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = lastCulture;
            }
        }

        private long? OnesImportParseXMLRow(XElement row, ISession session, string filename)
        {
            //TODO
            return null;
        }

        private string TrimComment(string comment)
        {
            if (string.IsNullOrEmpty(comment)) return comment;
            var result = comment.Trim();
            if (result.StartsWith(Environment.NewLine)) result = result.Remove(0, Environment.NewLine.Length);
            if (result.EndsWith(Environment.NewLine)) result = result.Remove(result.Length - Environment.NewLine.Length, Environment.NewLine.Length);
            return result.Trim();
        }

        private long? OnesImportParseXMLPayment(XElement payment, ISession session, string filename, string type, string kind)
        {
            switch (kind)
            {
				case OnesDocKinds.K_DEP:
				case OnesDocKinds.K_DEPV:
				case OnesDocKinds.K_DEPVP:
					var dpo = new PaymentOrder
                    {
                        DIDate = DateTime.Today,
						DirectionElID = type == OnesDocKinds.VYP ? (long)Element.SpecialDictionaryItems.PPDirOutcome : (long)Element.SpecialDictionaryItems.PPDirIncome,
						StatusID = 1,
						PayerIssueDate = DateTime.Parse(GetXmlFieldValue(payment, "DateOperation")),

						DraftRegNum = GetXmlFieldValue(payment, "Number"),
						DraftDate = DateTime.Parse(GetXmlFieldValue(payment, "Date")),
						DraftAmount = Convert.ToDecimal(GetXmlFieldValue(payment, "Sum_Payment"), new NumberFormatInfo()),
						
						Comment = TrimComment(GetXmlFieldValue(payment, "Purpose"))
                    };

					// Указываем тип документа
                    switch (kind)
                    {
                        case OnesDocKinds.K_DEP:
                            dpo.DocumentTypeElID = (long) PaymentOrder.DocumentType.Allocation;
                            break;
                        case OnesDocKinds.K_DEPV:
							dpo.DocumentTypeElID = (long) PaymentOrder.DocumentType.Refund;
                            break;
						case OnesDocKinds.K_DEPVP:
							dpo.DocumentTypeElID = (long)PaymentOrder.DocumentType.Percent;
							break;
                    }
                    var accElDpo = payment.Descendants(type == OnesDocKinds.VYP ? "{http://www.pfr.ru}Organization_Rcp" : "{http://www.pfr.ru}Organization_Pay").FirstOrDefault();
                    if(accElDpo == null)
                        throw new XmlParseException($"Ошибка данных XML документа {filename}! Не найден тег Organization_XXX!");

					var dpoInn = TrimComment(GetXmlFieldValue(accElDpo, "INN"));
					var dpoKpp = TrimComment(GetXmlFieldValue(accElDpo, "KPP"));
                    //ищем по ИНН субъектов
                    if (!string.IsNullOrEmpty(dpoInn))
                    {
                        var ppLe = string.IsNullOrEmpty(dpoKpp)
                            ? GetListByPropertyConditions<LegalEntity>(session, ListPropertyCondition.Equal("INN", dpoInn)).Cast<LegalEntity>().FirstOrDefault()
                            : GetListByPropertyConditions<LegalEntity>(session, ListPropertyCondition.Equal("INN", dpoInn), ListPropertyCondition.Equal("OKPP", dpoKpp)).Cast<LegalEntity>().FirstOrDefault();
                        if (ppLe != null)
                            dpo.BankID = ppLe.ID;
                    }
                    var resultDpo = SaveEntity(dpo, session);
                    SaveJornalLog(JournalEventType.IMPORT_DATA, "Платежное поручение по депозитам", resultDpo, dpo.GetType().Name);
                    
					return resultDpo;
                case OnesDocKinds.K_NPF:
                case OnesDocKinds.K_NPFV:
                case OnesDocKinds.K_UK:
                case OnesDocKinds.K_UKV:
                    var pp = new AsgFinTr
                    {
                        DIDate = DateTime.Today,
                        PayNumber = GetXmlFieldValue(payment, "Number"),
                        PayDate = DateTime.Parse(GetXmlFieldValue(payment, "Date")),
                        SPNDate = DateTime.Parse(GetXmlFieldValue(payment, "DateOperation")),
                        DirectionElID = type == OnesDocKinds.VYP ? (long) Element.SpecialDictionaryItems.PPDirOutcome : (long) Element.SpecialDictionaryItems.PPDirIncome,
                        PaymentDetailsID = null,
                        PaySum = Convert.ToDecimal(GetXmlFieldValue(payment, "Sum_Payment"), new NumberFormatInfo()),
                        KBKID = null,
                        LinkElID = (long)AsgFinTr.Links.NoLink,
                        StatusID = 1,
                        PortfolioID = _onesImportDefaultPortfolio.ID,
                        PFRBankAccountID = _onesImportDefaultBAcc.ID
                    };

                    switch (kind)
                    {
                        case OnesDocKinds.K_NPF:
                        case OnesDocKinds.K_NPFV:
                            pp.SectionElID = (long) AsgFinTr.Sections.NPF;
                            pp.ContragentTypeID = (long) AsgFinTr.ContragentTypeEnum.NPF;
                            //pp.LinkElID = (long) AsgFinTr.Links.NPF;
                            break;
                        case OnesDocKinds.K_UK:
                        case OnesDocKinds.K_UKV:
                            //TODO пока только СИ, но что с ВР?
                            pp.SectionElID = (long) AsgFinTr.Sections.SI;
                            pp.ContragentTypeID = (long)AsgFinTr.ContragentTypeEnum.UK;
                            //pp.LinkElID = (long) AsgFinTr.Links.UK;
                            break;   
                    }
                    var kbkEl =
                        payment.Descendants("{http://www.pfr.ru}Summs_PP")
                            .FirstOrDefault()?
                            .Descendants("{http://www.pfr.ru}Sum")
                            .FirstOrDefault();
                          //  .Descendants("{http://www.pfr.ru}KBK")
                           // .FirstOrDefault();
                    if (kbkEl != null)
                    {
                        var kbk = TrimComment(GetXmlFieldValue(kbkEl, "KBK"));
                        var item = GetListByProperty(typeof(KBK).FullName, "Code", kbk).Cast<KBK>().FirstOrDefault();
                        if (item != null)
                            pp.KBKID = item.ID;
                        else
                        {
                            var id = SaveEntity(new KBK
                            {
                                Code = kbk,
                                Name = $"ЗАПИСЬ ВНЕСЕНА ПРИ ИМПОРТЕ П/П ИЗ 1С <{DateTime.Today:dd.MM.yyyy}>"
                            });
                            pp.KBKID = id;
                        }
                    }

                    var accEl = payment.Descendants(type == OnesDocKinds.VYP ? "{http://www.pfr.ru}Organization_Rcp" : "{http://www.pfr.ru}Organization_Pay").FirstOrDefault();
                        if(accEl == null)
                            throw new XmlParseException($"Ошибка данных XML документа {filename}! Не найден тег Organization_XXX!");
                        pp.Comment = $"{TrimComment(GetXmlFieldValue(accEl, "Name"))}{Environment.NewLine}{TrimComment(GetXmlFieldValue(payment, "Purpose"))}";

                    bool ppFoundInn = false;
                    var ppInn = TrimComment(GetXmlFieldValue(accEl, "INN"));
                    var ppKpp = TrimComment(GetXmlFieldValue(accEl, "KPP"));
                    //ищем по ИНН счетов для УК и УКВ
                    if ((kind == OnesDocKinds.K_UK || kind == OnesDocKinds.K_UKV) && !string.IsNullOrEmpty(ppInn))
                    {
                        //ищем в договорах
                        var ppAccount =
                            GetListByPropertyConditions<BankAccount>(session, ListPropertyCondition.Equal("INN", ppInn), ListPropertyCondition.Equal("KPP", ppKpp))
                                .Cast<BankAccount>()
                                .FirstOrDefault();

                        if (ppAccount != null)
                        {
                            var contract =
                                GetListByPropertyConditions<PFR_INVEST.DataObjects.Contract>(session, ListPropertyCondition.Equal("BankAccountID", ppAccount.ID),
                                    ListPropertyCondition.NotEqual("Status", -1)).Cast<PFR_INVEST.DataObjects.Contract>().FirstOrDefault();
                            //нашли в договор
                            if (contract != null)
                            {
                                var le = GetByID<LegalEntity>(contract.LegalEntityID);
                                //у договора и LE все совпадает
                                if (le != null && le.INN == ppInn && le.OKPP == ppKpp)
                                {
                                    ppFoundInn = true;
                                    pp.InnContractID = contract.ID;
                                    pp.InnLegalEntityID = contract.LegalEntityID;
                                }
                                else
                                {
                                    //договор есть, но у LE другие ИНН и КПП
                                    //ищем чисто по LE
                                    var ppLe =
                                        GetListByPropertyConditions<LegalEntity>(session, ListPropertyCondition.Equal("INN", ppInn), ListPropertyCondition.Equal("OKPP", ppKpp))
                                            .Cast<LegalEntity>()
                                            .FirstOrDefault();
                                    if (ppLe != null)
                                    {
                                        pp.InnLegalEntityID = ppLe.ID;
                                        ppFoundInn = true;
                                    }
                                }
                            }
                        }
                        //если не нашли по договору, ищем по LE
                        //НИЖЕ

                        /* var ppAccount = string.IsNullOrEmpty(ppKpp)
                             ? GetListByPropertyConditions<BankAccount>(session, ListPropertyCondition.Equal("INN", ppInn)).Cast<BankAccount>().FirstOrDefault()
                             : GetListByPropertyConditions<BankAccount>(session, ListPropertyCondition.Equal("INN", ppInn), ListPropertyCondition.Equal("KPP", ppKpp)).Cast<BankAccount>().FirstOrDefault();
                         if (ppAccount != null)
                         {
                             var contract =
                                 GetListByPropertyConditions<PFR_INVEST.DataObjects.Contract>(session, ListPropertyCondition.Equal("BankAccountID", ppAccount.ID), ListPropertyCondition.NotEqual("Status", -1)).Cast<PFR_INVEST.DataObjects.Contract>().FirstOrDefault();
                             if (contract != null)
                             {
                                 ppFoundInn = true;
                                 pp.InnContractID = contract.ID;
                             }
                         }*/
                    }
                    //ищем по ИНН субъектов
                    if (!ppFoundInn && !string.IsNullOrEmpty(ppInn))
                    {
                        var ppLe = string.IsNullOrEmpty(ppKpp)
                            ? GetListByPropertyConditions<LegalEntity>(session, ListPropertyCondition.Equal("INN", ppInn)).Cast<LegalEntity>().FirstOrDefault()
                            : GetListByPropertyConditions<LegalEntity>(session, ListPropertyCondition.Equal("INN", ppInn), ListPropertyCondition.Equal("OKPP", ppKpp)).Cast<LegalEntity>().FirstOrDefault();
                        if (ppLe != null)
                            pp.InnLegalEntityID = ppLe.ID;
                    }

                    var result = SaveEntity(pp, session);
                    SaveJornalLog(JournalEventType.IMPORT_DATA, "Платежное поручение", result, pp.GetType().Name);
                    return result;
                case OnesDocKinds.K_SOF:
                case OnesDocKinds.K_MSK:
                case OnesDocKinds.K_DOST:
                case OnesDocKinds.K_DOSTV:
                    var op2 = payment.Descendants(kind == OnesDocKinds.K_DOST ? "{http://www.pfr.ru}Organization_Rcp" : "{http://www.pfr.ru}Organization_Pay").FirstOrDefault();
                    if(op2 == null)
                        throw new XmlParseException($"Ошибка данных XML документа {filename}! Не найден тег Organization_XXX!");

                    //get account
                    var accountNode = payment.Descendants(kind == OnesDocKinds.K_DOST ? "{http://www.pfr.ru}Account_Rcp" : "{http://www.pfr.ru}Account_Pay").FirstOrDefault();
                    if (accountNode == null)
                        throw new XmlParseException($"Ошибка данных XML документа {filename}! Не найден тег Account_XXX!");
                    var accNum = GetXmlFieldValue(accountNode, "BS");
                    //var aBIC = GetXmlFieldValue(accountNode, "BIC");
                    //var aNameBIC = GetXmlFieldValue(accountNode, "Name_BIC");
                   // var aBsKs = GetXmlFieldValue(accountNode, "BS_KS");

                    var account = string.IsNullOrEmpty(accNum) ? null : session.CreateQuery("FROM PfrBankAccount a where a.AccountNumber=:num and a.StatusID<>-1")
                        .SetParameter("num", accNum)
                        .SetMaxResults(1)
                        .UniqueResult<PfrBankAccount>();

                    var accOp = new AccOperation
                    {
                        OperationDate = DateTime.Today,
                        PFRBankAccountID = account?.ID ?? _onesImportDefaultBAcc.ID,
                        SourcePFRBankAccountID = null,
                        PortfolioID = _onesImportDefaultPortfolio.ID,
                        NewDate = DateTime.Parse(GetXmlFieldValue(payment, "DateOperation")),
                        Content = kind == OnesDocKinds.K_DOST ? "Расходы на доставку" : "Прочие доходы",
                        KindID = kind == OnesDocKinds.K_DOST ? 4 : 2,
                        CurrencyID = account == null ? _onesImportDefaultBAcc.CurrencyID : account.CurrencyID,
                        CursID = null,
                        Summ = Convert.ToDecimal(GetXmlFieldValue(payment, "Sum_Payment"), new NumberFormatInfo()),
                        Comment = $"{TrimComment(GetXmlFieldValue(op2, "Name"))}{Environment.NewLine}{TrimComment(GetXmlFieldValue(payment, "Purpose"))}"
                    };
                    var result2 =  SaveEntity(accOp, session);
                    SaveJornalLog(JournalEventType.IMPORT_DATA, "Зачисление поступлений", result2, accOp.GetType().Name);
                    return result2;
            }
            return null;
        }

        private static string GetXmlFieldValue(XElement node, string name)
        {
            var fNode = node.Nodes().FirstOrDefault(a => ((XElement)a).Name.LocalName == name);
            //throw new XmlParseException(string.Format("Ошибка данных XML документа {0}! Не найден {1}!", filename, name));
            return ((XElement) fNode)?.Value;
        }

        #endregion


        #region Работа с Журналами 1С

        private void OnesLogJournal(string message, long sessionId, OnesJournal.Actions action = OnesJournal.Actions.General, OnesJournal.LogTypes logType = OnesJournal.LogTypes.Info, long? importId = null, ISession session = null)
        {
            var msg = string.IsNullOrEmpty(message) ? "" : message.Length > 2500 ? message.Substring(0, 2499) : message;
            SaveEntity(new OnesJournal
            {Message = msg,
                SessionID = sessionId,
                ImportID = importId,
                Action = (int)action,
                LogType = (int)logType,
                LogDate = DateTime.Now
            }, session);
        }

        public List<OnesJournal> GetOnesExportJournal(int index, DateTime? periodStart, DateTime? periodEnd)
        {
            return TransactionWrapper(session => session.CreateQuery(@"FROM OnesJournal j where (j.LogDate >=:dateFrom and j.LogDate <=:dateTo) and j.Action in (:idList)")
                .SetParameter("dateFrom", periodStart?.Date ?? DateTime.MinValue.Date)
                .SetParameter("dateTo", periodEnd?.Date ?? DateTime.MaxValue.Date)
                .SetParameterList("idList", new List<int> {(int) OnesJournal.Actions.ExportComplete, (int) OnesJournal.Actions.Export})
                .SetFirstResult(index)
                .SetMaxResults(PageSize).List<OnesJournal>()
                .ToList(), IsolationLevel.ReadUncommitted);
        }

        
        public long GetOnesExportJournalListCount(DateTime? periodStart, DateTime? periodEnd)
        {
            return TransactionWrapper(session => session.CreateQuery(@"select count(j.ID) FROM OnesJournal j where (j.LogDate >=:dateFrom and j.LogDate <=:dateTo) and j.Action in (:idList)")
                .SetParameter("dateFrom", periodStart?.Date ?? DateTime.MinValue.Date)
                .SetParameter("dateTo", periodEnd?.Date ?? DateTime.MaxValue.Date)
                .SetParameterList("idList", new List<int> { (int)OnesJournal.Actions.ExportComplete, (int)OnesJournal.Actions.Export })
                .UniqueResult<long>(), IsolationLevel.ReadUncommitted);
        }

        public RepositoryNoAucImpExpFile GetOnesXmlStorageItem(long id)
        {
            //обеспечиваем loose транзакцию для возможности чтения в процессе работы экспорта
            return TransactionWrapper(session => GetByID<RepositoryNoAucImpExpFile>(id), IsolationLevel.ReadUncommitted);
        }

        public long OnesImportErrorsCount(DateTime? periodStart, DateTime? periodEnd, int exportComplete, string s)
        {
            return TransactionWrapper(session =>
            {
                var result = session.CreateSQLQuery($@"SELECT j.ID
                                                       FROM PFR_BASIC.ONES_FILEIMPORT fi
                                                       RIGHT JOIN PFR_BASIC.ONES_JOURNAL j ON j.IMPORT_ID = fi.ID
                                                       WHERE (j.LOG_DATE IS NULL OR (j.LOG_DATE >= :dateFrom AND j.LOG_DATE <= :dateTo)) 
                                                            AND j.ACTION < :expComplete {(s != null ? $" AND fi.DOC_KIND='{s}'" : null)}
                                                   ")
                    .SetParameter("dateFrom", periodStart?.Date ?? DateTime.MinValue.Date)
                    .SetParameter("dateTo", periodEnd?.Date ?? DateTime.MaxValue.Date)
                    .SetParameter("expComplete", exportComplete)
                    .List<long>();
                return result.Count;
            });
        }

        public List<OnesErrorJournalListItem> GetOnesErrorJournal(int index, DateTime? periodStart, DateTime? periodEnd, int exportComplete, string s)
        {
            return TransactionWrapper(session =>
            {
                var arr = session.CreateSQLQuery(
                    $@"SELECT j.SESSION_ID, j.LOG_DATE, fi.DOC_KIND, j.LOG_TYPE, fi.FILE_ID, j.MESSAGE
                                                       FROM PFR_BASIC.ONES_FILEIMPORT fi
                                                       RIGHT JOIN PFR_BASIC.ONES_JOURNAL j ON j.IMPORT_ID = fi.ID
                                                       WHERE (j.LOG_DATE IS NULL OR (j.LOG_DATE >= :dateFrom AND j.LOG_DATE <= :dateTo))
                                                        AND j.ACTION < :expComplete {(s != null ? $" AND fi.DOC_KIND='{s}'" : null)}")
                                                    .SetParameter("dateFrom", periodStart?.Date ?? DateTime.MinValue.Date)
                                                    .SetParameter("dateTo", periodEnd?.Date ?? DateTime.MaxValue.Date)
                                                    .SetParameter("expComplete", exportComplete)
                                                .SetFirstResult(index)
                                                .SetMaxResults(PageSize)
                                                .List<object[]>();
                var list = new List<OnesErrorJournalListItem>();
                arr?.ForEach(a =>
                {
                    int startIndex = a.Length == 6 ? 0 : 1;
                    list.Add(new OnesErrorJournalListItem
                    {
                        SessionID = (long) (a[startIndex] ?? 0L),
                        LoadDate = a[startIndex + 1] == null ? null : (DateTime?)DateTime.Parse(a[startIndex + 1].ToString()),
                        DocKind = a[startIndex + 2]?.ToString() ?? "",
                        LogType = (int)(a[startIndex + 3] ?? 0),
                        FileID = (long)(a[startIndex + 4] ?? 0L),
                        Message = (string)(a[startIndex + 5] ?? "")
                    });
                });

                return list;
            }, IsolationLevel.ReadUncommitted);
        }
        #endregion

        #region Обработка настроек по импорту/экспорту 1С
        public List<string> GetOnesKindsList()
        {
            return SessionWrapper(session =>
            {
                return session.CreateCriteria(typeof(OnesFileImport))
                    .List<OnesFileImport>()
                    .Select(a => a.DocKind)
                    .GroupBy(a => a)
                    .Select(a => a.Key)
                    .ToList();
            });
        }

        public List<long> SaveOnesOpSettings(List<OnesOpSettings> addList, List<long> remList)
        {
            return TransactionWrapper(session =>
            {
                var list = new List<long>();
                addList.ForEach(a =>
                {
                    var res = session.CreateQuery(@"select count(s.ID) from OnesOpSettings s where s.SetDefID =:defid and s.OperationContentID=:cid and s.PortfolioTypeID=:pf")
                        .SetParameter("defid", a.SetDefID)
                        .SetParameter("cid", a.OperationContentID)
                        .SetParameter("pf", a.PortfolioTypeID)
                        .UniqueResult<long>();
                    if(res == 0)
                        list.Add(SaveEntity(a, session));
                });
                DeleteEntities<OnesOpSettings>(remList, session);
                return list;
            }, IsolationLevel.ReadUncommitted);
        }
        #endregion

        private static ImportFileListItem OnesReadFileFromServer(string fileName)
        {
            return new ImportFileListItem { FilePath = fileName, FileContent = File.ReadAllBytes(fileName) };
        }
    }

    internal class XmlParseException : Exception
    {
        public XmlParseException(string format): base(format)
        {
        }
    }
}
