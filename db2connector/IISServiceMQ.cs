﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using PFR_INVEST.Core.MQConnector;
using PFR_INVEST.Proxy.Tools;

namespace db2connector
{
    /// <summary>
    /// Часть сервиса - реализация работы с MQ
    /// </summary>
    public partial class IISServiceHib
    {
        private const string REMOVE_SUCCESS_LOADED_REPORTS_KEY = "RemoveSuccessLoadedODKXMLReports";

        public long LoadEDOReports(string path)
        {
            int count = 0;
            if (Directory.Exists(path))
            {
                var oXMLFiles = new DirectoryInfo(path).GetFiles("*.xml", SearchOption.AllDirectories);

                foreach (var xmlFile in oXMLFiles)
                { 
                    using (var myFile = new StreamReader(xmlFile.FullName, Encoding.GetEncoding("windows-1251")))
                        myFile.ReadToEnd();

                    count++;
                }
            }
            else return -2;

            return count;
        }

        public long LoadODKReports(string managerName,  string qName, string path)
        {
            int count = 0;
            LogManager.Log($"{managerName} | {qName} | {path}");

            try
            {
                var connector = new MQCoreConnector(managerName, "DurableSubscription.DOKIP.", null, message => LogManager.Log(message), exception => LogManager.LogException(exception));
                var oXMLFiles = new DirectoryInfo(path).GetFiles("*.xml", SearchOption.AllDirectories);

                ConfigurationManager.RefreshSection("appSettings");
                bool remove;
                if (!bool.TryParse(ConfigurationManager.AppSettings[REMOVE_SUCCESS_LOADED_REPORTS_KEY], out remove))
                    remove = false;

                foreach (var xmlFile in oXMLFiles)
                {
                    string strXML;
                    using (var myFile = new StreamReader(xmlFile.FullName, Encoding.GetEncoding("windows-1251")))
                        strXML = myFile.ReadToEnd();

                    if (strXML != string.Empty)
                    {
                        //Вставляем имя файла как подноду EDO_ODKXXX
                        int first = strXML.IndexOf("<EDO_ODK", StringComparison.Ordinal);
                        if (first != -1)
                        {
                            int end = strXML.IndexOf(">", first, StringComparison.Ordinal);
                            if (end != -1)
                                strXML = strXML.Insert(end+1, $"\r\n\t<FILENAME>{Path.GetFileName(xmlFile.FullName)}</FILENAME>");
                        }
                        
                        if (connector.QueueMessage(strXML, qName, 1251, 1251) && remove)
                            File.Delete(xmlFile.FullName);
                        count++;
                    }
                }
            }
            catch (Exception e)
            {
                LogExceptionForHib(e);
                return -1;
            }

            LogManager.Log($"All {count} XML files sended to MQ");
            return count;
        }
    }
}
