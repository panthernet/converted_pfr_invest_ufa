﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PFR_INVEST.Auth.ServerData;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;

namespace PFR_INVEST.Auth.Mock
{
    public class AuthConnectorMock: IAuthConnector
    {
        #region Список доступных, распознаваемых логинов
        public const string USER_ADMIN =  "admin";
        public const string USER_OVSI_V = "ovsiv";
        public const string USER_OVSI_W = "ovsiw";
        public const string USER_OVSI_E = "ovsie";
        public const string USER_OVSI_M = "ovsim";
        public const string USER_OFPR_V = "ofprv";
        public const string USER_OFPR_W = "ofprw";
        public const string USER_OFPR_E = "ofpre";
        public const string USER_OFPR_M = "ofprm";
        public const string USER_OSRP_V = "osrpv";
        public const string USER_OSRP_W = "osrpw";
        public const string USER_OSRP_E = "osrpe";
        public const string USER_OSRP_M = "osrpm";
        public const string USER_OKIP_V = "okipv";
        public const string USER_OKIP_W = "okipw";
        public const string USER_OKIP_E = "okipe";
        public const string USER_OKIP_M = "okipm";
        public const string USER_OARRS_V = "oarrsv";
        public const string USER_OARRS_W = "oarrsw";
        public const string USER_OARRS_E = "oarrse";
        public const string USER_OARRS_M = "oarrsm";
        public const string USER_OUFV_V = "oufvv";
        public const string USER_OUFV_W = "oufvw";
        public const string USER_OUFV_E = "oufve";
        public const string USER_OUFV_M = "oufvm";
        #endregion

        /// <summary>
        /// Тестовый юзер для отладки работы с правами в настройках клиента
        /// </summary>
        private readonly User _testUser = new User("TestUser", " Test User")
        {
            Roles = new List<Role>
            {
                new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.User)),
                new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.Administrator))
            }
        };

        /// <summary>
        /// Сущносьб текущего юзера, работающего с коннектором
        /// </summary>
        private readonly User _currentuser;

        public AuthConnectorMock(string userName)
        {
            _currentuser = new User(userName, userName);
        }

        /// <summary>
        /// Возвращает список ролей назначенных указанному пользователю
        /// </summary>
        /// <param name="ptkRoles"></param>
        /// <returns>Список ролей</returns>
        private static List<Role> SetPTKShowName(List<Role> ptkRoles)
        {
            foreach (var ptkRole in ptkRoles)
            {
                ptkRole.SetShowName();
            }
            return ptkRoles;
        }


        public ServiceAuthMsg Init()
        {
            _currentuser.Roles = new List<Role> { new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.User)) };

            //назначаем права согласно логину, по умолчанию = админ
            switch (_currentuser.Login.ToLower())
            {
                default:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.Administrator)));
                    break;
                case USER_OVSI_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_worker)));
                    break;
                case USER_OVSI_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_directory_editor)));
                    break;
                case USER_OVSI_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_manager)));
                    break;
                case USER_OFPR_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_worker)));
                    break;
                case USER_OFPR_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_directory_editor)));
                    break;
                case USER_OFPR_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_manager)));
                    break;
                case USER_OSRP_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_worker)));
                    break;
                case USER_OSRP_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_directory_editor)));
                    break;
                case USER_OSRP_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_manager)));
                    break;
                case USER_OKIP_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_worker)));
                    break;
                case USER_OKIP_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_directory_editor)));
                    break;
                case USER_OKIP_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_manager)));
                    break;
                case USER_OARRS_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_worker)));
                    break;
                case USER_OARRS_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_directory_editor)));
                    break;
                case USER_OARRS_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_manager)));
                    break;
                case USER_OUFV_W:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_worker)));
                    break;
                case USER_OUFV_E:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_directory_editor)));
                    break;
                case USER_OUFV_M:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_worker)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_directory_editor)));
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_manager)));
                    break;

                case USER_OKIP_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OKIP_viewer)));
                    break;
                case USER_OUFV_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OUFV_viewer)));
                    break;
                case USER_OVSI_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OVSI_viewer)));
                    break;
                case USER_OARRS_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OARRS_viewer)));
                    break;
                case USER_OFPR_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OFPR_viewer)));
                    break;
                case USER_OSRP_V:
                    _currentuser.Roles.Add(new Role(UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.OSRP_viewer)));
                    break;
            }
            SetPTKShowName(_testUser.Roles);
            SetPTKShowName(_currentuser.Roles);

            return new ServiceAuthMsg(AuthMsg.Success);
        }

        public ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password)
        {
            return new ServiceAuthMsg(AuthMsg.Success);
        }

        public object GetToken()
        {
            return Guid.NewGuid().ToString();
        }

        public bool IsValidRequest(object token)
        {
            //запрос всегда валидный, не проверяем токены на секьюрность
            return true;
        }

        public List<User> GetGroupChildUsers(string value1)
        {
            var list = new List<User>();
            if(_testUser.Roles.FirstOrDefault(a=> a.Name == value1) != null)
                list.Add(_testUser);
            if(_currentuser.Roles.FirstOrDefault(a=> a.Name == value1) != null)
                list.Add(_currentuser);
            return list;
        }

        public List<Role> GetRoles(string userName = null)
        {
            return userName == "TestUser" ? _testUser.Roles : _currentuser.Roles;
        }

        public bool IsAdmin()
        {
            return _currentuser.Roles.FirstOrDefault(a => a.Name == UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.Administrator)) != null;
        }

        public User AddUserToGroup(string userName, string groupName)
        {
            if (userName == "TestUser")
            {
                _testUser.Roles.Add(new Role(groupName));
                return _testUser;
            }
            _currentuser.Roles.Add(new Role(groupName));
            return _currentuser;
        }

        public User RemoveUserFromGroup(string userName, string groupName)
        {
            if (userName == "TestUser")
            {
                _testUser.Roles.Remove(_testUser.Roles.FirstOrDefault(a=> a.Name == groupName));
                return _testUser;
            }
            _currentuser.Roles.Remove(_currentuser.Roles.FirstOrDefault(a => a.Name == groupName));
            return _currentuser;
        }

        public List<User> GetUsersNotInGroup(string groupName)
        {
            var list = new List<User>();
            if (_testUser.Roles.FirstOrDefault(a => a.Name == groupName) == null)
                list.Add(_testUser);
            if (_currentuser.Roles.FirstOrDefault(a => a.Name == groupName) == null)
                list.Add(_currentuser);
            return list;
        }

        public bool IsUserInRole(string userName, string role)
        {
            if (userName == "TestUser" && _testUser.Roles.FirstOrDefault(a => a.Name == role) != null)
                return true;
            if (userName == _currentuser.Name && _currentuser.Roles.FirstOrDefault(a => a.Name == role) != null)
                return true;
            return false;
        }

        public User IsUserExist(string dn)
        {
            if (dn == "TestUser") return _testUser;
            if (dn == _currentuser.Name) return _currentuser;
            return null;
        }

        public User GetUserInfo(string userName)
        {
            if (userName == "TestUser") return _testUser;
            if (userName == _currentuser.Name) return _currentuser;
            return null;
        }
    }
}
