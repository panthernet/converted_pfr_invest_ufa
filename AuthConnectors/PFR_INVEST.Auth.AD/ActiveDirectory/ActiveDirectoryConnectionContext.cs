using System;
using System.Linq;

namespace PFR_INVEST.Auth.AD.ActiveDirectory
{
    public class ActiveDirectoryConnectionContext
    {
        public string LdapServer { get; set; }
        public string LdapDistinguishedName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string GetActiveDirectoryConnectionString(string userName = null)
        {
            var result = string.Format("LDAP://{0}", LdapServer);
            if (!string.IsNullOrEmpty(LdapDistinguishedName))
            {
                var components = LdapDistinguishedName.Split(new[] {"."},
                                                             StringSplitOptions.RemoveEmptyEntries);
                components = components.Select(dc => string.Format("DC={0}", dc)).ToArray();
                result = string.Format("{0}/{2}{1}", result, string.Join(",", components), string.IsNullOrEmpty(userName) ? null : string.Format("CN={0},CN=Users,",userName));
            }
            //result = "LDAP://10.102.0.48/DC=center,DC=pfr,DC=ru";
            //TODO LogManager.getSingleton().LogAD(result);
            return result;
        }
    }
}