﻿using log4net;

namespace PFR_INVEST.Auth.AD
{
    public static class Logs
    {
        internal static readonly ILog AD = LogManager.GetLogger("Auth");

    }
}
