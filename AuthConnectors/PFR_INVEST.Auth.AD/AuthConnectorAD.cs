﻿using System;
using System.Collections.Generic;
using PFR_INVEST.Auth.AD.ActiveDirectory;
using PFR_INVEST.Auth.ServerData;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Common.Tools;
using ActiveDirectoryClass = PFR_INVEST.Auth.AD.ActiveDirectory.ActiveDirectory;

namespace PFR_INVEST.Auth.AD
{
    public class AuthConnectorAD : IAuthConnector
    {
        private ActiveDirectoryClass _activeDirectory;
        private bool m_UserIsPtkDokipAdmin;

        /// <summary>
        /// Идентификатор точки доступа.
        /// </summary>
        private string _accessToken;

        public AuthConnectorAD(string userName, string password)
        {
            var context = new ActiveDirectoryConnectionContext
            {
                LdapServer =
#if WEBCLIENT
                    PFR_INVEST.Web2.Auth.
#endif
                    Properties.Settings.Default.ADServerAddress,
                LdapDistinguishedName = 
#if WEBCLIENT
                    PFR_INVEST.Web2.Auth.
#endif
                    Properties.Settings.Default.ADServerUserDomain,
                UserName = userName,
                Password = password
            };
            _activeDirectory = new ActiveDirectoryClass(context);
            
        }

        public bool IsAdmin()
        {
            return m_UserIsPtkDokipAdmin;
        }

        public User AddUserToGroup(string userName, string groupName)
        {
            _activeDirectory.AddUserToGroup(userName, groupName);
            return Extensions.MapFromAdUser(_activeDirectory.GetUserInfo(userName));
        }

        public User RemoveUserFromGroup(string userName, string groupName)
        {
            _activeDirectory.RemoveUserFromGroup(userName, groupName);
            return Extensions.MapFromAdUser(_activeDirectory.GetUserInfo(userName));
        }

        public List<User> GetUsersNotInGroup(string groupName)
        {
            return Extensions.MapFromAdUsers(_activeDirectory.GetUsersNotInGroup(groupName));
        }

        public bool IsUserInRole(string userName, string role)
        {
            return _activeDirectory.IsUserInRole(userName, role);
        }

        public User IsUserExist(string dn)
        {
            return Extensions.MapFromAdUser(_activeDirectory.IsUserExist(dn));
        }

        public User GetUserInfo(string userName)
        {
            return Extensions.MapFromAdUser(_activeDirectory.GetUserInfo(userName));
        }

        public ServiceAuthMsg Init()
        {
            var result = _activeDirectory.Validate();
            if (result == ADValidationResult.Success)
            {
                m_UserIsPtkDokipAdmin = IsUserInRole(_activeDirectory.Context.UserName, UserSecurity.GetRoleADName(DOKIP_ROLE_TYPE.Administrator));
                return new ServiceAuthMsg(AuthMsg.Success);
            }
            switch (result)
            {
                case ADValidationResult.UserMustChangePassword:
                    return new ServiceAuthMsg(AuthMsg.UserMustChangePassword, string.Format("Ошибка авторизации в AD! Login: {0} Domain: {1} Error: {2}", _activeDirectory.Context.UserName, _activeDirectory.Context.LdapDistinguishedName, result.GetDescription()));
                default:
                    return new ServiceAuthMsg(AuthMsg.Denied);
            }
        }

        public ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password)
        {
            //создаем логин АД
            var context = new ActiveDirectoryConnectionContext
            {
                LdapServer = 
#if WEBCLIENT
                    PFR_INVEST.Web2.Auth.
#endif
                    Properties.Settings.Default.ADServerAddress,
                LdapDistinguishedName = 
#if WEBCLIENT
                    PFR_INVEST.Web2.Auth.
#endif
                    Properties.Settings.Default.ADServerUserDomain,
                UserName = userName,
                Password = oldPassword
            };
            _activeDirectory = new ActiveDirectoryClass(context);

            //пытаемся сменить пароль юзеру
            var result = _activeDirectory.ChangeUserPassword(userName, oldPassword, password);
            switch (result)
            {
                case ADPasswordSetResult.Success:
                    result = ADPasswordSetResult.SuccessNoLogin;
                    //обновляем котекст АД на юзера, которому меняем пароль
                    context.UserName = userName;
                    context.Password = password;
                    //валидируем юзера
                    if (_activeDirectory.Validate() == ADValidationResult.Success)
                    {
                        //     LogManager.getSingleton().LogAD("AD pwd ch: user validated after pwd change");
                        result = ADPasswordSetResult.Success;
                        return new ServiceAuthMsg(AuthMsg.Success);
                        //  else LogManager.getSingleton().LogAD("AD pwd ch: user validation failed after pwd change!!!");
                    }
                    return new ServiceAuthMsg(AuthMsg.SuccessNoLogin);
                case ADPasswordSetResult.FailPwdPolicy:
                    return new ServiceAuthMsg(AuthMsg.FailPwdPolicy);
                case ADPasswordSetResult.FailPwdWrong:
                    return new ServiceAuthMsg(AuthMsg.FailPwdWrong);
                default:
                    return new ServiceAuthMsg(AuthMsg.Denied);
            }
        }

        /// <summary>
        /// Возвращает уникальный идентификатор сессии, который действителен в течении всего периода действия сессии.
        /// </summary>
        public object GetToken()
        {
            _accessToken = Guid.NewGuid().ToString();
            return _accessToken;
        }

        /// <summary>
        /// Проверка на валидность запроса
        /// </summary>
        /// <param name="token">Токен безопасности</param>
        public bool IsValidRequest(object token)
        {
            Logs.AD.Debug(string.Format("ADToken: {0}  iToken: {1}", _accessToken, token));
            return _accessToken == (string)token;
        }

        public List<User> GetGroupChildUsers(string groupName)
        {
            return Extensions.MapFromAdUsers(_activeDirectory.GetGroupChildUsers(groupName));
        }

        public List<Role> GetRoles(string userName = null)
        {
            var result = string.IsNullOrEmpty(userName) ? _activeDirectory.GetGroupInfos() : _activeDirectory.GetUserGroups(userName);
            var ptkRoles = Extensions.MapFromAdGroups(result);
            ptkRoles = ptkRoles.FindAll(ad => ad.Name.StartsWith("PTK_DOKIP"));
            ptkRoles = SetPTKShowName(ptkRoles);
            return ptkRoles;
        }

        /// <summary>
        /// Возвращает список ролей назначенных указанному пользователю
        /// </summary>
        /// <param name="ptkRoles"></param>
        /// <returns>Список ролей</returns>
        private static List<Role> SetPTKShowName(List<Role> ptkRoles)
        {
            foreach (var ptkRole in ptkRoles)
            {
                ptkRole.SetShowName();
            }
            return ptkRoles;
        }
    }
}
