﻿using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData
{
    [DataContract]
    public class ServiceAuthMsg
    {
        [DataMember]
        public AuthMsg Response { get; private set; }
        [DataMember]
        public object ResultValue { get; private set; }

        public ServiceAuthMsg(AuthMsg response, object value = null)
        {
            Response = response;
            ResultValue = value;
        }

        public ServiceAuthMsg()
        {         
        }
    }

    [DataContract]
    public enum AuthMsg
    {
        [EnumMember]
        FatalError = -1,
        [EnumMember]
        Success = 0,
        [EnumMember]
        NotSupported = 1,
        [EnumMember]
        Denied = 2,
        [EnumMember]
        UserMustChangePassword = 3,
        [EnumMember]
        FailPwdPolicy = 4,
        [EnumMember]
        FailPwdWrong = 5,
        [EnumMember]
        SuccessNoLogin = 6
    }
}
