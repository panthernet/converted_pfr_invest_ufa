﻿using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    [DataContract]
    //[DataContract]
    public class AuthResponse
    {
        [DataMember]
        public AcknowledgeType Acknowledge { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public object Result { get; set; }

        public AuthResponse()
        {
            Acknowledge = AcknowledgeType.Success;
        }

    }
}
