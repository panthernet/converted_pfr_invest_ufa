﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PFR_INVEST.Auth.SharedData.Auth
{
    /// <summary>
    /// Пользователь
    /// </summary>
    [DataContract]
    public class User
    {
        /// <summary>
        /// Логин
        /// </summary>
        [DataMember]
        public string Login { get; set; }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Список ролей доступных пользователю
        /// </summary>
        [DataMember]
        public List<Role> Roles { get; set; }

        public User()
        {
        }

        public User(string login)
        {
            Login = login;
        }

        public User(string login, string name)
        {
            Login = login;
            Name = name;
        }
    }
}