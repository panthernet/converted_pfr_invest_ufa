﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PFR_INVEST.Auth.ServerData;
using PFR_INVEST.Auth.SharedData;
using PFR_INVEST.Auth.SharedData.Auth;
using PFR_INVEST.Core.BusinessLogic;
using PFR_INVEST.Core.Tools;
using PFR_INVEST.Core.ListExtensions;

namespace PFR_INVEST.Auth.ECASA
{
    public class AuthConnectorECASA : IAuthConnector
    {
        /// <summary>
        /// Он же по идее userGuid
        /// </summary>
        private readonly string _userName;
        /// <summary>
        /// Ecasa auth user token
        /// </summary>
      //  private readonly string _userGuid;
        /// <summary>
        /// ECASA app token
        /// </summary>
        private readonly string _appId;
        /// <summary>
        /// ECASA session token
        /// </summary>
        private readonly string _sessionId;

        private bool _mUserIsPtkDokipAdmin;
        private List<Role> _roles;
        private User _currentUser;

        public AuthConnectorECASA(string userName, string sessionId, bool isNormal)
        {
            _userName = userName;
            _appId = "DOKIP";
            _sessionId = sessionId;
            //если быстрый коннект, присваиваем null сессии идентификатор приложения для корректности токена
            if (!isNormal)
                _sessionId = _sessionId ?? _appId;
        }

        public ServiceAuthMsg Init()
        {
            try
            {
#if WEBCLIENT
                using (var client = new Web2.Auth.VerifyAuthenticationWSService.VerifyAuthenticationWSClient())
#else
                using (var client = new VerifyAuthenticationWSService.VerifyAuthenticationWSClient())

#endif
                {
                    if (!client.isLoggedIn(_sessionId, $"dn={_userName}"))
                    {
                        Logs.Log.Error($@"Can't validate verify ECASA session {_sessionId} through isLoggedIn!");
                        //return new ServiceAuthMsg(AuthMsg.Denied, "Аутентификация в ЕЦАСА не пройдена!");
                    }
                    else
                        Logs.Log.Error("ECASA session verified successfuly!");
                }


                //получаем карту прав юзера
                _roles = GetRolesFromMap();
                _currentUser = new User {Login = _userName, Name = _userName, Roles = _roles};
                return new ServiceAuthMsg( _roles == null || _roles.Count == 0 ? AuthMsg.Denied : AuthMsg.Success);
            }
            catch (Exception ex)
            {
                Logs.Log.Error("Init exception", ex);
                return new ServiceAuthMsg(AuthMsg.FatalError, ex.Message);
            }
            
        }

        private List<Role> GetRolesFromMap(string userName = null)
        {
            var list = new List<Role>();
            var name = string.IsNullOrEmpty(userName) ? _userName : userName;
            var query =
#if WEBCLIENT
                PFR_INVEST.Web2.Auth.
#endif
                Properties.Settings.Default.MAPRequestUrl.Replace("{userGuid}", name).Replace("{appId}", _appId);
            var result = (JArray)JsonConvert.DeserializeObject(WebHelper.HttpGet(query, name));
            if (result == null || result.Count == 0) return list;
            foreach (var item in result)
            {
                if(!item.HasValues) continue;
                item.Values().ForEach(a =>
                {
                    switch (a.Value<string>())
                    {
                        case "core.a_admin":
                            _mUserIsPtkDokipAdmin = true;
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.Administrator));
                            break;
                        case "core.a_osrp_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OSRP_viewer));
                            break;
                        case "core.a_osrp_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OSRP_worker));
                            break;
                        case "core.a_osrp_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OSRP_directory_editor));
                            break;
                        case "core.a_osrp_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OSRP_manager));
                            break;
                        case "core.a_okip_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OKIP_viewer));
                            break;
                        case "core.a_okip_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OKIP_worker));
                            break;
                        case "core.a_okip_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OKIP_directory_editor));
                            break;
                        case "core.a_okip_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OKIP_manager));
                            break;
                        case "core.a_ofpr_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OFPR_viewer));
                            break;
                        case "core.a_ofpr_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OFPR_worker));
                            break;
                        case "core.a_ofpr_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OFPR_directory_editor));
                            break;
                        case "core.a_ofpr_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OFPR_manager));
                            break;
                        case "core.a_oarrs_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OARRS_viewer));
                            break;
                        case "core.a_oarrs_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OARRS_worker));
                            break;
                        case "core.a_oarrs_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OARRS_directory_editor));
                            break;
                        case "core.a_oarrs_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OARRS_manager));
                            break;
                        case "core.a_ovsi_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OVSI_viewer));
                            break;
                        case "core.a_ovsi_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OVSI_worker));
                            break;
                        case "core.a_ovsi_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OVSI_directory_editor));
                            break;
                        case "core.a_ovsi_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OVSI_manager));
                            break;
                        case "core.a_oufv_viewer":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OUFV_viewer));
                            break;
                        case "core.a_oufv_worker":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OUFV_worker));
                            break;
                        case "core.a_oufv_editor":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OUFV_directory_editor));
                            break;
                        case "core.a_oufv_manager":
                            list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.OUFV_manager));
                            break;
                    }
                });
            }
            if (list.Count != 0)
                list.Add(UserSecurity.CreatePtkRole(DOKIP_ROLE_TYPE.User));
            return list;
        }


#region IAuthConnector
        public ServiceAuthMsg ChangeCurrentUserPassword(string userName, string oldPassword, string password)
        {
            return new ServiceAuthMsg(AuthMsg.NotSupported);
        }

        public object GetToken()
        {
            return _sessionId;
        }

        public bool IsValidRequest(object token)
        {
            return _sessionId == token.ToString();
        }

        public List<User> GetGroupChildUsers(string value1)
        {
            return new List<User> { _currentUser };
        }

        public List<Role> GetRoles(string userName = null)
        {
            return string.IsNullOrEmpty(userName) ? _roles : GetRolesFromMap(userName);
        }

        public bool IsAdmin()
        {
            return _mUserIsPtkDokipAdmin;
        }

        public User AddUserToGroup(string userName, string groupName)
        {
            throw new Exception("Операция не поддерживается!");
        }

        public User RemoveUserFromGroup(string userName, string groupName)
        {
            throw new Exception("Операция не поддерживается!");
        }

        public List<User> GetUsersNotInGroup(string groupName)
        {
            return null;
        }

        public bool IsUserInRole(string userName, string role)
        {
            if (userName == _userName)
                return _roles.FirstOrDefault(a=> a.Name == role) != null;
            return GetRolesFromMap(userName).FirstOrDefault(a => a.Name == role) != null;
        }

        public User IsUserExist(string dn)
        {
            var roles = GetRolesFromMap(dn);
            return roles.Any() ? new User(dn, dn) {Roles = roles} : null;
        }

        public User GetUserInfo(string userName)
        {
            return new User(userName, userName) { Roles = GetRolesFromMap(userName)};
        }
#endregion
    }
}
