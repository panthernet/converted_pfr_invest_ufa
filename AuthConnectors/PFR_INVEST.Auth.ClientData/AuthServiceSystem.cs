﻿using db2connector;

namespace PFR_INVEST.Auth.ClientData
{
    public class AuthServiceSystem
    {
        public static string EndPointConfigName = "WSHttpBinding_IDB2Connector";
        public static string ECASAEndPointConfigName = "WSHttpsBinding_IDB2Connector";

        public static AuthServiceClient CreateNewDataClient(bool isEcasa)
        {
            var client = new AuthServiceClient(isEcasa ? ECASAEndPointConfigName : EndPointConfigName);
            return client;
        }

        public static GetDataService GetDataServiceDelegate { get; set; }

        internal static IDB2Connector Client => GetDataServiceDelegate();

        public delegate IDB2Connector GetDataService();
    }
}
