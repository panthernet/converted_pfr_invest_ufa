﻿using System.Reflection;
using System.Runtime.CompilerServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ПТК ДОКИП. Клиентский модуль связи с системой авторизации и управления доступом.")]

[assembly: InternalsVisibleTo("PFR_INVEST.Tests")]
