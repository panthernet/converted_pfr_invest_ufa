﻿using System;
using System.IO;

namespace TestECASA.ServiceEmulator
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace = "http://ws.auth.ecasa.pfr.rstyle.com/", ConfigurationName = "Iservice")]
    public interface IService
    {
        [System.ServiceModel.OperationContractAttribute(Action = "*", ReplyAction = "*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults = true)]
        isLoggedInResponse isLoggedIn(string userName);


    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.auth.ecasa.pfr.rstyle.com/")]
    public class isLoggedInResponse
    {
        public bool Result;
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KIPSeviceEmulator" in both code and config file together.
	//[DispatchByBodyElementServiceBehaviorAttribute]
	public class ECASASeviceEmulator : IService
	{
	    public isLoggedInResponse isLoggedIn(string userName)
	    {
	        return new isLoggedInResponse() { Result = true };
	    }
	}
}
