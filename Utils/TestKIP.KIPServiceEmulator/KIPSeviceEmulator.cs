﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;


namespace TestKIP.KIPServiceEmulator
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KIPSeviceEmulator" in both code and config file together.
	//[DispatchByBodyElementServiceBehaviorAttribute]
	public class KIPSeviceEmulator : Iservice
	{
		private string Register(byte[] file)
		{
			var baseFolder = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
			var zipFolder = Path.Combine(baseFolder, "OutputFiles");
			if (!Directory.Exists(zipFolder))
				Directory.CreateDirectory(zipFolder);

			var fileName = string.Format("{0}_{1}.zip", DateTime.Now.ToString("yyyy-dd-MM-hh-mm-ss"), Guid.NewGuid());

			File.WriteAllBytes(Path.Combine(zipFolder, fileName), file);

			Console.WriteLine("Принят файл '{0}'", fileName);

			return "Без ошибки";
		}

		public RegisterResponse1 Register(RegisterRequest1 request)
		{
			try
			{
				var res = this.Register(request.RegisterRequest.inputData);

				return new RegisterResponse1() { RegisterResponse = new RegisterResponse() { outputData = res } };
			}
			catch(Exception ex)
			{
				return new RegisterResponse1() { RegisterResponse = new RegisterResponse() { outputData = "Ошибка чтения" } };
			}
		}
	}
}
