﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using PFR_INVEST.Common.Tools;

namespace TestVIO.RESTServiceEmulator
{
    public class Config
    {
        public static int RestPort => ConfigurationManager.AppSettings["RestPort"].To<int>();

        public static string RestBaseUri => ConfigurationManager.AppSettings["RestBaseUri"] ?? "";

        public static string StreamDocumentName => ConfigurationManager.AppSettings["StreamDocumentName"] ?? "";
    }
}
