﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PFR_INVEST.Constants.Identifiers
{
    public static class BalanceExtraIdentifier
    {
        public const string CB_BUY = "Покупка ценных бумаг";
        public const string CB_SELL = "реализация активов";
        public const string DEPOSIT_SETLE = "Размещение депозита";
        public const string DEPOSIT_RETURN = "возврат средств по депозитам";

        public const string CB_COUPON = "Купонный доход";

    }
}
