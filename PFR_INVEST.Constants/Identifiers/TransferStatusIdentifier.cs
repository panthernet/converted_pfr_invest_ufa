﻿using System;

namespace PFR_INVEST.Constants.Identifiers
{
	public class TransferStatusIdentifier
	{
		/// <summary>
		/// "Начальное состояние"
		/// </summary>
		public const string sBeginState = "Начальное состояние";

		/// <summary>
		/// "Заявка оформлена"
		/// </summary>
		public const string sClaimCompleted = "Заявка оформлена";

		/// <summary>
		/// "Требование сформировано"
		/// </summary>
		public const string sDemandGenerated = "Требование сформировано";

		/// <summary>
		/// "Требование отправлено в УК"
		/// </summary>
		public const string sDemandSentInUK = "Требование отправлено в УК";

		/// <summary>
		/// "СПН перечислены"
		/// </summary>
		public const string sSPNTransferred = "СПН перечислены";

		/// <summary>
		/// "Требование получено УК"
		/// </summary>
		public const string sDemandReceivedUK = "Требование получено УК";

		/// <summary>
		/// "Акт подписан"
		/// </summary>
		public const string sActSigned = "Акт подписан";

		public const string sFakeTransfer = "Нет перечислений";

		public enum ActionTransfer { EnterTransferActData, RollbackStatus, DeleteTransfer };

		public const string EnterTransferActData = "Ввод даты акта передачи";
		public const string RollbackStatus = "Откат статуса";
		public const string DeleteTransfer = "Удаление перечисления";

		public static bool IsStatusRequestReceivedByUK(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sDemandReceivedUK, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusSPNTransfered(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sSPNTransferred, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusRequestSentToUK(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sDemandSentInUK, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusRequestFormed(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sDemandGenerated, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusInitialState(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sBeginState, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusRequestRegistered(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sClaimCompleted, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsStatusActSigned(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sActSigned, StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsFakeTransfer(string p_sStatus)
		{
			return p_sStatus != null && p_sStatus.Trim().Equals(sFakeTransfer, StringComparison.OrdinalIgnoreCase);
		}
	}
}