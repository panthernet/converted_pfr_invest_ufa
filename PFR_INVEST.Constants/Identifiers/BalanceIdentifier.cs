﻿using System;

namespace PFR_INVEST.Constants.Identifiers
{

	public enum SaldoParts
	{
        //0 - используется для проверки на отсудствие записей
        Accoperation = 1,
		Apps = 2,
		DoApps = 3,
		AddSpn = 4,
		DopSpn_M = 5,
		[Obsolete("Не используется в сальдо/счета", true)]
		K_Doc = 6,
		OrdReport = 7,
		Cost = 8,
		IncomeSec = 9,
		Asg_Fin_Tr = 10,
		Req_Transfer = 11,
		Transfer = 12,
		Return = 13,
		Deposit = 14,
		BalanceExtra = 15
	}

	public struct BalanceIdentifier
	{

		public const string s_Incoming = "приход";
		public const string s_Outgoing = "расход";
		public const string s_BuyingCB = "покупка ЦБ";
		public const string s_SellingCB = "продажа ЦБ";
		public const string s_ReturnFromUKToPFR = "Возврат из УК в ПФР";
		public const string s_TransferFromPFRToUK = "Передача из ПФР в УК";
		public const string s_ReturnFromUKToPFR_VR = "Возврат из ГУК ВР в ПФР";
		public const string s_TransferFromPFRToUK_VR = "Передача из ПФР в ГУК ВР";


		public const string s_TransferCostsILS = "Перечисление. Расходы на ведение СЧ ИЛС";
		public const string s_ReturnCostsILS = "Возврат. Расходы на ведение СЧ ИЛС";

		public const string s_DepositPlacement = "Размещение депозита";
		public const string s_DepositReturn = "Возврат депозита";

		//Уже есть в IncomeSecIdentifier.s_DepositsPercents
		//public const string s_DepositPercent = "Проценты по депозитам";
	}

	public static class DepclaimType
	{
		[Obsolete("Use DepClaimMaxListItem.Types.Max instead", true)]
		public const int DepclaimMax = 1;
		[Obsolete("Use DepClaimMaxListItem.Types.Common instead", true)]
		public const int Depclaim = 2;

	}
}
