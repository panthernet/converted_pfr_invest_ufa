﻿namespace PFR_INVEST.Constants.Identifiers
{
    public enum DocumentStatus
    {
        main,
        attachment
    }

    public class CorrespondenceComments
    {
        public const string ExecutorNotAssigned = "Исполнитель не назначен";
        public const string Attachment = "Связанный документ";
    }
}
