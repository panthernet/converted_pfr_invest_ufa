﻿using System;

namespace PFR_INVEST.Constants.Identifiers
{
	public static class SupAgreementIdentifier
	{
		public const string PROLONGATION = "Продление договора ДУ";
        public const string CHANGE_NAME = "Смена наименования";
        public const string OTHER = "Прочее";

        /// <summary>
        /// Является ли доп. соглашение продлением срока договора
        /// </summary>
        /// <param name="value">Тип доп. соглашения</param>
		public static bool IsProlongation(string value)
		{
			return value != null && value.Trim().Equals(PROLONGATION, StringComparison.OrdinalIgnoreCase);
		}

        /// <summary>
        /// Является ли доп. соглашение сменой наименования
        /// </summary>
        /// <param name="value">Тип доп. соглашения</param>
        public static bool IsNameChange(string value)
        {
            return value != null && value.Trim().Equals(CHANGE_NAME, StringComparison.OrdinalIgnoreCase);
        }
	}
}
