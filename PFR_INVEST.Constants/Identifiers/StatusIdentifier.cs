﻿using System;
using System.Collections;
using System.Runtime.Serialization;

namespace PFR_INVEST.Constants.Identifiers
{

	public enum BOP // boolean operation
	{
		Or,
		And
	}

	public struct OP
	{
		public const string EQUAL = "=";
		public const string NOTEQUAL = "<>";
	}

	public struct FilterOP
	{
		public BOP BOP;
		public string OP;
		public string Identifier;
	}

	[DataContract]
	public class StatusFilter : IEnumerable
	{
		/// <summary>
		/// Фильтр для всех статусов кроме "Удален"
		/// аналог
		/// new filterOP(){ bop = BOP.and, op = OP.notequal, IDENTIFIER = StatusIdentifier.s_Deleted}
		/// </summary>
		[IgnoreDataMember]		
		public static StatusFilter NotDeletedFilter
		{
			get
			{
				return new StatusFilter() 
                                    { 
                                        new FilterOP(){ BOP = BOP.And, OP = OP.NOTEQUAL, Identifier = StatusIdentifier.S_DELETED}
                                    };
			}
		}

		[DataMember]
		public const string FILTER_QUERY = "LOWER(TRIM({0}.Name)) {1} :st{2}";
		[DataMember]
		public FilterOP[] Filter;

		public void Add(FilterOP f)
		{
			f.Identifier = f.Identifier.Trim().ToLower();
			if (Filter == null)
				Filter = new[] { f };
			else
			{
				int size = Filter.Length + 1;
				Array.Resize(ref Filter, size);
				Filter[size - 1] = f;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public SFEnum GetEnumerator()
		{
			return new SFEnum(Filter);
		}

		public class SFEnum : IEnumerator
		{
		    readonly FilterOP[] _f;
			int _pos;

			public SFEnum(FilterOP[] f)
			{
				_f = f;
				Reset();
			}

			public bool MoveNext()
			{
				_pos++;
				return (_pos < _f.Length);
			}

			public void Reset()
			{
				_pos = -1;
			}

			object IEnumerator.Current
			{
				get { return Current; }
			}

			public FilterOP Current
			{
				get
				{
					try
					{
						return _f[_pos];
					}
					catch (IndexOutOfRangeException)
					{
						throw new InvalidOperationException();
					}
				}
			}
		}
	}

	public struct StatusIdentifier
	{
        /// <summary>
        /// Идентификаторы из таблицы STATUS статусов договоров
        /// </summary>
        public enum Identifier : long
        {
            Active = 1L,
            Suspended = 2L,
            LicenseRevoked = 3L,
            LicenseSuspended = 4L,
            Reorganized = 5L,
            Eliminated = 6L,
            Deleted = -1L,
            Deleted2 = -2L
        }

		/// <summary>
		/// "Деятельность осуществляется"
		/// </summary>
		public const string S_ACTIVE = "Деятельность осуществляется";
		/// <summary>
		/// "Деятельность приостановлена"
		/// </summary>
		public const string S_SUSPENDED = "Деятельность приостановлена";
		public const string S_LICENSE_REVOKED = "Лицензия аннулирована";
		public const string S_LICENSE_SUSPENDED = "Лицензия приостановлена";
		public const string S_REORGANIZED = "Реорганизован";
		public const string S_ELIMINATED = "Ликвидирован";
		public const string S_DELETED = "Удален";

		public static bool IsNPFStatusActivityCarries(string status)
		{
			return status != null && status.Trim().ToLower().Equals(S_ACTIVE.ToLower());
		}
		public static bool IsNPFStatusActivitySuspended(string status)
		{
			return status != null && status.Trim().ToLower().Equals("приостановление привлечения зл");
		}
		public static bool IsNPFStatusLicenseAnnuled(string status)
		{
			return status != null && status.Trim().ToLower().Equals(S_LICENSE_REVOKED.ToLower());
		}
		public static bool IsNPFStatusLicenseSuspended(string status)
		{
			return status != null && status.Trim().ToLower().Equals(S_LICENSE_SUSPENDED.ToLower());
		}
		public static bool IsNPFStatusReorganized(string status)
		{
			return status != null && status.Trim().ToLower().Equals(S_REORGANIZED.ToLower());
		}
		public static bool IsNPFStatusLiquidated(string status)
		{
			return status != null &&status.Trim().ToLower().Equals(S_ELIMINATED.ToLower());
		}

		public static string GetStatusName(long value)
		{
			switch ((Identifier)value)
			{
				case Identifier.Active: return S_ACTIVE;
				case Identifier.Suspended: return S_SUSPENDED;
				case Identifier.LicenseRevoked: return S_LICENSE_REVOKED;
				case Identifier.LicenseSuspended: return S_LICENSE_SUSPENDED;
				case Identifier.Reorganized: return S_REORGANIZED;
				case Identifier.Eliminated: return S_ELIMINATED;
                default: return S_DELETED;
            }
		}
	}
}
