﻿using System.Web.Mvc;

namespace PFR_INVEST.MetricService.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}
