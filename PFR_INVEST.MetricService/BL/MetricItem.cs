﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFR_INVEST.MetricService.BL
{
	public class MetricItem
	{
		public string ServiveCode { get; set; }
		public DateTime Date { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }

		public MetricItem()
		{
			this.ServiveCode = "SERVICE_DOKIP";
			this.Date = DateTime.Now;
		}

		public MetricItem(string name, string value)
			: this()
		{
			this.Name = name;
			this.Value = value;
		}
	}
}