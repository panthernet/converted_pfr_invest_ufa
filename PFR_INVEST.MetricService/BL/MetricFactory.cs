﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFR_INVEST.MetricService.BL
{
	public static class MetricFactory
	{
		public const string DBSERVICE = "DBSERVICE";
		public const string DBSERVER = "DBSERVER";
		public const string DBENVIRONMENT = "DBENVIRONMENT";
		public const string ACTIVEUSERS = "ACTIVEUSERS";
		public const string SETTINGSSERVICE = "SETTINGSSERVICE";
		public const string ALLSYSTEMS = "ALLSYSTEMS";


		public const string STATUS_GOOD = "GOOD";
		public const string STATUS_BAD = "BAD";

		public static List<MetricItem> GetMetric()
		{
			bool AllGood = true;
			var metric = new List<MetricItem>();
			
			metric.AddRange(GetServiceDBMetric(ref AllGood));

			metric.AddRange(GetServiceSettingsMetric(ref AllGood));


			if(AllGood)
				metric.Add(new MetricItem(ALLSYSTEMS, STATUS_GOOD));
			else
				metric.Add(new MetricItem(ALLSYSTEMS, STATUS_BAD));

			return metric;
		}


		public static List<MetricItem> GetServiceDBMetric(ref bool allGood)
		{
			var metric = new List<MetricItem>();
			var client = WCFClient.StartClient("test", "test");

			if (!client)
			{
				metric.Add(new MetricItem(DBSERVICE, STATUS_BAD));
				allGood = false;
				return metric;
			}
			else
			{
				//Проверяем, что сервис сам по себе работает
				try
				{
					var version = WCFClient.Current.Client.GetServiceVersion();
					metric.Add(new MetricItem(DBSERVICE, STATUS_GOOD));
				}
				catch (Exception)
				{
					metric.Add(new MetricItem(DBSERVICE, STATUS_BAD));
					allGood = false;
					return metric;
				}
				

				//Проверяем, что работает выборка из базы данных
				try
				{
					var data = WCFClient.Current.Client.GetContactsForUKs();
					metric.Add(new MetricItem(DBSERVER, STATUS_GOOD));
				}
				catch(Exception) 
				{
					metric.Add(new MetricItem(DBSERVER, STATUS_BAD));
					allGood = false;
					return metric;
				}

				try 
				{
					var users = WCFClient.Current.Client.GetMetricActiveUser();
					metric.Add(new MetricItem(ACTIVEUSERS, users.ToString()));
				}
				catch (Exception) 
				{
				
				}

				try
				{
					var fix = WCFClient.Current.Client.GetMetricDecimalFix();
					if(fix)
						metric.Add(new MetricItem(DBENVIRONMENT, STATUS_GOOD));
					else
						metric.Add(new MetricItem(DBENVIRONMENT, STATUS_BAD));
				}
				catch (Exception)
				{

				}

				try
				{
					//Отключаемся от сервиса, что-бы не висели в статистике активных ещё 20 мин.
					WCFClient.Current.Client.CloseConnection();
				}
				catch (Exception) 
				{ 
				
				}

				//Проверяем загруженость базы
			}
			return metric;
		}

		public static List<MetricItem> GetServiceSettingsMetric(ref bool allGood) 
		{
			var metric = new List<MetricItem>();
			try
			{
				var client = new UserSettingsServiceClient();
				client.Client.Ping();

				metric.Add(new MetricItem(SETTINGSSERVICE, STATUS_GOOD));
			}
			catch (Exception ex) 
			{
				metric.Add(new MetricItem(SETTINGSSERVICE, STATUS_BAD));
				allGood = false;
				return metric;
			}

			return metric;
		}
	}
}