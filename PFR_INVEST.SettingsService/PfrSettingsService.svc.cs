﻿using System;
using PFR_INVEST.SettingsService.Contract;
using PFR_INVEST.SettingsService.ProvidersBase;

namespace PFR_INVEST.SettingsService
{
    public class PfrSettingsService : ISettingsService
    {
        private Exception _lastError;

        public UserSettings GetUserSettings(string domain, string userName)
        {
            try
            {
                var us = new UserSettings
                {
                    Domain = domain,
                    UserName = userName,
                    Settings =
                        UserSettingsProviderService.LoadUserSettings(domain, userName)
                };

                return us;
            }
            catch (Exception err)
            {
                _lastError = err;
                return null;
            }
        }

        public bool SetUserSettings(UserSettings userSettings)
        {
            try
            {
                UserSettingsProviderService.SaveUserSettings(userSettings.Domain,
                                                             userSettings.UserName,
                                                             userSettings.Settings);
                return true;
            }
            catch (Exception err)
            {
                _lastError = err;
                return false;
            }
        }

        public string GetLastError()
        {
            return _lastError == null ? string.Empty : _lastError.ToString();
        }

        public string Ping()
        {
            return "Test";
        }
    }
}