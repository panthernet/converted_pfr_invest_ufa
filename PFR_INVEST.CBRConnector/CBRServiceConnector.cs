﻿using System;
using System.Data;
using System.Linq;
using PFR_INVEST.Core.BusinessLogic;

namespace PFR_INVEST.CBRConnector
{
    public class CBRServiceConnectorException : Exception
    {
        public CBRServiceConnectorException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public CBRServiceConnectorException(string message)
            : base(message)
        {
        }
    }

    public class CBRServiceConnector : ICBRConnector
    {
        private const string TABLE_CURRENCIES_COURSES_ON_DATE = "ValuteCursOnDate";
        private const string SITE_CONNECTION_ERROR = "Ошибка получения данных с сайта. Проверьте настройки соединения с интернетом.";
        private const string COURCE_NOT_FOUND = "Курс на {0} для валюты {1} не найден.";
        private const string INVALID_DATE = "Невозможно получить курс на дату, которая еще не наступила.";
        private const string DOLLARS = "Доллар США";
        private const string EURO = "Евро";

        private static string GetCurrencyNameForID(long pCurrencyID)
        {
            switch (pCurrencyID)
            {
                case 1:
                case 3:
                    return EURO;
                case 2:
                    return DOLLARS;
                default:
                    return null;
            }
        }

        public decimal GetCourseRate(long pCurrencyID, DateTime pDate)
        {
            string currencyName = GetCurrencyNameForID(pCurrencyID);
            if (currencyName == null) return 0;
            //Проверка даты, за которую запрашивается курс (не больше текущей UTC даты)
            if (pDate.Date > DateTime.UtcNow.Date)
            {
                throw new CBRServiceConnectorException(INVALID_DATE);
            }

            //Получаем ответ от сервиса
            DataTable response;
            try
            {
                response = DailyInfoClient.Client.GetCursOnDate(pDate).Tables[TABLE_CURRENCIES_COURSES_ON_DATE];
            }
            catch (Exception ex)
            {
                throw new CBRServiceConnectorException(SITE_CONNECTION_ERROR, ex);
            }

            //Проверяем ответ
            EnumerableRowCollection<DataRow> currenciesCourcesOnDate;
            if (response == null)
                throw new CBRServiceConnectorException(string.Format(COURCE_NOT_FOUND, pDate.ToString("dd.MM.yyyy"), currencyName));
            currenciesCourcesOnDate = response.AsEnumerable();

            //Достаем курс
            var currencyCource = (from row in currenciesCourcesOnDate
                where row.Field<string>("Vname").Trim() == currencyName
                select row.Field<decimal>("Vcurs"));
            if (currencyCource.Any())
                return currencyCource.First();
            throw new CBRServiceConnectorException(string.Format(COURCE_NOT_FOUND, pDate.ToString("dd.MM.yyyy"), currencyName));
        }
    }
}
